! ========================== !
! Lagrangian particle solver !
! ========================== !
module lpt_solver
  use lpt
  use lpt_acoustics
  implicit none
  
  ! Local copy of current particle
  type(part_type) :: myp
  
  ! Gas phase properties at particle position
  real(WP) :: ug,vg,wg
  real(WP) :: ep,eg
  real(WP) :: rhog,mug
  real(WP), dimension(3) :: stress
  
  ! Jacobian
  real(WP) :: dxdt,dydt,dzdt
  real(WP) :: dudt,dvdt,dwdt
  real(WP) :: dwxdt,dwydt,dwzdt
  real(WP) :: accx,accy,accz
  
  ! Acoustophoresis
  real(WP) :: acoust_x,acoust_y,acoust_z
  
  ! Optimal timestep
  real(WP) :: dtd
  
end module lpt_solver


! ===================================== !
! Time solver - advance - wall - locate !
! 2nd order RK                          !
! ===================================== !
subroutine lpt_solver_solve(dt_solve,myi)
  use lpt_solver
  implicit none
  
  real(WP) :: dt_solve,mass
  integer, intent(in) :: myi
  
  ! Create local copy of particle
  myp=part(myi)
  
  ! Advance - Euler prediction
  call lpt_solver_rhs
  myp%x  = part(myi)%x + 0.5_WP*dxdt*dt_solve
  myp%y  = part(myi)%y + 0.5_WP*dydt*dt_solve
  myp%z  = part(myi)%z + 0.5_WP*dzdt*dt_solve
  myp%u  = part(myi)%u + 0.5_WP*dudt*dt_solve
  myp%v  = part(myi)%v + 0.5_WP*dvdt*dt_solve
  myp%w  = part(myi)%w + 0.5_WP*dwdt*dt_solve
  myp%wx = part(myi)%wx + 0.5_WP*dwxdt*dt_solve
  myp%wy = part(myi)%wy + 0.5_WP*dwydt*dt_solve
  myp%wz = part(myi)%wz + 0.5_WP*dwzdt*dt_solve
  
  ! Correct - Midpoint rule
  call lpt_solver_rhs
  myp%x  = part(myi)%x + dxdt*dt_solve
  myp%y  = part(myi)%y + dydt*dt_solve
  myp%z  = part(myi)%z + dzdt*dt_solve
  myp%u  = part(myi)%u + dudt*dt_solve
  myp%v  = part(myi)%v + dvdt*dt_solve
  myp%w  = part(myi)%w + dwdt*dt_solve
  myp%wx = part(myi)%wx + dwxdt*dt_solve
  myp%wy = part(myi)%wy + dwydt*dt_solve
  myp%wz = part(myi)%wz + dwzdt*dt_solve
  
  ! Top and bottom conditions
  if (xper.eq.0) then
     ! Prevent particles from escaping the bottom
     if (myp%x-0.5_WP*myp%d.lt.x(imin)) myp%x=x(imin)+0.5_WP*myp%d
     if (bound_top) then
        ! Prevent particles from escaping the top
        if (myp%x+0.5_WP*myp%d.ge.x(imax+1)) myp%x=x(imax+1)-0.5_WP*myp%d
     else
        ! Exit condition at top
        if (myp%x.gt.x(imax+1)) then
           myp%stop=1
           npart_out=npart_out+1
        end if
     end if
  end if
  
  ! Relocalize
  call lpt_localize(part(myi)%i,part(myi)%j,part(myi)%k,&
                          myp%i,      myp%j,      myp%k,&
                          myp%x,      myp%y,      myp%z)
  
  ! Exchange source terms with gas phase
  if (lpt_twoway) then
     mass=rhod*Pi/6.0_WP*myp%d**3
     call lpt_source_momentum(myp%x,myp%y,myp%z,&
                              myp%i,myp%j,myp%k,&
                              myp%u,accx*dt_solve,myp%v,accy*dt_solve,myp%w,accz*dt_solve,&
                              mass,0.0_WP,1.0_WP)
  end if
  
  ! Update optimal timestep size
  myp%dt=dtd
  
  ! Copy back to particle
  if (myp%id.ne.-1) part(myi)=myp
  
  return
end subroutine lpt_solver_solve


! =============== !
! RHS computation !
! =============== !
subroutine lpt_solver_rhs
  use lpt_solver
  implicit none
  
  real(WP) :: Rep,drag,taud,I,madd
  real(WP) :: b1,b2,F
  !real(WP) :: cd,ksi
  
  ! Interpolate the gas phase info at the droplet location
  call lpt_solver_interpolate
  
  ! Particle Reynolds number (include gas volume fraction)
  Rep = rhog*eg*sqrt((myp%u-ug)**2+(myp%v-vg)**2+(myp%w-wg)**2)*myp%d/mug
  Rep = Rep+epsilon(1.0_WP)
  
  ! Tenneti & Subramaniam (2011)
  b1 = 5.81_WP*ep/eg**3+0.48_WP*ep**(1.0_WP/3.0_WP)/eg**4
  b2 = ep**3*Rep*(0.95_WP+0.61_WP*ep**3/eg**2)
  F = (1.0_WP+0.15_WP*Rep**(0.687_WP))/eg**3+b1+b2
  F = eg*F ! To remove mean pressure gradient forcing
  
  ! Beetstra (2006)
  !b1 = 10.0_WP*ep/eg**2 + eg**2 * (1.0_WP+1.5_WP*sqrt(ep))
  !b2 = 0.413_WP/24.0_WP*Rep/eg**2*(1.0_WP/eg+3.0_WP*eg*ep+8.4_WP*Rep**(-0.343_WP))/(1.0_WP+10.0_WP**(3.0_WP*ep)*Rep**(2.0_WP*eg-2.5_WP))
  !F = b1 + b2
  
  ! Di Felice
  !cd = (0.63_WP+4.8_WP/sqrt(Rep))**2
  !ksi = 3.7_WP-0.65_WP*exp(-0.5_WP*(1.5_WP-log(Rep)/log(10.0_WP))**2)
  !F = Rep/24.0_WP*cd*eg**(-ksi)
  
  ! Particle response time
  taud = rhod*myp%d**2/(18.0_WP*mug)
  
  ! Drag force over particle mass
  drag = F/taud
  
  ! Particle moment of inertia per unit mass
  I = 0.1_WP*myp%d**2
  
  ! Gas phase source term
  accx = drag*eg*(ug-myp%u)+stress(1)/rhod
  accy = drag*eg*(vg-myp%v)+stress(2)/rhod
  accz = drag*eg*(wg-myp%w)+stress(3)/rhod
  
  ! Return rhs
  dxdt = myp%u
  dydt = myp%v
  dzdt = myp%w
  dudt = accx+gravity(1)+myp%Acol(1)
  dvdt = accy+gravity(2)+myp%Acol(2)
  dwdt = accz+gravity(3)+myp%Acol(3)
  dwxdt = myp%Tcol(1)/I
  dwydt = myp%Tcol(2)/I
  dwzdt = myp%Tcol(3)/I

  ! Account for added mass
  !madd = 0.5_WP*rhog/rhod
  !accx = accx - madd * (dudt-myp%Acol(1))
  !accy = accy - madd * (dvdt-myp%Acol(2))
  !accz = accz - madd * (dwdt-myp%Acol(3))
  !dudt = dudt - madd * (dudt-myp%Acol(1))
  !dvdt = dvdt - madd * (dvdt-myp%Acol(2))
  !dwdt = dwdt - madd * (dwdt-myp%Acol(3))
  
  ! Acoustophoresis force - with added mass?
  if (use_acoustics) then
     madd=0.5_WP*rhog/rhod
     dudt=dudt-(1.0_WP-madd)*acoust_x/rhod
     dudt=dudt-(1.0_WP-madd)*acoust_y/rhod
     dudt=dudt-(1.0_WP-madd)*acoust_z/rhod
  end if
  
  ! Update optimal timestep size
  dtd = taud/lpt_substeps
    
  return
end subroutine lpt_solver_rhs


! ======================== !
! Gas phase interpolations !
! ======================== !
subroutine lpt_solver_interpolate
  use lpt_solver
  use interpolate
  implicit none
    
  integer :: sx,sy,sz
  integer :: i1,j1,k1
  integer :: i2,j2,k2
  real(WP) :: wx1,wx2,wy1,wy2,wz1,wz2,mysum
  real(WP), dimension(2,2,2) :: wwd,wwn
  real(WP), parameter :: eps=1.0e-9_WP
  
  ! Get interpolation cells w/r to centroids
  sx = -1
  sy = -1
  sz = -1
  if (myp%x.ge.xm(myp%i)) sx = 0
  if (myp%y.ge.ym(myp%j)) sy = 0
  if (myp%z.ge.zm(myp%k)) sz = 0
  
  ! Get the first interpolation point
  i1=myp%i+sx; i2=i1+1
  j1=myp%j+sy; j2=j1+1
  k1=myp%k+sz; k2=k1+1
  
  ! Compute the linear interpolation coefficients
  wx1 = (xm(i2)-myp%x )/(xm(i2)-xm(i1))
  wx2 = (myp%x -xm(i1))/(xm(i2)-xm(i1))
  wy1 = (ym(j2)-myp%y )/(ym(j2)-ym(j1))
  wy2 = (myp%y -ym(j1))/(ym(j2)-ym(j1))
  wz1 = (zm(k2)-myp%z )/(zm(k2)-zm(k1))
  wz2 = (myp%z -zm(k1))/(zm(k2)-zm(k1))
  
  ! Combine the interpolation coefficients to form a tri-linear interpolation
  wwd(1,1,1)=wx1*wy1*wz1
  wwd(2,1,1)=wx2*wy1*wz1
  wwd(1,2,1)=wx1*wy2*wz1
  wwd(2,2,1)=wx2*wy2*wz1
  wwd(1,1,2)=wx1*wy1*wz2
  wwd(2,1,2)=wx2*wy1*wz2
  wwd(1,2,2)=wx1*wy2*wz2
  wwd(2,2,2)=wx2*wy2*wz2
  
  ! Correct for walls with Dirichlet condition
  if (mask(i1,j1).eq.1) wwd(1,1,:) = 0.0_WP
  if (mask(i1,j2).eq.1) wwd(1,2,:) = 0.0_WP
  if (mask(i2,j1).eq.1) wwd(2,1,:) = 0.0_WP
  if (mask(i2,j2).eq.1) wwd(2,2,:) = 0.0_WP
  
  ! Correct for walls with Neumann condition
  mysum=sum(wwd)+epsilon(1.0_WP)
  wwn = wwd/mysum
  
  ! Stress term
  stress(1)=sum(wwn*lpt_stress_x(i1:i2,j1:j2,k1:k2))
  stress(2)=sum(wwn*lpt_stress_y(i1:i2,j1:j2,k1:k2))
  stress(3)=sum(wwn*lpt_stress_z(i1:i2,j1:j2,k1:k2))
  
  ! Volume fractions
  eg=sum(wwn*epsf(i1:i2,j1:j2,k1:k2))
  eg=eg-eps
  ep=1.0_WP-eg
  
  ! Velocities
  ug=sum(wwd*Ui(i1:i2,j1:j2,k1:k2))
  vg=sum(wwd*Vi(i1:i2,j1:j2,k1:k2))
  wg=sum(wwd*Wi(i1:i2,j1:j2,k1:k2))
  
  ! Gas density
  rhog=sum(wwn*RHO(i1:i2,j1:j2,k1:k2))
  rhog=rhog/eg
  
  ! Gas viscosity
  mug=sum(wwn*VISCmol(i1:i2,j1:j2,k1:k2))
  
  ! Acoustophoresis force
  if (use_acoustics) then
     acoust_x=sum(wwn*AcAccx(i1:i2,j1:j2,k1:k2))
     acoust_y=sum(wwn*AcAccy(i1:i2,j1:j2,k1:k2))
     acoust_z=sum(wwn*AcAccz(i1:i2,j1:j2,k1:k2))
  end if
  
  return
end subroutine lpt_solver_interpolate
