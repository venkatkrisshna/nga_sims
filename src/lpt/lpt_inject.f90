! ================== !
! Particle injection !
! ================== !
module lpt_inject
  use lpt
  implicit none
  
  ! Injection parameters
  character(len=str_medium) :: lpt_injection
  
  ! Bulk uniform injection
  real(WP) :: mfr
  real(WP) :: inj_u,inj_v,inj_w

  ! Reinjection parameters
  integer :: npart0
  
  ! Particle inflow file
  character(len=str_medium) :: part_inflow
  integer :: inflow_unit,npart_inflow,nflow
  integer :: npart_inflow_read
  real(WP) :: inflow_time, t_in
  type(part_type) :: part_in
  
contains
  
  ! Compute particle diameter
  function get_diameter() result(dp)
    use math
    use random
    implicit none
    real(WP) :: dp
    dp=random_lognormal(m=dmean-dshift,sd=dsd)+dshift
    do while (dp.gt.dmax+epsilon(1.0_WP).or.dp.lt.dmin-epsilon(1.0_WP))
       dp=random_lognormal(m=dmean-dshift,sd=dsd)+dshift
    end do
  end function get_diameter
  
end module lpt_inject


! ============================ !
! Initialize injection routine !
! ============================ !
subroutine lpt_inject_init
  use lpt_inject
  use parser
  use parallel
  implicit none
  
  integer :: i,mysize,ierr
  real(WP) :: my_dmin,my_dmax,my_dmean
  
  ! Injection parameters
  call parser_read('Particle injection',lpt_injection,'none')
  select case (trim(lpt_injection))
  case ('none')
     ! Read particle diameter from part file
     my_dmin =+huge(1.0_WP)
     my_dmax =-huge(1.0_WP)
     my_dmean=0.0_WP
     do i=1,npart_
        my_dmin =min(my_dmin,part(i)%d)
        my_dmax =max(my_dmax,part(i)%d)
        my_dmean=my_dmean+part(i)%d
     end do
     call parallel_min(my_dmin ,dmin )
     call parallel_max(my_dmax ,dmax )
     call parallel_sum(my_dmean,dmean); dmean=dmean/npart
     dsd=0.0_WP
     dshift=0.0_WP
  case ('bulk')
     ! Mass flow rate to impose
     call parser_read('Particle mass flow rate',mfr)
     ! Injection velocity
     call parser_read('Particle u velocity',inj_u)
     call parser_read('Particle v velocity',inj_v,0.0_WP)
     call parser_read('Particle w velocity',inj_w,0.0_WP)
     ! Read particle diameter from input file
     call parser_read('Particle mean diameter',dmean)
     call parser_read('Particle standard deviation',dsd)
     call parser_read('Particle min diameter',dmin)
     call parser_read('Particle max diameter',dmax)
     call parser_read('Particle diameter shift',dshift)
     if (dsd.le.epsilon(1.0_WP)) then
        dmin=dmean
        dmax=dmean
     end if
  case ('reinject')
     ! Keep initial number of particles constant
     npart0=npart
     ! Injection velocity
     call parser_read('Particle u velocity',inj_u)
     call parser_read('Particle v velocity',inj_v,0.0_WP)
     call parser_read('Particle w velocity',inj_w,0.0_WP)
     ! Read particle diameter from input file
     call parser_read('Particle mean diameter',dmean)
     call parser_read('Particle standard deviation',dsd)
     call parser_read('Particle min diameter',dmin)
     call parser_read('Particle max diameter',dmax)
     call parser_read('Particle diameter shift',dshift)
     if (dsd.le.epsilon(1.0_WP)) then
        dmin=dmean
        dmax=dmean
     end if
  case ('file')
     ! Particle inflow file to read from
     call parser_read('Part inflow file',part_inflow)
     ! Injection velocity
     call parser_read('Injection u velocity',inj_u)
     ! Only root continues
     if (irank.eq.iroot) then
        ! Prepare access to the particle inflow file
        call BINARY_FILE_OPEN(inflow_unit,trim(part_inflow),"r",ierr)
        ! Read the header
        call BINARY_FILE_READ(inflow_unit,npart_inflow,1,kind(npart_inflow),ierr)
        call BINARY_FILE_READ(inflow_unit,mysize,1,kind(mysize),ierr)
        if (mysize.ne.part_kind) call die('Particle inflow file incompatible with current particle type.')
        call BINARY_FILE_READ(inflow_unit,inflow_time,1,kind(inflow_time),ierr)
        inflow_time=inflow_time/inj_u
        nflow=floor(time/inflow_time)
        ! Read up to the right position without saving - read one more particle than needed
        t_in=-huge(1.0_WP)
        npart_inflow_read=0
        do while (t_in.lt.mod(time,inflow_time))
           call BINARY_FILE_READ(inflow_unit,part_in,1,part_kind,ierr)
           npart_inflow_read=npart_inflow_read+1
           t_in=part_in%x/inj_u
        end do
     end if
  case default
     call die('Unknown particle injection type.')
  end select
  
  return
end subroutine lpt_inject_init

! =========================== !
! Add new particles uniformly !
! =========================== !
subroutine lpt_inject_bulk
  use lpt_inject
  use time_info
  use parallel
  use ib
  implicit none
  
  real(WP) :: Mgoal,Madded,Mtmp,buf
  real(WP), save :: previous_error=0.0_WP
  integer :: count,n,npart0_,npart_tmp,ibuf,ierr
  integer(kind=8) :: maxid_,maxid
  real(WP) :: dist

  ! Initial number of particles
  npart0_=npart_
  npart_in=0
  
  ! Get the particle mass that should be added to the system
  Mgoal  = mfr*dt+previous_error
  Madded = 0.0_WP

  ! Determine id to assign to particle
  maxid_=0
  do n=1,npart_
     maxid_=max(maxid_,part(n)%id)
  end do
  call MPI_ALLREDUCE(maxid_,maxid,1,MPI_INTEGER8,MPI_MAX,comm,ierr)

  ! Add new particles until desired mass is achieved
  do while (Madded.lt.Mgoal)

     if (irank.eq.iroot) then
        ! Initialize parameters
        Mtmp = 0.0_WP
        npart_tmp = 0
        ! Loop while the added volume is not sufficient
        do while (Mtmp.lt.Mgoal-Madded)
           ! Increment counter
           npart_tmp=npart_tmp+1
           count = npart0_+npart_tmp
           ! Create space for new particle
           call lpt_resize(count)
           ! Generate a diameter
           part(count)%d=get_diameter()
           ! Set various parameters for the particle
           part(count)%id  =maxid + int(npart_tmp,8)
           part(count)%dt  =0.0_WP
           part(count)%Acol=0.0_WP
           part(count)%Tcol=0.0_WP
           part(count)%wx  =0.0_WP
           part(count)%wy  =0.0_WP
           part(count)%wz  =0.0_WP
           ! Give a position at the injector to the particle
           call lpt_get_position(part(count)%x,part(count)%y,part(count)%z,0.6_WP*part(count)%d)
           ! Localize the particle
           call lpt_locate(part(count)%x,part(count)%y,part(count)%z,part(count)%i,part(count)%j,part(count)%k)
           ! Give it a velocity
           part(count)%u=inj_u
           part(count)%v=inj_v
           part(count)%w=inj_w
           ! Make it an "official" particle
           part(count)%stop=0
           ! Update the added mass for the timestep
           Mtmp = Mtmp + rhod*Pi/6.0_WP*part(count)%d**3
        end do
     end if
     ! Communicate particles
     call lpt_communication
     ! Loop through newly created particles
     buf=0.0_WP
     do n=npart0_+1,npart_
        ! Remove if out of bounds
        if (vol(part(n)%i,part(n)%j,part(n)%k).le.0.0_WP) part(n)%stop=1
        if (use_ib) then
           call lpt_interpolate(Gib,part(n)%x,part(n)%y,part(n)%z,part(n)%i,part(n)%j,part(n)%k,dist,"SC")
           if (dist.lt.0.6_WP*part(n)%d) part(n)%stop=1
        end if
        if (part(n)%stop.eq.0) then
           ! Increment counter
           npart_in=npart_in+1
           ! Update the added mass for the timestep
           buf = buf + rhod*Pi/6.0_WP*part(n)%d**3
           ! Update the max particle id
           maxid = max(maxid,part(n)%id)
        end if
     end do
     ! Total mass added
     call parallel_sum(buf,Mtmp); Madded=Madded+Mtmp
     ! Clean up particles
     call lpt_recycle
     ! Update initial npart
     npart0_=npart_
     ! Maximum particle id
     call MPI_ALLREDUCE(maxid,maxid_,1,MPI_INTEGER8,MPI_MAX,comm,ierr); maxid=maxid_
  end do
  
  ! Remember the error
  previous_error = Mgoal-Madded
  
  ! Sum up injected particles
  call parallel_sum(npart_in,ibuf); npart_in=ibuf
  
contains
  
  ! Position for bulk injection of particles
  subroutine lpt_get_position(xp,yp,zp,mind)
    use lpt_inject
    use random
    implicit none
    
    real(WP), intent(out) :: xp,yp,zp
    real(WP), intent(in) :: mind
    real(WP) :: rand
    integer :: ip,jp,kp
    
    ! Random position
    xp = x(imin)+mind
    if (ny.eq.1) then
       yp = ym(jmin_)
    else
       call random_number(rand)
       yp = y(jmin)+mind+rand*(yL-2.0_WP*mind)
    end if
    if (nz.eq.1) then
       zp = zm(kmin_)
    else
       call random_number(rand)
       zp = z(kmin)+mind+rand*(zL-2.0_WP*mind)
    end if
    ! Localize the particle
    call lpt_locate(xp,yp,zp,ip,jp,kp)
    
    return
  end subroutine lpt_get_position
  
end subroutine lpt_inject_bulk


! =================================== !
! Reinject particles that left domain !
! =================================== !
subroutine lpt_inject_reinject
  use lpt_inject
  use parallel
  use ib
  implicit none
  
  integer :: count,n,npart0_,ibuf,ierr
  integer(kind=8) :: maxid_,maxid
  real(WP) :: dist

  if (trim(lpt_injection).ne.'reinject') return

  ! Initial number of particles
  npart0_=npart_
  npart_in=0

  ! Return if no particles left domain
  if (npart.eq.npart0) return

  ! Determine id to assign to particle
  maxid_=int(0,8)
  do n=1,npart_
     maxid_=max(maxid_,part(n)%id)
  end do
  call MPI_ALLREDUCE(maxid_,maxid,1,MPI_INTEGER8,MPI_MAX,comm,ierr)

  ! Add new particles
  do while (npart.ne.npart0)

     if (irank.eq.iroot) then
        ! Increment counter
        count = npart0_ + 1
        ! Create space for new particle
        call lpt_resize(count)
        ! Generate a diameter
        part(count)%d=get_diameter()
        ! Set various parameters for the particle
        part(count)%id  =maxid + int(1,8)
        part(count)%dt  =0.0_WP
        part(count)%Acol=0.0_WP
        part(count)%Tcol=0.0_WP
        part(count)%wx  =0.0_WP
        part(count)%wy  =0.0_WP
        part(count)%wz  =0.0_WP
        ! Give a position at the injector to the particle
        call lpt_get_position(part(count)%x,part(count)%y,part(count)%z,0.6_WP*part(count)%d)
        ! Localize the particle
        call lpt_locate(part(count)%x,part(count)%y,part(count)%z,part(count)%i,part(count)%j,part(count)%k)
        ! Give it a velocity
        part(count)%u=inj_u
        part(count)%v=inj_v
        part(count)%w=inj_w
        ! Make it an "official" particle
        part(count)%stop=0
     end if
     ! Communicate particles
     call lpt_communication
     ! Loop through newly created particles
     do n=npart0_+1,npart_
        ! Remove if out of bounds
        if (vol(part(n)%i,part(n)%j,part(n)%k).le.0.0_WP) part(n)%stop=1
        if (use_ib) then
           call lpt_interpolate(Gib,part(n)%x,part(n)%y,part(n)%z,part(n)%i,part(n)%j,part(n)%k,dist,"SC")
           if (dist.lt.0.6_WP*part(n)%d) part(n)%stop=1
        end if
        if (part(n)%stop.eq.0) then
           ! Increment counter
           npart_in=npart_in+1
           ! Update the max particle id
           maxid = max(maxid,part(n)%id)
        end if
     end do
     ! Clean up particles
     call lpt_recycle
     ! Update initial npart
     npart0_=npart_
     ! Maximum particle id
     call MPI_ALLREDUCE(maxid,maxid_,1,MPI_INTEGER8,MPI_MAX,comm,ierr); maxid=maxid_
  end do

  ! Sum up injected particles
  call parallel_sum(npart_in,ibuf); npart_in=ibuf
  
contains
  
  ! Position for bulk injection of particles
  subroutine lpt_get_position(xp,yp,zp,mind)
    use lpt_inject
    use random
    implicit none
    
    real(WP), intent(out) :: xp,yp,zp
    real(WP), intent(in) :: mind
    real(WP) :: rand
    integer :: ip,jp,kp
    
    ! Random position
    xp = x(imin)+mind
    if (ny.eq.1) then
       yp = ym(jmin_)
    else
       call random_number(rand)
       yp = y(jmin)+mind+rand*(yL-2.0_WP*mind)
    end if
    if (nz.eq.1) then
       zp = zm(kmin_)
    else
       call random_number(rand)
       zp = z(kmin)+mind+rand*(zL-2.0_WP*mind)
    end if
    ! Localize the particle
    call lpt_locate(xp,yp,zp,ip,jp,kp)
    
    return
  end subroutine lpt_get_position
  
end subroutine lpt_inject_reinject


! ==================== !
! Inject new particles !
! ==================== !
subroutine lpt_inject_step
  use lpt_inject
  implicit none
  
  ! Choose injection type
  select case (trim(lpt_injection))
  case ('none')
     return
  case ('bulk')
     call lpt_inject_bulk
  case ('file')
     call lpt_inject_file
  end select
  
  return
end subroutine lpt_inject_step


! ============================== !
! Add particle by injecting from !
! particle inflow file -- serial !
! ============================== !
subroutine lpt_inject_file
  use lpt_inject
  use time_info
  implicit none
  
  integer :: ip,mysize,ierr
  
  ! Prepare variables for info gathering
  npart_in=0
  
  ! Only the source processor reads the file
  if (irank.eq.iroot) then
     
     ! Check if rewind is necessary
     if (floor(time/inflow_time).ne.nflow) then
        
        ! Finish the file
        do while (npart_inflow_read.lt.npart_inflow)
           
           ! Current particle should be added (adjust position, velocity, and location)
           npart_in=npart_in+1
           ip=npart_+npart_in
           call lpt_resize(ip)
           part(ip)=part_in
           part(ip)%x=x(imin)+((time-real(nflow,WP)*inflow_time)*inj_u-part(ip)%x)
           part(ip)%u=part(ip)%u+inj_u
           call lpt_locate(part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k)
           
           ! Read one more particle
           call BINARY_FILE_READ(inflow_unit,part_in,1,part_kind,ierr)
           npart_inflow_read=npart_inflow_read+1
           t_in=part_in%x/inj_u
           
        end do
        
        ! Current particle should be added (adjust position, velocity, and location)
        npart_in=npart_in+1
        ip=npart_+npart_in
        call lpt_resize(ip)
        part(ip)=part_in
        part(ip)%x=x(imin)+((time-real(nflow,WP)*inflow_time)*inj_u-part(ip)%x)
        part(ip)%u=part(ip)%u+inj_u
        call lpt_locate(part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k)
        
        ! Close the file and reopen
        call BINARY_FILE_CLOSE(inflow_unit,ierr)
        call BINARY_FILE_OPEN(inflow_unit,trim(part_inflow),"r",ierr)
        call BINARY_FILE_READ(inflow_unit,npart_inflow,1,kind(npart_inflow),ierr)
        call BINARY_FILE_READ(inflow_unit,mysize,1,kind(mysize),ierr)
        call BINARY_FILE_READ(inflow_unit,inflow_time,1,kind(inflow_time),ierr)
        inflow_time=inflow_time/inj_u
        nflow=floor(time/inflow_time)
        
        ! Read first particle
        call BINARY_FILE_READ(inflow_unit,part_in,1,part_kind,ierr)
        npart_inflow_read=1
        t_in=part_in%x/inj_u
        
     end if
     
     ! Read enough particles (this might mess up the last one)
     do while (t_in.lt.mod(time,inflow_time) .and. npart_inflow_read.lt.npart_inflow)
        
        ! Current particle should be added (adjust position, velocity, and location)
        npart_in=npart_in+1
        ip=npart_+npart_in
        call lpt_resize(ip)
        part(ip)=part_in
        part(ip)%x=x(imin)+((time-real(nflow,WP)*inflow_time)*inj_u-part(ip)%x)
        part(ip)%u=part(ip)%u+inj_u
        call lpt_locate(part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k)
        
        ! Read one more particle
        call BINARY_FILE_READ(inflow_unit,part_in,1,part_kind,ierr)
        npart_inflow_read=npart_inflow_read+1
        t_in=part_in%x/inj_u
        
     end do
     
  end if
  
  ! Send particules to everybody - recycling is done here
  call lpt_communication
  
  return
end subroutine lpt_inject_file
