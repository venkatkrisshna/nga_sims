! ======================== !
! Acoustophoresis modeling !
! ======================== !
module lpt_acoustics
  use lpt
  implicit none
  
  ! Pointers to acoustics fields
  real(WP), dimension(:,:,:), allocatable :: AcP
  real(WP), dimension(:,:,:), allocatable :: AcV
  
  ! Acoustic potential
  real(WP), dimension(:,:,:), allocatable :: AcPot
  
  ! Gradient of acoustic potential
  real(WP), dimension(:,:,:), allocatable :: AcAccx
  real(WP), dimension(:,:,:), allocatable :: AcAccy
  real(WP), dimension(:,:,:), allocatable :: AcAccz
  
  ! Acoustics parameters
  real(WP) :: a_fluid,a_particle,rcoeff,acoeff,f1,f2
  
  ! Flag
  logical :: use_acoustics
  
end module lpt_acoustics


! Initialization of the acoustophoresis model
subroutine lpt_acoustics_init
  use lpt_acoustics
  use optdata
  use string
  implicit none
  
  ! Optdata info
  character(len=str_short) :: name
  integer :: iod1,iod2,i
  
  ! Do we use acoustophoresis
  call parser_read('Use acoustics',use_acoustics,.false.)
  if (.not.use_acoustics) return
  
  ! Identify optdata fields
  iod1=0; iod2=0
  do i=1,nod
     read(OD_name(i),*) name
     ! Find acoustic pressure
     if (trim(name).eq.'PM') iod1=i
     ! Find acoustic velocity
     if (trim(name).eq.'UM') iod2=i
  end do
  
  ! Copy acoustic pressure
  if (iod1.eq.0) call die("lpt_acoustics_init: Acoustophoresis model requires PM variable in optdata file.")
  allocate(AcP(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  AcP(imin_:imax_,jmin_:jmax_,kmin_:kmax_)=OD(imin_:imax_,jmin_:jmax_,kmin_:kmax_,iod1)
  call boundary_neumann_alldir(AcP)
  call boundary_update_border(AcP,'+','ym')
  
  ! Copy acoustic velocity
  if (iod2.eq.0) call die("lpt_acoustics_init: Acoustophoresis model requires UM variable in optdata file.")
  allocate(AcV(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  AcV(imin_:imax_,jmin_:jmax_,kmin_:kmax_)=OD(imin_:imax_,jmin_:jmax_,kmin_:kmax_,iod2)
  call boundary_neumann_alldir(AcV)
  call boundary_update_border(AcV,'+','ym')
  
  ! Prepare acoustic potential field
  allocate(AcPot(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  
  ! Prepare acoustic acceleration fields
  allocate(AcAccx(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AcAccy(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AcAccz(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  
  ! Read in sound speeds
  call parser_read('Fluid sound speed',a_fluid)
  call parser_read('Particle sound speed',a_particle)
  
  return
end subroutine lpt_acoustics_init


! Computation of acoustic potential
subroutine lpt_acoustics_potential
  use lpt_acoustics
  implicit none
  
  integer :: i,j,k
  
  ! Check if used
  if (.not.use_acoustics) return
  
  ! Acoustophoresis potential
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Prepare parameters
           rcoeff=rhod/RHO(i,j,k)
           acoeff=a_particle**2/a_fluid**2
           f1=1.0_WP-1.0_WP/(rcoeff*acoeff**2)
           f2=2.0_WP*(rcoeff-1.0_WP)/(2.0_WP*rcoeff+1.0_WP)
           ! Compute potential
           AcPot(i,j,k)=0.25_WP*AcP(i,j,k)**2.0_WP*f1/(RHO(i,j,k)*a_fluid**2)-0.375_WP*RHO(i,j,k)*AcV(i,j,k)**2.0_WP*f2
           
           ! Add a tanh envelope to the potential to clean up boundary conditions
           AcPot(i,j,k)=AcPot(i,j,k)*(0.5_WP+0.5_WP*tanh((xm(i)-1.0e-3_WP)/3.0e-4_WP))
           
        end do
     end do
  end do
  
  ! Coundary conditions
  call boundary_neumann_alldir(AcPot)
  call boundary_update_border(AcPot,'+','ym')
  
  ! Compute its gradient
  call gradient_vector(AcPot,AcAccx,AcAccy,AcAccz)
  
  return
end subroutine lpt_acoustics_potential
