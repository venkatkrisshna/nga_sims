module lpt_mod
  use precision
  implicit none
  
  ! Particle type
  type part_type
     ! Particle ID
     integer(kind=8) :: id
     ! Position (Cartesian)
     real(WP) :: x
     real(WP) :: y
     real(WP) :: z
     ! Velocity (Cartesian)
     real(WP) :: u
     real(WP) :: v
     real(WP) :: w
     ! Angular velocity
     real(WP) :: wx
     real(WP) :: wy
     real(WP) :: wz
     ! Diameter
     real(WP) :: d
     ! Time step size
     real(WP) :: dt
     ! Collision force
     real(WP), dimension(3) :: Acol
     ! Collision torque
     real(WP), dimension(3) :: Tcol
     ! Position (mesh)
     integer :: i
     integer :: j
     integer :: k
     ! Control parameter
     integer :: stop
  end type part_type
  
  ! CHANGE HERE
  integer :: part_kind=160
  
  ! Particle array and sizes
  type(part_type), dimension(:), pointer :: part
  integer :: npart_
  integer :: npart
  integer, dimension(:), allocatable :: npart_proc
  
  ! Particle array and sizes for com
  type(part_type), dimension(:), pointer :: part_gp
  integer :: npart_gp_
  
end module lpt_mod
