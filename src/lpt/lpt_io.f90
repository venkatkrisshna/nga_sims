! ========================================== !
! Particle I/O routines                      !
! Contains both serial and parallel versions !
! ========================================== !
module lpt_io
  use parallel
  use string
  use lpt
  use lpt_com
  implicit none
  
  ! I/O stuff
  integer(kind=MPI_OFFSET_KIND) :: HEADER_OFFSET
  integer :: lpt_last
  !lpt_last = int(lpt_time/lpt_freq)
  
  ! Filename
  character(len=str_medium) :: part_file_init
  character(len=str_medium) :: part_file
  
  ! Serial vs parallel LPT I/O
  logical :: lpt_use_pIO
  
  ! Overwrite flag
  logical :: overwrite
  
end module lpt_io


! ====================== !
! LPT I/O initialization !
! ====================== !
subroutine lpt_io_init
  use lpt_io
  use parser
  implicit none
    
  ! Read filenames
  call parser_read('Part file to read',part_file_init)
  call parser_read('Part file to write',part_file)
  
  ! Read how to do I/O
  call parser_read('Use parallel lpt I/O',lpt_use_pIO,use_pIO)
  
  ! Overwrite flag
  call parser_read('Data overwrite',overwrite,.true.)
  
  return
end subroutine lpt_io_init


! ======================= !
! Test if we need to save !
! the particle data file  !
! ======================= !
subroutine lpt_write(flag)
  use lpt_io
  use time_info
  use data
  implicit none
  logical, intent(in) :: flag
  
  ! Check wether spray is used
  if (.not.use_lpt) return
  
  if (int(time/data_freq).ne.data_last .or. flag) then
     call lpt_write_full3D
  end if
  
  return
end subroutine lpt_write


! =================== !
! Write particle file !
! =================== !
subroutine lpt_write_full3D
  use lpt_io
  implicit none
  
  ! Check wether spray is used
  if (.not.use_lpt) return
  
  ! Call right IO routine
  if (lpt_use_pIO) then
     call lpt_write_full3D_parallel
  else
     call lpt_write_full3D_serial
  end if
  
  return
end subroutine lpt_write_full3D


! ============================== !
! Write particle file - parallel !
! ============================== !
subroutine lpt_write_full3D_parallel
  use lpt_io
  implicit none
  
  integer :: ierr,file,info,i
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer(kind=MPI_OFFSET_KIND) :: offset,npart_MOK
  character(len=str_medium) :: filename,buffer
  logical :: file_is_there
  
  ! Get the name
  filename=trim(mpiiofs)//trim(part_file)
  
  ! Add time info to the file name
  if (.not.overwrite) then
     write(buffer,'(ES12.3)') time
     filename = trim(adjustl(filename))//'_'//trim(adjustl(buffer))
  end if
  
  ! Open the file to write
  inquire(file=filename,exist=file_is_there)
  if (file_is_there .and. irank.eq.iroot) call MPI_file_delete(filename,info,ierr)
  call MPI_file_open(comm,filename,IOR(MPI_MODE_CREATE,MPI_MODE_WRONLY),mpi_info,file,ierr)
  
  ! Write header
  if (irank.eq.iroot) then
     call MPI_FILE_WRITE(file,npart,1,MPI_INTEGER,status,ierr)
     call MPI_FILE_WRITE(file,SIZE_MPI_PARTICLE,1,MPI_INTEGER,status,ierr)
     call MPI_FILE_WRITE(file,dt,1,MPI_REAL_WP,status,ierr)
     call MPI_FILE_WRITE(file,time,1,MPI_REAL_WP,status,ierr)
  end if
  
  ! Compute the offset and write
  npart_MOK=0
  do i=1,irank-1
     npart_MOK=npart_MOK+int(npart_proc(i),MPI_OFFSET_KIND)
  end do
  offset = int(SIZE_MPI_PARTICLE,MPI_OFFSET_KIND)*npart_MOK+HEADER_OFFSET
  if (npart_.gt.0) call MPI_file_write_at(file,offset,part,npart_,MPI_particle,status,ierr)
  
  ! Close the file
  call MPI_file_close(file,ierr)
  
  ! Filename and info on the screen
  call monitor_log("PART_DATA WRITTEN")
  
  return
end subroutine lpt_write_full3D_parallel


! ============================ !
! Write particle file - serial !
! ============================ !
subroutine lpt_write_full3D_serial
  use lpt_io
  implicit none
  
  integer :: i,ierr,file,part_size
  character(len=str_medium) :: filename,dirname,buffer
  
  ! Get the name of the file to write to
  dirname=trim(part_file)
  
  ! Add time info to the file name
  if (.not.overwrite) then
     write(buffer,'(ES12.3)') time
     dirname = trim(adjustl(dirname))//'_'//trim(adjustl(buffer))
  end if
  
  ! Add serial tag to dirname
  dirname = trim(adjustl(dirname))//'_serial'

  ! Create directory
  if (irank.eq.iroot) then
     call CREATE_FOLDER(trim(dirname))
  end if
  call MPI_BARRIER(comm,ierr)

  ! Loop over processors, in chunks of 100
  do i=0,(nproc-1)/100+1
     if (irank/100.eq.i) then
        
        ! Create filename
        filename = trim(adjustl(dirname)) // "/part."
        write(filename(len_trim(filename)+1:len_trim(filename)+6),'(i6.6)') irank
        
        ! Open the file to write
        call BINARY_FILE_OPEN(file,trim(adjustl(filename)),"w",ierr)
        
        ! Write header corresponding to proc
        part_size = 0
        if (npart_.gt.0) part_size = sizeof(part(1))
        call BINARY_FILE_WRITE(file,npart_,1,kind(npart_),ierr)                                      
        call BINARY_FILE_WRITE(file,part_size,1,kind(part_size),ierr)                              
        call BINARY_FILE_WRITE(file,dt,1,kind(dt),ierr)                                            
        call BINARY_FILE_WRITE(file,time,1,kind(time),ierr) 
        
        ! Write local particles
        if (npart_.gt.0) call BINARY_FILE_WRITE(file,part,npart_,part_size,ierr)
        
        ! Close the file 
        call BINARY_FILE_CLOSE(file,ierr)
     end if

     ! Synchronize                                                                             
     call MPI_BARRIER(comm,ierr)
  end do

  ! Log
  if (irank.eq.iroot) call monitor_log("3D PART WRITTEN (SERIAL)")
  
  return
end subroutine lpt_write_full3D_serial


! ================== !
! Read particle file !
! ================== !
subroutine lpt_read
  use lpt_io
  implicit none
  
  ! Call right IO routine
  if (lpt_use_pIO) then
     call lpt_read_parallel
  else
     call lpt_read_serial
  end if
  
  return
end subroutine lpt_read


! ============================= !
! Read particle file - parallel !
! ============================= !
subroutine lpt_read_parallel
  use lpt_io
  implicit none
  
  integer, dimension(:,:), allocatable :: ppp
  integer :: ierr,file,i,j,npart_add,ibuf
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer(kind=MPI_OFFSET_KIND) :: offset
  real(WP) :: dt_read,time_read
  integer :: part_size
  integer :: nchunk,count
  character(len=str_medium) :: filename
  logical :: file_is_there
  integer, parameter :: chunk_size=1000
  
  ! Define the HEADER size
  HEADER_OFFSET = kind(npart_add)+kind(part_size)+kind(dt)+kind(time)
  
  ! Open the file
  inquire(file=trim(part_file_init),exist=file_is_there)
  if (.not.file_is_there) return
  filename= trim(mpiiofs) // trim(part_file_init)
  call MPI_file_open(comm,filename,MPI_MODE_RDONLY,mpi_info,file,ierr)
  if (ierr .ne. 0) return
  
  ! Get the number of particles from the size of the file
  !call MPI_file_get_size(file,SIZE_FILE,ierr)
  !npart_add=SIZE_FILE/SIZE_MPI_PARTICLE
  
  ! Read header
  call MPI_FILE_READ_ALL(file,npart_add,1,MPI_INTEGER,status,ierr)
  call MPI_FILE_READ_ALL(file,part_size,1,MPI_INTEGER,status,ierr)
  call MPI_FILE_READ_ALL(file,dt_read,1,MPI_REAL_WP,status,ierr)
  call MPI_FILE_READ_ALL(file,time_read,1,MPI_REAL_WP,status,ierr)
  if (part_size.ne.SIZE_MPI_PARTICLE) call die("Particle type unreadable")
  
  ! Share this among the processors
  nchunk=int(npart_add/(nproc*chunk_size))+1
  allocate(ppp(nproc,nchunk))
  ppp(:,:) = int(npart_add/(nproc*nchunk))
  count = 0
  out:do j=1,nchunk
     do i=1,nproc
        count = count + 1
        if (count.gt.mod(npart_add,nproc*nchunk)) exit out
        ppp(i,j) = ppp(i,j) + 1
     end do
  end do out
  
  ! Read by chunk
  do j=1,nchunk
     
     ! Find offset
     ibuf = sum(ppp(1:irank-1,:))+sum(ppp(irank,1:j-1))
     offset = int(SIZE_MPI_PARTICLE,MPI_OFFSET_KIND)*int(ibuf,MPI_OFFSET_KIND)+HEADER_OFFSET
     
     ! Resize particle array
     call lpt_resize(npart_+ppp(irank,j))
     
     ! Read this file
     call MPI_file_read_at(file,offset,part(npart_+1:npart_+ppp(irank,j)),ppp(irank,j),MPI_particle,status,ierr)
  
     ! Most general case: relocate every droplet
     do i=npart_+1,npart_+ppp(irank,j)
        call lpt_locate(part(i)%x,part(i)%y,part(i)%z,part(i)%i,part(i)%j,part(i)%k)
     end do
     
     ! Exchange all that
     call lpt_communication
     
     ! Monitoring
     if (irank.eq.iroot) print*,'Reading',100.0_WP*real(j,WP)/real(nchunk,WP),'% complete'
     
  end do
  
  ! Close the file
  call MPI_file_close(file,ierr)
  
  return
end subroutine lpt_read_parallel


! =========================== !
! Read particle file - serial !
! =========================== !
subroutine lpt_read_serial
  use lpt_io
  implicit none
  
  integer :: ierr,file,i,j,part_size
  real(WP) :: dt_read,time_read
  character(len=str_medium) :: filename,buffer
  logical :: file_is_there

  ! Check if there is a file to read                  
  filename = trim(adjustl(part_file_init)) // '/part.000001'
  inquire(file=filename,exist=file_is_there)
  if (.not.file_is_there) then
     call lpt_read_parallel
     return
  end if

  ! Each processor reads its own particle file, in chunks of 100
  do i=0,(nproc-1)/100+1
     if (irank/100.eq.i) then

        !print *, 'processor ',irank,' reading'

        ! Set filename 
        write(buffer,'(i6.6)') irank
        filename = trim(adjustl(part_file_init)) // '/part.' // trim(adjustl(buffer))

        ! Open file        
        call BINARY_FILE_OPEN(file,trim(filename),"r",ierr)
        
        ! Read local number of particles
        call binary_file_read(file,npart_,1,kind(npart_),ierr)
        call binary_file_read(file,part_size,1,kind(part_size),ierr)                                      
        call binary_file_read(file,dt_read,1,kind(dt_read),ierr)                                          
        call binary_file_read(file,time_read,1,kind(time_read),ierr)   
        
        ! Resize local particle array
        call lpt_resize(npart_)
        
        ! Read particles
        if (npart_>0) call binary_file_read(file,part(1:npart_),npart_,part_size,ierr)
        
        ! Re-locate
        do j=1,npart_
           call lpt_locate(part(j)%x,part(j)%y,part(j)%z,part(j)%i,part(j)%j,part(j)%k)
        end do

        ! Close the file
        call binary_file_close(file,ierr)
     end if
     
     ! Communicate (should not be needed)
     call lpt_communication
     
     ! Synchronize 
     call MPI_BARRIER(comm,ierr)
  end do

  return
end subroutine lpt_read_serial
