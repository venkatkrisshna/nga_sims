! ====================================== !
! Tools for Lagrangian particle tracking !
! ====================================== !
module lpt_tools
  use precision
  use geometry
  use partition
  implicit none
  
  ! Volume fraction clipping
  real(WP), parameter :: epsf_clip=0.2_WP
  
  ! LPT diffusion coefficient
  real(WP), dimension(:,:,:), allocatable :: lpt_diff3D
  real(WP) :: lpt_diff,Np
  logical :: use_varfilter
  
end module lpt_tools


! ===================================== !
! Subroutine to compute volume fraction !
! from a part array up-to-date          !
! Assume the indices are fully updated  !
! ===================================== !
subroutine lpt_get_epsf
  use lpt_tools
  use lpt
  use math
  use data
  implicit none
  
  integer :: n,i,j,k
  real(WP) :: volp,sig,C
  
  ! Extrapolate particle data to mesh
  epsf=0.0_WP
  
  ! Loop over all particles
  part_loop: do n=1,npart_
     
     ! Cycle if particle is inactive
     if (part(n)%id.le.0) cycle part_loop
     
     ! Transfer particle volume
     volp=Pi/6.0_WP*part(n)%d**3
     !call lpt_mollify_sc(epsf,part(n)%x,part(n)%y,part(n)%z,part(n)%i,part(n)%j,part(n)%k,volp)
     call lpt_extrapolate(epsf,part(n)%x,part(n)%y,part(n)%z,part(n)%i,part(n)%j,part(n)%k,volp)
     
  end do part_loop
  
  ! Divide by local cell volume
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           if (vol(i,j,k).gt.epsilon(1.0_WP)) then
              epsf(i,j,k)=epsf(i,j,k)/vol(i,j,k)
           else
              epsf(i,j,k)=0.0_WP
           end if
        end do
     end do
  end do
  
  ! Communicate
  call summation_border(epsf)
  
  ! Filter epsf with constant diffusion coefficient
  call lpt_smooth(epsf,'+')
  
  ! Apply Dirichlet in x
  call boundary_dirichlet(epsf,'-xm')
  call boundary_dirichlet(epsf,'+xm')
  
  ! Form gas fraction
  epsf=1.0_WP-epsf
  
  ! Clip epsf
  where (epsf.lt.epsf_clip)
     epsf=epsf_clip
  end where

  ! Compute variable diffusion coefficient
  if (use_varfilter) then
     C=1.0_WP/(2.0_WP*sqrt(2.0_WP*log(2.0_WP)))
     do k=kmino_,kmaxo_
        do j=jmino_,jmaxo_
           do i=imino_,imaxo_
              ! Compute variable smoothing length
              sig=(Np*dmean**3/(1.0_WP-epsf(i,j,k)+epsilon(1.0_WP)))**(1.0_WP/3.0_WP)
              sig=sig*C
              lpt_diff3D(i,j,k)=0.5_WP*sig**2
           end do
        end do
     end do
  end if

  return
end subroutine lpt_get_epsf


! ============================== !
! Initialize Laplacian smoothing !
! ============================== !
subroutine lpt_smooth_init
  use lpt_tools
  use lpt
  implicit none
  
  real(WP) :: delta,sig

 ! Read in filter size
  call parser_read('LPT filter width',delta,7.0_WP*dmean)

  ! Check if variable filter is used
  call parser_read('LPT variable filter',use_varfilter,.false.)
  if (use_varfilter) then
     ! Filter sample size
     call parser_read('Filter sample size',Np,10.0_WP)

     ! Allocate 3D diffusion coefficient
     allocate(lpt_diff3D(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  end if
  
  ! Determine constant diffusion coefficient
  !delta=max(delta,0.5_WP*min_meshsize)
  sig=delta/(2.0_WP*sqrt(2.0_WP*log(2.0_WP)))
  lpt_diff=0.5_WP*sig**2
  
  return
end subroutine lpt_smooth_init


! =================== !
! Laplacian Smoothing !
! =================== !
subroutine lpt_smooth(A,axis)
  use lpt_tools
  use memory
  use implicit
  implicit none
  
  integer :: i,j,k
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: A
  character(len=*), intent(in) :: axis

  ! Inverse in X-direction
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           Ax(j,k,i,-1) = - div_u(i,j,k,0) * lpt_diff * grad_x(i,j,-1)
           Ax(j,k,i, 0) = 1.0_WP - (div_u(i,j,k,0) * lpt_diff * grad_x(i,j,0) &
                                  + div_u(i,j,k,1) * lpt_diff * grad_x(i+1,j,-1))
           Ax(j,k,i,+1) = - div_u(i,j,k,1) * lpt_diff * grad_x(i+1,j, 0)
           Rx(j,k,i) = A(i,j,k)
        end do
     end do
  end do
  call linear_solver_x(3)
  
  ! Inverse in Y-direction
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           Ay(i,k,j,-1) = - div_v(i,j,k,0) * lpt_diff * grad_y(i,j,-1)
           Ay(i,k,j, 0) = 1.0_WP - (div_v(i,j,k,0)* lpt_diff * grad_y(i,j,0) &
                                  + div_v(i,j,k,1)* lpt_diff * grad_y(i,j+1,-1))
           Ay(i,k,j,+1) = - div_v(i,j,k,1) * lpt_diff * grad_y(i,j+1,0)
           Ry(i,k,j) = Rx(j,k,i)
        end do
     end do
  end do
  call linear_solver_y(3)
  
  ! Inverse in Z-direction
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           Az(i,j,k,-1) = - div_w(i,j,k,0) * lpt_diff * grad_z(i,j,-1)
           Az(i,j,k, 0) = 1.0_WP - (div_w(i,j,k,0) * lpt_diff * grad_z(i,j,0) &
                                  + div_w(i,j,k,1) * lpt_diff * grad_z(i,j,-1))
           Az(i,j,k,+1) = - div_w(i,j,k,1) * lpt_diff * grad_z(i,j,0)
           Rz(i,j,k) = Ry(i,k,j)
        end do
     end do
  end do
  call linear_solver_z(3)
  
  ! Update A
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           A(i,j,k)=Rz(i,j,k)
        end do
     end do
  end do
  
  ! Communicate
  call boundary_update_border(A,axis,'ym')
  
  return
end subroutine lpt_smooth


! ======================================================= !
! Laplacian Smoothing with Variable Diffusion Coefficient !
! ======================================================= !
subroutine lpt_smooth_var(A,axis)
  use lpt_tools
  use memory
  use implicit
  implicit none
  
  integer :: i,j,k
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: A
  character(len=*), intent(in) :: axis

  ! Inverse in X-direction
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           Ax(j,k,i,-1) = - div_u(i,j,k,0) * sum(interp_sc_x(i,j,:)*lpt_diff3D(i-st2:i+st1,j,k)) * grad_x(i,j,-1)
           Ax(j,k,i, 0) = 1.0_WP - (div_u(i,j,k,0) * sum(interp_sc_x(i,j,:)*lpt_diff3D(i-st2:i+st1,j,k)) * grad_x(i,j,0) &
                                  + div_u(i,j,k,1) * sum(interp_sc_x(i+1,j,:)*lpt_diff3D(i-st2+1:i+st1+1,j,k)) * grad_x(i+1,j,-1))
           Ax(j,k,i,+1) = - div_u(i,j,k,1) * sum(interp_sc_x(i+1,j,:)*lpt_diff3D(i-st2+1:i+st1+1,j,k)) * grad_x(i+1,j, 0)
           Rx(j,k,i) = A(i,j,k)
        end do
     end do
  end do
  call linear_solver_x(3)
  
  ! Inverse in Y-direction
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           Ay(i,k,j,-1) = - div_v(i,j,k,0) * sum(interp_sc_y(i,j,:)*lpt_diff3D(i,j-st2:j+st1,k)) * grad_y(i,j,-1)
           Ay(i,k,j, 0) = 1.0_WP - (div_v(i,j,k,0)* sum(interp_sc_y(i,j,:)*lpt_diff3D(i,j-st2:j+st1,k)) * grad_y(i,j,0) &
                                  + div_v(i,j,k,1)* sum(interp_sc_y(i,j+1,:)*lpt_diff3D(i,j-st2+1:j+st1+1,k)) * grad_y(i,j+1,-1))
           Ay(i,k,j,+1) = - div_v(i,j,k,1) * sum(interp_sc_y(i,j+1,:)*lpt_diff3D(i,j-st2+1:j+st1+1,k)) * grad_y(i,j+1,0)
           Ry(i,k,j) = Rx(j,k,i)
        end do
     end do
  end do
  call linear_solver_y(3)
  
  ! Inverse in Z-direction
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           Az(i,j,k,-1) = - div_w(i,j,k,0) * sum(interp_sc_z(i,j,:)*lpt_diff3D(i,j,k-st2:k+st1)) * grad_z(i,j,-1)
           Az(i,j,k, 0) = 1.0_WP - (div_w(i,j,k,0) *  sum(interp_sc_z(i,j,:)*lpt_diff3D(i,j,k-st2:k+st1)) * grad_z(i,j,0) &
                                  + div_w(i,j,k,1) *  sum(interp_sc_z(i,j,:)*lpt_diff3D(i,j,k-st2+1:k+st1+1)) * grad_z(i,j,-1))
           Az(i,j,k,+1) = - div_w(i,j,k,1) *  sum(interp_sc_z(i,j,:)*lpt_diff3D(i,j,k-st2+1:k+st1+1)) * grad_z(i,j,0)
           Rz(i,j,k) = Ry(i,k,j)
        end do
     end do
  end do
  call linear_solver_z(3)
  
  ! Update A
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           A(i,j,k)=Rz(i,j,k)
        end do
     end do
  end do
  
  ! Communicate
  call boundary_update_border(A,axis,'ym')
  
  return
end subroutine lpt_smooth_var


! ================================ !
! Generate data for Eulerian stats !
! ================================ !
subroutine lpt_stat_prepare
  use lpt_tools
  use lpt
  use data
  use memory
  implicit none
  
  integer :: ip,i,j,k
  real(WP) :: volp

  ! Zero temporary arrays for filtering particle data to mesh
  tmp6 =0.0_WP
  tmp7 =0.0_WP
  tmp8 =0.0_WP
  tmp9 =0.0_WP
  tmp10=0.0_WP
  tmp11=0.0_WP
  tmp12=0.0_WP
  tmp13=0.0_WP
  tmp14=0.0_WP
  tmp15=0.0_WP
  
  ! Transfer Lagrangian data to the mesh
  do ip=1,npart_
     ! Mollify Lagrangian data to mesh     
     volp=Pi/6.0_WP*part(ip)%d**3
     !call lpt_mollify_sc(tmp6 ,part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k,volp)
     !call lpt_mollify_sc(tmp7 ,part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k,volp*part(ip)%u)
     !call lpt_mollify_sc(tmp8 ,part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k,volp*part(ip)%v)
     !call lpt_mollify_sc(tmp9 ,part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k,volp*part(ip)%w)
     !call lpt_mollify_sc(tmp10,part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k,volp*part(ip)%u*part(ip)%u)
     !call lpt_mollify_sc(tmp11,part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k,volp*part(ip)%v*part(ip)%v)
     !call lpt_mollify_sc(tmp12,part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k,volp*part(ip)%w*part(ip)%w)
     !call lpt_mollify_sc(tmp13,part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k,volp*part(ip)%u*part(ip)%v)
     !call lpt_mollify_sc(tmp14,part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k,volp*part(ip)%v*part(ip)%w)
     !call lpt_mollify_sc(tmp15,part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k,volp*part(ip)%w*part(ip)%u)
     call lpt_extrapolate(tmp6 ,part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k,volp)
     call lpt_extrapolate(tmp7 ,part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k,volp*part(ip)%u)
     call lpt_extrapolate(tmp8 ,part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k,volp*part(ip)%v)
     call lpt_extrapolate(tmp9 ,part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k,volp*part(ip)%w)
     call lpt_extrapolate(tmp10,part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k,volp*part(ip)%u*part(ip)%u)
     call lpt_extrapolate(tmp11,part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k,volp*part(ip)%v*part(ip)%v)
     call lpt_extrapolate(tmp12,part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k,volp*part(ip)%w*part(ip)%w)
     call lpt_extrapolate(tmp13,part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k,volp*part(ip)%u*part(ip)%v)
     call lpt_extrapolate(tmp14,part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k,volp*part(ip)%v*part(ip)%w)
     call lpt_extrapolate(tmp15,part(ip)%x,part(ip)%y,part(ip)%z,part(ip)%i,part(ip)%j,part(ip)%k,volp*part(ip)%w*part(ip)%u)
  end do
  
  ! Divide by volume
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           if (vol(i,j,k).gt.0.0_WP) then
              tmp6 (i,j,k)=tmp6 (i,j,k)/vol(i,j,k)
              tmp7 (i,j,k)=tmp7 (i,j,k)/vol(i,j,k)
              tmp8 (i,j,k)=tmp8 (i,j,k)/vol(i,j,k)
              tmp9 (i,j,k)=tmp9 (i,j,k)/vol(i,j,k)
              tmp10(i,j,k)=tmp10(i,j,k)/vol(i,j,k)
              tmp11(i,j,k)=tmp11(i,j,k)/vol(i,j,k)
              tmp12(i,j,k)=tmp12(i,j,k)/vol(i,j,k)
              tmp13(i,j,k)=tmp13(i,j,k)/vol(i,j,k)
              tmp14(i,j,k)=tmp14(i,j,k)/vol(i,j,k)
              tmp15(i,j,k)=tmp15(i,j,k)/vol(i,j,k)
           else
              tmp6 (i,j,k)=0.0_WP
              tmp7 (i,j,k)=0.0_WP
              tmp8 (i,j,k)=0.0_WP
              tmp9 (i,j,k)=0.0_WP
              tmp10(i,j,k)=0.0_WP
              tmp11(i,j,k)=0.0_WP
              tmp12(i,j,k)=0.0_WP
              tmp13(i,j,k)=0.0_WP
              tmp14(i,j,k)=0.0_WP
              tmp15(i,j,k)=0.0_WP
           end if
        end do
     end do
  end do
  
  ! Add up source at boundaries
  call summation_border(tmp6 )
  call summation_border(tmp7 )
  call summation_border(tmp8 )
  call summation_border(tmp9 )
  call summation_border(tmp10)
  call summation_border(tmp11)
  call summation_border(tmp12)
  call summation_border(tmp13)
  call summation_border(tmp14)
  call summation_border(tmp15)
  
  ! Filter statistics - fixed filter width
  !call lpt_smooth(tmp6 ,'+')
  !call lpt_smooth(tmp7 ,'+')
  !call lpt_smooth(tmp8 ,'+')
  !call lpt_smooth(tmp9 ,'+')
  !call lpt_smooth(tmp10,'+')
  !call lpt_smooth(tmp11,'+')
  !call lpt_smooth(tmp12,'+')
  !call lpt_smooth(tmp13,'+')
  !call lpt_smooth(tmp14,'+')
  !call lpt_smooth(tmp15,'+')
  
  ! Apply boundary conditions
  !call boundary_dirichlet(tmp6 ,'+xm'); call boundary_dirichlet(tmp6 ,'-xm')
  !call boundary_dirichlet(tmp7 ,'+xm'); call boundary_dirichlet(tmp7 ,'-xm')
  !call boundary_dirichlet(tmp8 ,'+xm'); call boundary_dirichlet(tmp8 ,'-xm')
  !call boundary_dirichlet(tmp9 ,'+xm'); call boundary_dirichlet(tmp9 ,'-xm')
  !call boundary_dirichlet(tmp10,'+xm'); call boundary_dirichlet(tmp10,'-xm')
  !call boundary_dirichlet(tmp11,'+xm'); call boundary_dirichlet(tmp11,'-xm')
  !call boundary_dirichlet(tmp12,'+xm'); call boundary_dirichlet(tmp12,'-xm')
  !call boundary_dirichlet(tmp13,'+xm'); call boundary_dirichlet(tmp13,'-xm')
  !call boundary_dirichlet(tmp14,'+xm'); call boundary_dirichlet(tmp14,'-xm')
  !call boundary_dirichlet(tmp15,'+xm'); call boundary_dirichlet(tmp15,'-xm')
  
  ! Form phase average
  where (tmp6.gt.0.0_WP)
     tmp7 =tmp7 /tmp6 ! <up>
     tmp8 =tmp8 /tmp6 ! <vp>
     tmp9 =tmp9 /tmp6 ! <wp>
     tmp10=tmp10/tmp6 ! <upup>
     tmp11=tmp11/tmp6 ! <vpvp>
     tmp12=tmp12/tmp6 ! <wpwp>
     tmp13=tmp13/tmp6 ! <upvp>
     tmp14=tmp14/tmp6 ! <vpwp>
     tmp15=tmp15/tmp6 ! <wpup>
  end where
  
  return
end subroutine lpt_stat_prepare
