! ========================================= !
! Memory routines for spray tracking        !
! Provides a smarter handling of the memory !
! - limits the number of de/allocations     !
! - handles fully dynamic size              !
! ========================================= !
module lpt_memory
  use geometry
  use partition
  use parallel
  use lpt
  implicit none
  !
end module lpt_memory


! ===================================== !
! Extremely basic sorting routine       !
! Should be replaced by heap/quick sort !
! ===================================== !
subroutine lpt_recycle
  use lpt_memory
  implicit none
  
  integer :: i,new_size
  
  ! Compact real particles at the beginning of the array
  new_size = 0
  if (associated(part)) then
     do i=1,size(part,dim=1)
        if (part(i)%stop.eq.0) then
           new_size = new_size + 1
           if (i .ne. new_size) then
              part(new_size)=part(i)
              part(i)%stop = 1
           end if
        end if
     end do
  end if
  
  ! Resize array to new_size
  call lpt_resize(new_size)

  ! Update sizes
  call lpt_update_size(new_size)
  
 return
end subroutine lpt_recycle


! ========================================= !
! Update the number of particles everywhere !
! ========================================= !
subroutine lpt_update_size(n)
  use lpt_memory
  implicit none
  
  integer, intent(in) :: n
  integer :: ierr
  
  ! Update sizes
  npart_ = n
  call MPI_allgather(npart_,1,MPI_INTEGER,npart_proc,1,MPI_INTEGER,comm,ierr)
  npart = sum(npart_proc)
  
  return
end subroutine lpt_update_size


! =========================================== !
! Resize the storage array to an optimal size !
! in the sense of size and frequency of       !
! allocation/deallocation                     !
! =========================================== !
subroutine lpt_resize(n)
  use lpt_memory
  implicit none
  
  integer, intent(in) :: n
  type(part_type), dimension(:), pointer :: part_temp
  integer :: n_now,n_new,i
  real(WP), parameter :: coeff_up   = 1.3_WP
  real(WP), parameter :: coeff_down = 0.7_WP
  real(WP) :: up,down
  
  ! Resize part array to size n
  if (.NOT.associated(part)) then
     ! part is of size 0
     if (n.eq.0) then
        ! Nothing to do, that's what we want
     else
        ! Allocate directly of size n
        allocate(part(n))
        part(1:n)%stop=1
     end if
  else if (n.eq.0) then
     ! part is associated, be we want to empty it
     deallocate(part)
     nullify(part)
  else
     ! Update non zero size to another non zero size
     n_now = size(part,dim=1)
     up  =real(n_now,WP)*coeff_up
     down=real(n_now,WP)*coeff_down
     if (n.gt.n_now) then
        ! Increase from n_now to n_new
        n_new = max(n,int(up))
        allocate(part_temp(n_new))
        do i=1,n_now
           part_temp(i) = part(i)
        end do
        deallocate(part)
        nullify(part)
        part => part_temp
        part(n_now+1:n_new)%stop=1
     else if (n.lt.int(down)) then
        ! Decrease from n_now to n_new
        allocate(part_temp(n))
        do i=1,n
           part_temp(i) = part(i)
        end do
        deallocate(part)
        nullify(part)
        part => part_temp
     end if
  end if
  
  return
end subroutine lpt_resize


! ========================================= !
! Resizing routine for ghost particle array !
! ========================================= !
subroutine lpt_resize_gp(n)
  use lpt_memory
  implicit none
  
  integer, intent(in) :: n
  type(part_type), dimension(:), pointer :: part_temp
  integer :: n_now,n_new,i
  real(WP), parameter :: coeff_up   = 1.3_WP
  real(WP), parameter :: coeff_down = 0.7_WP
  real(WP) :: up,down
  
  ! Resize part array to size n
  if (.NOT.associated(part_gp)) then
     ! part is of size 0
     if (n.eq.0) then
        ! Nothing to do, that's what we want
     else
        ! Allocate directly of size n
        allocate(part_gp(n))
        part_gp(1:n)%stop=1
     end if
  else if (n.eq.0) then
     ! part is associated, be we want to empty it
     deallocate(part_gp)
     nullify(part_gp)
  else
     ! Update non zero size to another non zero size
     n_now = size(part_gp,dim=1)
     up  =real(n_now,WP)*coeff_up
     down=real(n_now,WP)*coeff_down
     if (n.gt.n_now) then
        ! Increase from n_now to n_new
        n_new = max(n,int(up))
        allocate(part_temp(n_new))
        do i=1,n_now
           part_temp(i) = part_gp(i)
        end do
        deallocate(part_gp)
        nullify(part_gp)
        part_gp => part_temp
        part_gp(n_now+1:n_new)%stop=1
     else if (n.lt.int(down)) then
        ! Decrease from n_now to n_new
        allocate(part_temp(n))
        do i=1,n
           part_temp(i) = part_gp(i)
        end do
        deallocate(part_gp)
        nullify(part_gp)
        part_gp => part_temp
     end if
  end if
  
  return
end subroutine lpt_resize_gp
! ------------------------------------------ !

