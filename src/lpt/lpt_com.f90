! ================================== !
! Various LPT communication routines !
! ================================== !
module lpt_com
  use lpt
  use lpt_mod
  use parallel
  implicit none
  
  ! MPI structure
  integer :: MPI_PARTICLE
  integer :: SIZE_MPI_PARTICLE
  
end module lpt_com


! ================================ !
! Particle interprocessor exchange !
! ================================ !
subroutine lpt_communication
  use lpt_com
  implicit none
  
  integer, dimension(nproc) :: who_send,who_recv,counter
  integer :: i,nb_recv,nb_send,npart_old
  integer :: rank_send,rank_recv,part_rank,ierr,status(MPI_STATUS_SIZE)
  type(part_type), dimension(:,:), allocatable :: buf_send
  
  ! Recycle
  call lpt_recycle
  
  ! Prepare information about who sends what to whom
  who_send=0
  do i=1,npart_
     call lpt_proc(part(i)%i,part(i)%j,part(i)%k,part_rank)
     who_send(part_rank)=who_send(part_rank)+1
  end do
  ! Remove the diagonal
  who_send(irank)=0

  ! Prepare information about who receives what from whom
  do rank_recv=1,nproc
     call MPI_gather(who_send(rank_recv),1,MPI_INTEGER,who_recv,1, &
          MPI_INTEGER,rank_recv-1,comm,ierr)
  end do

  ! Prepare the buffers
  nb_send=maxval(who_send)
  nb_recv=sum(who_recv)
  
  ! Allocate buffers to send particles
  allocate(buf_send(nproc,nb_send))
  
  ! Find and pack the particles to be sent
  ! Prepare the counter
  counter(:)=0
  do i=1,npart_
     ! Get the cpu
     call lpt_proc(part(i)%i,part(i)%j,part(i)%k,part_rank)
     if (part_rank .NE. irank) then
        ! Prepare for sending
        counter(part_rank)=counter(part_rank)+1
        buf_send(part_rank,counter(part_rank))=part(i)
        ! Need to remove the particle
        part(i)%stop = 1
     end if
  end do
  
  ! Everybody resizes
  npart_old = npart_
  call lpt_resize(npart_+nb_recv)
  
  ! We just loop through the CPUs, pack particles in buf_send, send, unpack
  do rank_send=1,nproc
     if (irank.eq.rank_send) then
        ! I'm the sender of particles
        do rank_recv=1,nproc
           if(who_send(rank_recv).gt.0) then
              call MPI_send(buf_send(rank_recv,:),who_send(rank_recv), &
                   MPI_PARTICLE,rank_recv-1,0,comm,ierr)
           end if
        end do
     else
        ! I'm not the sender, I receive
        if (who_recv(rank_send).gt.0) then
           call MPI_recv(part(npart_old+1:npart_old+who_recv(rank_send)),who_recv(rank_send), &
                MPI_PARTICLE,rank_send-1,0,comm,status,ierr)
           npart_old = npart_old+who_recv(rank_send)
        end if
     end if
  end do
  
  ! Done, deallocate
  deallocate(buf_send)
  
  ! Recycle
  call lpt_recycle
  
  return
end subroutine lpt_communication


! ============================== !
! MPI Particle datatype creation !
! Old MPI-1.2 standard - doesn't !
! account for 64 bits machines   !
! ============================== !
subroutine lpt_mpi_prepare
  use lpt_com
  implicit none
  
  integer, dimension(23) :: types,lengths,displacement
  integer :: base,ierr
  
  ! Create the MPI structure to send particles
  types( 1: 1) = MPI_INTEGER8
  types( 2:18) = MPI_REAL_WP
  types(19:22) = MPI_INTEGER
  types(23)    = MPI_UB
  lengths(:)   = 1
  
  ! Count the displacement for this structure
  !call MPI_address(part(1),         base,             ierr)
  ! Particle ID
  !call MPI_address(part(1)%id,      displacement(1),  ierr)
  ! Position - cartesian
  !call MPI_address(part(1)%x,       displacement(2),  ierr)
  !call MPI_address(part(1)%y,       displacement(3),  ierr)
  !call MPI_address(part(1)%z,       displacement(4),  ierr)
  ! Velocity - cartesian
  !call MPI_address(part(1)%u,       displacement(5),  ierr)
  !call MPI_address(part(1)%v,       displacement(6),  ierr)
  !call MPI_address(part(1)%w,       displacement(7),  ierr)
  ! Angular velocity
  !call MPI_address(part(1)%wx,      displacement(8),  ierr)
  !call MPI_address(part(1)%wy,      displacement(9),  ierr)
  !call MPI_address(part(1)%wz,      displacement(10), ierr)
  ! Diameter
  !call MPI_address(part(1)%d,       displacement(11), ierr)
  ! Time step size
  !call MPI_address(part(1)%dt,      displacement(12), ierr)
  ! Collision force
  !call MPI_address(part(1)%Acol(1), displacement(13), ierr)
  !call MPI_address(part(1)%Acol(2), displacement(14), ierr)
  !call MPI_address(part(1)%Acol(3), displacement(15), ierr)
  ! Collision torque
  !call MPI_address(part(1)%Tcol(1), displacement(16), ierr)
  !call MPI_address(part(1)%Tcol(2), displacement(17), ierr)
  !call MPI_address(part(1)%Tcol(3), displacement(18), ierr)
  ! Position - indices
  !call MPI_address(part(1)%i,       displacement(19), ierr)
  !call MPI_address(part(1)%j,       displacement(20), ierr)
  !call MPI_address(part(1)%k,       displacement(21), ierr)
  ! Alive/dead flag
  !call MPI_address(part(1)%stop,    displacement(22), ierr)
  !call MPI_address(part(2),         displacement(23), ierr)
  !displacement=displacement-base
  
  !if (irank.eq.iroot) write(*,*) "displacement=",displacement
  ! Hard-code displacement here
  displacement( 1)=  0
  displacement( 2)=  8
  displacement( 3)= 16
  displacement( 4)= 24
  displacement( 5)= 32
  displacement( 6)= 40
  displacement( 7)= 48
  displacement( 8)= 56
  displacement( 9)= 64
  displacement(10)= 72
  displacement(11)= 80
  displacement(12)= 88
  displacement(13)= 96
  displacement(14)=104
  displacement(15)=112
  displacement(16)=120
  displacement(17)=128
  displacement(18)=136
  displacement(19)=144
  displacement(20)=148
  displacement(21)=152
  displacement(22)=156
  displacement(23)=160
  
  ! Finalize by creating and commiting the new type
  call mpi_type_create_struct(23,lengths,displacement,types,MPI_PARTICLE,ierr)
  call MPI_Type_commit(MPI_PARTICLE,ierr)
  
  ! If problem, say it
  if (ierr.ne.0) call die("Problem with MPI_PARTICLE")
  
  ! Get the size of this type
  call MPI_type_size(MPI_PARTICLE,SIZE_MPI_PARTICLE,ierr)
  
  return
end subroutine lpt_mpi_prepare


!!$! ============================== !
!!$! MPI Particle datatype creation !
!!$! New MPI-2 standard - accounts  !
!!$! for 64 bits machines           !
!!$! ============================== !
!!$subroutine lpt_mpi_prepare2
!!$  use lpt_com
!!$  implicit none
!!$  
!!$  integer(KIND=MPI_ADDRESS_KIND), dimension(23) :: displacement
!!$  integer, dimension(23) :: types,lengths
!!$  integer(KIND=MPI_ADDRESS_KIND) :: base
!!$  integer :: ierr
!!$  
!!$  ! Create the MPI structure to send particles
!!$  types( 1: 1) = MPI_INTEGER8
!!$  types( 2:18) = MPI_REAL_WP
!!$  types(19:22) = MPI_INTEGER
!!$  types(23)    = MPI_UB
!!$  lengths(:)   = 1
!!$  
!!$  ! Count the displacement for this structure
!!$  call MPI_get_address(part(1),         base,             ierr)
!!$  ! Particle ID
!!$  call MPI_get_address(part(1)%id,      displacement(1),  ierr)
!!$  ! Position - cartesian
!!$  call MPI_get_address(part(1)%x,       displacement(2),  ierr)
!!$  call MPI_get_address(part(1)%y,       displacement(3),  ierr)
!!$  call MPI_get_address(part(1)%z,       displacement(4),  ierr)
!!$  ! Velocity - cartesian
!!$  call MPI_get_address(part(1)%u,       displacement(5),  ierr)
!!$  call MPI_get_address(part(1)%v,       displacement(6),  ierr)
!!$  call MPI_get_address(part(1)%w,       displacement(7),  ierr)
!!$  ! Angular velocity
!!$  call MPI_get_address(part(1)%wx,      displacement(8),  ierr)
!!$  call MPI_get_address(part(1)%wy,      displacement(9),  ierr)
!!$  call MPI_get_address(part(1)%wz,      displacement(10), ierr)
!!$  ! Diameter
!!$  call MPI_get_address(part(1)%d,       displacement(11), ierr)
!!$  ! Time step size
!!$  call MPI_get_address(part(1)%dt,      displacement(12), ierr)
!!$  ! Collision force
!!$  call MPI_get_address(part(1)%Acol(1), displacement(13), ierr)
!!$  call MPI_get_address(part(1)%Acol(2), displacement(14), ierr)
!!$  call MPI_get_address(part(1)%Acol(3), displacement(15), ierr)
!!$  ! Collision torque
!!$  call MPI_get_address(part(1)%Tcol(1), displacement(16), ierr)
!!$  call MPI_get_address(part(1)%Tcol(2), displacement(17), ierr)
!!$  call MPI_get_address(part(1)%Tcol(3), displacement(18), ierr)
!!$  ! Position - indices
!!$  call MPI_get_address(part(1)%i,       displacement(19), ierr)
!!$  call MPI_get_address(part(1)%j,       displacement(20), ierr)
!!$  call MPI_get_address(part(1)%k,       displacement(21), ierr)
!!$  ! Alive/dead flag
!!$  call MPI_get_address(part(1)%stop,    displacement(22), ierr)
!!$  call MPI_get_address(part(2),         displacement(23), ierr)
!!$  displacement=displacement-base
!!$  
!!$  ! Finalize by creating and commiting the new type
!!$  call MPI_Type_create_struct(23,lengths,displacement,types,MPI_PARTICLE,ierr)
!!$  call MPI_Type_commit(MPI_PARTICLE,ierr)
!!$  
!!$  ! If problem, say it
!!$  if (ierr.ne.0) call die("Problem with MPI_PARTICLE")
!!$  
!!$  ! Get the size of this type
!!$  call MPI_type_size(MPI_PARTICLE,SIZE_MPI_PARTICLE,ierr)
!!$  
!!$  return
!!$end subroutine lpt_mpi_prepare2


! ===================================== !
! Particle communication for collisions !
! ===================================== !
subroutine lpt_communication_gp
  use lpt_com
  implicit none
    
  ! Clean ghost particles structure
  call lpt_resize_gp(0)
  npart_gp_=0
  
  ! Communicate
  call lpt_communication_border_x(1)
  call lpt_communication_border_y(1)
  call lpt_communication_border_z(1)
  
  return
end subroutine lpt_communication_gp


! ================================ !
! LPT communication in x direction !
! ================================ !
subroutine lpt_communication_border_x(no)
  use lpt_com
  use parallel
  implicit none
  
  ! Input parameters
  integer, intent(in) :: no
  
  ! Local variables
  type(part_type), dimension(:), pointer :: tosend
  type(part_type), dimension(:), pointer :: torecv
  integer :: nsend,nrecv
  integer :: istatus(MPI_STATUS_SIZE)
  integer :: icount,isource,idest
  integer :: i,ierr,n
  
  ! Count number of particles to send left
  nsend=0
  do n=1,npart_
     if (part(n)%i.lt.imin_+no) nsend=nsend+1
  end do
  
  ! Allocate send buffer
  allocate(tosend(nsend))
  
  ! Copy particles in buffer
  nsend=0
  do n=1,npart_
     if (part(n)%i.lt.imin_+no) then
        nsend=nsend+1
        tosend(nsend)=part(n)
        if (xper.eq.1 .and. tosend(nsend)%i.lt.imin+no) then
           tosend(nsend)%x=tosend(nsend)%x+xL
           tosend(nsend)%i=tosend(nsend)%i+nx
        end if
     end if
  end do
 
  ! Communicate sizes
  nrecv=0
  call MPI_CART_SHIFT(comm,0,-1,isource,idest,ierr)
  call MPI_SENDRECV(nsend,1,MPI_INTEGER,idest,0,nrecv,1,MPI_INTEGER,isource,0,comm,istatus,ierr)
  
  ! Allocate recv buffer
  allocate(torecv(nrecv))

  ! Communicate
  call MPI_SENDRECV(tosend,nsend,MPI_PARTICLE,idest,0,torecv,nrecv,MPI_PARTICLE,isource,0,comm,istatus,ierr)
     
  ! Add to ghost particles
  if (nrecv.gt.0) then
     call lpt_resize_gp(npart_gp_+nrecv)
     part_gp(npart_gp_+1:npart_gp_+nrecv)=torecv(1:nrecv)
     npart_gp_=npart_gp_+nrecv
  end if

  ! Count number of particles to send right
  nsend=0
  do n=1,npart_
     if (part(n)%i.gt.imax_-no) nsend=nsend+1
  end do

  ! Allocate send buffer
  if (associated(tosend)) deallocate(tosend)
  allocate(tosend(nsend))

  ! Copy particles in buffer
  nsend=0
  do n=1,npart_
     if (part(n)%i.gt.imax_-no) then
        nsend=nsend+1
        tosend(nsend)=part(n)
        if (xper.eq.1 .and. tosend(nsend)%i.gt.imax-no) then
           tosend(nsend)%x=tosend(nsend)%x-xL
           tosend(nsend)%i=tosend(nsend)%i-nx
        end if
     end if
  end do

  ! Communicate sizes
  nrecv=0
  call MPI_CART_SHIFT(comm,0,+1,isource,idest,ierr)
  call MPI_SENDRECV(nsend,1,MPI_INTEGER,idest,2,nrecv,1,MPI_INTEGER,isource,2,comm,istatus,ierr)

  ! Allocate recv buffer
  deallocate(torecv)
  allocate(torecv(nrecv))
 
  ! Communicate
  call MPI_SENDRECV(tosend,nsend,MPI_PARTICLE,idest,0,torecv,nrecv,MPI_PARTICLE,isource,0,comm,istatus,ierr)

  ! Add to ghost particles
  if (nrecv.gt.0) then
     call lpt_resize_gp(npart_gp_+nrecv)
     part_gp(npart_gp_+1:npart_gp_+nrecv)=torecv(1:nrecv)
     npart_gp_=npart_gp_+nrecv
  end if

  ! Deallocate
  deallocate(tosend);nullify(tosend)
  deallocate(torecv);nullify(torecv)
  
  return
end subroutine lpt_communication_border_x


! ================================ !
! LPT communication in y direction !
! ================================ !
subroutine lpt_communication_border_y(no)
  use lpt_com
  use parallel
  implicit none
  
  ! Input parameters
  integer, intent(in) :: no
  
  ! Local variables
  type(part_type), dimension(:), pointer :: tosend
  type(part_type), dimension(:), pointer :: torecv
  integer :: nsend,nrecv
  integer :: istatus(MPI_STATUS_SIZE)
  integer :: icount,isource,idest
  integer :: i,ierr,n
  
  ! Count number of particles to send left
  nsend=0
  do n=1,npart_
     if (part(n)%j.lt.jmin_+no) nsend=nsend+1
  end do
  do n=1,npart_gp_
     if (part_gp(n)%j.lt.jmin_+no) nsend=nsend+1
  end do

  ! Allocate send buffer
  allocate(tosend(nsend))
  
  ! Copy particles in buffer
  nsend=0
  do n=1,npart_
     if (part(n)%j.lt.jmin_+no) then
        nsend=nsend+1
        tosend(nsend)=part(n)
        if (yper.eq.1 .and. tosend(nsend)%j.lt.jmin+no) then
           tosend(nsend)%y=tosend(nsend)%y+yL
           tosend(nsend)%j=tosend(nsend)%j+ny
        end if
     end if
  end do
  do n=1,npart_gp_
     if (part_gp(n)%j.lt.jmin_+no) then
        nsend=nsend+1
        tosend(nsend)=part_gp(n)
        if (yper.eq.1 .and. tosend(nsend)%j.lt.jmin+no) then
           tosend(nsend)%y=tosend(nsend)%y+yL
           tosend(nsend)%j=tosend(nsend)%j+ny
        end if
     end if
  end do
  
  ! Communicate sizes
  nrecv=0
  call MPI_CART_SHIFT(comm,1,-1,isource,idest,ierr)
  call MPI_SENDRECV(nsend,1,MPI_INTEGER,idest,4,nrecv,1,MPI_INTEGER,isource,4,comm,istatus,ierr)
  
  ! Allocate recv buffer
  allocate(torecv(nrecv))


  ! Communicate
  call MPI_SENDRECV(tosend,nsend,MPI_PARTICLE,idest,0,torecv,nrecv,MPI_PARTICLE,isource,0,comm,istatus,ierr)

  ! Add to ghost particles
  call lpt_resize_gp(npart_gp_+nrecv)
  if (nrecv.gt.0) part_gp(npart_gp_+1:npart_gp_+nrecv)=torecv(1:nrecv)
  npart_gp_=npart_gp_+nrecv
  
  ! Count number of particles to send right
  nsend=0
  do n=1,npart_
     if (part(n)%j.gt.jmax_-no) nsend=nsend+1
  end do
  do n=1,npart_gp_-nrecv
     if (part_gp(n)%j.gt.jmax_-no) nsend=nsend+1
  end do
  
  ! Allocate send buffer
  deallocate(tosend)
  allocate(tosend(nsend))
  
  ! Copy particles in buffer
  nsend=0
  do n=1,npart_
     if (part(n)%j.gt.jmax_-no) then
        nsend=nsend+1
        tosend(nsend)=part(n)
        if (yper.eq.1 .and. tosend(nsend)%j.gt.jmax-no) then
           tosend(nsend)%y=tosend(nsend)%y-yL
           tosend(nsend)%j=tosend(nsend)%j-ny
        end if
     end if
  end do
  do n=1,npart_gp_-nrecv
     if (part_gp(n)%j.gt.jmax_-no) then
        nsend=nsend+1
        tosend(nsend)=part_gp(n)
        if (yper.eq.1 .and. tosend(nsend)%j.gt.jmax-no) then
           tosend(nsend)%y=tosend(nsend)%y-yL
           tosend(nsend)%j=tosend(nsend)%j-ny
        end if
     end if
  end do
  
  ! Communicate sizes
  nrecv=0
  call MPI_CART_SHIFT(comm,1,+1,isource,idest,ierr)
  call MPI_SENDRECV(nsend,1,MPI_INTEGER,idest,6,nrecv,1,MPI_INTEGER,isource,6,comm,istatus,ierr)
  
  ! Allocate recv buffer
  deallocate(torecv)
  allocate(torecv(nrecv))

  ! Communicate     
  call MPI_SENDRECV(tosend,nsend,MPI_PARTICLE,idest,0,torecv,nrecv,MPI_PARTICLE,isource,0,comm,istatus,ierr)

  ! Add to ghost particles
  call lpt_resize_gp(npart_gp_+nrecv)
  if (nrecv.gt.0) part_gp(npart_gp_+1:npart_gp_+nrecv)=torecv(1:nrecv)
  npart_gp_=npart_gp_+nrecv
  
  ! Deallocate
  deallocate(tosend);nullify(tosend)
  deallocate(torecv);nullify(torecv)
  
  return
end subroutine lpt_communication_border_y


! ================================ !
! LPT communication in z direction !
! ================================ !
subroutine lpt_communication_border_z(no)
  use lpt_com
  use parallel
  implicit none
  
  ! Input parameters
  integer, intent(in) :: no

  ! Local variables
  type(part_type), dimension(:), pointer :: tosend
  type(part_type), dimension(:), pointer :: torecv
  integer :: nsend,nrecv
  integer :: istatus(MPI_STATUS_SIZE)
  integer :: icount,isource,idest
  integer :: i,ierr,n
  
  ! Count number of particles to send left
  nsend=0
  do n=1,npart_
     if (part(n)%k.lt.kmin_+no) nsend=nsend+1
  end do
  do n=1,npart_gp_
     if (part_gp(n)%k.lt.kmin_+no) nsend=nsend+1
  end do

  ! Allocate send buffer
  allocate(tosend(nsend))
  
  ! Copy particles in buffer
  nsend=0
  do n=1,npart_
     if (part(n)%k.lt.kmin_+no) then
        nsend=nsend+1
        tosend(nsend)=part(n)
        if (zper.eq.1 .and. tosend(nsend)%k.lt.kmin+no) then
           tosend(nsend)%z=tosend(nsend)%z+zL
           tosend(nsend)%k=tosend(nsend)%k+nz
        end if
     end if
  end do
  do n=1,npart_gp_
     if (part_gp(n)%k.lt.kmin_+no) then
        nsend=nsend+1
        tosend(nsend)=part_gp(n)
        if (zper.eq.1 .and. tosend(nsend)%k.lt.kmin+no) then
           tosend(nsend)%z=tosend(nsend)%z+zL
           tosend(nsend)%k=tosend(nsend)%k+nz
        end if
     end if
  end do
  
  ! Communicate sizes
  nrecv=0
  call MPI_CART_SHIFT(comm,2,-1,isource,idest,ierr)
  call MPI_SENDRECV(nsend,1,MPI_INTEGER,idest,8,nrecv,1,MPI_INTEGER,isource,8,comm,istatus,ierr)
  
  ! Allocate recv buffer
  allocate(torecv(nrecv))

  ! Communicate     
  call MPI_SENDRECV(tosend,nsend,MPI_PARTICLE,idest,0,torecv,nrecv,MPI_PARTICLE,isource,0,comm,istatus,ierr)

  ! Add to ghost particles
  call lpt_resize_gp(npart_gp_+nrecv)
  if (nrecv.gt.0) part_gp(npart_gp_+1:npart_gp_+nrecv)=torecv(1:nrecv)
  npart_gp_=npart_gp_+nrecv
  
  ! Count number of particles to send right
  nsend=0
  do n=1,npart_
     if (part(n)%k.gt.kmax_-no) nsend=nsend+1
  end do
  do n=1,npart_gp_-nrecv
     if (part_gp(n)%k.gt.kmax_-no) nsend=nsend+1
  end do

  ! Allocate send buffer
  deallocate(tosend)
  allocate(tosend(nsend))
  
  ! Copy particles in buffer
  nsend=0
  do n=1,npart_
     if (part(n)%k.gt.kmax_-no) then
        nsend=nsend+1
        tosend(nsend)=part(n)
        if (zper.eq.1 .and. tosend(nsend)%k.gt.kmax-no) then
           tosend(nsend)%z=tosend(nsend)%z-zL
           tosend(nsend)%k=tosend(nsend)%k-nz
        end if
     end if
  end do
  do n=1,npart_gp_-nrecv
     if (part_gp(n)%k.gt.kmax_-no) then
        nsend=nsend+1
        tosend(nsend)=part_gp(n)
        if (zper.eq.1 .and. tosend(nsend)%k.gt.kmax-no) then
           tosend(nsend)%z=tosend(nsend)%z-zL
           tosend(nsend)%k=tosend(nsend)%k-nz
        end if
     end if
  end do
  
  ! Communicate sizes
  nrecv=0
  call MPI_CART_SHIFT(comm,2,+1,isource,idest,ierr)
  call MPI_SENDRECV(nsend,1,MPI_INTEGER,idest,10,nrecv,1,MPI_INTEGER,isource,10,comm,istatus,ierr)

  ! Allocate recv buffer
  deallocate(torecv)
  allocate(torecv(nrecv))

  ! Communicate
  call MPI_SENDRECV(tosend,nsend,MPI_PARTICLE,idest,0,torecv,nrecv,MPI_PARTICLE,isource,0,comm,istatus,ierr)

  ! Add to ghost particles
  call lpt_resize_gp(npart_gp_+nrecv)
  if (nrecv.gt.0) part_gp(npart_gp_+1:npart_gp_+nrecv)=torecv(1:nrecv)
  npart_gp_=npart_gp_+nrecv
  
  ! Deallocate
  deallocate(tosend);nullify(tosend)
  deallocate(torecv);nullify(torecv)
  
  return
end subroutine lpt_communication_border_z
