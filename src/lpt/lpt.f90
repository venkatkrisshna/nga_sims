! ============================ !
! Lagrangian particle tracking !
! ============================ !
module lpt
  use precision
  use geometry
  use partition
  use parallel
  use parser
  use lpt_mod
  use string
  use data
  use scalar
  use velocity
  use combustion
  use bodyforce
  use math
  implicit none
  
  ! Stress term
  real(WP), dimension(:,:,:), allocatable :: lpt_stress_x
  real(WP), dimension(:,:,:), allocatable :: lpt_stress_y
  real(WP), dimension(:,:,:), allocatable :: lpt_stress_z
  
  ! Localization of particles in cells
  integer, dimension(:,:,:), allocatable   :: npartincell_lpt
  integer, dimension(:,:,:,:), allocatable :: partincell_lpt
  
  ! Particle diameter
  real(WP) :: dmean,dmin,dmax,dsd,dshift
  
  ! Particle density
  real(WP) :: rhod
  
  ! Solver parameters
  real(WP) :: lpt_substeps
  logical :: lpt_twoway
  
  ! Collision parameters
  integer, dimension(:,:), allocatable :: lpt_xwall,lpt_ywall
  logical :: use_col,bound_top,use_wall
  real(WP) :: e_n,e_w,mu_f,dt_col
  
  ! Monitoring info
  real(WP) :: lpt_umin,lpt_umax,lpt_umean,lpt_uvar
  real(WP) :: lpt_vmin,lpt_vmax,lpt_vmean,lpt_vvar
  real(WP) :: lpt_wmin,lpt_wmax,lpt_wmean,lpt_wvar
  real(WP) :: lpt_wxmin,lpt_wxmax,lpt_wxmean,lpt_wxvar
  real(WP) :: lpt_wymin,lpt_wymax,lpt_wymean,lpt_wyvar
  real(WP) :: lpt_wzmin,lpt_wzmax,lpt_wzmean,lpt_wzvar
  real(WP) :: epsp_max,epsp_mean,epsp_var
  real(WP) :: lpt_cfl
  integer  :: npart_in,npart_out
    
end module lpt


! ======================= !
! Initialize lpt routines !
! ======================= !
subroutine lpt_init
  use lpt
  use parser
  implicit none
  
  integer, dimension(:), allocatable :: windex
  integer :: nwall,n,maxwall
  integer :: i,j,maxpartincell
  real(WP) :: max_vol
  
  ! Check whether lpt is used
  call parser_read('Use particle tracking',use_lpt,.false.)
  if (.not.use_lpt) return
  
  ! Create & start the timer
  call timing_create('lpt')
  call timing_start ('lpt')
  
  ! Allocate arrays
  allocate(npart_proc(nproc))
  allocate(lpt_stress_x(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); lpt_stress_x=0.0_WP
  allocate(lpt_stress_y(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); lpt_stress_y=0.0_WP
  allocate(lpt_stress_z(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); lpt_stress_z=0.0_WP
  
  ! Initialize MPI
  call lpt_resize(2)
  call lpt_mpi_prepare ! Old MPI-1.2 standard
  !call lpt_mpi_prepare2 ! New MPI-2 standard to account for 64 bits machines
  call lpt_resize(0)
  
  ! Read the input parameters
  call parser_read('Particle density',rhod)
  call parser_read('Particle collisions',use_col,.true.)
  if (use_col) then
     call parser_read('Collision time',dt_col)
     call parser_read('Restitution coeff',e_n,0.85_WP)
     call parser_read('Wall restitution',e_w,0.75_WP)
     call parser_read('Friction coeff',mu_f,0.1_WP)
  end if
  call parser_read('Number of substeps',lpt_substeps)
  call parser_read('Two-way coupling',lpt_twoway)
  call parser_read('Bound top',bound_top,.false.)
  
  ! Read the particle file
  call lpt_io_init
  call lpt_read
  
  ! Zero collision forces if collisions are turned off
  if (.not.use_col) then
     do i=1,npart_
        part(i)%Acol=0.0_WP
        part(i)%Tcol=0.0_WP
     end do
  end if
  
  ! Initialize LPT injection
  call lpt_inject_init
  
  ! Initialize routines for two-way coupling
  if (lpt_twoway) then
     call lpt_smooth_init
     call lpt_get_epsf
  end if
  
  ! Number of particles per cell
  if (use_col) then
     ! Estimate max number of particles per cell
     call parallel_max(maxval(vol),max_vol)
     maxpartincell=ceiling(2.0_WP*max_vol/(pi*dmin**3/6.0_WP))
     ! Allocate particle-per-cell counter
     allocate(npartincell_lpt(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
     ! Check current max number of particle-per-cell
     npartincell_lpt=0
     do i=1,npart_
        npartincell_lpt(part(i)%i,part(i)%j,part(i)%k)=npartincell_lpt(part(i)%i,part(i)%j,part(i)%k)+1
     end do
     call parallel_max(maxval(npartincell_lpt),i)
     ! Adjust estimate of max particle-per-cell
     maxpartincell=max(maxpartincell,i)
     ! Allocate particle-per-cell storage
     allocate( partincell_lpt(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,maxpartincell))
  end if

  ! Identify walls for collision detection
  use_wall=.false.
  if ( use_col.and.maxval(mask).gt.0 .or. &
       use_col.and.xper.eq.0 .or. use_col.and.yper.eq.0 ) then
     use_wall=.true.
     ! Temporary storage for wall indices
     maxwall=2*max(nxo,nyo)
     allocate(windex(maxwall))
     ! Allocate wall arrays
     allocate(lpt_xwall(imino:imaxo,jmino:jmaxo)); lpt_xwall=maxwall
     allocate(lpt_ywall(imino:imaxo,jmino:jmaxo)); lpt_ywall=maxwall
     ! Find distance to closest wall in x
     do j=jmin,jmax
        ! Find indices of walls
        nwall=0
        do i=imin+1,imax
           if ( mask(i,j).eq.0 .and. mask(i-1,j).eq.1 .or. &
                mask(i,j).eq.1 .and. mask(i-1,j).eq.0 ) then
              nwall=nwall+1; windex(nwall)=i
           end if
        end do
        if (xper.eq.0 .and. mask(imin,j).eq.0) then
           nwall=nwall+1; windex(nwall)=imin
        end if
        if (xper.eq.0 .and. mask(imax,j).eq.0 .and. bound_top) then
           nwall=nwall+1; windex(nwall)=imax+1
        end if
        ! Compute distance to closest wall (in cells)
        do i=imino,imaxo
           do n=1,nwall
              if (abs(windex(n)-i).lt.abs(lpt_xwall(i,j))) lpt_xwall(i,j)=windex(n)-i
           end do
        end do
     end do
     ! Copy in geometric ghost cells
     do i=imino,imaxo
        lpt_xwall(i,jmino:jmin-1)=lpt_xwall(i,jmin)
        lpt_xwall(i,jmax+1:jmaxo)=lpt_xwall(i,jmax)
     end do
     ! Find distance to closest wall in y
     do i=imin,imax
        ! Find indices of walls
        nwall=0
        do j=jmin+1,jmax
           if ( mask(i,j).eq.0 .and. mask(i,j-1).eq.1 .or. &
                mask(i,j).eq.1 .and. mask(i,j-1).eq.0 ) then
              nwall=nwall+1; windex(nwall)=j
           end if
        end do
        if (yper.eq.0 .and. mask(i,jmin).eq.0) then
           nwall=nwall+1; windex(nwall)=jmin
        end if
        if (yper.eq.0 .and. mask(i,jmax).eq.0) then
           nwall=nwall+1; windex(nwall)=jmax+1
        end if
        ! Compute distance to closest wall (in cells)
        do j=jmino,jmaxo
           do n=1,nwall
              if (abs(windex(n)-j).lt.abs(lpt_ywall(i,j))) lpt_ywall(i,j)=windex(n)-j
           end do
        end do
     end do
     ! Copy in geometric ghost cells
     do j=jmino,jmaxo
        lpt_ywall(imino:imin-1,j)=lpt_ywall(imin,j)
        lpt_ywall(imax+1:imaxo,j)=lpt_ywall(imax,j)
     end do
     ! Clean up
     deallocate(windex)
  end if
  
  ! Acoustophoresis model
  call lpt_acoustics_init
  
  ! Monitor particles
  call monitor_create_file_step('lpt',4)
  call monitor_set_header(1,'Npart','i')
  call monitor_set_header(2,'Npart in','i')
  call monitor_set_header(3,'Npart out','i')
  call monitor_set_header(4,'CFL','r')
  
  ! Monitor particle velocity
  call monitor_create_file_step('lpt_vel',24)
  call monitor_set_header( 1,'Umin'  ,'r')
  call monitor_set_header( 2,'Umax'  ,'r')
  call monitor_set_header( 3,'Umean' ,'r')
  call monitor_set_header( 4,'Uvar'  ,'r')
  call monitor_set_header( 5,'Vmin'  ,'r')
  call monitor_set_header( 6,'Vmax'  ,'r')
  call monitor_set_header( 7,'Vmean' ,'r')
  call monitor_set_header( 8,'Vvar'  ,'r')
  call monitor_set_header( 9,'Wmin'  ,'r')
  call monitor_set_header(10,'Wmax'  ,'r')
  call monitor_set_header(11,'Wmean' ,'r')
  call monitor_set_header(12,'Wvar'  ,'r')
  call monitor_set_header(13,'Wxmin' ,'r')
  call monitor_set_header(14,'Wxmax' ,'r')
  call monitor_set_header(15,'Wxmean','r')
  call monitor_set_header(16,'Wxvar' ,'r')
  call monitor_set_header(17,'Wymin' ,'r')
  call monitor_set_header(18,'Wymax' ,'r')
  call monitor_set_header(19,'Wymean','r')
  call monitor_set_header(20,'Wyvar' ,'r')
  call monitor_set_header(21,'Wzmin' ,'r')
  call monitor_set_header(22,'Wzmax' ,'r')
  call monitor_set_header(23,'Wzmean','r')
  call monitor_set_header(24,'Wzvar' ,'r')
  
  ! Monitor volume fraction
  call monitor_create_file_step('lpt_vf',3)
  call monitor_set_header(1,'VFavg','r')
  call monitor_set_header(2,'VFmax','r')
  call monitor_set_header(3,'VFvar','r')
  
  ! Stop a timer
  call timing_stop('lpt')
  
  return
end subroutine lpt_init


! ============================== !
! Advance the particle equations !
! part%id=0 => no coll, no solve !
! part%id=-1=> no coll, no move  !
! ============================== !
subroutine lpt_step
  use lpt
  use memory
  use metric_generic
  use ib
  use math
  implicit none
  
  ! Collision parameters
  integer  :: i,j,k,ii,jj,kk,ip,jp,n
  integer(kind=8) :: id1,id2
  real(WP), dimension(3) :: r1,r2,v1,v2,w1,w2,v12
  real(WP) :: m12,d1,d2,m1,m2
  
  ! Normal collision parameters
  real(WP) :: k_n,d12,delta_n,eta_n,r_influ,rnv
  real(WP), dimension(3) :: n12,v12_n,f_n
  real(WP), parameter :: clip_col=0.2_WP
  real(WP), parameter :: aclipnorm=1.0e-6_WP
  
  ! Tangential collision parameters
  real(WP) :: rtv
  real(WP), parameter :: acliptan=1.0e-9_WP
  real(WP), parameter :: rcliptan=0.05_WP
  real(WP), dimension(3) :: t12,f_t
  
  ! Buffers
  real(WP) :: buf,xwall,ywall
  
  ! Loop on neighboring cells
  integer :: i1,i2,j1,j2,k1,k2
  
  ! Check whether lpt is used
  if (.not.use_lpt) return
  
  ! Start a timer
  call timing_start('lpt')
  
  ! Zero counter for particle leaving the domain
  npart_out=0
  
  ! Add new particles to the computation
  call lpt_inject_step
  
  ! Compute stress term
  call lpt_compute_stress
  
  ! Compute acoustic potential
  call lpt_acoustics_potential
  
  ! Collisions
  if (use_col) then
     
     ! ------------------------------------ !
     ! --- Go through cell localization --- !
     ! ------------------------------------ !
     
     ! Communicate ghost particles
     call lpt_communication_gp
     
     ! Initialize arrays for particle localization
     npartincell_lpt =  0
     partincell_lpt  = -1
     
     ! Map normal particle to cell
     do i=1,npart_
        
        ! Localize particles in cells
        ii = part(i)%i
        jj = part(i)%j
        kk = part(i)%k
        npartincell_lpt(ii,jj,kk) = npartincell_lpt(ii,jj,kk) + 1
        partincell_lpt(ii,jj,kk,npartincell_lpt(ii,jj,kk)) = i
        
        ! Initialize collision forces
        part(i)%Acol = 0.0_WP
        part(i)%Tcol = 0.0_WP
        
     end do
     
     ! Map ghost particle to cell
     do i=1,npart_gp_
        
        ! Localize particles in cells
        ii = part_gp(i)%i
        jj = part_gp(i)%j
        kk = part_gp(i)%k
        npartincell_lpt(ii,jj,kk) = npartincell_lpt(ii,jj,kk) + 1
        partincell_lpt(ii,jj,kk,npartincell_lpt(ii,jj,kk)) = -i
        
     end do
     
     ! --------------------------------- !
     ! --- Go through collision term --- !
     ! --------------------------------- !
     
     ! Implement collision term
     collision: do ip=1,npart_
        
        ! Cycle if negative id
        if (part(ip)%id.le.0) cycle collision
        
        ! Localize particles in cells
        i = part(ip)%i
        j = part(ip)%j
        k = part(ip)%k
        
        ! Particle data
        id1   = part(ip)%id
        r1(1) = part(ip)%x
        r1(2) = part(ip)%y
        r1(3) = part(ip)%z
        v1(1) = part(ip)%u
        v1(2) = part(ip)%v
        v1(3) = part(ip)%w
        w1(1) = part(ip)%wx
        w1(2) = part(ip)%wy
        w1(3) = part(ip)%wz
        d1    = part(ip)%d
        m1    = rhod*Pi/6.0_WP*d1**3
        
        ! Define bounds for loop on neighboring cells
        i1 = i-1
        i2 = i+1
        j1 = j-1
        j2 = j+1
        k1 = k-1
        k2 = k+1
        
        ! -------------------------------- !
        !    PARTICLE-WALL COLLISIONS      !
        ! only collision with closest wall !
        ! algorithm will fail in corners   !
        ! -------------------------------- !
        if (use_wall) then
           
           ! Particle-wall radius of influence
           r_influ = 0.2_WP*d1
           
           ! Closest wall parameters
           r2(1) = r1(1)
           r2(2) = r1(2)
           r2(3) = r1(3)
           v2(1) = 0.0_WP
           v2(2) = 0.0_WP
           v2(3) = 0.0_WP
           w2(1) = 0.0_WP
           w2(2) = 0.0_WP
           w2(3) = 0.0_WP
           d2 = 0.0_WP
           
           ! Distance between a and b
           if (i+lpt_xwall(i,j).ge.imino.and.i+lpt_xwall(i,j).le.imaxo) then
              xwall=x(i+lpt_xwall(i,j))
           else
              xwall=huge(1.0_WP)
           end if
           if (j+lpt_ywall(i,j).ge.jmino.and.j+lpt_ywall(i,j).le.jmaxo) then
              ywall=y(j+lpt_ywall(i,j))
           else
              ywall=huge(1.0_WP)
           end if
           if (abs(r1(1)-xwall).lt.abs(r1(2)-ywall)) then
              r2(1)=xwall
           else
              r2(2)=ywall
           end if
           d12 = sqrt(sum((r1-r2)*(r1-r2)))
           if (mask(i,j).eq.1) d12=-d12
           
           ! Distance of influence
           delta_n = 0.5_WP*d1 + r_influ - d12
           delta_n = min(delta_n,clip_col*0.5_WP*d1)
           
           if (delta_n .gt. 0.0_WP) then
              ! Recompute distance of influence
              n12 = (r2-r1)/d12
              v12 = v1-v2
              rnv = sum(v12*n12)
              r_influ = min(0.2_WP*10.0_WP*abs(rnv)*dt,r_influ)
              delta_n = 0.5_WP*d1 + r_influ - d12
              delta_n = min(delta_n,clip_col*0.5_WP*d1)
              if (delta_n .gt. 0.0_WP) then
                 ! Normal collision
                 v12_n = rnv*n12
                 m12 = m1
                 k_n = m12/dt_col**2*(pi**2+log(e_w)**2)
                 eta_n = -2.0_WP*log(e_w)*m12/dt_col
                 f_n = -k_n*delta_n*n12 -eta_n*v12_n
                 ! Tangential collision
                 t12 = v12-v12_n+cross_product(0.5_WP*d1*w1,n12)
                 rtv = sqrt(sum(t12*t12))
                 if (rnv*dt/d1.gt.aclipnorm) then
                    if (rtv/rnv.lt.rcliptan) rtv=0.0_WP
                 else
                    if (rtv*dt/d1.lt.acliptan) rtv=0.0_WP
                 end if
                 f_t = 0.0_WP
                 if (rtv.gt.0.0_WP) f_t = -mu_f*sqrt(sum(f_n*f_n))*t12/rtv
                 ! Calculate collision force
                 f_n = f_n/m1; f_t = f_t/m1
                 part(ip)%Acol = part(ip)%Acol + f_n + f_t
                 ! Calculate collision torque
                 part(ip)%Tcol(1) = part(ip)%Tcol(1) + 0.5_WP*(d1*n12(2)*f_t(3) - d1*n12(3)*f_t(2))
                 part(ip)%Tcol(2) = part(ip)%Tcol(2) + 0.5_WP*(d1*n12(3)*f_t(1) - d1*n12(1)*f_t(3))
                 part(ip)%Tcol(3) = part(ip)%Tcol(3) + 0.5_WP*(d1*n12(1)*f_t(2) - d1*n12(2)*f_t(1))
              end if
           end if
        end if

        ! ------------------------------ !
        !     PARTICLE-IB COLLISIONS     !
        ! ------------------------------ !
        if (use_ib) then
           
           ! Particle-ib radius of influence
           r_influ = 0.2_WP*d1
           
           ! Assume fixed IB
           v2(1) = 0.0_WP
           v2(2) = 0.0_WP
           v2(3) = 0.0_WP
           w2(1) = 0.0_WP
           w2(2) = 0.0_WP
           w2(3) = 0.0_WP
           d2 = 0.0_WP
           
           ! Distance between a and b
           call lpt_interpolate(Gib,r1(1),r1(2),r1(3),i,j,k,d12,"SC")
           
           ! Distance of influence
           delta_n = 0.5_WP*d1 + r_influ - d12
           delta_n = min(delta_n,clip_col*0.5_WP*d1)
           
           if (delta_n .gt. 0.0_WP) then
              ! Normal vector
              call lpt_interpolate(Nxib,r1(1),r1(2),r1(3),i,j,k,n12(1),"SC")
              call lpt_interpolate(Nyib,r1(1),r1(2),r1(3),i,j,k,n12(2),"SC")
              call lpt_interpolate(Nzib,r1(1),r1(2),r1(3),i,j,k,n12(3),"SC")
              buf = sqrt(sum(n12*n12))+epsilon(1.0_WP)
              n12 = -n12/buf
              ! Recompute distance of influence
              v12 = v1-v2
              rnv = sum(v12*n12)
              r_influ = min(0.2_WP*10.0_WP*abs(rnv)*dt,r_influ)
              delta_n = 0.5_WP*d1 + r_influ - d12
              delta_n = min(delta_n,clip_col*0.5_WP*d1)
              if (delta_n .gt. 0.0_WP) then
                 ! Normal collision
                 v12_n = rnv*n12
                 m12 = m1
                 k_n = m12/dt_col**2*(pi**2+log(e_w)**2)
                 eta_n = -2.0_WP*log(e_w)*m12/dt_col
                 f_n = -k_n*delta_n*n12 -eta_n*v12_n
                 ! Tangential collision
                 t12 = v12-v12_n+cross_product(0.5_WP*d1*w1,n12)
                 rtv = sqrt(sum(t12*t12))
                 if (rnv*dt/d1.gt.aclipnorm) then
                    if (rtv/rnv.lt.rcliptan) rtv=0.0_WP
                 else
                    if (rtv*dt/d1.lt.acliptan) rtv=0.0_WP
                 end if
                 f_t = 0.0_WP
                 if (rtv.gt.0.0_WP) f_t = -mu_f*sqrt(sum(f_n*f_n))*t12/rtv
                 ! Calculate collision force
                 f_n = f_n/m1; f_t = f_t/m1
                 part(ip)%Acol = part(ip)%Acol + f_n + f_t
                 ! Calculate collision torque
                 part(ip)%Tcol(1) = part(ip)%Tcol(1) + 0.5_WP*(d1*n12(2)*f_t(3) - d1*n12(3)*f_t(2))
                 part(ip)%Tcol(2) = part(ip)%Tcol(2) + 0.5_WP*(d1*n12(3)*f_t(1) - d1*n12(1)*f_t(3))
                 part(ip)%Tcol(3) = part(ip)%Tcol(3) + 0.5_WP*(d1*n12(1)*f_t(2) - d1*n12(2)*f_t(1))
              end if
           end if
           
        end if
        
        ! ------------------------------ !
        !  PARTICLE-PARTICLE COLLISIONS  !
        ! ------------------------------ !
        ! Loop over neighboring cells
        do kk=k1,k2
           do jj=j1,j2
              do ii=i1,i2
                 
                 ! Loop over particles in cells
                 do n=1,npartincell_lpt(ii,jj,kk)
                    
                    ! Get particle id
                    jp=partincell_lpt(ii,jj,kk,n)
                    
                    if (jp.gt.0) then
                       ! Get particle data
                       id2   = part(jp)%id
                       r2(1) = part(jp)%x
                       r2(2) = part(jp)%y
                       r2(3) = part(jp)%z
                       v2(1) = part(jp)%u
                       v2(2) = part(jp)%v
                       v2(3) = part(jp)%w
                       w2(1) = part(jp)%wx
                       w2(2) = part(jp)%wy
                       w2(3) = part(jp)%wz
                       d2    = part(jp)%d
                       m2    = rhod*Pi/6.0_WP*d2**3
                    else
                       ! Get position and velocity ! CAREFUL, GHOST CELL
                       jp=-jp
                       id2   = part_gp(jp)%id
                       r2(1) = part_gp(jp)%x
                       r2(2) = part_gp(jp)%y
                       r2(3) = part_gp(jp)%z
                       v2(1) = part_gp(jp)%u
                       v2(2) = part_gp(jp)%v
                       v2(3) = part_gp(jp)%w
                       w2(1) = part_gp(jp)%wx
                       w2(2) = part_gp(jp)%wy
                       w2(3) = part_gp(jp)%wz
                       d2    = part_gp(jp)%d
                       m2    = rhod*Pi/6.0_WP*d2**3
                    end if
                    
                    ! Distance between a and b
                    d12 = sqrt(sum((r1-r2)*(r1-r2)))
                    
                    ! Guess particle-particle radius of influence
                    r_influ = 0.1_WP*(d1+d2)
                    
                    ! Distance of influence
                    delta_n = 0.5_WP*(d1+d2) + r_influ - d12
                    delta_n = min(delta_n,clip_col*0.5_WP*(d1+d2))
                    
                    if (delta_n .gt. 0.0_WP .and. id1.ne.id2) then
                       ! Recompute distance of influence
                       n12 = (r2-r1)/d12
                       v12 = v1-v2
                       rnv = sum(v12*n12)
                       r_influ = min(0.1_WP*10.0_WP*abs(rnv)*dt,r_influ)
                       delta_n = 0.5_WP*(d1+d2) + r_influ - d12
                       delta_n = min(delta_n,clip_col*0.5_WP*(d1+d2))
                       if (delta_n .gt. 0.0_WP) then
                          ! Normal collision
                          v12_n = rnv*n12
                          m12 = m1*m2/(m1+m2)
                          k_n = m12/dt_col**2*(pi**2+log(e_n)**2)
                          eta_n = -2.0_WP*log(e_n)*m12/dt_col
                          f_n  = -k_n*delta_n*n12 -eta_n*v12_n
                          ! Tangential collision
                          t12 = v12-v12_n+cross_product(0.5_WP*(d1*w1+d2*w2),n12)
                          rtv = sqrt(sum(t12*t12))
                          if (rnv*dt*2.0_WP/(d1+d2).gt.aclipnorm) then
                             if (rtv/rnv.lt.rcliptan) rtv=0.0_WP
                          else
                             if (rtv*dt*2.0_WP/(d1+d2).lt.acliptan) rtv=0.0_WP
                          end if
                          f_t = 0.0_WP
                          if (rtv.gt.0.0_WP) f_t = -mu_f*sqrt(sum(f_n*f_n))*t12/rtv
                          ! Calculate collision force
                          f_n = f_n/m1; f_t = f_t/m1
                          part(ip)%Acol = part(ip)%Acol + f_n + f_t
                          ! Calculate collision torque
                          part(ip)%Tcol(1) = part(ip)%Tcol(1) + 0.5_WP*(d1*n12(2)*f_t(3) - d1*n12(3)*f_t(2))
                          part(ip)%Tcol(2) = part(ip)%Tcol(2) + 0.5_WP*(d1*n12(3)*f_t(1) - d1*n12(1)*f_t(3))
                          part(ip)%Tcol(3) = part(ip)%Tcol(3) + 0.5_WP*(d1*n12(1)*f_t(2) - d1*n12(2)*f_t(1))
                       end if
                    end if
                    
                 end do
                 
              end do
           end do
        end do
        
        ! Deal with dimensionality
        if (nx.eq.1) then
           part(ip)%Acol(1)=0.0_WP
           part(ip)%Tcol(2)=0.0_WP
           part(ip)%Tcol(3)=0.0_WP
        end if
        if (ny.eq.1) then
           part(ip)%Tcol(1)=0.0_WP
           part(ip)%Acol(2)=0.0_WP
           part(ip)%Tcol(3)=0.0_WP
        end if
        if (nz.eq.1) then
           part(ip)%Tcol(1)=0.0_WP
           part(ip)%Tcol(2)=0.0_WP
           part(ip)%Acol(3)=0.0_WP
        end if
        
     end do collision
  end if
  
  ! Zero temporary arrays for source terms
  tmp1=0.0_WP
  tmp2=0.0_WP
  tmp3=0.0_WP
  
  ! Advance the equations
  do i=1,npart_
     if (part(i)%id.ne.0) call lpt_solve(i)
  end do
  
  ! Communicate particles
  call lpt_communication
  
  !  Sum up the source terms at the boundaries
  if (lpt_twoway) then
     
     ! Divide by volume
     do k=kmino_,kmaxo_
        do j=jmino_,jmaxo_
           do i=imino_,imaxo_
              if (vol(i,j,k).gt.epsilon(1.0_WP)) then
                 tmp1(i,j,k)=tmp1(i,j,k)/vol(i,j,k)
                 tmp2(i,j,k)=tmp2(i,j,k)/vol(i,j,k)
                 tmp3(i,j,k)=tmp3(i,j,k)/vol(i,j,k)
              else
                 tmp1(i,j,k)=0.0_WP
                 tmp2(i,j,k)=0.0_WP
                 tmp3(i,j,k)=0.0_WP
              end if
           end do
        end do
     end do
     
     ! Add up source at boundaries
     call summation_border(tmp1)
     call summation_border(tmp2)
     call summation_border(tmp3)
     
     ! Filter source term
     call lpt_smooth(tmp1,'+')
     call lpt_smooth(tmp2,'-')
     call lpt_smooth(tmp3,'-')
     
     ! Apply boundary conditions
     call boundary_dirichlet(tmp1,'+xm')
     call boundary_dirichlet(tmp1,'-xm')
     call boundary_dirichlet(tmp2,'+xm')
     call boundary_dirichlet(tmp2,'-xm')
     call boundary_dirichlet(tmp3,'+xm')
     call boundary_dirichlet(tmp3,'-xm')
     
     ! Interpolate to the faces - inside only
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (mask_u(i,j).eq.0) srcU(i,j,k)=srcU(i,j,k)+sum(interp_sc_x(i,j,:)*tmp1(i-st2:i+st1,j,k))
              if (mask_v(i,j).eq.0) srcV(i,j,k)=srcV(i,j,k)+sum(interp_sc_y(i,j,:)*tmp2(i,j-st2:j+st1,k))
              if (mask_w(i,j).eq.0) srcW(i,j,k)=srcW(i,j,k)+sum(interp_sc_z(i,j,:)*tmp3(i,j,k-st2:k+st1))
           end do
        end do
     end do
     
     ! Compute epsf
     call lpt_get_epsf
     
  end if

  ! Reinject particles that left domain
  call lpt_inject_reinject
  
  ! Stop a timer
  call timing_stop('lpt')
  
  return
end subroutine lpt_step


! ============================= !
! Droplet solver: integrates in !
! time the droplets equations   !
! using RK and sub-stepping     !
! ============================= !
subroutine lpt_solve(id)
  use lpt
  implicit none
  
  integer, intent(in) :: id
  real(WP) :: dt_try,dt_done
  integer :: nstep
  
  ! Prepare the time integrator
  nstep = 0
  dt_done = 0.0_WP
  
  ! Loop until dt_done=dt
  solve: do while (dt_done.lt.dt)
     ! Decide the timestep size
     dt_try = min(part(id)%dt,dt-dt_done)
     ! Solve the equations
     call lpt_solver_solve(dt_try,id)
     ! Increment
     dt_done = dt_done+dt_try
  end do solve
  
  ! Correct the position to take into account periodicity
  ! Z periodicity
  if (zper.eq.1) part(id)%z=z(kmin)+modulo(part(id)%z-z(kmin),z(kmax+1)-z(kmin))
  ! Y periodicity
  if (yper.eq.1) part(id)%y=y(jmin)+modulo(part(id)%y-y(jmin),y(jmax+1)-y(jmin))
  ! X periodicity
  if (xper.eq.1) part(id)%x=x(imin)+modulo(part(id)%x-x(imin),x(imax+1)-x(imin))
  
  ! Relocalize
  call lpt_localize(part(id)%i,part(id)%j,part(id)%k,part(id)%i,part(id)%j,part(id)%k,part(id)%x,part(id)%y,part(id)%z)
  
  return
end subroutine lpt_solve


! ========================= !
! Particle phase monitoring !
! ========================= !
subroutine lpt_monitor
  use lpt
  use time_info
  implicit none
  
  integer :: i,j,k,ibuf
  real(WP) :: buf
  integer, parameter :: nbin=100
  real(WP), dimension(nbin) :: pdf,my_pdf
  
  ! Check whether lpt is used
  if (.not.use_lpt) return
    
  ! Velocity min/max/mean
  lpt_umin=huge(1.0_WP); lpt_umax=-huge(1.0_WP); lpt_umean=0.0_WP
  lpt_vmin=huge(1.0_WP); lpt_vmax=-huge(1.0_WP); lpt_vmean=0.0_WP
  lpt_wmin=huge(1.0_WP); lpt_wmax=-huge(1.0_WP); lpt_wmean=0.0_WP
  lpt_wxmin=huge(1.0_WP); lpt_wxmax=-huge(1.0_WP); lpt_wxmean=0.0_WP
  lpt_wymin=huge(1.0_WP); lpt_wymax=-huge(1.0_WP); lpt_wymean=0.0_WP
  lpt_wzmin=huge(1.0_WP); lpt_wzmax=-huge(1.0_WP); lpt_wzmean=0.0_WP
  do i=1,npart_
     lpt_umin=min(lpt_umin,part(i)%u); lpt_umax=max(lpt_umax,part(i)%u); lpt_umean=lpt_umean+part(i)%u
     lpt_vmin=min(lpt_vmin,part(i)%v); lpt_vmax=max(lpt_vmax,part(i)%v); lpt_vmean=lpt_vmean+part(i)%v
     lpt_wmin=min(lpt_wmin,part(i)%w); lpt_wmax=max(lpt_wmax,part(i)%w); lpt_wmean=lpt_wmean+part(i)%w
     lpt_wxmin=min(lpt_wxmin,part(i)%wx); lpt_wxmax=max(lpt_wxmax,part(i)%wx); lpt_wxmean=lpt_wxmean+part(i)%wx
     lpt_wymin=min(lpt_wymin,part(i)%wy); lpt_wymax=max(lpt_wymax,part(i)%wy); lpt_wymean=lpt_wymean+part(i)%wy
     lpt_wzmin=min(lpt_wzmin,part(i)%wz); lpt_wzmax=max(lpt_wzmax,part(i)%wz); lpt_wzmean=lpt_wzmean+part(i)%wz
  end do
  call parallel_min(lpt_umin ,buf); lpt_umin =buf
  call parallel_max(lpt_umax ,buf); lpt_umax =buf
  call parallel_sum(lpt_umean,buf); lpt_umean=buf/npart
  call parallel_min(lpt_vmin ,buf); lpt_vmin =buf
  call parallel_max(lpt_vmax ,buf); lpt_vmax =buf
  call parallel_sum(lpt_vmean,buf); lpt_vmean=buf/npart
  call parallel_min(lpt_wmin ,buf); lpt_wmin =buf
  call parallel_max(lpt_wmax ,buf); lpt_wmax =buf
  call parallel_sum(lpt_wmean,buf); lpt_wmean=buf/npart
  call parallel_min(lpt_wxmin ,buf); lpt_wxmin =buf
  call parallel_max(lpt_wxmax ,buf); lpt_wxmax =buf
  call parallel_sum(lpt_wxmean,buf); lpt_wxmean=buf/npart
  call parallel_min(lpt_wymin ,buf); lpt_wymin =buf
  call parallel_max(lpt_wymax ,buf); lpt_wymax =buf
  call parallel_sum(lpt_wymean,buf); lpt_wymean=buf/npart
  call parallel_min(lpt_wzmin ,buf); lpt_wzmin =buf
  call parallel_max(lpt_wzmax ,buf); lpt_wzmax =buf
  call parallel_sum(lpt_wzmean,buf); lpt_wzmean=buf/npart
  
  ! Velocity variance
  lpt_uvar=0.0_WP
  lpt_vvar=0.0_WP
  lpt_wvar=0.0_WP
  lpt_wxvar=0.0_WP
  lpt_wyvar=0.0_WP
  lpt_wzvar=0.0_WP
  do i=1,npart_
     lpt_uvar=lpt_uvar+(part(i)%u-lpt_umean)**2.0_WP
     lpt_vvar=lpt_vvar+(part(i)%v-lpt_vmean)**2.0_WP
     lpt_wvar=lpt_wvar+(part(i)%w-lpt_wmean)**2.0_WP
     lpt_wxvar=lpt_wxvar+(part(i)%wx-lpt_wxmean)**2.0_WP
     lpt_wyvar=lpt_wyvar+(part(i)%wy-lpt_wymean)**2.0_WP
     lpt_wzvar=lpt_wzvar+(part(i)%wz-lpt_wzmean)**2.0_WP
  end do
  call parallel_sum(lpt_uvar,buf); lpt_uvar=buf/npart
  call parallel_sum(lpt_vvar,buf); lpt_vvar=buf/npart
  call parallel_sum(lpt_wvar,buf); lpt_wvar=buf/npart
  call parallel_sum(lpt_wxvar,buf); lpt_wxvar=buf/npart
  call parallel_sum(lpt_wyvar,buf); lpt_wyvar=buf/npart
  call parallel_sum(lpt_wzvar,buf); lpt_wzvar=buf/npart
    
  ! Transfer to monitor
  call monitor_select_file('lpt_vel')
  call monitor_set_single_value( 1,lpt_umin)
  call monitor_set_single_value( 2,lpt_umax)
  call monitor_set_single_value( 3,lpt_umean)
  call monitor_set_single_value( 4,lpt_uvar)
  call monitor_set_single_value( 5,lpt_vmin)
  call monitor_set_single_value( 6,lpt_vmax)
  call monitor_set_single_value( 7,lpt_vmean)
  call monitor_set_single_value( 8,lpt_vvar)
  call monitor_set_single_value( 9,lpt_wmin)
  call monitor_set_single_value(10,lpt_wmax)
  call monitor_set_single_value(11,lpt_wmean)
  call monitor_set_single_value(12,lpt_wvar)
  call monitor_set_single_value(13,lpt_wxmin)
  call monitor_set_single_value(14,lpt_wxmax)
  call monitor_set_single_value(15,lpt_wxmean)
  call monitor_set_single_value(16,lpt_wxvar)
  call monitor_set_single_value(17,lpt_wymin)
  call monitor_set_single_value(18,lpt_wymax)
  call monitor_set_single_value(19,lpt_wymean)
  call monitor_set_single_value(20,lpt_wyvar)
  call monitor_set_single_value(21,lpt_wzmin)
  call monitor_set_single_value(22,lpt_wzmax)
  call monitor_set_single_value(23,lpt_wzmean)
  call monitor_set_single_value(24,lpt_wzvar)
  
  ! Particle volume fraction mean/max
  epsp_mean=0.0_WP
  epsp_max=-huge(1.0_WP)
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           epsp_mean=epsp_mean+vol(i,j,k)*(1.0_WP-epsf(i,j,k))
           epsp_max=max(epsp_max,1.0_WP-epsf(i,j,k))
        end do
     end do
  end do
  call parallel_sum(epsp_mean,buf); epsp_mean=buf/vol_total
  call parallel_max(epsp_max,buf); epsp_max=buf
  
  ! Particle volume fraction variance
  epsp_var=0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           epsp_var=epsp_var+vol(i,j,k)*(1.0_WP-epsf(i,j,k)-epsp_mean)**2.0_WP
        end do
     end do
  end do
  call parallel_sum(epsp_var,buf); epsp_var=buf/vol_total
  
  ! Transfer to monitor
  call monitor_select_file('lpt_vf')
  call monitor_set_single_value(1,epsp_mean)
  call monitor_set_single_value(2,epsp_max)
  call monitor_set_single_value(3,epsp_var)
  
  ! Particle CFL pdf
  my_pdf=0.0_WP
  do i=1,npart_
     lpt_cfl=dt*sqrt(part(i)%u**2+part(i)%v**2+part(i)%w**2)/part(i)%d
     my_pdf(min(floor(lpt_cfl*nbin)+1,nbin))=my_pdf(min(floor(lpt_cfl*nbin)+1,nbin))+1.0_WP
  end do
  call parallel_sum(my_pdf,pdf)
  if (npart.gt.0) pdf=pdf/real(npart,WP)
  
  ! Find CFL of 90th percentile
  i=1; buf=pdf(1)
  do while (buf.lt.0.9_WP .and. npart.ne.0)
     i=i+1
     buf=buf+pdf(i)
  end do
  lpt_cfl=real(i,WP)/real(nbin,WP)
  
  ! Exit condition
  call parallel_sum(npart_out,ibuf); npart_out=ibuf
  
  ! Transfer to monitor
  call monitor_select_file('lpt')
  call monitor_set_single_value(1,real(npart,WP))
  call monitor_set_single_value(2,real(npart_in,WP))
  call monitor_set_single_value(3,real(npart_out,WP))
  call monitor_set_single_value(4,lpt_cfl)
  
  ! Modify NGA CFL
  if (use_col) CFL = max(CFL,10.0_WP*lpt_cfl)
  
  return
end subroutine lpt_monitor


! ==================== !
! Last call : save all !
! ==================== !
subroutine lpt_finalize
  use lpt
  implicit none
  
  ! Check wether lpt is used
  if (.not.use_lpt) return
  
  ! Dump all the droplets to a file for restart
  call lpt_write_full3D
  
  return
end subroutine lpt_finalize


! ============================= !
! Compute and store stress term !
! ============================= !
subroutine lpt_compute_stress
  use lpt
  use data
  use metric_generic
  use metric_velocity_conv
  use metric_velocity_visc
  use memory
  implicit none
  
  integer :: i,j,k
  integer :: ii,jj,kk
  
  ! Viscous part in X
  do kk=kmin_-stv1,kmax_+stv2
     do jj=jmin_-stv1,jmax_+stv2
        do ii=imin_-stv1,imax_+stv2
           
           i = ii-1; j = jj-1; k = kk-1;
           
           FX(i,j,k) = &
                + 2.0_WP*VISC(i,j,k)*( &
                   + sum(grad_u_x(i,j,k,:)*U(i-stv1:i+stv2,j,k)) &
                   - 1.0_WP/3.0_WP*( sum(divv_u(i,j,k,:)*U(i-stv1:i+stv2,j,k)) &
                                   + sum(divv_v(i,j,k,:)*V(i,j-stv1:j+stv2,k)) &
                                   + sum(divv_w(i,j,k,:)*W(i,j,k-stv1:k+stv2))))
           
           i = ii; j = jj; k = kk;
           
           FY(i,j,k) = &
                + sum(interp_sc_xy(i,j,:,:)*VISC(i-st2:i+st1,j-st2:j+st1,k)) * &
                ( sum(grad_u_y(i,j,k,:)*U(i,j-stv2:j+stv1,k)) &
                + sum(grad_v_x(i,j,k,:)*V(i-stv2:i+stv1,j,k)) )
           
           FZ(i,j,k) = &
                + sum(interp_sc_xz(i,j,:,:)*VISC(i-st2:i+st1,j,k-st2:k+st1)) * &
                ( sum(grad_u_z(i,j,k,:)*U(i,j,k-stv2:k+stv1)) &
                + sum(grad_w_x(i,j,k,:)*W(i-stv2:i+stv1,j,k)) )
        end do
     end do
  end do
  
  ! Residual in X
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           tmp1(i,j,k) = -sum(grad_Px(i,j,k,:)*P (i-stc2:i+stc1,j,k)) &
                         +sum(divv_xx(i,j,k,:)*FX(i-stv2:i+stv1,j,k)) &
                         +sum(divv_xy(i,j,k,:)*FY(i,j-stv1:j+stv2,k)) &
                         +sum(divv_xz(i,j,k,:)*FZ(i,j,k-stv1:k+stv2)) &
                         +meanSRC(1)/dt_uvw
        end do
     end do
  end do

  ! Viscous part in Y
  do kk=kmin_-stv1,kmax_+stv2
     do jj=jmin_-stv1,jmax_+stv2
        do ii=imin_-stv1,imax_+stv2
           
           i = ii-1; j = jj-1; k = kk-1;
           
           FY(i,j,k) = &
                + 2.0_WP*VISC(i,j,k)*( &
                   + sum(grad_v_y(i,j,k,:)*V(i,j-stv1:j+stv2,k)) &
                   - 1.0_WP/3.0_WP*( sum(divv_u(i,j,k,:)*U(i-stv1:i+stv2,j,k)) &
                                   + sum(divv_v(i,j,k,:)*V(i,j-stv1:j+stv2,k)) &
                                   + sum(divv_w(i,j,k,:)*W(i,j,k-stv1:k+stv2))))
           
           Fcylv(i,j,k) = &
                + 2.0_WP*VISC(i,j,k)*( &
                   + sum(grad_w_z(i,j,k,:)*W(i,j,k-stv1:k+stv2)) &
                   - 1.0_WP/3.0_WP*( sum(divv_u(i,j,k,:)*U(i-stv1:i+stv2,j,k))  &
                                   + sum(divv_v(i,j,k,:)*V(i,j-stv1:j+stv2,k))  &
                                   + sum(divv_w(i,j,k,:)*W(i,j,k-stv1:k+stv2))) &
                   + ymi(j)*sum(interpv_cyl_v_ym(i,j,:)*V(i,j-stv1:j+stv2,k)))
           
           i = ii; j = jj; k = kk;
           
           FX(i,j,k) = &
                + sum(interp_sc_xy(i,j,:,:)*VISC(i-st2:i+st1,j-st2:j+st1,k)) * &
                ( sum(grad_u_y(i,j,k,:)*U(i,j-stv2:j+stv1,k)) &
                + sum(grad_v_x(i,j,k,:)*V(i-stv2:i+stv1,j,k)) )
           
           FZ(i,j,k) = &
                + sum(interp_sc_yz(i,j,:,:)*VISC(i,j-st2:j+st1,k-st2:k+st1)) * &
                ( sum(grad_v_z(i,j,k,:)*V(i,j,k-stv2:k+stv1)) &
                + sum(grad_w_y(i,j,k,:)*W(i,j-stv2:j+stv1,k)) &
                - yi(j)*sum(interpv_cyl_w_y(i,j,:)*W(i,j-stv2:j+stv1,k)) )
        end do
     end do
  end do
  
  ! Residual in Y
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           tmp2(i,j,k) = -sum(grad_Py(i,j,k,:)*P (i,j-stc2:j+stc1,k)) &
                         +sum(divv_yx(i,j,k,:)*FX(i-stv1:i+stv2,j,k)) &
                         +sum(divv_yy(i,j,k,:)*FY(i,j-stv2:j+stv1,k)) &
                         +sum(divv_yz(i,j,k,:)*FZ(i,j,k-stv1:k+stv2)) &
                         -yi(j)*sum(interpv_cyl_F_y(i,j,:)*Fcylv(i,j-stv2:j+stv1,k)) &
                         +meanSRC(2)/dt_uvw
        end do
     end do
  end do
  
  ! Viscous part in Z
  do kk=kmin_-stv1,kmax_+stv2
     do jj=jmin_-stv1,jmax_+stv2
        do ii=imin_-stv1,imax_+stv2
           
           i = ii-1; j = jj-1; k = kk-1;
           
           FZ(i,j,k) = &
                + 2.0_WP*VISC(i,j,k)*( &
                   + sum(grad_w_z(i,j,k,:)*W(i,j,k-stv1:k+stv2)) &
                   - 1.0_WP/3.0_WP*( sum(divv_u(i,j,k,:)*U(i-stv1:i+stv2,j,k))  &
                                   + sum(divv_v(i,j,k,:)*V(i,j-stv1:j+stv2,k))  &
                                   + sum(divv_w(i,j,k,:)*W(i,j,k-stv1:k+stv2))) &
                   + ymi(j)*sum(interpv_cyl_v_ym(i,j,:)*V(i,j-stv1:j+stv2,k)))
           
           i = ii; j = jj; k = kk;
           
           FX(i,j,k) = &
                + sum(interp_sc_xz(i,j,:,:)*VISC(i-st2:i+st1,j,k-st2:k+st1)) * &
                ( sum(grad_u_z(i,j,k,:)*U(i,j,k-stv2:k+stv1)) &
                + sum(grad_w_x(i,j,k,:)*W(i-stv2:i+stv1,j,k)) )
           
           FY(i,j,k) = &
                + sum(interp_sc_yz(i,j,:,:)*VISC(i,j-st2:j+st1,k-st2:k+st1)) * &
                ( sum(grad_v_z(i,j,k,:)*V(i,j,k-stv2:k+stv1)) &
                + sum(grad_w_y(i,j,k,:)*W(i,j-stv2:j+stv1,k)) &
                - yi(j)*sum(interpv_cyl_w_y(i,j,:)*W(i,j-stv2:j+stv1,k)) )
        end do
     end do
  end do
  
  ! Residual in Z
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           tmp3(i,j,k) = -sum(grad_Pz(i,j,k,:)*P (i,j,k-stc2:k+stc1)) &
                         +sum(divv_zx(i,j,k,:)*FX(i-stv1:i+stv2,j,k)) &
                         +sum(divv_zy(i,j,k,:)*FY(i,j-stv1:j+stv2,k)) &
                         +sum(divv_zz(i,j,k,:)*FZ(i,j,k-stv2:k+stv1)) &
                         +ymi(j)*sum(interpv_cyl_F_ym(i,j,:)*FY(i,j-stv1:j+stv2,k)) &
                         +meanSRC(3)/dt_uvw
        end do
     end do
  end do
  
  ! Update borders
  call boundary_update_border(tmp1,'+','ym')
  call boundary_update_border(tmp2,'-','y')
  call boundary_update_border(tmp3,'-','ym')
  call boundary_neumann(tmp1,'+ym')
  call boundary_neumann(tmp1,'-ym')
  call boundary_neumann(tmp1,'+x' )
  call boundary_neumann(tmp1,'-x' )
  call boundary_neumann(tmp2,'+y' )
  call boundary_neumann(tmp2,'-y' )
  call boundary_neumann(tmp2,'+xm')
  call boundary_neumann(tmp2,'-xm')
  call boundary_neumann(tmp3,'+ym')
  call boundary_neumann(tmp3,'-ym')
  call boundary_neumann(tmp3,'+xm')
  call boundary_neumann(tmp3,'-xm')
  
  ! Interpolate to cell centers
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           lpt_stress_x(i,j,k) = sum(interp_u_xm(i,j,:)*tmp1(i-st1:i+st2,j,k))
           lpt_stress_y(i,j,k) = sum(interp_v_ym(i,j,:)*tmp2(i,j-st1:j+st2,k))
           lpt_stress_z(i,j,k) = sum(interp_w_zm(i,j,:)*tmp3(i,j,k-st1:k+st2))
        end do
     end do
  end do
  
  ! Update borders
  call boundary_update_border(lpt_stress_x,'+','ym')
  call boundary_update_border(lpt_stress_y,'-','ym')
  call boundary_update_border(lpt_stress_z,'-','ym')
  call boundary_neumann(lpt_stress_x,'+ym')
  call boundary_neumann(lpt_stress_x,'-ym')
  call boundary_neumann(lpt_stress_x,'+xm')
  call boundary_neumann(lpt_stress_x,'-xm')
  call boundary_neumann(lpt_stress_y,'+ym')
  call boundary_neumann(lpt_stress_y,'-ym')
  call boundary_neumann(lpt_stress_y,'+xm')
  call boundary_neumann(lpt_stress_y,'-xm')
  call boundary_neumann(lpt_stress_z,'+ym')
  call boundary_neumann(lpt_stress_z,'-ym')
  call boundary_neumann(lpt_stress_z,'+xm')
  call boundary_neumann(lpt_stress_z,'-xm')
  
  return
end subroutine lpt_compute_stress
