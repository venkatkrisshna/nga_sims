! ==================================== !
! Exchange routines for spray tracking !
! Performs exchanges from spray to gas !
! - momentum                           !
! - if evaporation, mass               !
! ==================================== !
module lpt_source
  use geometry
  use partition
  use parallel
  use data
  use combustion
  use velocity
  use scalar
  implicit none
  !  
end module lpt_source


! ==================================== !
! Spray -> gas phase momentum exchange !
! Careful, still need to divide by vol !
! ==================================== !
subroutine lpt_source_momentum(xd,yd,zd,id,jd,kd,ud,dud,vd,dvd,wd,dwd,md,dmd,nparceld)
  use lpt_source
  use memory
  implicit none
  
  real(WP) :: xd,yd,zd,ud,vd,wd,md,nparceld
  real(WP) :: dud,dvd,dwd,dmd
  integer :: id,jd,kd
  real(WP), dimension(3) :: mom_src
  
  ! Create the source term
  mom_src(1) = -(md*dud+dmd*ud)*nparceld
  mom_src(2) = -(md*dvd+dmd*vd)*nparceld
  mom_src(3) = -(md*dwd+dmd*wd)*nparceld
  
  ! Cell-centered to facilitate mollification
  !if (nx.gt.1) call lpt_mollify_sc(tmp1,xd,yd,zd,id,jd,kd,mom_src(1))
  !if (ny.gt.1) call lpt_mollify_sc(tmp2,xd,yd,zd,id,jd,kd,mom_src(2))
  !if (nz.gt.1) call lpt_mollify_sc(tmp3,xd,yd,zd,id,jd,kd,mom_src(3))
  if (nx.gt.1) call lpt_extrapolate(tmp1,xd,yd,zd,id,jd,kd,mom_src(1))
  if (ny.gt.1) call lpt_extrapolate(tmp2,xd,yd,zd,id,jd,kd,mom_src(2))
  if (nz.gt.1) call lpt_extrapolate(tmp3,xd,yd,zd,id,jd,kd,mom_src(3))
  
  return
end subroutine lpt_source_momentum
