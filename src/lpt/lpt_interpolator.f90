! ========================================= !
! Interpolation routines for spray tracking !
! Creates the interpolation coefficients    !
! Then performs the actual interpolations:  !
! - from gaz phase to spray                 !
! - from spray back to gaz phase            !
! ========================================= !
module lpt_interpolator
  use geometry
  use partition
  use parallel
  use math
  use masks
  use compgeom
  implicit none
  ! Dummy
end module lpt_interpolator


! ============================================ !
! Actual interpolation routine                 !
! Takes in (xp,yp,zp) and (ip,jp,kp)           !
! as well as any 'big' array A                 !
! and returns Ap, interpolation of A @ p       !
! Assumes everything is available: consistency !
! checks need to be done before                !
! ============================================ !
! ------ U ------ | ------ V ------ | ------ W ------ | ------ C ------ |
! ip    - ip+1    | ip+sx - ip+sx+1 | ip+sx - ip+sx+1 | ip+sx - ip+sx+1 |
! jp+sy - jp+sy+1 | jp    - jp+1    | jp+sy - jp+sy+1 | jp+sy - jp+sy+1 |
! kp+sz - kp+sz+1 | kp+sz - kp+sz+1 | kp    - kp+1    | kp+sz - kp+sz+1 |
! --------------- | --------------- | --------------- | --------------- |
subroutine lpt_interpolate(A,xp,yp,zp,id,jd,kd,Ap,dir)
  use lpt_interpolator
  implicit none
  
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_), intent(in) :: A
  real(WP), intent(in) :: xp,yp,zp
  integer, intent(in) :: id,jd,kd
  real(WP), intent(out) :: Ap
  character(len=*) :: dir
  
  integer :: sx,sy,sz
  integer :: i1,i2
  integer :: j1,j2
  integer :: k1,k2
  real(WP), dimension(2,2,2) :: ww
  real(WP) :: wx1,wx2,wy1,wy2,wz1,wz2
  
  ! Get interpolation cells w/r to staggering
  sx = -1
  sy = -1
  sz = -1
  if (xp.ge.xm(id)) sx = 0
  if (yp.ge.ym(jd)) sy = 0
  if (zp.ge.zm(kd)) sz = 0
  
  ! Get the first interpolation point
  select case (trim(adjustl(dir)))
  case ('U')
     i1=id
     j1=jd+sy
     k1=kd+sz
  case ('V')
     i1=id+sx
     j1=jd
     k1=kd+sz
  case ('W')
     i1=id+sx
     j1=jd+sy
     k1=kd
  case ('SC')
     i1=id+sx
     j1=jd+sy
     k1=kd+sz
  end select
  
  ! Set the second interpolation point
  i2=i1+1
  j2=j1+1
  k2=k1+1
  
  ! Compute the linear interpolation coefficients
  select case (trim(adjustl(dir)))
  case ('U')
     wx1 = (x(i2) -xp    )/(x(i2) -x(i1) )
     wx2 = (xp    -x(i1) )/(x(i2) -x(i1) )
     wy1 = (ym(j2)-yp    )/(ym(j2)-ym(j1))
     wy2 = (yp    -ym(j1))/(ym(j2)-ym(j1))
     wz1 = (zm(k2)-zp    )/(zm(k2)-zm(k1))
     wz2 = (zp    -zm(k1))/(zm(k2)-zm(k1))
  case ('V')
     wx1 = (xm(i2)-xp    )/(xm(i2)-xm(i1))
     wx2 = (xp    -xm(i1))/(xm(i2)-xm(i1))
     wy1 = (y(j2) -yp    )/(y(j2) -y(j1) )
     wy2 = (yp    -y(j1) )/(y(j2) -y(j1) )
     wz1 = (zm(k2)-zp    )/(zm(k2)-zm(k1))
     wz2 = (zp    -zm(k1))/(zm(k2)-zm(k1))
  case ('W')
     wx1 = (xm(i2)-xp    )/(xm(i2)-xm(i1))
     wx2 = (xp    -xm(i1))/(xm(i2)-xm(i1))
     wy1 = (ym(j2)-yp    )/(ym(j2)-ym(j1))
     wy2 = (yp    -ym(j1))/(ym(j2)-ym(j1))
     wz1 = (z(k2) -zp    )/(z(k2) -z(k1) )
     wz2 = (zp    -z(k1) )/(z(k2) -z(k1) )
  case ('SC')
     wx1 = (xm(i2)-xp    )/(xm(i2)-xm(i1))
     wx2 = (xp    -xm(i1))/(xm(i2)-xm(i1))
     wy1 = (ym(j2)-yp    )/(ym(j2)-ym(j1))
     wy2 = (yp    -ym(j1))/(ym(j2)-ym(j1))
     wz1 = (zm(k2)-zp    )/(zm(k2)-zm(k1))
     wz2 = (zp    -zm(k1))/(zm(k2)-zm(k1))
  end select
  
  ! Combine the interpolation coefficients to form a tri-linear interpolation
  ww(1,1,1)=wx1*wy1*wz1
  ww(2,1,1)=wx2*wy1*wz1
  ww(1,2,1)=wx1*wy2*wz1
  ww(2,2,1)=wx2*wy2*wz1
  ww(1,1,2)=wx1*wy1*wz2
  ww(2,1,2)=wx2*wy1*wz2
  ww(1,2,2)=wx1*wy2*wz2
  ww(2,2,2)=wx2*wy2*wz2
  
  ! Correct for walls
  select case (trim(adjustl(dir)))
  case ('U')
     if (xper.eq.0) then
        i1=max(imin+1,i1)
        i2=i1+1
     end if
     ! Dirichlet
     if (mask_u(i1,j1).eq.1) ww(1,1,:) = 0.0_WP
     if (mask_u(i1,j2).eq.1) ww(1,2,:) = 0.0_WP
     if (mask_u(i2,j1).eq.1) ww(2,1,:) = 0.0_WP
     if (mask_u(i2,j2).eq.1) ww(2,2,:) = 0.0_WP
  case ('V')
     if (xper.eq.0) then
        i1=max(imin,i1)
        i2=i1+1
     end if
     ! Dirichlet
     if (mask_v(i1,j1).eq.1) ww(1,1,:) = 0.0_WP
     if (mask_v(i1,j2).eq.1) ww(1,2,:) = 0.0_WP
     if (mask_v(i2,j1).eq.1) ww(2,1,:) = 0.0_WP
     if (mask_v(i2,j2).eq.1) ww(2,2,:) = 0.0_WP
  case ('W')
     if (xper.eq.0) then
        i1=max(imin,i1)
        i2=i1+1
     end if
     ! Dirichlet
     if (mask_w(i1,j1).eq.1) ww(1,1,:) = 0.0_WP
     if (mask_w(i1,j2).eq.1) ww(1,2,:) = 0.0_WP
     if (mask_w(i2,j1).eq.1) ww(2,1,:) = 0.0_WP
     if (mask_w(i2,j2).eq.1) ww(2,2,:) = 0.0_WP
  case ('SC')
     if (xper.eq.0) then
        i1=max(imin,i1)
        i2=i1+1
     end if
     ! Neumann
     if (mask(i1,j1)  .eq.1) ww(1,1,:) = 0.0_WP
     if (mask(i1,j2)  .eq.1) ww(1,2,:) = 0.0_WP
     if (mask(i2,j1)  .eq.1) ww(2,1,:) = 0.0_WP
     if (mask(i2,j2)  .eq.1) ww(2,2,:) = 0.0_WP
     ww = ww/sum(ww)
  end select
  
  ! Perform the actual interpolation on A
  Ap = sum(ww*A(i1:i2,j1:j2,k1:k2))
  
  return
end subroutine lpt_interpolate


! ========================================== !
! Extrapolate particle data to Eulerian mesh !
! ========================================== !
subroutine lpt_extrapolate(A,xp,yp,zp,ip,jp,kp,Ap)
  use lpt_interpolator
  implicit none

  ! Particle data
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_), intent(out) :: A
  real(WP), intent(in) :: xp,yp,zp
  integer,  intent(in) :: ip,jp,kp
  real(WP), intent(in) :: Ap
  
  ! Interpolation coefficients
  integer :: sx,sy,sz
  integer :: i1,i2
  integer :: j1,j2
  integer :: k1,k2
  real(WP), dimension(2,2,2) :: ww
  real(WP) :: wx1,wx2,wy1,wy2,wz1,wz2
  
  ! Get interpolation cells w/r to staggering
  sx=-1
  sy=-1
  sz=-1
  if (xp.ge.xm(ip)) sx=0
  if (yp.ge.ym(jp)) sy=0
  if (zp.ge.zm(kp)) sz=0

  ! Get the first interpolation point
  i1=ip+sx
  j1=jp+sy
  k1=kp+sz
  
  ! Set the second interpolation point
  i2=i1+1
  j2=j1+1
  k2=k1+1
  
  ! Compute the linear interpolation coefficients
  wx1=(xm(i2)-xp    )/(xm(i2)-xm(i1))
  wx2=(xp    -xm(i1))/(xm(i2)-xm(i1))
  wy1=(ym(j2)-yp    )/(ym(j2)-ym(j1))
  wy2=(yp    -ym(j1))/(ym(j2)-ym(j1))
  wz1=(zm(k2)-zp    )/(zm(k2)-zm(k1))
  wz2=(zp    -zm(k1))/(zm(k2)-zm(k1))
  
  ! Combine the interpolation coefficients to form a tri-linear interpolation
  ww(1,1,1)=wx1*wy1*wz1
  ww(2,1,1)=wx2*wy1*wz1
  ww(1,2,1)=wx1*wy2*wz1
  ww(2,2,1)=wx2*wy2*wz1
  ww(1,1,2)=wx1*wy1*wz2
  ww(2,1,2)=wx2*wy1*wz2
  ww(1,2,2)=wx1*wy2*wz2
  ww(2,2,2)=wx2*wy2*wz2
  
  ! Correct for walls
  if (xper.eq.0) then
     i1=max(imin,i1)
     i2=i1+1
  end if
  if (mask(i1,j1).eq.1) ww(1,1,:)=0.0_WP
  if (mask(i1,j2).eq.1) ww(1,2,:)=0.0_WP
  if (mask(i2,j1).eq.1) ww(2,1,:)=0.0_WP
  if (mask(i2,j2).eq.1) ww(2,2,:)=0.0_WP
  ww=ww/sum(ww)
  
  ! Perform the actual extrapolation on A
  A(i1:i2,j1:j2,k1:k2)=A(i1:i2,j1:j2,k1:k2)+ww*Ap
  
  return
end subroutine lpt_extrapolate


! =================================== !
! Case periodic in y and z directions !
! =================================== !
subroutine lpt_mollify_sc(A,xp,yp,zp,ip,jp,kp,Ap)
  use lpt_interpolator
  use ib
  implicit none
  
  ! Array on which data will be extrapolated
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: A
  ! Particle position in cartesian coordinates
  real(WP) :: xp,yp,zp
  ! Cell in which particle is located  
  integer  :: ip,jp,kp
  ! Value to extrapolate on grid
  real(WP) :: Ap
  
  real(WP), dimension(-1:+1,-1:+1,-1:+1) :: ksi,ksi_image
  integer  :: di,dj,dk
  real(WP) :: d12,buf,xpi,ypi,zpi
  real(WP), dimension(3) :: n12
  
  ! If particle has left processor domain or reached last ghost cell, kill job
  if ( ip.lt.imin_-1.or.ip.gt.imax_+1.or.&
       jp.lt.jmin_-1.or.jp.gt.jmax_+1.or.&
       kp.lt.kmin_-1.or.kp.gt.kmax_+1) then
     write(*,*) ip,jp,kp,xp,yp,zp
     call die('Particle has left the domain')
  end if
  
  ! Loop over neighboring cells and get ksi
  do dk=-1,+1
     do dj=-1,+1
        do di=-1,+1
           call lpt_integrate_mol(ksi(di,dj,dk),ip+di,jp+dj,kp+dk,xp,yp,zp)
        end do
     end do
  end do
  
  ! Get image of ksi
  if (use_ib) then
     
     ! Distance from the wall
     call lpt_interpolate(Gib,xp,yp,zp,ip,jp,kp,d12,"SC")
     
     ! Compute image if close to wall
     if (abs(d12).lt.min_meshsize) then
        
	! Create normal
        call lpt_interpolate(Nxib,xp,yp,zp,ip,jp,kp,n12(1),"SC")
        call lpt_interpolate(Nyib,xp,yp,zp,ip,jp,kp,n12(2),"SC")
        call lpt_interpolate(Nzib,xp,yp,zp,ip,jp,kp,n12(3),"SC")
        buf = sqrt(sum(n12*n12))+epsilon(1.0_WP)
        n12 = n12/buf
        
	! Create image of position
        xpi=xp-2.0_WP*d12*n12(1)
        ypi=yp-2.0_WP*d12*n12(2)
        zpi=zp-2.0_WP*d12*n12(3)

        ! Loop over neighboring cells and get ksi
        do dk=-1,+1
           do dj=-1,+1
              do di=-1,+1
                 call lpt_integrate_mol(ksi_image(di,dj,dk),ip+di,jp+dj,kp+dk,xpi,ypi,zpi)
              end do
           end do
        end do
        ksi=ksi+ksi_image
        
     end if
  end if
  
  ! Loop over cells and identify walls
  do dk=-1,+1
     do dj=-1,+1
        do di=-1,+1
           if (mask(ip+di,jp+dj).eq.1 .or. mask(ip+di,jp+dj).eq.3) ksi(di,dj,dk)=0.0_WP
           if (xper.eq.0) then
              if (ip+di.lt.imin.or.ip+di.gt.imax) ksi(di,dj,dk)=0.0_WP
           end if
        end do
     end do
  end do
  
  ! Normalize
  buf = sum(ksi)
  if (buf.gt.0.0_WP) ksi = ksi/buf
  
  ! Perform the actual extrapolation on A
  A(ip-1:ip+1,jp-1:jp+1,kp-1:kp+1)=A(ip-1:ip+1,jp-1:jp+1,kp-1:kp+1)+ksi*Ap
  
  return
end subroutine lpt_mollify_sc


! ====================== !
! Mollification function !
! ====================== !
subroutine lpt_integrate_mol(fs,ic,jc,kc,xp,yp,zp)
  use lpt_interpolator
  use ib
  implicit none
  
  ! Value of integral
  real(WP), intent(out) :: fs
  ! Cell index
  integer, intent(in) :: ic,jc,kc
  ! Particle position in cartesian coordinates
  real(WP), intent(in) :: xp,yp,zp
  ! Cell barycenter
  real(WP), dimension(3) :: bar
  real(WP) :: r,val,sig
  
  ! Distance between particle and barycentre
  if (use_ib) then
     bar=Cbar(ic,jc,kc,:)
  else
     bar=(/xm(ic),ym(jc),zm(kc)/)
  end if
  r = sqrt((xp-bar(1))**2+(yp-bar(2))**2+(zp-bar(3))**2)
  
  ! Gaussian kernel
  !sig = min_meshsize/(2.0_WP*sqrt(2.0_WP*log(2.0_WP)))
  !val = exp(-r**2/(2.0_WP*sig**2))
  !fs=vol(ic,jc,kc)*val

  ! Linear kernel
  fs=max(1.5_WP*min_meshsize-r,0.0_WP)*vol(ic,jc,kc)
  
  return
end subroutine lpt_integrate_mol
