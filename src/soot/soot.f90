module soot
  use combustion
  use optdata
  use precision
  use config
  use math
  implicit none
  
  ! Size of problem
  integer, parameter :: dim      = 3
  integer, parameter :: nDeltas  = 2
  integer, parameter :: nMoments = 5
  
  ! Indices
  integer :: iod_weight0,iod_weight1
  integer :: iod_nbrC1
  integer :: iod_surf1
  integer :: iod_nbrH1
  
  ! Array for Transport and Source terms
  real(WP), dimension(:,:,:,:), pointer :: sootNEW
  real(WP), dimension(:,:,:,:), pointer :: sootMID
  real(WP), dimension(:,:,:,:), pointer :: sootOLD
  real(WP), dimension(:,:,:,:), pointer :: sootSRC
  real(WP), dimension(:,:,:,:), pointer :: sootRES
  
  ! Array of derived mean quantities
  real(WP), dimension(:,:,:), pointer :: numdens
  real(WP), dimension(:,:,:), pointer :: fv
  real(WP), dimension(:,:,:), pointer :: partdiam
  real(WP), dimension(:,:,:), pointer :: partaggr
  real(WP), dimension(:,:,:), pointer :: surfreac

  ! Total momentum (with thermophoresis)
  real(WP), dimension(:,:,:), pointer :: rhoUt
  real(WP), dimension(:,:,:), pointer :: rhoVt
  real(WP), dimension(:,:,:), pointer :: rhoWt
  
  ! Name of transport scheme for abscissas
  character(len=str_medium) :: soot_abs_scheme

  ! Values to monitor
  real(WP) :: min_N,  max_N
  real(WP) :: min_fv, max_fv
  real(WP) :: min_dp, max_dp
  real(WP) :: min_np, max_np
  real(WP) :: min_chi,max_chi

contains
  
  ! Get the indices of variables from their names
  ! ---------------------------------------------
  subroutine soot_get_indices
    implicit none
    character(len=str_short) :: name
    integer :: iod
    
    iod_weight0 = -1
    iod_weight1 = -1
    iod_nbrC1   = -1
    iod_surf1   = -1
    iod_nbrH1   = -1
    
    do iod=1,nod
       read(OD_name(iod),*) name
       if (name(1:4).eq.'SOOT') then
          select case(name(6:7))
          case ('W0')
             iod_weight0 = iod
          case ('W1')
             iod_weight1 = iod
          case ('C1')
             iod_nbrC1 = iod
          case ('S1')
             iod_surf1 = iod
          case ('H1')
             iod_nbrH1 = iod
          case default
             print*,OD_name(iod)
             call die('soot_get_indices: unknown soot variable')
          end select
       end if
    end do
    
    if (iod_weight0.eq.-1) call die('soot_get_indices: missing SOOT_W0')
    if (iod_weight1.eq.-1) call die('soot_get_indices: missing SOOT_W1')
    if (iod_nbrC1.eq.-1)   call die('soot_get_indices: missing SOOT_C1')
    if (iod_surf1.eq.-1)   call die('soot_get_indices: missing SOOT_S1')
    if (iod_nbrH1.eq.-1)   call die('soot_get_indices: missing SOOT_H1')
    
    return
  end subroutine soot_get_indices
  
  ! Compute the full momentum - Thermophoresis
  ! ------------------------------------------
  subroutine soot_full_momentum
    use metric_generic
    use masks
    implicit none
    integer :: i,j,k
    
    ! regular momentum
    rhoUt = rhoU
    rhoVt = rhoV
    rhoWt = rhoW
    
    ! Add thermophoresis
    do k=kmin_,kmax_
       do j=jmin_,jmax_
          do i=imin_,imax_
             if (mask_u(i,j).eq.0) &
                  rhoUt(i,j,k) = rhoUt(i,j,k) - 0.55_WP*sum(interp_sc_x(i,j,:)*VISC(i-st2:i+st1,j,k)) &
                  * sum(grad_x(i,j,:)*T(i-st2:i+st1,j,k)) / sum(interp_sc_x(i,j,:)*T(i-st2:i+st1,j,k))
             if (mask_v(i,j).eq.0) &
                  rhoVt(i,j,k) = rhoVt(i,j,k) - 0.55_WP*sum(interp_sc_y(i,j,:)*VISC(i,j-st2:j+st1,k)) &
                  * sum(grad_y(i,j,:)*T(i,j-st2:j+st1,k)) / sum(interp_sc_y(i,j,:)*T(i,j-st2:j+st1,k))
             if (mask_w(i,j).eq.0) &
                  rhoWt(i,j,k) = rhoWt(i,j,k) - 0.55_WP*sum(interp_sc_z(i,j,:)*VISC(i,j,k-st2:i+st1)) &
                  * sum(grad_z(i,j,:)*T(i,j,k-st2:k+st1)) / sum(interp_sc_z(i,j,:)*T(i,j,k-st2:k+st1))
          end do
       end do
    end do
    
    ! Update borders
    call boundary_update_border(rhoUt,'+','ym')
    call boundary_update_border(rhoVt,'-','y')
    call boundary_update_border(rhoWt,'-','ym')
    
    return
  end subroutine soot_full_momentum
  
end module soot


! ========================== !
! Initialize the soot module !
! ========================== !
subroutine soot_init
  use soot
  implicit none

  character(len=str_medium) :: filename
  integer :: i,j,k

  ! If not using soot model=> return
  call parser_read('Use soot model',use_soot,.false.)
  if (.not.use_soot) return

  ! Create & Start the timer
  call timing_create('soot')
  call timing_start ('soot')

  ! Allocate general arrays for Soot
  allocate(numdens (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(fv      (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(partdiam(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(partaggr(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(surfreac(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  
  ! Allocate arrays for transport equation
  allocate(sootNEW (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nMoments))
  allocate(sootMID (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nMoments))
  allocate(sootOLD (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nMoments))
  allocate(sootSRC (imin_ :imax_ ,jmin_ :jmax_ ,kmin_ :kmax_ ,nMoments))
  allocate(sootRES (imin_ :imax_ ,jmin_ :jmax_ ,kmin_ :kmax_ ,nMoments))
  
  ! Allocate arrays for full momentum
  allocate(rhoUt(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(rhoVt(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(rhoWt(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  
  ! Specific initialization
  select case(trim(chemistry))
  case ('one-step')
     ! Get the chemtable name and initialize it
     call parser_read('Soot chemtable',filename)
     call diffusion_chemtable_init(filename)
  case ('diffusion')
     ! Nothing to do
  case default
     call die('soot_source: module requires one-step or chemtable chemistry for now')
  end select
  
  ! Detect the variables names
  call soot_get_indices
  
  ! Get the values and store them locally
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
!!$           do n=1,nMoments
!!$              sootNEW(i,j,k,n) = 1.0e-30_WP
!!$           end do
           sootNEW(i,j,k,1) = OD(i,j,k,iod_weight0)
           sootNEW(i,j,k,2) = OD(i,j,k,iod_weight1)
           sootNEW(i,j,k,3) = OD(i,j,k,iod_nbrC1)
           sootNEW(i,j,k,4) = OD(i,j,k,iod_surf1)
           sootNEW(i,j,k,5) = OD(i,j,k,iod_nbrH1)
        end do
     end do
  end do
  
  ! Initialize the DQMOM part of soot
  call soot_dqmom_init
  
  ! Initialize the transport module
  call soot_bquick_init
  
  ! Create a new file to monitor at each iterations
  call monitor_create_file_step('soot',10)
  call monitor_set_header (1, 'min_N [1/cm^3]', 'r')
  call monitor_set_header (2, 'max_N [1/cm^3]', 'r')
  call monitor_set_header (3, 'min_fv','r')
  call monitor_set_header (4, 'max_fv','r')
  call monitor_set_header (5, 'min_dp [nm]','r')
  call monitor_set_header (6, 'max_dp [nm]','r')
  call monitor_set_header (7, 'min_np','r')
  call monitor_set_header (8, 'max_np','r')
  call monitor_set_header (9, 'min_Chi [10^19/m^2]','r')
  call monitor_set_header (10,'max_Chi [10^19/m^2]','r')
  
  ! Stop the timer
  call timing_stop('soot')
  
  return
end subroutine soot_init



! ===================================================== !
! PRE-TIMESTEP Routine                                  !
!                                                       !
! -> Set up the iterative process                       !
! ===================================================== !
subroutine soot_prestep
  use soot
  implicit none
  
  ! If not soot => exit
  if (.not.use_soot) return

  ! Start the timer
  call timing_start('soot')

  ! Store old values of soot quantities
  sootOLD = sootNEW

  ! Stop the timer
  call timing_stop('soot')

  return
end subroutine soot_prestep


! ========================================================== !
! ADVANCE the solution                                       !
!   -> second order in time                                  !
!   -> explicit prediction                                   !
!   -> ADI to sequentially make implicit corrections         !
! ========================================================== !
subroutine soot_step
  use soot
  implicit none
  integer  :: i,j,k,n
  integer :: nstep

  ! If not soot => exit
  if (.not.use_soot) return

  ! Start the timer
  call timing_start('soot')
  
  ! Compute the source terms
  sootSRC = 0.0_WP  
  call soot_dqmom_source
  
  ! Compute the full momentum - Thermophoresis
  call soot_full_momentum
  
  do nstep=1,2
     
     ! Compute mid soot quantities used only for transport
     sootMID = 0.5_WP*(sootNEW+sootOLD)
     
     ! Solve the transport equations
     call soot_bquick_residual
     call soot_bquick_inverse
     
     ! Update the soot quantities
     do n=1,nMoments
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 sootNEW(i,j,k,n) = sootNEW(i,j,k,n) + sootRES(i,j,k,n)
              end do
           end do
        end do
     end do
     
     ! Soot post step operations
     call soot_dqmom_poststep
  end do
  
  ! Return the values in OptData
  OD(:,:,:,iod_weight0) = sootNEW(:,:,:,1)
  OD(:,:,:,iod_weight1) = sootNEW(:,:,:,2)
  OD(:,:,:,iod_nbrC1)   = sootNEW(:,:,:,3)
  OD(:,:,:,iod_surf1)   = sootNEW(:,:,:,4)
  OD(:,:,:,iod_nbrH1)   = sootNEW(:,:,:,5)
  
  ! Stop the timer
  call timing_stop('soot')

  return
end subroutine soot_step




! ======================= !
! Monitor the soot module !
! ======================= !
subroutine soot_monitor
  use soot
  use parallel
  implicit none

  ! If not soot => exit
  if (.not.use_soot) return

  ! Compute min/max
  call parallel_max(maxval(numdens),max_N)
  call parallel_max(-minval(numdens),min_N)
  min_N = -min_N

  call parallel_max(fv,max_fv)
  call parallel_max(-fv,min_fv)
  min_fv = -min_fv

  call parallel_max(partdiam,max_dp)
  call parallel_max(-partdiam,min_dp)
  min_dp = -min_dp

  call parallel_max(partaggr,max_np)
  call parallel_max(-partaggr,min_np)
  min_np = -min_np
  
  call parallel_max(surfreac,max_chi)
  call parallel_max(-surfreac,min_chi)
  min_chi = -min_chi
  
  ! Transfer values to monitor
  call monitor_select_file('soot')
  call monitor_set_single_value(1, min_N)
  call monitor_set_single_value(2, max_N)
  call monitor_set_single_value(3, min_fv)
  call monitor_set_single_value(4, max_fv)
  call monitor_set_single_value(5, min_dp)
  call monitor_set_single_value(6, max_dp)
  call monitor_set_single_value(7, min_np)
  call monitor_set_single_value(8, max_np)
  call monitor_set_single_value(9, min_chi)
  call monitor_set_single_value(10,max_chi)
  
  return
end subroutine soot_monitor
