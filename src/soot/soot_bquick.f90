module soot_bquick
  use soot
  use data
  use metric_generic
  use scalar_quick
  implicit none

  ! ================================================= !
  ! Definition of the representation of the metrics   !
  ! ================================================= !
  ! 1D interpolation                                  !
  ! [point 1] [point 2] [point 3]            case U>0 !
  !           [point 4] [point 5] [point 6]  case U<0 !
  ! ---------------------------------> x,y,z          !
  ! ================================================= !

  
  !
  ! Use the same weights from the main quick module
  !

  ! Withint physical bounds
  integer, dimension(:,:,:), pointer :: bounded
  
  ! Coefficient for the Bounded QUICK
  real(WP), dimension(:,:,:,:), pointer :: bquick_xp,bquick_yp,bquick_zp
  real(WP), dimension(:,:,:,:), pointer :: bquick_xm,bquick_ym,bquick_zm

  ! Temporary array
  real(WP), dimension(:), pointer :: soot_
  
  ! Values to monitor
  real(WP) :: not_bounded
  
end module soot_bquick


! =========================================== !
! Initialize the metrics for the QUICK scheme !
! =========================================== !
subroutine soot_bquick_init
  use soot_bquick
  use masks
  implicit none
  
  ! Allocate arrays
  allocate(bquick_xp(imin_-st1-1:imax_+st2+1,jmin_-st1-1:jmax_+st2+1,kmin_-st1-1:kmax_+st2+1,-2: 0))
  allocate(bquick_yp(imin_-st1-1:imax_+st2+1,jmin_-st1-1:jmax_+st2+1,kmin_-st1-1:kmax_+st2+1,-2: 0))
  allocate(bquick_zp(imin_-st1-1:imax_+st2+1,jmin_-st1-1:jmax_+st2+1,kmin_-st1-1:kmax_+st2+1,-2: 0))
  allocate(bquick_xm(imin_-st1-1:imax_+st2+1,jmin_-st1-1:jmax_+st2+1,kmin_-st1-1:kmax_+st2+1,-1:+1))
  allocate(bquick_ym(imin_-st1-1:imax_+st2+1,jmin_-st1-1:jmax_+st2+1,kmin_-st1-1:kmax_+st2+1,-1:+1))
  allocate(bquick_zm(imin_-st1-1:imax_+st2+1,jmin_-st1-1:jmax_+st2+1,kmin_-st1-1:kmax_+st2+1,-1:+1))

  allocate(bounded (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  
  ! Allocate temp array
  allocate(soot_(nMoments))
  
  ! Create new file to monitor at each subiterations
  call monitor_create_file_step('soot-bquick',1)
  call monitor_set_header(1,'Not_Bounded ','i')
  
  return
end subroutine soot_bquick_init


! ==================================================== !
! Compute the BQUICK coefficients for the given scalar !
! ==================================================== !
subroutine soot_bquick_check_bounds
  use soot_bquick
  use masks
  implicit none
  
  integer  :: i,j,k
  real(WP) :: tmp
  
  bounded = 1

  ! Check if soot quantities are bounded
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           soot_ = sootRES(i,j,k,:)
           call soot_dqmom_check_bounds(soot_,bounded(i,j,k))
        end do
     end do
  end do
  call boundary_update_border_int(bounded(:,:,:),'+','ym')     
  
  ! Force bounded in walls
  do j=jmin_,jmax_
     do i=imin_,imax_
        if (mask(i,j).ne.0) bounded(i,j,:) = 1
     end do
  end do
  
  ! Count the points not bounded
  tmp = real(nx_*ny_*nz_-sum(bounded(imin_:imax_,jmin_:jmax_,kmin_:kmax_)),WP)
  call parallel_sum(tmp,not_bounded)
  
  ! Transfer values to monitor
  call monitor_select_file('soot-bquick')
  call monitor_set_single_value(1,not_bounded)
  
  return
end subroutine soot_bquick_check_bounds


! ==================================================== !
! Compute the BQUICK coefficients for the given scalar !
! ==================================================== !
subroutine soot_bquick_coeff
  use soot_bquick
  implicit none
  integer :: i,j,k
  
  ! By default, set the coefficients to quick
  do k=kmin_-st1,kmax_+st2
     do j=jmin_-st1,jmax_+st2
        do i=imin_-st1,imax_+st2
           bquick_xp(i,j,k,:) = quick_xp(i,j,:)
           bquick_yp(i,j,k,:) = quick_yp(i,j,:)
           bquick_zp(i,j,k,:) = quick_zp(i,j,:)
           bquick_xm(i,j,k,:) = quick_xm(i,j,:)
           bquick_ym(i,j,k,:) = quick_ym(i,j,:)
           bquick_zm(i,j,k,:) = quick_zm(i,j,:)
        end do
     end do
  end do
  
  ! Switch to first order if locally out of bounds
  do k=kmin_-1,kmax_+1
     do j=jmin_-1,jmax_+1
        do i=imin_-1,imax_+1
           if (bounded(i,j,k).eq.0) then
              ! Direction x - U>0
              bquick_xp(i:i+1,j,k,-2) = 0.0_WP
              bquick_xp(i:i+1,j,k,-1) = 1.0_WP
              bquick_xp(i:i+1,j,k, 0) = 0.0_WP
              ! Direction x - U<0
              bquick_xm(i:i+1,j,k,-1) = 0.0_WP
              bquick_xm(i:i+1,j,k, 0) = 1.0_WP
              bquick_xm(i:i+1,j,k,+1) = 0.0_WP
              ! Direction y - V>0
              bquick_yp(i,j:j+1,k,-2) = 0.0_WP
              bquick_yp(i,j:j+1,k,-1) = 1.0_WP
              bquick_yp(i,j:j+1,k, 0) = 0.0_WP
              ! Direction y - V<0
              bquick_ym(i,j:j+1,k,-1) = 0.0_WP
              bquick_ym(i,j:j+1,k, 0) = 1.0_WP
              bquick_ym(i,j:j+1,k,+1) = 0.0_WP
              ! Direction z - W>0
              bquick_zp(i,j,k:k+1,-2) = 0.0_WP
              bquick_zp(i,j,k:k+1,-1) = 1.0_WP
              bquick_zp(i,j,k:k+1, 0) = 0.0_WP
              ! Direction z - W<0
              bquick_zm(i,j,k:k+1,-1) = 0.0_WP
              bquick_zm(i,j,k:k+1, 0) = 1.0_WP
              bquick_zm(i,j,k:k+1,+1) = 0.0_WP
           end if
        end do
     end do
  end do
  
  return
end subroutine soot_bquick_coeff



! =========================================================== !
! Compute the residuals of the soot equations                 !
!                                                             !
! - velocity field n+1 stored in U/rhoU                       !
! - soot field n+1 stored in sootMID                          !
!                                                             !
! 3 working arrays of size (at least) (nx_+1)*(ny_+1)*(nz_+1) !
! =========================================================== !
subroutine soot_bquick_residual
  use soot_bquick
  use time_info
  use parallel
  use memory
  implicit none
  
  integer  :: i,j,k,n
  real(WP) :: rhs

  do n=1,nMoments
     do k=kmin_-st1,kmax_+st2
        do j=jmin_-st1,jmax_+st2
           do i=imin_-st1,imax_+st2
              
              FX(i,j,k) = &
                   - 0.5_WP*(rhoUt(i,j,k)+abs(rhoUt(i,j,k))) * sum(quick_xp(i,j,:)*sootMID(i-2:i  ,j,k,n)) &
                   - 0.5_WP*(rhoUt(i,j,k)-abs(rhoUt(i,j,k))) * sum(quick_xm(i,j,:)*sootMID(i-1:i+1,j,k,n))

              FY(i,j,k) = &
                   - 0.5_WP*(rhoVt(i,j,k)+abs(rhoVt(i,j,k))) * sum(quick_yp(i,j,:)*sootMID(i,j-2:j  ,k,n)) &
                   - 0.5_WP*(rhoVt(i,j,k)-abs(rhoVt(i,j,k))) * sum(quick_ym(i,j,:)*sootMID(i,j-1:j+1,k,n))

              FZ(i,j,k) = &
                   - 0.5_WP*(rhoWt(i,j,k)+abs(rhoWt(i,j,k))) * sum(quick_zp(i,j,:)*sootMID(i,j,k-2:k  ,n)) &
                   - 0.5_WP*(rhoWt(i,j,k)-abs(rhoWt(i,j,k))) * sum(quick_zm(i,j,:)*sootMID(i,j,k-1:k+1,n))
              
           end do
        end do
     end do
     
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              
              rhs =+sum(div_u(i,j,k,:)*FX(i-st1:i+st2,j,k)) &
                   +sum(div_v(i,j,k,:)*FY(i,j-st1:j+st2,k)) &
                   +sum(div_w(i,j,k,:)*FZ(i,j,k-st1:k+st2)) 
              
              sootRES(i,j,k,n) =  RHOold(i,j,k)*sootOLD(i,j,k,n) - RHO(i,j,k)*sootNEW(i,j,k,n) &
                   + dt*rhs + sootSRC(i,j,k,n)
              
              sootRES(i,j,k,n) = sootNEW(i,j,k,n) + sootRES(i,j,k,n)/RHO(i,j,k)

           end do
        end do
     end do
  end do
  
  ! Check the bounds
  call soot_bquick_check_bounds
  
  ! Compute the coefficients
  call soot_bquick_coeff

  do n=1,nMoments
     do k=kmin_-st1,kmax_+st2
        do j=jmin_-st1,jmax_+st2
           do i=imin_-st1,imax_+st2
              
              FX(i,j,k) = &
                   - 0.5_WP*(rhoUt(i,j,k)+abs(rhoUt(i,j,k))) * sum(bquick_xp(i,j,k,:)*sootMID(i-2:i  ,j,k,n)) &
                   - 0.5_WP*(rhoUt(i,j,k)-abs(rhoUt(i,j,k))) * sum(bquick_xm(i,j,k,:)*sootMID(i-1:i+1,j,k,n))

              FY(i,j,k) = &
                   - 0.5_WP*(rhoVt(i,j,k)+abs(rhoVt(i,j,k))) * sum(bquick_yp(i,j,k,:)*sootMID(i,j-2:j  ,k,n)) &
                   - 0.5_WP*(rhoVt(i,j,k)-abs(rhoVt(i,j,k))) * sum(bquick_ym(i,j,k,:)*sootMID(i,j-1:j+1,k,n))

              FZ(i,j,k) = &
                   - 0.5_WP*(rhoWt(i,j,k)+abs(rhoWt(i,j,k))) * sum(bquick_zp(i,j,k,:)*sootMID(i,j,k-2:k  ,n)) &
                   - 0.5_WP*(rhoWt(i,j,k)-abs(rhoWt(i,j,k))) * sum(bquick_zm(i,j,k,:)*sootMID(i,j,k-1:k+1,n))
              
           end do
        end do
     end do
     
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              
              rhs =+sum(div_u(i,j,k,:)*FX(i-st1:i+st2,j,k)) &
                   +sum(div_v(i,j,k,:)*FY(i,j-st1:j+st2,k)) &
                   +sum(div_w(i,j,k,:)*FZ(i,j,k-st1:k+st2)) 
              
              sootRES(i,j,k,n) =  RHOold(i,j,k)*sootOLD(i,j,k,n) - RHO(i,j,k)*sootNEW(i,j,k,n) &
                   + dt*rhs + sootSRC(i,j,k,n)
              
              sootRES(i,j,k,n) = sootRES(i,j,k,n) / RHO(i,j,k)
           end do
        end do
     end do
  end do
  
  return
end subroutine soot_bquick_residual


! =========================================================== !
! Inverse the linear system obtained from the implicit soot !
! transport equation                                          !
! =========================================================== !
subroutine soot_bquick_inverse
  use soot_bquick
  use parallel
  use memory
  use implicit
  use time_info
  implicit none
  
  integer  :: i,j,k,n
  real(WP) :: conv1,conv2,conv3,conv4
  real(WP) :: dt2
  
  dt2 = dt/2.0_WP

  ! If purely explicit return
  if (.not.implicit_any) return
  
  do n=1,nMoments
     
     ! X-direction
     if (implicit_x) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 
                 conv1 = 0.5_WP*(rhoUt(i  ,j,k) + abs(rhoUt(i  ,j,k)))
                 conv2 = 0.5_WP*(rhoUt(i  ,j,k) - abs(rhoUt(i  ,j,k)))
                 conv3 = 0.5_WP*(rhoUt(i+1,j,k) + abs(rhoUt(i+1,j,k)))
                 conv4 = 0.5_WP*(rhoUt(i+1,j,k) - abs(rhoUt(i+1,j,k)))
                 
                 Ax(j,k,i,-2) = + dt2 * div_u(i,j,k,0)*conv1*bquick_xp(i,j,k,-2)
                 
                 Ax(j,k,i,-1) = - dt2 * ( &
                      + div_u(i,j,k,0)*( - conv1*bquick_xp(i  ,j,k,-1) - conv2*bquick_xm(i  ,j,k,-1)) &
                      + div_u(i,j,k,1)*( - conv3*bquick_xp(i+1,j,k,-2)))
                 
                 Ax(j,k,i, 0) = RHO(i,j,k) - dt2 * ( &
                      + div_u(i,j,k,0)*( - conv1*bquick_xp(i  ,j,k, 0) - conv2*bquick_xm(i  ,j,k, 0)) &
                      + div_u(i,j,k,1)*( - conv3*bquick_xp(i+1,j,k,-1) - conv4*bquick_xm(i+1,j,k,-1)) )
                 
                 Ax(j,k,i,+1) = - dt2 * ( &
                      + div_u(i,j,k,0)*( - conv2*bquick_xm(i  ,j,k,+1)) &
                      + div_u(i,j,k,1)*( - conv3*bquick_xp(i+1,j,k, 0) - conv4*bquick_xm(i+1,j,k, 0)))
                 
                 Ax(j,k,i,+2) = + dt2 * div_u(i,j,k,1)*conv4*bquick_xm(i+1,j,k,+1)
                 
                 Rx(j,k,i) = RHO(i,j,k)*sootRES(i,j,k,n)
                 
              end do
           end do
        end do
        call linear_solver_x(5)
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Rx(j,k,i) = sootRES(i,j,k,n)
              end do
           end do
        end do
     end if
     
     ! Y-direction
     if (implicit_y) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 
                 conv1 = 0.5_WP*(rhoVt(i,j  ,k) + abs(rhoVt(i,j  ,k)))
                 conv2 = 0.5_WP*(rhoVt(i,j  ,k) - abs(rhoVt(i,j  ,k)))
                 conv3 = 0.5_WP*(rhoVt(i,j+1,k) + abs(rhoVt(i,j+1,k)))
                 conv4 = 0.5_WP*(rhoVt(i,j+1,k) - abs(rhoVt(i,j+1,k)))
                 
                 Ay(i,k,j,-2) = + dt2 * div_v(i,j,k,0)*conv1*bquick_yp(i,j,k,-2)
                 
                 Ay(i,k,j,-1) = - dt2*( &
                      + div_v(i,j,k,0)*( - conv1*bquick_yp(i,j  ,k,-1) - conv2*bquick_ym(i,j  ,k,-1)) &
                      + div_v(i,j,k,1)*( - conv3*bquick_yp(i,j+1,k,-2)))
                           
                 Ay(i,k,j, 0) = RHO(i,j,k) - dt2 * ( &
                      + div_v(i,j,k,0)*( - conv1*bquick_yp(i,j  ,k, 0) - conv2*bquick_ym(i,j  ,k, 0)) &
                      + div_v(i,j,k,1)*( - conv3*bquick_yp(i,j+1,k,-1) - conv4*bquick_ym(i,j+1,k,-1)) )
                 
                 Ay(i,k,j,+1) = - dt2*( &
                      + div_v(i,j,k,0)*( - conv2*bquick_ym(i,j  ,k,+1)) &
                      + div_v(i,j,k,1)*( - conv3*bquick_yp(i,j+1,k, 0) - conv4*bquick_ym(i,j+1,k,0)))
                 
                 Ay(i,k,j,+2) = dt2 * div_v(i,j,k,1)*conv4*bquick_ym(i,j+1,k,+1)
                 
                 Ry(i,k,j) = RHO(i,j,k)*Rx(j,k,i)
                 
              end do
           end do
        end do
        if (icyl.eq.1 .and. jproc.eq.1) then
           do k=kmin_,kmax_
              Ay(:,k,jmin+1,-1) = Ay(:,k,jmin+1,-1) + Ay(:,k,jmin+1,-2)
              Ay(:,k,jmin+1,-2) = 0.0_WP
              Ay(:,k,jmin, 0) = Ay(:,k,jmin, 0) + Ay(:,k,jmin,-1) + Ay(:,k,jmin,-2)
              Ay(:,k,jmin,-1) = 0.0_WP
              Ay(:,k,jmin,-2) = 0.0_WP
           end do
        end if
        call linear_solver_y(5)
     else 
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Ry(i,k,j) = Rx(j,k,i)
              end do
           end do
        end do
     end if

     ! Z-direction
     if (implicit_z) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 
                 conv1 = 0.5_WP*(rhoWt(i,j,k  ) + abs(rhoWt(i,j,k  )))
                 conv2 = 0.5_WP*(rhoWt(i,j,k  ) - abs(rhoWt(i,j,k  )))
                 conv3 = 0.5_WP*(rhoWt(i,j,k+1) + abs(rhoWt(i,j,k+1)))
                 conv4 = 0.5_WP*(rhoWt(i,j,k+1) - abs(rhoWt(i,j,k+1)))
                 
                 Az(i,j,k,-2) = + dt2 * div_w(i,j,k,0)*conv1*bquick_zp(i,j,k, -2)

                 Az(i,j,k,-1) = - dt2 * ( &
                      + div_w(i,j,k,0)*( - conv1*bquick_zp(i,j,k  ,-1) - conv2*bquick_zm(i,j,k,-1)) &
                      + div_w(i,j,k,1)*( - conv3*bquick_zp(i,j,k+1,-2)))

                 Az(i,j,k, 0) = RHO(i,j,k) - dt2 * ( &
                      + div_w(i,j,k,0)*( - conv1*bquick_zp(i,j,k  , 0) - conv2*bquick_zm(i,j,k  , 0)) &
                      + div_w(i,j,k,1)*( - conv3*bquick_zp(i,j,k+1,-1) - conv4*bquick_zm(i,j,k+1,-1)) )

                 Az(i,j,k,+1) = - dt2 * ( &
                      + div_w(i,j,k,0)*( - conv2*bquick_zm(i,j,k  ,+1)) &
                      + div_w(i,j,k,1)*( - conv3*bquick_zp(i,j,k+1, 0) - conv4*bquick_zm(i,j,k+1, 0)))

                 Az(i,j,k,+2) = + dt2 * div_w(i,j,k,1)*conv4*bquick_zm(i,j,k+1,+1)
                 
                 Rz(i,j,k) = RHO(i,j,k)*Ry(i,k,j)
                 
              end do
           end do
        end do
        call linear_solver_z(5)
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 sootRES(i,j,k,n) = Rz(i,j,k)
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 sootRES(i,j,k,n) = Ry(i,k,j)
              end do
           end do
        end do
     end if
     
  end do
  
  return
end subroutine soot_bquick_inverse
