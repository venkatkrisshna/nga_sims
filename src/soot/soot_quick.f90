module soot_quick
  use soot
  use data
  use metric_generic
  use scalar_quick
  implicit none

  ! ================================================= !
  ! Definition of the representation of the metrics   !
  ! ================================================= !
  ! 1D interpolation                                  !
  ! [point 1] [point 2] [point 3]            case U>0 !
  !           [point 4] [point 5] [point 6]  case U<0 !
  ! ---------------------------------> x,y,z          !
  ! ================================================= !

  
  !
  ! Use the same weights from the main quick module
  !

end module soot_quick


! =========================================== !
! Initialize the metrics for the QUICK scheme !
! =========================================== !
subroutine soot_quick_init
  use soot_quick
  use masks
  implicit none
  
  ! Nothing to do so far
  
  return
end subroutine soot_quick_init


! =========================================================== !
! Compute the residuals of the soot equations               !
!                                                             !
! - velocity field n+1 stored in U/rhoU                       !
! - soot field n+1 stored in SCmid                          !
!                                                             !
! 3 working arrays of size (at least) (nx_+1)*(ny_+1)*(nz_+1) !
! =========================================================== !
subroutine soot_quick_residual
  use soot_quick
  use time_info
  use parallel
  use memory
  implicit none
  
  integer  :: i,j,k,n
  real(WP) :: rhs

  do n=1,nMoments
     do k=kmin_-st1,kmax_+st2
        do j=jmin_-st1,jmax_+st2
           do i=imin_-st1,imax_+st2
              
              FX(i,j,k) = &
                   - 0.5_WP*(U(i,j,k)+abs(U(i,j,k))) * sum(quick_xp(i,j,:)*sootMID(i-2:i  ,j,k,n)) &
                   - 0.5_WP*(U(i,j,k)-abs(U(i,j,k))) * sum(quick_xm(i,j,:)*sootMID(i-1:i+1,j,k,n))

              FY(i,j,k) = &
                   - 0.5_WP*(V(i,j,k)+abs(V(i,j,k))) * sum(quick_yp(i,j,:)*sootMID(i,j-2:j  ,k,n)) &
                   - 0.5_WP*(V(i,j,k)-abs(V(i,j,k))) * sum(quick_ym(i,j,:)*sootMID(i,j-1:j+1,k,n))

              FZ(i,j,k) = &
                   - 0.5_WP*(W(i,j,k)+abs(W(i,j,k))) * sum(quick_zp(i,j,:)*sootMID(i,j,k-2:k  ,n)) &
                   - 0.5_WP*(W(i,j,k)-abs(W(i,j,k))) * sum(quick_zm(i,j,:)*sootMID(i,j,k-1:k+1,n))
              
           end do
        end do
     end do
     
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              
              rhs =+sum(div_u(i,j,k,:)*FX(i-st1:i+st2,j,k)) &
                   +sum(div_v(i,j,k,:)*FY(i,j-st1:j+st2,k)) &
                   +sum(div_w(i,j,k,:)*FZ(i,j,k-st1:k+st2)) 
              
              sootRES(i,j,k,n) =  sootOLD(i,j,k,n) - sootNEW(i,j,k,n) &
                   + dt*rhs + sootSRC(i,j,k,n)
           end do
        end do
     end do
  end do
  
  return
end subroutine soot_quick_residual


! =========================================================== !
! Inverse the linear system obtained from the implicit soot !
! transport equation                                          !
! =========================================================== !
subroutine soot_quick_inverse
  use soot_quick
  use parallel
  use memory
  use implicit
  use time_info
  implicit none
  
  integer  :: i,j,k,n
  real(WP) :: conv1,conv2,conv3,conv4
  real(WP) :: dt2
  
  dt2 = dt/2.0_WP

  ! If purely explicit return
  if (.not.implicit_any) return
  
  do n=1,nMoments
     
     ! X-direction
     if (implicit_x) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 
                 conv1 = 0.5_WP*(U(i  ,j,k) + abs(U(i  ,j,k)))
                 conv2 = 0.5_WP*(U(i  ,j,k) - abs(U(i  ,j,k)))
                 conv3 = 0.5_WP*(U(i+1,j,k) + abs(U(i+1,j,k)))
                 conv4 = 0.5_WP*(U(i+1,j,k) - abs(U(i+1,j,k)))
                 
                 Ax(j,k,i,-2) = + dt2 * div_u(i,j,k,0)*conv1*quick_xp(i,j,-2)
                 
                 Ax(j,k,i,-1) = - dt2 * ( &
                      + div_u(i,j,k,0)*( - conv1*quick_xp(i  ,j,-1)   - conv2*quick_xm(i,j,-1)) &
                      + div_u(i,j,k,1)*( - conv3*quick_xp(i+1,j,-2)))
                 
                 Ax(j,k,i, 0) = 1.0_WP - dt2 * ( &
                      + div_u(i,j,k,0)*( - conv1*quick_xp(i  ,j, 0) - conv2*quick_xm(i  ,j, 0)) &
                      + div_u(i,j,k,1)*( - conv3*quick_xp(i+1,j,-1) - conv4*quick_xm(i+1,j,-1)) )
                 
                 Ax(j,k,i,+1) = - dt2 * ( &
                      + div_u(i,j,k,0)*( - conv2*quick_xm(i  ,j,+1)) &
                      + div_u(i,j,k,1)*( - conv3*quick_xp(i+1,j, 0) - conv4*quick_xm(i+1,j, 0)))
                 
                 Ax(j,k,i,+2) = + dt2 * div_u(i,j,k,1)*conv4*quick_xm(i+1,j,+1)
                 
                 Rx(j,k,i) = sootRES(i,j,k,n)
                 
              end do
           end do
        end do
        call linear_solver_x(5)
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Rx(j,k,i) = sootRES(i,j,k,n)
              end do
           end do
        end do
     end if
     
     ! Y-direction
     if (implicit_y) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 
                 conv1 = 0.5_WP*(V(i,j  ,k) + abs(V(i,j  ,k)))
                 conv2 = 0.5_WP*(V(i,j  ,k) - abs(V(i,j  ,k)))
                 conv3 = 0.5_WP*(V(i,j+1,k) + abs(V(i,j+1,k)))
                 conv4 = 0.5_WP*(V(i,j+1,k) - abs(V(i,j+1,k)))
                 
                 Ay(i,k,j,-2) = + dt2 * div_v(i,j,k,0)*conv1*quick_yp(i,j,-2)
                 
                 Ay(i,k,j,-1) = - dt2*( &
                      + div_v(i,j,k,0)*( - conv1*quick_yp(i,j  ,-1)   - conv2*quick_ym(i,j,-1)) &
                      + div_v(i,j,k,1)*( - conv3*quick_yp(i,j+1,-2)))
                           
                 Ay(i,k,j, 0) = 1.0_WP - dt2 * ( &
                      + div_v(i,j,k,0)*( - conv1*quick_yp(i,j  , 0) - conv2*quick_ym(i,j  , 0)) &
                      + div_v(i,j,k,1)*( - conv3*quick_yp(i,j+1,-1) - conv4*quick_ym(i,j+1,-1)) )
                 
                 Ay(i,k,j,+1) = - dt2*( &
                      + div_v(i,j,k,0)*( - conv2*quick_ym(i,j  ,+1)) &
                      + div_v(i,j,k,1)*( - conv3*quick_yp(i,j+1, 0) - conv4*quick_ym(i,j+1,0)))
                 
                 Ay(i,k,j,+2) = dt2 * div_v(i,j,k,1)*conv4*quick_ym(i,j+1,+1)
                 
                 Ry(i,k,j) = Rx(j,k,i)
                 
              end do
           end do
        end do
        if (icyl.eq.1 .and. jproc.eq.1) then
           do k=kmin_,kmax_
              Ay(:,k,jmin+1,-1) = Ay(:,k,jmin+1,-1) + Ay(:,k,jmin+1,-2)
              Ay(:,k,jmin+1,-2) = 0.0_WP
              Ay(:,k,jmin, 0) = Ay(:,k,jmin, 0) + Ay(:,k,jmin,-1) + Ay(:,k,jmin,-2)
              Ay(:,k,jmin,-1) = 0.0_WP
              Ay(:,k,jmin,-2) = 0.0_WP
           end do
        end if
        call linear_solver_y(5)
     else 
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Ry(i,k,j) = Rx(j,k,i)
              end do
           end do
        end do
     end if

     ! Z-direction
     if (implicit_z) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 
                 conv1 = 0.5_WP*(W(i,j,k  ) + abs(W(i,j,k  )))
                 conv2 = 0.5_WP*(W(i,j,k  ) - abs(W(i,j,k  )))
                 conv3 = 0.5_WP*(W(i,j,k+1) + abs(W(i,j,k+1)))
                 conv4 = 0.5_WP*(W(i,j,k+1) - abs(W(i,j,k+1)))
                 
                 Az(i,j,k,-2) = + dt2 * div_w(i,j,k,0)*conv1*quick_zp(i,j, -2)

                 Az(i,j,k,-1) = - dt2 * ( &
                      + div_w(i,j,k,0)*( - conv1*quick_zp(i,j,-1) - conv2*quick_zm(i,j,-1)) &
                      + div_w(i,j,k,1)*( - conv3*quick_zp(i,j,-2)))

                 Az(i,j,k, 0) = 1.0_WP - dt2 * ( &
                      + div_w(i,j,k,0)*( - conv1*quick_zp(i,j, 0) - conv2*quick_zm(i,j, 0)) &
                      + div_w(i,j,k,1)*( - conv3*quick_zp(i,j,-1) - conv4*quick_zm(i,j,-1)) )

                 Az(i,j,k,+1) = - dt2 * ( &
                      + div_w(i,j,k,0)*( - conv2*quick_zm(i,j,+1)) &
                      + div_w(i,j,k,1)*( - conv3*quick_zp(i,j, 0) - conv4*quick_zm(i,j, 0)))

                 Az(i,j,k,+2) = + dt2 * div_w(i,j,k,1)*conv4*quick_zm(i,j,+1)
                 
                 Rz(i,j,k) = Ry(i,k,j)
                 
              end do
           end do
        end do
        call linear_solver_z(5)
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 sootRES(i,j,k,n) = Rz(i,j,k)
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 sootRES(i,j,k,n) = Ry(i,k,j)
              end do
           end do
        end do
     end if
     
  end do
  
  return
end subroutine soot_quick_inverse
