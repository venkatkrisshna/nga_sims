module soot_dqmom
  use soot
  implicit none

  ! ------------ Constants ------------

  ! Soot relevant constants
  real(WP), parameter :: SootDensity = 1800.0_WP    ! kg/m^3
  real(WP), parameter :: MolarMassSoot = 12.0e-3_WP ! kg/mol
  real(WP), parameter :: Avogadro = 6.022e23_WP     ! 1/mol
  real(WP), parameter :: Rgas = 8.314_WP            ! J/mol/K
  real(WP), parameter :: Df = 1.8_WP                ! 1

  ! Derived constants
  real(WP) :: CarbonToDiam
  real(WP) :: Cred
  real(WP) :: expDf

  ! Clipping for DQMOM
  real(WP), parameter :: NUCL_NBRC = 32.0_WP
  real(WP), parameter :: NUCL_NBRH = 20.0_WP
  real(WP), parameter :: SMALLWEIGHT = 1.0e-20_WP
  real(WP), parameter :: SMALLSEP    = 1.1_WP
  real(WP), parameter :: BIG_NP = 1.0e4_WP
  
  ! --------- Source terms --------
  logical :: inucleation
  logical :: icoagulation
  logical :: icondensation
  logical :: isurface
  
  ! ----------  Local environment -----------

  ! Relevant quantities to soot formation
  real(WP) :: DIMER_conc
  real(WP) :: temp_,rho_,visc_,Wmol_

  ! Parameter for the chemtable look-ups
  real(WP) :: table_x1,table_x2,table_x3

  ! ------------- DQMOM -------------

  ! DQMOM moments
  real(WP), dimension(:),   pointer :: frac
  real(WP), dimension(:),   pointer :: kmom
  real(WP), dimension(:,:), pointer :: moments

  ! DQMOM quantities
  real(WP), dimension(:), pointer :: weight
  real(WP), dimension(:), pointer :: nbrC
  real(WP), dimension(:), pointer :: surf
  real(WP), dimension(:), pointer :: nbrH
  real(WP), dimension(:), pointer :: dp
  real(WP), dimension(:), pointer :: np

  ! DQMOM src terms and matrix
  real(WP), dimension(:,:), pointer :: matrix
  real(WP), dimension(:),   pointer :: src_mom
  real(WP), dimension(:),   pointer :: src_dqmom

  ! Time integration

  ! Maximum rate of change we accept to be explicit
  real(WP), parameter :: rate_max = 0.1_WP

  ! working arrays
  real(WP), dimension(:), pointer :: sol
  
  ! Quantities to monitor
  real(WP) :: soot_niter
  
contains

  ! Create the monents
  ! ------------------
  subroutine soot_define_moments
    implicit none

    allocate(moments(nMoments,dim))

    ! Total number
    moments(1,1) = 0.0_WP
    moments(1,2) = 0.0_WP
    moments(1,3) = 0.0_WP
    ! Total nbrC
    moments(2,1) = 1.0_WP
    moments(2,2) = 0.0_WP
    moments(2,3) = 0.0_WP
    ! Total surf
    moments(3,1) = 0.0_WP
    moments(3,2) = 1.0_WP
    moments(3,3) = 0.0_WP
    ! Total nbrH
    moments(4,1) = 0.0_WP
    moments(4,2) = 0.0_WP
    moments(4,3) = 1.0_WP

    ! Medium moment for first peak
    moments(5,1) = 0.5_WP
    moments(5,2) = 0.0_WP
    moments(5,3) = 0.0_WP

    return
  end subroutine soot_define_moments

  ! Compute the fractional moment
  ! -----------------------------
  subroutine soot_compute_moment(moment)
    use soot
    implicit none
    real(WP), intent(out) :: moment
    integer :: n

    moment = 0.0_WP
    do n=1,nDeltas
       moment = moment + weight(n) * nbrC(n)**frac(1) * surf(n)**frac(2) * nbrH(n)**frac(3)
    end do

    return
  end subroutine soot_compute_moment

  ! Compute Knudsen number
  ! ----------------------
  function getKnudsen(vi,ai)
    use soot
    use math
    implicit none
    real(WP), intent(in) :: vi,ai
    real(WP) :: getKnudsen
    real(WP) :: ri,ni,rci
    
    ri = vi/ai
    ni = ai/(ri*ri)
    rci = ri * ni**(1.0_WP/Df)
    
    getKnudsen = 3.0*visc_/rho_*sqrt(Pi*Wmol_/(8.0_WP*Rgas*temp_))
    
    return
  end function getKnudsen
  
  ! Compute the collision coefficients
  ! ----------------------------------
  function soot_beta_nucl()
    use soot
    implicit none
    real(WP) :: soot_beta_nucl

    ! 2.2_WP * 4.0_WP * sqrt(2.0_WP) = 12.45
    soot_beta_nucl = 12.45_WP * Cred * sqrt(temp_) * NUCL_NBRC**(1.0_WP/6.0_WP)

    return
  end function soot_beta_nucl

  function soot_beta_coag(vi,vj,ai,aj)
    use soot
    implicit none
    real(WP) :: soot_beta_coag
    real(WP), intent(in) :: vi,vj
    real(WP), intent(in) :: ai,aj
    real(WP) :: ri,ni,rci,Ci
    real(WP) :: rj,nj,rcj,Cj
    real(WP) :: beta_fm,beta_cont
    
    ri = vi/ai
    rj = vj/aj

    ni = ai/(ri*ri)
    nj = aj/(rj*rj)
    
    rci = ri * ni**(1.0_WP/Df)
    rcj = rj * nj**(1.0_WP/Df)
    
    ! Free-Molecular regime
    beta_fm = 2.2_WP * Cred * sqrt(temp_) * (rci+rcj)**2 * sqrt(1.0_WP/vi+1.0_WP/vj)
    
    ! Continuum regime
    Ci = 1.0_WP + 1.257_WP*getKnudsen(vi,ai)
    Cj = 1.0_WP + 1.257_WP*getKnudsen(vj,aj)
    beta_cont = 2.667_WP * Rgas*temp_/visc_ * (Ci/rci+Cj/rcj)*(rci+rcj)
    
    soot_beta_coag = beta_fm*beta_cont/(beta_fm+beta_cont)

    return
  end function soot_beta_coag

  function soot_beta_cond(vi,ai)
    use soot
    implicit none
    real(WP) :: soot_beta_cond
    real(WP), intent(in) :: vi,ai
    real(WP) :: ri,ni,rci,Ci
    real(WP) :: rj,nj,rcj,Cj
    real(WP) :: aj,vj
    real(WP) :: beta_fm,beta_cont
    
    vj = NUCL_NBRC
    aj = vj**(2.0_WP/3.0_WP)
    
    ri = vi/ai
    rj = vj/aj

    ni = ai/(ri*ri)
    nj = 1.0_WP
    
    rci = ri * ni**(1.0_WP/Df)
    rcj = rj

    ! Free-Molecular regime
    beta_fm = Cred * sqrt(temp_) * (rci+rcj)**2 * sqrt(1.0_WP/vi+1.0_WP/vj)
    
    ! Continuum regime
    Ci = 1.0_WP + 1.257_WP*getKnudsen(vi,ai)
    Cj = 1.0_WP + 1.257_WP*getKnudsen(vj,aj)
    beta_cont = 2.667_WP * Rgas*temp_/visc_ * (Ci/rci+Cj/rcj)*(rci+rcj)
    
    soot_beta_cond = beta_fm*beta_cont/(beta_fm+beta_cont)

    return
  end function soot_beta_cond


  ! Compute Nucleation source term for moment k
  ! -------------------------------------------
  subroutine soot_nucleation(k,src)
    use soot
    implicit none

    real(WP), dimension(dim), intent(in) :: k
    real(WP), intent(out) :: src

    ! If no nucleation, return
    src = 0.0_WP
    if (.not.inucleation) return 
    
    src = 0.5_WP * soot_beta_nucl() * DIMER_conc**2 &
         * (2.0_WP*NUCL_NBRC)**k(1) &
         * (2.0_WP*NUCL_NBRC)**(2.0_WP/3.0_WP*k(2)) &
         * (2.0_WP*NUCL_NBRH)**k(3) 
    
    return
  end subroutine soot_nucleation

  ! Compute Coagulation source term for moment k
  ! --------------------------------------------
  subroutine soot_coagulation(k,src)
    use soot
    implicit none

    real(WP), dimension(dim), intent(in) :: k
    real(WP), intent(out) :: src
    real(WP) :: new_nbrC,new_surf,new_nbrH
    real(WP) :: dV_V,dS_S
    integer  :: n,q
    
    ! If no coagulation, return
    src = 0.0_WP
    if (.not.icoagulation) return
    
    do n=1,nDeltas
       do q=1,nDeltas
          
          if (n.eq.q) then
             new_surf = surf(n)+surf(q)
          else
             if (nbrC(n).gt.nbrC(q)) then
                dV_V = nbrC(q)/nbrC(n)
                dS_S = dV_V * &
                     (1.0_WP-2.0_WP/Df-np(n)**(-1.0_WP/Df)) / &
                     (1.0_WP-3.0_WP/Df-np(n)**(-1.0_WP/Df))
                new_surf = surf(n)*(1.0_WP+dS_S)
             else
                dV_V = nbrC(n)/nbrC(q)
                dS_S = dV_V * &
                     (1.0_WP-2.0_WP/Df-np(q)**(-1.0_WP/Df)) / &
                     (1.0_WP-3.0_WP/Df-np(q)**(-1.0_WP/Df))
                new_surf = surf(q)*(1.0_WP+dS_S)
             end if
          end if
          
          new_nbrC = nbrC(n)+nbrC(q)
          new_nbrH = new_surf * (nbrH(n)+nbrH(q))/(surf(n)+surf(q))
          
          src = src + 0.5_WP * soot_beta_coag(nbrC(n),nbrC(q),surf(n),surf(q)) * &
               weight(n) * weight(q) * &
               ( new_nbrC**k(1) * new_surf**k(2) * new_nbrH**k(3) &
               - nbrC(n) **k(1) * surf(n) **k(2) * nbrH(n) **k(3) &
               - nbrC(q) **k(1) * surf(q) **k(2) * nbrH(q) **k(3) )
       end do
    end do

    return
  end subroutine soot_coagulation

  ! Compute Condensation source term for moment k
  ! ---------------------------------------------
  subroutine soot_condensation(k,src)
    use soot
    implicit none

    real(WP), dimension(dim), intent(in) :: k
    real(WP), intent(out) :: src
    real(WP) :: dV_V,dS_S,dH_H,rate
    integer  :: n

    ! If no condensation, return
    src = 0.0_WP
    if (.not.icondensation) return
    
    do n=1,nDeltas
       
       dV_V = + NUCL_NBRC / nbrC(n)
       dS_S = dV_V * &
            (1.0_WP-2.0_WP/Df-np(n)**(-1.0_WP/Df)) / &
            (1.0_WP-3.0_WP/Df-np(n)**(-1.0_WP/Df))
       dH_H = + NUCL_NBRH / nbrH(n)
       
       rate = weight(n) * DIMER_conc * soot_beta_cond(nbrc(n),surf(n))
       
       src = src + rate * ( k(1)*dV_V + k(2)*dS_S + k(3)*dH_H ) * &
            nbrC(n)**k(1) * surf(n)**k(2) * nbrH(n)**k(3)
    end do

    return
  end subroutine soot_condensation

  ! Compute Surface Reaction source term for moment k
  ! -------------------------------------------------
  subroutine soot_surfacereaction(k,src)
    use soot
    implicit none

    real(WP), dimension(dim), intent(in) :: k
    real(WP), intent(out) :: src
    real(WP) :: dV_V,dS_S,dH_H
    real(WP) :: wCoeff
    integer  :: n
    
    ! If no surface growth, return
    src = 0.0_WP
    if (.not.isurface) return
    
    call diffusion_chemtable_lookup('SurfRateCoeff',wCoeff,table_x1,table_x2,table_x3,1)
    
    do n=1,nDeltas
       
       dV_V = + 2.0_WP  / nbrC(n)
       dS_S = dV_V * &
            (1.0_WP-2.0_WP/Df-np(n)**(-1.0_WP/Df)) / &
            (1.0_WP-3.0_WP/Df-np(n)**(-1.0_WP/Df))
       dH_H = + 0.25_WP / nbrH(n)
       
       src = src + wCoeff * weight(n) * nbrH(n) * &
            ( k(1)*dV_V + k(2)*dS_S + k(3)*dH_H ) * &
            nbrC(n)**k(1) * surf(n)**k(2) * nbrH(n)**k(3)
    end do
    
    return
  end subroutine soot_surfacereaction


  ! Compute PAH concentration
  ! -------------------------
  subroutine soot_pah_molecules
    use soot
    implicit none
    real(WP) :: DIMER_nbrC,prodRate
    real(WP) :: betaN,betaC,delta
    integer :: n

    ! Get pah structure
    call diffusion_chemtable_lookup('Dimer_nbrC',DIMER_nbrC,table_x1,table_x2,table_x3,1)
    call diffusion_chemtable_lookup('Dimer_ProdRate',prodRate,table_x1,table_x2,table_x3,1)
    
    ! Rescale to conserve mass flux
    prodRate = prodRate * DIMER_nbrC/NUCL_NBRC
    
    ! Nucleation collision coeff
    betaN = soot_beta_nucl()

    ! Condensation collision coeff
    betaC = 0.0_WP
    if (icondensation) then
       do n=1,nDeltas
          betaC = betaC + soot_beta_cond(nbrC(n),surf(n)) * weight(n)
       end do
    end if
    
    ! Solve quadratic equation
    delta = betaC**2 + 4.0_WP*betaN*prodRate
    DIMER_conc = (sqrt(delta)-betaC)/(2.0_WP*betaN)

    return
  end subroutine soot_pah_molecules

  ! Compute the source terms for moments
  ! ------------------------------------
  subroutine soot_dqmom_src_mom
    use soot
    implicit none
    integer :: i
    real(WP) :: src_tmp
    
    do i=1,nMoments
       kmom = moments(i,:)
       src_mom(i) = 0.0_WP
       ! Nucleation
       call soot_nucleation(kmom,src_tmp)
       src_mom(i) = src_mom(i) + src_tmp
       ! Coagulation
       call soot_coagulation(kmom,src_tmp)
       src_mom(i) = src_mom(i) + src_tmp
       ! Condensation
       call soot_condensation(kmom,src_tmp)
       src_mom(i) = src_mom(i) + src_tmp
       ! Surface Reaction
       call soot_surfacereaction(kmom,src_tmp)
       src_mom(i) = src_mom(i) + src_tmp
    end do
    
    return
  end subroutine soot_dqmom_src_mom

  ! Compute the matrix for mom<->dqmom conversion
  ! ---------------------------------------------
  subroutine soot_dqmom_matrix
    use soot
    implicit none
    integer :: i

    do i=1,nMoments
       kmom = moments(i,:)
       ! First peak - fixed abscissa
       matrix(i,1) = nbrC(1)**kmom(1) * surf(1)**kmom(2) * nbrH(1)**kmom(3)
       ! Second peak - free to move
       matrix(i,2) = (1.0_WP-kmom(1)-kmom(2)-kmom(3)) * &
            nbrC(2)**kmom(1) * surf(2)**kmom(2) * nbrH(2)**kmom(3)
       matrix(i,3) = kmom(1) * &
            nbrC(2)**(kmom(1)-1.0_WP) * surf(2)**kmom(2) * nbrH(2)**kmom(3)
       matrix(i,4) = kmom(2) * &
            nbrC(2)**kmom(1) * surf(2)**(kmom(2)-1.0_WP) * nbrH(2)**kmom(3)
       matrix(i,5) = kmom(3) * &
            nbrC(2)**kmom(1) * surf(2)**kmom(2) * nbrH(2)**(kmom(3)-1.0_WP)
    end do
    
    return
  end subroutine soot_dqmom_matrix
  
  ! Inverse the source terms for dqmom
  ! ----------------------------------
  subroutine soot_dqmom_src_dqmom
    use soot
    implicit none
    
    call solve_linear_system(matrix,src_mom,src_dqmom,nMoments)
    
    return
  end subroutine soot_dqmom_src_dqmom
  
  ! Compute the source terms
  ! d(M_n)/dt = S_n
  ! ------------------------
  subroutine soot_dqmom_compute_src(sol)
    use soot
    implicit none
    real(WP), dimension(nMoments) :: sol
    
    ! Get back the weights and abscissas
    weight(1) = sol(1)
    weight(2) = sol(2)
    nbrC(2)   = sol(3) / max(sol(2),SMALLWEIGHT)
    surf(2)   = sol(4) / max(sol(2),SMALLWEIGHT)
    nbrH(2)   = sol(5) / max(sol(2),SMALLWEIGHT)
    
    ! Compute related quantities
    dp = nbrC/surf
    np = surf**3/nbrC**2
    
    ! Compute PAH molecules
    call soot_pah_molecules
    
    ! Compute and inverse source terms
    call soot_dqmom_src_mom
    call soot_dqmom_matrix
    call soot_dqmom_src_dqmom
    
    return
  end subroutine soot_dqmom_compute_src
  
  ! Apply boundary conditions on a scalar soot field
  ! ------------------------------------------------
  subroutine soot_dqmom_apply_bc(S)
    implicit none
    real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nMoments) ::S
    integer :: i,j,n
    
    ! Update the ghost cells
    do n=1,nMoments
       call boundary_update_border(S(:,:,:,n),'+','ym')
    end do
    
    ! Enforce default values for left BC
    if (xper.eq.0 .and. iproc.eq.1) then
       do i=imino,imin-1
          ! First peak
          S(i,:,:,1) = SMALLWEIGHT
          ! Second peak
          S(i,:,:,2) = SMALLWEIGHT
          S(i,:,:,3) = SMALLWEIGHT * SMALLSEP * nbrC(1)
          S(i,:,:,4) = SMALLWEIGHT * SMALLSEP * surf(1)
          S(i,:,:,5) = SMALLWEIGHT * SMALLSEP * nbrH(1)
       end do
    end if
    
    ! Enforce Newmann condition for Outflow and Open Boundaries
    if (xper.eq.0 .and. iproc.eq.npx) then
       do i=imax+1,imaxo
          S(i,:,:,:) = S(imax,:,:,:)
       end do
    end if
    if (yper.eq.0 .and. jproc.eq.1 .and. icyl.eq.0) then
       do j=jmino,jmin-1
          S(:,j,:,:) = S(:,jmin,:,:)
       end do
    end if
    if (yper.eq.0 .and. jproc.eq.npy) then
       do j=jmax+1,jmaxo
          S(:,j,:,:) = S(:,jmax,:,:)
       end do
    end if
    
    return
  end subroutine soot_dqmom_apply_bc
  
end module soot_dqmom


! ====================================== !
! INITIALIZE the dqmom submodule of soot !
! ====================================== !
subroutine soot_dqmom_init
  use soot_dqmom
  use parser
  use masks
  implicit none
  integer  :: i,j,k
  
  ! Define some parameters
  CarbonToDiam = (6.0_WP*MolarMassSoot/(Pi*SootDensity*Avogadro))**(1.0_WP/3.0_WP)
  Cred = sqrt(0.5_WP*Pi*Rgas/MolarMassSoot) * CarbonToDiam**2 * Avogadro
  expDf = 1.0_WP/Df - 1.0_WP/3.0_WP

  ! Allocate arrays for DQMOM
  allocate(frac(dim))
  allocate(kmom(dim))
  allocate(weight(nDeltas))
  allocate(nbrC  (nDeltas))
  allocate(surf  (nDeltas))
  allocate(nbrH  (nDeltas))
  allocate(dp    (nDeltas))
  allocate(np    (nDeltas))
  allocate(matrix   (nMoments,nMoments))
  allocate(src_mom  (nMoments))
  allocate(src_dqmom(nMoments))
  allocate(sol      (nMoments))
  
  ! Define the momenst used for DQMOM
  call soot_define_moments
  
  ! Read the physical processes to consider
  call parser_read('Nucleation',     inucleation)
  call parser_read('Condensation',   icondensation)
  call parser_read('Coagulation',    icoagulation)
  call parser_read('Surface growth', isurface)
  
  ! Force location of first peak
  nbrC(1) =  2.0_WP*NUCL_NBRC
  surf(1) = (2.0_WP*NUCL_NBRC)**(2.0_WP/3.0_WP)
  nbrH(1) =  2.0_WP*NUCL_NBRH
  
  ! If walls, put some typical values
  ! Overall min values from chemtable
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           if (mask(i,j).eq.1) then
              ! First peak
              sootNEW(i,j,k,1) = SMALLWEIGHT
              ! Second peak
              sootNEW(i,j,k,2) = SMALLWEIGHT
              sootNEW(i,j,k,3) = SMALLWEIGHT * SMALLSEP * nbrC(1)
              sootNEW(i,j,k,4) = SMALLWEIGHT * SMALLSEP * surf(1)
              sootNEW(i,j,k,5) = SMALLWEIGHT * SMALLSEP * nbrH(1)
           end if
        end do
     end do
  end do
  
  ! Create monitor file
  call monitor_create_file_step('soot-dqmom',1)
  call monitor_set_header(1,'niter ', 'r')
  soot_niter = 0.0_WP
  
  ! Soot post step operations
  call soot_dqmom_poststep
  
  return
end subroutine soot_dqmom_init


! ================================= !
! Compute the source terms for Soot !
! ================================= !
subroutine soot_dqmom_source
  use soot_dqmom
  use onestep
  use diffusion
  use masks
  use time_info
  implicit none

  integer  :: i,j,k
  integer  :: my_count,my_count2,count
  real(WP) :: t_start,rate
  
  real(WP) :: dt_
  integer  :: step
  
  ! Precompute some quantities
  select case(trim(chemistry))
  case ('one-step')
     call onestep_get_mixfrac
     call onestep_get_chi
  case ('diffusion')
     ! Nothing to do
  case default
     call die('soot_dqmom_source: module requires one-step or chemtable chemistry for now')
  end select
  
  my_count  = 0
  my_count2 = 0
  
  ! Evaluate all quantities at half timestep for second order
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (mask(i,j).ne.1) then

              ! Get Some quantites
              select case(trim(chemistry))
              case ('one-step')
                 table_x1 = Mix_Frac(i,j,k)
                 table_x2 = 0.0_WP
                 table_x3 = CHI(i,j,k)
                 Wmol_ = Wmol(i,j,k)
              case ('diffusion')
                 table_x1 = SC(i,j,k,isc_ZMIX)
                 table_x2 = ZVAR(i,j,k)
                 select case(trim(diffModel))
                 case ('FPVA')
                    table_x3 = SC(i,j,k,isc_PROG)
                 case ('Steady Flamelet')
                    table_x3 = CHI(i,j,k)
                 end select
                 Wmol_ = 30.0e-3_WP
              case default
              end select
              temp_ = T(i,j,k)
              rho_  = RHO (i,j,k)
              visc_ = VISC(i,j,k)
              
              ! Transfer values to the sol vector
              sol = sootOLD(i,j,k,:) * rho_
              
              ! Start the interation
              t_start = 0.0_WP
              step = 0
              do while (t_start.lt.dt .and. step.lt.10)
                 
                 ! Compute source terms
                 call soot_dqmom_compute_src(sol)
                 
                 ! Compute timestep
                 dt_ = dt - t_start
                 rate = 2.0_WP*maxval(-dt_*src_dqmom/sol)
                 if (rate.gt.1.0_WP) then
                    dt_ = dt_/rate
                 end if
                 
                 ! Explicit integration
                 sol = sol + dt_*src_dqmom
                 
                 ! Increment
                 t_start = t_start+dt_
                 step = step+1
                 
              end do
              my_count = my_count + step
              
              ! If not done, add remaining source term
              if (t_start.lt.dt) then
                 sol = sol + (dt-t_start)*src_dqmom
              end if
              
              ! Clip source terms
              sol = sol/rho_
              call soot_dqmom_clip(sol)
              sootSRC(i,j,k,:) = (sol - sootOLD(i,j,k,:)) * rho_
              
           end if
        end do
     end do
  end do
  
  call parallel_sum(my_count, count)
  soot_niter = real(count,WP)/real(nx*ny*nz,WP)
  
  ! Dump the quantities of interest in a monitor file
  call monitor_select_file('soot-dqmom')
  call monitor_set_single_value(1,soot_niter)
  
  return
end subroutine soot_dqmom_source


! ======================== !
! Clip the soot quantities !
! ======================== !
subroutine soot_dqmom_clip(soot_)
  use soot_dqmom
  use infnan
  implicit none
  real(WP), dimension(nMoments) :: soot_
  real(WP) :: mom0,mom1,mom2,mom3
  
  ! Get back weights and abscissas
  weight(1) = soot_(1)
  weight(2) = soot_(2)
  nbrC(2)   = soot_(3) / weight(2)
  surf(2)   = soot_(4) / weight(2)
  nbrH(2)   = soot_(5) / weight(2)
  
  ! Compute total moments
  mom0 = soot_(2) + soot_(1)
  mom1 = soot_(3) + soot_(1)*nbrC(1)
  mom2 = soot_(4) + soot_(1)*surf(1)
  mom3 = soot_(5) + soot_(1)*nbrH(1)
  
  ! Check for negative total number density
  if ( mom0.lt.SMALLWEIGHT .or. &
       mom1.lt.SMALLWEIGHT*nbrC(1)*(1.0_WP+SMALLSEP) .or. &
       mom2.lt.SMALLWEIGHT*surf(1)*(1.0_WP+SMALLSEP) .or. &
       mom3.lt.SMALLWEIGHT*nbrH(1)*(1.0_WP+SMALLSEP) ) then
     mom1 = max(mom1, SMALLWEIGHT*nbrC(1)*(1.0_WP+SMALLSEP))
     nbrC(2) = nbrC(1)*SMALLSEP
     surf(2) = surf(1)*SMALLSEP
     nbrH(2) = nbrH(1)*SMALLSEP
     weight(1) = SMALLWEIGHT
     weight(2) = (mom1 - weight(1)*nbrC(1))/nbrC(2)
  end if
  
  ! Compute related quantities
  dp = nbrC/surf
  np = surf**3/nbrC**2
  
  ! If weight is not significant force minimum
  if (weight(1).lt.SMALLWEIGHT) then
     weight(1) = SMALLWEIGHT
     weight(2) = max(mom0 - weight(1), SMALLWEIGHT)
     nbrC(2) = (mom1 - weight(1)*nbrC(1))/weight(2)
     surf(2) = (mom2 - weight(1)*surf(1))/weight(2)
     nbrH(2) = (mom3 - weight(1)*nbrH(1))/weight(2)
  end if
  if (weight(2).lt.SMALLWEIGHT) then
     nbrC(2) = nbrC(1)*SMALLSEP
     surf(2) = surf(1)*SMALLSEP
     nbrH(2) = nbrH(1)*SMALLSEP
     weight(2) = SMALLWEIGHT
     weight(1) = (mom1 - weight(2)*nbrC(2))/nbrC(1)
  end if
  
  ! Otherwise ensure some seperation between peaks
  nbrC(2)   = max(nbrC(2), nbrC(1)*SMALLSEP)
  surf(2)   = max(surf(2), surf(1)*SMALLSEP)
  nbrH(2)   = max(nbrH(2), nbrH(1)*SMALLSEP)
  
  ! Clip for dp/np
  if (dp(2).lt.dp(1))  surf(2) = surf(1)*nbrC(2)/nbrC(1)
  if (np(2).lt.1.0_WP) surf(2) = nbrC(2)**(2.0_WP/3.0_WP)
  
  ! Put back the values
  soot_(1) = weight(1)
  soot_(2) = weight(2)
  soot_(3) = nbrC(2) * weight(2)
  soot_(4) = surf(2) * weight(2)
  soot_(5) = nbrH(2) * weight(2)
  
  if (soot_(3) + soot_(1)*nbrC(1) .lt. 0.999_WP*mom1) then
     print*,soot_
     print*,mom1,soot_(3) + soot_(1)*nbrC(1) 
     !call die ('not possible')
  end if
  
  return
end subroutine soot_dqmom_clip

  

! ============================================== !
! Routine to execute after the transport of soot !
! -> Clean up the variables (clipping,..)        !
! -> Compute some mean statistical quantities    !
! ============================================== !
subroutine soot_dqmom_poststep
  use soot_dqmom
  use masks
  use onestep
  use infnan
  implicit none
  integer  :: i,j,k
  real(WP) :: mom0,mom1,mom2
  
  ! Update the physical boundaries
  call soot_dqmom_apply_bc(sootNEW)
  
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_

           ! ===== CLIPPING =====
           
           sol = sootNEW(i,j,k,:)
           if (mask(i,j).ne.1) call soot_dqmom_clip(sol)
           sootNEW(i,j,k,:) = sol
           
           ! ===== MEAN QUANTITIES =====
           
           ! Number density
           frac(1) = 0.0_WP; frac(2) = 0.0_WP; frac(3) = 0.0_WP
           call soot_compute_moment(mom0)
           numdens(i,j,k) = RHO(i,j,k) * mom0 * Avogadro*1.0E-6_WP
           
           ! Volume Fraction
           frac(1) = 1.0_WP; frac(2) = 0.0_WP; frac(3) = 0.0_WP
           call soot_compute_moment(mom1)
           fv(i,j,k) = RHO(i,j,k) * mom1 * MolarMassSoot/SootDensity
           
           ! Primary particle diameter
           frac(1) = 4.0_WP; frac(2) =-3.0_WP; frac(3) = 0.0_WP
           call soot_compute_moment(mom2)
           partdiam(i,j,k) = (mom2/mom1)**(1.0_WP/3.0_WP) * CarbonToDiam*1.0E9_WP
           
           ! Number of primary particles per aggregate
           frac(1) =-2.0_WP; frac(2) = 3.0_WP; frac(3) = 0.0_WP
           call soot_compute_moment(mom1)
           partaggr(i,j,k) = mom1/mom0
           
           ! Surface reactivity
           frac(1) = 0.0_WP; frac(2) = 0.0_WP; frac(3) = 1.0_WP
           call soot_compute_moment(mom1)
           frac(1) = 0.0_WP; frac(2) = 1.0_WP; frac(3) = 0.0_WP
           call soot_compute_moment(mom2)
           surfreac(i,j,k) = mom1/(mom2*Pi*CarbonToDiam**2) * 1.0E-19_WP
           
           if (isnan(fv(i,j,k)) .or. isinf(fv(i,j,k))) then
              print*,i,j,k
              print*,sootNEW(i,j,k,:)
              call die('WHOA!')
           end if
           
        end do
     end do
  end do
  
  return
end subroutine soot_dqmom_poststep


! ============================================== !
! Check if the soot quantities are out of bounds !
! ============================================== !
subroutine soot_dqmom_check_bounds(soot_,bounded)
  use soot_dqmom
  use masks
  use onestep
  implicit none
  real(WP), dimension(nMoments), intent(in) :: soot_
  integer,  intent(inout) :: bounded
  
  bounded = 1
  
  ! Get back weight and abscissas
  weight(1) = soot_(1)
  weight(2) = soot_(2)
  nbrC(2)   = soot_(3) / weight(2)
  surf(2)   = soot_(4) / weight(2)
  nbrH(2)   = soot_(5) / weight(2)
  
  ! Compute related quantities
  dp = nbrC/surf
  np = surf**3/nbrC**2
  
  ! If weight is not significant force minimum
  if (weight(1).lt.0.0_WP) bounded = 0
  if (weight(2).lt.0.0_WP) bounded = 0
  ! Aggregate too small or too large
  if (np(2).lt.0.9_WP) bounded = 0
  if (np(2).gt.BIG_NP) bounded = 0
  ! Min volume, surface, ...
  if (nbrC(2).lt.nbrC(1)) bounded = 0
  if (nbrH(2).lt.nbrH(1)) bounded = 0
  if (dp(2)  .lt.dp(1))   bounded = 0
  
  return
end subroutine soot_dqmom_check_bounds

