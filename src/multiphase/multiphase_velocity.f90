! ==================================== !
! Velocity module for multiphase flows !
! Use energy conserving 2nd order FD   !
! scheme away from interface, and 3rd  !
! order WENO at the interface          !
! ==================================== !
module multiphase_velocity
  use velocity
  use multiphase
  use multiphase_band
  use metric_velocity_conv
  use multiphase_tools
  use masks
  implicit none

  ! Dummy module
end module multiphase_velocity

! ============== !
! Initialization !
! ============== !
subroutine multiphase_velocity_init
  use multiphase_velocity
  implicit none

  ! Initialize rho_UVW
  call multiphase_velocity_prestep

  return
end subroutine multiphase_velocity_init

! ================= !
! Velocity pre-step !
! ================= !
subroutine multiphase_velocity_prestep
  use multiphase_velocity
  use multiphase_fluxes
  implicit none
   
  integer :: i,j,k
  real(WP) :: myVOF
    
  ! Initialize rho_U/V/W
  do k=kmin_,kmax_+1
     do j=jmin_,jmax_+1
        do i=imin_,imax_+1
          if(mask(i,j) .eq. 0) then
            ! U-cell density
             myVOF=(0.25_WP*sum(VOF(subface2subcell_x (1:4),i  ,j,k))*vol(i  ,j,k) &
                     +0.25_WP*sum(VOF(subface2subcell_xm(1:4),i-1,j,k))*vol(i-1,j,k))/(vol(i,j,k)+vol(i-1,j,k)+epsilon(0.0_WP))
            rho_U(i,j,k)=myVOF*rho_l+(1.0_WP-myVOF)*rho_g
            ! V-cell density
            myVOF=(0.25_WP*sum(VOF(subface2subcell_y (1:4),i,j  ,k))*vol(i,j  ,k) &
                    +0.25_WP*sum(VOF(subface2subcell_ym(1:4),i,j-1,k))*vol(i,j-1,k))/(vol(i,j,k)+vol(i,j-1,k)+epsilon(0.0_WP))
            rho_V(i,j,k)=myVOF*rho_l+(1.0_WP-myVOF)*rho_g
            ! W-cell density
            myVOF=(0.25_WP*sum(VOF(subface2subcell_z (1:4),i,j,k  ))*vol(i,j,k  ) &
                    +0.25_WP*sum(VOF(subface2subcell_zm(1:4),i,j,k-1))*vol(i,j,k-1))/(vol(i,j,k)+vol(i,j,k-1)+epsilon(0.0_WP))
            rho_W(i,j,k)=myVOF*rho_l+(1.0_WP-myVOF)*rho_g
          else
          ! If in wall, set to arbitrary value. Shouldn't be used
            rho_U(i,j,k) = 1.0_WP
            rho_V(i,j,k) = 1.0_WP
            rho_W(i,j,k) = 1.0_WP
          end If
        end do
     end do
  end do
  
  ! Communication - careful, inlet/outlet not treated!
  call communication_border(rho_U)
  call communication_border(rho_V)
  call communication_border(rho_W)

  ! Store old fields
  rho_Uold=rho_U
  rho_Vold=rho_V
  rho_Wold=rho_W

  return
end subroutine multiphase_velocity_prestep

! ========================================================= !
! Time advance the velocity equation adapted for multiphase !
! ========================================================= !
subroutine multiphase_velocity_step
  use multiphase_velocity
  use data
  use parallel
  use memory
  implicit none

  integer :: i,j,k

  ! Start the timer
  call timing_start('velocity')

  ! Compute density at n+1 for rho_multiply/divide
  RHOmid=0.5_WP*(RHO+RHOold)

  ! Compute mid point
  ! Store it in the 'n+1' velocity
  rhoU=0.5_WP*(rhoU+rhoUold); U=0.5_WP*(U+Uold)
  rhoV=0.5_WP*(rhoV+rhoVold); V=0.5_WP*(V+Vold)
  rhoW=0.5_WP*(rhoW+rhoWold); W=0.5_WP*(W+Wold)

  ! Zero the source terms
  srcUmid=0.0_WP
  srcVmid=0.0_WP
  srcWmid=0.0_WP
  srcPmid=0.0_WP
  
  ! Compute body forces based on mid-step
  call bodyforce_src_mid
  
  ! Compute IB no-slip conditions
  call timing_stop('velocity')
  call multiphase_ibsource
  call timing_start('velocity')

  ! Add prestep sources
  srcUmid=srcUmid+srcU
  srcVmid=srcVmid+srcV
  srcWmid=srcWmid+srcW
  srcPmid=srcPmid+srcP

  ! Solve for U
  call multiphase_residuals_u
  call multiphase_inverse_u

  ! Solve for V
  call multiphase_residuals_v
  call multiphase_inverse_v

  ! Solve for W
  call multiphase_residuals_w
  call multiphase_inverse_w

  ! Update U,V,W
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           U(i,j,k) = 2.0_WP*U(i,j,k)-Uold(i,j,k) + ResU(i,j,k)
           V(i,j,k) = 2.0_WP*V(i,j,k)-Vold(i,j,k) + ResV(i,j,k)
           W(i,j,k) = 2.0_WP*W(i,j,k)-Wold(i,j,k) + ResW(i,j,k)
        end do
     end do
  end do

  ! Set the monitor values
  call parallel_max(maxval(abs(ResU)),max_resU)
  call parallel_max(maxval(abs(ResV)),max_resV)
  call parallel_max(maxval(abs(ResW)),max_resW)

  ! Update the physical boundaries
  call boundary_velocity_neumann
  call boundary_velocity_outflow
  call boundary_update_border(U,'+','ym')
  call boundary_update_border(V,'-','y')
  call boundary_update_border(W,'-','ym')

  ! Synchronise U/rhoU
  call rho_multiply
  call boundary_momentum_neumann

  ! Transfer values to monitor
  call monitor_select_file('convergence_velocity')
  call monitor_set_single_value(1,max_resU)
  call monitor_set_single_value(2,max_resV)
  call monitor_set_single_value(3,max_resW)

  ! Stop the timer
  call timing_stop('velocity')

  return
end subroutine multiphase_velocity_step

! ==================== !
! U-momentum component !
!  & density update    !
! ==================== !
subroutine multiphase_residuals_u
  use multiphase_velocity
  use data
  use memory
  use metric_velocity_conv
  use metric_velocity_visc
  use metric_generic
  use multiphase_fluxes
  implicit none

  ! FX(i,j,k) -> xm(i),ym(j),zm(k)
  ! FY(i,j,k) -> x(i),y(j),zm(k)
  ! FZ(i,j,k) -> x(i),ym(j),z(k)
  real(WP) :: rhs
  integer  :: i,j,k,ii,jj,kk, s
  real(WP) :: my_fvol,my_fliq,my_fgas,my_frho,my_fmom

  ! Zero fluxes
  FX=0.0_WP; FY=0.0_WP; FZ=0.0_WP
  rhoUi=0.0_WP; rhoVi=0.0_WP; rhoWi=0.0_WP

  ! Compute fluxes
  do kk=kmin_,kmax_+1
     do jj=jmin_,jmax_+1 
        do ii=imin_,imax_+1

           my_fvol = 0.0_WP
           my_fliq = 0.0_WP
           my_fgas = 0.0_WP

           ! Convective fluxes
           i = ii-1; j = jj-1; k = kk-1;
           ! X direction
           do s = 1,4
              my_fliq= my_fliq + SLF_liqx(subcell2flux_Ux_s(s),i+subcell2flux_Ux_i(s),j,k)
              my_fgas= my_fgas + SLF_gasx(subcell2flux_Ux_s(s),i+subcell2flux_Ux_i(s),j,k)
           end do
           my_fmom=SLF_rhoUx(i,j,k)
           my_frho=my_fliq*rho_l+my_fgas*rho_g
           FX(i,j,k)   =-my_fmom/(dt_uvw*dy(j)*dz)
           rhoUi(i,j,k)=-my_frho/(dt_uvw*dy(j)*dz)

           my_fvol = 0.0_WP
           my_fliq = 0.0_WP
           my_fgas = 0.0_WP

           i = ii; j = jj; k = kk;
           ! Y direction
           do s = 1,4
              my_fliq= my_fliq + SLF_liqy(subcell2flux_Uy_s(s),i+subcell2flux_Uy_i(s),j,k)
              my_fgas= my_fgas + SLF_gasy(subcell2flux_Uy_s(s),i+subcell2flux_Uy_i(s),j,k)
           end do
           my_fmom=SLF_rhoUy(i,j,k)
           my_frho=my_fliq*rho_l+my_fgas*rho_g
           FY(i,j,k)   =-my_fmom/(dt_uvw*dx(i)*dz)
           rhoVi(i,j,k)=-my_frho/(dt_uvw*dx(i)*dz)

           my_fvol = 0.0_WP
           my_fliq = 0.0_WP
           my_fgas = 0.0_WP

           ! Z direction
           do s = 1,4
              my_fliq= my_fliq + SLF_liqz(subcell2flux_Uz_s(s),i+subcell2flux_Uz_i(s),j,k)
              my_fgas= my_fgas + SLF_gasz(subcell2flux_Uz_s(s),i+subcell2flux_Uz_i(s),j,k)
           end do
           my_fmom=SLF_rhoUz(i,j,k)
           my_frho=my_fliq*rho_l+my_fgas*rho_g
           FZ(i,j,k)   =-my_fmom/(dt_uvw*dx(i)*dy(j))
           rhoWi(i,j,k)=-my_frho/(dt_uvw*dx(i)*dy(j))

           ! Viscous fluxes
           i = ii-1; j = jj-1; k = kk-1;
           FX(i,j,k) = FX(i,j,k) + 2.0_WP*VISC(i,j,k)*(sum(grad_u_x(i,j,k,:)*U(i-stv1:i+stv2,j,k)) &
                -(sum(divv_u(i,j,k,:)*U(i-stv1:i+stv2,j,k)) &
                +sum(divv_v(i,j,k,:)*V(i,j-stv1:j+stv2,k)) &
                +sum(divv_w(i,j,k,:)*W(i,j,k-stv1:k+stv2)))/3.0_WP)
           i = ii; j = jj; k = kk;
           FY(i,j,k) = FY(i,j,k) + sum(interp_sc_xy(i,j,:,:)*VISC(i-st2:i+st1,j-st2:j+st1,k))*( &
                +sum(grad_u_y(i,j,k,:)*U(i,j-stv2:j+stv1,k)) &
                +sum(grad_v_x(i,j,k,:)*V(i-stv2:i+stv1,j,k)))
           FZ(i,j,k) = FZ(i,j,k) + sum(interp_sc_xz(i,j,:,:)*VISC(i-st2:i+st1,j,k-st2:k+st1)) *( &
                +sum(grad_u_z(i,j,k,:)*U(i,j,k-stv2:k+stv1)) &
                +sum(grad_w_x(i,j,k,:)*W(i-stv2:i+stv1,j,k)))
        end do
     end do
  end do

  ! Transport density
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Divergence of the density fluxes
           rhs =+sum(divc_xx(i,j,k,:)*rhoUi(i-stc2:i+stc1,j,k)) &
                +sum(divc_xy(i,j,k,:)*rhoVi(i,j-stc1:j+stc2,k)) &
                +sum(divc_xz(i,j,k,:)*rhoWi(i,j,k-stc1:k+stc2))

           ! Update
           rho_U(i,j,k)=rho_Uold(i,j,k)+dt_uvw*rhs
        end do
     end do
  end do

  ! Communicate density
  call communication_border(rho_U)

  ! Compute rhs
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_

           ! Divergence of the fluxes
           rhs =+sum(divc_xx(i,j,k,:)*FX(i-stc2:i+stc1,j,k)) &
                +sum(divc_xy(i,j,k,:)*FY(i,j-stc1:j+stc2,k)) &
                +sum(divc_xz(i,j,k,:)*FZ(i,j,k-stc1:k+stc2)) 

           ! Full residual
           ResU(i,j,k) = -2.0_WP*U(i,j,k)+Uold(i,j,k) &
                + (rho_Uold(i,j,k)*Uold(i,j,k) + dt_uvw*rhs)/rho_U(i,j,k) + srcUmid(i,j,k)/rho_Uold(i,j,k) - dt_uvw*gradPx(i,j,k)
        end do
     end do
  end do

  return
end subroutine multiphase_residuals_u

! ==================== !
! V-momentum component !
!  & density update    !
! ==================== !
subroutine multiphase_residuals_v
  use multiphase_velocity
  use data
  use memory
  use metric_velocity_conv
  use metric_velocity_visc
  use metric_generic
  use multiphase_fluxes
  implicit none

  ! FX(i,j,k) -> x(i),y(j),zm(k)
  ! FY(i,j,k) -> xm(i),ym(j),zm(k)
  ! FZ(i,j,k) -> xm(i),y(j),z(k)
  real(WP) :: rhs
  integer  :: i,j,k,ii,jj,kk, s
  real(WP) :: Acos,Asin
  real(WP) :: my_fvol,my_fliq,my_fgas,my_frho,my_fmom

  ! Compute fluxes
  do kk=kmin_,kmax_+1
     do jj=jmin_,jmax_+1
        do ii=imin_,imax_+1
           my_fvol = 0.0_WP
           my_fliq = 0.0_WP
           my_fgas = 0.0_WP
           ! Convective fluxes
           i = ii-1; j = jj-1; k = kk-1;
           ! Y direction
           do s = 1,4
              my_fliq= my_fliq + SLF_liqy(subcell2flux_Vy_s(s),i,j+subcell2flux_Vy_j(s),k)
              my_fgas= my_fgas + SLF_gasy(subcell2flux_Vy_s(s),i,j+subcell2flux_Vy_j(s),k)
           end do
           my_fmom=SLF_rhoVy(i,j,k)
           my_frho=my_fliq*rho_l+my_fgas*rho_g
           FY(i,j,k)   =-my_fmom/(dt_uvw*dx(i)*dz)
           rhoVi(i,j,k)=-my_frho/(dt_uvw*dx(i)*dz)

           my_fvol = 0.0_WP
           my_fliq = 0.0_WP
           my_fgas = 0.0_WP

           i = ii; j = jj; k = kk;           
           ! X direction
           do s = 1,4
              my_fliq= my_fliq + SLF_liqx(subcell2flux_Vx_s(s),i,j+subcell2flux_Vx_j(s),k)
              my_fgas= my_fgas + SLF_gasx(subcell2flux_Vx_s(s),i,j+subcell2flux_Vx_j(s),k)
           end do
           my_fmom=SLF_rhoVx(i,j,k)
           my_frho=my_fliq*rho_l+my_fgas*rho_g
           FX(i,j,k)   =-my_fmom/(dt_uvw*dy(j)*dz)
           rhoUi(i,j,k)=-my_frho/(dt_uvw*dy(j)*dz)

           my_fvol = 0.0_WP
           my_fliq = 0.0_WP
           my_fgas = 0.0_WP

           ! Z direction
           do s = 1,4
              my_fliq= my_fliq + SLF_liqz(subcell2flux_Vz_s(s),i,j+subcell2flux_Vz_j(s),k)
              my_fgas= my_fgas + SLF_gasz(subcell2flux_Vz_s(s),i,j+subcell2flux_Vz_j(s),k)
           end do
           my_fmom=SLF_rhoVz(i,j,k)
           my_frho=my_fliq*rho_l+my_fgas*rho_g
           FZ(i,j,k)   =-my_fmom/(dt_uvw*dx(i)*dy(j))
           rhoWi(i,j,k)=-my_frho/(dt_uvw*dx(i)*dy(j))

           ! Viscous fluxes
           i = ii-1; j = jj-1; k = kk-1;
           FY(i,j,k) = FY(i,j,k) + 2.0_WP*VISC(i,j,k)*(sum(grad_v_y(i,j,k,:)*V(i,j-stv1:j+stv2,k)) &
                -(sum(divv_u(i,j,k,:)*U(i-stv1:i+stv2,j,k)) &
                +sum(divv_v(i,j,k,:)*V(i,j-stv1:j+stv2,k)) &
                +sum(divv_w(i,j,k,:)*W(i,j,k-stv1:k+stv2)))/3.0_WP)
           Fcylv(i,j,k) = 2.0_WP*VISC(i,j,k)*(sum(grad_w_z(i,j,k,:)*W(i,j,k-stv1:k+stv2)) &
                -(sum(divv_u(i,j,k,:)*U(i-stv1:i+stv2,j,k)) &
                +sum(divv_v(i,j,k,:)*V(i,j-stv1:j+stv2,k)) &
                +sum(divv_w(i,j,k,:)*W(i,j,k-stv1:k+stv2)))/3.0_WP &
                +ymi(j)*sum(interpv_cyl_v_ym(i,j,:)*V(i,j-stv1:j+stv2,k)))
           i = ii; j = jj; k = kk;
           FX(i,j,k) = FX(i,j,k) + sum(interp_sc_xy(i,j,:,:)*VISC(i-st2:i+st1,j-st2:j+st1,k))*( &
                +sum(grad_u_y(i,j,k,:)*U(i,j-stv2:j+stv1,k)) &
                +sum(grad_v_x(i,j,k,:)*V(i-stv2:i+stv1,j,k)))
           FZ(i,j,k) = FZ(i,j,k) + sum(interp_sc_yz(i,j,:,:)*VISC(i,j-st2:j+st1,k-st2:k+st1))*( &
                +sum(grad_v_z(i,j,k,:)*V(i,j,k-stv2:k+stv1)) &
                +sum(grad_w_y(i,j,k,:)*W(i,j-stv2:j+stv1,k)) &
                -yi(j)*sum(interpv_cyl_w_y(i,j,:)*W(i,j-stv2:j+stv1,k)))
        end do
     end do
  end do
  
  ! Calculation of density calculated through VOF, see prestep
  !Transport density
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Divergence of the density fluxes
           rhs =+sum(divc_yx(i,j,k,:)*rhoUi(i-stc1:i+stc2,j,k)) &
                +sum(divc_yy(i,j,k,:)*rhoVi(i,j-stc2:j+stc1,k)) &
                +sum(divc_yz(i,j,k,:)*rhoWi(i,j,k-stc1:k+stc2))

           ! Update
           rho_V(i,j,k)=rho_Vold(i,j,k)+dt_uvw*rhs

        end do
     end do
  end do

  ! Communicate density
  call communication_border(rho_V)

  ! Full residual
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_

           ! Divergence of the fluxes
           rhs =+sum(divc_yx(i,j,k,:)*FX(i-stc1:i+stc2,j,k)) &
                +sum(divc_yy(i,j,k,:)*FY(i,j-stc2:j+stc1,k)) &
                +sum(divc_yz(i,j,k,:)*FZ(i,j,k-stc1:k+stc2))
          
           ! Cylindrical terms
           rhs = rhs - ymmi(j)*sum(interp_cyl_F_y (i,j,:)*Fcyl (i,j-stc2:j+stc1,k)) &
                     - yi  (j)*sum(interpv_cyl_F_y(i,j,:)*Fcylv(i,j-stv2:j+stv1,k))

          ! Full residual
           ResV(i,j,k) = -2.0_WP*V(i,j,k)+Vold(i,j,k) &
                + (rho_Vold(i,j,k)*Vold(i,j,k) + dt_uvw*rhs)/rho_V(i,j,k) + srcVmid(i,j,k)/rho_Vold(i,j,k)  - dt_uvw*gradPy(i,j,k)
        end do
     end do
  end do

  ! Morinishi's axis treatment
  if (icyl.eq.1 .and. jproc.eq.1) then
     do i=imin_,imax_
        Acos = 2.0_WP*sum(ResV(i,jmin,:)*cos(zm(kmin:kmax)))/real(nz,WP)
        Asin = 2.0_WP*sum(ResV(i,jmin,:)*sin(zm(kmin:kmax)))/real(nz,WP)
        ResV(i,jmin,:) = Acos*cos(zm(kmin:kmax)) + Asin*sin(zm(kmin:kmax))
     end do
  end if
  
  return
end subroutine multiphase_residuals_v

! ==================== !
! W-momentum component !
!  & density update    !
! ==================== !
subroutine multiphase_residuals_w
  use multiphase_velocity
  use data
  use memory
  use metric_velocity_conv
  use metric_velocity_visc
  use metric_generic
  use multiphase_fluxes
  implicit none

  ! FX(i,j,k) -> x(i),ym(j),z(k)
  ! FY(i,j,k) -> xm(i),y(j),z(k)
  ! FZ(i,j,k) -> xm(i),ym(j),zm(k)
  ! Fcyl(i,j,k) -> xm(i),ym(j),zm(k)
  real(WP) :: rhs
  integer  :: i,j,k,ii,jj,kk, s
  real(WP) :: my_fvol,my_fliq,my_fgas,my_frho,my_fmom

  ! Compute fluxes
  do kk=kmin_,kmax_+1
     do jj=jmin_,jmax_+1 
        do ii=imin_,imax_+1

           my_fvol = 0.0_WP
           my_fliq = 0.0_WP
           my_fgas = 0.0_WP

           ! Convective fluxes
           i = ii-1; j = jj-1; k = kk-1;
           ! Z direction
           do s = 1,4
              my_fliq= my_fliq + SLF_liqz(subcell2flux_Wz_s(s),i,j,k+subcell2flux_Wz_k(s))
              my_fgas= my_fgas + SLF_gasz(subcell2flux_Wz_s(s),i,j,k+subcell2flux_Wz_k(s))
           end do
           my_fmom=SLF_rhoWz(i,j,k)
           my_frho=my_fliq*rho_l+my_fgas*rho_g
           FZ(i,j,k)   =-my_fmom/(dt_uvw*dx(i)*dy(j))
           rhoWi(i,j,k)=-my_frho/(dt_uvw*dx(i)*dy(j))

           my_fvol = 0.0_WP
           my_fliq = 0.0_WP
           my_fgas = 0.0_WP

           i = ii; j = jj; k = kk;
           ! X direction
           do s = 1,4
              my_fliq= my_fliq + SLF_liqx(subcell2flux_Wx_s(s),i,j,k+subcell2flux_Wx_k(s))
              my_fgas= my_fgas + SLF_gasx(subcell2flux_Wx_s(s),i,j,k+subcell2flux_Wx_k(s))
           end do
           my_fmom=SLF_rhoWx(i,j,k)
           my_frho=my_fliq*rho_l+my_fgas*rho_g
           FX(i,j,k)   =-my_fmom/(dt_uvw*dy(j)*dz)
           rhoUi(i,j,k)=-my_frho/(dt_uvw*dy(j)*dz)

           my_fvol = 0.0_WP
           my_fliq = 0.0_WP
           my_fgas = 0.0_WP

           ! Y direction
           do s = 1,4
              my_fliq= my_fliq + SLF_liqy(subcell2flux_Wy_s(s),i,j,k+subcell2flux_Wy_k(s))
              my_fgas= my_fgas + SLF_gasy(subcell2flux_Wy_s(s),i,j,k+subcell2flux_Wy_k(s))
           end do
           my_fmom=SLF_rhoWy(i,j,k)
           my_frho=my_fliq*rho_l+my_fgas*rho_g
           FY(i,j,k)   =-my_fmom/(dt_uvw*dx(i)*dz)
           rhoVi(i,j,k)=-my_frho/(dt_uvw*dx(i)*dz)

           ! Viscous flux
           i = ii-1; j = jj-1; k = kk-1;
           FZ(i,j,k) = FZ(i,j,k) + 2.0_WP*VISC(i,j,k)*(sum(grad_w_z(i,j,k,:)*W(i,j,k-stv1:k+stv2)) &
                -(sum(divv_u(i,j,k,:)*U(i-stv1:i+stv2,j,k)) &
                +sum(divv_v(i,j,k,:)*V(i,j-stv1:j+stv2,k)) &
                +sum(divv_w(i,j,k,:)*W(i,j,k-stv1:k+stv2)))/3.0_WP &
                +ymi(j)*sum(interpv_cyl_v_ym(i,j,:)*V(i,j-stv1:j+stv2,k)))
           i = ii; j = jj; k = kk;
           FX(i,j,k) = FX(i,j,k) + sum(interp_sc_xz(i,j,:,:)*VISC(i-st2:i+st1,j,k-st2:k+st1))*( &
                +sum(grad_u_z(i,j,k,:)*U(i,j,k-stv2:k+stv1)) &
                +sum(grad_w_x(i,j,k,:)*W(i-stv2:i+stv1,j,k)))
           ! Y viscous flux
           FY(i,j,k) = FY(i,j,k) + sum(interp_sc_yz(i,j,:,:)*VISC(i,j-st2:j+st1,k-st2:k+st1))*( &
                +sum(grad_v_z(i,j,k,:)*V(i,j,k-stv2:k+stv1)) &
                +sum(grad_w_y(i,j,k,:)*W(i,j-stv2:j+stv1,k)) &
                -yi(j)*sum(interpv_cyl_w_y(i,j,:)*W(i,j-stv2:j+stv1,k)) )

        end do
     end do
  end do

  ! Transport density
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Divergence of the density fluxes
           rhs =+sum(divc_zx(i,j,k,:)*rhoUi(i-stc1:i+stc2,j,k)) &
                +sum(divc_zy(i,j,k,:)*rhoVi(i,j-stc1:j+stc2,k)) &
                +sum(divc_zz(i,j,k,:)*rhoWi(i,j,k-stc2:k+stc1))
           ! Update
           rho_W(i,j,k)=rho_Wold(i,j,k)+dt_uvw*rhs
        end do
     end do
  end do
  ! Communicate density
  call communication_border(rho_W)

  ! Full residual
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_

           ! Divergence of the fluxes
           rhs =+sum(divc_zx(i,j,k,:)*FX(i-stc1:i+stc2,j,k)) &
                +sum(divc_zy(i,j,k,:)*FY(i,j-stc1:j+stc2,k)) &
                +sum(divc_zz(i,j,k,:)*FZ(i,j,k-stc2:k+stc1)) 
           ! Cylindrical terms
           rhs = rhs + ymi(j)*sum(interp_cyl_F_z  (i,j,:)*Fcyl(i,j,k-stc2:k+stc1)) &
                     + ymi(j)*sum(interpv_cyl_F_ym(i,j,:)*FY(i,j-stv1:j+stv2,k))

           ! Full residual
           ResW(i,j,k) = -2.0_WP*W(i,j,k)+Wold(i,j,k) &
                + (rho_Wold(i,j,k)*Wold(i,j,k) + dt_uvw*rhs)/rho_W(i,j,k) + srcWmid(i,j,k)/rho_Wold(i,j,k) - dt_uvw*gradPz(i,j,k)

        end do
     end do
  end do

  return
end subroutine multiphase_residuals_w
