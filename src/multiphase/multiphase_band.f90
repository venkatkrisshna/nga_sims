! ================================= !
! Band structure based on VOF field !
! ================================= !
module multiphase_band
  use multiphase
  implicit none
  
  ! Banded structure
  integer :: nband
  integer, dimension(:,:), pointer :: band_map  
  integer, dimension(:),   allocatable :: band_size 
  integer, dimension(:),   allocatable :: band_count
  
end module multiphase_band


! =============================== !
! Band information initialization !
! =============================== !
subroutine multiphase_band_init
  use multiphase_band
  implicit none
  
  ! Set band size
  nband=5
  
  ! Allocate band info
  allocate(band   (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); band   =0
  allocate(bandold(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); bandold=0
  allocate(band_size (0:nband)); band_size=0
  allocate(band_count(0:nband))
  
  return
end subroutine multiphase_band_init

! ================= !
! Bands for P cells !
! ================= !
subroutine multiphase_band_update
  use multiphase_band
  implicit none
  
  integer :: i,j,k,ii,jj,kk
  integer :: index, n
  
  ! Create bands ============================================================
  
  ! Reset band
  band=(nband+1)*int(sign(1.0_WP,VOFavg-0.5_WP))

  ! Check for band at domain wall with ghost cells (for inflows/outflows)

  if (iproc.eq.1 .and. xper.ne.1) then
     do k = kmin_,kmax_
        do j = jmin_,jmax_
           i = imin-1
           ! Deal with solid cells
           if (vol(i,j,k).lt.tiny(0.0_WP)) cycle

           ! Cells with interface inside
           if ( VOFavg(i,j,k).gt.VOFlo .and. &
                VOFavg(i,j,k).lt.VOFhi) then
              band(i,j,k)=0

              ! Cells with potential interface on the face
           else
              if ( (VOFavg(i,j,k).lt.VOFlo .and. VOFavg(i+1,j,k).gt.VOFhi) .and. vol(i+1,j,k).gt.tiny(0.0_WP) .or. &
                   (VOFavg(i,j,k).gt.VOFhi .and. VOFavg(i+1,j,k).lt.VOFlo) .and. vol(i+1,j,k).gt.tiny(0.0_WP) ) then
                 band(i+1,j,k)=int(sign(1.0_WP,VOFavg(i+1,j,k)-0.5_WP))
              end if
           end if
        end do
     end do
  end if
  
  
  if (jproc.eq.1 .and. yper.ne.1) then
     do k = kmin_,kmax_
        do i = imin_,imax_
           j = jmin-1
           ! Deal with solid cells
           if (vol(i,j,k).lt.tiny(0.0_WP)) cycle

           ! Cells with interface inside
           if ( VOFavg(i,j,k).gt.VOFlo .and. &
                VOFavg(i,j,k).lt.VOFhi) then
              band(i,j,k)=0

              ! Cells with potential interface on the face
           else
              if ( (VOFavg(i,j,k).lt.VOFlo .and. VOFavg(i,j+1,k).gt.VOFhi) .and. vol(i,j+1,k).gt.tiny(0.0_WP) .or. &
                   (VOFavg(i,j,k).gt.VOFhi .and. VOFavg(i,j+1,k).lt.VOFlo) .and. vol(i,j+1,k).gt.tiny(0.0_WP) ) then
                 band(i,j+1,k)=int(sign(1.0_WP,VOFavg(i,j+1,k)-0.5_WP))
              end if
           end if
        end do
     end do
  end if

  ! First sweep to identify cells with interface
  do k=kmin_-1,kmax_
     do j=jmin_-1,jmax_
        do i=imin_-1,imax_

           ! Deal with solid cells
           if (vol(i,j,k).lt.tiny(0.0_WP)) cycle

           ! Cells with interface inside
           if ( VOFavg(i,j,k).gt.VOFlo .and. &
                VOFavg(i,j,k).lt.VOFhi) then 
              band(i,j,k)=0
              
           ! Cells with potential interface on the face
           else
              if ( (VOFavg(i,j,k).lt.VOFlo .and. VOFavg(i+1,j,k).gt.VOFhi) .and. vol(i+1,j,k).gt.tiny(0.0_WP) .or. &
                   (VOFavg(i,j,k).gt.VOFhi .and. VOFavg(i+1,j,k).lt.VOFlo) .and. vol(i+1,j,k).gt.tiny(0.0_WP) ) then
                 band(i:i+1,j,k)=int(sign(1.0_WP,VOFavg(i:i+1,j,k)-0.5_WP))
              end if
              if ( (VOFavg(i,j,k).lt.VOFlo .and. VOFavg(i,j+1,k).gt.VOFhi) .and. vol(i,j+1,k).gt.tiny(0.0_WP) .or. &
                   (VOFavg(i,j,k).gt.VOFhi .and. VOFavg(i,j+1,k).lt.VOFlo) .and. vol(i,j+1,k).gt.tiny(0.0_WP) ) then
                 band(i,j:j+1,k)=int(sign(1.0_WP,VOFavg(i,j:j+1,k)-0.5_WP))
              end if
              if ( (VOFavg(i,j,k).lt.VOFlo .and. VOFavg(i,j,k+1).gt.VOFhi) .and. vol(i,j,k+1).gt.tiny(0.0_WP) .or. &
                   (VOFavg(i,j,k).gt.VOFhi .and. VOFavg(i,j,k+1).lt.VOFlo) .and. vol(i,j,k+1).gt.tiny(0.0_WP) ) then
                 band(i,j,k:k+1)=int(sign(1.0_WP,VOFavg(i,j,k:k+1)-0.5_WP))
              end if
           end if
        end do
     end do
  end do

  call boundary_update_border_int(band,'+','ym')
  
  ! Sweep to identify the bands
  do n=1,nband
     ! For each band
     do k=kmin_,kmax_
        do j=jmin_,jmax_+joutlet
           do i=imin_,imax_+ioutlet
              ! At each points, check 6 neighbours
              if (vol(i,j,k).lt.tiny(1.0_WP)) cycle
              if (abs(band(i,j,k)).gt.n) then
                 ! Loop over 26 neighbors
                 do kk=k-1,k+1
                    do jj=j-1,j+1
                       do ii=i-1,i+1
                          ! Check for wall
                          if (vol(ii,jj,kk).lt.tiny(1.0_WP)) cycle
                          ! Extend the band
                          if (abs(band(ii,jj,kk)).eq.n-1) &
                               band(i,j,k)=int(sign(real(n,WP),VOFavg(i,j,k)-0.5_WP))
                       end do
                    end do
                 end do
              end if
           end do
        end do
     end do
     call boundary_update_border_int(band,'+','ym')
  end do


  ! Create mapping ============================================================
  
  ! Count the band size
  band_size=0
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (abs(band(i,j,k)).le.nband) then
              band_size(abs(band(i,j,k)))=band_size(abs(band(i,j,k)))+1
           end if
        end do
     end do
  end do
  
  ! Unstructured mapping
  if (associated(band_map)) deallocate(band_map); allocate(band_map(sum(band_size),3))
  band_count=0
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (abs(band(i,j,k)).le.nband) then
              band_count(abs(band(i,j,k)))=band_count(abs(band(i,j,k)))+1
              index=sum(band_size(0:abs(band(i,j,k))-1))+band_count(abs(band(i,j,k)))
              band_map(index,1)=i
              band_map(index,2)=j
              band_map(index,3)=k
           end if
        end do
     end do
  end do
  
  return
end subroutine multiphase_band_update

