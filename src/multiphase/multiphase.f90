! ================================ !
! NGA multiphase module:           !
! - VOF for interface transport    !
! _ GFM for discontinuous pressure !
! ================================ !
module multiphase
  use data
  use string
  use precision
  use config
  use geometry
  use partition
  use time_info
  use memory
  use simulation
  implicit none

  ! Flow type
  character(len=str_medium) :: flowType

  ! Fluid properties
  real(WP) :: rho_l,rho_g
  real(WP) :: mu_l,mu_g
  real(WP) :: sigma

  ! VOF tolerance and bounds
  real(WP), parameter :: VOFlo=0.0000000001_WP
  real(WP), parameter :: VOFhi=0.9999999999_WP

  ! Distance level set
  real(WP), dimension(:,:,:), allocatable :: G

  ! Curvature
  real(WP), dimension(:,:,:), allocatable :: curv,curvU,curvV,curvW
  integer,  dimension(:,:,:), allocatable :: hasCurv,curvFlag
  
  ! Band structure
  integer, dimension(:,:,:), allocatable :: band,bandold
  
  ! Density in velocity cells
  real(WP), dimension(:,:,:), allocatable :: rho_U,   rho_V,   rho_W
  real(WP), dimension(:,:,:), allocatable :: rho_Uold,rho_Vold,rho_Wold
  
  ! Pressure gradient (1/rho*grad(P)) 
  real(WP), dimension(:,:,:), allocatable :: gradPx,gradPy,gradPz

  ! Old VOF field
  real(WP), dimension(:,:,:,:), allocatable, target :: VOFold
  real(WP), dimension(:,:,:), allocatable, target :: VOFavgold

  ! VOF field in velocity cells
  real(WP), dimension(:,:,:), allocatable :: VOF_U,VOF_V,VOF_W

  ! Allocate subcell divergence
  real(WP), dimension(:,:,:,:), allocatable :: SCdiv

  ! PLIC reconstruction
  real(WP), dimension(:,:,:,:), allocatable :: normx   ,normy   ,normz   ,dist
  real(WP), dimension(:,:,:,:), allocatable :: normxold,normyold,normzold,distold

  ! Normal on flow mesh
  real(WP), dimension(:,:,:), allocatable :: normx_avg,normy_avg,normz_avg

  ! VOF average
  real(WP), dimension(:,:,:), allocatable, target :: VOFavg

  ! Second order velocity reconstructions: u'(x)=u(COM)+gradU.(x-COM)
  real(WP), dimension(:,:,:,:), allocatable, target :: COMu,COMv,COMw
  real(WP), dimension(:,:,:,:), allocatable, target :: gradU,gradV,gradW

  ! Scalar transport
  integer :: nband_CFL
  real(WP), dimension(:),       allocatable :: SCphase
  real(WP), dimension(:,:,:,:), allocatable :: COMliq,COMgas
  real(WP), dimension(:,:,:,:,:), allocatable :: gradSC

  ! Normal error
  real(WP), dimension(:,:,:), allocatable :: norm_err
  integer,  dimension(:,:,:), allocatable :: norm_itr

  ! Masks
  !   0 = Semi-Lagragian flux
  !   1 = Finite difference flux
  integer, dimension(:,:,:,:), allocatable :: cmask_x,cmask_y,cmask_z

  ! Monitoring
  real(WP) :: intVOFinit
  real(WP), dimension(:), allocatable :: intSCinit

  ! Factor to compute additonal fluxes at outlets
  integer :: ioutlet,joutlet
  
contains
  
  ! Height fraction with 2 values
  real(WP) function hf2(G1,G2)
    implicit none
    real(WP), intent(in) :: G1,G2
    if (G1.ge.0.0_WP.and.G2.ge.0.0_WP) then
       hf2=1.0_WP
    else if (G1.lt.0.0_WP.and.G2.lt.0.0_WP) then
       hf2=0.0_WP
    else
       hf2=(max(G1,0.0_WP)+max(G2,0.0_WP))/(abs(G1)+abs(G2))
    end if
  end function hf2
  
  ! Height fraction with 4 values
  real(WP) function hf4(G1,G2,G3,G4)
    implicit none
    real(WP), intent(in) :: G1,G2,G3,G4
    if (G1.ge.0.0_WP.and.G2.ge.0.0_WP.and.G3.ge.0.0_WP.and.G4.ge.0.0_WP) then
       hf4=1.0_WP
    else if (G1.lt.0.0_WP.and.G2.lt.0.0_WP.and.G3.lt.0.0_WP.and.G4.lt.0.0_WP) then
       hf4=0.0_WP
    else
       hf4=(max(G1,0.0_WP)+max(G2,0.0_WP)+max(G3,0.0_WP)+max(G4,0.0_WP))/(abs(G1)+abs(G2)+abs(G3)+abs(G4))
    end if
  end function hf4
  
end module multiphase

! =================== !
! Multiphase pre-init !
! =================== !
subroutine multiphase_pre_init
  use multiphase
  implicit none

  ! Test if used
  call parser_read('Use multiphase',use_multiphase,.false.)
  if (.not.use_multiphase) return
  
  ! Test if VOF fields are available
  if ( .not. multiphase_present ) &
       call die('Multiphase module requires VOF[1-8] in data.')
  
  ! Physical properties
  call parser_read('Liquid density',rho_l)
  call parser_read('Gas density',rho_g)
  call parser_read('Liquid viscosity',mu_l)
  call parser_read('Gas viscosity',mu_g)
  call parser_read('Surface tension',sigma)

  return
end subroutine multiphase_pre_init

! ========================= !
! Multiphase initialization !
! ========================= !
subroutine multiphase_init
  use multiphase
  use config
  use pressure
  use scalar
  use borders
  implicit none
  
  ! Test if used
  call parser_read('Use multiphase',use_multiphase,.false.)
  if (.not.use_multiphase) return
  
  ! Test if VOF fields are available
  if ( .not. multiphase_present ) &
       call die('Multiphase module requires VOF[1-8] in data.')

  ! Create and start the timer
  call timing_create('multiphase')
  call timing_start ('multiphase')

  ! Check for flow type
  call parser_read('Flow type',flowType,'NS')
  
  ! Allocate arrays
  allocate(G    (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(curv (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(curvU(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(curvV(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(curvW(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(hascurv(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(curvFlag(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))

  ! PLIC reconstruction on subcells
  allocate(dist (8,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(normx(8,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(normy(8,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(normz(8,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(distold (8,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(normxold(8,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(normyold(8,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(normzold(8,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))

  ! Normal on flow mesh
  allocate(normx_avg(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(normy_avg(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(normz_avg(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))

  ! Density
  allocate(rho_U   (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(rho_V   (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(rho_W   (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(rho_Uold(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(rho_Vold(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(rho_Wold(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  
  ! Pressure gradient
  allocate(gradPx(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); gradPx=0.0_WP
  allocate(gradPy(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); gradPy=0.0_WP
  allocate(gradPz(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); gradPz=0.0_WP

  ! Volume of fluid - average value in P cell
  allocate(VOFavg(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_));
  allocate(VOFavgold(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_));

  ! Old VOF
  allocate(VOFold(8,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_));

  ! VOF - velocity cells
  allocate(VOF_U(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(VOF_V(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(VOF_W(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))

  ! Center of Mass
  allocate(COMu(3,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(COMv(3,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(COMw(3,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))

  ! Conservative velocity gradients
  allocate(gradU(3,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(gradV(3,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(gradW(3,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))

  ! Allocate scalar flux arrays
  if (.not.allocated(SCphase)) then 
     allocate(SCphase(nscalar)); SCphase=0.0_WP
  end if
  if (.not.associated(SCold)) then
     allocate(SCold(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nscalar)); SCold=0.0_WP
  end if
  allocate(COMliq(3,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(COMgas(3,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(gradSC(3,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nscalar))

  ! Normal error and iteration array
  allocate(norm_err(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(norm_itr(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))

  ! Set outlet index modifications
  ioutlet=1
  joutlet=1
  if (noutlet .eq.0 .or. iproc.ne.npx .or. xper.eq.1) ioutlet=0
  if (noutlety.eq.0 .or. jproc.ne.npy .or. yper.eq.1) joutlet=0
  
  ! Initialize the flux module
  call multiphase_fluxes_init

  ! Initialize the VOF module
  call multiphase_vof_init

  ! Initialize the velocity module
  call multiphase_velocity_init

  ! Initialize multiphase monitoring
  call multiphase_monitor_init

  ! Prepare pressure solver
  pressure_cst_lap=2
  use_pcorr=.false.
    
  ! Initialize the gfm solver
  call multiphase_gfm_init
  
  ! Initialize EHD solver
  call multiphase_ehd_init

  ! Compute gradients for second order reconstructions (initial monitor)
  call multiphase_centerofmass
  call multiphase_reconstruct
  
  ! Stop the timer
  call timing_stop('multiphase')
  
  return
end subroutine multiphase_init

! ================== !
! Multiphase prestep !
! ================== !
subroutine multiphase_prestep
  use multiphase
  implicit none
  
  ! Test if used
  if (.not.use_multiphase) return

  ! Start timer
  call timing_start('multiphase')

  ! Multiphase VOF pre-step routines
  call multiphase_vof_prestep

  ! Velocity pre-step routines
  call multiphase_velocity_prestep

  ! Compute gradients for second order reconstructions
  call multiphase_centerofmass
  call multiphase_reconstruct

  ! Interpolate velocities
!  call interpolate_velocities

  ! Compute semi-Lagrangian fluxes
!  call multiphase_fluxes_calc

  ! Transport VOF
!  call multiphase_vof_transport

  ! Stop timer
  call timing_stop('multiphase')

  return
end subroutine multiphase_prestep


! =============== !
! Multiphase step !
! =============== !
subroutine multiphase_step
  use multiphase
  use math
  implicit none
 
  integer :: i,j,k
  
  ! Test if used
  if (.not.use_multiphase) return
  
  ! Start the timer
  call timing_start('multiphase')

  ! Set velocity based on flowType
  select case(flowType)
  case ('deformation')
     ! Set the velocity field - 2D deformation field test case
     do k=kmino_,kmaxo_
        do j=jmino_,jmaxo_
           do i=imino_,imaxo_
              U(i,j,k) = -2.0_WP*sin(Pi*x(i))**2*sin(Pi*ym(j))*cos(Pi*ym(j))*cos(Pi*(time-0.5_WP*dt)/8.0_WP)
              V(i,j,k) =  2.0_WP*sin(Pi*y(j))**2*sin(Pi*xm(i))*cos(Pi*xm(i))*cos(Pi*(time-0.5_WP*dt)/8.0_WP)
              W(i,j,k) =  0.0_WP
           end do
        end do
     end do

  case ('deformation3D')
     ! Set the velocity field - 3D deformation field test case
     do k=kmino_,kmaxo_
        do j=jmino_,jmaxo_
           do i=imino_,imaxo_
              U(i,j,k) = +2.0_WP*sin(   Pi*x (i))**2*sin(twoPi*ym(j))   *sin(twoPi*zm(k))   *cos(Pi*(time-0.5_WP*dt)/3.0_WP)
              V(i,j,k) = -       sin(twoPi*xm(i))   *sin(   Pi*y (j))**2*sin(twoPi*zm(k))   *cos(Pi*(time-0.5_WP*dt)/3.0_WP)
              W(i,j,k) = -       sin(twoPi*xm(i))   *sin(twoPi*ym(j))   *sin(   Pi*z (k))**2*cos(Pi*(time-0.5_WP*dt)/3.0_WP)
           end do
        end do
     end do
  end select

  ! Interpolate velocities
  call interpolate_velocities
  
  ! Compute semi-Lagrangian fluxes
  call multiphase_fluxes_calc

  
  ! Transport VOF
  call multiphase_vof_transport
  
  ! Run EHD solver
  call multiphase_ehd_step

  ! Stop the timer
  call timing_stop('multiphase')
  
  return
end subroutine multiphase_step

! =============================================== !
! Compute the density from the level set          !
! Careful - this is called before multiphase_init ! 
! =============================================== !
subroutine multiphase_density
  use multiphase
  implicit none
  
  integer :: i,j,k
  ! real(WP) :: VF
  
  ! Set NGA density to one
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           !VF=max(0.0_WP,min(1.0_WP,0.5_WP*(VOFold(i,j,k)+VOF(i,j,k))))
           !RHO(i,j,k)=VF*rho_l+(1.0_WP-VF)*rho_g
           RHO(i,j,k)=1.0_WP
        end do
     end do
  end do
  
  return
end subroutine multiphase_density


! =============================================== !
! Compute the dynamic viscosity from VOF          !
! Careful - this is called before multiphase_init !
! =============================================== !
subroutine multiphase_viscosity
  use multiphase
  use time_info
  implicit none
  
  integer :: i,j,k
  real(WP) :: VF
  
  ! Sharp kinematic viscosity for NGA
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           VF=max(0.0_WP,min(1.0_WP,0.125_WP*sum(VOF(:,i,j,k))))
           !VISC(i,j,k)=mu_g*mu_l/(mu_g*VF + mu_l*(1.0_WP-VF)+epsilon(1.0_WP))
           VISC(i,j,k)=mu_g*(1.0_WP-VF) + mu_l*VF
        end do
     end do
  end do
  VISCmol=VISC
  
  return
end subroutine multiphase_viscosity

! ============================= !
! Monitor the multiphase solver !
! ============================= !
subroutine multiphase_monitor_init
  use multiphase
  use math
  implicit none

  integer :: i,j,k,n
  real(WP) :: my_intVOF
  real(WP), dimension(nscalar) :: my_intSC

  if (trim(scalar_scheme).eq.'multiphase') then
     call monitor_create_file_step('multiphase',6+nscalar)
  else
     call monitor_create_file_step('multiphase',6)
  end if
  call monitor_set_header(1,'Liq vol change','r')
  call monitor_set_header(2,'S-Tension dt','r')
  call monitor_set_header(3,'max(VOF)','r')
  call monitor_set_header(4,'min(VOF)','r')
  call monitor_set_header(5,'Subcell divg','r')
  call monitor_set_header(6,'Surface','r')
  if (trim(scalar_scheme).eq.'multiphase') then
     do n=1,nscalar
        call monitor_set_header(6+n,SC_name(n),'r')
     end do
  end if
  ! Compute integrals of initial condition
  my_intVOF=0.0_WP
  my_intSC =0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           my_intVOF  =my_intVOF  +            VOFavg(i,j,k)*vol(i,j,k)
           if (nscalar.gt.0) &
                my_intSC(:)=my_intSC(:)+SC(i,j,k,:)*VOFavg(i,j,k)*vol(i,j,k)
        end do
     end do
  end do
  call parallel_sum(my_intVOF,intVOFinit)
  allocate(intSCinit(nscalar))
  call parallel_sum(my_intSC,intSCinit)

  return
end subroutine multiphase_monitor_init

subroutine multiphase_monitor
  use multiphase
  use math
  implicit none
  
  integer  :: i,j,k,n
  real(WP) :: my_intVOF,intVOF
  real(WP) :: my_dtST,dtST
  real(WP) :: maxVOF,minVOF,maxSCdiv, surf_tot
  real(WP), dimension(nscalar) :: my_intSC,intSC
  real(WP) :: L2,Li,myL2,myLi
  
  ! real(WP), dimension(3,8) :: pos
  ! real(WP), dimension(  8) :: val
  ! real(WP), dimension(  3) :: com_
  ! real(WP), dimension(  3) :: coms_
  ! real(WP), dimension(  3) :: nvec
  ! real(WP) ::  my_surf,my_surf_tot, my_vol

  if (.not.use_multiphase) return
  
  ! Useful quantities to monitor
  my_intVOF=0.0_WP
  my_intSC=0.0_WP
  my_dtST=huge(1.0_WP)
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           my_intVOF  =my_intVOF  +            VOFavg(i,j,k)*vol(i,j,k)
           if (nscalar.gt.0) &
                my_intSC(:)=my_intSC(:)+SC(i,j,k,:)*VOFavg(i,j,k)*vol(i,j,k)
           my_dtST=min(my_dtST,sqrt(rho_l*meshsize(i,j,k)**3/(2.0_WP*Pi*sigma+epsilon(1.0_WP))))
        end do
     end do
  end do

  call parallel_sum(my_intVOF,intVOF)
  call parallel_sum(my_intSC,intSC)
  call parallel_min(my_dtST,dtST)
  call parallel_max(maxval(VOF),maxVOF)
  call parallel_min(minval(VOF),minVOF)
  call parallel_max(maxval(abs(SCdiv)),maxSCdiv)

  ! Surface data
!  my_surf_tot=0.0_WP
!  do k=kmin_,kmax_
!    do j=jmin_,jmax_
!      do i=imin_,imax_
!      ! Cells with enough volume only
!      if (VOFavg(i,j,k).lt.VOFlo) cycle
!      if (VOFavg(i,j,k).gt.VOFhi) cycle
!      ! Use marching tets to obtain surface-in-cell
!      pos=reshape((/ &
!        x(i  ),y(j  ),z(k  ), &
!        x(i+1),y(j  ),z(k  ), &
!        x(i  ),y(j+1),z(k  ), &
!        x(i+1),y(j+1),z(k  ), &
!        x(i  ),y(j  ),z(k+1), &
!        x(i+1),y(j  ),z(k+1), &
!        x(i  ),y(j+1),z(k+1), &
!        x(i+1),y(j+1),z(k+1) /),(/3,8/))
!      val=-(normx(i,j,k)*pos(1,:)+normy(i,j,k)*pos(2,:)+normz(i,j,k)*pos(3,:)-dist(i,j,k))
!      call cut_3cube(0.0_WP,pos,val,my_vol,com_,my_surf,coms_,nvec)
!      my_surf_tot=my_surf_tot+my_surf
!      end do
!    end do
!  end do
!  call parallel_sum(my_surf_tot,surf_tot)

  ! Transfer to monitor
  call monitor_select_file("multiphase")
  call monitor_set_single_value(1,intVOF-intVOFinit)
  call monitor_set_single_value(2,dtST)
  call monitor_set_single_value(3,maxVOF)
  call monitor_set_single_value(4,minVOF)
  call monitor_set_single_value(5,maxSCdiv)
  call monitor_set_single_value(6,surf_tot)
  if (trim(scalar_scheme).eq.'multiphase') then
     do n=1,nscalar
        call monitor_set_single_value(6+n,intSC(n)-intSCinit(n),'r')
     end do
  end if

  return
end subroutine multiphase_monitor

! =========================== !
! Monitor conserved variables !
! =========================== !
subroutine multiphase_monitor_conservation
  use multiphase
  use metric_generic
  implicit none
  integer :: i,j,k,isc
  real(WP) :: myrho,myU,myV,myW
  real(WP) :: buf1,buf2,buf3,buf4,buf5
  real(WP) :: mass,mom_x,mom_y,mom_z,energy
  real(WP), dimension(nscalar) :: buf6,cons_sc

  ! Initialize variables
  buf1 = 0.0_WP
  buf2 = 0.0_WP
  buf3 = 0.0_WP
  buf4 = 0.0_WP
  buf5 = 0.0_WP
  buf6 = 0.0_WP

  ! Compute quantites
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Primative variables in this cell
           myrho=(rho_l*VOFavg(i,j,k) + rho_g*(1.0_WP-VOFavg(i,j,k)))
           myU = sum(interp_u_xm(i,j,:)*U(i-st1:i+st2,j,k))
           myV = sum(interp_v_ym(i,j,:)*V(i,j-st1:j+st2,k))
           myW = sum(interp_w_zm(i,j,:)*W(i,j,k-st1:k+st2))
           ! Update conserved varibles
           buf1 = buf1 + vol(i,j,k) * myrho
           ! buf2 = buf2 + vol(i,j,k) * myrho*myU
           ! buf3 = buf3 + vol(i,j,k) * myrho*myV
           ! buf4 = buf4 + vol(i,j,k) * myrho*myW
           buf2 = buf2 + vol(i,j,k) * rho_U(i,j,k)*U(i,j,k)
           buf3 = buf3 + vol(i,j,k) * rho_V(i,j,k)*V(i,j,k)
           buf4 = buf4 + vol(i,j,k) * rho_W(i,j,k)*W(i,j,k)
           buf5 = buf5 + vol(i,j,k) * 0.5_WP*myrho*( myU**2 + myV**2 + myW**2 )
           do isc=1,nscalar
              if (nint(SCphase(isc)).eq.1) then
                 buf6(isc) = buf6(isc) + vol(i,j,k)*(       VOFavg(i,j,k))*SC(i,j,k,isc)
              else
                 buf6(isc) = buf6(isc) + vol(i,j,k)*(1.0_WP-VOFavg(i,j,k))*SC(i,j,k,isc)
              end if
           end do                 
        end do
     end do
  end do

  ! Communicate
  call parallel_sum(buf1,mass  )
  call parallel_sum(buf2,mom_x )
  call parallel_sum(buf3,mom_y )
  call parallel_sum(buf4,mom_z )
  call parallel_sum(buf5,energy)
  do isc=1,nscalar
     call parallel_sum(buf6(isc),cons_sc(isc))
  end do

  ! Transfer to monitor
  call monitor_set_single_value(1,mass)
  call monitor_set_single_value(2,mom_x)
  call monitor_set_single_value(3,mom_y)
  call monitor_set_single_value(4,mom_z)
  call monitor_set_single_value(5,energy)
  do isc=1,nscalar
     call monitor_set_single_value(5+isc,cons_sc(isc))
  end do

  return
end subroutine multiphase_monitor_conservation
