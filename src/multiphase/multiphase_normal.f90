module multiphase_normal
  use multiphase

  ! Dummy module

end module multiphase_normal

! ==================================== !
! Compute interface normal using LVIRA !
!  Pilliod and Puckett, JCP 199, 2004  !
! ==================================== !
subroutine multiphase_normal_elvira
  use multiphase_normal
  use multiphase_band
  implicit none
  integer :: i,j,k,s
  real(WP) :: norm

  ! Zero normals
  normx=0.0_WP
  normy=0.0_WP
  normz=0.0_WP

  ! Loop over cells
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           do s=1,8
              if (VOF(s,i,j,k).ge.VOFlo .and. VOF(s,i,j,k).le.VOFhi) then
                 call multiphase_elvira_calc(s,i,j,k)
              end if
           end do
        end do
     end do
  end do
  
  ! Communicate normal
  do s=1,8
     call communication_border(normx(s,:,:,:))
     call communication_border(normy(s,:,:,:))
     call communication_border(normz(s,:,:,:))
     call boundary_symmetry_alldir(normx(s,:,:,:),normy(s,:,:,:),normz(s,:,:,:))
  end do

  ! Compute average normal
  normx_avg=0.0_WP
  normy_avg=0.0_WP
  normz_avg=0.0_WP
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           do s=1,8
              normx_avg(i,j,k)=normx_avg(i,j,k)+normx(s,i,j,k)
              normy_avg(i,j,k)=normy_avg(i,j,k)+normy(s,i,j,k)
              normz_avg(i,j,k)=normz_avg(i,j,k)+normz(s,i,j,k)
           end do
           ! Normalize
           norm=max(epsilon(1.0_WP),sqrt(normx_avg(i,j,k)**2+normy_avg(i,j,k)**2+normz_avg(i,j,k)**2))
           normx_avg(i,j,k)=normx_avg(i,j,k)/norm
           normy_avg(i,j,k)=normy_avg(i,j,k)/norm
           normz_avg(i,j,k)=normz_avg(i,j,k)/norm
        end do
     end do
  end do
  
  return
end subroutine multiphase_normal_elvira
   
subroutine multiphase_elvira_calc(s,i,j,k)
  use multiphase_normal
  use multiphase_plic
  use multiphase_fluxes
  implicit none
  
  integer, intent(in) :: s,i,j,k
  real(WP) :: min_error
  real(WP) :: my_normx,my_normy,my_normz  
  integer :: st1,st2
  integer :: cm,im,jm,km 
  integer :: cp,ip,jp,kp
  
  min_error=huge(1.0_WP)

  ! X-Y Stencils
  if (nz.gt.1) then 
     do st1=1,3
        ! Set x-indices
        select case(st1)
        case (1)  ! i-1 and i
           cm=sc_sx(s);   cp=s
           im=i+sc_im(s); ip=i
        case (2)  ! i-1 and i+1
           cm=sc_sx(s);   cp=sc_sx(s)
           im=i+sc_im(s); ip=i+sc_ip(s)
        case (3)  ! i and i+1
           cm=s;          cp=sc_sx(s)
           im=i;          ip=i+sc_ip(s)
        end select
        jm=j;     jp=j
        km=k;     kp=k
        my_normx=-((VOF(sc_sz(cp),ip,jp,kp+sc_km(cp))*0.5_WP*dz    &
                   +VOF(cp       ,ip,jp,kp          )*0.5_WP*dz    &
                   +VOF(sc_sz(cp),ip,jp,kp+sc_kp(cp))*0.5_WP*dz )  &
                 - (VOF(sc_sz(cm),im,jm,km+sc_km(cm))*0.5_WP*dz    &
                   +VOF(cm       ,im,jm,km          )*0.5_WP*dz    &
                   +VOF(sc_sz(cm),im,jm,km+sc_kp(cm))*0.5_WP*dz )) &
                 / ( (xm(ip)+subx(cp)*dx(ip)) - (xm(im)+subx(cm)*dx(im)) )

        do st2=1,3
           ! Set z-indices
           select case(st2)
           case (1)  ! j-1 and j
              cm=sc_sy(s);   cp=s
              jm=j+sc_jm(s); jp=j
           case (2)  ! j-1 and j+1
              cm=sc_sy(s);   cp=sc_sy(s)
              jm=j+sc_jm(s); jp=j+sc_jp(s)
           case (3)  ! j and j+1
              cm=s;          cp=sc_sy(s)
              jm=j;          jp=j+sc_jp(s)
           end select
           im=i;     ip=i
           km=k;     kp=k
           my_normy=-((VOF(sc_sz(cp),ip,jp,kp+sc_km(cp))*0.5_WP*dz    &
                      +VOF(cp       ,ip,jp,kp          )*0.5_WP*dz    &
                      +VOF(sc_sz(cp),ip,jp,kp+sc_kp(cp))*0.5_WP*dz )  &
                    - (VOF(sc_sz(cm),im,jm,km+sc_km(cm))*0.5_WP*dz    &
                      +VOF(cm       ,im,jm,km          )*0.5_WP*dz    &
                      +VOF(sc_sz(cm),im,jm,km+sc_kp(cm))*0.5_WP*dz )) &
                    / ( (ym(jp)+suby(cp)*dy(jp)) - (ym(jm)+suby(cm)*dy(jm)) )

           ! Normal direction
           if (VOF(sc_sz(s),i,j,k+sc_kp(s)).gt.VOF(sc_sz(s),i,j,k+sc_km(s))) then
              my_normz=-1.0_WP
           else 
              my_normz=+1.0_WP
           end if

           ! Check normal
           call check_normal(s,i,j,k)

        end do
     end do
  end if

  ! X-Z Stencils
  if (ny.gt.1) then
     do st1=1,3
        ! Set x-indices
        select case(st1)
        case (1)  ! i-1 and i
           cm=sc_sx(s);   cp=s
           im=i+sc_im(s); ip=i
        case (2)  ! i-1 and i+1
           cm=sc_sx(s);   cp=sc_sx(s)
           im=i+sc_im(s); ip=i+sc_ip(s)
        case (3)  ! i and i+1
           cm=s;          cp=sc_sx(s)
           im=i;          ip=i+sc_ip(s)
        end select
        jm=j;     jp=j
        km=k;     kp=k
        my_normx=-((VOF(sc_sy(cp),ip,jp+sc_jm(cp),kp)*0.5_WP*dy(jp+sc_jm(cp))    &
                   +VOF(cp       ,ip,jp          ,kp)*0.5_WP*dy(jp          )    &
                   +VOF(sc_sy(cp),ip,jp+sc_jp(cp),kp)*0.5_WP*dy(jp+sc_jp(cp)) )  &
                 - (VOF(sc_sy(cm),im,jm+sc_jm(cm),km)*0.5_WP*dy(jm+sc_jm(cm))    &
                   +VOF(cm       ,im,jm          ,km)*0.5_WP*dy(jm          )    &
                   +VOF(sc_sy(cm),im,jm+sc_jp(cm),km)*0.5_WP*dy(jm+sc_jp(cm)) )) &
                 / ( (xm(ip)+subx(cp)*dx(ip)) - (xm(im)+subx(cm)*dx(im)) )

        do st2=1,3
           ! Set z-indices
           select case(st2)
           case (1)  ! k-1 and k
              cm=sc_sz(s);   cp=s
              km=k+sc_km(s); kp=k
           case (2)  ! k-1 and k+1
              cm=sc_sz(s);   cp=sc_sz(s)
              km=k+sc_km(s); kp=k+sc_kp(s)
           case (3)  ! k and k+1
              cm=s;          cp=sc_sz(s)
              km=k;          kp=k+sc_kp(s)
           end select
           im=i;     ip=i
           jm=j;     jp=j
           my_normz=-((VOF(sc_sy(cp),ip,jp+sc_jm(cp),kp)*0.5_WP*dy(jp+sc_jm(cp))    &
                      +VOF(cp       ,ip,jp          ,kp)*0.5_WP*dy(jp          )    &
                      +VOF(sc_sy(cp),ip,jp+sc_jp(cp),kp)*0.5_WP*dy(jp+sc_jp(cp)) )  &
                    - (VOF(sc_sy(cm),im,jm+sc_jm(cm),km)*0.5_WP*dy(jm+sc_jm(cm))    &
                      +VOF(cm       ,im,jm          ,km)*0.5_WP*dy(jm          )    &
                      +VOF(sc_sy(cm),im,jm+sc_jp(cm),km)*0.5_WP*dy(jm+sc_jp(cm)) )) &
                      / ( (zm(kp)+subz(cp)*dz) - (zm(km)+subz(cm)*dz) )

           ! Normal direction
           if (VOF(sc_sy(s),i,j+sc_jp(s),k).gt.VOF(sc_sy(s),i,j+sc_jm(s),k)) then
              my_normy=-1.0_WP
           else 
              my_normy=+1.0_WP
           end if


           ! Check normal
           call check_normal(s,i,j,k)

        end do
     end do
  end if

  ! Y-Z Stencils
  if (nx.gt.1) then
     do st1=1,3
        ! Set y-indices
        select case(st1)
        case (1)  ! j-1 and j
           cm=sc_sy(s);   cp=s
           jm=j+sc_jm(s); jp=j
        case (2)  ! j-1 and j+1
           cm=sc_sy(s);   cp=sc_sy(s)
           jm=j+sc_jm(s); jp=j+sc_jp(s)
        case (3)  ! j and j+1
           cm=s;          cp=sc_sy(s)
           jm=j;          jp=j+sc_jp(s)
        end select
        im=i;     ip=i
        km=k;     kp=k
        my_normy=-((VOF(sc_sx(cp),ip+sc_im(cp),jp,kp)*0.5_WP*dx(ip+sc_im(cp))    &
                   +VOF(cp       ,ip          ,jp,kp)*0.5_WP*dx(ip          )    &
                   +VOF(sc_sx(cp),ip+sc_ip(cp),jp,kp)*0.5_WP*dx(ip+sc_ip(cp)) )  &
                 - (VOF(sc_sx(cm),im+sc_im(cm),jm,km)*0.5_WP*dx(im+sc_im(cm))    &
                   +VOF(cm       ,im          ,jm,km)*0.5_WP*dx(im          )    &
                   +VOF(sc_sx(cm),im+sc_ip(cm),jm,km)*0.5_WP*dx(im+sc_ip(cm)) )) &
                 / ( (ym(jp)+suby(cp)*dy(jp)) - (ym(jm)+suby(cm)*dy(jm)) )

        do st2=1,3
           ! Set z-indices
           select case(st2)
           case (1)  ! k-1 and k
              cm=sc_sz(s);   cp=s
              km=k+sc_km(s); kp=k
           case (2)  ! k-1 and k+1
              cm=sc_sz(s);   cp=sc_sz(s)
              km=k+sc_km(s); kp=k+sc_kp(s)
           case (3)  ! k and k+1
              cm=s;          cp=sc_sz(s)
              km=k;          kp=k+sc_kp(s)
           end select
           im=i;     ip=i
           jm=j;     jp=j
           my_normz=-((VOF(sc_sx(cp),ip+sc_im(cp),jp,kp)*0.5_WP*dx(ip+sc_im(cp))    &
                      +VOF(cp       ,ip          ,jp,kp)*0.5_WP*dx(ip          )    &
                      +VOF(sc_sx(cp),ip+sc_ip(cp),jp,kp)*0.5_WP*dx(ip+sc_ip(cp)) )  &
                    - (VOF(sc_sx(cm),im+sc_im(cm),jm,km)*0.5_WP*dx(im+sc_im(cm))    &
                      +VOF(cm       ,im          ,jm,km)*0.5_WP*dx(im          )    &
                      +VOF(sc_sx(cm),im+sc_ip(cm),jm,km)*0.5_WP*dx(im+sc_ip(cm)) )) &
                    / ( (zm(kp)+subz(cp)*dz) - (zm(km)+subz(cm)*dz) )

           ! Normal direction
           if (VOF(sc_sx(s),i+sc_ip(s),j,k).gt.VOF(sc_sx(s),i+sc_im(s),j,k)) then
              my_normx=-1.0_WP
           else 
              my_normx=+1.0_WP
           end if

           ! Check normal
           call check_normal(s,i,j,k)


        end do
     end do
  end if
  
  return

contains
  subroutine check_normal(s,i,j,k)
    use multiphase_fluxes
    implicit none
    integer, intent(in) :: s,i,j,k
    real(WP) :: tmp_normx,tmp_normy,tmp_normz
    real(WP) :: tmp_error
    integer :: ii,jj,kk,ss,s1,s2
    integer :: n,case,tet,v1,v2
    integer :: xdir,ydir,zdir
    real(WP) :: tmp_VOF,mu
    real(WP) :: norm,tmp_dist,tmp_vol
    real(WP), dimension(3) :: cen,p1,p2,p3,p4,p5,p6,p7,p8
    real(WP), dimension(3) :: a,b,c
    real(WP), dimension(3,4,6) :: tets
    real(WP), dimension(3,8) :: vert
    real(WP), dimension(4) :: d

    ! Initialize error
    tmp_error=0.0_WP

    ! Transfer normal
    tmp_normx=my_normx
    tmp_normy=my_normy
    tmp_normz=my_normz

    ! Normalize normal
    norm=sqrt(tmp_normx**2+tmp_normy**2+tmp_normz**2)
    tmp_normx=tmp_normx/norm
    tmp_normy=tmp_normy/norm
    tmp_normz=tmp_normz/norm 

    ! Form PLIC
    tmp_dist=calc_dist(s,i,j,k, &
                      tmp_normx,tmp_normy,tmp_normz,VOF(s,i,j,k))

    ! Loop over neighbors and compute error --------------------------
    do zdir=-1,1
       select case(zdir)
       case (-1); kk=k+sc_km(s); s1=sc_sz(s)
       case ( 0); kk=k         ; s1=s
       case (+1); kk=k+sc_kp(s); s1=sc_sz(s)
       end select
       do ydir=-1,1
          select case(ydir)
          case (-1); jj=j+sc_jm(s1); s2=sc_sy(s1)
          case ( 0); jj=j          ; s2=s1
          case (+1); jj=j+sc_jp(s1); s2=sc_sy(s1)
          end select
          do xdir=-1,1
             select case(xdir)
             case (-1); ii=i+sc_im(s2); ss=sc_sx(s2)
             case ( 0); ii=i          ; ss=s2
             case (+1); ii=i+sc_ip(s2); ss=sc_sx(s2)
             end select

             tmp_VOF=0.0_WP
             ! Subcell center
             cen=(/xm(ii)+subx(ss)*dx(ii), &
                   ym(jj)+suby(ss)*dy(jj), &
                   zm(kk)+subz(ss)*dz     /)
             ! Vertices of subcell
             p1 = cen + (/-0.25_WP*dx(ii),-0.25_WP*dy(jj),-0.25_WP*dz/)
             p2 = cen + (/+0.25_WP*dx(ii),-0.25_WP*dy(jj),-0.25_WP*dz/)
             p3 = cen + (/-0.25_WP*dx(ii),+0.25_WP*dy(jj),-0.25_WP*dz/)
             p4 = cen + (/+0.25_WP*dx(ii),+0.25_WP*dy(jj),-0.25_WP*dz/)
             p5 = cen + (/-0.25_WP*dx(ii),-0.25_WP*dy(jj),+0.25_WP*dz/)
             p6 = cen + (/+0.25_WP*dx(ii),-0.25_WP*dy(jj),+0.25_WP*dz/)
             p7 = cen + (/-0.25_WP*dx(ii),+0.25_WP*dy(jj),+0.25_WP*dz/)
             p8 = cen + (/+0.25_WP*dx(ii),+0.25_WP*dy(jj),+0.25_WP*dz/)
             ! Make five tets
             tets(:,1,1)=p1; tets(:,2,1)=p2; tets(:,3,1)=p4; tets(:,4,1)=p6
             tets(:,1,2)=p1; tets(:,2,2)=p4; tets(:,3,2)=p3; tets(:,4,2)=p7
             tets(:,1,3)=p1; tets(:,2,3)=p5; tets(:,3,3)=p6; tets(:,4,3)=p7
             tets(:,1,4)=p4; tets(:,2,4)=p7; tets(:,3,4)=p6; tets(:,4,4)=p8
             tets(:,1,5)=p1; tets(:,2,5)=p4; tets(:,3,5)=p7; tets(:,4,5)=p6
             do tet=1,5
                ! Copy verts
                do n=1,4
                   vert(:,n)=tets(:,n,tet)
                end do
                ! Calculate distance to PLIC reconstruction
                do n=1,4
                   d(n)=tmp_normx*vert(1,n)+tmp_normy*vert(2,n)+tmp_normz*vert(3,n)-tmp_dist
                end do
                ! Find cut case
                case=1+int(0.5_WP+sign(0.5_WP,d(1)))+&
                     2*int(0.5_WP+sign(0.5_WP,d(2)))+&
                     4*int(0.5_WP+sign(0.5_WP,d(3)))+&
                     8*int(0.5_WP+sign(0.5_WP,d(4)))
                ! Create interpolated vertices on cut plane
                do n=1,cut_nvert(case)
                   v1=cut_v1(n,case); v2=cut_v2(n,case)
                   mu=min(1.0_WP,max(0.0_WP,-d(v1)/(sign(abs(d(v2)-d(v1))+epsilon(1.0_WP),d(v2)-d(v1)))))
                   vert(:,4+n)=(1.0_WP-mu)*vert(:,v1)+mu*vert(:,v2)
                end do
                ! Create new tets on liquid side
                do n=cut_ntets(case),cut_nntet(case),-1
                   ! Compute volume
                   a=vert(:,cut_vtet(1,n,case)) - vert(:,cut_vtet(4,n,case))
                   b=vert(:,cut_vtet(2,n,case)) - vert(:,cut_vtet(4,n,case))
                   c=vert(:,cut_vtet(3,n,case)) - vert(:,cut_vtet(4,n,case))
                   tmp_vol=abs(a(1)*(b(2)*c(3)-c(2)*b(3)) &
                        -      a(2)*(b(1)*c(3)-c(1)*b(3)) &
                        +      a(3)*(b(1)*c(2)-c(1)*b(2)))/6.0_WP
                   ! Update VOF in this cell
                   tmp_VOF=tmp_VOF+tmp_vol*0.125_WP*dxi(ii)*dyi(jj)*dzi
                end do
             end do

             ! Update L2 error
             tmp_error=tmp_error+(VOF(ss,ii,jj,kk)-tmp_VOF)**2
          end do
       end do
    end do

    ! Check if this is the best normal ------------------------------
    if (tmp_error.lt.min_error) then
       min_error=tmp_error
       normx(s,i,j,k)=tmp_normx
       normy(s,i,j,k)=tmp_normy
       normz(s,i,j,k)=tmp_normz
    end if
    
    return
  end subroutine check_normal

end subroutine multiphase_elvira_calc

