! =========================== !
! Electrohydrodynamics module !
! =========================== !
module multiphase_ehd   
  use multiphase_vof
  use pressure
  use metric_velocity_conv
  implicit none
  
  ! Whether we use EHD   
  logical :: use_ehd
  
  ! Electric field
  real(WP), dimension(:,:,:), allocatable :: Ex
  real(WP), dimension(:,:,:), allocatable :: Ey
  real(WP), dimension(:,:,:), allocatable :: Ez
  
  ! Electric potential
  real(WP), dimension(:,:,:), allocatable :: phi
  
  ! Volumetric charge density index
  integer :: isc_Q
  logical :: varQ
  
  ! Volumetric charge density
  real(WP) :: q0
  
  ! Permittivity
  real(WP), parameter :: eps_0=8.8541878176e-12_WP
  real(WP) :: reps_l,reps_g
  real(WP) ::  eps_l, eps_g
  real(WP) ::  eps_r

  ! Ionic mobility
  real(WP) :: mu_i
  
  ! Molecular diffusivity
  real(WP) :: D_i
  
  ! Potential for Dirichlet BC's
  real(WP) :: phi_top,phi_bottom
  
  ! Quick scalar transport coefficients
  real(WP), dimension(:,:,:,:), allocatable :: ehd_quick_xp,ehd_quick_yp,ehd_quick_zp
  real(WP), dimension(:,:,:,:), allocatable :: ehd_quick_xm,ehd_quick_ym,ehd_quick_zm

  ! Face diffusivity
  real(WP), dimension(:,:,:), allocatable :: DIFFx,DIFFy,DIFFz
  
contains
  function wa_adjx(i,j,k)
    implicit none
    integer, intent(in) :: i,j,k
    real(WP) :: wa_adjx
    wa_adjx=((min(VOFavg(i-1,j,k),VOFavg(i,j,k))))**2
  end function wa_adjx

  function wa_adjy(i,j,k)
    implicit none
    integer, intent(in) :: i,j,k
    real(WP) :: wa_adjy
    wa_adjy=((min(VOFavg(i,j-1,k),VOFavg(i,j,k))))**2
  end function wa_adjy

  function wa_adjz(i,j,k)
    implicit none
    integer, intent(in) :: i,j,k
    real(WP) :: wa_adjz
    wa_adjz=((min(VOFavg(i,j,k-1),VOFavg(i,j,k))))**2
  end function wa_adjz
  
end module multiphase_ehd


! ============================ !
! Initialization of EHD module !
! ============================ !
subroutine multiphase_ehd_init
  use multiphase_ehd
  use parser
  implicit none
  
  integer :: isc
  
  ! Check if we're using EHD
  call parser_read('Use EHD',use_ehd,.false.)
  if (.not.use_ehd) return
  
  phi_top   =79846.4_WP
  phi_bottom=80000.0_WP
  
  ! Read permittivity
  call parser_read('Gas rel. perm.',reps_g)
  call parser_read('Liquid rel. perm.',reps_l)
  eps_l=reps_l*eps_0
  eps_g=reps_g*eps_0
  eps_r=reps_l/reps_g

  ! Read other parameters
  call parser_read('Ionic mobility',mu_i,0.0_WP)
  call parser_read('Molecular diffusivity',D_i,0.0_WP)
 
  ! Create new file to monitor electric potential convergence
  call monitor_create_file_step('ehd',5)
  call monitor_set_header(1,'iter','i')
  call monitor_set_header(2,'res','r')
  call monitor_set_header(3,'Ex','r')
  call monitor_set_header(4,'Ey','r')
  call monitor_set_header(5,'Ez','r')

  ! Initial values
  call monitor_select_file('ehd')
  call monitor_set_single_value(1,0.0_WP)
  call monitor_set_single_value(2,0.0_WP)
  call monitor_set_single_value(3,0.0_WP)
  call monitor_set_single_value(4,0.0_WP)
  call monitor_set_single_value(5,0.0_WP)
  
  ! Allocate memory
  allocate(Ex (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(Ey (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(Ez (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(phi(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  
  ! Get the charge density array index in the data structure
  isc_Q=0
  varQ=.false.
  do isc=1,nscalar
     if (trim(SC_name(isc)).eq.'L_Q') then
        isc_Q = isc
        varQ=.true.
     end if
  end do
  
  ! Check if constant charge
  if (.not.varQ) call parser_read('Charge density',q0)
  
  ! Initialize QUICK scalar transport
  if (varQ) then
     call scalar_quick_init
     allocate(ehd_quick_xp(imin_-st1:imax_+st2,jmin_-st1:jmax_+st2,kmin_-st1:kmax_+st2,-2:+0))
     allocate(ehd_quick_yp(imin_-st1:imax_+st2,jmin_-st1:jmax_+st2,kmin_-st1:kmax_+st2,-2:+0))
     allocate(ehd_quick_zp(imin_-st1:imax_+st2,jmin_-st1:jmax_+st2,kmin_-st1:kmax_+st2,-2:+0))
     allocate(ehd_quick_xm(imin_-st1:imax_+st2,jmin_-st1:jmax_+st2,kmin_-st1:kmax_+st2,-1:+1))
     allocate(ehd_quick_ym(imin_-st1:imax_+st2,jmin_-st1:jmax_+st2,kmin_-st1:kmax_+st2,-1:+1))
     allocate(ehd_quick_zm(imin_-st1:imax_+st2,jmin_-st1:jmax_+st2,kmin_-st1:kmax_+st2,-1:+1))     
  end if

  ! Allocate face diffusivity arrays
  if (varQ) then
     allocate(DIFFx(imin_-st1:imax_+st2,jmin_-st1:jmax_+st2,kmin_-st1:kmax_+st2))
     allocate(DIFFy(imin_-st1:imax_+st2,jmin_-st1:jmax_+st2,kmin_-st1:kmax_+st2))
     allocate(DIFFz(imin_-st1:imax_+st2,jmin_-st1:jmax_+st2,kmin_-st1:kmax_+st2))
  end if

  ! Initialize seperate electric potential mesh
  call multiphase_emesh_init
  
  return
end subroutine multiphase_ehd_init


! ================== !
! EHD Poisson solver !
! ================== !
subroutine multiphase_ehd_step
  use multiphase_ehd
  use multiphase_velocity
  use masks
  use time_info
  use bbmg
  implicit none
  
  integer  :: i,j,k
  real(WP) :: qi
  
  ! Test if used
  if (.not.use_multiphase .or. .not.use_ehd) return

  ! Compute electric potential on separate mesh
  ! (used as BCs for standard electric potential Poisson equation)
  call multiphase_emesh_step
    
  ! Set RHS of Poisson equation
  if (.not.varQ) then
     ! Get charge from VOFavg
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              RP(i,j,k)=-q0*VOFavg(i,j,k)
           end do
        end do
     end do
  else
     ! Get charge from transported scalar
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              RP(i,j,k)=-SC(i,j,k,isc_Q)*VOFavg(i,j,k)
           end do
        end do
     end do
  end if
  
  ! Recompute Laplacian
  call multiphase_ehd_laplacian
  
  ! Initial guess
  DP=0.0_WP
  
  ! Rescale operator
  call pressure_fourier
  call pressure_rescale
  
  ! Prepare preconditioner
  if (.not.use_HYPRE) then
     select case(trim(pressure_precond))
     case('none')
        ! Nothing to do
     case('diag')
        ! Nothing to do
     case('tridiag')
        ! Nothing to do
     case('icc')
        call pressure_icc_diag
     end select
  end if
  
  ! Prepare the RHS
  call pressure_RHS
  
  ! Solve
  bbmg_normalize=.true.
  call pressure_SOLVE
  bbmg_normalize=.false.
  
  ! Update boundaries of DP
  call boundary_update_border(DP,'+','ym')
  
  ! Transfer back electric potential
  phi=DP
  
  ! Compute electric field
  call multiphase_ehd_gradient
  
  ! Create Coulomb force
  if (.not.varQ) then
     ! Get force from VOF
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ! In x
              qi = q0*VOF_U(i,j,k)
              if (mask_u(i,j).eq.0) srcU(i,j,k)=srcU(i,j,k)+dt_uvw*qi*Ex(i,j,k)
              ! In y
              qi = q0*VOF_V(i,j,k)
              if (mask_v(i,j).eq.0) srcV(i,j,k)=srcV(i,j,k)+dt_uvw*qi*Ey(i,j,k)
              ! In z
              qi = q0*VOF_W(i,j,k)
              if (mask_w(i,j).eq.0) srcW(i,j,k)=srcW(i,j,k)+dt_uvw*qi*Ez(i,j,k)
           end do
        end do
     end do
  else
     ! Get force from transported scalar
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ! In x
              qi = 0.5_WP*sum(SC(i-1:i,j,k,isc_Q))*VOF_U(i,j,k)
              if (mask_u(i,j).eq.0) srcU(i,j,k)=srcU(i,j,k)+dt_uvw*qi*Ex(i,j,k)
              ! In y
              qi = 0.5_WP*sum(SC(i,j-1:j,k,isc_Q))*VOF_V(i,j,k)
              if (mask_v(i,j).eq.0) srcV(i,j,k)=srcV(i,j,k)+dt_uvw*qi*Ey(i,j,k)
              ! In z
              qi = 0.5_WP*sum(SC(i,j,k-1:k,isc_Q))*VOF_W(i,j,k)
              if (mask_w(i,j).eq.0) srcW(i,j,k)=srcW(i,j,k)+dt_uvw*qi*Ez(i,j,k)
           end do
        end do
     end do
  end if
  
  ! Pass the Electric field values to monitor
  call monitor_select_file('ehd')
  call monitor_set_single_value(1,real(it_p,WP))
  call monitor_set_single_value(2,max_resP)
  call monitor_set_single_value(3,maxval(abs(Ex)))
  call monitor_set_single_value(4,maxval(abs(Ey)))
  call monitor_set_single_value(5,maxval(abs(Ez))) 
  
  return
end subroutine multiphase_ehd_step


! ==================================== !
! Laplacian operator for EHD using GFM !
! ==================================== !
subroutine multiphase_ehd_laplacian
  use multiphase_ehd
  implicit none
  
  integer :: i,j,k
  integer :: dim,dir
  real(WP) :: eps_star
  
  ! Recompute the operator and rhs
  lap = 0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           
           ! Loop over dimension
           do dim=1,3
              
              ! Loop over direction
              do dir=-1,+1,2
                 
                 ! Update the operator and the RHS
                 if      (dir.eq.-1 .and. dim.eq.1) then
                    eps_star=eps_l*eps_g/(VOF_U(i,j,k)*eps_g+(1.0_WP-VOF_U(i,j,k))*eps_l)
                    lap(i,j,k,dim,  0) = lap(i,j,k,dim,  0) + divc_u(i,j,k,0)*grad_Px(i  ,j  ,k  , 0)*eps_star
                    lap(i,j,k,dim,dir) = lap(i,j,k,dim,dir) + divc_u(i,j,k,0)*grad_Px(i  ,j  ,k  ,-1)*eps_star
                 else if (dir.eq.+1 .and. dim.eq.1) then
                    eps_star=eps_l*eps_g/(VOF_U(i+1,j,k)*eps_g+(1.0_WP-VOF_U(i+1,j,k))*eps_l)
                    lap(i,j,k,dim,  0) = lap(i,j,k,dim,  0) + divc_u(i,j,k,1)*grad_Px(i+1,j  ,k  ,-1)*eps_star
                    lap(i,j,k,dim,dir) = lap(i,j,k,dim,dir) + divc_u(i,j,k,1)*grad_Px(i+1,j  ,k  , 0)*eps_star
                 else if (dir.eq.-1 .and. dim.eq.2) then
                    eps_star=eps_l*eps_g/(VOF_V(i,j,k)*eps_g+(1.0_WP-VOF_V(i,j,k))*eps_l)
                    lap(i,j,k,dim,  0) = lap(i,j,k,dim,  0) + divc_v(i,j,k,0)*grad_Py(i  ,j  ,k  , 0)*eps_star
                    lap(i,j,k,dim,dir) = lap(i,j,k,dim,dir) + divc_v(i,j,k,0)*grad_Py(i  ,j  ,k  ,-1)*eps_star
                 else if (dir.eq.+1 .and. dim.eq.2) then
                    eps_star=eps_l*eps_g/(VOF_V(i,j+1,k)*eps_g+(1.0_WP-VOF_V(i,j+1,k))*eps_l)
                    lap(i,j,k,dim,  0) = lap(i,j,k,dim,  0) + divc_v(i,j,k,1)*grad_Py(i  ,j+1,k  ,-1)*eps_star
                    lap(i,j,k,dim,dir) = lap(i,j,k,dim,dir) + divc_v(i,j,k,1)*grad_Py(i  ,j+1,k  , 0)*eps_star
                 else if (dir.eq.-1 .and. dim.eq.3) then
                    eps_star=eps_l*eps_g/(VOF_W(i,j,k)*eps_g+(1.0_WP-VOF_W(i,j,k))*eps_l)
                    lap(i,j,k,dim,  0) = lap(i,j,k,dim,  0) + divc_w(i,j,k,0)*grad_Pz(i  ,j  ,k  , 0)*eps_star
                    lap(i,j,k,dim,dir) = lap(i,j,k,dim,dir) + divc_w(i,j,k,0)*grad_Pz(i  ,j  ,k  ,-1)*eps_star
                 else if (dir.eq.+1 .and. dim.eq.3) then
                    eps_star=eps_l*eps_g/(VOF_W(i,j,k+1)*eps_g+(1.0_WP-VOF_W(i,j,k+1))*eps_l)
                    lap(i,j,k,dim,  0) = lap(i,j,k,dim,  0) + divc_w(i,j,k,1)*grad_Pz(i  ,j  ,k+1,-1)*eps_star
                    lap(i,j,k,dim,dir) = lap(i,j,k,dim,dir) + divc_w(i,j,k,1)*grad_Pz(i  ,j  ,k+1, 0)*eps_star
                 end if
                 
              end do
           end do
        end do
     end do
  end do
  
  !! BOUNDARY CONDITIONS 	!!
  !!	IN X	     	!! 
  !   if (xper.ne.1) then
  !         if (iproc.eq.1) then
  !             lap(imin,:,:,:,:)=lap(imin+1,:,:,:,:)
  ! 	    lap(imin,:,:,1,-1)=0.0_WP
  ! 	    RP (imin,:,:)    =RP (imin,:,:)-phi_bottom*lap(imin+1,jmin_:jmax_,kmin_:kmax_,1,-1)
  !         end if
  !  
  !         if (iproc.eq.npx) then
  !             lap(imax,:,:,:,:)=lap(imax-1,:,:,:,:)
  ! 	    lap(imax,:,:,1,+1)=0.0_WP
  ! 	    RP (imax,:,:)    =RP (imax,:,:)-phi_top    *lap(imax-1,jmin_:jmax_,kmin_:kmax_,1,+1)
  !         end if
  !   end if
  
  ! In y
  !if (yper.ne.1) then
  !   if (jproc.eq.1) then
  !      RP (:,jmin,:)     =RP (:,jmin,:)-phi_bottom*lap(imin_:imax_,jmin,kmin_:kmax_,2,-1)
  !      lap(:,jmin,:,2,-1)=0.0_WP
  !   end if
  !   if (jproc.eq.npy) then
  !      RP (:,jmax,:)     =RP (:,jmax,:)-phi_top   *lap(imin_:imax_,jmax,kmin_:kmax_,2,+1)
  !      lap(:,jmax,:,2,+1)=0.0_WP
  !   end if
  !end if
  
  !!	IN Z		!! 
  !   if (zper.ne.1) then
  !         if (kproc.eq.1) then
  !             lap(:,:,kmin,:,:)=lap(:,:,kmin+1,:,:)
  ! 	    lap(:,:,kmin,3,-1)=0.0_WP
  ! 	    RP (:,:,kmin)    =RP (:,:,kmin)-phi_bottom*lap(imin_:imax_,jmin_:jmax_,kmin+1,3,-1)
  !         end if
  !         if (kproc.eq.npz) then
  !             lap(:,:,kmax,:,:)=lap(:,:,kmax-1,:,:)
  ! 	    lap(:,:,kmax,3,+1)=0.0_WP
  ! 	    RP (:,:,kmax)    =RP (:,:,kmax)-phi_top    *lap(imin_:imax_,jmin_:jmax_,kmax-1,3,+1)
  !         end if
  !   end if
  
  !! 	Dirichlet BC's original 
  !	In x			!
  !          if (iproc.eq.1) then
  !              lap(imin,:,:,:,:)=0.0_WP
  !              lap(imin,:,:,1,0)=1.0_WP!/3.0_WP
  !              RP(imin,:,:)     =phi_bottom
  !          end if  
  !	!!  Try setting inflow plane to zero outside of jet diameter
  !	 if (iproc.eq.1) then
  !	    do k=kmin_,kmax_
  !	      do j=jmin_,jmax_
  !		diameter = sqrt(ym(j)**2+zm(k)**2)
  !		if (diameter.gt.(0.6_WP*Djet)) then
  !		!if (diameter.gt.(0.5_WP*Djet+3.0_WP*del_x)) then
  !		  lap(imin,j,k,:,:)=0.0_WP
  !		  lap(imin,j,k,1,0)=1.0_WP!/3.0_WP
  !		  RP(imin,j,k)     =phi_bottom
  !		end if
  !	       end do
  !	      end do
  !        end if
  !         if (iproc.eq.npx) then
  !             lap(imax,:,:,:,:)=0.0_WP
  !             lap(imax,:,:,1,0)=1.0_WP!/3.0_WP
  !             RP(imax,:,:)     =phi_top
  !        end if

  ! ! In X
  ! if (iproc.eq.1.and.nx.gt.1) then
  !    lap(imin,:,:,:,:)=0.0_WP
  !    lap(imin,:,:,1,0)=1.0_WP
  !    RP (imin,:,:)    =phi_bottom
  ! end if
  ! if (iproc.eq.npx.and.nx.gt.1) then
  !    lap(imax,:,:,:,:)=0.0_WP
  !    lap(imax,:,:,1,0)=1.0_WP
  !    RP (imax,:,:)    =phi_top
  ! end if

  ! In Y
  if (jproc.eq.1.and.ny.gt.1) then
     lap(:,jmin,:,:,:)=0.0_WP
     lap(:,jmin,:,2,0)=1.0_WP
     RP (:,jmin,:)    =phi_bottom
  end if
  if (jproc.eq.npy.and.ny.gt.1) then
     lap(:,jmax,:,:,:)=0.0_WP
     lap(:,jmax,:,2,0)=1.0_WP
     RP (:,jmax,:)    =phi_top
  end if
  
!  ! In Z
!  if (kproc.eq.1.and.nz.gt.1) then
!    lap(:,:,kmin,:,:)=0.0_WP
!    lap(:,:,kmin,3,0)=1.0_WP
!    RP (:,:,kmin)    =phi_bottom
!  end if
!  if (kproc.eq.npz.and.nz.gt.1) then
!    lap(:,:,kmax,:,:)=0.0_WP
!    lap(:,:,kmax,3,0)=1.0_WP
!    RP (:,:,kmax)    =phi_top
!  end if
  
!!!!!!!!!!!!!! 	Dirichlet BC's original 
  
  
  ! !             lap(:,jmin,:,:,:)=lap(:,jmin+1,:,:,:) ! Copy jmin+1
  ! !             lap(:,jmin,:,2,0)=lap(:,jmin,:,2,0)+lap(:,jmin,:,2,-1)
  ! !             RP(imin_:imax_,jmin,kmin_:kmax_)=-dym(jmin)*lap(imin_:imax_,jmin,kmin_:kmax_,2,-1)*E0
  ! !             lap(:,jmin,:,2,-1)=0.0_WP
  ! ! 	  !!
  !          end if
  !       
  
  ! !             lap(:,jmax,:,:,:)=lap(:,jmax-1,:,:,:) ! Copy jmax-1
  ! !             lap(:,jmax,:,2,0)=lap(:,jmax,:,2,0)+lap(:,jmax,:,2,+1)
  ! !             RP(imin_:imax_,jmax,kmin_:kmax_)= dym(jmax+1)*lap(imin_:imax_,jmax,kmin_:kmax_,2,+1)*E0
  ! !             lap(:,jmax,:,2,+1)=0.0_WP
  !          end if
  ! ! 	 
  
  return
end subroutine multiphase_ehd_laplacian


! =================================== !
! Gradient operator for EHD using GFM !
! =================================== !
subroutine multiphase_ehd_gradient
  use multiphase_ehd
  use metric_generic
  implicit none
  
  integer :: i, j, k
  real(WP) :: eps_star, coeff
  
  ! Loop of the interior faces
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_

           ! --- X dim ---
           eps_star=eps_l*VOF_U(i,j,k)+eps_g*(1.0_WP-VOF_U(i,j,k))
           if (VOF_U(i,j,k).ge.0.5_WP) then;  coeff=eps_l/eps_star
           else;                              coeff=eps_g/eps_star
           end if
           Ex(i,j,k) = -coeff*sum(grad_Px(i,j,k,:)*phi(i-stc2:i+stc1,j,k))

           ! --- Y dim ---
           eps_star=eps_l*VOF_V(i,j,k)+eps_g*(1.0_WP-VOF_V(i,j,k))
           if (VOF_V(i,j,k).ge.0.5_WP) then;  coeff=eps_l/eps_star
           else;                              coeff=eps_g/eps_star
           end if
           Ey(i,j,k) = -coeff*sum(grad_Py(i,j,k,:)*phi(i,j-stc2:j+stc1,k))

           ! --- Z dim ---
           eps_star=eps_l*VOF_W(i,j,k)+eps_g*(1.0_WP-VOF_W(i,j,k))
           if (VOF_W(i,j,k).ge.0.5_WP) then;  coeff=eps_l/eps_star
           else;                              coeff=eps_g/eps_star
           end if
           Ez(i,j,k) = -coeff*sum(grad_Pz(i,j,k,:)*phi(i,j,k-stc2:k+stc1))
           
        end do
     end do
  end do
  
  ! Update ghost cells
  call boundary_update_border(Ex,'+','ym')
  call boundary_update_border(Ey,'-','y' )
  call boundary_update_border(Ez,'-','ym')
  
  ! Non periodic directions
  call boundary_neumann(Ex,'-x' )
  call boundary_neumann(Ey,'-xm')
  call boundary_neumann(Ez,'-xm')
  
  call boundary_neumann(Ex,'+x' )
  call boundary_neumann(Ey,'+xm')
  call boundary_neumann(Ez,'+xm')
  
  call boundary_neumann(Ex,'-ym')
  call boundary_neumann(Ey,'-y' )
  call boundary_neumann(Ez,'-ym')

  call boundary_neumann(Ex,'+ym')
  call boundary_neumann(Ey,'+y' )
  call boundary_neumann(Ez,'+ym')
  
  return
end subroutine multiphase_ehd_gradient


! ============================== !
! GFM interpolation of staggered !
! electric field to cell center  !
! ============================== !
subroutine multiphase_ehd_interpolate(Exi,Eyi,Ezi)
  use multiphase_ehd
  use metric_generic
  implicit none
  
  integer :: i,j,k
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: Exi,Eyi,Ezi
  real(WP) :: Cx,Cy,Cz,denom,Gx1,Gx2,Gy1,Gy2,Gz1,Gz2,G0
  
  ! Loop over interior domain
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_  
                      
           ! Compute phase indicators on cell faces
           G0  = sign(1.0_WP,VOFavg(i  ,j  ,k  )-0.5_WP)
           Gx1 = sign(1.0_WP,VOF_U (i  ,j  ,k  )-0.5_WP)
           Gy1 = sign(1.0_WP,VOF_V (i  ,j  ,k  )-0.5_WP)
           Gz1 = sign(1.0_WP,VOF_W (i  ,j  ,k  )-0.5_WP)
           Gx2 = sign(1.0_WP,VOF_U (i+1,j  ,k  )-0.5_WP)
           Gy2 = sign(1.0_WP,VOF_V (i  ,j+1,k  )-0.5_WP)
           Gz2 = sign(1.0_WP,VOF_W (i  ,j  ,k+1)-0.5_WP)
           
           ! Create interpolation coefficients  CORRECT Tri-Linear Interp.
           Cx = 0.0_WP
           if (G0.ge.0.0_WP .and. Gx1.lt.0.0_WP) Cx = Cx + 1.0_WP-eps_r
           if (G0.lt.0.0_WP .and. Gx1.ge.0.0_WP) Cx = Cx + 1.0_WP-1.0_WP/eps_r
           if (G0.ge.0.0_WP .and. Gx2.lt.0.0_WP) Cx = Cx + 1.0_WP-eps_r
           if (G0.lt.0.0_WP .and. Gx2.ge.0.0_WP) Cx = Cx + 1.0_WP-1.0_WP/eps_r
           Cy = 0.0_WP
           if (G0.ge.0.0_WP .and. Gy1.lt.0.0_WP) Cy = Cy + 1.0_WP-eps_r
           if (G0.lt.0.0_WP .and. Gy1.ge.0.0_WP) Cy = Cy + 1.0_WP-1.0_WP/eps_r
           if (G0.ge.0.0_WP .and. Gy2.lt.0.0_WP) Cy = Cy + 1.0_WP-eps_r
           if (G0.lt.0.0_WP .and. Gy2.ge.0.0_WP) Cy = Cy + 1.0_WP-1.0_WP/eps_r
           Cz = 0.0_WP
           if (G0.ge.0.0_WP .and. Gz1.lt.0.0_WP) Cz = Cz + 1.0_WP-eps_r
           if (G0.lt.0.0_WP .and. Gz1.ge.0.0_WP) Cz = Cz + 1.0_WP-1.0_WP/eps_r
           if (G0.ge.0.0_WP .and. Gz2.lt.0.0_WP) Cz = Cz + 1.0_WP-eps_r
           if (G0.lt.0.0_WP .and. Gz2.ge.0.0_WP) Cz = Cz + 1.0_WP-1.0_WP/eps_r
           
           ! Interpolation scheme, correct implementation: [eps En]=0
           denom = Cx*normx_avg(i,j,k)**2+Cy*normy_avg(i,j,k)**2+Cz*normz_avg(i,j,k)**2-2
           denom = 0.5_WP/denom
           
           Exi(i,j,k) = denom*( &
                +(Ex(i,j,k)+Ex(i+1,j,k))*(Cy*normy_avg(i,j,k)**2+Cz*normz_avg(i,j,k)**2-2) &
                -(Ey(i,j,k)+Ey(i,j+1,k))*(Cx*normy_avg(i,j,k)*normx_avg(i,j,k))            &
                -(Ez(i,j,k)+Ez(i,j,k+1))*(Cx*normz_avg(i,j,k)*normx_avg(i,j,k))            )
           
           Eyi(i,j,k) = denom*( &
                -(Ex(i,j,k)+Ex(i+1,j,k))*(Cy*normx_avg(i,j,k)*normy_avg(i,j,k))            &
                +(Ey(i,j,k)+Ey(i,j+1,k))*(Cx*normx_avg(i,j,k)**2+Cz*normz_avg(i,j,k)**2-2) &
                -(Ez(i,j,k)+Ez(i,j,k+1))*(Cy*normz_avg(i,j,k)*normy_avg(i,j,k))            )
           
           Ezi(i,j,k) = denom*( &
                -(Ex(i,j,k)+Ex(i+1,j,k))*(Cz*normx_avg(i,j,k)*normz_avg(i,j,k))            &
                -(Ey(i,j,k)+Ey(i,j+1,k))*(Cz*normy_avg(i,j,k)*normz_avg(i,j,k))            &
                +(Ez(i,j,k)+Ez(i,j,k+1))*(Cx*normx_avg(i,j,k)**2+Cy*normy_avg(i,j,k)**2-2) )
           
        end do
     end do
  end do
  
  ! Update ghost cells
  call boundary_update_border(Exi,'+','ym')
  call boundary_update_border(Eyi,'-','ym')
  call boundary_update_border(Ezi,'-','ym')
  
  ! Non periodic directions
  call boundary_neumann(Exi,'-xm')
  call boundary_neumann(Eyi,'-xm')
  call boundary_neumann(Ezi,'-xm')
  
  call boundary_neumann(Exi,'+xm')
  call boundary_neumann(Eyi,'+xm')
  call boundary_neumann(Ezi,'+xm')
  
  call boundary_neumann(Exi,'-ym')
  call boundary_neumann(Eyi,'-ym')
  call boundary_neumann(Ezi,'-ym')
  
  call boundary_neumann(Exi,'+ym')
  call boundary_neumann(Eyi,'+ym')
  call boundary_neumann(Ezi,'+ym')
  
  return
end subroutine multiphase_ehd_interpolate


! ======================================= !
! Computation of the GFM jump for the EHD !
! ======================================= !
subroutine multiphase_ehd_jump(jump)
  use multiphase_ehd
  use metric_generic
  use memory
  implicit none
  
   real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: jump
  ! integer  :: i,j,k
  ! real(WP) :: Ei2,Ei,E,Enx,Eny,Enz,En,En2,Et2,Et,Ejump_n,Ejump_t,Ejump
  
  ! ! Check whether EHD is used
  ! if (.not.use_ehd) return
  
  ! ! Interpolate electric field to cell centers
  ! call multiphase_ehd_interpolate(tmp1,tmp2,tmp3)
  
  ! ! Create EHD jump
  ! do k=kmin_,kmax_
  !    do j=jmin_,jmax_
  !       do i=imin_,imax_
  !          ! Create normal and tangential components
  !          Ei2 = tmp1(i,j,k)**2+tmp2(i,j,k)**2+tmp3(i,j,k)**2   
  !          Ei  = sqrt(Ei2)
  !          E   = sqrt(Ex(i,j,k)**2+Ey(i,j,k)**2+Ez(i,j,k)**2)
  !          Enx = tmp1(i,j,k)*normx_avg(i,j,k)
  !          Eny = tmp2(i,j,k)*normy_avg(i,j,k)
  !          Enz = tmp3(i,j,k)*normz_avg(i,j,k)
  !          En  = (Enx+Eny+Enz)
  !          En2 = En**2
  !          Et2 = Ei2-En2
  !          Et  = sqrt(Et2)
  !          if (VOF(i,j,k).ge.0.5_WP) then
  !             ! Liquid
  !             Ejump_n = 0.5_WP*(eps_g-eps_l)*(En2*eps_r)
  !             Ejump_t = 0.5_WP*(eps_g-eps_l)*(Et2)
  !             Ejump   = Ejump_n+Ejump_t
  !          else
  !             ! Gas
  !             Ejump_n = 0.5_WP*(eps_g-eps_l)*(En2/eps_r)
  !             Ejump_t = 0.5_WP*(eps_g-eps_l)*(Et2)
  !             Ejump   = Ejump_n+Ejump_t
  !          end if
  !          ! Send this value back to multiphase_GFM
  !          jump(i,j,k) = jump(i,j,k)+Ejump
  !       end do
  !    end do
  ! end do
  
  ! ! Don't use pressure jump for now
  jump=0.0_WP

  return
end subroutine multiphase_ehd_jump

! ==================================== !
! Update electric charge density using !
! fluxes based on VOF and q at t^{n+1} !
! ==================================== !
subroutine multiphase_ehd_scalar_step
  use multiphase_ehd
  implicit none
  
  integer :: i,j,k,isc
 
  ! Test if used
  if (.not.use_multiphase .or. .not.use_ehd) return
  
  ! Start the timer
  call timing_start('multiphase')
  
  ! Find the right scalar
  do isc=1,nscalar
     if (isc.ne.isc_Q) cycle

     ! Save the old scalar
     SCold(:,:,:,isc) = SC(:,:,:,isc)

     ! Subiterations
     do niter=1,max_niter

        ! Compute mid point scalar
        SC(:,:,:,isc)=0.5_WP*(SC(:,:,:,isc)+SCold(:,:,:,isc))
      
        ! Compute residuals
        call multiphase_ehd_scalar_residual(isc)
        Ax=0.0_WP; Ay=0.0_WP; Az=0.0_WP
        call multiphase_ehd_scalar_operator
        call multiphase_scalar_inverse(isc)

        ! Update the scalar
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 SC(i,j,k,isc) = 2.0_WP*SC(i,j,k,isc)-SCold(i,j,k,isc) + ResSC(i,j,k,isc)
              end do
           end do
        end do

        ! Update the physcial boundaries 
        call boundary_scalar_neumann
        call boundary_scalar_outflow
  
        ! Update the overlapped cells
        call boundary_update_border(SC(:,:,:,isc),'+','ym')
        
     end do
  end do

  ! Stop the timer
  call timing_stop('multiphase')
  
  return
end subroutine multiphase_ehd_scalar_step

! ==================================== !
! Compute residual for electric charge !
! ==================================== !
subroutine multiphase_ehd_scalar_residual(isc)
  use multiphase_ehd
  use multiphase_tools
  implicit none

  integer, intent(in) :: isc
  integer  :: i,j,k
  real(WP), parameter :: eps=1.0e-12_WP
  real(WP) :: rhs

  ! Compute VOF aware quick coefficents
  call multiphase_ehd_quick
  
  ! Compute diffusivity
  call multiphase_ehd_diff
           
  ! Compute fluxes
  do k=kmin_,kmax_+1
     do j=jmin_,jmax_+1 
        do i=imin_,imax_+1    
           ! X direction
           FX(i,j,k)=wa_adjx(i,j,k)*wetted_area_frac_x(i,j,k)*( &
                - mu_i * 0.5_WP*(Ex(i,j,k)+abs(Ex(i,j,k))) * sum(ehd_quick_xp(i,j,k,:)*SC(i-2:i  ,j,k,isc)) &
                - mu_i * 0.5_WP*(Ex(i,j,k)-abs(Ex(i,j,k))) * sum(ehd_quick_xm(i,j,k,:)*SC(i-1:i+1,j,k,isc)) &
                + DIFFx(i,j,k)*sum(grad_x(i,j,:)*SC(i-st2:i+st1,j,k,isc)) )
           ! Y direction
           FY(i,j,k)=wa_adjy(i,j,k)*wetted_area_frac_y(i,j,k)*( &
                - mu_i * 0.5_WP*(Ey(i,j,k)+abs(Ey(i,j,k))) * sum(ehd_quick_yp(i,j,k,:)*SC(i,j-2:j  ,k,isc)) &
                - mu_i * 0.5_WP*(Ey(i,j,k)-abs(Ey(i,j,k))) * sum(ehd_quick_ym(i,j,k,:)*SC(i,j-1:j+1,k,isc)) &
                + DIFFy(i,j,k)*sum(grad_y(i,j,:)*SC(i,j-st2:j+st1,k,isc)) )
           ! Z direction
           FZ(i,j,k)=wa_adjz(i,j,k)*wetted_area_frac_z(i,j,k)*( &
                - mu_i * 0.5_WP*(Ez(i,j,k)+abs(Ez(i,j,k))) * sum(ehd_quick_zp(i,j,k,:)*SC(i,j,k-2:k  ,isc)) &
                - mu_i * 0.5_WP*(Ez(i,j,k)-abs(Ez(i,j,k))) * sum(ehd_quick_zm(i,j,k,:)*SC(i,j,k-1:k+1,isc)) &
                + DIFFz(i,j,k)*sum(grad_z(i,j,:)*SC(i,j,k-st2:k+st1,isc)) )
        end do
     end do
  end do

  ! Compute residual
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (VOFavg(i,j,k).lt.VOFlo) then
              ResSC(i,j,k,isc) =  -2.0_WP*SC(i,j,k,isc)+SCold(i,j,k,isc)
           else
              rhs =+sum(div_u(i,j,k,:)*FX(i-st1:i+st2,j,k)) &
                   +sum(div_v(i,j,k,:)*FY(i,j-st1:j+st2,k)) &
                   +sum(div_w(i,j,k,:)*FZ(i,j,k-st1:k+st2)) 
              ResSC(i,j,k,isc) =  -2.0_WP*SC(i,j,k,isc)+SCold(i,j,k,isc) &
                   + SCold(i,j,k,isc) + dt*rhs/sign(max(abs(VOFavg(i,j,k)),eps*meshsize(i,j,k)),VOFavg(i,j,k))
           end if
        end do
     end do
  end do

  return
end subroutine multiphase_ehd_scalar_residual

! ==================================== !
! Inverse operator for electric charge !
! ==================================== !
subroutine multiphase_ehd_scalar_operator
  use multiphase_ehd
  use multiphase_tools
  use implicit
  implicit none

  integer :: i,j,k
  real(WP) :: dt2,vofi,mydiag
  real(WP), parameter :: eps=epsilon(1.0_WP)
  real(WP) :: conv1,conv2,conv3,conv4
  real(WP) :: wa1,wa2

  dt2 = dt !/2.0_WP  ! Use SC(t^n+1)

  ! X-direction
  if (implicit_x) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_

              conv1 = 0.5_WP*(Ex(i  ,j,k) + abs(Ex(i  ,j,k)))
              conv2 = 0.5_WP*(Ex(i  ,j,k) - abs(Ex(i  ,j,k)))
              conv3 = 0.5_WP*(Ex(i+1,j,k) + abs(Ex(i+1,j,k)))
              conv4 = 0.5_WP*(Ex(i+1,j,k) - abs(Ex(i+1,j,k)))
              
              wa1 = wa_adjx(i  ,j,k)*wetted_area_frac_x(i  ,j,k)
              wa2 = wa_adjx(i+1,j,k)*wetted_area_frac_x(i+1,j,k)

              vofi=1.0_WP/(sign(max(abs(VOFavg(i,j,k)),eps),VOFavg(i,j,k)))

              Ax(j,k,i,-2) = Ax(j,k,i,-2) - dt2*vofi*( &
                   + div_u(i,j,k,0)*wa1*( -mu_i*(conv1*ehd_quick_xp(i  ,j,k,-2)                               )                        ) )

              Ax(j,k,i,-1) = Ax(j,k,i,-1) - dt2*vofi*( &
                   + div_u(i,j,k,0)*wa1*( -mu_i*(conv1*ehd_quick_xp(i  ,j,k,-1)+conv2*ehd_quick_xm(i  ,j,k,-1)) + DIFFx(i  ,j,k)*grad_x(i  ,j,-1) ) &
                   + div_u(i,j,k,1)*wa2*( -mu_i*(conv3*ehd_quick_xp(i  ,j,k,-2)                               )                        ) )

              Ax(j,k,i, 0) = Ax(j,k,i, 0) - dt2*vofi*(  &
                   + div_u(i,j,k,0)*wa1*( -mu_i*(conv1*ehd_quick_xp(i  ,j,k, 0)+conv2*ehd_quick_xm(i  ,j,k, 0)) + DIFFx(i  ,j,k)*grad_x(i  ,j, 0) ) &
                   + div_u(i,j,k,1)*wa2*( -mu_i*(conv3*ehd_quick_xp(i+1,j,k,-1)+conv4*ehd_quick_xm(i+1,j,k,-1)) + DIFFx(i+1,j,k)*grad_x(i+1,j,-1) ) )
                   
              Ax(j,k,i,+1) = Ax(j,k,i,+1) - dt2*vofi*( &
                   + div_u(i,j,k,0)*wa1*( -mu_i*(                               conv2*ehd_quick_xm(i  ,j,k,+1))                        ) &
                   + div_u(i,j,k,1)*wa2*( -mu_i*(conv3*ehd_quick_xp(i+1,j,k, 0)+conv4*ehd_quick_xm(i+1,j,k, 0)) + DIFFx(i+1,j,k)*grad_x(i+1,j, 0) ) )

              Ax(j,k,i,+2) = Ax(j,k,i,+2) - dt2*vofi*( &
                   + div_u(i,j,k,1)*wa2*( -mu_i*(                               conv4*ehd_quick_xm(i+1,j,k,+1))                        ) ) 

           end do
        end do
     end do
  end if

  ! Y-direction
  if (implicit_y) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_

              conv1 = 0.5_WP*(Ey(i,j  ,k) + abs(Ey(i,j  ,k)))
              conv2 = 0.5_WP*(Ey(i,j  ,k) - abs(Ey(i,j  ,k)))
              conv3 = 0.5_WP*(Ey(i,j+1,k) + abs(Ey(i,j+1,k)))
              conv4 = 0.5_WP*(Ey(i,j+1,k) - abs(Ey(i,j+1,k)))
              
              wa1 = wa_adjy(i,j  ,k)*wetted_area_frac_y(i,j  ,k)
              wa2 = wa_adjy(i,j+1,k)*wetted_area_frac_y(i,j+1,k)

              vofi=1.0_WP/(sign(max(abs(VOFavg(i,j,k)),eps),VOFavg(i,j,k)))

              Ay(i,k,j,-2) = Ay(i,k,j,-2) - dt2*vofi*( &
                   + div_v(i,j,k,0)*wa1*( -mu_i*(conv1*ehd_quick_yp(i,j  ,k,-2)                               )                        ) )

              Ay(i,k,j,-1) = Ay(i,k,j,-1) - dt2*vofi*( &
                   + div_v(i,j,k,0)*wa1*( -mu_i*(conv1*ehd_quick_yp(i,j  ,k,-1)+conv2*ehd_quick_ym(i,j  ,k,-1)) + DIFFy(i,j  ,k)*grad_y(i,j  ,-1) ) &
                   + div_v(i,j,k,1)*wa2*( -mu_i*(conv3*ehd_quick_yp(i,j  ,k,-2)                               )                        ) )

              Ay(i,k,j, 0) = Ay(i,k,j, 0) - dt2*vofi*(  &
                   + div_v(i,j,k,0)*wa1*( -mu_i*(conv1*ehd_quick_yp(i,j  ,k, 0)+conv2*ehd_quick_ym(i,j  ,k, 0)) + DIFFy(i,j  ,k)*grad_y(i,j  , 0) ) &
                   + div_v(i,j,k,1)*wa2*( -mu_i*(conv3*ehd_quick_yp(i,j+1,k,-1)+conv4*ehd_quick_ym(i,j+1,k,-1)) + DIFFy(i,j+1,k)*grad_y(i,j+1,-1) ) )
                   
              Ay(i,k,j,+1) = Ay(i,k,j,+1) - dt2*vofi*( &
                   + div_v(i,j,k,0)*wa1*( -mu_i*(                               conv2*ehd_quick_ym(i,j  ,k,+1))                        ) &
                   + div_v(i,j,k,1)*wa2*( -mu_i*(conv3*ehd_quick_yp(i,j+1,k, 0)+conv4*ehd_quick_ym(i,j+1,k, 0)) + DIFFy(i,j+1,k)*grad_y(i,j+1, 0) ) )

              Ay(i,k,j,+2) = Ay(i,k,j,+2) - dt2*vofi*( &
                   + div_v(i,j,k,1)*wa2*( -mu_i*(                               conv4*ehd_quick_ym(i,j+1,k,+1))                        ) ) 
           end do
        end do
     end do
  end if

  ! Z-direction
  if (implicit_z) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_

              conv1 = 0.5_WP*(Ez(i,j,k  ) + abs(Ez(i,j,k  )))
              conv2 = 0.5_WP*(Ez(i,j,k  ) - abs(Ez(i,j,k  )))
              conv3 = 0.5_WP*(Ez(i,j,k+1) + abs(Ez(i,j,k+1)))
              conv4 = 0.5_WP*(Ez(i,j,k+1) - abs(Ez(i,j,k+1)))
              
              wa1 = wa_adjz(i,j,k  )*wetted_area_frac_z(i,j,k  )
              wa2 = wa_adjz(i,j,k+1)*wetted_area_frac_z(i,j,k+1)

              vofi=1.0_WP/(sign(max(abs(VOFavg(i,j,k)),eps),VOFavg(i,j,k)))

              Az(i,j,k,-2) = Az(i,j,k,-2) - dt2*vofi*( &
                   + div_u(i,j,k,0)*wa1*( -mu_i*(conv1*ehd_quick_zp(i,j,k,-2)                             )                      ) )

              Az(i,j,k,-1) = Az(i,j,k,-1) - dt2*vofi*( &
                   + div_u(i,j,k,0)*wa1*( -mu_i*(conv1*ehd_quick_zp(i,j,k,-1)+conv2*ehd_quick_zm(i,j,k,-1)) + DIFFz(i,j,k  )*grad_z(i,j,-1) ) &
                   + div_u(i,j,k,1)*wa2*( -mu_i*(conv3*ehd_quick_zp(i,j,k,-2)                             )                      ) )

              Az(i,j,k, 0) = Az(i,j,k, 0) - dt2*vofi*(  &
                   + div_u(i,j,k,0)*wa1*( -mu_i*(conv1*ehd_quick_zp(i,j,k, 0)+conv2*ehd_quick_zm(i,j,k, 0)) + DIFFz(i,j,k  )*grad_z(i,j, 0) ) &
                   + div_u(i,j,k,1)*wa2*( -mu_i*(conv3*ehd_quick_zp(i,j,k,-1)+conv4*ehd_quick_zm(i,j,k,-1)) + DIFFz(i,j,k+1)*grad_z(i,j,-1) ) )
                   
              Az(i,j,k,+1) = Az(i,j,k,+1) - dt2*vofi*( &
                   + div_u(i,j,k,0)*wa1*( -mu_i*(                             conv2*ehd_quick_zm(i,j,k,+1))                      ) &
                   + div_u(i,j,k,1)*wa2*( -mu_i*(conv3*ehd_quick_zp(i,j,k, 0)+conv4*ehd_quick_zm(i,j,k, 0)) + DIFFz(i,j,k+1)*grad_z(i,j, 0) ) )

              Az(i,j,k,+2) = Az(i,j,k,+2) - dt2*vofi*( &
                   + div_u(i,j,k,1)*wa2*( -mu_i*(                             conv4*ehd_quick_zm(i,j,k,+1))                      ) ) 
           end do
        end do
     end do
  end if

  ! Create full diagonal
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           mydiag = 1.0_WP + Ax(j,k,i,0)+Ay(i,k,j,0)+Az(i,j,k,0)
           Ax(j,k,i,0) = mydiag
           Ay(i,k,j,0) = mydiag
           Az(i,j,k,0) = mydiag
        end do
     end do
  end do

  return
end subroutine multiphase_ehd_scalar_operator

! ============================ !
! VOF aware QUICK coefficients !
! ============================ !
subroutine multiphase_ehd_quick
  use multiphase_ehd
  use scalar_quick
  implicit none

  integer :: i,j,k

  ! Copy default values
  do k=kmin_-st1,kmax_+st2
     ehd_quick_xp(:,:,k,:)=quick_xp(:,:,:)
     ehd_quick_yp(:,:,k,:)=quick_yp(:,:,:)
     ehd_quick_zp(:,:,k,:)=quick_zp(:,:,:)
     ehd_quick_xm(:,:,k,:)=quick_xm(:,:,:)
     ehd_quick_ym(:,:,k,:)=quick_ym(:,:,:)
     ehd_quick_zm(:,:,k,:)=quick_zm(:,:,:)
  end do
  
  ! Modify stencils near interface if needed
  do k=kmin_-st1,kmax_+st2
     do j=jmin_-st1,jmax_+st2
        do i=imin_-st1,imax_+st2
           ! xp
           if (VOFavg(i-2,j,k).lt.VOFlo) then
              ehd_quick_xp(i,j,k,-2)=0.0_WP
              ehd_quick_xp(i,j,k,-1)=1.0_WP
              ehd_quick_xp(i,j,k, 0)=0.0_WP
           end if
           ! xm
           if (VOFavg(i+1,j,k).lt.VOFlo) then
              ehd_quick_xm(i,j,k,-1)=0.0_WP
              ehd_quick_xm(i,j,k, 0)=1.0_WP
              ehd_quick_xm(i,j,k,+1)=0.0_WP
           end if
           ! yp
           if (VOFavg(i,j-2,k).lt.VOFlo) then
              ehd_quick_yp(i,j,k,-2)=0.0_WP
              ehd_quick_yp(i,j,k,-1)=1.0_WP
              ehd_quick_yp(i,j,k, 0)=0.0_WP
           end if
           ! ym
           if (VOFavg(i,j+1,k).lt.VOFlo) then
              ehd_quick_ym(i,j,k,-1)=0.0_WP
              ehd_quick_ym(i,j,k, 0)=1.0_WP
              ehd_quick_ym(i,j,k,+1)=0.0_WP
           end if
           ! zp
           if (VOFavg(i,j,k-2).lt.VOFlo) then
              ehd_quick_zp(i,j,k,-2)=0.0_WP
              ehd_quick_zp(i,j,k,-1)=1.0_WP
              ehd_quick_zp(i,j,k, 0)=0.0_WP
           end if
           ! zm
           if (VOFavg(i,j,k+1).lt.VOFlo) then
              ehd_quick_zm(i,j,k,-1)=0.0_WP
              ehd_quick_zm(i,j,k, 0)=1.0_WP
              ehd_quick_zm(i,j,k,+1)=0.0_WP
           end if

           ! Don't convect into small cells
           if (min(VOFavg(i-1,j,k),VOFavg(i,j,k)).lt.1e-5_WP) then
              ehd_quick_xp(i,j,k,:)=0.0_WP
              ehd_quick_xm(i,j,k,:)=0.0_WP
           end if
           if (min(VOFavg(i,j-1,k),VOFavg(i,j,k)).lt.1e-5_WP) then
              ehd_quick_yp(i,j,k,:)=0.0_WP
              ehd_quick_ym(i,j,k,:)=0.0_WP
           end if
           if (min(VOFavg(i,j,k-1),VOFavg(i,j,k)).lt.1e-5_WP) then
              ehd_quick_zp(i,j,k,:)=0.0_WP
              ehd_quick_zm(i,j,k,:)=0.0_WP
           end if

        end do
     end do
  end do
  
  return
end subroutine multiphase_ehd_quick

! ==================================== !
! Compute face diffusvity based of VOF !
! ==================================== !
subroutine multiphase_ehd_diff
  use multiphase_ehd
  implicit none

  integer  :: i,j,k,cell
  real(WP) :: myDiff
  real(WP), parameter :: VOFcutoff=0.5_WP
  real(WP), parameter :: coeff=1e5_WP
    
  ! Initialize diffusivities
  DIFFx=D_i
  DIFFy=D_i
  DIFFz=D_i
  
  do k=kmin_,kmax_
     do j=jmin_,jmax_ 
        do i=imin_,imax_ 
           ! Identify small cells
           if (VOFavg(i,j,k).gt.VOFlo.and.VOFavg(i,j,k).lt.VOFcutoff) then 
              
              ! Compute diffusivity based on size
              myDIFF=min(1e5_WP*D_i,D_i+exp(coeff*D_i*(VOFcutoff-VOFavg(i,j,k))))

              
              ! ! Identify largest neighboring cell/weighted area fraction combo
              ! cell=maxloc((/ VOFavg(i-1,j  ,k  )*wetted_area_frac_x(i  ,j  ,k  ), &
              !                VOFavg(i+1,j  ,k  )*wetted_area_frac_x(i+1,j  ,k  ), &
              !                VOFavg(i  ,j-1,k  )*wetted_area_frac_y(i  ,j  ,k  ), &
              !                VOFavg(i  ,j+1,k  )*wetted_area_frac_y(i  ,j+1,k  ), &
              !                VOFavg(i  ,j  ,k-1)*wetted_area_frac_z(i  ,j  ,k  ), &
              !                VOFavg(i  ,j  ,k+1)*wetted_area_frac_z(i  ,j  ,k+1) /),1)

              ! ! Update face diffusivity between small and large cell
              ! select case(cell)
              ! case (1); DIFFx(i  ,j  ,k  )=myDIFF
              ! case (2); DIFFx(i+1,j  ,k  )=myDIFF
              ! case (3); DIFFy(i  ,j  ,k  )=myDIFF
              ! case (4); DIFFy(i  ,j+1,k  )=myDIFF
              ! case (5); DIFFz(i  ,j  ,k  )=myDIFF
              ! case (6); DIFFz(i  ,j  ,k+1)=myDIFF
              ! end select

           end if
        end do
     end do
  end do

  return
end subroutine multiphase_ehd_diff 
