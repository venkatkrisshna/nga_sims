! =========================== !
! Height function calculation !
! =========================== !
module multiphase_height
  use multiphase_vof
  use multiphase
  implicit none
  
  ! Stencils
  integer, parameter :: sts=1
  integer, parameter :: sth=3
  
  ! Wide stencil volume fraction field
  real(WP), dimension(:,:,:), pointer :: VF
  
  ! Wide stencil mesh data
  real(WP), dimension(:), pointer :: VFxm,VFym,VFzm
  real(WP), dimension(:), pointer :: VFdx,VFdy,VFdz

  ! Local normal
  real(WP), dimension(:,:,:), pointer :: normxH,normyH,normzH
  
  ! Interface curvature pointer
  real(WP), dimension(:,:,:), pointer :: curvDir
  
end module multiphase_height


! ================================= !
! Initialization of height function !
! ================================= !
subroutine multiphase_height_init
  use multiphase_height
  implicit none
  
  integer :: i,j,k
  
  ! Allocate volume fraction array with appropriate ghost cells
  allocate(VF(imin_-sth:imax_+sth,jmin_-sth:jmax_+sth,kmin_-sth:kmax_+sth))
  
  ! Allocate mesh data with wider stencil in x
  allocate(VFxm(imin-sth:imax+sth))
  allocate(VFdx(imin-sth:imax+sth))
  if (imin-sth.ge.imino) then
     VFxm(imin-sth:imax+sth)=xm(imin-sth:imax+sth)
     VFdx(imin-sth:imax+sth)=dx(imin-sth:imax+sth)
  else
     VFxm(imino:imaxo)=xm(imino:imaxo)
     VFdx(imino:imaxo)=dx(imino:imaxo)
     do i=imino-1,imin-sth,-1
        VFxm(i)=VFxm(i+1)-dxm(imino)
        VFdx(i)=dx(imino)
     end do
     do i=imaxo+1,imax+sth,+1
        VFxm(i)=VFxm(i-1)+dxm(imaxo-1)
        VFdx(i)=dx(imaxo)
     end do
  end if
  
  ! Allocate mesh data with wider stencil in y
  allocate(VFym(jmin-sth:jmax+sth))
  allocate(VFdy(jmin-sth:jmax+sth))
  if (jmin-sth.ge.jmino) then
     VFym(jmin-sth:jmax+sth)=ym(jmin-sth:jmax+sth)
     VFdy(jmin-sth:jmax+sth)=dy(jmin-sth:jmax+sth)
  else
     VFym(jmino:jmaxo)=ym(jmino:jmaxo)
     VFdy(jmino:jmaxo)=dy(jmino:jmaxo)
     do j=jmino-1,jmin-sth,-1
        VFym(j)=VFym(j+1)-dym(jmino)
        VFdy(j)=dy(jmino)
     end do
     do j=jmaxo+1,jmax+sth,+1
        VFym(j)=VFym(j-1)+dym(jmaxo-1)
        VFdy(j)=dy(jmaxo)
     end do
  end if
  
  ! Allocate mesh data with wider stencil in z
  allocate(VFzm(kmin-sth:kmax+sth))
  allocate(VFdz(kmin-sth:kmax+sth))
  VFdz=dz
  if (kmin-sth.ge.kmino) then
     VFzm(kmin-sth:kmax+sth)=zm(kmin-sth:kmax+sth)
  else
     VFzm(kmino:kmaxo)=zm(kmino:kmaxo)
     do k=kmino-1,kmin-sth,-1
        VFzm(k)=VFzm(k+1)-dz
     end do
     do k=kmaxo+1,kmax+sth,+1
        VFzm(k)=VFzm(k-1)+dz
     end do
  end if

  ! Setup pointers
  normxH  => tmp1
  normyH  => tmp2
  normzH  => tmp3
  curvDir => tmp4
  
  return
end subroutine multiphase_height_init


! ======================================== !
! Height function for normal and curvature !
! ======================================== !
subroutine multiphase_height_calc(update_norm)
  use multiphase_height
  use math
  implicit none
  logical :: update_norm 
  integer :: i,j,k,n,nmin,nmax,count,iter
  integer, target :: ii,jj,kk,nHx,nHy,nHz
  integer, pointer :: i1,i2,nn,nH
  integer :: cell,dir,nloc
  integer :: pm,Fside
  integer :: indexF,indexE
  integer :: st1l,st1u,st2l,st2u
  integer, dimension(-1:1) :: mysth
  real(WP) :: height,norm,dVdN,mydist,mydist2,tt1,tt2,Wmax
  real(WP), dimension(6) :: sol,AtWB
  real(WP), dimension(6,6) :: AtWA
  real(WP), dimension(:), allocatable :: B
  real(WP), dimension(:,:), allocatable :: A,At,Weight
  real(WP), dimension(3,8) :: norm_here
  real(WP), dimension(3) :: nv,t1,t2,x_here,x_rot,n_here,n_rot,dx_vec
  real(WP), dimension(3,3) :: Rot,iRot
  real(WP) :: h1,h2,h11,h22,h12
  real(WP), dimension(3,75) :: I_loc=0.0_WP
  real(WP), dimension(  75) :: I_dis=0.0_WP
  logical :: found_full,found_empty,is_done
  real(WP), parameter :: cvg=1.0e-3_WP
  real(WP) :: myx,myy,myz
  
  ! Transfer VF field and communicate
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           VF(i,j,k)=VOFavg(i,j,k)
        end do
     end do
  end do
  call communication_border_adv(VF,nx_+2*sth,ny_+2*sth,nz_+2*sth,sth)
  call boundary_neumann_alldir_adv(VF,sth)
  
  ! Quick test with Young's method
  !do k=kmin_,kmax_
  !   do j=jmin_,jmax_
  !      do i=imin_,imax_
  !         n=0
  !         do kk=0,1
  !            do jj=0,1
  !               do ii=0,1
  !                  n=n+1
  !                  norm_here(1,n)=-(interp_VOF(i+ii,j+jj,k+kk,1)-interp_VOF(i+ii-1,j+jj  ,k+kk  ,1))*dxi(i+ii-1)
  !                  norm_here(2,n)=-(interp_VOF(i+ii,j+jj,k+kk,2)-interp_VOF(i+ii  ,j+jj-1,k+kk  ,2))*dyi(j+jj-1)
  !                  norm_here(3,n)=-(interp_VOF(i+ii,j+jj,k+kk,3)-interp_VOF(i+ii  ,j+jj  ,k+kk-1,3))*dzi
  !               end do
  !            end do
  !         end do
  !         ! Average normals at each corner of cell
  !         normxH(i,j,k)=0.125_WP*sum(norm_here(1,:))
  !         normyH(i,j,k)=0.125_WP*sum(norm_here(2,:))
  !         normzH(i,j,k)=0.125_WP*sum(norm_here(3,:))
  !         ! Normalize normal
  !         norm=sqrt(normxH(i,j,k)**2+normyH(i,j,k)**2+normzH(i,j,k)**2)+epsilon(1.0_WP)
  !         normxH(i,j,k)=normxH(i,j,k)/norm
  !         normyH(i,j,k)=normyH(i,j,k)/norm
  !         normzH(i,j,k)=normzH(i,j,k)/norm
  !         curv(i,j,k)=0.0_WP
  !      end do
  !   end do 
  !end do

  ! Loop over domain
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_        
                     
           ! Calculate interface locations by forming heights in x,y,z dirs
           ! ==============================================================

           ! Loop over cells: P,U,V,W
           cell_loop : do cell=0,3

              select case(cell)
              case(0)
                 ! Cell center
                 myx=xm(i); myy=ym(j); myz=zm(k);
                 ! Handle cells away from interface
                 if (maxval(VF(i-1:i+1,j-1:j+1,k-1:k+1)).lt.VOFlo .or. minval(VF(i-1:i+1,j-1:j+1,k-1:k+1)).gt.VOFhi) then
                    normxH (i,j,k)=1.0_WP
                    normyH (i,j,k)=0.0_WP
                    normzH (i,j,k)=0.0_WP
                    if (update_norm) then
                       normx(i,j,k)=normxH (i,j,k)
                       normy(i,j,k)=normyH (i,j,k)
                       normz(i,j,k)=normzH (i,j,k)
                    end if
                    curv (i,j,k)=0.0_WP
                    cycle cell_loop
                 end if
              case(1)
                 ! Cell center
                 myx=x (i); myy=ym(j); myz=zm(k);
                 ! Handle cells away from interface
                 if (maxval(VF(i-2:i+1,j-1:j+1,k-1:k+1)).lt.VOFlo .or. minval(VF(i-2:i+1,j-1:j+1,k-1:k+1)).gt.VOFhi) then
                    curvU(i,j,k)=0.0_WP
                    cycle cell_loop
                 end if
                    
              case(2)
                 ! Cell center
                 myx=xm(i); myy=y (j); myz=zm(k);
                 ! Handle cells away from interface
                 if (maxval(VF(i-1:i+1,j-2:j+1,k-1:k+1)).lt.VOFlo .or. minval(VF(i-1:i+1,j-2:j+1,k-1:k+1)).gt.VOFhi) then
                    curvV(i,j,k)=0.0_WP
                    cycle cell_loop
                 end if

              case(3)
                 ! Cell center
                 myx=x (i); myy=ym(j); myz=z (k);
                 ! Handle cells away from interface
                 if (maxval(VF(i-1:i+1,j-1:j+1,k-2:k+1)).lt.VOFlo .or. minval(VF(i-1:i+1,j-1:j+1,k-2:k+1)).gt.VOFhi) then
                    curvW(i,j,k)=0.0_WP
                    cycle cell_loop
                 end if
              end select
              
              ! Initialize height counter
              nloc=0
              
              ! Loop over directions
              do dir=1,3

                 ! Link index adjustments and counters based on direction
                 select case (dir)
                 case (1)
                    nn=>ii; i1=>jj; i2=>kk; nH=>nHx
                 case (2)
                    i1=>ii; nn=>jj; i2=>kk; nH=>nHy
                 case (3)
                    i1=>ii; i2=>jj; nn=>kk; nH=>nHz
                 end select

                 ! Initialize directional height counters
                 nH=0

                 ! Create stencil bounds
                 mysth(-1)=-sth; mysth(1)=sth
                 st1l=-sts; st1u=sts
                 st2l=-sts; st2u=sts
                 select case (cell)
                 case (1) 
                    if (dir.eq.1) mysth(1)=sth-1
                    if (dir.eq.2) st1l=-sts-1
                    if (dir.eq.3) st1l=-sts-1
                 case (2) 
                    if (dir.eq.1) st1l=-sts-1
                    if (dir.eq.2) mysth(1)=sth-1
                    if (dir.eq.3) st2l=-sts-1
                 case (3)
                    if (dir.eq.1) st2l=-sts-1
                    if (dir.eq.2) st2l=-sts-1
                    if (dir.eq.3) mysth(1)=sth-1
                 end select
                                     
                 ! Loop over neighbors and accumulate interface locations 
                 do i2=st2l,st2u
                    do i1=st1l,st1u

                       ! Initialize 
                       found_full =.false.
                       found_empty=.false.
                       height=0.0_WP

                       ! Loop in +/- directions
                       count=0
                       do pm=-1,1,2

                          ! Initialize height and logicals
                          nn=0
                          if (VF(i+ii,j+jj,k+kk).gt.VOFhi) then
                             found_full=.true.
                             indexF=nn
                          else if (VF(i+ii,j+jj,k+kk).lt.VOFlo) then
                             found_empty=.true.
                             indexE=nn
                          end if

                          ! Loop over cells in column
                          height_loop: do nn=pm,mysth(pm),pm
                             if (VF(i+ii,j+jj,k+kk).gt.VOFhi.and..not.found_full) then
                                found_full=.true.
                                indexF=nn
                                exit height_loop
                             else if (VF(i+ii,j+jj,k+kk).lt.VOFlo.and..not.found_empty) then
                                ! found empty cell
                                found_empty=.true.
                                indexE=nn
                                exit height_loop
                             end if
                          end do height_loop

                          ! If good height add to set
                          if (found_full.and.found_empty) then
                             ! Update counters
                             nloc=nloc+1; nH=nH+1;
                             ! Save interface location
                             height=0.0_WP
                             do nn=min(indexF,indexE),max(indexF,indexE)
                                dx_vec=(/VFdx(i+ii),VFdy(j+jj),VFdz(k+kk)/)
                                height=height+VF(i+ii,j+jj,k+kk)*dx_vec(dir)
                             end do
                             nn=indexF; dx_vec=(/VFdx(i+ii),VFdy(j+jj),VFdz(k+kk)/)
                             Fside=+1
                             if (indexF.lt.indexE) Fside=-1
                             I_loc(:,nloc)=(/VFxm(i+ii),VFym(j+jj),VFzm(k+kk)/);
                             I_loc(dir,nloc)=I_loc(dir,nloc)+real(Fside,WP)*0.5_WP*dx_vec(dir)
                             I_loc(dir,nloc)=I_loc(dir,nloc)-real(Fside,WP)*height
                             select case(dir)
                             case(1); I_dis(nloc)=sqrt((I_loc(2,nloc)-myy)**2+(I_loc(3,nloc)-myz)**2)
                             case(2); I_dis(nloc)=sqrt((I_loc(1,nloc)-myx)**2+(I_loc(3,nloc)-myz)**2)
                             case(3); I_dis(nloc)=sqrt((I_loc(1,nloc)-myx)**2+(I_loc(2,nloc)-myy)**2)
                             end select
                             mydist=sqrt((I_loc(1,nloc)-xm(i))**2+(I_loc(2,nloc)-ym(j))**2+(I_loc(3,nloc)-zm(k))**2)
                             ! Throw away if too far away
                             ! if (mydist.gt.3.0_WP*maxval((/dx(i),dy(j),dz/))) then
                             !    nloc=nloc-1; nH=nH-1
                             !    found_full =.false.
                             !    found_empty=.false.
                             !    cycle
                             ! end if
                             ! Only keep closest point in a column
                             count=count+1
                             if (count.eq.2) then
                                if (mydist2.le.mydist) then
                                   ! Previous location is closer
                                   nloc=nloc-1; nH=nH-1
                                 else
                                   ! Overwrite previous location with this one
                                   I_loc(:,nloc-1)=I_loc(:,nloc)
                                   I_dis(  nloc-1)=I_dis(  nloc)
                                   nloc=nloc-1; nH=nH-1
                                end if
                             else 
                                mydist2=mydist
                             end if
                             found_full =.false.
                             found_empty=.false.
                          end if
                       end do

                    end do ! Loop over neighbors
                 end do

                 ! Nullify pointers
                 nullify(nn,i1,i2,nH)

              end do ! Loop over directions            

              ! Calculate normal and curvature using locations 
              ! ================================================
              ! Get estimate for normal using Young's method
              n=0
              do kk=0,1
                 do jj=0,1
                    do ii=0,1
                       n=n+1
                       norm_here(1,n)=-(interp_VOF(i+ii,j+jj,k+kk,1,cell)-interp_VOF(i+ii-1,j+jj  ,k+kk  ,1,cell))*dxi(i+ii-1)
                       norm_here(2,n)=-(interp_VOF(i+ii,j+jj,k+kk,2,cell)-interp_VOF(i+ii  ,j+jj-1,k+kk  ,2,cell))*dyi(j+jj-1)
                       norm_here(3,n)=-(interp_VOF(i+ii,j+jj,k+kk,3,cell)-interp_VOF(i+ii  ,j+jj  ,k+kk-1,3,cell))*dzi
                    end do
                 end do
              end do
              ! Average normals at each corner of cell
              normxH(i,j,k)=0.125_WP*sum(norm_here(1,:))
              normyH(i,j,k)=0.125_WP*sum(norm_here(2,:))
              normzH(i,j,k)=0.125_WP*sum(norm_here(3,:))
              ! Normalize normal
              norm=sqrt(normxH(i,j,k)**2+normyH(i,j,k)**2+normzH(i,j,k)**2)+epsilon(1.0_WP)
              normxH(i,j,k)=normxH(i,j,k)/norm
              normyH(i,j,k)=normyH(i,j,k)/norm
              normzH(i,j,k)=normzH(i,j,k)/norm
              ! Form rotated coordinate system n,t1,t2
              nv=(/normxH(i,j,k),normyH(i,j,k),normzH(i,j,k)/)
              is_done=.false.
              n=0
              do while(.not.is_done)
                 n=n+1
                 ! Form temp t2 vector
                 select case(n)
                 case(1); t2=0.0_WP; t2(3)=-1.0_WP
                 case(2); t2=0.0_WP; t2(2)=-1.0_WP
                 case(3); t2=0.0_WP; t2(1)=-1.0_WP
                 end select
                 ! Make sure t2 is not the same as n
                 if (abs(dot_product(nv,t2)).gt.0.6) cycle
                 ! Form real t1 & t2
                 t1=normalize(cross_product(nv,t2))
                 t2=normalize(cross_product(nv,t1))
                 is_done=.true.
              end do
              ! Construct rotation matrices: x'=iRot*x, x=Rot*x'
              Rot(:,1)=nv; Rot(:,2)=t1; Rot(:,3)=t2
              iRot=transpose(Rot)

              ! Rotate interface locations
              I_loc=matmul(iRot,I_loc)

              ! Form index bounds
              if (nHx.ge.nHy.and.nHx.ge.nHz.and.nHx.ge.9) then
                 nmin=1; nmax=nHx
              else if (nHy.ge.nHz.and.nHy.ge.9) then
                 nmin=1+nHx; nmax=nHx+nHy
              else if (nHz.ge.9) then
                 nmin=1+nHx+nHy; nmax=nHx+nHy+nHz
              else if (nHx+nHy+nHz.ge.9) then
                 nmin=1; nmax=nHx+nHy+nHz
              else 
                 ! Degenerative cell
                 ! Use normal from Young's method and zero curvature
                 select case (cell)
                 case (0); curv (i,j,k)=0.0_WP
                 case (1); curvU(i,j,k)=0.0_WP
                 case (2); curvV(i,j,k)=0.0_WP
                 case (3); curvW(i,j,k)=0.0_WP
                 end select
                 cycle cell_loop
              end if

              ! Fit paraboloid to heights using least squares
              allocate(A(nmax-nmin+1,6))
              allocate(At(6,nmax-nmin+1))
              allocate(B(nmax-nmin+1))
              allocate(Weight(nmax-nmin+1,nmax-nmin+1))

              ! Fill matrices
              count=0
              Weight=0.0_WP
              Wmax=0.0_WP
              do n=nmin,nmax
                 count=count+1
                 A(count,1)=I_loc(2,n)**2
                 A(count,2)=I_loc(3,n)**2
                 A(count,3)=I_loc(2,n)*I_loc(3,n)
                 A(count,4)=I_loc(2,n)
                 A(count,5)=I_loc(3,n)
                 A(count,6)=1.0_WP
                 B(count)  =I_loc(1,n)
                 Weight(count,count)=I_dis(n)
                 Wmax=max(Wmax,I_dis(n))
              end do
              Weight=Weight/Wmax
              count=0
              do n=nmin,nmax
                 count=count+1
                 Weight(count,count)=1.0_WP/(Weight(count,count)+0.1_WP)
              end do

              ! Solve system A*sol=B in least squares sense: A'A*sol=A'B
              At=transpose(A)
              AtWA=matmul(matmul(At,Weight),A)
              AtWB=matmul(matmul(At,Weight),B)
              call solve_linear_system_safe(AtWA,AtWB,sol,6)
              deallocate(A,At,B,Weight)

              x_here=(/myx,myy,myz/)
              
              ! Iterate to find location of point on paraboid that is normal to cell center
              n_rot=matmul(iRot,nv)
              x_rot=matmul(iRot,x_here) ! Change coordiante system of location

              ! Form normal vector in rotated coordinate system
              n_rot(1)=-1.0_WP
              n_rot(2)= 2.0_WP*sol(1)*x_rot(2)+sol(3)*x_rot(3)+sol(4)
              n_rot(3)= 2.0_WP*sol(2)*x_rot(3)+sol(3)*x_rot(2)+sol(5)

              ! Iterate to find 2nd order normal
              !x_here_old=0.0_WP
              locate: do iter=1,5
                 ! Recalculate point on parabloid
                 tt1=-(n_rot(2)*sol(4) - n_rot(1) + n_rot(3)*sol(5) + (n_rot(2)**2*sol(4)**2 + n_rot(3)**2*sol(5)**2 + n_rot(1)**2 &
                      - 4.0_WP*n_rot(2)**2*sol(1)*sol(6) - 4.0_WP*n_rot(3)**2*sol(2)*sol(6) + 4.0_WP*n_rot(2)**2*sol(1)*x_rot(1) &
                      + 4.0_WP*n_rot(3)**2*sol(2)*x_rot(1) - 2.0_WP*n_rot(1)*n_rot(2)*sol(4) - 2.0_WP*n_rot(1)*n_rot(3)*sol(5) &
                      + n_rot(3)**2*sol(3)**2*x_rot(2)**2 + n_rot(2)**2*sol(3)**2*x_rot(3)**2 - 4.0_WP*n_rot(2)*n_rot(3)*sol(3)*sol(6) &
                      + 2.0_WP*n_rot(2)*n_rot(3)*sol(4)*sol(5) + 4.0_WP*n_rot(2)*n_rot(3)*sol(3)*x_rot(1) - 4.0_WP*n_rot(1)*n_rot(2)*sol(1)*x_rot(2) &
                      - 2.0_WP*n_rot(1)*n_rot(3)*sol(3)*x_rot(2) - 2.0_WP*n_rot(1)*n_rot(2)*sol(3)*x_rot(3) - 4.0_WP*n_rot(1)*n_rot(3)*sol(2)*x_rot(3) &
                      - 4.0_WP*n_rot(3)**2*sol(2)*sol(4)*x_rot(2) + 2.0_WP*n_rot(3)**2*sol(3)*sol(5)*x_rot(2) - 4.0_WP*n_rot(2)**2*sol(1)*sol(5)*x_rot(3) &
                      + 2.0_WP*n_rot(2)**2*sol(3)*sol(4)*x_rot(3) - 4.0_WP*n_rot(3)**2*sol(1)*sol(2)*x_rot(2)**2 - 4.0_WP*n_rot(2)**2*sol(1)*sol(2)*x_rot(3)**2 &
                      - 2.0_WP*n_rot(2)*n_rot(3)*sol(3)**2*x_rot(2)*x_rot(3) + 4.0_WP*n_rot(2)*n_rot(3)*sol(1)*sol(5)*x_rot(2) &
                      - 2.0_WP*n_rot(2)*n_rot(3)*sol(3)*sol(4)*x_rot(2) + 4.0_WP*n_rot(2)*n_rot(3)*sol(2)*sol(4)*x_rot(3) &
                      - 2.0_WP*n_rot(2)*n_rot(3)*sol(3)*sol(5)*x_rot(3) + 8.0_WP*n_rot(2)*n_rot(3)*sol(1)*sol(2)*x_rot(2)*x_rot(3))**(0.5_WP) &
                      + 2.0_WP*n_rot(2)*sol(1)*x_rot(2) + n_rot(3)*sol(3)*x_rot(2) + n_rot(2)*sol(3)*x_rot(3) + 2.0_WP*n_rot(3)*sol(2)*x_rot(3)) &
                      /(2.0_WP*(sol(1)*n_rot(2)**2 + sol(3)*n_rot(2)*n_rot(3) + sol(2)*n_rot(3)**2)+epsilon(1.0_WP))
                 tt2=-(n_rot(2)*sol(4) - n_rot(1) + n_rot(3)*sol(5) - (n_rot(2)**2*sol(4)**2 + n_rot(3)**2*sol(5)**2 + n_rot(1)**2 &
                      - 4.0_WP*n_rot(2)**2*sol(1)*sol(6) - 4.0_WP*n_rot(3)**2*sol(2)*sol(6) + 4.0_WP*n_rot(2)**2*sol(1)*x_rot(1) &
                      + 4.0_WP*n_rot(3)**2*sol(2)*x_rot(1) - 2.0_WP*n_rot(1)*n_rot(2)*sol(4) - 2.0_WP*n_rot(1)*n_rot(3)*sol(5) &
                      + n_rot(3)**2*sol(3)**2*x_rot(2)**2 + n_rot(2)**2*sol(3)**2*x_rot(3)**2 - 4.0_WP*n_rot(2)*n_rot(3)*sol(3)*sol(6) &
                      + 2.0_WP*n_rot(2)*n_rot(3)*sol(4)*sol(5) + 4.0_WP*n_rot(2)*n_rot(3)*sol(3)*x_rot(1) - 4.0_WP*n_rot(1)*n_rot(2)*sol(1)*x_rot(2) &
                      - 2.0_WP*n_rot(1)*n_rot(3)*sol(3)*x_rot(2) - 2.0_WP*n_rot(1)*n_rot(2)*sol(3)*x_rot(3) - 4.0_WP*n_rot(1)*n_rot(3)*sol(2)*x_rot(3) &
                      - 4.0_WP*n_rot(3)**2*sol(2)*sol(4)*x_rot(2) + 2.0_WP*n_rot(3)**2*sol(3)*sol(5)*x_rot(2) - 4.0_WP*n_rot(2)**2*sol(1)*sol(5)*x_rot(3) &
                      + 2.0_WP*n_rot(2)**2*sol(3)*sol(4)*x_rot(3) - 4.0_WP*n_rot(3)**2*sol(1)*sol(2)*x_rot(2)**2 - 4.0_WP*n_rot(2)**2*sol(1)*sol(2)*x_rot(3)**2 &
                      - 2.0_WP*n_rot(2)*n_rot(3)*sol(3)**2*x_rot(2)*x_rot(3) + 4.0_WP*n_rot(2)*n_rot(3)*sol(1)*sol(5)*x_rot(2) &
                      - 2.0_WP*n_rot(2)*n_rot(3)*sol(3)*sol(4)*x_rot(2) + 4.0_WP*n_rot(2)*n_rot(3)*sol(2)*sol(4)*x_rot(3) &
                      - 2.0_WP*n_rot(2)*n_rot(3)*sol(3)*sol(5)*x_rot(3) + 8.0_WP*n_rot(2)*n_rot(3)*sol(1)*sol(2)*x_rot(2)*x_rot(3))**(0.5_WP) &
                      + 2.0_WP*n_rot(2)*sol(1)*x_rot(2) + n_rot(3)*sol(3)*x_rot(2) + n_rot(2)*sol(3)*x_rot(3) + 2.0_WP*n_rot(3)*sol(2)*x_rot(3)) &
                      /(2.0_WP*(sol(1)*n_rot(2)**2 + sol(3)*n_rot(2)*n_rot(3) + sol(2)*n_rot(3)**2)+epsilon(1.0_WP))
                 if (abs(tt1).lt.abs(tt2).and.abs(tt1).lt.3.0_WP*meshsize(i,j,k)) then
                    x_here=x_rot+n_rot*tt1
                 else if (abs(tt2).lt.3.0_WP*meshsize(i,j,k)) then
                    x_here=x_rot+n_rot*tt2
                 else
                    x_here=x_rot
                 end if
                 ! Recalculate normal in rotated coordinate system
                 n_rot(1)=-1.0_WP
                 n_rot(2)= 2.0_WP*sol(1)*x_here(2)+sol(3)*x_here(3)+sol(4)
                 n_rot(3)= 2.0_WP*sol(2)*x_here(3)+sol(3)*x_here(2)+sol(5)
                 ! Stop if close enough
                 !dd=sqrt((x_here_old(1)-x_here(1))**2+(x_here_old(2)-x_here(2))**2+(x_here_old(3)-x_here(3))**2)
                 !if (dd.lt.cvg*meshsize(i,j,k)) exit locate
                 !x_here_old=x_here
              end do locate

              ! Change coordinate system back to original and normalize
              n_here=normalize(matmul(Rot,n_rot))
              normxH(i,j,k)=n_here(1)
              normyH(i,j,k)=n_here(2)
              normzH(i,j,k)=n_here(3)

              ! Calculate interface curvature from paraboloid
              h1 =2.0_WP*sol(1)*x_here(2)+sol(3)*x_here(3)+sol(4)
              h2 =2.0_WP*sol(2)*x_here(3)+sol(3)*x_here(2)+sol(5)
              h11=2.0_WP*sol(1)
              h22=2.0_WP*sol(2)
              h12=sol(3)
              curvDir(i,j,k)=((1.0_WP+h2**2)*h11-2.0_WP*h1*h2*h12+(1.0_WP+h1**2)*h22) &
                   /(2.0_WP*(1.0_WP+h1**2+h2**2)**1.5_WP)

              ! Update direction of normal and sign of curvature
              norm=max(abs(normxH(i,j,k)),abs(normyH(i,j,k)),abs(normzH(i,j,k)))
              if (abs(normxH(i,j,k)).eq.norm) then
                 dVdN=(VF(i+2,j,k)-VF(i-2,j,k))/(xm(i+2)-xm(i-2))
                 if (dVdN*normxH(i,j,k).gt.0.0_WP) then
                    normxH (i,j,k)=-normxH (i,j,k)
                    normyH (i,j,k)=-normyH (i,j,k)
                    normzH (i,j,k)=-normzH (i,j,k)
                    curvDir(i,j,k)=-curvDir(i,j,k)
                 end if
              else if (abs(normyH(i,j,k)).eq.norm) then
                 dVdN=(VF(i,j+2,k)-VF(i,j-2,k))/(ym(j+2)-ym(j-2))
                 if (dVdN*normyH(i,j,k).gt.0.0_WP) then
                    normxH (i,j,k)=-normxH (i,j,k)
                    normyH (i,j,k)=-normyH (i,j,k)
                    normzH (i,j,k)=-normzH (i,j,k)
                    curvDir(i,j,k)=-curvDir(i,j,k)
                 end if
              else
                 dVdN=(VF(i,j,k+2)-VF(i,j,k-2))/(zm(k+2)-zm(k-2))
                 if (dVdN*normzH(i,j,k).gt.0.0_WP) then
                    normxH (i,j,k)=-normxH (i,j,k)
                    normyH (i,j,k)=-normyH (i,j,k)
                    normzH (i,j,k)=-normzH (i,j,k)
                    curvDir(i,j,k)=-curvDir(i,j,k)
                 end if
              end if

              select case (cell)
              case (0)
                 if (update_norm) then
                    normx(i,j,k)=normxH (i,j,k)
                    normy(i,j,k)=normyH (i,j,k)
                    normz(i,j,k)=normzH (i,j,k)
                 end if
                 curv (i,j,k)=curvDir(i,j,k)
              case (1)
                 curvU(i,j,k)=curvDir(i,j,k)
              case (2)
                 curvV(i,j,k)=curvDir(i,j,k)
              case (3)
                 curvW(i,j,k)=curvDir(i,j,k)
              end select

           end do cell_loop
        end do
     end do
  end do

  ! Clip the curvature
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_  
           curv (i,j,k)=max(min(curv (i,j,k),1.0_WP/meshsize(i,j,k)),-1.0_WP/meshsize(i,j,k))
           curvU(i,j,k)=max(min(curvU(i,j,k),1.0_WP/meshsize(i,j,k)),-1.0_WP/meshsize(i,j,k))
           curvV(i,j,k)=max(min(curvV(i,j,k),1.0_WP/meshsize(i,j,k)),-1.0_WP/meshsize(i,j,k))
           curvW(i,j,k)=max(min(curvW(i,j,k),1.0_WP/meshsize(i,j,k)),-1.0_WP/meshsize(i,j,k))
        end do
     end do
  end do
  
  ! Communicate
  call communication_border(curv )
  call communication_border(curvU)
  call communication_border(curvV)
  call communication_border(curvW)
  if (update_norm) then
     call communication_border(normx)
     call communication_border(normy)
     call communication_border(normz)
     call boundary_symmetry_alldir(normx,normy,normz)
  end if
  
  return
contains 
  
  ! Interpolate VOF on plane
  function interp_vof(i,j,k,dir,cell)
    implicit none
    integer, intent(in) :: i,j,k,dir,cell
    real(WP) :: interp_vof
    select case(cell)
    case (0)
       select case(dir)
       case(1); interp_vof=0.25_WP*(VF(i,j-1,k)+VF(i,j,k)+VF(i,j-1,k-1)+VF(i,j,k-1))
       case(2); interp_vof=0.25_WP*(VF(i-1,j,k)+VF(i,j,k)+VF(i-1,j,k-1)+VF(i,j,k-1))
       case(3); interp_vof=0.25_WP*(VF(i-1,j,k)+VF(i,j,k)+VF(i-1,j-1,k)+VF(i,j-1,k))
       end select
    case (1)
       select case(dir)
       case(1); interp_vof=0.125_WP*(VF(i  ,j-1,k)+VF(i  ,j,k)+VF(i  ,j-1,k-1)+VF(i  ,j,k-1) &
                                   + VF(i-1,j-1,k)+VF(i-1,j,k)+VF(i-1,j-1,k-1)+VF(i-1,j,k-1) )
       case(2); interp_vof=0.5_WP*(VF(i-1,j,k)+VF(i-1,j,k-1))
       case(3); interp_vof=0.5_WP*(VF(i-1,j,k)+VF(i-1,j-1,k))
       end select
    case (2)
       select case(dir)
       case(1); interp_vof=0.5_WP*(VF(i,j-1,k)+VF(i,j-1,k-1))
       case(2); interp_vof=0.125_WP*(VF(i-1,j  ,k)+VF(i,j  ,k)+VF(i-1,j  ,k-1)+VF(i,j  ,k-1) &
                                   + VF(i-1,j-1,k)+VF(i,j-1,k)+VF(i-1,j-1,k-1)+VF(i,j-1,k-1) )
       case(3); interp_vof=0.5_WP*(VF(i-1,j-1,k)+VF(i,j-1,k))
       end select
    case (3)
       select case(dir)
       case(1); interp_vof=0.5_WP*(VF(i,j-1,k-1)+VF(i,j,k-1))
       case(2); interp_vof=0.5_WP*(VF(i-1,j,k-1)+VF(i,j,k-1))
       case(3); interp_vof=0.125_WP*(VF(i-1,j,k  )+VF(i,j,k  )+VF(i-1,j-1,k  )+VF(i,j-1,k  ) &
                                   + VF(i-1,j,k-1)+VF(i,j,k-1)+VF(i-1,j-1,k-1)+VF(i,j-1,k-1))
       end select
    end select
    return
  end function interp_vof
 
end subroutine multiphase_height_calc
