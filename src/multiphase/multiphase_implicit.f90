module multiphase_implicit
  use multiphase_velocity
  
  ! Dummy module

end module multiphase_implicit

! ================================== !
! Form DDADI operator for U velocity !
! ================================== !
subroutine multiphase_operator_u
  use implicit
  use data
  use metric_generic
  use metric_velocity_conv
  use metric_velocity_visc
  use memory
  use time_info
  use multiphase_velocity
  use masks
  implicit none
  
  integer  :: i,j,k,st,n,s
  real(WP) :: VISCi
  real(WP) :: dt2,theta,mydiag
  
  ! Prepare scaled time step size for second order
  dt2 = dt_uvw/2.0_WP  
  
  ! Zero operators =====================================
  Ax=0.0_WP; Ay=0.0_WP; Az=0.0_WP
  
  ! X-direction ========================================
  ! Convective part
  if (imp_conv_x) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stc2,+stc1
                 do n=-stc1,+stc2
                    do s=1,4
                       Ax(j,k,i,st+n) = Ax(j,k,i,st+n) + dt2*divc_xx(i,j,k,st)*rhoUi(i+st,j,k)*interp_Ju_xm(i+st,j,n) &
                            *0.25_WP*real(cmask_x(subcell2flux_Ux_s(s),i+st+subcell2flux_Ux_i(s),j,k),WP)
                    end do
                 end do
              end do
           end do
        end do
     end do
  end if
  ! Viscous part
  if (imp_visc_x) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv2,stv1
                 VISCi=VISC(i+st,j,k)
                 do n=-stv1,stv2
                    Ax(j,k,i,st+n) = Ax(j,k,i,st+n) - &
                         dt2*divv_xx(i,j,k,st)*2.0_WP*VISCi*(grad_u_x(i+st,j,k,n)-1.0_WP/3.0_WP*divv_u(i+st,j,k,n))
                 end do
              end do
           end do
        end do
     end do
  end if
  
  ! Y-direction ========================================
  ! Convective part
  if (imp_conv_y) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stc1,+stc2
                 do n=-stc2,+stc1
                    do s=1,4
                       Ay(i,k,j,st+n) = Ay(i,k,j,st+n) + dt2*divc_xy(i,j,k,st)*rhoVi(i,j+st,k)*interp_Ju_y(i,j+st,n) &
                            *0.25_WP*real(cmask_y(subcell2flux_Uy_s(s),i+subcell2flux_Uy_i(s),j+st,k),WP)
                    end do
                 end do
              end do
           end do
        end do
     end do
  end if
  ! Viscous part
  if (imp_visc_y) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv1,stv2
                 VISCi=sum(interp_sc_xy(i,j+st,:,:)*VISC(i-st2:i+st1,j+st-st2:j+st+st1,k))
                 do n=-stv2,stv1
                    Ay(i,k,j,st+n) = Ay(i,k,j,st+n) - dt2*divv_xy(i,j,k,st)*VISCi*grad_u_y(i,j+st,k,n)
                 end do
              end do
           end do
        end do
     end do
  end if
  ! Boundary conditions
  if (icyl.eq.1 .and. jproc.eq.1 .and. (imp_conv_y .or. imp_visc_y)) then
     do k=kmin_,kmax_
        do n=0,(ndy-1)/2-1
           do st=-(ndy-1)/2,-n-1
              Ay(:,k,jmin+n,-n) = Ay(:,k,jmin+n,-n) + Ay(:,k,jmin+n,st)
              Ay(:,k,jmin+n,st) = 0.0_WP
           end do
        end do
     end do
  end if
  
  ! Z-direction ========================================
  ! Convective part
  if (imp_conv_z) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stc1,+stc2
                 do n=-stc2,+stc1
                    do s=1,4
                       Az(i,j,k,st+n) = Az(i,j,k,st+n) + dt2*divc_xz(i,j,k,st)*rhoWi(i,j,k+st)*interp_Ju_z(i,j,n) &
                            *0.25_WP*real(cmask_z(subcell2flux_Uz_s(s),i+subcell2flux_Uz_i(s),j,k+st),WP)
                    end do
                 end do
              end do
           end do
        end do
     end do
  end if
  ! Viscous part
  if (imp_visc_z) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv1,stv2
                 VISCi=sum(interp_sc_xz(i,j,:,:)*VISC(i-st2:i+st1,j,k+st-st2:k+st+st1)) 
                 do n=-stv2,stv1
                    Az(i,j,k,st+n) = Az(i,j,k,st+n) - dt2*divv_xz(i,j,k,st)*VISCi*grad_u_z(i,j,k,n)
                 end do
              end do
           end do
        end do
     end do
  end if
  
  ! Create full diagonal ===============================
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           mydiag = rho_U(i,j,k)+Ax(j,k,i,0)+Ay(i,k,j,0)+Az(i,j,k,0)
           Ax(j,k,i,0) = mydiag
           Ay(i,k,j,0) = mydiag
           Az(i,j,k,0) = mydiag
        end do
     end do
  end do
  
  ! Implicit IB treatment ==============================
  if (use_ib) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (SAu(i,j,k).gt.0.0_WP.and.VFu(i,j,k).gt.0.0_WP.and.mask_u(i,j).eq.0) then
                 theta=hf2(G(i-1,j,k),G(i,j,k)); VISCi=mu_g*mu_l/(mu_g*theta+mu_l*(1.0_WP-theta)+epsilon(1.0_WP))
                 mydiag=dt2*VISCi*SAu(i,j,k)/(VHu(i,j,k)*VFu(i,j,k)*dxm(i-1)*dy(j)*dz)
                 Ax(j,k,i,0) = Ax(j,k,i,0) + mydiag
                 Ay(i,k,j,0) = Ay(i,k,j,0) + mydiag
                 Az(i,j,k,0) = Az(i,j,k,0) + mydiag
              end if
           end do
        end do
     end do
  end if
  
  return
end subroutine multiphase_operator_u


! =============================== !
! Inverse the residual of u by    !
! using approximate factorization !
! =============================== !
subroutine multiphase_inverse_u
  use implicit
  use data
  use metric_generic
  use metric_velocity_conv
  use metric_velocity_visc
  use memory
  use time_info
  use multiphase_velocity
  use masks
  implicit none
  
  integer  :: i,j,k
      
  ! If purely explicit return
  if (.not.(imp_conv_x .or. imp_visc_x .or. &
            imp_conv_y .or. imp_visc_y .or. &
            imp_conv_z .or. imp_visc_z)) return
  
  ! Prepare the operator
  call multiphase_operator_u
  
  ! X-direction ========================================
  if (imp_conv_x .or. imp_visc_x) then
     
     ! Prepare RHS
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Rx(j,k,i) = ResU(i,j,k)*rho_U(i,j,k)
           end do
        end do
     end do
     
     ! Solve
     call linear_solver_x(5)
     
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Rx(j,k,i) = ResU(i,j,k)
           end do
        end do
     end do
  end if
  
  ! Y-direction ========================================
  if (imp_conv_y .or. imp_visc_y) then
          
     ! Prepare RHS
     if (imp_conv_x .or. imp_visc_x) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Ry(i,k,j) = Rx(j,k,i)*Ay(i,k,j,0)
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Ry(i,k,j) = Rx(j,k,i)*rho_U(i,j,k)
              end do
           end do
        end do
     end if
     
     ! Solve
     call linear_solver_y(5)
     
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Ry(i,k,j) = Rx(j,k,i)
           end do
        end do
     end do
  end if
  
  ! Z-direction ========================================
  if (imp_conv_z .or. imp_visc_z) then
     
     ! Prepare RHS
     if (imp_conv_x .or. imp_visc_x .or. imp_conv_y .or. imp_visc_y) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Rz(i,j,k) = Ry(i,k,j)*Az(i,j,k,0)
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Rz(i,j,k) = Ry(i,k,j)*rho_U(i,j,k)
              end do
           end do
        end do
     end if
     
     ! Solve
     call linear_solver_z(5)
     
     ! Get back the residual
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ResU(i,j,k) = Rz(i,j,k)
           end do
        end do
     end do
     
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ResU(i,j,k) = Ry(i,k,j)
           end do
        end do
     end do
  end if
  
  return
end subroutine multiphase_inverse_u


! ================================== !
! Form DDADI operator for V velocity !
! ================================== !
subroutine multiphase_operator_v
  use implicit
  use data
  use metric_generic
  use metric_velocity_conv
  use metric_velocity_visc
  use memory
  use time_info
  use multiphase_velocity
  use masks
  implicit none
  
  integer  :: i,j,k,st,n,s
  real(WP) :: VISCi
  real(WP) :: dt2,theta,mydiag
  
  ! Prepare scaled time step size for second order
  dt2 = dt_uvw/2.0_WP
  
  ! Zero operators =====================================
  Ax=0.0_WP; Ay=0.0_WP; Az=0.0_WP
  
  ! X-direction ========================================
  ! Convective part
  if (imp_conv_x) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stc1,+stc2
                 do n=-stc2,+stc1
                    do s=1,4
                       Ax(j,k,i,st+n) = Ax(j,k,i,st+n) + dt2*divc_yx(i,j,k,st)*rhoUi(i+st,j,k)*interp_Jv_x(i+st,j,n) &
                            *0.25_WP*real(cmask_x(subcell2flux_Vx_s(s),i+st,j+subcell2flux_Vx_j(s),k),WP)
                    end do
                 end do
              end do
           end do
        end do
     end do
  end if
  ! Viscous part
  if (imp_visc_x) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv1,stv2
                 VISCi=sum(interp_sc_xy(i+st,j,:,:)*VISC(i+st-st2:i+st+st1,j-st2:j+st1,k)) 
                 do n=-stv2,stv1
                    Ax(j,k,i,st+n) = Ax(j,k,i,st+n) - dt2*divv_yx(i,j,k,st)*VISCi*grad_v_x(i+st,j,k,n)
                 end do
              end do
           end do
        end do
     end do
  end if
  
  ! Y-direction ========================================
  ! Convective part
  if (imp_conv_y) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stc2,+stc1
                 do n=-stc1,+stc2
                    do s=1,4
                       Ay(i,k,j,st+n) = Ay(i,k,j,st+n) + dt2*divc_yy(i,j,k,st)*rhoVi(i,j+st,k)*interp_Jv_ym(i,j+st,n) &
                            *0.25_WP*real(cmask_y(subcell2flux_Vy_s(s),i,j+st+subcell2flux_Vy_j(s),k),WP)
                    end do
                 end do
              end do
           end do
        end do
     end do
  end if
  ! Viscous part
  if (imp_visc_y) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv2,stv1
                 VISCi=VISC(i,j+st,k)
                 do n=-stv1,stv2
                    Ay(i,k,j,st+n) = Ay(i,k,j,st+n) - dt2*2.0_WP*VISCi*( &
                         +divv_yy(i,j,k,st)*(grad_v_y(i,j+st,k,n)-1.0_WP/3.0_WP*divv_v(i,j+st,k,n)) &
                         -yi(j)*interpv_cyl_F_y(i,j,st)*( ymi(j+st)*interpv_cyl_v_ym(i,j+st,n)-1.0_WP/3.0_WP*divv_v(i,j+st,k,n)) )
                 end do
              end do
           end do
        end do
     end do
  end if
  ! Boundary conditions
  if (icyl.eq.1 .and. jproc.eq.1 .and. (imp_conv_y .or. imp_visc_y)) then
     do k=kmin_,kmax_
        do n=0,(ndy-1)/2-1
           do st=-(ndy-1)/2,-n-1
              Ay(:,k,jmin+n,-n) = Ay(:,k,jmin+n,-n) + Ay(:,k,jmin+n,st)
              Ay(:,k,jmin+n,st) = 0.0_WP
           end do
        end do
     end do
  end if
  
  ! Z-direction ========================================
  ! Convective part
  if (imp_conv_z) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stc1,+stc2
                 do n=-stc2,+stc1
                    do s=1,4
                       Az(i,j,k,st+n) = Az(i,j,k,st+n) + dt2*divc_yz(i,j,k,st)*rhoWi(i,j,k+st)*interp_Jv_z(i,j,n) &
                            *0.25_WP*real(cmask_z(subcell2flux_Vz_s(s),i,j+subcell2flux_Vz_j(s),k+st),WP)
                    end do
                 end do
              end do
           end do
        end do
     end do
  end if
  ! Viscous part
  if (imp_visc_z) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv1,stv2
                 VISCi=sum(interp_sc_yz(i,j,:,:)*VISC(i,j-st2:j+st1,k+st-st2:k+st+st1))
                 do n=-stv2,stv1
                    Az(i,j,k,st+n) = Az(i,j,k,st+n) - dt2*divv_yz(i,j,k,st)*VISCi*grad_v_z(i,j,k,n)
                 end do
              end do
           end do
        end do
     end do
  end if
  
  ! Create full diagonal ===============================
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           mydiag = rho_V(i,j,k)+Ax(j,k,i,0)+Ay(i,k,j,0)+Az(i,j,k,0)
           Ax(j,k,i,0) = mydiag
           Ay(i,k,j,0) = mydiag
           Az(i,j,k,0) = mydiag
        end do
     end do
  end do
  
  ! Implicit IB treatment ==============================
  if (use_ib) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (SAv(i,j,k).gt.0.0_WP.and.VFv(i,j,k).gt.0.0_WP.and.mask_v(i,j).eq.0) then
                 theta=hf2(G(i,j-1,k),G(i,j,k)); VISCi=mu_g*mu_l/(mu_g*theta+mu_l*(1.0_WP-theta)+epsilon(1.0_WP))
                 mydiag=dt2*VISCi*SAv(i,j,k)/(VHv(i,j,k)*VFv(i,j,k)*dx(i)*dym(j-1)*dz)
                 Ax(j,k,i,0) = Ax(j,k,i,0) + mydiag
                 Ay(i,k,j,0) = Ay(i,k,j,0) + mydiag
                 Az(i,j,k,0) = Az(i,j,k,0) + mydiag
              end if
           end do
        end do
     end do
  end if
  
  return
end subroutine multiphase_operator_v


! =============================== !
! Inverse the Residual of v by    !
! using approximate factorization !
! =============================== !
subroutine multiphase_inverse_v
  use implicit
  use data
  use metric_generic
  use metric_velocity_conv
  use metric_velocity_visc
  use memory
  use time_info
  use multiphase_velocity
  use masks
  implicit none
  
  integer  :: i,j,k
  real(WP) :: Acos,Asin
  
  ! If purely explicit return
  if (.not.(imp_conv_x .or. imp_visc_x .or. &
            imp_conv_y .or. imp_visc_y .or. &
            imp_conv_z .or. imp_visc_z)) return
  
  ! Prepare the operator
  call multiphase_operator_v
  
  ! X-direction ========================================
  if (imp_conv_x .or. imp_visc_x) then
     
     ! Prepare RHS
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Rx(j,k,i) = ResV(i,j,k)*rho_V(i,j,k)
           end do
        end do
     end do
     
     ! Solve
     call linear_solver_x(5)
     
     ! Axis treatment
     if (icyl.eq.1 .and. jproc.eq.1) then
        do i=imin_,imax_
           Acos = 2.0_WP*sum(Rx(jmin,:,i)*cos(zm(kmin:kmax)))/real(nz,WP)
           Asin = 2.0_WP*sum(Rx(jmin,:,i)*sin(zm(kmin:kmax)))/real(nz,WP)
           Rx(jmin,:,i) = Acos*cos(zm(kmin:kmax)) + Asin*sin(zm(kmin:kmax))
        end do
     end if
     
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Rx(j,k,i) = ResV(i,j,k)
           end do
        end do
     end do
  end if
  
  ! Y-direction ========================================
  if (imp_conv_y .or. imp_visc_y) then
          
     ! Prepare RHS
     if (imp_conv_x .or. imp_visc_x) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Ry(i,k,j) = Rx(j,k,i)*Ay(i,k,j,0)
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Ry(i,k,j) = Rx(j,k,i)*rho_V(i,j,k)
              end do
           end do
        end do
     end if
     
     ! Solve
     call linear_solver_y(5)
     
     ! Axis treatment
     if (icyl.eq.1 .and. jproc.eq.1) then
        do i=imin_,imax_
           Acos = 2.0_WP*sum(Ry(i,:,jmin)*cos(zm(kmin:kmax)))/real(nz,WP)
           Asin = 2.0_WP*sum(Ry(i,:,jmin)*sin(zm(kmin:kmax)))/real(nz,WP)
           Ry(i,:,jmin) = Acos*cos(zm(kmin:kmax)) + Asin*sin(zm(kmin:kmax))
        end do
     end if
     
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Ry(i,k,j) = Rx(j,k,i)
           end do
        end do
     end do
  end if
  
  ! Z-direction ========================================
  if (imp_conv_z .or. imp_visc_z) then
     
     ! Prepare RHS
     if (imp_conv_x .or. imp_visc_x .or. imp_conv_y .or. imp_visc_y) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Rz(i,j,k) = Ry(i,k,j)*Az(i,j,k,0)
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Rz(i,j,k) = Ry(i,k,j)*rho_V(i,j,k)
              end do
           end do
        end do
     end if
     
     ! Solve
     call linear_solver_z(5)
     
     ! Morinishi's axis treatment
     if (icyl.eq.1 .and. jproc.eq.1) then
        do i=imin_,imax_
           Acos = 2.0_WP*sum(ResV(i,jmin,:)*cos(zm(kmin:kmax)))/real(nz,WP)
           Asin = 2.0_WP*sum(ResV(i,jmin,:)*sin(zm(kmin:kmax)))/real(nz,WP)
           Rz(i,jmin,:) = Acos*cos(zm(kmin:kmax)) + Asin*sin(zm(kmin:kmax))
        end do
     end if
     
     ! Get back the residual
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ResV(i,j,k) = Rz(i,j,k)
           end do
        end do
     end do
     
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ResV(i,j,k) = Ry(i,k,j)
           end do
        end do
     end do
  end if
  
  return
end subroutine multiphase_inverse_v


! ================================== !
! Form DDADI operator for W velocity !
! ================================== !
subroutine multiphase_operator_w
  use implicit
  use data
  use metric_generic
  use metric_velocity_conv
  use metric_velocity_visc
  use memory
  use time_info
  use multiphase_velocity
  use masks
  implicit none
  
  integer  :: i,j,k,st,n,s
  real(WP) :: VISCi
  real(WP) :: dt2,theta,mydiag
  
  ! Prepare scaled time step size for second order
  dt2 = dt_uvw/2.0_WP
  
  ! Zero operators =====================================
  Ax=0.0_WP; Ay=0.0_WP; Az=0.0_WP
  
  ! X-direction ========================================
  ! Convective part
  if (imp_conv_x) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stc1,+stc2
                 do n=-stc2,+stc1
                    do s=1,4
                       Ax(j,k,i,st+n) = Ax(j,k,i,st+n) + dt2*divc_zx(i,j,k,st)*rhoUi(i+st,j,k)*interp_Jw_x(i+st,j,n) &
                            *0.25_WP*real(cmask_x(subcell2flux_Wx_s(s),i+st,j,k+subcell2flux_Wx_k(s)),WP)
                    end do
                 end do
              end do
           end do
        end do
     end do
  end if
  ! Viscous part
  if (imp_visc_x) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv1,stv2
                 VISCi=sum(interp_sc_xz(i+st,j,:,:)*VISC(i+st-st2:i+st+st1,j,k-st2:k+st1))
                 do n=-stv2,stv1
                    Ax(j,k,i,st+n) = Ax(j,k,i,st+n) - dt2*divv_zx(i,j,k,st)*VISCi*grad_w_x(i+st,j,k,n)
                 end do
              end do
           end do
        end do
     end do
  end if
  
  ! Y-direction ========================================
  ! Convective part
  if (imp_conv_y) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stc1,+stc2
                 do n=-stc2,+stc1
                    do s=1,4
                       Ay(i,k,j,st+n) = Ay(i,k,j,st+n) + dt2*divc_zy(i,j,k,st)*rhoVi(i,j+st,k)*interp_Jw_y(i,j+st,n) &
                            *0.25_WP*real(cmask_y(subcell2flux_Wy_s(s),i,j+st,k+subcell2flux_Wy_k(s)),WP)
                    end do
                 end do
              end do
           end do
        end do
     end do
  end if
  ! Viscous part
  if (imp_visc_y) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv1,stv2
                 VISCi=sum(interp_sc_yz(i,j+st,:,:)*VISC(i,j+st-st2:j+st+st1,k-st2:k+st1))
                 do n=-stv2,stv1
                    Ay(i,k,j,st+n) = Ay(i,k,j,st+n) - dt2*VISCi*( &
                         +divv_zy(i,j,k,st)*grad_w_y(i,j+st,k,n) &
                         +ymi(j)*interpv_cyl_F_ym(i,j,st)*(grad_w_y(i,j+st,k,n)-yi(j+st)*interpv_cyl_w_y(i,j+st,n)) )
                 end do
              end do
           end do
        end do
     end do
  end if
  ! Boundary conditions
  if (icyl.eq.1 .and. jproc.eq.1 .and. (imp_conv_y .or. imp_visc_y)) then
     do k=kmin_,kmax_
        do n=0,(ndy-1)/2-1
           do st=-(ndy-1)/2,-n-1
              Ay(:,k,jmin+n,-n) = Ay(:,k,jmin+n,-n) + Ay(:,k,jmin+n,st)
              Ay(:,k,jmin+n,st) = 0.0_WP
           end do
        end do
     end do
  end if
  
  ! Z-direction ========================================
  ! Convective part
  if (imp_conv_z) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stc2,+stc1
                 do n=-stc1,+stc2
                    do s=1,4
                       Az(i,j,k,st+n) = Az(i,j,k,st+n) + dt2*divc_zz(i,j,k,st)*rhoWi(i,j,k+st)*interp_Jw_zm(i,j,n) &
                            *0.25_WP*real(cmask_z(subcell2flux_Wz_s(s),i,j,k+st+subcell2flux_Wz_k(s)),WP) + &
                            dt2*ymi(j)*interp_cyl_F_z(i,j,st)*sum(interp_cyl_v_ym(i,j,:)*rhoV(i,j-stc1:j+stc2,k+st))*interp_cyl_w_zm(i,j,n)
                    end do
                 end do
              end do
           end do
        end do
     end do
  end if
  ! Viscous part
  if (imp_visc_z) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv2,stv1
                 VISCi=VISC(i,j,k+st)
                 do n=-stv1,stv2
                    Az(i,j,k,st+n) = Az(i,j,k,st+n) - &
                         dt2*divv_zz(i,j,k,st)*2.0_WP*VISCi*(grad_w_z(i,j,k,n)-1.0_WP/3.0_WP*divv_w(i,j,k+st,n))
                 end do
              end do
           end do
        end do
     end do
  end if
  
  ! Create full diagonal ===============================
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           mydiag = rho_W(i,j,k)+Ax(j,k,i,0)+Ay(i,k,j,0)+Az(i,j,k,0)
           Ax(j,k,i,0) = mydiag
           Ay(i,k,j,0) = mydiag
           Az(i,j,k,0) = mydiag
        end do
     end do
  end do
  
  ! Implicit IB treatment ==============================
  if (use_ib) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (SAw(i,j,k).gt.0.0_WP.and.VFw(i,j,k).gt.0.0_WP.and.mask_w(i,j).eq.0) then
                 theta=hf2(G(i,j,k-1),G(i,j,k)); VISCi=mu_g*mu_l/(mu_g*theta+mu_l*(1.0_WP-theta)+epsilon(1.0_WP))
                 mydiag=dt2*VISCi*SAw(i,j,k)/(VHw(i,j,k)*VFw(i,j,k)*dx(i)*dy(j)*dz)
                 Ax(j,k,i,0) = Ax(j,k,i,0) + mydiag
                 Ay(i,k,j,0) = Ay(i,k,j,0) + mydiag
                 Az(i,j,k,0) = Az(i,j,k,0) + mydiag
              end if
           end do
        end do
     end do
  end if
  
  return
end subroutine multiphase_operator_w


! =============================== !
! Inverse the Residual of w by    !
! using approximate factorization !
! =============================== !
subroutine multiphase_inverse_w
  use implicit
  use data
  use metric_generic
  use metric_velocity_conv
  use metric_velocity_visc
  use memory
  use time_info
  use multiphase_velocity
  use masks
  implicit none
  
  integer  :: i,j,k
  
  ! If purely explicit return
  if (.not.(imp_conv_x .or. imp_visc_x .or. &
            imp_conv_y .or. imp_visc_y .or. &
            imp_conv_z .or. imp_visc_z)) return
  
  ! Prepare the operator
  call multiphase_operator_w
  
  ! X-direction ========================================
  if (imp_conv_x .or. imp_visc_x) then
     
     ! Prepare RHS
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Rx(j,k,i) = ResW(i,j,k)*rho_W(i,j,k)
           end do
        end do
     end do
     
     ! Solve
     call linear_solver_x(5)
     
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Rx(j,k,i) = ResW(i,j,k)
           end do
        end do
     end do
  end if
  
  ! Y-direction ========================================
  if (imp_conv_y .or. imp_visc_y) then
          
     ! Prepare RHS
     if (imp_conv_x .or. imp_visc_x) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Ry(i,k,j) = Rx(j,k,i)*Ay(i,k,j,0)
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Ry(i,k,j) = Rx(j,k,i)*rho_W(i,j,k)
              end do
           end do
        end do
     end if
     
     ! Solve
     call linear_solver_y(5)
     
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Ry(i,k,j) = Rx(j,k,i)
           end do
        end do
     end do
  end if
  
  ! Z-direction ========================================
  if (imp_conv_z .or. imp_visc_z) then
          
     ! Prepare RHS
     if (imp_conv_x .or. imp_visc_x .or. imp_conv_y .or. imp_visc_y) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Rz(i,j,k) = Ry(i,k,j)*Az(i,j,k,0)
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Rz(i,j,k) = Ry(i,k,j)*rho_W(i,j,k)
              end do
           end do
        end do
     end if
     
     ! Solve
     call linear_solver_z(5)
     
     ! Get back the residual
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ResW(i,j,k) = Rz(i,j,k)
           end do
        end do
     end do
     
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ResW(i,j,k) = Ry(i,k,j)
           end do
        end do
     end do
  end if
  
  return
end subroutine multiphase_inverse_w
