! ============================== !
! Multiphase boundary conditions !
! ============================== !
module multiphase_boundary
  use multiphase
  use boundary
  implicit none

  character(len=str_medium) inflowVOFtype
  
end module multiphase_boundary

! ===================== !
! Module initialization !
! ===================== !
subroutine multiphase_boundary_init
  use multiphase_boundary
  implicit none
  
  ! Nothing to do if:
  ! -> not the first proc in x
  ! -> periodic in x
  if (iproc.eq.1 .and. xper.ne.1) then
     ! Parse inflow type
     call parser_read('Inflow VOF type',inflowVOFtype,'file')

     ! Select inflow type
     select case(trim(inflowVOFtype))
     case ('file')
        ! Read from file each timestep
     case ('circle')
        call multiphase_inflow_circle
     case default
        call die('multiphase_boundary: Inflow VOF type unknown')
     end select
  end if
  
  return
end subroutine multiphase_boundary_init

! ===================== ! 
! Update VOF boundaries !
! (deals with subcells) !
! ===================== !
subroutine multiphase_VOF_boundaries
  use multiphase_boundary
  implicit none

  call communication_border_4d(VOF,8,nxo_,nyo_,nzo_,nover)
  call multiphase_boundary_dirichlet
  ! call multiphase_boundary_neumann
  call multiphase_boundary_outflow


  return
end subroutine multiphase_VOF_boundaries

! ==================================== !
! Dirichlet boundary condition for VOF !
! ==================================== !
subroutine multiphase_boundary_dirichlet
  use multiphase_boundary
  use inflow
  implicit none
  
  integer :: i,j,k,nflow

  ! Nothing to do if:
  ! -> not the first proc in x
  ! -> periodic in x
  if (iproc.eq.1 .and. xper.ne.1) then

     ! Select inflow type
     select case(trim(inflowVOFtype))
     case ('file')
        ! Read inflow profile from a file 
        call inflow_file_VOF
     case ('circle')
        ! Computed in init
     case default
        call die('multiphase_boundary: Inflow VOF type unknown')
     end select

     ! Different types of inflows
     do nflow=1,ninlet
        do k=kmino_,kmaxo_
           do j=max(jmino_,inlet(nflow)%jmino),min(jmaxo_,inlet(nflow)%jmaxo)
              do i=imino,imin-1
                 VOF(1,i,j,k)  = VOF1in(j,k)
                 VOF(2,i,j,k)  = VOF2in(j,k)
                 VOF(3,i,j,k)  = VOF3in(j,k)
                 VOF(4,i,j,k)  = VOF4in(j,k)
                 VOF(5,i,j,k)  = VOF5in(j,k)
                 VOF(6,i,j,k)  = VOF6in(j,k)
                 VOF(7,i,j,k)  = VOF7in(j,k)
                 VOF(8,i,j,k)  = VOF7in(j,k)
              end do
           end do
        end do
     end do
  end if

  ! Nothing to do if:
  ! -> not the first proc in y
  ! -> periodic in y
  if (jproc.eq.1 .and. yper.ne.1) then

     ! Precompute the inflow profile from a file
     call inflowy_file_VOF

     ! Different types of inflows
     do nflow=1,ninlety
        do k=kmino_,kmaxo_
           do j=jmino,jmin-1
              do i=max(imino_,inlety(nflow)%imino),min(imaxo_,inlety(nflow)%imaxo)
                 VOF(1,i,j,k)  = VOF1iny(i,k)
                 VOF(2,i,j,k)  = VOF2iny(i,k)
                 VOF(3,i,j,k)  = VOF3iny(i,k)
                 VOF(4,i,j,k)  = VOF4iny(i,k)
                 VOF(5,i,j,k)  = VOF5iny(i,k)
                 VOF(6,i,j,k)  = VOF6iny(i,k)
                 VOF(7,i,j,k)  = VOF7iny(i,k)
                 VOF(8,i,j,k)  = VOF8iny(i,k)
              end do
           end do
        end do
     end do
  end if

  return
end subroutine multiphase_boundary_dirichlet


! ============================================= !
! Convective outflow boundary condition for VOF !
! ============================================= !
subroutine multiphase_boundary_outflow
  use multiphase_boundary
  use outflow
  use time_info
  implicit none
  
  ! real(WP) :: u_a,tmp,alpha,delta,newVOF
  ! integer :: i,j,k

  ! No special treatment
  return
  
  ! ! Nothing to do if:
  ! ! -> no outlet
  ! ! -> periodic in x
  ! if (noutlet.eq.0 .or. xper.eq.1) return
  
  ! ! Take u_a to be the maximum velocity at the exit plane
  ! if (iproc.eq.npx) then
  !    tmp = maxval(abs(U(imax,:,:)))
  ! else
  !    tmp = 0.0_WP
  ! end if
  ! call parallel_max(tmp,u_a)
  
  ! ! Outlow condition only if last cpu in x
  ! if (iproc.eq.npx) then
     
  !    if (ntime.ne.0) then
  !       ! Convective condition: u,t = -c u,x
  !       ! Implicit formulation for stability reasons
  !       delta = dt*u_a*dxmi(imax)
  !       alpha = (1.0_WP-0.5_WP*delta)/(1.0_WP+0.5_WP*delta)
  !       do k=kmin_,kmax_
  !          do j=jmin_,jmax_
  !             newVOF = alpha * VOFold(imax+1,j,k) &
  !                  + (1.0_WP-alpha) * 0.5_WP*(VOF(imax,j,k)+VOFold(imax,j,k))
  !             do i=imax+1,imaxo
  !                VOF(i,j,k) = min(max(newVOF,0.0_WP),1.0_WP)
  !             end do
  !          end do
  !       end do
  !    else
  !       ! Neumann condition
  !       do i=imax+1,imaxo
  !          VOF(i,:,:) = VOF(imax,:,:)
  !       end do
  !    end if
  ! end if
  
  return
end subroutine multiphase_boundary_outflow


! ================================= !
! Neumann boundary condition on VOF !
! ================================= !
subroutine multiphase_boundary_neumann
  use multiphase_boundary
  implicit none
  integer :: s
  
  ! Open boundary conditions
  do s=1,8
     call boundary_neumann(VOF(s,:,:,:),'-ym')
     call boundary_neumann(VOF(s,:,:,:),'+ym')
  end do
  
  return
end subroutine multiphase_boundary_neumann


! ========================== !
! Creates VOF subcell inflow !
! ========================== !
subroutine multiphase_inflow_circle
  use multiphase_tools
  use inflow
  implicit none

  integer :: s,sn,j,k,jj,kk
  integer, parameter :: nF=50
  integer, dimension(4) :: subs
  data subs (1:4) /1,3,5,7/
  real(WP) :: Dcircle,vof_tmp,myy,myz,d

  call parser_read('Inflow circle diam',Dcircle)

  ! Loop over inflow face
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        if (icyl.eq.0) then
           d=sqrt(ym(j)**2+zm(k)**2)
        else
           d=ym(j)
        end if
        
        if (abs(d-0.5_WP*Dcircle).le.2.0_WP*max(dy(j),dz)) then
           ! Looop over subcells on inflow
           do sn=1,4
              s=subs(sn)
              if (icyl.eq.0) then
                 ! Use finer mesh within the subcell
                 vof_tmp=0.0_WP
                 do kk=1,nF
                    do jj=1,nF
                       myy=ys1(s,j)+real(jj,WP)/real(nF+1,WP)*(ys2(s,j)-ys1(s,j))
                       myz=zs1(s,k)+real(kk,WP)/real(nF+1,WP)*(zs2(s,k)-zs1(s,k))
                       if (0.5_WP*Dcircle.gt.sqrt(myy**2+myz**2)) then
                          vof_tmp=vof_tmp+1.0_WP
                       end if
                    end do
                 end do
                 vof_tmp=vof_tmp/real(nF,WP)**2
              else
                 vof_tmp = ((min(0.5_WP*Dcircle,ys2(s,j)))**2-ys1(s,j)**2)/(ys2(s,j)**2-ys1(s,j)**2)
              end if

              select case(s)
              case (1)
                 VOF1in(j,k)=vof_tmp
              case (3)
                 VOF3in(j,k)=vof_tmp
              case (5)
                 VOF5in(j,k)=vof_tmp
              case (7)
                 VOF7in(j,k)=vof_tmp
              end select

           end do
        else
           ! Set VOF directly
           if (d.gt.0.5_WP*Dcircle) then
              VOF1in(j,k)=0.0_WP
              VOF3in(j,k)=0.0_WP
              VOF5in(j,k)=0.0_WP
              VOF7in(j,k)=0.0_WP
           else
              VOF1in(j,k)=1.0_WP
              VOF3in(j,k)=1.0_WP
              VOF5in(j,k)=1.0_WP
              VOF7in(j,k)=1.0_WP
           end if
        end if
     end do
  end do

  ! Duplicate common subcells
  VOF2in=VOF1in
  VOF4in=VOF3in
  VOF6in=VOF5in
  VOF8in=VOF7in          

  return
end subroutine multiphase_inflow_circle
