! ====================================== !
! Piecewise linear interface calculation !
! ====================================== !
module multiphase_plic
   use multiphase
   implicit none
 
 contains

   ! =============================== !
   ! Compute d in equation for plane !
   !         nx*x+ny*y+nz*z=d        !
   ! =============================== !
   function calc_dist(s,i,j,k,mynormx,mynormy,mynormz,myVOF) result(mydist)
     use compgeom_lookup
     implicit none 
     integer, intent(in) :: s,i,j,k
     real(WP), intent(in) :: mynormx,mynormy,mynormz,myVOF
     real(WP) :: mydist
     real(WP) :: alpha,mm1,mm2,mm3
     real(WP) :: VOFo,factor,norm
     
     ! Form plane equation variables, dealing with non-cubic cells
     mm1=mynormx*0.5_WP*dx(i)
     mm2=mynormy*0.5_WP*dy(j)
     mm3=mynormz*0.5_WP*dz

     ! Normalize mm
     norm=abs(mm1)+abs(mm2)+abs(mm3)+epsilon(1.0_WP)
     mm1=mm1/norm; mm2=mm2/norm; mm3=mm3/norm

     ! Deal with negative mm
     factor=0.0_WP
     if (mm1.lt.0.0_WP) factor=factor+mm1
     if (mm2.lt.0.0_WP) factor=factor+mm2
     if (mm3.lt.0.0_WP) factor=factor+mm3

     ! Deal with VOF>0.5
     VOFo=myVOF
     if (myVOF.gt.0.5_WP) VOFo=1.0_WP-VOFo

     ! Compute alpha
     alpha=get_alpha(abs(mm1),abs(mm2),abs(mm3),VOFo)

     ! Finish dealing with VOF>0.5
     if (myVOF.gt.0.5_WP) alpha=1.0_WP-alpha

     ! Adjust alpha due to negative mm
     alpha=alpha+factor

     ! Write plane with barycenter as origin inside cube
     alpha=alpha-mm1*0.5_WP-mm2*0.5_WP-mm3*0.5_WP

     ! Make distance consistant with original normal
     mydist=alpha*norm

     ! Write plane in a global reference frame
     mydist = mydist &
          + mynormx*(xm(i)+subx(s)*dx(i)) &
          + mynormy*(ym(j)+suby(s)*dy(j)) &
          + mynormz*(zm(k)+subz(s)*dz   )

   end function calc_dist

  ! ============================================= !
  ! Compute alpha for plane given volume of fluid !
  ! m1*x1+m2*x2+m3*x3=alpha and VOF=V             !
  ! Scardovelli & Zaleski, JCP 164,228-247 (2000) !
  ! ============================================= !
  function get_alpha(m_1,m_2,m_3,V)
    use precision
    use math
    implicit none
    
    real(WP) :: get_alpha
    real(WP), intent(in)  :: m_1,m_2,m_3,V
    real(WP) :: m1,m2,m3
    real(WP) :: m12,tmp
    real(WP) :: V1,V2,V3
    real(WP) :: a0,a1,a2,a3
    real(WP) :: p0,q0,theta
    real(WP), parameter :: eps=1.0e-16_WP
    
    ! Copy m1, m2, and m3
    m1=m_1; m2=m_2; m3=m_3
    
    ! Sort [m1,m2,m3] so that m1<m2<m3
    if (m2.lt.m1) then
       tmp=m2; m2=m1; m1=tmp
    end if
    if (m3.lt.m2) then
       tmp=m3; m3=m2; m2=tmp
    end if
    if (m2.lt.m1) then
       tmp=m2; m2=m1; m1=tmp
    end if

    ! Form constants
    m12=m1+m2
    V1=m1**2/max(6.0_WP*m2*m3,eps)
    V2=V1+0.5_WP*(m2-m1)/safe_epsilon(m3)
    if (m12.le.m3) then
       V3=0.5_WP*m12/safe_epsilon(m3)
    else
       V3=(m3**2*(3.0_WP*m12-m3)+m1**2*(m1-3.0_WP*m3)+m2**2*(m2-3.0_WP*m3))/safe_epsilon(6.0_WP*m1*m2*m3)
    end if
    
    ! Calculate alpha
    if (0.0_WP.le.V .and. V.lt.V1) then
       get_alpha=(6.0_WP*m1*m2*m3*V)**(1.0_WP/3.0_WP)
    else if (V1.le.V .and. V.lt.V2) then
       get_alpha=0.5_WP*(m1+sqrt(m1**2+8.0_WP*m2*m3*(V-V1)))
    else if (V2.le.V .and. V.lt.V3) then
       a0=m1**3+m2**3-6.0_WP*m1*m2*m3*V
       a1=-3.0_WP*(m1**2+m2**2)
       a2=3.0_WP*m12
       a3=-1.0_WP
       a0=a0/a3; a1=a1/a3; a2=a2/a3; a3=a3/a3;
       p0=a1/3.0_WP-a2**2/9.0_WP
       q0=(a1*a2-3*a0)/6.0_WP-a2**3/27.0_WP
       theta=acos(min(+1.0_WP,max(-1.0_WP,q0/sqrt(-1.0_WP*p0**3))))/3.0_WP
       get_alpha=sqrt(-p0)*(sqrt(3.0_WP)*sin(theta)-cos(theta))-a2/3.0_WP
    else
       if (m12.le.m3) then
          get_alpha=m3*V+m12*0.5_WP
       else
          a0=m1**3+m2**3+m3**3-6.0_WP*m1*m2*m3*V
          a1=-3.0_WP*(m1**2+m2**2+m3**2)
          a2=3.0_WP
          a3=-2.0_WP    
          a0=a0/a3; a1=a1/a3; a2=a2/a3; a3=a3/a3;          
          p0=a1/3.0_WP-a2**2/9.0_WP
          q0=(a1*a2-3*a0)/6.0_WP-a2**3/27.0_WP
          theta=acos(min(+1.0_WP,max(-1.0_WP,q0/sqrt(-p0**3))))/3.0_WP
          get_alpha=sqrt(-p0)*(sqrt(3.0_WP)*sin(theta)-cos(theta))-a2/3.0_WP
       end if
    end if
    
    return
  end function get_alpha
  
end module multiphase_plic


! ============================================ !
! Perform PLIC reconstruction of the interface !
! Normal vector must be provided in normx/y/z  !
! ============================================ !
subroutine multiphase_plic_calc
  use multiphase_plic
  implicit none
  
  integer  :: i,j,k,s
  real(WP) :: large_dist

  large_dist = xL+yL+zL
  
  ! Include ghost cells
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           do s=1,8 ! loop over subcells
           
              ! Cells with interface
              if (VOF(s,i,j,k).ge.VOFlo .and. VOF(s,i,j,k).le.VOFhi) then

                 dist(s,i,j,k)=calc_dist(s,i,j,k, &
                      normx(s,i,j,k),normy(s,i,j,k),normz(s,i,j,k),VOF(s,i,j,k))

              else

                 ! Cells without interface - needed for easy cutting
                 if (VOF(s,i,j,k).gt.0.5_WP) then 
                    ! Full cell
                    dist(s,i,j,k)=+large_dist
                 else  
                    ! Empty cell
                    dist(s,i,j,k)=-large_dist
                 end if

              end if

           end do
        end do
     end do
  end do
     
  return
end subroutine multiphase_plic_calc








           
           
  

