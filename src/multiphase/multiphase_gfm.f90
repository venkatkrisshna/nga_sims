! ================================ !
! Ghost Fluid Methodology for      !
! handling discontinuous variables !
! ================================ !
module multiphase_gfm
  use precision
  use data
  use partition
  use geometry
  use metric_velocity_conv
  use multiphase
  use velocity
  use multiphase_vof
  implicit none
  
  ! Small value
  real(WP), parameter :: eps=1.0e-12_WP
  
  ! Jump condition
  real(WP), dimension(:,:,:), allocatable :: jump,RP_gfm

contains
  ! Interpolate curvature to faces
  function curv_u(i,j,k)
    integer, intent(in) :: i,j,k
    real(WP) :: curv_u
    curv_u=sum(curv(i-1:i,j,k)*real(hascurv(i-1:i,j,k),WP))/max(sum(real(hascurv(i-1:i,j,k),WP)),epsilon(1.0_WP))
  end function curv_u

  function curv_v(i,j,k)
    integer, intent(in) :: i,j,k
    real(WP) :: curv_v
    curv_v=sum(curv(i,j-1:j,k)*real(hascurv(i,j-1:j,k),WP))/max(sum(real(hascurv(i,j-1:j,k),WP)),epsilon(1.0_WP))
  end function curv_v

  function curv_w(i,j,k)
    integer, intent(in) :: i,j,k
    real(WP) :: curv_w
    curv_w=sum(curv(i,j,k-1:k)*real(hascurv(i,j,k-1:k),WP))/max(sum(real(hascurv(i,j,k-1:k),WP)),epsilon(1.0_WP))
  end function curv_w
    
end module multiphase_gfm


! ================================ !
! Initialization of the GFM module !
! Assumes : - G > 0 => liquid      !
!           - G < 0 => gas         !
! ================================ !
subroutine multiphase_gfm_init
  use multiphase_gfm
  use parser
  use config
  use pressure
  implicit none
  
  ! Create & Start the timer
  call timing_create('gfm')
  call timing_start ('gfm')
  
  ! Allocate the jump condition
  allocate(jump  (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); jump=0.0_WP
  allocate(RP_gfm(imin_ :imax_ ,jmin_ :jmax_ ,kmin_ :kmax_ ))
  
  ! Stop timer
  call timing_stop('gfm')
  
  return
end subroutine multiphase_gfm_init


! ==================================== !
! Preparation of the pressure equation !
! ==================================== !
subroutine multiphase_gfm_pressure
  use multiphase_gfm
  use pressure
  use time_info
  use ib
  implicit none
  
  integer :: i, j, k
  integer :: dim,dir
  real(WP) :: jc
    
  ! Start the timer
  call timing_start('gfm')

  ! Initialize jump
  jump=0.0_WP

  ! Add EHD jump
  call multiphase_ehd_jump(jump)

  ! Recompute the operator and rhs
  lap=0.0_WP
  RP_gfm=0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_

           ! Loop over dimension
           do dim=1,3

              ! Loop over direction
              do dir=-1,+1,2

                 ! Update the operator and the RHS
                 if      (dir.eq.-1 .and. dim.eq.1) then
                    jc=0.5_WP*(jump(i-1,j,k)+jump(i,j,k))! - (VOFavg(i,j,k)-VOFavg(i-1,j,k))*curv_u(i,j,k)*sigma
                    if      (VOFavg(i-1,j,k).gt.0.5_WP.and.VOFavg(i,j,k).le.0.5_WP) then; jc=+jc+0.5_WP*(curv(i-1,j,k)+curv(i,j,k))*sigma
                    else if (VOFavg(i-1,j,k).le.0.5_WP.and.VOFavg(i,j,k).gt.0.5_WP) then; jc=-jc-0.5_WP*(curv(i-1,j,k)+curv(i,j,k))*sigma
                    else;                                                           jc=0.0_WP
                    end if
                    lap(i,j,k,dim,  0) = lap(i,j,k,dim,  0) + divc_u(i,j,k,0)*grad_Px(i  ,j  ,k  , 0)/rho_U(i,j,k)
                    lap(i,j,k,dim,dir) = lap(i,j,k,dim,dir) + divc_u(i,j,k,0)*grad_Px(i  ,j  ,k  ,-1)/rho_U(i,j,k)
                    RP_gfm(i,j,k) = RP_gfm(i,j,k) + divc_u(i,j,k,0)*grad_Px(i  ,j  ,k  ,-1)*jc/rho_U(i,j,k)
                 else if (dir.eq.+1 .and. dim.eq.1) then
                    jc=0.5_WP*(jump(i,j,k)+jump(i+1,j,k))! - (VOFavg(i,j,k)-VOFavg(i+1,j,k))*curv_u(i+1,j,k)*sigma
                    if      (VOFavg(i,j,k).le.0.5_WP.and.VOFavg(i+1,j,k).gt.0.5_WP) then; jc=+jc+0.5_WP*(curv(i,j,k)+curv(i+1,j,k))*sigma
                    else if (VOFavg(i,j,k).gt.0.5_WP.and.VOFavg(i+1,j,k).le.0.5_WP) then; jc=-jc-0.5_WP*(curv(i,j,k)+curv(i+1,j,k))*sigma
                    else;                                                           jc=0.0_WP
                    end if
                    lap(i,j,k,dim,  0) = lap(i,j,k,dim,  0) + divc_u(i,j,k,1)*grad_Px(i+1,j  ,k  ,-1)/rho_U(i+1,j,k)
                    lap(i,j,k,dim,dir) = lap(i,j,k,dim,dir) + divc_u(i,j,k,1)*grad_Px(i+1,j  ,k  , 0)/rho_U(i+1,j,k)
                    RP_gfm(i,j,k) = RP_gfm(i,j,k) + divc_u(i,j,k,1)*grad_Px(i+1,j  ,k  , 0)*jc/rho_U(i+1,j,k)
                 else if (dir.eq.-1 .and. dim.eq.2) then
                    jc=0.5_WP*(jump(i,j-1,k)+jump(i,j,k))! - (VOFavg(i,j,k)-VOFavg(i,j-1,k))*curv_v(i,j,k)*sigma
                    if      (VOFavg(i,j-1,k).gt.0.5_WP.and.VOFavg(i,j,k).le.0.5_WP) then; jc=+jc+0.5_WP*(curv(i,j-1,k)+curv(i,j,k))*sigma
                    else if (VOFavg(i,j-1,k).le.0.5_WP.and.VOFavg(i,j,k).gt.0.5_WP) then; jc=-jc-0.5_WP*(curv(i,j-1,k)+curv(i,j,k))*sigma
                    else;                                                           jc=0.0_WP
                    end if
                    lap(i,j,k,dim,  0) = lap(i,j,k,dim,  0) + divc_v(i,j,k,0)*grad_Py(i  ,j  ,k  , 0)/rho_V(i,j,k)
                    lap(i,j,k,dim,dir) = lap(i,j,k,dim,dir) + divc_v(i,j,k,0)*grad_Py(i  ,j  ,k  ,-1)/rho_V(i,j,k)
                    RP_gfm(i,j,k) = RP_gfm(i,j,k) + divc_v(i,j,k,0)*grad_Py(i  ,j  ,k  ,-1)*jc/rho_V(i,j,k)
                 else if (dir.eq.+1 .and. dim.eq.2) then
                    jc=0.5_WP*(jump(i,j,k)+jump(i,j+1,k))! - (VOFavg(i,j,k)-VOFavg(i,j+1,k))*curv_v(i,j+1,k)*sigma
                    if      (VOFavg(i,j,k).le.0.5_WP.and.VOFavg(i,j+1,k).gt.0.5_WP) then; jc=+jc+0.5_WP*(curv(i,j,k)+curv(i,j+1,k))*sigma
                    else if (VOFavg(i,j,k).gt.0.5_WP.and.VOFavg(i,j+1,k).le.0.5_WP) then; jc=-jc-0.5_WP*(curv(i,j,k)+curv(i,j+1,k))*sigma
                    else;                                                           jc=0.0_WP
                    end if
                    lap(i,j,k,dim,  0) = lap(i,j,k,dim,  0) + divc_v(i,j,k,1)*grad_Py(i  ,j+1,k  ,-1)/rho_V(i,j+1,k)
                    lap(i,j,k,dim,dir) = lap(i,j,k,dim,dir) + divc_v(i,j,k,1)*grad_Py(i  ,j+1,k  , 0)/rho_V(i,j+1,k)
                    RP_gfm(i,j,k) = RP_gfm(i,j,k) + divc_v(i,j,k,1)*grad_Py(i  ,j+1,k  , 0)*jc/rho_V(i,j+1,k)
                 else if (dir.eq.-1 .and. dim.eq.3) then
                    jc=0.5_WP*(jump(i,j,k-1)+jump(i,j,k))! - (VOFavg(i,j,k)-VOFavg(i,j,k-1))*curv_w(i,j,k)*sigma
                    if      (VOFavg(i,j,k-1).gt.0.5_WP.and.VOFavg(i,j,k).le.0.5_WP) then; jc=+jc+0.5_WP*(curv(i,j,k-1)+curv(i,j,k))*sigma
                    else if (VOFavg(i,j,k-1).le.0.5_WP.and.VOFavg(i,j,k).gt.0.5_WP) then; jc=-jc-0.5_WP*(curv(i,j,k-1)+curv(i,j,k))*sigma
                    else;                                                           jc=0.0_WP
                    end if
                    lap(i,j,k,dim,  0) = lap(i,j,k,dim,  0) + divc_w(i,j,k,0)*grad_Pz(i  ,j  ,k  , 0)/rho_W(i,j,k)
                    lap(i,j,k,dim,dir) = lap(i,j,k,dim,dir) + divc_w(i,j,k,0)*grad_Pz(i  ,j  ,k  ,-1)/rho_W(i,j,k)
                    RP_gfm(i,j,k) = RP_gfm(i,j,k) + divc_w(i,j,k,0)*grad_Pz(i  ,j  ,k  ,-1)*jc/rho_W(i,j,k)
                 else if (dir.eq.+1 .and. dim.eq.3) then
                    jc=0.5_WP*(jump(i,j,k)+jump(i,j,k+1))! - (VOFavg(i,j,k)-VOFavg(i,j,k+1))*curv_w(i,j,k+1)*sigma
                    if      (VOFavg(i,j,k).le.0.5_WP.and.VOFavg(i,j,k+1).gt.0.5_WP) then; jc=+jc+0.5_WP*(curv(i,j,k)+curv(i,j,k+1))*sigma
                    else if (VOFavg(i,j,k).gt.0.5_WP.and.VOFavg(i,j,k+1).le.0.5_WP) then; jc=-jc-0.5_WP*(curv(i,j,k)+curv(i,j,k+1))*sigma
                    else;                                                           jc=0.0_WP
                    end if
                    lap(i,j,k,dim,  0) = lap(i,j,k,dim,  0) + divc_w(i,j,k,1)*grad_Pz(i  ,j  ,k+1,-1)/rho_W(i,j,k+1)
                    lap(i,j,k,dim,dir) = lap(i,j,k,dim,dir) + divc_w(i,j,k,1)*grad_Pz(i  ,j  ,k+1, 0)/rho_W(i,j,k+1)
                    RP_gfm(i,j,k) = RP_gfm(i,j,k) + divc_w(i,j,k,1)*grad_Pz(i  ,j  ,k+1, 0)*jc/rho_W(i,j,k+1)
                 end if

              end do
           end do

        end do
     end do
  end do

  ! Add jump to RHS
  RP=RP+RP_gfm

  ! Stop the timer
  call timing_stop('gfm')

  return
end subroutine multiphase_gfm_pressure


! ================================ !
! Computation of pressure gradient !
! ================================ !
subroutine multiphase_gfm_apply_pressure
  use multiphase_gfm
  use pressure
  use time_info
  implicit none
  
  integer :: i, j, k
  integer :: dim,dir
  real(WP) :: jc

  ! Start the timer
  call timing_start('gfm')
  
  ! Loop of the interior faces
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           
           ! Loop over dimension
           do dim=1,3
              
              ! Loop over direction - ONLY -1 direction
              do dir=-1,-1

                 ! Update the operator and the RHS
                 if      (dim.eq.1) then
                    jc=0.5_WP*(jump(i-1,j,k)+jump(i,j,k))! - (VOFavg(i,j,k)-VOFavg(i-1,j,k))*curv_u(i,j,k)*sigma
                    if      (VOFavg(i-1,j,k).gt.0.5_WP.and.VOFavg(i,j,k).le.0.5_WP) then; jc=+jc+0.5_WP*(curv(i-1,j,k)+curv(i,j,k))*sigma
                    else if (VOFavg(i-1,j,k).le.0.5_WP.and.VOFavg(i,j,k).gt.0.5_WP) then; jc=-jc-0.5_WP*(curv(i-1,j,k)+curv(i,j,k))*sigma
                    else;                                                           jc=0.0_WP
                    end if
                    gradPx(i,j,k) = sum(grad_Px(i,j,k,:)*P(i-stc2:i+stc1,j,k))/rho_U(i,j,k) + grad_Px(i,j,k,0)*jc/rho_U(i,j,k)
                    rhoU(i,j,k) = rhoU(i,j,k) - dt_uvw*gradPx(i,j,k)
                 else if (dim.eq.2) then
                    jc=0.5_WP*(jump(i,j-1,k)+jump(i,j,k))! - (VOFavg(i,j,k)-VOFavg(i,j-1,k))*curv_v(i,j,k)*sigma
                    if      (VOFavg(i,j-1,k).gt.0.5_WP.and.VOFavg(i,j,k).le.0.5_WP) then; jc=+jc+0.5_WP*(curv(i,j,k)+curv(i,j-1,k))*sigma
                    else if (VOFavg(i,j-1,k).le.0.5_WP.and.VOFavg(i,j,k).gt.0.5_WP) then; jc=-jc-0.5_WP*(curv(i,j,k)+curv(i,j-1,k))*sigma
                    else;                                                           jc=0.0_WP
                    end if
                    gradPy(i,j,k) = sum(grad_Py(i,j,k,:)*P(i,j-stc2:j+stc1,k))/rho_V(i,j,k) + grad_Py(i,j,k,0)*jc/rho_V(i,j,k)
                    rhoV(i,j,k) = rhoV(i,j,k) - dt_uvw*gradPy(i,j,k)
                 else if (dim.eq.3) then
                    jc=0.5_WP*(jump(i,j,k-1)+jump(i,j,k))! - (VOFavg(i,j,k)-VOFavg(i,j,k-1))*curv_w(i,j,k)*sigma
                    if      (VOFavg(i,j,k-1).gt.0.5_WP.and.VOFavg(i,j,k).le.0.5_WP) then; jc=+jc+0.5_WP*(curv(i,j,k)+curv(i,j,k-1))*sigma
                    else if (VOFavg(i,j,k-1).le.0.5_WP.and.VOFavg(i,j,k).gt.0.5_WP) then; jc=-jc-0.5_WP*(curv(i,j,k)+curv(i,j,k-1))*sigma
                    else;                                                           jc=0.0_WP
                    end if
                    gradPz(i,j,k) = sum(grad_Pz(i,j,k,:)*P(i,j,k-stc2:k+stc1))/rho_W(i,j,k) + grad_Pz(i,j,k,0)*jc/rho_W(i,j,k)
                    rhoW(i,j,k) = rhoW(i,j,k) - dt_uvw*gradPz(i,j,k)
                 end if

              end do
           end do
        end do
     end do
  end do

  ! Stop the timer
  call timing_stop('gfm')
  
  return
end subroutine multiphase_gfm_apply_pressure


! ======================================== !
! Remove pressure correction from momentum !
! ======================================== !
subroutine multiphase_gfm_remove_pressure
  use multiphase_gfm
  use pressure
  use time_info
  implicit none

  integer :: i,j,k

  ! Start the timer
  call timing_start('gfm')

  ! Remove pressure gradient
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           rhoU(i,j,k) = rhoU(i,j,k) + dt_uvw*gradPx(i,j,k)
           rhoV(i,j,k) = rhoV(i,j,k) + dt_uvw*gradPy(i,j,k)
           rhoW(i,j,k) = rhoW(i,j,k) + dt_uvw*gradPz(i,j,k)
        end do
     end do
  end do
  ! Stop the timer
  call timing_stop('gfm')

  return
end subroutine multiphase_gfm_remove_pressure
