! ================================================= !
! Volume-of-Fluid scheme for multiphase simulations !
! ================================================= !
module multiphase_vof
  use multiphase
  use multiphase_tools
  use scalar
  use lagrange_geom
  implicit none
  
  character(len=str_medium) :: sim_name
  character(len=str_medium) :: curv_scheme

end module multiphase_vof

! ========================= !
! VOF module initialization !
! ========================= !
subroutine multiphase_vof_init
  use multiphase_vof
  implicit none

  ! Check input file for HIT/interface sim
  call parser_read('Simulation',sim_name)
  call parser_read('Curvature scheme',curv_scheme,'LS')

  ! Initialize the boundaries
  call multiphase_boundary_init
  call multiphase_VOF_boundaries

  ! Alter BCs for HIT/interface simulation
  if (trim(sim_name).eq.'HIT interface' .or. trim(sim_name) .eq. 'HITA_VOF') then
    if (irank.eq.iroot) print*,'modified VOF BCs'
    if (jproc.eq.1  ) VOF(:,:,jmino:jmin,:)=1.0_WP
    if (jproc.eq.npy) VOF(:,:,jmax:jmaxo,:)=0.0_WP
  end if


  if (trim(sim_name).eq.'HITA_VOF') then
    if(iproc.eq.1) then
      VOF(:,imin_,jmin_:jmin_+ny/2-1,:) = 1.0_WP
      VOF(:,imin_,jmin_+ny/2:jmax_,:) = 0.0_WP
    end if
  end if

  ! Update VOFavg
  call multiphase_VOFavg

  ! Initialize the band
  call multiphase_band_init
  call multiphase_band_update

  ! Interface normal
  call multiphase_normal_elvira

  ! PLIC reconstruction
  call multiphase_plic_calc

  ! Contact angle: set normal
  call multiphase_contact_init
  call multiphase_contact_normal

  ! Interface curvature
  select case(trim(curv_scheme))
  case ('height')
     call multiphase_curv_calc
  case ('height_4')
     call multiphase_curvh4_calc
  case ('3L')
     call multiphase_curv3L_init
     call multiphase_curv3L_calc
  case ('LS')
     call multiphase_curvLS_init
     call multiphase_curvLS_calc
  case default
     call die('Unknown curvature scheme')
  end select

  ! Standing wave test case
  call multiphase_wave_init 
  
  return
end subroutine multiphase_vof_init

! ================== !
! VOF module prestep !
! ================== !
subroutine multiphase_vof_prestep
  use multiphase_vof
  implicit none

  ! Save old VOF field
  VOFold=VOF
  VOFavgold=VOFavg

  ! Save the old PLIC
  normxold=normx
  normyold=normy
  normzold=normz
  distold=dist

  ! Save the old band
  bandold=band

  return
end subroutine multiphase_vof_prestep
  

! ========================================== !
! Transport VOF using semi-Lagrangian fluxes !
! ========================================== !
subroutine multiphase_vof_transport
  use multiphase_vof
  use metric_generic
  use multiphase_fluxes
  implicit none

  integer :: i,j,k,s

  ! Update VOF with semi-Lag fluxes
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Only work with interfacial cells
           if (abs(band(i,j,k)).le.nband_CFL) then
              ! Loop over subcells
              do s=1,8
                 ! Update VOF field
                 VOF(s,i,j,k)=(VOFold(s,i,j,k)*0.125_WP*dx(i)*dy(j)*dz &
                      + SLF_liqx(s,i,j,k) - SLF_liqx(sc_sx(s),i+sc_ip(s),j,k) &
                      + SLF_liqy(s,i,j,k) - SLF_liqy(sc_sy(s),i,j+sc_jp(s),k) &
                      + SLF_liqz(s,i,j,k) - SLF_liqz(sc_sz(s),i,j,k+sc_kp(s)) &
                                      ) / ( 0.125_WP*dx(i)*dy(j)*dz &
                      + SLF_volx(s,i,j,k) - SLF_volx(sc_sx(s),i+sc_ip(s),j,k) &
                      + SLF_voly(s,i,j,k) - SLF_voly(sc_sy(s),i,j+sc_jp(s),k) &
                      + SLF_volz(s,i,j,k) - SLF_volz(sc_sz(s),i,j,k+sc_kp(s)) )
              end do
           end if
        end do
     end do
  end do

  ! Communicate
  call multiphase_VOF_boundaries

  ! Alter BCs for HIT/interface simulation
  if (trim(sim_name).eq.'HIT interface' .or. trim(sim_name) .eq. 'HITA_VOF') then
    if (jproc.eq.1  ) VOF(:,:,jmino:jmin,:)=1.0_WP
    if (jproc.eq.npy) VOF(:,:,jmax:jmaxo,:)=0.0_WP
  end if


  if (trim(sim_name).eq.'HITA_VOF') then
    if(iproc.eq.1) then
      VOF(:,imin_,jmin_:jmin_+ny/2-1,:) = 1.0_WP
      VOF(:,imin_,jmin_+ny/2:jmax_,:) = 0.0_WP
    end if
  end if

  ! Update VOFavg
  call multiphase_VOFavg

  ! Update VOF in velocity cells
  call multiphase_VOFvel

  ! Compute updated band
  call multiphase_band_update

  ! Interface normal
  call multiphase_normal_elvira

  ! Interface reconstruction
  call multiphase_plic_calc
  
  ! Contact angle: set normal
  call multiphase_contact_normal

  ! Interface curvature
  select case(trim(curv_scheme))
  case ('height')
     call multiphase_curv_calc
  case ('height_4')
     call multiphase_curvh4_calc
  case ('3L')
     call multiphase_curv3L_calc
  case ('LS')
     call multiphase_curvLS_calc
  case default
     call die('Unknown curvature scheme')
  end select

  ! Standing wave test case
  call multiphase_wave
  
  return
end subroutine multiphase_vof_transport


! ====================== !
! Compute average of VOF !
! ====================== !
subroutine multiphase_VOFavg
  use multiphase_vof
  implicit none
  integer :: i,j,k
  
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           VOFavg(i,j,k)=0.125_WP*sum(VOF(:,i,j,k))
        end do
     end do
  end do
  
  return
end subroutine multiphase_VOFavg

! ========================== !
! Compute VOF in U/V/W cells !
! ========================== !
subroutine multiphase_VOFvel
  use multiphase_vof
  use multiphase_fluxes
  implicit none
  integer :: i,j,k,s

  VOF_U=0.0_WP; VOF_V=0.0_WP; VOF_W=0.0_WP
  do k=kmino_+1,kmaxo_
     do j=jmino_+1,jmaxo_
        do i=imino_+1,imaxo_
           do s=1,8
              VOF_U(i,j,k)=VOF_U(i,j,k)+0.125_WP*VOF(s,i+u2sub_i(s),j,k)
              VOF_V(i,j,k)=VOF_V(i,j,k)+0.125_WP*VOF(s,i,j+v2sub_j(s),k)
              VOF_W(i,j,k)=VOF_W(i,j,k)+0.125_WP*VOF(s,i,j,k+w2sub_k(s))
           end do
        end do
     end do
  end do
  
  return
end subroutine multiphase_VOFvel

! ======================= !
! Standing Wave Test Case !
! ======================= !
subroutine multiphase_wave_init
  use multiphase_vof
  implicit none
  
  ! Check if wave simulation
  if (trim(sim_name).ne.'wave') return

  ! Create monitor file
  call monitor_create_file_step('Wave_amp',2)
  call monitor_set_header(1,'Amplitude','r')
  call monitor_set_header(2,'Fit Error','r')

  ! Compute initial values
  call multiphase_wave

  return
end subroutine multiphase_wave_init

subroutine multiphase_wave
  use multiphase_vof
  implicit none
  
  integer :: s,i,j,k,n,npoint,narray
  real(WP) :: Amp,L2,myL2n,L2n,myL2d,L2d
  real(WP), dimension(:,:), allocatable :: point,point_tmp
  real(WP), dimension(:), allocatable :: A,b
  real(WP) :: myNum,Num,myDen,Den

  ! Check if wave simulation
  if (trim(sim_name).ne.'wave') return

  ! Caclulate points on interface
  npoint=0
  narray=100
  allocate(point(3,narray))
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           do s=1,8
              if (VOF(s,i,j,k).gt.VOFlo.and.VOF(s,i,j,k).lt.VOFhi) then
                 npoint=npoint+1
                 point(:,npoint)=get_BC(s,i,j,k)
              end if
              ! Check size of point and reallocate as necessary
              if (npoint.gt.narray-5) then
                 allocate(point_tmp(3,narray))
                 point_tmp=point
                 deallocate(point)
                 narray=narray+100
                 allocate(point(3,narray))
                 point(:,1:narray-100)=point_tmp(:,:)
                 deallocate(point_tmp)
              end if
           end do
        end do
     end do
  end do
  
  ! Fit A*cos(x) function
  allocate(A(1:npoint))
  allocate(b(1:npoint))
  A=cos(point(1,1:npoint))
  b=    point(2,1:npoint)
  myNum=sum(A(:)*b(:))
  myDen=sum(A(:)*A(:))
  call parallel_sum(myNum,Num)
  call parallel_sum(myDen,Den)
  Amp=Num/Den
  deallocate(A,B)

  ! Compute L2 error
  myL2n=0.0_WP
  myL2d=0.0_WP
  do n=1,npoint
     myL2n=myL2n+(point(2,n)-Amp*cos(point(1,n)))**2
     myL2d=myL2d+(           Amp*cos(point(1,n)))**2
  end do
  call parallel_sum(myL2n,L2n)
  call parallel_sum(myL2d,L2d)
  L2=sqrt(L2n)/max(sqrt(L2d),epsilon(1.0_WP))

  call monitor_select_file("Wave_amp")
  call monitor_set_single_value(1,Amp)
  call monitor_set_single_value(2,L2)
  
  return

contains
  ! Compute barycenter of interface
  function get_BC(s,i,j,k) result(BC)
    integer, intent(in) :: s,i,j,k
    real(WP), dimension(3) :: BC
    integer :: n,nn,ninter 
    real(WP), dimension(3) :: cen
    real(WP) :: sdx2,sdy2,sdz2
    real(WP), dimension(3,8) :: cellverts
    real(WP), dimension(3,7) :: inter_pt
    real(WP), dimension(3) :: e1,e2,l,l0
    real(WP) :: ldotn,d

    ! Cell center
    cen=(/ xm(i)+subx(s)*dx(i), &
         ym(j)+suby(s)*dy(j), &
         zm(k)+subz(s)*dz    /)

    ! Subcell size/2
    sdx2=0.25_WP*dx(i);
    sdy2=0.25_WP*dy(j);
    sdz2=0.25_WP*dz;

    ! Cell vertices
    cellverts(:,1)=cen+(/-sdx2,-sdy2,-sdz2/)
    cellverts(:,2)=cen+(/+sdx2,-sdy2,-sdz2/)
    cellverts(:,3)=cen+(/-sdx2,+sdy2,-sdz2/)
    cellverts(:,4)=cen+(/+sdx2,+sdy2,-sdz2/)
    cellverts(:,5)=cen+(/-sdx2,-sdy2,+sdz2/)
    cellverts(:,6)=cen+(/+sdx2,-sdy2,+sdz2/)
    cellverts(:,7)=cen+(/-sdx2,+sdy2,+sdz2/)
    cellverts(:,8)=cen+(/+sdx2,+sdy2,+sdz2/)

    ! Compute intersections of PLIC with cell boundaries
    ninter=0
    do n=1,12
       ! Get edge
       e1(:)=cellverts(:,verts2edge(1,n))
       e2(:)=cellverts(:,verts2edge(2,n))

       ! Equation of line  p=d*l+l0
       l0=e1
       l =e2-e1
       ldotn=dot_product(l,(/normx(s,i,j,k),normy(s,i,j,k),normz(s,i,j,k)/))
       if (abs(ldotn).gt.epsilon(0.0_WP)) then ! intersection of line and PLIC
          d=(dist(s,i,j,k)-dot_product(l0,(/normx(s,i,j,k),normy(s,i,j,k),normz(s,i,j,k)/))) &
               / ldotn
          if (d.ge.0.and.d.le.1) then ! On edge
             ninter=ninter+1
             inter_pt(:,ninter)=l0+d*l
             ! Check that this point is unique
             d=huge(1.0_WP)
             do nn=1,ninter-1
                d=min(d,sqrt(sum((inter_pt(:,nn)-inter_pt(:,ninter))**2)))
             end do
             if (d.lt.1e-12*min(dx(i),dy(j),dz)) then ! not unique
                ninter=ninter-1
             end if
          end if
       end if
    end do

    ! Create middle of PLIC based on intersections
    BC(1)=sum(inter_pt(1,1:ninter))/real(ninter,WP)
    BC(2)=sum(inter_pt(2,1:ninter))/real(ninter,WP)
    BC(3)=sum(inter_pt(3,1:ninter))/real(ninter,WP)
    
    return
  end function get_BC

end subroutine multiphase_wave

