! ========================= !
! Separate mesh to compute  !
! Electric potential module !
! ========================= !
module multiphase_emesh
  use multiphase_ehd
  implicit none

  ! Electric Potential Mesh - Cartesian only
  real(WP), dimension(:), allocatable :: e_x , e_y , e_z
  real(WP), dimension(:), allocatable :: e_xm, e_ym, e_zm
  real(WP), dimension(:), allocatable :: e_dx, e_dy, e_dz
  integer  :: e_nx , e_ny , e_nz
  integer  :: e_nx_, e_ny_, e_nz_
  real(WP) :: e_Lx, e_Ly, e_Lz
  integer  :: e_imin  , e_imax  , e_jmin  , e_jmax  , e_kmin  , e_kmax
  integer  :: e_imino , e_imaxo , e_jmino , e_jmaxo , e_kmino , e_kmaxo
  integer  :: e_imin_ , e_imax_ , e_jmin_ , e_jmax_ , e_kmin_ , e_kmax_
  integer  :: e_imino_, e_imaxo_, e_jmino_, e_jmaxo_, e_kmino_, e_kmaxo_
  integer, parameter :: e_nover=1  ! Number of ghost cells
  real(WP) :: bellrad

  ! Arrays of processor index extents
  integer, dimension(:), allocatable :: e_imin_a, e_imax_a, e_jmin_a, e_jmax_a, e_kmin_a, e_kmax_a
  integer, dimension(:), allocatable ::   imin_a,   imax_a,   jmin_a,   jmax_a,   kmin_a,   kmax_a

  ! Overlap Regions
  ! Indices on the e-mesh
  integer, dimension(:), allocatable :: e_imin_ol,e_imax_ol
  integer, dimension(:), allocatable :: e_jmin_ol,e_jmax_ol
  integer, dimension(:), allocatable :: e_kmin_ol,e_kmax_ol
  logical, dimension(:), allocatable :: ns2e_overlap,e2ns_overlap
  ! Indicies on the NS mesh
  integer, dimension(:), allocatable :: imin_ol,imax_ol
  integer, dimension(:), allocatable :: jmin_ol,jmax_ol
  integer, dimension(:), allocatable :: kmin_ol,kmax_ol

  ! Comm List
  type comm_list_type
     integer :: send_proc
     integer :: recv_proc
     integer :: nx_ol,ny_ol,nz_ol
     integer :: imin_ol,imax_ol,jmin_ol,jmax_ol,kmin_ol,kmax_ol
  end type comm_list_type
  integer, dimension(:), allocatable :: ns2e_comm_list_length, e2ns_comm_list_length
  type(comm_list_type), dimension(:), allocatable :: ns2e_comm_list, e2ns_comm_list
  ! MPI type for comm_list
  integer :: MPI_COMMLIST

  ! Data arrays
  real(WP), dimension(:,:,:), allocatable :: ns_q,e_q,ns_v,e_v,e_v0,lapcn

  ! HYPRE objects
  integer :: e_dim_hypre
  integer(kind=8) :: e_grid
  integer(kind=8) :: e_stencil
  integer(kind=8) :: e_matrix
  integer(kind=8) :: e_rhs
  integer(kind=8) :: e_sol
  integer(kind=8) :: e_solver
  integer(kind=8) :: e_precond
  integer, dimension(3) :: e_lower,e_upper
  integer, dimension(3) :: e_periodicity_hypre
  integer,  dimension(:), allocatable :: e_sten_ind
  real(WP), dimension(:), allocatable :: val
  integer :: e_stencil_size
  integer :: e_precond_id
  integer :: e_max_iter=1000
  real(WP) :: e_cvg=1e-13_WP

end module multiphase_emesh

! ============== !
! Initialization !
! ============== !
subroutine multiphase_emesh_init
  use multiphase_emesh
  implicit none

  call multiphase_emesh_create
  call multiphase_emesh_partition
  call multiphase_emesh_data_init
  call multiphase_emesh_ns2e_interp_init
  call multiphase_emesh_e2ns_interp_init
  call multiphase_emesh_ns2e_commlist
  call multiphase_emesh_e2ns_commlist
  call multiphase_emesh_hypre_init
  call multiphase_emesh_hypre_solver_init

  return
end subroutine multiphase_emesh_init

! ============================ !
! E-mesh step                  !
! - compute electric potential !
! ============================ !
subroutine multiphase_emesh_step
  use multiphase_emesh
  implicit none

  call multiphase_emesh_ns2e_comm          ! NS2E data migration
  call multiphase_emesh_poisson_solver     ! Poisson Solver
  call multiphase_emesh_e2ns_comm          ! E2NS data migration

  return
end subroutine multiphase_emesh_step

! ======================== !
! Create the seperate mesh !
! ======================== !
subroutine multiphase_emesh_create
  use multiphase_emesh
  implicit none
  integer :: i,j,k
  real(WP) :: stretchx,stretchy,stretchz

  ! Read mesh parameters
  call parser_read('emesh nx',e_nx)
  call parser_read('emesh ny',e_ny)
  call parser_read('emesh nz',e_nz)
  call parser_read('emesh Lx',e_Lx)
  call parser_read('emesh Ly',e_Ly)
  call parser_read('emesh Lz',e_Lz)
  call parser_read('emesh Stretching x',stretchx,0.0_WP)
  call parser_read('emesh Stretching y',stretchy,0.0_WP)
  call parser_read('emesh Stretching z',stretchz,0.0_WP)

  ! Set the global indices
  e_imino = 1
  e_imin  = e_imino + e_nover
  e_imax  = e_imin  + e_nx - 1
  e_imaxo = e_imax  + e_nover

  e_jmino = 1
  e_jmin  = e_jmino + e_nover
  e_jmax  = e_jmin  + e_ny - 1
  e_jmaxo = e_jmax  + e_nover

  e_kmino = 1
  e_kmin  = e_kmino + e_nover
  e_kmax  = e_kmin  + e_nz - 1
  e_kmaxo = e_kmax  + e_nover

  ! Allocate Arrays
  allocate(e_x(e_imino:e_imaxo+1))
  allocate(e_y(e_jmino:e_jmaxo+1))
  allocate(e_z(e_kmino:e_kmaxo+1))

  ! Compute stretched e-mesh coordinates
  ! x coordinates
  do i=e_imino,e_imaxo+1
     e_x(i) = real(i-e_imin,WP)/real(e_nx,WP) +&
          exp(stretchx*real(i-e_imin,WP)/real(e_nx,WP))-1.0_WP
  end do
  e_x=e_Lx*e_x/e_x(e_imax+1)
  ! y coordinates
  do j=e_jmino,e_jmaxo+1
     e_y(j) = real(j-e_jmin,WP)/real(e_ny,WP) +&
          exp(stretchy*real(j-e_jmin,WP)/real(e_ny,WP))-1.0_WP
  end do
  e_y=e_Ly*e_y/e_y(e_jmax+1)
  ! z coordinates
  do k=e_kmino,e_kmaxo+1
     e_z(k) = real(k-e_kmin,WP)/real(e_nz,WP) +&
          exp(stretchz*real(k-e_kmin,WP)/real(e_nz,WP))-1.0_WP
  end do
  e_z=(e_Lz*e_z/e_z(e_kmax+1))-e_Lz*0.5

  ! Allocate the arrays
  allocate(e_xm (e_imino:e_imaxo))
  allocate(e_ym (e_jmino:e_jmaxo))
  allocate(e_zm (e_kmino:e_kmaxo))
  allocate(e_dx(e_imin:e_imax))
  allocate(e_dy(e_jmin:e_jmax))
  allocate(e_dz(e_kmin:e_kmax))

  ! Compute location of cell centers
  do i=e_imino,e_imaxo
     e_xm(i) = 0.5_WP*(e_x(i)+e_x(i+1))
  end do
  do j=e_jmino,e_jmaxo
     e_ym(j) = 0.5_WP*(e_y(j)+e_y(j+1))
  end do
  do k=e_kmino,e_kmaxo
     e_zm(k) = 0.5_WP*(e_z(k)+e_z(k+1))
  end do

  ! Compute mesh sizes
  do i=e_imin,e_imax
     e_dx(i) = e_xm(i+1) - e_xm(i)
  end do
  do j=e_jmin,e_jmax
     e_dy(j) = e_ym(j+1) - e_ym(j)
  end do
  do k=e_kmin,e_kmax
     e_dz(k) = e_zm(k+1) - e_zm(k)
  end do

  call parser_read('Bell radius',bellrad)

  return
end subroutine multiphase_emesh_create

! ========================= !
! Partition mesh onto procs !
! ========================= !
subroutine multiphase_emesh_partition
  use multiphase_emesh
  implicit none

  integer :: q,r,ierr

  ! Set initial partitions along x
  if (npx.gt.e_nx) call die('partition_init: e_nx has to be greater or equal to npx')
  q = e_nx/npx
  r = mod(e_nx,npx)
  if (iproc<=r) then
     e_nx_   = q+1
     e_imin_ = e_imin + (iproc-1)*(q+1)
  else
     e_nx_   = q
     e_imin_ = e_imin + r*(q+1) + (iproc-r-1)*q
  end if
  e_imax_  = e_imin_ + e_nx_ - 1
  e_imino_ = e_imin_ - e_nover
  e_imaxo_ = e_imax_ + e_nover

  ! Set initial partitions along y
  if (npy.gt.e_ny) call die('partition_init: e_ny has to be greater or equal to npy')
  q = e_ny/npy
  r = mod(e_ny,npy)
  if (jproc<=r) then
     e_ny_   = q+1
     e_jmin_ = e_jmin + (jproc-1)*(q+1)
  else
     e_ny_   = q
     e_jmin_ = e_jmin + r*(q+1) + (jproc-r-1)*q
  end if
  e_jmax_  = e_jmin_ + e_ny_ - 1
  e_jmino_ = e_jmin_ - e_nover
  e_jmaxo_ = e_jmax_ + e_nover

  ! Set initial partitions along z
  if (npz.gt.e_nz) call die('partition_init: e_nz has to be greater or equal to npz')
  q = e_nz/npz
  r = mod(e_nz,npz)
  if (kproc<=r) then
     e_nz_   = q+1
     e_kmin_ = e_kmin + (kproc-1)*(q+1)
  else
     e_nz_   = q
     e_kmin_ = e_kmin + r*(q+1) + (kproc-r-1)*q
  end if
  e_kmax_  = e_kmin_ + e_nz_ - 1
  e_kmino_ = e_kmin_ - e_nover
  e_kmaxo_ = e_kmax_ + e_nover

  ! Create arrays of processor indices
  allocate(e_imin_a(1:nproc))
  allocate(e_imax_a(1:nproc))
  allocate(e_jmin_a(1:nproc))
  allocate(e_jmax_a(1:nproc))
  allocate(e_kmin_a(1:nproc))
  allocate(e_kmax_a(1:nproc))
  allocate(  imin_a(1:nproc))
  allocate(  imax_a(1:nproc))
  allocate(  jmin_a(1:nproc))
  allocate(  jmax_a(1:nproc))
  allocate(  kmin_a(1:nproc))
  allocate(  kmax_a(1:nproc))
  call MPI_ALLGATHER(e_imin_,1,MPI_INTEGER,e_imin_a,1,MPI_INTEGER,comm,ierr)
  call MPI_ALLGATHER(e_imax_,1,MPI_INTEGER,e_imax_a,1,MPI_INTEGER,comm,ierr)
  call MPI_ALLGATHER(e_jmin_,1,MPI_INTEGER,e_jmin_a,1,MPI_INTEGER,comm,ierr)
  call MPI_ALLGATHER(e_jmax_,1,MPI_INTEGER,e_jmax_a,1,MPI_INTEGER,comm,ierr)
  call MPI_ALLGATHER(e_kmin_,1,MPI_INTEGER,e_kmin_a,1,MPI_INTEGER,comm,ierr)
  call MPI_ALLGATHER(e_kmax_,1,MPI_INTEGER,e_kmax_a,1,MPI_INTEGER,comm,ierr)
  call MPI_ALLGATHER(  imin_,1,MPI_INTEGER,  imin_a,1,MPI_INTEGER,comm,ierr)
  call MPI_ALLGATHER(  imax_,1,MPI_INTEGER,  imax_a,1,MPI_INTEGER,comm,ierr)
  call MPI_ALLGATHER(  jmin_,1,MPI_INTEGER,  jmin_a,1,MPI_INTEGER,comm,ierr)
  call MPI_ALLGATHER(  jmax_,1,MPI_INTEGER,  jmax_a,1,MPI_INTEGER,comm,ierr)
  call MPI_ALLGATHER(  kmin_,1,MPI_INTEGER,  kmin_a,1,MPI_INTEGER,comm,ierr)
  call MPI_ALLGATHER(  kmax_,1,MPI_INTEGER,  kmax_a,1,MPI_INTEGER,comm,ierr)

  return
end subroutine multiphase_emesh_partition

! ======================= !
! Inititalize data arrays !
! ======================= !
subroutine multiphase_emesh_data_init
  use multiphase_emesh
  implicit none

  allocate(ns_q(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))            ; ns_q=irank
  allocate(ns_v(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))            ;
  allocate(e_q (e_imino_:e_imaxo_,e_jmino_:e_jmaxo_,e_kmino_:e_kmaxo_));
  allocate(e_v (e_imino_:e_imaxo_,e_jmino_:e_jmaxo_,e_kmino_:e_kmaxo_)); e_v=0

  return  
end subroutine multiphase_emesh_data_init

! ================================================= !
! ns2e interpolation init                           !
! - finds the NS-mesh indices for the nth proc that !
!   overlap with this procs e-mesh                  !
! ================================================= !
subroutine multiphase_emesh_ns2e_interp_init
  use multiphase_emesh
  implicit none

  integer :: n,i,j,k
  logical :: ns2e_overlap_x,ns2e_overlap_y,ns2e_overlap_z

  ! Allocate arrays
  allocate(imin_ol(1:nproc)); imin_ol=0
  allocate(imax_ol(1:nproc)); imax_ol=0
  allocate(jmin_ol(1:nproc)); jmin_ol=0
  allocate(jmax_ol(1:nproc)); jmax_ol=0
  allocate(kmin_ol(1:nproc)); kmin_ol=0
  allocate(kmax_ol(1:nproc)); kmax_ol=0
  allocate(ns2e_overlap(1:nproc))

  ! Loop over processors of the NS-mesh
  do n=1,nproc

     ns2e_overlap_x=.false.
     ns2e_overlap_y=.false.
     ns2e_overlap_z=.false.

     ! Check for overlap in x-direction
     if ( xm(imin_a(n)).le.e_x(e_imax_+1) .and. &
          xm(imax_a(n)).ge.e_x(e_imin_) ) then ! overlaps
        ns2e_overlap_x=.true.
        ! Finds back of overlap region
        do i=imin_a(n),imax_a(n)
           if (xm(i).ge.e_x(e_imin_)) then
              imin_ol(n)=i
              exit
           end if
        end do
        ! Finds front of overlap region
        do i=imax_a(n),imin_a(n),-1
           if (xm(i).le.e_x(e_imax_+1)) then
              imax_ol(n)=i
              exit
           end if
        end do
     end if

     ! Check for overlap in y-direction
     if ( ym(jmin_a(n)).le.e_y(e_jmax_+1) .and. &
          ym(jmax_a(n)).ge.e_y(e_jmin_) ) then ! overlaps
        ns2e_overlap_y=.true.
        ! Finds back of overlap region
        do j=jmin_a(n),jmax_a(n)
           if (ym(j).ge.e_y(e_jmin_)) then
              jmin_ol(n)=j
              exit
           end if
        end do
        ! Finds front of overlap region
        do j=jmax_a(n),jmin_a(n),-1
           if (ym(j).le.e_y(e_jmax_+1)) then
              jmax_ol(n)=j
              exit
           end if
        end do
     end if

     ! Check for overlap in z-direction
     if ( zm(kmin_a(n)).le.e_z(e_kmax_+1) .and. &
          zm(kmax_a(n)).ge.e_z(e_kmin_) ) then ! overlaps
        ns2e_overlap_z=.true.
        ! Finds back of overlap region
        do k=kmin_a(n),kmax_a(n)
           if (zm(k).ge.e_z(e_kmin_)) then
              kmin_ol(n)=k
              exit
           end if
        end do
        ! Finds front of overlap region
        do k=kmax_a(n),kmin_a(n),-1
           if (zm(k).le.e_z(e_kmax_+1)) then
              kmax_ol(n)=k
              exit
           end if
        end do
     end if

     if (ns2e_overlap_x.and.ns2e_overlap_y.and.ns2e_overlap_z) then
        ns2e_overlap(n)=.true.
     else
        ns2e_overlap(n)=.false.
     end if

  end do

  return
end subroutine multiphase_emesh_ns2e_interp_init

! ================================================ !
! e2ns interpolation init                          !
! - finds the e-mesh indices for the nth proc that !
!   overlap with this procs NS mesh                !
! ================================================ !
subroutine multiphase_emesh_e2ns_interp_init
  use multiphase_emesh
  implicit none

  integer :: n,i,j,k
  logical :: e2ns_overlap_x,e2ns_overlap_y,e2ns_overlap_z

  ! Allocate arrays
  allocate(e_imin_ol(1:nproc)); e_imin_ol=0
  allocate(e_imax_ol(1:nproc)); e_imax_ol=0
  allocate(e_jmin_ol(1:nproc)); e_jmin_ol=0
  allocate(e_jmax_ol(1:nproc)); e_jmax_ol=0
  allocate(e_kmin_ol(1:nproc)); e_kmin_ol=0
  allocate(e_kmax_ol(1:nproc)); e_kmax_ol=0
  allocate(e2ns_overlap(1:nproc))

  ! Loop over processors of the e-mesh
  do n=1,nproc

     e2ns_overlap_x=.false.
     e2ns_overlap_y=.false.
     e2ns_overlap_z=.false.

     ! Check for overlap in x-direction
     if ( e_xm(e_imin_a(n)).le.x(imax_+1) .and. &
          e_xm(e_imax_a(n)).ge.x(imin_) ) then ! overlaps
        e2ns_overlap_x=.true.
        ! Finds left of overlap region
        do i=e_imin_a(n),e_imax_a(n)
           if (e_xm(i).ge.x(imin_)) then
              e_imin_ol(n)=i
              exit
           end if
        end do
        ! Finds right of overlap region
        do i=e_imax_a(n),e_imin_a(n),-1
           if (e_xm(i).le.x(imax_+1)) then
              e_imax_ol(n)=i
              exit
           end if
        end do
     end if

     ! Check for overlap in y-direction
     if ( e_ym(e_jmin_a(n)).le.y(jmax_+1) .and. &
          e_ym(e_jmax_a(n)).ge.y(jmin_) ) then ! overlaps
        e2ns_overlap_y=.true.
        ! Finds bottom of overlap region
        do j=e_jmin_a(n),e_jmax_a(n)
           if (e_ym(j).ge.y(jmin_)) then
              e_jmin_ol(n)=j
              exit
           end if
        end do
        ! Finds top of overlap region
        do j=e_jmax_a(n),e_jmin_a(n),-1
           if (e_ym(j).le.y(jmax_+1)) then
              e_jmax_ol(n)=j
              exit
           end if
        end do
     end if

     ! Check for overlap in z-direction
     if ( e_zm(e_kmin_a(n)).le.z(kmax_+1) .and. &
          e_zm(e_kmax_a(n)).ge.z(kmin_) ) then ! overlaps
        e2ns_overlap_z=.true.
        ! Finds back of overlap region
        do k=e_kmin_a(n),e_kmax_a(n)
           if (e_zm(k).ge.z(kmin_)) then
              e_kmin_ol(n)=k
              exit
           end if
        end do
        ! Finds front of overlap region
        do k=e_kmax_a(n),e_kmin_a(n),-1
           if (e_zm(k).le.z(kmax_+1)) then
              e_kmax_ol(n)=k
              exit
           end if
        end do
     end if

     if (e2ns_overlap_x.and.e2ns_overlap_y.and.e2ns_overlap_z) then
        e2ns_overlap(n)=.true.
     else
        e2ns_overlap(n)=.false.
     end if

  end do

  return
end subroutine multiphase_emesh_e2ns_interp_init

! ==================================== !
! Build ns2e comm_list to know         !
! which proc send NS data to this proc !
! ==================================== !
subroutine multiphase_emesh_ns2e_commlist
  use multiphase_emesh
  implicit none
  integer :: n,count,ierr
  integer :: my_ns2e_comm_list_length
  type(comm_list_type), dimension(nproc) :: my_ns2e_comm_list
  integer, dimension(:), allocatable  :: displs

  ! Loop over processors
  count=0
  do n=1,nproc
     ! Check if overlap exits
     if (ns2e_overlap(n)) then
        count=count+1

        my_ns2e_comm_list(count)%send_proc=n
        my_ns2e_comm_list(count)%recv_proc=irank

        my_ns2e_comm_list(count)%imin_ol=imin_ol(n)-1
        my_ns2e_comm_list(count)%imax_ol=imax_ol(n)+1
        my_ns2e_comm_list(count)%jmin_ol=jmin_ol(n)-1
        my_ns2e_comm_list(count)%jmax_ol=jmax_ol(n)+1
        my_ns2e_comm_list(count)%kmin_ol=kmin_ol(n)-1
        my_ns2e_comm_list(count)%kmax_ol=kmax_ol(n)+1
        my_ns2e_comm_list(count)%nx_ol = &
             my_ns2e_comm_list(count)%imax_ol-my_ns2e_comm_list(count)%imin_ol+1
        my_ns2e_comm_list(count)%ny_ol = &
             my_ns2e_comm_list(count)%jmax_ol-my_ns2e_comm_list(count)%jmin_ol+1
        my_ns2e_comm_list(count)%nz_ol = &
             my_ns2e_comm_list(count)%kmax_ol-my_ns2e_comm_list(count)%kmin_ol+1

     end if
     ! Store length of useful comm_list
     my_ns2e_comm_list_length=count
  end do

  ! Broadcast list
  call MPI_TYPE_CONTIGUOUS (11, MPI_INTEGER, MPI_COMMLIST,ierr)
  call MPI_TYPE_COMMIT(MPI_COMMLIST,ierr)

  ! Communicate number useful rows in comm_list
  allocate(ns2e_comm_list_length(nproc))
  call MPI_ALLGATHER(my_ns2e_comm_list_length,1,MPI_INTEGER,ns2e_comm_list_length,1,MPI_INTEGER,comm,ierr)

  ! Communicate useful rows in comm_list
  allocate(ns2e_comm_list(sum(ns2e_comm_list_length)))
  allocate(displs(nproc));
  do n=1,nproc
     displs(n)=sum(ns2e_comm_list_length(1:n-1))
  end do
  call MPI_AllGATHERV(my_ns2e_comm_list(1:my_ns2e_comm_list_length),my_ns2e_comm_list_length,&
       MPI_COMMLIST,ns2e_comm_list,ns2e_comm_list_length,displs,MPI_COMMLIST,comm,ierr)

  ! do n = 1,sum(ns2e_comm_list_length)
  !    if (irank.eq.1) print*,ns2e_comm_list(n)
  ! end do

  return
end subroutine multiphase_emesh_ns2e_commlist

! ================================== !
! Communicate data from NS to e-mesh !
! ================================== !
subroutine multiphase_emesh_ns2e_comm
  use multiphase_emesh
  implicit none
  integer ierr,status(MPI_STATUS_SIZE),request

  integer sizecomm,count,nsi1,nsi2,nsj1,nsj2,nsk1,nsk2,from,to
  integer,dimension(:),allocatable :: e_range
  real(WP),dimension(:,:,:),allocatable :: recvq
  integer ei,ej,ek,nsi,nsj,nsk
  real(WP) b00,b01,b10,b11,c0,c1
  real(WP) xd,yd,zd

  allocate(e_range(1:6))

  do count = 1,sum(ns2e_comm_list_length)
     from  = ns2e_comm_list(count)%send_proc
     to    = ns2e_comm_list(count)%recv_proc
     nsi1  = ns2e_comm_list(count)%imin_ol
     nsi2  = ns2e_comm_list(count)%imax_ol
     nsj1  = ns2e_comm_list(count)%jmin_ol
     nsj2  = ns2e_comm_list(count)%jmax_ol
     nsk1  = ns2e_comm_list(count)%kmin_ol
     nsk2  = ns2e_comm_list(count)%kmax_ol
     sizecomm = (ns2e_comm_list(count)%nx_ol)*(ns2e_comm_list(count)%ny_ol)*(ns2e_comm_list(count)%nz_ol)

     if (irank.eq.from) then
        e_range = [e_imin_ol(to),e_imax_ol(to),e_jmin_ol(to),&
             e_jmax_ol(to),e_kmin_ol(to),e_kmax_ol(to)]
        call MPI_ISEND(e_range,6,mpi_integer,to-1,21,comm,request,ierr)
        call MPI_ISEND(ns_q(nsi1:nsi2,nsj1:nsj2,nsk1:nsk2),sizecomm,mpi_double_precision,to-1,20,comm,request,ierr)
     end if

     if (irank.eq.to) then
        e_range = 0
        allocate(recvq(nsi1:nsi2,nsj1:nsj2,nsk1:nsk2))

        call MPI_RECV(e_range,6,mpi_integer,from-1,21,comm,status,ierr)
        call MPI_RECV(recvq,sizecomm,mpi_double_precision,from-1,20,comm,status,ierr)

        do ek = e_range(5),e_range(6)
           nsk = nsk1
           do
              if (zm(nsk+1).ge.e_zm(ek)) exit
              nsk = nsk + 1
           end do
           do ej = e_range(3),e_range(4)
              nsj = nsj1
              do
                 if (ym(nsj+1).ge.e_ym(ej)) exit
                 nsj = nsj + 1
              end do
              do ei = e_range(1),e_range(2)
                 nsi = nsi1
                 do
                    if (xm(nsi+1).ge.e_xm(ei)) exit
                    nsi = nsi + 1
                 end do

                 ! Trilinear interpolation
                 ! -----------------------
                 xd = (e_xm(ei)-xm(nsi))/(xm(nsi+1)-xm(nsi))
                 yd = (e_ym(ej)-ym(nsj))/(ym(nsj+1)-ym(nsj))
                 zd = (e_zm(ek)-zm(nsk))/(zm(nsk+1)-zm(nsk))
                 ! x-interp
                 b00 = recvq(nsi,nsj  ,nsk  )*(1-xd) + recvq(nsi+1,nsj  ,nsk  )*xd
                 b01 = recvq(nsi,nsj  ,nsk+1)*(1-xd) + recvq(nsi+1,nsj  ,nsk+1)*xd
                 b10 = recvq(nsi,nsj+1,nsk  )*(1-xd) + recvq(nsi+1,nsj+1,nsk  )*xd
                 b11 = recvq(nsi,nsj+1,nsk+1)*(1-xd) + recvq(nsi+1,nsj+1,nsk+1)*xd
                 ! y-interp
                 c0 = b00*(1-yd) + b10*yd
                 c1 = b01*(1-yd) + b11*yd
                 ! z-interp
                 e_q(ei,ej,ek) = c0*(1-zd) + c1*zd

              end do
           end do
        end do
        deallocate(recvq)
     end if

     call MPI_BARRIER(comm,ierr)
  end do

  ! if (irank.eq.2) then
  !    do ej=e_jmax_,e_jmin_,-1
  !       print*,ej,int(e_q(10,ej,3))
  !    end do
  ! end if

  return
end subroutine multiphase_emesh_ns2e_comm

! =================================== !
! Build e2ns comm_list to know        !
! which proc send e-data to this proc !
! =================================== !
subroutine multiphase_emesh_e2ns_commlist
  use multiphase_emesh
  implicit none
  integer :: n,count,ierr
  integer :: my_e2ns_comm_list_length
  type(comm_list_type), dimension(nproc) :: my_e2ns_comm_list
  integer, dimension(:), allocatable  :: displs

  ! Loop over processors
  count=0
  do n=1,nproc
     ! Check if overlap exits
     if (e2ns_overlap(n)) then
        count=count+1

        my_e2ns_comm_list(count)%send_proc=n
        my_e2ns_comm_list(count)%recv_proc=irank
        my_e2ns_comm_list(count)%imin_ol=e_imin_ol(n)-1
        my_e2ns_comm_list(count)%imax_ol=e_imax_ol(n)+1
        my_e2ns_comm_list(count)%jmin_ol=e_jmin_ol(n)-1
        my_e2ns_comm_list(count)%jmax_ol=e_jmax_ol(n)+1
        my_e2ns_comm_list(count)%kmin_ol=e_kmin_ol(n)-1
        my_e2ns_comm_list(count)%kmax_ol=e_kmax_ol(n)+1
        my_e2ns_comm_list(count)%nx_ol = &
             my_e2ns_comm_list(count)%imax_ol-my_e2ns_comm_list(count)%imin_ol+1
        my_e2ns_comm_list(count)%ny_ol = &
             my_e2ns_comm_list(count)%jmax_ol-my_e2ns_comm_list(count)%jmin_ol+1
        my_e2ns_comm_list(count)%nz_ol = &
             my_e2ns_comm_list(count)%kmax_ol-my_e2ns_comm_list(count)%kmin_ol+1
        
     end if
     
     ! Store length of useful comm_list
     my_e2ns_comm_list_length=count
  end do
  
  ! Broadcast list
  call MPI_TYPE_CONTIGUOUS (11, MPI_INTEGER, MPI_COMMLIST,ierr)
  call MPI_TYPE_COMMIT(MPI_COMMLIST,ierr)
  
  ! Communicate number useful rows in comm_list
  allocate(e2ns_comm_list_length(nproc))
  call MPI_ALLGATHER(my_e2ns_comm_list_length,1,MPI_INTEGER,e2ns_comm_list_length,1,MPI_INTEGER,comm,ierr)
  
  ! Communicate useful rows in comm_list
  allocate(e2ns_comm_list(sum(e2ns_comm_list_length)))
  allocate(displs(nproc));
  do n=1,nproc
     displs(n)=sum(e2ns_comm_list_length(1:n-1))
  end do
  call MPI_AllGATHERV(my_e2ns_comm_list(1:my_e2ns_comm_list_length),my_e2ns_comm_list_length,&
       MPI_COMMLIST,e2ns_comm_list,e2ns_comm_list_length,displs,MPI_COMMLIST,comm,ierr)
  
  ! do n = 1,sum(e2ns_comm_list_length)
  !    if (irank.eq.1) print*,e2ns_comm_list(n)
  ! end do

  return
end subroutine multiphase_emesh_e2ns_commlist

! ================================== !
! Communicate data from e to NS-mesh !
! ================================== !
subroutine multiphase_emesh_e2ns_comm
  use multiphase_emesh
  implicit none
  integer ierr,status(MPI_STATUS_SIZE),request

  integer sizecomm,count,ei1,ei2,ej1,ej2,ek1,ek2,from,to
  integer,dimension(:),allocatable :: ns_range
  real(WP),dimension(:,:,:),allocatable :: recvv
  integer nsi,nsj,nsk,ei,ej,ek
  real(WP) b00,b01,b10,b11,c0,c1
  real(WP) xd,yd,zd

  allocate(ns_range(1:6))

  do count = 1,sum(e2ns_comm_list_length)
     from = e2ns_comm_list(count)%send_proc
     to   = e2ns_comm_list(count)%recv_proc
     ei1  = e2ns_comm_list(count)%imin_ol
     ei2  = e2ns_comm_list(count)%imax_ol
     ej1  = e2ns_comm_list(count)%jmin_ol
     ej2  = e2ns_comm_list(count)%jmax_ol
     ek1  = e2ns_comm_list(count)%kmin_ol
     ek2  = e2ns_comm_list(count)%kmax_ol
     sizecomm = (e2ns_comm_list(count)%nx_ol)*(e2ns_comm_list(count)%ny_ol)*(e2ns_comm_list(count)%nz_ol)

     if (irank.eq.from) then
        ns_range = [imin_ol(to),imax_ol(to),jmin_ol(to),&
             jmax_ol(to),kmin_ol(to),kmax_ol(to)]
        call MPI_ISEND(ns_range,6,mpi_integer,to-1,11,comm,request,ierr)
        call MPI_ISEND(e_v(ei1:ei2,ej1:ej2,ek1:ek2),sizecomm,mpi_double_precision,to-1,10,comm,request,ierr)
     end if
     if (irank.eq.to) then
        ns_range = 0
        allocate(recvv(ei1:ei2,ej1:ej2,ek1:ek2))
        call MPI_RECV(ns_range,6,mpi_integer,from-1,11,comm,status,ierr)
        call MPI_RECV(recvv,sizecomm,mpi_double_precision,from-1,10,comm,status,ierr)
        do nsk = ns_range(5),ns_range(6)
           ek = ek1
           do
              if (e_zm(ek+1).ge.zm(nsk)) exit
              ek = ek + 1
           end do
           do nsj = ns_range(3),ns_range(4)
              ej = ej1
              do
                 if (e_ym(ej+1).ge.ym(nsj)) exit
                 ej = ej + 1
              end do
              do nsi = ns_range(1),ns_range(2)
                 ei = ei1
                 do
                    if (e_xm(ei+1).ge.xm(nsi)) exit
                    ei = ei + 1
                 end do

                 ! Trilinear interpolation
                 ! -----------------------
                 xd = (xm(nsi)-e_xm(ei))/(e_xm(ei+1)-e_xm(ei))
                 yd = (ym(nsj)-e_ym(ej))/(e_ym(ej+1)-e_ym(ej))
                 zd = (zm(nsk)-e_zm(ek))/(e_zm(ek+1)-e_zm(ek))
                 ! x-interp
                 b00 = recvv(ei,ej  ,ek  )*(1-xd) + recvv(ei+1,ej  ,ek  )*xd
                 b01 = recvv(ei,ej  ,ek+1)*(1-xd) + recvv(ei+1,ej  ,ek+1)*xd
                 b10 = recvv(ei,ej+1,ek  )*(1-xd) + recvv(ei+1,ej+1,ek  )*xd
                 b11 = recvv(ei,ej+1,ek+1)*(1-xd) + recvv(ei+1,ej+1,ek+1)*xd
                 ! y-interp
                 c0 = b00*(1-yd) + b10*yd
                 c1 = b01*(1-yd) + b11*yd
                 ! z-interp
                 ns_v(nsi,nsj,nsk) = c0*(1-zd) + c1*zd

              end do
           end do
        end do
        deallocate(recvv)
     end if

     call MPI_BARRIER(comm,ierr)
  end do

  return
end subroutine multiphase_emesh_e2ns_comm

! =============================== !
! Solve electric Poisson equation !
! =============================== !
subroutine multiphase_emesh_poisson_solver
  use multiphase_emesh

  call multiphase_emesh_hypre_rhs_transfer
  call multiphase_emesh_hypre_set_ig
  call multiphase_emesh_hypre_solve
  call multiphase_emesh_hypre_sol_transfer

  return
end subroutine multiphase_emesh_poisson_solver

! ========================= !
! Update the HYPRE operator !
! ========================= !
subroutine multiphase_emesh_hypre_update
  use multiphase_emesh
  use metric_velocity_conv
  use masks
  implicit none
  
  integer :: i,j,k,ierr
  integer, dimension(3) :: ind
  real(WP) ddx,ddy,ddz,dnx,dny,dnz
  real(WP) w1,w2

  ! Build the matrix
  do k=e_kmin_,e_kmax_
     do j=e_jmin_,e_jmax_
        do i=e_imin_,e_imax_
           ind(1) = i
           ind(2) = j
           ind(3) = k

           ! Distances in x,y,z
           ddx = (e_xm(i+1)-e_xm(i))/(e_xm(i)-e_xm(i-1))
           dnx = (e_xm(i+1)-e_xm(i))*(e_xm(i)-e_xm(i-1))*(1+ddx)
           ddy = (e_ym(j+1)-e_ym(j))/(e_ym(j)-e_ym(j-1))
           dny = (e_ym(j+1)-e_ym(j))*(e_ym(j)-e_ym(j-1))*(1+ddy)
           ddz = (e_zm(k+1)-e_zm(k))/(e_zm(k)-e_zm(k-1))
           dnz = (e_zm(k+1)-e_zm(k))*(e_zm(k)-e_zm(k-1))*(1+ddz)

           ! Matrix coefficients
           val    =  0.0_WP
           val(1) = -2.0_WP*(1+ddx)/dnx
           val(2) =  2.0_WP*ddx/dnx
           val(3) =  2.0_WP/dnx
           if(e_dim_hypre.gt.1)then
              val(1) = val(1)+(-2.0_WP*(1+ddy)/dny)
              val(4) = 2.0_WP*ddy/dny
              val(5) = 2.0_WP/dny
              if(e_dim_hypre.gt.2)then
                 val(1) = val(1)+(-2.0_WP*(1+ddz)/dnz)
                 val(6) = 2.0_WP*ddz/dnz
                 val(7) = 2.0_WP/dnz
              end if
           end if

           ! ! Dirichlet BCs
           ! if (i.eq.e_imin) then
           !    w1 = (e_x (i)-e_xm(i-1))/(e_xm(i)-e_xm(i-1))
           !    w2 = (e_xm(i)-e_x (i  ))/(e_xm(i)-e_xm(i-1))
           !    val(1) = val(1)-((w1/w2)*val(2))
           !    val(2) = 0.0_WP
           ! end if
           ! if (i.eq.e_imax) then
           !    w1 = (e_x (i+1)-e_xm(i  ))/(e_xm(i+1)-e_xm(i))
           !    w2 = (e_xm(i+1)-e_x (i+1))/(e_xm(i+1)-e_xm(i))
           !    val(1) = val(1)-((w2/w1)*val(3))
           !    val(3) = 0.0_WP
           ! end if
           ! if (e_dim_hypre.gt.1) then
           !    if (j.eq.e_jmin) then
           !       w1 = (e_y (j)-e_ym(j-1))/(e_ym(j)-e_ym(j-1))
           !       w2 = (e_ym(j)-e_y (j  ))/(e_ym(j)-e_ym(j-1))
           !       val(1) = val(1)-((w1/w2)*val(4))
           !       val(4) = 0.0_WP
           !    end if
           !    if (j.eq.e_jmax) then
           !       w1 = (e_y (j+1)-e_ym(j  ))/(e_ym(j+1)-e_ym(j))
           !       w2 = (e_ym(j+1)-e_y (j+1))/(e_ym(j+1)-e_ym(j))
           !       val(1) = val(1)-((w2/w1)*val(5))
           !       val(5) = 0.0_WP
           !    end if
           !       if (e_dim_hypre.gt.2) then
           !          if (k.eq.e_kmin) then
           !             w1 = (e_z (k)-e_zm(k-1))/(e_zm(k)-e_zm(k-1))
           !             w2 = (e_zm(k)-e_z (k  ))/(e_zm(k)-e_zm(k-1))
           !             val(1) = val(1)-((w1/w2)*val(4))
           !             val(4) = 0.0_WP
           !          end if
           !          if (k.eq.e_kmax) then
           !             w1 = (e_z (k+1)-e_zm(k  ))/(e_zm(k+1)-e_zm(k))
           !             w2 = (e_zm(k+1)-e_z (k+1))/(e_zm(k+1)-e_zm(k))
           !             val(1) = val(1)-((w2/w1)*val(5))
           !             val(5) = 0.0_WP
           !          end if
           !       end if
           ! end if

           ! Neumann BCs
           if (i.eq.e_imin) then
              val(1) = val(1)+val(2)
              val(2) = 0.0_WP
           end if
           if (i.eq.e_imax) then
              val(1) = val(1)+val(3)
              val(3) = 0.0_WP
           end if
           if (e_dim_hypre.gt.1) then
              if (j.eq.e_jmin) then
                 val(1) = val(1)+val(4)
                 val(4) = 0.0_WP
              end if
              if (j.eq.e_jmax) then
                 val(1) = val(1)+val(5)
                 val(5) = 0.0_WP
              end if
              if (e_dim_hypre.gt.2) then
                 if (k.eq.e_kmin) then
                    val(1) = val(1)+val(6)
                    val(6) = 0.0_WP
                 end if
                 if (k.eq.e_kmax) then
                    val(1) = val(1)+val(7)
                    val(7) = 0.0_WP
                 end if
              end if
           end if

           ! ! Print values
           ! if (e_dim_hypre.eq.1)&
           !      print*,'MAT VAL',val(2),val(1),val(3)
           ! if (e_dim_hypre.eq.2)&
           !      print*,'MAT VAL',val(4),val(2),val(1),val(3),val(5)
           ! if (e_dim_hypre.eq.3)&
           !      print*,'MAT VAL',val(6),val(4),val(2),val(1),val(3),val(5),val(7)

           call HYPRE_StructMatrixSetValues(e_matrix,ind(1:e_dim_hypre),e_stencil_size,e_sten_ind,val,ierr)
        end do
     end do
  end do

  call HYPRE_StructMatrixAssemble(e_matrix,ierr)
  ! call HYPRE_StructMatrixPrint(e_matrix,1,ierr)
  
  return
end subroutine multiphase_emesh_hypre_update

! ========================= !
! Transfer the RHS to HYPRE !
! ========================= !
subroutine multiphase_emesh_hypre_rhs_transfer
  use multiphase_emesh
  use masks
  use metric_velocity_conv
  implicit none

  integer :: i,j,k,ierr
  integer, dimension(3) :: ind
  real(WP) :: value
  real(WP) ddx,ddy,ddz,dnx,dny,dnz
  real(WP) w1,w2,v2,v3
  real(WP) phi_imin,phi_imax,phi_jmin,phi_jmax,phi_kmin,phi_kmax

  ! Dirichlet BC values
  call parser_read('Potential at imin',phi_imin,0.0_WP)
  call parser_read('Potential at imax',phi_imax,0.0_WP)
  call parser_read('Potential at jmin',phi_jmin,0.0_WP)
  call parser_read('Potential at jmax',phi_jmax,0.0_WP)
  call parser_read('Potential at kmin',phi_kmin,0.0_WP)
  call parser_read('Potential at kmax',phi_kmax,0.0_WP)

  do k = e_kmin_,e_kmax_
     do j = e_jmin_,e_jmax_
        do i = e_imin_,e_imax_

           ! Mesh position
           ind(1) = i
           ind(2) = j
           ind(3) = k

           ! RHS value
           value = 0.0_WP
           
           ! ! Dirichlet BCs
           ! if ( i.eq.e_imin .or. i.eq.e_imax .or.&
           !      j.eq.e_jmin .or. j.eq.e_jmax .or.&
           !      k.eq.e_kmin .or. k.eq.e_kmax) then
           !    if(i.eq.e_imin)then
           !       ddx = (e_xm(i+1)-e_xm(i))/(e_xm(i)-e_xm(i-1))
           !       dnx = (e_xm(i+1)-e_xm(i))*(e_xm(i)-e_xm(i-1))*(1+ddx)
           !       w2  = (e_xm(i)-e_x (i  ))/(e_xm(i)-e_xm(i-1))
           !       w1  = (e_x (i)-e_xm(i-1))/(e_xm(i)-e_xm(i-1))
           !       v2  = 2.0_WP*ddx/dnx
           !       value = value-(phi_imin*v2/w2)
           !    end if
           !    if(i.eq.e_imax)then
           !       ddx = (e_xm(i+1)-e_xm(i))/(e_xm(i)-e_xm(i-1))
           !       dnx = (e_xm(i+1)-e_xm(i))*(e_xm(i)-e_xm(i-1))*(1+ddx)
           !       w2  = (e_xm(i+1)-e_x (i+1))/(e_xm(i+1)-e_xm(i))
           !       w1  = (e_x (i+1)-e_xm(i  ))/(e_xm(i+1)-e_xm(i))
           !       v3  = 2.0_WP/dnx
           !       value = value-(phi_imax*v3/w1)
           !    end if
           !    if (e_dim_hypre.gt.1) then
           !       if(j.eq.e_jmin)then
           !          ddy = (e_ym(j+1)-e_ym(j))/(e_ym(j)-e_ym(j-1))
           !          dny = (e_ym(j+1)-e_ym(j))*(e_ym(j)-e_ym(j-1))*(1+ddy)
           !          w2  = (e_ym(j)-e_y (j  ))/(e_ym(j)-e_ym(j-1))
           !          w1  = (e_y (j)-e_ym(j-1))/(e_ym(j)-e_ym(j-1))
           !          v2  = 2.0_WP*ddy/dny
           !          value = value-(phi_jmin*v2/w2)
           !       end if
           !       if(j.eq.e_jmax)then
           !          ddy = (e_ym(j+1)-e_ym(j))/(e_ym(j)-e_ym(j-1))
           !          dny = (e_ym(j+1)-e_ym(j))*(e_ym(j)-e_ym(j-1))*(1+ddy)
           !          w2  = (e_ym(j+1)-e_y (j+1))/(e_ym(j+1)-e_ym(j))
           !          w1  = (e_y (j+1)-e_ym(j  ))/(e_ym(j+1)-e_ym(j))
           !          v3  = 2.0_WP/dny
           !          value = value-(phi_jmax*v3/w1)
           !       end if
           !       if (e_dim_hypre.gt.2) then
           !          if(k.eq.e_kmin)then
           !             ddz = (e_zm(k+1)-e_zm(k))/(e_zm(k)-e_zm(k-1))
           !             dnz = (e_zm(k+1)-e_zm(k))*(e_zm(k)-e_zm(k-1))*(1+ddz)
           !             w2  = (e_zm(k)-e_z (k  ))/(e_zm(k)-e_zm(k-1))
           !             w1  = (e_z (k)-e_zm(k-1))/(e_zm(k)-e_zm(k-1))
           !             v2  = 2.0_WP*ddz/dnz
           !             value = value-(phi_kmin*v2/w2)
           !          end if
           !          if(k.eq.e_kmax)then
           !             ddz = (e_zm(k+1)-e_zm(k))/(e_zm(k)-e_zm(k-1))
           !             dnz = (e_zm(k+1)-e_zm(k))*(e_zm(k)-e_zm(k-1))*(1+ddz)
           !             w2  = (e_zm(k+1)-e_z (k+1))/(e_zm(k+1)-e_zm(k))
           !             w1  = (e_z (k+1)-e_zm(k  ))/(e_zm(k+1)-e_zm(k))
           !             v3  = 2.0_WP/dnz
           !             value = value-(phi_kmax*v3/w1)
           !          end if
           !       end if
           !    end if
           ! end if

           ! ! Print values
           ! print*,'RHS VAL',i,j,k,e_xm(i),value

           ! Set the values
           call HYPRE_StructVectorSetValues(e_rhs,ind(1:e_dim_hypre),value,ierr)

        end do
     end do
  end do

  call HYPRE_StructVectorAssemble(e_rhs,ierr)
  ! call HYPRE_StructVectorPrint(e_rhs,0,ierr)

  return
end subroutine multiphase_emesh_hypre_rhs_transfer

! ================================ !
! Transfer the solution from HYPRE !
! ================================ !
subroutine multiphase_emesh_hypre_sol_transfer
  use multiphase_emesh
  use masks
  use metric_velocity_conv
  implicit none

  integer :: i,j,k,ierr
  integer, dimension(3) :: ind
  real(WP) :: value

  do k=e_kmin_,e_kmax_
     do j=e_jmin_,e_jmax_
        do i=e_imin_,e_imax_
           ind(1) = i
           ind(2) = j
           ind(3) = k
           call HYPRE_StructVectorGetValues(e_sol,ind(1:e_dim_hypre),value,ierr)
           e_v(i,j,k) = value
           ! print*,i,j,k,value
        end do
     end do
  end do

  ! if (irank.eq.1) then
  !    print*,'SOLUTION'
  !    print*,'--------'
  !    print*,e_ym(e_jmin_:e_jmax_)
  !    do k=e_kmax_,e_kmin_,-1
  !       print*,'k = ',k
  !       do j=e_jmax_,e_jmin_,-1
  !          print*,e_v(e_imin_:e_imax_,j,k)
  !       end do
  !    end do
  ! end if

  return
end subroutine multiphase_emesh_hypre_sol_transfer

! ========================================== !
! Initialization of the HYPRE preconditioner !
! ========================================== !
subroutine multiphase_emesh_hypre_precond_init
  use multiphase_emesh
  implicit none

  e_precond_id = -1

  return
end subroutine multiphase_emesh_hypre_precond_init

! ================================== !
! Initialization of the HYPRE solver !
! ================================== !
subroutine multiphase_emesh_hypre_solver_init
  use multiphase_emesh
  implicit none

  integer  :: ierr
  integer  :: log_level

  ! Update the operator
  call multiphase_emesh_hypre_update

  ! Setup the preconditioner
  call multiphase_emesh_hypre_precond_init

  ! Setup the solver
  log_level = 1
  call HYPRE_StructBICGSTABCreate(comm,e_solver,ierr)
  call HYPRE_StructBICGSTABSetMaxIter(e_solver,e_max_iter,ierr)
  call HYPRE_StructBICGSTABSetTol(e_solver,e_cvg,ierr)
  call HYPRE_StructBICGSTABSetLogging(e_solver,log_level,ierr)
  if (e_precond_id.ge.0) call HYPRE_StructBICGSTABSetPrecond(e_solver,e_precond_id,e_precond,ierr)
  call HYPRE_StructBICGSTABSetup(e_solver,e_matrix,e_rhs,e_sol,ierr)

  return
end subroutine multiphase_emesh_hypre_solver_init

! ==================================== !
! Transfer the initial guess to HYPRE !
! ==================================== !
subroutine multiphase_emesh_hypre_set_ig
  use multiphase_emesh
  use masks
  use metric_velocity_conv
  implicit none

  integer :: i,j,k,ierr
  integer, dimension(3) :: ind
  real(WP) :: value

  do k=e_kmin_,e_kmax_
     do j=e_jmin_,e_jmax_
        do i=e_imin_,e_imax_
           ind(1) = i
           ind(2) = j
           ind(3) = k
           value  = 0.0_WP
           call HYPRE_StructVectorSetValues(e_sol,ind(1:e_dim_hypre),value,ierr)
        end do
     end do
  end do
  call HYPRE_StructVectorAssemble(e_sol,ierr)

  return
end subroutine multiphase_emesh_hypre_set_ig

! ============================ !
! Solve the problem with HYPRE !
! ============================ !
subroutine multiphase_emesh_hypre_solve
  use multiphase_emesh
  use time_info
  implicit none

  integer :: ierr
  integer :: e_it
  real(WP) :: e_max_res

  ! Prepare the solver
  call HYPRE_StructBICGSTABSolve(e_solver,e_matrix,e_rhs,e_sol,ierr)
  call HYPRE_StructBICGSTABGetNumItera(e_solver,e_it,ierr)
  call HYPRE_StructBICGSTABGetFinalRel(e_solver,e_max_res,ierr)
  ! print *,'Number of iterations=',e_it
  ! print *,'Max Residual        =',e_max_res

  return
end subroutine multiphase_emesh_hypre_solve

! ================================= !
!  Initiating HYPRE Poisson Solver  !
! ================================= !
subroutine multiphase_emesh_hypre_init
  use multiphase_emesh
  implicit none

  integer ierr
  integer :: count,i
  integer :: offsets(7,3)
  integer, dimension(6) :: e_matrix_num_ghost
  integer, dimension(3) :: offset

  ! Determine Hypre dimensions
  e_dim_hypre = 3
  if (e_nz.eq.1) e_dim_hypre = 2
  if (e_ny.eq.1) e_dim_hypre = 2
  if (e_ny.eq.1.and.e_nz.eq.1) e_dim_hypre = 1

  ! Initialize Hypre - Struct
  call HYPRE_StructGridCreate(comm,e_dim_hypre,e_grid,ierr)
  e_lower = 0; e_upper = 0
  e_lower(1) = e_imin_
  e_upper(1) = e_imax_
  if (e_dim_hypre.gt.1) then
     e_lower(2) = e_jmin_
     e_upper(2) = e_jmax_
     if (e_dim_hypre.gt.2) then
        e_lower(3) = e_kmin_
        e_upper(3) = e_kmax_
     end if
  end if
  call HYPRE_StructGridSetExtents(e_grid,e_lower(1:e_dim_hypre),e_upper(1:e_dim_hypre),ierr)
  if (periodicity(1).eq.1) e_periodicity_hypre(1) = e_nx
  if (periodicity(2).eq.1) e_periodicity_hypre(2) = e_ny
  if (periodicity(3).eq.1) e_periodicity_hypre(3) = e_nz
  ! MIGHT NEED TO CHANGE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  e_periodicity_hypre = 0
  call HYPRE_StructGridSetPeriodic(e_grid,e_periodicity_hypre(1:e_dim_hypre),ierr)
  call HYPRE_StructGridAssemble(e_grid,ierr)

  ! Build the stencil
  e_stencil_size = 2*e_dim_hypre+1
  offsets(1,:) = [0,0,0]
  offsets(2,:) = [-1,0,0]
  offsets(3,:) = [1,0,0]
  if (e_dim_hypre.gt.1) then
     offsets(4,:) = [0,-1,0]
     offsets(5,:) = [0,1,0]
     if (e_dim_hypre.gt.2) then
        offsets(6,:) = [0,0,1]
        offsets(7,:) = [0,0,1]
     end if
  end if
  call HYPRE_StructStencilCreate(e_dim_hypre,e_stencil_size,e_stencil,ierr)
  do count=1,e_stencil_size
     offset = offsets(count,:)
     call HYPRE_StructStencilSetElement(e_stencil,count-1,offset,ierr)
  end do

  ! Create stencil index
  allocate(e_sten_ind(e_stencil_size))
  allocate(val(e_stencil_size))
  do count=1,e_stencil_size
     e_sten_ind(count) = count-1
  end do

  ! Build the matrix
  call HYPRE_StructMatrixCreate(comm,e_grid,e_stencil,e_matrix,ierr)
  e_matrix_num_ghost = 0
  call HYPRE_StructMatrixSetNumGhost(e_matrix,e_matrix_num_ghost(1:2*e_dim_hypre),ierr)
  call HYPRE_StructMatrixInitialize(e_matrix,ierr)

  ! Prepare RHS
  call HYPRE_StructVectorCreate(comm,e_grid,e_rhs,ierr)
  call HYPRE_StructVectorInitialize(e_rhs,ierr)
  call HYPRE_StructVectorAssemble(e_rhs,ierr)

  ! Create solution vector
  call HYPRE_StructVectorCreate(comm,e_grid,e_sol,ierr)
  call HYPRE_StructVectorInitialize(e_sol,ierr)
  call HYPRE_StructVectorAssemble(e_sol,ierr)

  return
end subroutine multiphase_emesh_hypre_init
