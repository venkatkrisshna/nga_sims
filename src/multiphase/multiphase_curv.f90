module multiphase_curv
  use multiphase
  use lagrange_geom
  use multiphase_tools

  integer, parameter :: npnorm=3
  real(WP), dimension(3,npnorm) :: pnorm
  data pnorm(:,1) /  1,  0,  0 /
  data pnorm(:,2) /  0,  1,  0 /
  data pnorm(:,3) /  0,  0,  1 /

contains

  ! Routine to check if column has monotonicily increasing/decreasing VOF values
  subroutine monotonicity_check(dir,searchDir,i,j,k,monotonic,ind_full,ind_empty) 
    implicit none
    integer, intent(in)  :: dir,searchDir,i,j,k
    integer, intent(out) :: ind_full,ind_empty
    logical, intent(out) :: monotonic
    integer :: l
    integer, dimension(3) :: ind
    logical :: foundFull,foundEmpty

    ! Initialize
    monotonic  =.false.
    foundFull =.false.
    foundEmpty=.false.
    ind_full =-(3+1)*searchDir
    ind_empty=+(3+1)*searchDir

    if (VOFavg(i,j,k).gt.VOFhi) then ! Full cell search for empty cell
       do l=1,3+1
          ind=(/i,j,k/)
          ind(dir)=ind(dir)+l*searchDir
          if (VOFavg(ind(1),ind(2),ind(3)).lt.VOFlo) then ! Found empty cell
             ind_full=0; ind_empty=+l*searchDir
             monotonic=.true.
             return
          end if
       end do
    else if (VOFavg(i,j,k).lt.VOFlo) then ! Empty cell search for full cell
       do l=1,3+1
          ind=(/i,j,k/)
          ind(dir)=ind(dir)-l*searchDir
          if (VOFavg(ind(1),ind(2),ind(3)).gt.VOFhi) then ! Found full cell
             ind_empty=0; ind_full=-l*searchDir
             monotonic=.true.
             return
          end if
       end do
    else ! Interface cell search for full and empty cells
       ! Search for empty
       do l=1,3+1
          ind=(/i,j,k/)
          ind(dir)=ind(dir)+l*searchDir
          if (VOFavg(ind(1),ind(2),ind(3)).lt.VOFlo) then ! Found empty
             foundEmpty=.true.
             ind_empty=+l*searchDir
             exit
          end if
       end do
       ! Search for full
       do l=1,3+1
          ind=(/i,j,k/)
          ind(dir)=ind(dir)-l*searchDir
          if (VOFavg(ind(1),ind(2),ind(3)).gt.VOFhi) then ! Found full
             foundFull=.true.
             ind_full=-l*searchDir
             exit
          end if
       end do
       ! Check if found full and empty
       if (foundFull.and.foundEmpty) then
          monotonic=.true.
          return
       end if
    end if
       
    return 
  end subroutine monotonicity_check

  ! Routines to check if height is within the provided cell
  subroutine height_check_x(height_dir,i,j,k,ind_liq,ind_gas,contains_height)
    implicit none
    integer, intent(in)  :: i,j,k,ind_liq,ind_gas,height_dir
    logical, intent(out) :: contains_height
    integer :: n
    real(WP) :: height
    ! Compute height in physical reference frame
    height=x(i+ind_liq+int(0.5_WP*(1.0_WP-real(height_dir,WP))))
    do n=ind_liq,ind_gas,height_dir
       height=height+VOFavg(i+n,j,k)*dx(i+n)*real(height_dir,WP)
    end do
    ! Check height 
    contains_height=.false.
    if (height.ge.x(i).and.height.le.x(i+1)) &
         contains_height=.true.
    return
  end subroutine height_check_x

  subroutine height_check_y(height_dir,i,j,k,ind_liq,ind_gas,contains_height)
    implicit none
    integer, intent(in)  :: i,j,k,ind_liq,ind_gas,height_dir
    logical, intent(out) :: contains_height
    integer :: n
    real(WP) :: height
    ! Compute height in physical reference frame
    height=y(j+ind_liq+int(0.5_WP*(1.0_WP-real(height_dir,WP))))
    do n=ind_liq,ind_gas,height_dir
       height=height+VOFavg(i,j+n,k)*dy(j+n)*real(height_dir,WP)
    end do
    ! Check height 
    contains_height=.false.
    if (height.ge.y(j).and.height.le.y(j+1)) &
         contains_height=.true.
    return
  end subroutine height_check_y

  subroutine height_check_z(height_dir,i,j,k,ind_liq,ind_gas,contains_height)
    implicit none
    integer, intent(in)  :: i,j,k,ind_liq,ind_gas,height_dir
    logical, intent(out) :: contains_height
    integer :: n
    real(WP) :: height
    ! Compute height in physical reference frame
    height=z(k+ind_liq+int(0.5_WP*(1.0_WP-real(height_dir,WP))))
    do n=ind_liq,ind_gas,height_dir
       height=height+VOFavg(i,j,k+n)*dz*real(height_dir,WP)
    end do
    ! Check height 
    contains_height=.false.
    if (height.ge.z(k).and.height.le.z(k+1)) &
         contains_height=.true.
    return
  end subroutine height_check_z

end module multiphase_curv
! ====================================== !
! Routine to compute interface curvature !
! ====================================== !
subroutine multiphase_curv_calc
  use multiphase_curv
  use multiphase_band
  use infnan
  use memory
  implicit none

  integer :: i,j,k,l,m,n
  logical :: is_done
  real(WP) :: my_liq
  real(WP), dimension(3)     :: nv,t1,t2,dx_rot,Xo
  real(WP), dimension(3,3)   :: Rot,iRot
  real(WP), dimension(3,-1:1,-1:1)   :: c
  real(WP), dimension(-1:1,-1:1)     :: height
  real(WP) :: Ht1,Ht2,Ht1t1,Ht1t2,Ht2t2
  real(WP), dimension(npnorm) :: dot
  logical :: monotonic,contains_height
  integer :: ind_liq,ind_gas,height_dir
  ! Column parameters
  real(WP), parameter :: col_width =0.75_WP 
  real(WP), parameter :: col_space =0.75_WP
  real(WP), parameter :: col_height=2.25_WP


  ! Zero curvature 
  curv = 0.0_WP
  hascurv=0

  ! CurvFlag 
  !  0 = curvature not computed
  !  1 = curvature computed using standard height function
  !  2 = curvature computed using mesh-decoupled height function
  !  3 = cell skipped 
  curvFlag = 0

  ! Loop over domain
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        cells1 : do i=imin_,imax_ 
           if ( VOFavg(i,j,k).le.0.5_WP.and.maxval((/VOFavg(i-1,j,k),VOFavg(i,j-1,k),VOFavg(i,j,k-1)/)).gt.0.5_WP .or. &
                VOFavg(i,j,k).le.0.5_WP.and.maxval((/VOFavg(i+1,j,k),VOFavg(i,j+1,k),VOFavg(i,j,k+1)/)).gt.0.5_WP .or. &
                VOFavg(i,j,k).gt.0.5_WP.and.minval((/VOFavg(i-1,j,k),VOFavg(i,j-1,k),VOFavg(i,j,k-1)/)).lt.0.5_WP .or. &
                VOFavg(i,j,k).gt.0.5_WP.and.minval((/VOFavg(i+1,j,k),VOFavg(i,j+1,k),VOFavg(i,j,k+1)/)).lt.0.5_WP ) then
              ! Continue computing curvature
           else
              cycle cells1
           end if
           
           ! Compute average normal 
           nv(1)=sum(normx(:,i-1:i+1,j-1:j+1,k-1:k+1))
           nv(2)=sum(normy(:,i-1:i+1,j-1:j+1,k-1:k+1))
           nv(3)=sum(normz(:,i-1:i+1,j-1:j+1,k-1:k+1))
           if (sqrt(sum(nv(:)**2)).lt.1e-10) cycle
           nv=nv/sqrt(sum(nv(:)**2))

           ! ================================ !
           ! MESH-DECOUPLED METHOD EVERYWHERE
           ! curv(i,j,k)=mesh_decoupled(i,j,k)
           ! curvFlag(i,j,k)=1
           ! hascurv(i,j,k)=1
           ! cycle cells1
           ! ================================ !

           ! Compute dot product with pseudo-normals
           do n=1,npnorm
              dot(n)=abs(dot_product(nv,pnorm(:,n)))
           end do

           select case(maxloc(dot,1))
           case (1)
              ! Heights along x-coordinate
              ! =================================== !
              ! STANDARD HEIGHT FUNCTION EVERYWHERE
              ! (comment out below)
              ! do l=-1,1
              !    do m=-1,1
              !       height(l,m)=sum(VOFavg(i-3:i+3,j+l,k+m)*dx(i-3:i+3))
              !    end do
              ! end do
              ! dx_rot(2)=dy(j)
              ! dx_rot(3)=dz 
              ! =================================== !
              height=0.0_WP
              height_dir=int(sign(1.0_WP,nv(1)))
              call monotonicity_check(1,height_dir,i,j,k,monotonic,ind_liq,ind_gas)
              if (monotonic) then
                 call height_check_x(height_dir,i,j,k,ind_liq,ind_gas,contains_height)
                 if (contains_height) then
                    ! Continue working on computing height
                    xcolumns : do l=-1,1
                       do m=-1,1
                          ! Check if this column is well defined
                          call monotonicity_check(1,height_dir,i,j+l,k+m,monotonic,ind_liq,ind_gas)
                          if (.not.monotonic) then
                             ! Use mesh-decoupled method
                             curv(i,j,k)=mesh_decoupled(i,j,k)
                             hascurv(i,j,k)=1
                             curvFlag(i,j,k)=2
                             cycle cells1
                          end if
                          ! Compute this height for use in the standard method
                          height(l,m)=sum(dx(i-3*height_dir:i+ind_liq:height_dir))
                          do n=ind_liq,ind_gas,height_dir
                             height(l,m)=height(l,m)+VOFavg(i+n,j+l,k+m)*dx(i+n)
                          end do
                       end do
                    end do xcolumns
                    dx_rot(2)=dy(j)
                    dx_rot(3)=dz   
                 else ! Flag cell as skipped
                    curvFlag(i,j,k)=3
                    cycle cells1
                 end if
              else ! Flag cell as skipped
                 curvFlag(i,j,k)=3
                 cycle cells1
              end if

           case (2)
              ! Heights along y-coordinate
              ! =================================== !
              ! STANDARD HEIGHT FUNCTION EVERYWHERE
              ! (comment out below)
              ! do l=-1,1
              !    do m=-1,1
              !       height(l,m)=sum(VOFavg(i+l,j-3:j+3,k+m)*dy(j-3:j+3))
              !    end do
              ! end do
              ! dx_rot(2)=dx(i)
              ! dx_rot(3)=dz 
              ! =================================== !
              height=0.0_WP
              height_dir=int(sign(1.0_WP,nv(2)))
              call monotonicity_check(2,height_dir,i,j,k,monotonic,ind_liq,ind_gas)
              if (monotonic) then
                 call height_check_y(height_dir,i,j,k,ind_liq,ind_gas,contains_height)
                 if (contains_height) then
                    ! Continue working on computing height
                    ycolumns : do l=-1,1
                       do m=-1,1
                          ! Check if this column is well defined
                          call monotonicity_check(2,height_dir,i+l,j,k+m,monotonic,ind_liq,ind_gas)
                          if (.not.monotonic) then
                             ! Use mesh-decoupled method
                             curv(i,j,k)=mesh_decoupled(i,j,k)
                             hascurv(i,j,k)=1
                             curvFlag(i,j,k)=2
                             cycle cells1
                          end if
                          ! Compute this height for use in the standard method
                          height(l,m)=sum(dy(j-3*height_dir:j+ind_liq:height_dir))
                          do n=ind_liq,ind_gas,height_dir
                             height(l,m)=height(l,m)+VOFavg(i+l,j+n,k+m)*dy(j+n)
                          end do
                       end do
                    end do ycolumns
                    dx_rot(2)=dx(i)
                    dx_rot(3)=dz   
                 else ! Flag cell as skipped
                    curvFlag(i,j,k)=3
                    cycle cells1
                 end if
              else ! Flag cell as skipped
                 curvFlag(i,j,k)=3
                 cycle cells1
              end if

           case (3)
              ! Heights along z-coordinate
              ! =================================== !
              ! STANDARD HEIGHT FUNCTION EVERYWHERE
              ! (comment out below)
              ! do l=-1,1
              !    do m=-1,1
              !       height(l,m)=sum(VOFavg(i+l,j+m,k-3:k+3)*dz)
              !    end do
              ! end do
              ! dx_rot(2)=dx(i)
              ! dx_rot(3)=dy(j) 
              ! =================================== !
              height=0.0_WP
              height_dir=int(sign(1.0_WP,nv(3)))
              call monotonicity_check(3,height_dir,i,j,k,monotonic,ind_liq,ind_gas)
              if (monotonic) then
                 call height_check_z(height_dir,i,j,k,ind_liq,ind_gas,contains_height)
                 if (contains_height) then
                    ! Continue working on computing height
                    zcolumns : do l=-1,1
                       do m=-1,1
                          ! Check if this column is well defined
                          call monotonicity_check(3,height_dir,i+l,j+m,k,monotonic,ind_liq,ind_gas)
                          if (.not.monotonic) then
                             ! Use mesh-decoupled method
                             curv(i,j,k)=mesh_decoupled(i,j,k)
                             hascurv(i,j,k)=1
                             curvFlag(i,j,k)=2
                             cycle cells1
                          end if
                          ! Compute this height for use in the standard method
                          height(l,m)=dz*real(3-abs(ind_liq),WP)
                          do n=ind_liq,ind_gas,height_dir
                             height(l,m)=height(l,m)+VOFavg(i+l,j+m,k+n)*dz
                          end do
                       end do
                    end do zcolumns
                    dx_rot(2)=dx(i)
                    dx_rot(3)=dy(j)   
                 else ! Flag cell as skipped
                    curvFlag(i,j,k)=3
                    cycle cells1
                 end if
              else ! Flag cell as skipped
                 curvFlag(i,j,k)=3
                 cycle cells1
              end if
           end select

           ! Compute curvature using finite differences
           Ht1=(height(+1,0)-height(-1,0))/(2.0_WP*dx_rot(2))
           Ht2=(height(0,+1)-height(0,-1))/(2.0_WP*dx_rot(3))
           Ht1t1=(height(+1,0)-2.0_WP*height(0,0)+height(-1,0))/(dx_rot(2))**2
           Ht2t2=(height(0,+1)-2.0_WP*height(0,0)+height(0,-1))/(dx_rot(3))**2
           Ht1t2=(height(+1,+1)-height(+1,-1)-height(-1,+1)+height(-1,-1))/(4.0_WP*dx_rot(2)*dx_rot(3))
           curv(i,j,k) = -(Ht1t1 + Ht2t2 + Ht1t1*Ht2**2 + Ht2t2*Ht1**2 - 2.0_WP*Ht1t2*Ht1*Ht2) &
                / (1.0_WP + Ht1**2 + Ht2**2)**(1.5_WP)
           hascurv(i,j,k)=1

           ! Flag cell as computed using standard height function method
           curvFlag(i,j,k)=1

        end do cells1
     end do
  end do
        

  ! Loop over bands and check skipped cells
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        cells2 : do i=imin_,imax_ 
           if ( VOFavg(i,j,k).le.0.5_WP.and.maxval((/VOFavg(i-1,j,k),VOFavg(i,j-1,k),VOFavg(i,j,k-1)/)).gt.0.5_WP .or. &
                VOFavg(i,j,k).le.0.5_WP.and.maxval((/VOFavg(i+1,j,k),VOFavg(i,j+1,k),VOFavg(i,j,k+1)/)).gt.0.5_WP .or. &
                VOFavg(i,j,k).gt.0.5_WP.and.minval((/VOFavg(i-1,j,k),VOFavg(i,j-1,k),VOFavg(i,j,k-1)/)).lt.0.5_WP .or. &
                VOFavg(i,j,k).gt.0.5_WP.and.minval((/VOFavg(i+1,j,k),VOFavg(i,j+1,k),VOFavg(i,j,k+1)/)).lt.0.5_WP ) then
              ! Continue computing curvature
           else
              cycle cells2
           end if

           if (curvFlag(i,j,k).eq.3) then ! Previously skipped cell

              ! Compute average normal 
              nv(1)=sum(normx(:,i-1:i+1,j-1:j+1,k-1:k+1))
              nv(2)=sum(normy(:,i-1:i+1,j-1:j+1,k-1:k+1))
              nv(3)=sum(normz(:,i-1:i+1,j-1:j+1,k-1:k+1))
              if (sqrt(sum(nv(:)**2)).lt.1e-10) cycle
              nv=nv/sqrt(sum(nv(:)**2))

              ! Compute dot product with pseudo-normals
              do n=1,npnorm
                 dot(n)=abs(dot_product(nv,pnorm(:,n)))
              end do

              select case(maxloc(dot,1))
              case (1)
                 if (curvFlag(i+1,j,k).eq.1) then
                    curv(i,j,k)=curv(i+1,j,k)
                    hascurv(i,j,k)=1
                    curvFlag(i,j,k)=1
                 else if (curvFlag(i-1,j,k).eq.1) then
                    curv(i,j,k)=curv(i-1,j,k)
                    hascurv(i,j,k)=1
                    curvFlag(i,j,k)=1
                 else 
                    ! Use mesh-decoupled height function
                    curv(i,j,k)=mesh_decoupled(i,j,k)
                    hascurv(i,j,k)=1
                    curvFlag(i,j,k)=2
                 end if
              case (2)
                 if (curvFlag(i,j+1,k).eq.1) then
                    curv(i,j,k)=curv(i,j+1,k)
                    hascurv(i,j,k)=1
                    curvFlag(i,j,k)=1
                 else if (curvFlag(i,j-1,k).eq.1) then
                    curv(i,j,k)=curv(i,j-1,k)
                    hascurv(i,j,k)=1
                    curvFlag(i,j,k)=1
                 else 
                    ! Use mesh-decoupled height function
                    curv(i,j,k)=mesh_decoupled(i,j,k)
                    hascurv(i,j,k)=1
                    curvFlag(i,j,k)=2
                 end if
              case (3)
                 if (curvFlag(i,j,k+1).eq.1) then
                    curv(i,j,k)=curv(i,j,k+1)
                    hascurv(i,j,k)=1
                    curvFlag(i,j,k)=1
                 else if (curvFlag(i,j,k-1).eq.1) then
                    curv(i,j,k)=curv(i,j,k-1)
                    hascurv(i,j,k)=1
                    curvFlag(i,j,k)=1
                 else 
                    ! Use mesh-decoupled height function
                    curv(i,j,k)=mesh_decoupled(i,j,k)
                    hascurv(i,j,k)=1
                    curvFlag(i,j,k)=2
                 end if

              end select
           end if
        end do cells2
     end do
  end do
  
 

  ! Clip the curvature
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_  
           curv(i,j,k)=max(min(curv (i,j,k),1.0_WP/meshsize(i,j,k)),-1.0_WP/meshsize(i,j,k))
        end do
     end do
  end do

  ! Communicate
  call communication_border(curv)

  return
  
contains

  ! ============================================== !
  ! Compute curvature using mesh-decoupled method  !
  ! ============================================== !
  function mesh_decoupled(i,j,k) result(curv)
    implicit none
    integer, intent(in) :: i,j,k
    real(WP) :: curv
    real(WP), dimension(-1:1,-1:1) :: height
    real(WP) :: Ht1,Ht2,Ht1t1,Ht1t2,Ht2t2

    ! Form rotated coordinate system nv,t1,t2
    is_done=.false.
    n=0
    do while(.not.is_done)
       n=n+1
       ! Form temp t2 vector
       select case(n)
       case(1); t2=0.0_WP; t2(3)=-1.0_WP
       case(2); t2=0.0_WP; t2(2)=-1.0_WP
       case(3); t2=0.0_WP; t2(1)=-1.0_WP
       end select
       ! Make sure t2 is not the same as n
       if (abs(dot_product(nv,t2)).gt.0.6) cycle
       ! Form real t1 & t2
       t1=normalize(cross_product(nv,t2))
       t2=normalize(cross_product(nv,t1))
       is_done=.true.
    end do

    ! Construct rotation matrices: x'=iRot*x, x=Rot*x'
    Rot(:,1)=nv; Rot(:,2)=t1; Rot(:,3)=t2
    iRot=transpose(Rot)

    ! Cell center and size in rotated coordinate system
    Xo=matmul(iRot,(/xm(i),ym(j),zm(k)/)); 
    dx_rot(:)=sqrt( (iRot(:,1)*dx(i))**2 + (iRot(:,2)*dy(j))**2 + (iRot(:,3)*dz)**2 )

    ! Compute liquid heights in columns
    do l=-1,1
       do m=-1,1
          ! Column center
          c(1,l,m)=Xo(1)
          c(2,l,m)=Xo(2)+real(l,WP)*col_space*dx_rot(2)
          c(3,l,m)=Xo(3)+real(m,WP)*col_space*dx_rot(3)
          ! Liquid within column
          my_liq=multiphase_cut_column(c(:,l,m),i,j,k)
          ! Height
          height(l,m)=my_liq/safe_epsilon(col_width*dx_rot(2)*col_width*dx_rot(3))
       end do
    end do

    ! Finite difference opperators on heights
    Ht1=(height(+1,0)-height(-1,0))/(2.0_WP*col_space*dx_rot(2))
    Ht2=(height(0,+1)-height(0,-1))/(2.0_WP*col_space*dx_rot(3))
    Ht1t1=(height(+1,0)-2.0_WP*height(0,0)+height(-1,0))/(col_space*dx_rot(2))**2
    Ht2t2=(height(0,+1)-2.0_WP*height(0,0)+height(0,-1))/(col_space*dx_rot(3))**2
    Ht1t2=(height(+1,+1)-height(+1,-1)-height(-1,+1)+height(-1,-1))/(4.0_WP*col_space**2*dx_rot(2)*dx_rot(3))
    
    ! Curvature
    curv = -(Ht1t1 + Ht2t2 + Ht1t1*Ht2**2 + Ht2t2*Ht1**2 - 2.0_WP*Ht1t2*Ht1*Ht2) &
         / (1.0_WP + Ht1**2 + Ht2**2)**(1.5_WP)

    return
  end function mesh_decoupled
  
  ! ======================================== !
  ! Returns amount of liquid within a column !
  ! ======================================== !
  function multiphase_cut_column(col_cen,i,j,k) result(liq_vol)
    use multiphase_curv
    use multiphase_fluxes
    implicit none

    ! I/O
    real(WP), dimension(3), intent(in) :: col_cen
    integer, intent(in) :: i,j,k
    real(WP) :: liq_vol
    ! Working variables
    real(WP), dimension(3,8) :: col_verts
    integer :: ntet,n
    integer,  dimension(3,8) :: col_inds
    real(WP), dimension(3,4,5) :: verts
    integer,  dimension(3,4,5) :: inds
    real(WP), dimension(6+nscalar) :: fluxes

    ! Vertices of column in rotated coordinate system
    col_verts(:,1) = col_cen + (/ -0.5_WP*col_height*dx_rot(1), -0.5_WP*col_width*dx_rot(2), -0.5_WP*col_width*dx_rot(3) /)
    col_verts(:,2) = col_cen + (/ +0.5_WP*col_height*dx_rot(1), -0.5_WP*col_width*dx_rot(2), -0.5_WP*col_width*dx_rot(3) /)
    col_verts(:,3) = col_cen + (/ -0.5_WP*col_height*dx_rot(1), +0.5_WP*col_width*dx_rot(2), -0.5_WP*col_width*dx_rot(3) /)
    col_verts(:,4) = col_cen + (/ +0.5_WP*col_height*dx_rot(1), +0.5_WP*col_width*dx_rot(2), -0.5_WP*col_width*dx_rot(3) /)
    col_verts(:,5) = col_cen + (/ -0.5_WP*col_height*dx_rot(1), -0.5_WP*col_width*dx_rot(2), +0.5_WP*col_width*dx_rot(3) /)
    col_verts(:,6) = col_cen + (/ +0.5_WP*col_height*dx_rot(1), -0.5_WP*col_width*dx_rot(2), +0.5_WP*col_width*dx_rot(3) /)
    col_verts(:,7) = col_cen + (/ -0.5_WP*col_height*dx_rot(1), +0.5_WP*col_width*dx_rot(2), +0.5_WP*col_width*dx_rot(3) /)
    col_verts(:,8) = col_cen + (/ +0.5_WP*col_height*dx_rot(1), +0.5_WP*col_width*dx_rot(2), +0.5_WP*col_width*dx_rot(3) /)

    ! Vertices in physical coordiante system
    do n=1,8
       col_verts(:,n)=matmul(Rot,col_verts(:,n))
    end do

    ! Compute indices of tet verts
    do n=1,8
       col_inds(:,n)=get_indices(col_verts(:,n),(/i,j,k/))
    end do

    ! Break column into five tets
    verts(:,1,1)=col_verts(:,1); verts(:,2,1)=col_verts(:,2); verts(:,3,1)=col_verts(:,4); verts(:,4,1)=col_verts(:,6)
    verts(:,1,2)=col_verts(:,1); verts(:,2,2)=col_verts(:,4); verts(:,3,2)=col_verts(:,3); verts(:,4,2)=col_verts(:,7)
    verts(:,1,3)=col_verts(:,1); verts(:,2,3)=col_verts(:,5); verts(:,3,3)=col_verts(:,6); verts(:,4,3)=col_verts(:,7)
    verts(:,1,4)=col_verts(:,1); verts(:,2,4)=col_verts(:,6); verts(:,3,4)=col_verts(:,4); verts(:,4,4)=col_verts(:,7)
    verts(:,1,5)=col_verts(:,4); verts(:,2,5)=col_verts(:,6); verts(:,3,5)=col_verts(:,8); verts(:,4,5)=col_verts(:,7)

    inds(:,1,1)=col_inds(:,1); inds(:,2,1)=col_inds(:,2); inds(:,3,1)=col_inds(:,4); inds(:,4,1)=col_inds(:,6)
    inds(:,1,2)=col_inds(:,1); inds(:,2,2)=col_inds(:,4); inds(:,3,2)=col_inds(:,3); inds(:,4,2)=col_inds(:,7)
    inds(:,1,3)=col_inds(:,1); inds(:,2,3)=col_inds(:,5); inds(:,3,3)=col_inds(:,6); inds(:,4,3)=col_inds(:,7)
    inds(:,1,4)=col_inds(:,1); inds(:,2,4)=col_inds(:,6); inds(:,3,4)=col_inds(:,4); inds(:,4,4)=col_inds(:,7)
    inds(:,1,5)=col_inds(:,4); inds(:,2,5)=col_inds(:,6); inds(:,3,5)=col_inds(:,8); inds(:,4,5)=col_inds(:,7)

    ! Zero fluxes
    fluxes=0.0_WP

    ! Loop over tets
    do ntet=1,5
       ! Compute liquid volume within tet and add to fluxes
       fluxes=fluxes+tet2flux(verts(:,:,ntet),inds(:,:,ntet))
    end do

    ! Output
    liq_vol=fluxes(2)

    return
  end function multiphase_cut_column

end subroutine multiphase_curv_calc

! ================================ !
! 4th-order height function method !
! ================================ !
subroutine multiphase_curvh4_calc
  use multiphase_curv
  implicit none

  integer :: i,j,k,l,m,n
  real(WP), dimension(3) :: nv,dx_rot
  real(WP), dimension(npnorm) :: dot
  real(WP), dimension(-2:2,-2:2) :: height
  real(WP) :: Ht1,Ht2,Ht1t1,Ht1t2,Ht2t2
  real(WP), dimension(-2:2,-2:2) :: dhdx,dhdy,dhdxx,dhdyy,dhdxy
  ! Data generated with height_function_4th_order.m MATLAB code
  data dhdx(:,-2) / +4.882812500000000e-04_WP, -3.320312500000000e-03_WP, +0.000000000000000e+00_WP, +3.320312500000000e-03_WP, -4.882812500000000e-04_WP / 
  data dhdx(:,-1) / -6.293402777777778e-03_WP, +4.279513888888889e-02_WP, +0.000000000000000e+00_WP, -4.279513888888889e-02_WP, +6.293402777777778e-03_WP / 
  data dhdx(:,+0) / +1.157769097222222e-01_WP, -7.872829861111111e-01_WP, +0.000000000000000e+00_WP, +7.872829861111111e-01_WP, -1.157769097222222e-01_WP / 
  data dhdx(:,+1) / -6.293402777777778e-03_WP, +4.279513888888889e-02_WP, +0.000000000000000e+00_WP, -4.279513888888889e-02_WP, +6.293402777777778e-03_WP / 
  data dhdx(:,+2) / +4.882812500000000e-04_WP, -3.320312500000000e-03_WP, +0.000000000000000e+00_WP, +3.320312500000000e-03_WP, -4.882812500000000e-04_WP / 
  data dhdy(:,-2) / +4.882812500000000e-04_WP, -6.293402777777778e-03_WP, +1.157769097222222e-01_WP, -6.293402777777778e-03_WP, +4.882812500000000e-04_WP / 
  data dhdy(:,-1) / -3.320312500000000e-03_WP, +4.279513888888889e-02_WP, -7.872829861111111e-01_WP, +4.279513888888889e-02_WP, -3.320312500000000e-03_WP / 
  data dhdy(:,+0) / +0.000000000000000e+00_WP, +0.000000000000000e+00_WP, +0.000000000000000e+00_WP, +0.000000000000000e+00_WP, +0.000000000000000e+00_WP / 
  data dhdy(:,+1) / +3.320312500000000e-03_WP, -4.279513888888889e-02_WP, +7.872829861111111e-01_WP, -4.279513888888889e-02_WP, +3.320312500000000e-03_WP / 
  data dhdy(:,+2) / -4.882812500000000e-04_WP, +6.293402777777778e-03_WP, -1.157769097222222e-01_WP, +6.293402777777778e-03_WP, -4.882812500000000e-04_WP / 
  data dhdxx(:,-2) / -5.859375000000000e-04_WP, +7.031250000000000e-03_WP, -1.289062500000000e-02_WP, +7.031250000000000e-03_WP, -5.859375000000000e-04_WP / 
  data dhdxx(:,-1) / +7.552083333333333e-03_WP, -9.062500000000000e-02_WP, +1.661458333333333e-01_WP, -9.062500000000000e-02_WP, +7.552083333333333e-03_WP / 
  data dhdxx(:,+0) / -1.389322916666667e-01_WP, +1.667187500000000e+00_WP, -3.056510416666667e+00_WP, +1.667187500000000e+00_WP, -1.389322916666667e-01_WP / 
  data dhdxx(:,+1) / +7.552083333333333e-03_WP, -9.062500000000000e-02_WP, +1.661458333333333e-01_WP, -9.062500000000000e-02_WP, +7.552083333333333e-03_WP / 
  data dhdxx(:,+2) / -5.859375000000000e-04_WP, +7.031250000000000e-03_WP, -1.289062500000000e-02_WP, +7.031250000000000e-03_WP, -5.859375000000000e-04_WP / 
  data dhdyy(:,-2) / -5.859375000000000e-04_WP, +7.552083333333333e-03_WP, -1.389322916666667e-01_WP, +7.552083333333333e-03_WP, -5.859375000000000e-04_WP / 
  data dhdyy(:,-1) / +7.031250000000000e-03_WP, -9.062500000000000e-02_WP, +1.667187500000000e+00_WP, -9.062500000000000e-02_WP, +7.031250000000000e-03_WP / 
  data dhdyy(:,+0) / -1.289062500000000e-02_WP, +1.661458333333333e-01_WP, -3.056510416666667e+00_WP, +1.661458333333333e-01_WP, -1.289062500000000e-02_WP / 
  data dhdyy(:,+1) / +7.031250000000000e-03_WP, -9.062500000000000e-02_WP, +1.667187500000000e+00_WP, -9.062500000000000e-02_WP, +7.031250000000000e-03_WP / 
  data dhdyy(:,+2) / -5.859375000000000e-04_WP, +7.552083333333333e-03_WP, -1.389322916666667e-01_WP, +7.552083333333333e-03_WP, -5.859375000000000e-04_WP / 
  data dhdxy(:,-2) / +1.085069444444444e-02_WP, -7.378472222222222e-02_WP, +0.000000000000000e+00_WP, +7.378472222222222e-02_WP, -1.085069444444444e-02_WP / 
  data dhdxy(:,-1) / -7.378472222222222e-02_WP, +5.017361111111112e-01_WP, +0.000000000000000e+00_WP, -5.017361111111112e-01_WP, +7.378472222222222e-02_WP / 
  data dhdxy(:,+0) / +0.000000000000000e+00_WP, +0.000000000000000e+00_WP, +0.000000000000000e+00_WP, +0.000000000000000e+00_WP, +0.000000000000000e+00_WP / 
  data dhdxy(:,+1) / +7.378472222222222e-02_WP, -5.017361111111112e-01_WP, +0.000000000000000e+00_WP, +5.017361111111112e-01_WP, -7.378472222222222e-02_WP / 
  data dhdxy(:,+2) / -1.085069444444444e-02_WP, +7.378472222222222e-02_WP, +0.000000000000000e+00_WP, -7.378472222222222e-02_WP, +1.085069444444444e-02_WP /

  ! Zero curvature 
  curv = 0.0_WP

  ! Loop over domain
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        cells1 : do i=imin_,imax_ 
           if ( VOFavg(i,j,k).le.0.5_WP.and.maxval((/VOFavg(i-1,j,k),VOFavg(i,j-1,k),VOFavg(i,j,k-1)/)).gt.0.5_WP .or. &
                VOFavg(i,j,k).le.0.5_WP.and.maxval((/VOFavg(i+1,j,k),VOFavg(i,j+1,k),VOFavg(i,j,k+1)/)).gt.0.5_WP .or. &
                VOFavg(i,j,k).gt.0.5_WP.and.minval((/VOFavg(i-1,j,k),VOFavg(i,j-1,k),VOFavg(i,j,k-1)/)).lt.0.5_WP .or. &
                VOFavg(i,j,k).gt.0.5_WP.and.minval((/VOFavg(i+1,j,k),VOFavg(i,j+1,k),VOFavg(i,j,k+1)/)).lt.0.5_WP ) then
              ! Continue computing curvature
           else
              cycle cells1
           end if

           ! Compute average normal 
           nv(1)=sum(normx(:,i-1:i+1,j-1:j+1,k-1:k+1))
           nv(2)=sum(normy(:,i-1:i+1,j-1:j+1,k-1:k+1))
           nv(3)=sum(normz(:,i-1:i+1,j-1:j+1,k-1:k+1))
           if (sqrt(sum(nv(:)**2)).lt.1e-10) cycle
           nv=nv/sqrt(sum(nv(:)**2))

           ! Compute dot product with pseudo-normals
           do n=1,npnorm
              dot(n)=abs(dot_product(nv,pnorm(:,n)))
           end do

           ! Compute heights based on normal vector
           select case(maxloc(dot,1))
              
           case (1)
              ! Heights along x-coordinate
              do l=-2,2
                 do m=-2,2
                    height(l,m)=sum(VOFavg(i-6:i+6,j+l,k+m)*dx(i-6:i+6))
                 end do
              end do
              dx_rot(2)=dy(j)
              dx_rot(3)=dz 
           case (2)
              ! Heights along y-coordinate
              do l=-2,2
                 do m=-2,2
                    height(l,m)=sum(VOFavg(i+l,j-6:j+6,k+m)*dy(j-6:j+6))
                 end do
              end do 
              dx_rot(2)=dx(i)
              dx_rot(3)=dz 
           case (3)
              ! Heights along z-coordinate
              do l=-2,2
                 do m=-2,2
                    height(l,m)=sum(VOFavg(i+l,j+m,k-6:k+6)*dz)
                 end do
              end do
              dx_rot(2)=dx(i)
              dx_rot(3)=dy(j)
           end select
          
           ! Finite difference opperators on heights
           Ht1  =sum(dhdx *height)/(dx_rot(2))
           Ht2  =sum(dhdy *height)/(dx_rot(3))
           Ht1t1=sum(dhdxx*height)/(dx_rot(2)**2)
           Ht2t2=sum(dhdyy*height)/(dx_rot(3)**2)
           Ht1t2=sum(dhdxy*height)/(dx_rot(2)*dx_rot(3))
           ! Compute curvature
           curv(i,j,k) = -(Ht1t1 + Ht2t2 + Ht1t1*Ht2**2 + Ht2t2*Ht1**2 - 2.0_WP*Ht1t2*Ht1*Ht2) &
                / (1.0_WP + Ht1**2 + Ht2**2)**(1.5_WP)
           hascurv(i,j,k)=1

           ! Compute curvature (Bornia 2011 - kappa_p (paragraph after Eq. 15))
           ! curv(i,j,k)=(-2.0_WP*dx_rot(2)*(2.0_WP*dx_rot(2)**2*(12.0_WP*(height(-1,0)+height(+1,0)) - (height(-2,0)+22.0_WP*height(0,0)+height(2,0))) &
           !      + (height(-1,0)-height(+1,0))*((height(-1,0)-height(+1,0))*(height(-1,0)+height(+1,0)-height(0,0)) &
           !      + height(+2,0)*(5.0_WP*height(0,0)-3.0_WP*height(-1,0)-2.0_WP*height(+1,0)) &
           !      - height(-2,0)*(5.0_WP*height(0,0)-2.0_WP*height(-1,0)-3.0_WP*height(+1,0)))) )&
           !      / &
           !      (4.0_WP*dx_rot(2)**2+(height(-1,0)-height(+1,0))**2)**(2.5_WP)
           
        end do cells1
     end do
  end do

  return
end subroutine multiphase_curvh4_calc

