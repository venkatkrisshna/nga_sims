! ================================= !
! Module to compute semi-Lagrangian !
! and finte difference fluxes       !
! of mass and momentum              !
! ================================= !
module multiphase_fluxes
  use multiphase
  use compgeom_lookup
  use data
  implicit none
  
  ! Semi-Lagrangian fluxes on a mesh that is 2x finer 
  ! the flow solver mesh
  
  !              CELL INDEX : i,j,k and SUBCELLS
  !
  !                          ^ y
  !                         /
  !                       / 
  !                     /
  !                    ------------------------------------------z(k+1)
  !                   /                                        / |
  !                 /                  /                     /   |
  !               /    (7,i,j,k)             (8,i,j,k)     /     |
  !             /                  /                     /       |
  !  ^ z      ------------------------------------------         |
  !  |       /                                        / |        |
  !  |     /                  /                     /   |        |
  !  |   /                                        /     |        |zm(k)
  !    /                  /                     /       |        |
  !  ------------------------------------------         |     /  |
  !  |                  |                     |         |        |
  !  |                                        |         | /      |  
  !  |                  |                     |         |        |
  !  |     (5,i,j,k)           (6,i,j,k)      |         | (4,i,j,k)
  !  |                  |                     |     /   |        |
  !  |                                        |         |        |z(k)
  !  |                  |                     | /       |       / y(j+1)
  !  --  --  --  --  --  --  --  --  --  --  --         |     /  
  !  |                  |                     |         |   /
  !  |                                        |         | / 
  !  |                  |                     |         |ym(j)
  !  |      (1,i,j,)            (2,i,j,k)     |       /   
  !  |                  |                     |     /
  !  |                                        |   /
  !  |                  |                     | /y(j)
  !  ------------------------------------------    -------> x
  !  x(i)              xm(i)                 x(i+1)


  !     Cell face point and subface arrangement
  ! 
  !     ^ t2
  !     |
  !       p7        p8       p9     
  !     --------------------
  !     |         |        |
  !     |    f4   |    f3  |
  !     | p4      | p5     | p6
  !     --------------------
  !     |         |        |
  !     |    f1   |    f2  |
  !     | p1      | p2     | p3
  !     --------------------    --> t1
  

  ! Fluxes
  real(WP), dimension(:,:,:,:), allocatable :: SLF_volx,SLF_voly,SLF_volz
  real(WP), dimension(:,:,:,:), allocatable :: SLF_liqx,SLF_liqy,SLF_liqz
  real(WP), dimension(:,:,:,:), allocatable :: SLF_gasx,SLF_gasy,SLF_gasz
  real(WP), dimension(:,:,:),   allocatable :: SLF_rhoUx,SLF_rhoUy,SLF_rhoUz
  real(WP), dimension(:,:,:),   allocatable :: SLF_rhoVx,SLF_rhoVy,SLF_rhoVz
  real(WP), dimension(:,:,:),   allocatable :: SLF_rhoWx,SLF_rhoWy,SLF_rhoWz
  real(WP), dimension(:,:,:,:), allocatable :: SLF_SCx,SLF_SCy,SLF_SCz

  ! Adjusted projected points (pointer to tmp arrays)
  real(WP), dimension(:,:,:), pointer :: pt_xface,pt_yface,pt_zface

  ! Parameters
  real(WP) :: volume_epsilon
  
contains
  
  ! ========================= !
  ! Subcell index (s,i,j,k)   !
  ! to sub cell index (i,j,k) !
  ! ========================= !
  ! i index
  function sub2i(i,s) 
    implicit none 
    integer, intent(in) :: i,s
    integer :: sub2i
    sub2i=2*(i-1)+1+sub2u(s)
    return 
  end function sub2i
  ! j index
  function sub2j(j,s) 
    implicit none 
    integer, intent(in) :: j,s
    integer :: sub2j
    sub2j=2*(j-1)+1+sub2v(s)
    return 
  end function sub2j
  ! k index
  function sub2k(k,s) 
    implicit none 
    integer, intent(in) :: k,s
    integer :: sub2k
    sub2k=2*(k-1)+1+sub2w(s)
    return 
  end function sub2k
  
  ! ====================== !
  ! Computes volume of tet !
  ! ====================== !
  function tet_vol(verts) 
    implicit none
    real(WP) :: tet_vol
    real(WP), dimension(3,4), intent(in) :: verts
    real(WP), dimension(3) :: a,b,c
    real(WP), parameter :: f1_6=0.16666666666666667_WP
    a=verts(:,1)-verts(:,4)
    b=verts(:,2)-verts(:,4)
    c=verts(:,3)-verts(:,4)
    tet_vol = -f1_6 * &
         ( a(1) * ( b(2)*c(3) - c(2)*b(3) ) &
         - a(2) * ( b(1)*c(3) - c(1)*b(3) ) &
         + a(3) * ( b(1)*c(2) - c(1)*b(2) ) )
    return
  end function tet_vol

  ! ===================== !
  ! Calculate sign of tet !
  ! ===================== !
  function tetsign(tet) result(s)
    implicit none
    real(WP) :: s
    real(WP), dimension(3,4), intent(in) :: tet
    s=sign(1.0_WP,tet_vol(tet))
    return
  end function tetsign
  
  ! ================================ !
  ! Adjust flux center point on flux !
  ! volume to ensure divergence free !
  ! ================================ !
  function adjust_fluxVol(dir,p5,p11,p13,p14,p15,p17,V) result(p14out)
    implicit none
    integer, intent(in) :: dir
    real(WP), dimension(3), intent(in) :: p5,p11,p13,p14,p15,p17
    real(WP), intent(in) :: V
    real(WP), dimension(3) :: p14out
    integer :: i1,i2,i3
    ! Adjust indices based on direction
    select case(dir)
    case(1); i1=1; i2=2; i3=3; 
    case(2); i1=2; i2=1; i3=3;
    case(3); i1=3; i2=1; i3=2;
    end select
    ! Adjust dir vertex of p14
    p14out=p14
    p14out(dir)=-(-6.0_WP*V + p11(i1)*p13(i2)*p14(i3) - p11(i1)*p13(i3)*p14(i2) &
         - p11(i2)*p13(i1)*p14(i3) + p11(i3)*p13(i1)*p14(i2) + p11(i1)*p14(i2)*p15(i3) &
         - p11(i1)*p14(i3)*p15(i2) + p11(i2)*p15(i1)*p14(i3) - p11(i3)*p14(i2)*p15(i1) &
         - p13(i1)*p14(i2)*p17(i3) + p13(i1)*p14(i3)*p17(i2) - p13(i2)*p14(i3)*p17(i1) &
         + p13(i3)*p14(i2)*p17(i1) + p14(i2)*p15(i1)*p17(i3) - p14(i2)*p15(i3)*p17(i1) &
         - p15(i1)*p14(i3)*p17(i2) + p14(i3)*p15(i2)*p17(i1) - p11(i2)*p13(i3)*p5(i1) &
         + p11(i3)*p13(i2)*p5(i1) + p11(i2)*p15(i3)*p5(i1) - p11(i3)*p15(i2)*p5(i1) &
         - p13(i2)*p17(i3)*p5(i1) + p13(i3)*p17(i2)*p5(i1) + p15(i2)*p17(i3)*p5(i1) &
         - p15(i3)*p17(i2)*p5(i1) + p11(i1)*p13(i3)*p5(i2) - p11(i3)*p13(i1)*p5(i2) &
         - p11(i1)*p15(i3)*p5(i2) + p11(i3)*p15(i1)*p5(i2) + p13(i1)*p17(i3)*p5(i2) &
         - p13(i3)*p17(i1)*p5(i2) - p15(i1)*p17(i3)*p5(i2) + p15(i3)*p17(i1)*p5(i2) &
         - p11(i1)*p13(i2)*p5(i3) + p11(i2)*p13(i1)*p5(i3) + p11(i1)*p15(i2)*p5(i3) &
         - p11(i2)*p15(i1)*p5(i3) - p13(i1)*p17(i2)*p5(i3) + p13(i2)*p17(i1)*p5(i3) &
         + p15(i1)*p17(i2)*p5(i3) - p15(i2)*p17(i1)*p5(i3))/(p11(i2)*p13(i3) &
         - p11(i3)*p13(i2) - p11(i2)*p15(i3) + p11(i3)*p15(i2) + p13(i2)*p17(i3) &
         - p13(i3)*p17(i2) - p15(i2)*p17(i3) + p15(i3)*p17(i2))
    return 
  end function adjust_fluxVol

  ! ==================== !
  ! Get indices of point !
  ! ==================== !
  function get_indices(pt,ind_in) result(ind)
    implicit none
    real(WP), dimension(3), intent(in) :: pt
    integer,  dimension(3), intent(in) :: ind_in
    integer, dimension(3) :: ind
    ind=ind_in

    ! x direction
    do while (pt(1).gt.x(ind(1)+1)); ind(1)=ind(1)+1; end do
    do while (pt(1).lt.x(ind(1)  )); ind(1)=ind(1)-1; end do
   
    ! y direction
    do while (pt(2).gt.y(ind(2)+1)); ind(2)=ind(2)+1; end do
    do while (pt(2).lt.y(ind(2)  )); ind(2)=ind(2)-1; end do
          
    ! z direction
    do while (pt(3).gt.z(ind(3)+1)); ind(3)=ind(3)+1; end do
    do while (pt(3).lt.z(ind(3)  )); ind(3)=ind(3)-1; end do
  
    return 
  end function get_indices

  ! ============================= !
  ! Cut tet by computational mesh !
  ! to compute fluxes             !
  ! ============================= !
  recursive function tet2flux(tet,ind) result(fluxes)
    implicit none
    real(WP), dimension(3,4), intent(in) :: tet
    integer,  dimension(3,4), intent(in) :: ind
    real(WP), dimension(6+nscalar) :: fluxes
    integer :: dir,cut_ind
    integer :: n,nn,case
    integer :: v1,v2
    real(WP), dimension(4) :: d
    real(WP), dimension(3,8) :: vert
    integer,  dimension(3,8,2) :: vert_ind
    real(WP) :: mu
    real(WP), dimension(3,4) :: newtet
    integer,  dimension(3,4) :: newind
    
    ! Cut by x planes
    if (maxval(ind(1,:))-minval(ind(1,:)).gt.0) then
       dir=1
       cut_ind=maxval(ind(1,:))
       d(:)=tet(1,:)-x(cut_ind)
    ! Cut by y planes
    else if (maxval(ind(2,:))-minval(ind(2,:)).gt.0) then
       dir=2
       cut_ind=maxval(ind(2,:))
       d(:)=tet(2,:)-y(cut_ind)
    ! Cut by z planes
    else if (maxval(ind(3,:))-minval(ind(3,:)).gt.0) then
       dir=3
       cut_ind=maxval(ind(3,:))
       d(:)=tet(3,:)-z(cut_ind)
    ! Cut by interface and compute fluxes
    else 
       fluxes=tet2flux_subcells(tet,ind(:,1),1,1)    
       return
    end if
    
    ! Find case of cut
    case=1+int(0.5_WP+sign(0.5_WP,d(1)))+&
         2*int(0.5_WP+sign(0.5_WP,d(2)))+&
         4*int(0.5_WP+sign(0.5_WP,d(3)))+&
         8*int(0.5_WP+sign(0.5_WP,d(4)))   
    
    ! Get vertices and indices of tet
    do n=1,4
       vert    (:,n)   = tet(:,n)
       vert_ind(:,n,1) = ind(:,n)
       vert_ind(:,n,2) = ind(:,n)
       vert_ind(dir,n,1)=min(vert_ind(dir,n,1),cut_ind-1) ! Enforce boundedness
       vert_ind(dir,n,2)=max(vert_ind(dir,n,1),cut_ind  )
    end do

    ! Create interpolated vertices on cut plane
    do n=1,cut_nvert(case)
       v1=cut_v1(n,case); v2=cut_v2(n,case)
       mu=min(1.0_WP,max(0.0_WP,-d(v1)/(sign(abs(d(v2)-d(v1))+tiny(1.0_WP),d(v2)-d(v1)))))
       vert(:,4+n)=(1.0_WP-mu)*vert(:,v1)+mu*vert(:,v2)
       ! Get index for interpolated vertex
       vert_ind(:,4+n,1)=get_indices(vert(:,4+n),vert_ind(:,v1,1))
       ! Enforce boundedness
       vert_ind(:,4+n,1)=max(vert_ind(:,4+n,1),min(vert_ind(:,v1,1),vert_ind(:,v2,1)))
       vert_ind(:,4+n,1)=min(vert_ind(:,4+n,1),max(vert_ind(:,v1,1),vert_ind(:,v2,1)))
       ! Set +/- indices in cut direction
       vert_ind(:,4+n,2)=vert_ind(:,4+n,1)
       vert_ind(dir,4+n,1)=cut_ind-1
       vert_ind(dir,4+n,2)=cut_ind
    end do
    
    ! Create new tets
    fluxes=0.0_WP
    do n=1,cut_ntets(case)
       do nn=1,4
          newtet(:,nn)=vert    (:,cut_vtet(nn,n,case))
          newind(:,nn)=vert_ind(:,cut_vtet(nn,n,case),cut_side(n,case))
       end do
       
       ! Check for zero-volume tet
       if (abs(tet_vol(newtet)).lt.volume_epsilon) cycle

       ! Cut by next plane
       fluxes=fluxes+tet2flux(newtet,newind)
       
    end do
    
    return
  end function tet2flux
  
  ! ======================= !
  ! Cut tet by subcells and !
  ! PLIC interface          !
  ! ======================= !
  recursive function tet2flux_subcells(tet,ind,dir,subcell) result(fluxes)
    use velocity
    use scalar
    implicit none
    real(WP), dimension(3,4), intent(in) :: tet
    integer,  dimension  (3), intent(in) :: ind
    integer, intent(in) :: dir,subcell
    real(WP), dimension(6+nscalar) :: fluxes
    integer :: i,j,k,n,nn
    integer :: subcell_factor
    integer :: case,v1,v2
    real(WP) :: mu,my_vol
    real(WP), dimension(4) :: d
    real(WP), dimension(3,8) :: vert
    real(WP), dimension(3,4) :: newtet
    real(WP), dimension(3) :: a,b,c
    real(WP), dimension(3) :: tetCOM

    ! Get index of cell
    i=ind(1); j=ind(2); k=ind(3);
   
    ! Select cut plane
    select case (dir)
    case(1)
       ! Cut by xm(i) plane
       d(:)=tet(1,:)-xm(i)
       subcell_factor=1
    case(2)
       ! Cut by ym(j) plane
       d(:)=tet(2,:)-ym(j)
       subcell_factor=2
    case(3)
       ! Cut by zm(k) plane
       d(:)=tet(3,:)-zm(k)
       subcell_factor=4
    case(4)
       ! Cut by PLIC
       d(:) = normxold(subcell,i,j,k)*tet(1,:) &
            + normyold(subcell,i,j,k)*tet(2,:) &
            + normzold(subcell,i,j,k)*tet(3,:) &
            - distold (subcell,i,j,k)
    end select

    ! Find cut case
    case=1+int(0.5_WP+sign(0.5_WP,d(1)))+&
         2*int(0.5_WP+sign(0.5_WP,d(2)))+&
         4*int(0.5_WP+sign(0.5_WP,d(3)))+&
         8*int(0.5_WP+sign(0.5_WP,d(4)))

    ! Copy vertices
    do n=1,4
       vert(:,n)=tet(:,n)
    end do
    
    ! Create interpolated vertices on cut plane
    do n=1,cut_nvert(case)
       v1=cut_v1(n,case); v2=cut_v2(n,case)
       mu=min(1.0_WP,max(0.0_WP,-d(v1)/(sign(abs(d(v2)-d(v1))+tiny(1.0_WP),d(v2)-d(v1)))))
       vert(:,4+n)=(1.0_WP-mu)*vert(:,v1)+mu*vert(:,v2)
    end do
    
    ! Zero fluxes
    fluxes=0.0_WP

    ! Cut by plane or interface
    select case(dir)
    case(1:3)
       ! Create new tets on minus side
       do n=cut_ntets(case),cut_nntet(case),-1
          ! Form new tet
          do nn=1,4
             newtet(:,nn)=vert(:,cut_vtet(nn,n,case))
          end do
          ! Check for zero-volume tet
          if (abs(tet_vol(newtet)).lt.volume_epsilon) cycle
          ! Cut by next plane
          fluxes=fluxes+tet2flux_subcells(newtet,ind,dir+1,subcell)
       end do
       ! Create new tets on plus side
       do n=1,cut_nntet(case)-1
          ! Form new tet
          do nn=1,4
             newtet(:,nn)=vert(:,cut_vtet(nn,n,case))
          end do
          ! Check for zero-volume tet
          if (abs(tet_vol(newtet)).lt.volume_epsilon) cycle
          ! Cut by next plane
          fluxes=fluxes+tet2flux_subcells(newtet,ind,dir+1,subcell+subcell_factor)
       end do
       
    case(4)
       ! Create new tets in the liquid
       do n=cut_ntets(case),cut_nntet(case),-1
          ! Form new tet
          do nn=1,4
             newtet(:,nn)=vert(:,cut_vtet(nn,n,case))
          end do
          ! Compute tet volume
          a=newtet(:,1)-newtet(:,4)
          b=newtet(:,2)-newtet(:,4)
          c=newtet(:,3)-newtet(:,4)
          my_vol=abs(a(1)*(b(2)*c(3)-c(2)*b(3)) &
               -     a(2)*(b(1)*c(3)-c(1)*b(3)) &
               +     a(3)*(b(1)*c(2)-c(1)*b(2)))/6.0_WP
          ! Compute tet barycenter
          tetCOM(1)=0.25_WP*sum(newtet(1,:))
          tetCOM(2)=0.25_WP*sum(newtet(2,:))
          tetCOM(3)=0.25_WP*sum(newtet(3,:))
          ! Quantities in liquid phase
          fluxes(1) = fluxes(1) + my_vol  ! Flux volume
          fluxes(2) = fluxes(2) + my_vol  ! Liquid flux
          fluxes(3) = fluxes(3) + 0.0_WP  ! Gas flux
          ! Momentum fluxes - second order reconstruction
          fluxes(4) = fluxes(4) + my_vol*rho_l*(Uold(i+sub2u(subcell),j,k)+sum(gradU(:,i+sub2u(subcell),j,k)*(tetCOM(:)-COMu(:,i+sub2u(subcell),j,k)))) 
          fluxes(5) = fluxes(5) + my_vol*rho_l*(Vold(i,j+sub2v(subcell),k)+sum(gradV(:,i,j+sub2v(subcell),k)*(tetCOM(:)-COMv(:,i,j+sub2v(subcell),k))))
          fluxes(6) = fluxes(6) + my_vol*rho_l*(Wold(i,j,k+sub2w(subcell))+sum(gradW(:,i,j,k+sub2w(subcell))*(tetCOM(:)-COMw(:,i,j,k+sub2w(subcell)))))
          do nn=1,nscalar
            fluxes(6+nn) = fluxes(6+nn) + my_vol*(SCold(i,j,k,nn)+sum(gradSC(:,i,j,k,nn)*(tetCOM(:)-COMliq(:,i,j,k))))*SCphase(nn)
          end do
       end do
       ! Create new tets in the gas
       do n=1,cut_nntet(case)-1
         ! Form new tet
          do nn=1,4
             newtet(:,nn)=vert(:,cut_vtet(nn,n,case))
          end do
          ! Compute tet volume
          a=newtet(:,1)-newtet(:,4)
          b=newtet(:,2)-newtet(:,4)
          c=newtet(:,3)-newtet(:,4)
          my_vol=abs(a(1)*(b(2)*c(3)-c(2)*b(3)) &
               -     a(2)*(b(1)*c(3)-c(1)*b(3)) &
               +     a(3)*(b(1)*c(2)-c(1)*b(2)))/6.0_WP
          ! Compute tet barycenter
          tetCOM(1)=0.25_WP*sum(newtet(1,:))
          tetCOM(2)=0.25_WP*sum(newtet(2,:))
          tetCOM(3)=0.25_WP*sum(newtet(3,:))
          ! Quantities in gas phase
          fluxes(1) = fluxes(1) + my_vol  ! Flux volume
          fluxes(2) = fluxes(2) + 0.0_WP  ! Liquid flux
          fluxes(3) = fluxes(3) + my_vol  ! Gas flux
          ! Momentum fluxes - second order reconstruction
          fluxes(4) = fluxes(4) + my_vol*rho_g*(Uold(i+sub2u(subcell),j,k)+sum(gradU(:,i+sub2u(subcell),j,k)*(tetCOM(:)-COMu(:,i+sub2u(subcell),j,k)))) 
          fluxes(5) = fluxes(5) + my_vol*rho_g*(Vold(i,j+sub2v(subcell),k)+sum(gradV(:,i,j+sub2v(subcell),k)*(tetCOM(:)-COMv(:,i,j+sub2v(subcell),k))))
          fluxes(6) = fluxes(6) + my_vol*rho_g*(Wold(i,j,k+sub2w(subcell))+sum(gradW(:,i,j,k+sub2w(subcell))*(tetCOM(:)-COMw(:,i,j,k+sub2w(subcell)))))
          do nn=1,nscalar
            fluxes(6+nn) = fluxes(6+nn) + my_vol*(SCold(i,j,k,nn)+sum(gradSC(:,i,j,k,nn)*(tetCOM(:)-COMgas(:,i,j,k))))*(1.0_WP-SCphase(nn))
          end do
       end do

    end select
        
    return
  end function tet2flux_subcells
  
end module multiphase_fluxes

! ============================= !
! Semilag module initialization !
! ============================= !
subroutine multiphase_fluxes_init
  use multiphase_fluxes
  implicit none

  ! Flux volume on subcell faces
  allocate(SLF_volx(8,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1))
  allocate(SLF_voly(8,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1))
  allocate(SLF_volz(8,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1))
  ! Flux velocity of liquid on subfaces
  allocate(SLF_liqx(8,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1))
  allocate(SLF_liqy(8,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1))
  allocate(SLF_liqz(8,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1))
  ! Flux velocity of gas on subfaces
  allocate(SLF_gasx(8,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1))
  allocate(SLF_gasy(8,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1))
  allocate(SLF_gasz(8,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1))
  ! Flux of U velocity on U cell faces
  allocate(SLF_rhoUx(imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1))
  allocate(SLF_rhoUy(imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1))
  allocate(SLF_rhoUz(imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1))
  ! Flux of V velocity on V cell faces
  allocate(SLF_rhoVx(imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1))
  allocate(SLF_rhoVy(imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1))
  allocate(SLF_rhoVz(imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1))
  ! Flux of W velocity on W cell faces
  allocate(SLF_rhoWx(imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1))
  allocate(SLF_rhoWy(imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1))
  allocate(SLF_rhoWz(imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1))
  ! Flux of scalar on P cell faces
  allocate(SLF_SCx (imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1,nscalar))
  allocate(SLF_SCy (imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1,nscalar))
  allocate(SLF_SCz (imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1,nscalar))
  
  allocate(cmask_x(8,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1))
  allocate(cmask_y(8,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1))
  allocate(cmask_z(8,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1))
  
  ! Subcell divergence
  allocate(SCdiv(8,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  
  ! Pointers to adjusted projected points
  pt_xface => tmp4
  pt_yface => tmp5
  pt_zface => tmp6

  return
end subroutine multiphase_fluxes_init

! ======================= !
! Compute semi-Lag Fluxes !
! ======================= !
subroutine multiphase_fluxes_calc
  use multiphase_fluxes
  use multiphase_band
  use multiphase_tools
  use metric_velocity_conv
  implicit none

  integer :: s,i,j,k,n,nn,nnn,index,dir
  integer :: i_flux,j_flux,k_flux
  real(WP), dimension(3,22,3) :: pt  ! (xyz,Pt#,dir)
  integer,  dimension(3,22,3) :: ind ! (ijk,Pt#,dir)
  real(WP), dimension(3,4,7,4,3) :: tet_vert ! (xyz,pt,tet,subface,dir)
  integer,  dimension(3,4,7,4,3) :: tet_ind  ! (ijk,pt,tet,subface,dir)
  real(WP), dimension(6+nscalar) :: fluxes
  real(WP) :: fluxVol
  real(WP) :: my_band_CFL,band_CFL
  
  ! Zero fluxes
  SLF_volx =0.0_WP; SLF_voly =0.0_WP; SLF_volz =0.0_WP
  SLF_liqx =0.0_WP; SLF_liqy =0.0_WP; SLF_liqz =0.0_WP
  SLF_gasx =0.0_WP; SLF_gasy =0.0_WP; SLF_gasz =0.0_WP
  SLF_rhoUx=0.0_WP; SLF_rhoUy=0.0_WP; SLF_rhoUz=0.0_WP
  SLF_rhoVx=0.0_WP; SLF_rhoVy=0.0_WP; SLF_rhoVz=0.0_WP
  SLF_rhoWx=0.0_WP; SLF_rhoWy=0.0_WP; SLF_rhoWz=0.0_WP
  SLF_SCx  =0.0_WP; SLF_SCy  =0.0_WP; SLF_SCz  =0.0_WP

  ! Reset convective masks
  cmask_x=1; cmask_y=1; cmask_z=1
  
  ! Compute number of bands
  my_band_CFL=0.0_WP
  do index=1,sum(band_count(0:2))
     i=band_map(index,1)
     j=band_map(index,2)
     k=band_map(index,3)
     my_band_CFL=max(my_band_CFL,sqrt(U(i,j,k)**2+V(i,j,k)**2+W(i,j,k)**2)*dt_uvw/meshsize(i,j,k))
  end do
  call parallel_max(my_band_CFL,band_CFL)
  
  ! Compute number of bands to use
  if (nscalar.gt.0.and.scalar_scheme.eq.'multiphase') then
     nband_CFL=max(ceiling(band_CFL+epsilon(1.0_WP)),2)
  else
     nband_CFL=ceiling(band_CFL+epsilon(1.0_WP))
  end if
  ! Limit with nover
  if (nband_CFL.gt.nover) then
     if (irank.eq.iroot) print *,'Limiting SLF due to excessive CFL'
     nband_CFL=min(nband_CFL,nover)
  end if
  
  ! Loop over the domain and compute fluxes near interfaces
  ! ===========================================================================
  ! x face
  ! ===========================================================================
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_+ioutlet
           if (maxval(abs(band(i-1:i,j,k))).le.nband_CFL) then ! compute SL fluxes

              ! Compute epsilon relative to cell volume
              volume_epsilon=vol(i,j,k)*1.0e-15_WP
              
              ! Points on face at x(i) ============================
              pt(:,1,1)=(/x(i), y (j  ), z (k  ) /)
              pt(:,2,1)=(/x(i), ym(j  ), z (k  ) /)
              pt(:,3,1)=(/x(i), y (j+1), z (k  ) /)
              pt(:,4,1)=(/x(i), y (j  ), zm(k  ) /)
              pt(:,5,1)=(/x(i), ym(j  ), zm(k  ) /)
              pt(:,6,1)=(/x(i), y (j+1), zm(k  ) /)
              pt(:,7,1)=(/x(i), y (j  ), z (k+1) /)
              pt(:,8,1)=(/x(i), ym(j  ), z (k+1) /)
              pt(:,9,1)=(/x(i), y (j+1), z (k+1) /)
              
              ! Project points to create flux volume
              do n=1,9
                 pt(:,9+n,1)=project(pt(:,n,1),-dt_uvw,i,j,k)
              end do
              
              ! Create 5 tets for each subface flux volume
              do n=1,4 ! loop over subfaces
                 do nn=1,5 ! loop over tets
                    do nnn=1,4 ! loop over points on tets
                       tet_vert(:,nnn,nn,n,1)=pt(:,pts2tets(n,nn,nnn),1)
                    end do
                 end do
              end do
              
              ! Make flux divergence free 
              ! Compute volume of tets not dependent on pt14 (center)
              fluxVol=0.0_WP
              do n=1,4 ! loop over subfaces
                 do nn=1,4 ! loop over tets (excluding 5th tet)
                    fluxVol=fluxVol+tet_vol(tet_vert(:,:,nn,n,1))
                 end do
              end do
              ! Adjust x-coordinate of point 14 to ensure divg-free flux
              pt(:,14,1)=adjust_fluxVol(1,pt(:,5,1),pt(:,11,1),pt(:,13,1),pt(:,14,1),pt(:,15,1),pt(:,17,1), &
                   U(i,j,k)*dt_uvw*dy(j)*dz-fluxVol)
              ! Save point (needed to form ym and zm fluxes)
              pt_xface(i,j,k)=pt(1,14,1)-x(i)
              ! Update tets that depend on point 14
              do n=1,4 ! loop over subfaces
                 tet_vert(:,4,5,n,1)=pt(:,14,1)
              end  do
              
              ! Compute indices of all points
              i_flux=i-int(0.5_WP*(sign(1.0_WP,U(i,j,k))+1.0_WP))
              do n=1,18
                 ind(:,n,1)=get_indices(pt(:,n,1),(/i_flux,j,k/))
              end do
              
              ! Create tet indices
              do n=1,4 ! loop over subfaces
                 do nn=1,5 ! loop over tets
                    do nnn=1,4 ! loop over points on tets
                       tet_ind(:,nnn,nn,n,1)=ind(:,pts2tets(n,nn,nnn),1)
                    end do
                 end do
              end do
              
              ! Compute semi-Lagrangian fluxes 
              do n=1,4 ! loop over subfaces
                 do nn=1,5 ! loop over tets
                    fluxes(:)=+tetsign(tet_vert(:,:,nn,n,1))*tet2flux(tet_vert(:,:,nn,n,1),tet_ind(:,:,nn,n,1))
                    ! Transfer to semi-Lagrangian flux arrays
                    SLF_volx(subface2subcell_x(n),i,j,k)=SLF_volx(subface2subcell_x(n),i,j,k)+fluxes(1)
                    SLF_liqx(subface2subcell_x(n),i,j,k)=SLF_liqx(subface2subcell_x(n),i,j,k)+fluxes(2)
                    SLF_gasx(subface2subcell_x(n),i,j,k)=SLF_gasx(subface2subcell_x(n),i,j,k)+fluxes(3)
                    ! SLF_rhoUx internal at x(i)
                    SLF_rhoVx(i,j+subface2vcell_x(n),k)=SLF_rhoVx(i,j+subface2vcell_x(n),k)+fluxes(5)
                    SLF_rhoWx(i,j,k+subface2wcell_x(n))=SLF_rhoWx(i,j,k+subface2wcell_x(n))+fluxes(6)
                    do nnn=1,nscalar
                       SLF_SCx(i,j,k,nnn)=SLF_SCx(i,j,k,nnn)+fluxes(6+nnn)
                    end do
                 end do
              end do
              
              ! Set cmask
              cmask_x(subface2subcell_x(:),i,j,k)=0

           else ! Finite difference fluxes
              fluxVol=0.25_WP*U(i,j,k)*dt_uvw*dy(j)*dz
              SLF_volx(subface2subcell_x(:),i,j,k)=fluxVol
              if (sum(VOFavg(i-1:i,j,k)).lt.1.0_WP) then ! Gas
                 SLF_liqx(subface2subcell_x(:),i,j,k)=0.0_WP
                 SLF_gasx(subface2subcell_x(:),i,j,k)=fluxVol
              else  ! Liquid
                 SLF_liqx(subface2subcell_x(:),i,j,k)=fluxVol
                 SLF_gasx(subface2subcell_x(:),i,j,k)=0.0_WP
              end if
              do n=1,4
                 do nn=-stc2,+stc1
                    ! SLF_rhoUx internal at x(i)
                    SLF_rhoVx(i,j+subface2vcell_x(n),k)=SLF_rhoVx(i,j+subface2vcell_x(n),k)+0.25_WP &
                         * interp_Jv_x(i,j,nn)*rho_V(i+nn,j+subface2vcell_x(n),k)*V(i+nn,j+subface2vcell_x(n),k) &
                         * U(i,j,k) * dt_uvw*dy(j)*dz
                    SLF_rhoWx(i,j,k+subface2wcell_x(n))=SLF_rhoWx(i,j,k+subface2wcell_x(n))+0.25_WP &
                         * interp_Jw_x(i,j,nn)*rho_W(i+nn,j,k+subface2wcell_x(n))*W(i+nn,j,k+subface2wcell_x(n)) &
                         * U(i,j,k) * dt_uvw*dy(j)*dz
                 end do
                 ! Scalars fluxes computed with WENO in multiphase_scalar
              end do
           end if
        end do
     end do
  end do
  
  ! ===========================================================================
  ! y face
  ! ===========================================================================
  do k=kmin_,kmax_
     do j=jmin_,jmax_+joutlet
        do i=imin_,imax_
           if (maxval(abs(band(i,j-1:j,k))).le.nband_CFL) then ! compute fluxes
              
              ! Compute epsilon relative to cell volume
              volume_epsilon=vol(i,j,k)*1.0e-15_WP
              
              ! Points on face at y(j) ============================
              pt(:,1,2)=(/x (i  ), y(j), z (k  ) /)
              pt(:,2,2)=(/xm(i  ), y(j), z (k  ) /)
              pt(:,3,2)=(/x (i+1), y(j), z (k  ) /)
              pt(:,4,2)=(/x (i  ), y(j), zm(k  ) /)
              pt(:,5,2)=(/xm(i  ), y(j), zm(k  ) /)
              pt(:,6,2)=(/x (i+1), y(j), zm(k  ) /)
              pt(:,7,2)=(/x (i  ), y(j), z (k+1) /)
              pt(:,8,2)=(/xm(i  ), y(j), z (k+1) /)
              pt(:,9,2)=(/x (i+1), y(j), z (k+1) /)
              
              ! Project points to create flux volume
              do n=1,9
                 pt(:,9+n,2)=project(pt(:,n,2),-dt_uvw,i,j,k)
              end do
              
              ! Create 5 tets for each subface flux volume
              do n=1,4 ! loop over subfaces
                 do nn=1,5 ! loop over tets
                    do nnn=1,4 ! loop over points on tets
                       tet_vert(:,nnn,nn,n,2)=pt(:,pts2tets(n,nn,nnn),2)
                    end do
                 end do
              end do
              
              ! Make flux divergence free 
              ! Compute volume of tets not dependent on pt14 (center)
              fluxVol=0.0_WP
              do n=1,4 ! loop over subfaces
                 do nn=1,4 ! loop over tets (excluding 5th tet)
                    fluxVol=fluxVol-tet_vol(tet_vert(:,:,nn,n,2))
                 end do
              end do
              ! Adjust y-coordinate of point 14 to ensure divg-free flux
              pt(:,14,2)=adjust_fluxVol(2,pt(:,5,2),pt(:,11,2),pt(:,13,2),pt(:,14,2),pt(:,15,2),pt(:,17,2), &
                   (V(i,j,k)*dt_uvw*dx(i)*dz-fluxVol))
              ! Save point (needed to form xm and zm fluxes)
              pt_yface(i,j,k)=pt(2,14,2)-y(j)
              ! Update tets that depend on point 14
              do n=1,4 ! loop over subfaces
                 tet_vert(:,4,5,n,2)=pt(:,14,2)
              end  do
              
              ! Compute indices of all points
              j_flux=j-int(0.5_WP*(sign(1.0_WP,V(i,j,k))+1.0_WP))
              do n=1,18
                 ind(:,n,2)=get_indices(pt(:,n,2),(/i,j_flux,k/))
              end do
              
              ! Create tet indices
              do n=1,4 ! loop over subfaces
                 do nn=1,5 ! loop over tets
                    do nnn=1,4 ! loop over points on tets
                       tet_ind(:,nnn,nn,n,2)=ind(:,pts2tets(n,nn,nnn),2)
                    end do
                 end do
              end do
              
              ! Compute fluxes 
              do n=1,4 ! loop over subfaces
                 do nn=1,5 ! loop over tets
                    fluxes(:)=-tetsign(tet_vert(:,:,nn,n,2))*tet2flux(tet_vert(:,:,nn,n,2),tet_ind(:,:,nn,n,2))
                    ! Transfer to semi-Lagrangian flux arrays
                    SLF_voly(subface2subcell_y(n),i,j,k)=SLF_voly(subface2subcell_y(n),i,j,k)+fluxes(1)
                    SLF_liqy(subface2subcell_y(n),i,j,k)=SLF_liqy(subface2subcell_y(n),i,j,k)+fluxes(2)
                    SLF_gasy(subface2subcell_y(n),i,j,k)=SLF_gasy(subface2subcell_y(n),i,j,k)+fluxes(3)
                    SLF_rhoUy(i+subface2ucell_y(n),j,k)=SLF_rhoUy(i+subface2ucell_y(n),j,k)+fluxes(4)
                    ! SLF_rhoVy internal at y(i)
                    SLF_rhoWy(i,j,k+subface2wcell_y(n))=SLF_rhoWy(i,j,k+subface2wcell_y(n))+fluxes(6)
                    do nnn=1,nscalar
                       SLF_SCy(i,j,k,nnn)=SLF_SCy(i,j,k,nnn)+fluxes(6+nnn)
                    end do
                 end do
              end do

              ! Set cmask
              cmask_y(subface2subcell_y(:),i,j,k)=0

           else ! Finite difference fluxes
              fluxVol=0.25_WP*V(i,j,k)*dt_uvw*dx(i)*dz
              SLF_voly(subface2subcell_y(:),i,j,k)=fluxVol
              if (sum(VOFavg(i,j-1:j,k)).lt.1.0_WP) then ! Gas
                 SLF_liqy(subface2subcell_y(:),i,j,k)=0.0_WP
                 SLF_gasy(subface2subcell_y(:),i,j,k)=fluxVol
              else  ! Liquid
                 SLF_liqy(subface2subcell_y(:),i,j,k)=fluxVol
                 SLF_gasy(subface2subcell_y(:),i,j,k)=0.0_WP
              end if
              do n=1,4
                 do nn=-stc2,+stc1
                    SLF_rhoUy(i+subface2ucell_y(n),j,k)=SLF_rhoUy(i+subface2ucell_y(n),j,k)+0.25_WP &
                         * interp_Ju_y(i,j,nn)*rho_U(i+subface2ucell_y(n),j+nn,k)*U(i+subface2ucell_y(n),j+nn,k) &
                         * V(i,j,k) * dt_uvw*dx(i)*dz
                    ! SLF_rhoVy internal at y(j)
                    SLF_rhoWy(i,j,k+subface2wcell_y(n))=SLF_rhoWy(i,j,k+subface2wcell_y(n))+0.25_WP &
                         * interp_Jw_y(i,j,nn)*rho_W(i,j+nn,k+subface2wcell_x(n))*W(i,j+nn,k+subface2wcell_x(n)) &
                         * V(i,j,k) * dt_uvw*dx(i)*dz
                 end do
                 ! Scalars fluxes computed with WENO in multiphase_scalar
              end do
           end if
        end do
     end do
  end do

  ! ===========================================================================
  ! z face
  ! ===========================================================================
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (maxval(abs(band(i,j,k-1:k))).le.nband_CFL) then ! compute fluxes

              ! Compute epsilon relative to cell volume
              volume_epsilon=vol(i,j,k)*1.0e-15_WP
              
              ! Points on face at z(k) ============================
              pt(:,1,3)=(/x (i  ), y (j  ), z(k) /)
              pt(:,2,3)=(/xm(i  ), y (j  ), z(k) /)
              pt(:,3,3)=(/x (i+1), y (j  ), z(k) /)
              pt(:,4,3)=(/x (i  ), ym(j  ), z(k) /)
              pt(:,5,3)=(/xm(i  ), ym(j  ), z(k) /)
              pt(:,6,3)=(/x (i+1), ym(j  ), z(k) /)
              pt(:,7,3)=(/x (i  ), y (j+1), z(k) /)
              pt(:,8,3)=(/xm(i  ), y (j+1), z(k) /)
              pt(:,9,3)=(/x (i+1), y (j+1), z(k) /)
              
              ! Project points to create flux volume
              do n=1,9
                 pt(:,9+n,3)=project(pt(:,n,3),-dt_uvw,i,j,k)
              end do
              
              ! Create 5 tets for each subface flux volume
              do n=1,4 ! loop over subfaces
                 do nn=1,5 ! loop over tets
                    do nnn=1,4 ! loop over points on tets
                       tet_vert(:,nnn,nn,n,3)=pt(:,pts2tets(n,nn,nnn),3)
                    end do
                 end do
              end do
              
              ! Make flux divergence free 
              ! Compute volume of tets not dependent on pt14 (center)
              fluxVol=0.0_WP
              do n=1,4 ! loop over subfaces
                 do nn=1,4 ! loop over tets (excluding 5th tet)
                    fluxVol=fluxVol+tet_vol(tet_vert(:,:,nn,n,3))
                 end do
              end do
              ! Adjust z-coordinate of point 14 to ensure divg-free flux
              pt(:,14,3)=adjust_fluxVol(3,pt(:,5,3),pt(:,11,3),pt(:,13,3),pt(:,14,3),pt(:,15,3),pt(:,17,3), &
                   W(i,j,k)*dt_uvw*dx(i)*dy(j)-fluxVol)
              ! Save point (needed to form xm and ym fluxes)
              pt_zface(i,j,k)=pt(3,14,3)-z(k)
              ! Update tets that depend on point 14
              do n=1,4 ! loop over subfaces
                 tet_vert(:,4,5,n,3)=pt(:,14,3)
              end  do
              
              ! Compute indices of all points
              do n=1,18
                 k_flux=k-int(0.5_WP*(sign(1.0_WP,W(i,j,k))+1.0_WP))
                 ind(:,n,3)=get_indices(pt(:,n,3),(/i,j,k_flux/))
              end do
              
              ! Create tet indices
              do n=1,4 ! loop over subfaces
                 do nn=1,5 ! loop over tets
                    do nnn=1,4 ! loop over points on tets
                       tet_ind(:,nnn,nn,n,3)=ind(:,pts2tets(n,nn,nnn),3)
                    end do
                 end do
              end do

              ! Compute fluxes 
              do n=1,4 ! loop over subfaces
                 do nn=1,5 ! loop over tets
                    fluxes(:)=+tetsign(tet_vert(:,:,nn,n,3))*tet2flux(tet_vert(:,:,nn,n,3),tet_ind(:,:,nn,n,3))
                    ! Transfer to semi-Lagrangian flux arrays
                    SLF_volz(subface2subcell_z(n),i,j,k)=SLF_volz(subface2subcell_z(n),i,j,k)+fluxes(1)
                    SLF_liqz(subface2subcell_z(n),i,j,k)=SLF_liqz(subface2subcell_z(n),i,j,k)+fluxes(2)
                    SLF_gasz(subface2subcell_z(n),i,j,k)=SLF_gasz(subface2subcell_z(n),i,j,k)+fluxes(3)
                    SLF_rhoUz(i+subface2ucell_z(n),j,k)=SLF_rhoUz(i+subface2ucell_z(n),j,k)+fluxes(4)
                    SLF_rhoVz(i,j+subface2vcell_z(n),k)=SLF_rhoVz(i,j+subface2vcell_z(n),k)+fluxes(5)
                    ! SLF_Wz internal at z(k)
                    do nnn=1,nscalar
                       SLF_SCz(i,j,k,nnn)=SLF_SCz(i,j,k,nnn)+fluxes(6+nnn)
                    end do
                 end do
              end do

              ! Set cmask
              cmask_z(subface2subcell_z(:),i,j,k)=0
              
           else ! Finite difference fluxes
              fluxVol=0.25_WP*W(i,j,k)*dt_uvw*dx(i)*dy(j)
              SLF_volz(subface2subcell_z(:),i,j,k)=fluxVol
              if (sum(VOFavg(i,j,k-1:k)).lt.1.0_WP) then ! Gas
                 SLF_liqz(subface2subcell_z(:),i,j,k)=0.0_WP
                 SLF_gasz(subface2subcell_z(:),i,j,k)=fluxVol
              else ! Liquid
                 SLF_liqz(subface2subcell_z(:),i,j,k)=fluxVol
                 SLF_gasz(subface2subcell_z(:),i,j,k)=0.0_WP
              end if
              do n=1,4
                 do nn=-stc2,+stc1
                    SLF_rhoUz(i+subface2ucell_z(n),j,k)=SLF_rhoUz(i+subface2ucell_z(n),j,k)+0.25_WP &
                         * interp_Ju_z(i,j,nn)*rho_U(i+subface2ucell_z(n),j,k+nn)*U(i+subface2ucell_z(n),j,k+nn) &
                         * W(i,j,k) * dt_uvw*dx(i)*dy(j)
                    SLF_rhoVz(i,j+subface2vcell_z(n),k)=SLF_rhoVz(i,j+subface2vcell_z(n),k)+0.25_WP &
                         * interp_Jv_z(i,j,nn)*rho_V(i,j+subface2vcell_z(n),k+nn)*V(i,j+subface2vcell_z(n),k+nn) &
                         * W(i,j,k) * dt_uvw*dx(i)*dy(j)
                    ! SLF_rhoWz internal at z(k)
                 end do
                 ! Scalars fluxes computed with WENO in multiphase_scalar
              end do
             
           end if
        end do
     end do
  end do

  ! Communicate fluxes used for correction tets
  call communication_border_4d(SLF_volx,8,nx_+2,ny_+2,nz_+2,1)
  call communication_border_4d(SLF_voly,8,nx_+2,ny_+2,nz_+2,1)
  call communication_border_4d(SLF_volz,8,nx_+2,ny_+2,nz_+2,1)
  
  ! Communicate points of flux volumes shared with internal face fluxes
  call communication_border(pt_xface)
  call communication_border(pt_yface)
  call communication_border(pt_zface)

  ! Communicate masks
  call communication_border_4d_int(cmask_x,8,nx_+2,ny_+2,nz_+2,1)
  call communication_border_4d_int(cmask_y,8,nx_+2,ny_+2,nz_+2,1)
  call communication_border_4d_int(cmask_z,8,nx_+2,ny_+2,nz_+2,1)

  ! ===========================================================================
  ! Internal faces
  ! ===========================================================================
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_

           if (abs(band(i,j,k)).le.nband_CFL) then ! semi-Lagrangian fluxes

              ! Loop over xm,ym,zm faces
              do dir=1,3

                 ! Points on face
                 select case(dir)
                 case (1) ! xm face
                    pt(:,10,dir)=(/xm(i), y (j  ), z (k  ) /)
                    pt(:,11,dir)=(/xm(i), ym(j  ), z (k  ) /)
                    pt(:,12,dir)=(/xm(i), y (j+1), z (k  ) /)
                    pt(:,13,dir)=(/xm(i), y (j  ), zm(k  ) /)
                    pt(:,14,dir)=(/xm(i), ym(j  ), zm(k  ) /)
                    pt(:,15,dir)=(/xm(i), y (j+1), zm(k  ) /)
                    pt(:,16,dir)=(/xm(i), y (j  ), z (k+1) /)
                    pt(:,17,dir)=(/xm(i), ym(j  ), z (k+1) /)
                    pt(:,18,dir)=(/xm(i), y (j+1), z (k+1) /)
                 case (2) ! ym face
                    pt(:,10,dir)=(/x (i  ), ym(j), z (k  ) /)
                    pt(:,11,dir)=(/xm(i  ), ym(j), z (k  ) /)
                    pt(:,12,dir)=(/x (i+1), ym(j), z (k  ) /)
                    pt(:,13,dir)=(/x (i  ), ym(j), zm(k  ) /)
                    pt(:,14,dir)=(/xm(i  ), ym(j), zm(k  ) /)
                    pt(:,15,dir)=(/x (i+1), ym(j), zm(k  ) /)
                    pt(:,16,dir)=(/x (i  ), ym(j), z (k+1) /)
                    pt(:,17,dir)=(/xm(i  ), ym(j), z (k+1) /)
                    pt(:,18,dir)=(/x (i+1), ym(j), z (k+1) /)
                 case (3) ! zm face
                    pt(:,10,dir)=(/x (i  ), y (j  ), zm(k) /)
                    pt(:,11,dir)=(/xm(i  ), y (j  ), zm(k) /)
                    pt(:,12,dir)=(/x (i+1), y (j  ), zm(k) /)
                    pt(:,13,dir)=(/x (i  ), ym(j  ), zm(k) /)
                    pt(:,14,dir)=(/xm(i  ), ym(j  ), zm(k) /)
                    pt(:,15,dir)=(/x (i+1), ym(j  ), zm(k) /)
                    pt(:,16,dir)=(/x (i  ), y (j+1), zm(k) /)
                    pt(:,17,dir)=(/xm(i  ), y (j+1), zm(k) /)
                    pt(:,18,dir)=(/x (i+1), y (j+1), zm(k) /)
                 end select

                 ! Project points to create flux volume
                 do n=1,9
                    pt(:,n,dir)=project(pt(:,9+n,dir),-dt_uvw,i,j,k)
                 end do

                 ! Update projected points to match neighbor fluxes
                 select case(dir)
                 case (1) ! xm face
                    if (minval(cmask_z(subface2subcell_z(:),i,j,k  )).eq.0) pt(3,2,dir)=pt_zface(i,j,k  )+z(k  )
                    if (minval(cmask_y(subface2subcell_y(:),i,j  ,k)).eq.0) pt(2,4,dir)=pt_yface(i,j  ,k)+y(j  )
                    if (minval(cmask_y(subface2subcell_y(:),i,j+1,k)).eq.0) pt(2,6,dir)=pt_yface(i,j+1,k)+y(j+1)
                    if (minval(cmask_z(subface2subcell_z(:),i,j,k+1)).eq.0) pt(3,8,dir)=pt_zface(i,j,k+1)+z(k+1)
                 case (2) ! ym face
                    if (minval(cmask_z(subface2subcell_z(:),i,j,k  )).eq.0) pt(3,2,dir)=pt_zface(i,j,k  )+z(k  )
                    if (minval(cmask_x(subface2subcell_x(:),i  ,j,k)).eq.0) pt(1,4,dir)=pt_xface(i  ,j,k)+x(i  )
                    if (minval(cmask_x(subface2subcell_x(:),i+1,j,k)).eq.0) pt(1,6,dir)=pt_xface(i+1,j,k)+x(i+1)
                    if (minval(cmask_z(subface2subcell_z(:),i,j,k+1)).eq.0) pt(3,8,dir)=pt_zface(i,j,k+1)+z(k+1)
                 case (3) ! zm face
                    if (minval(cmask_y(subface2subcell_y(:),i,j  ,k)).eq.0) pt(2,2,dir)=pt_yface(i,j  ,k)+y(j  )
                    if (minval(cmask_x(subface2subcell_x(:),i  ,j,k)).eq.0) pt(1,4,dir)=pt_xface(i  ,j,k)+x(i  )
                    if (minval(cmask_x(subface2subcell_x(:),i+1,j,k)).eq.0) pt(1,6,dir)=pt_xface(i+1,j,k)+x(i+1)
                    if (minval(cmask_y(subface2subcell_y(:),i,j+1,k)).eq.0) pt(2,8,dir)=pt_yface(i,j+1,k)+y(j+1)
                 end select
                 
                 ! Compute indices of all points
                 do n=1,18
                    ind(:,n,dir)=get_indices(pt(:,n,dir),(/i,j,k/))
                 end do
              
                 ! Create 5 tets for each subface flux volume
                 do n=1,4 ! loop over subfaces
                    do nn=1,5 ! loop over tets
                       do nnn=1,4 ! loop over points on tets
                          tet_vert(:,nnn,nn,n,dir)=pt(:,pts2tets(n,nn,nnn),dir)
                          tet_ind(:,nnn,nn,n,dir)=ind(:,pts2tets(n,nn,nnn),dir)
                       end do
                    end do
                 end do

                 ! Compute uncorrected flux volume (store in SLF_vol arrays)
                 do n=1,4 ! loop over subfaces
                    fluxVol=0.0_WP
                    do nn=1,5 ! loop over tets
                       fluxVol=fluxVol-dirSign(dir)*tet_vol(tet_vert(:,:,nn,n,dir))
                    end do
                    select case(dir)
                    case (1); SLF_volx(subface2subcell_xm(n),i,j,k)=fluxVol
                    case (2); SLF_voly(subface2subcell_ym(n),i,j,k)=fluxVol
                    case (3); SLF_volz(subface2subcell_zm(n),i,j,k)=fluxVol
                    end select
                 end do

              end do

              ! Add correction tets to flux volume
              call correction_tets(i,j,k,pt,ind,tet_vert,tet_ind)
              
              ! Zero SLF_vol arrays
              SLF_volx(subface2subcell_xm(:),i,j,k)=0.0_WP
              SLF_voly(subface2subcell_ym(:),i,j,k)=0.0_WP
              SLF_volz(subface2subcell_zm(:),i,j,k)=0.0_WP

              ! Compute fluxes
              volume_epsilon=vol(i,j,k)*1.0e-15_WP
              ! xm face
              do n=1,4 ! loop over subfaces
                 do nn=1,7 ! loop over tets (5 original + 2 correction)
                    fluxes(:)=-tetsign(tet_vert(:,:,nn,n,1))*tet2flux(tet_vert(:,:,nn,n,1),tet_ind(:,:,nn,n,1))
                    ! Transfer to semi-Lagrangian flux arrays
                    SLF_volx(subface2subcell_xm(n),i,j,k)=SLF_volx(subface2subcell_xm(n),i,j,k)+fluxes(1)
                    SLF_liqx(subface2subcell_xm(n),i,j,k)=SLF_liqx(subface2subcell_xm(n),i,j,k)+fluxes(2)
                    SLF_gasx(subface2subcell_xm(n),i,j,k)=SLF_gasx(subface2subcell_xm(n),i,j,k)+fluxes(3)
                    SLF_rhoUx(i+subface2ucell_xm(n),j,k)=SLF_rhoUx(i+subface2ucell_xm(n),j,k)+fluxes(4)
                    ! SLF_rhoVx  internal at xm(i)
                    ! SLF_rhoWx  internal at xm(i)
                    ! SLF_SCx internal at xm(i)
                 end do
              end do
              
              ! ym face
              do n=1,4 ! loop over subfaces
                 do nn=1,7 ! loop over tets
                    fluxes(:)=+tetsign(tet_vert(:,:,nn,n,2))*tet2flux(tet_vert(:,:,nn,n,2),tet_ind(:,:,nn,n,2))
                    ! Transfer to semi-Lagrangian flux arrays
                    SLF_voly(subface2subcell_ym(n),i,j,k)=SLF_voly(subface2subcell_ym(n),i,j,k)+fluxes(1)
                    SLF_liqy(subface2subcell_ym(n),i,j,k)=SLF_liqy(subface2subcell_ym(n),i,j,k)+fluxes(2)
                    SLF_gasy(subface2subcell_ym(n),i,j,k)=SLF_gasy(subface2subcell_ym(n),i,j,k)+fluxes(3)
                    ! SLF_rhoUy  internal at ym(j)
                    SLF_rhoVy(i,j+subface2vcell_ym(n),k)=SLF_rhoVy(i,j+subface2vcell_ym(n),k)+fluxes(5)
                    ! SLF_rhoWy  internal at ym(j)
                    ! SLF_SCy internal at ym(j)
                 end do
              end do
              
              ! zm face
              do n=1,4 ! loop over subfaces
                 do nn=1,7 ! loop over tets
                    fluxes(:)=-tetsign(tet_vert(:,:,nn,n,3))*tet2flux(tet_vert(:,:,nn,n,3),tet_ind(:,:,nn,n,3))
                    ! Transfer to semi-Lagrangian flux arrays
                    SLF_volz(subface2subcell_zm(n),i,j,k)=SLF_volz(subface2subcell_zm(n),i,j,k)+fluxes(1)
                    SLF_liqz(subface2subcell_zm(n),i,j,k)=SLF_liqz(subface2subcell_zm(n),i,j,k)+fluxes(2)
                    SLF_gasz(subface2subcell_zm(n),i,j,k)=SLF_gasz(subface2subcell_zm(n),i,j,k)+fluxes(3)
                    ! SLF_rhoUz internal at zm(k)
                    ! SLF_rhoVz internal at zm(k)
                    SLF_rhoWz(i,j,k+subface2wcell_zm(n))=SLF_rhoWz(i,j,k+subface2wcell_zm(n))+fluxes(6)
                    ! SLF_SCz internal at zm(k)
                 end do
              end do

              ! Update cmasks
              cmask_x(subface2subcell_xm(:),i,j,k)=0
              cmask_y(subface2subcell_ym(:),i,j,k)=0
              cmask_z(subface2subcell_zm(:),i,j,k)=0
           
           else ! Finite difference fluxes
              ! Compute  for this cell
              if (0.125_WP*sum(VOF(:,i,j,k)).lt.0.5_WP) then ! gas
                 ! xm face
                 fluxVol=sum(interp_Ju_xm(i,j,:)*U(i-stc1:i+stc2,j,k)) * dt_uvw*dy(j)*dz
                 SLF_volx(subface2subcell_xm(:),i,j,k)=0.25_WP*fluxVol
                 SLF_liqx(subface2subcell_xm(:),i,j,k)=0.0_WP
                 SLF_gasx(subface2subcell_xm(:),i,j,k)=0.25_WP*fluxVol
                 SLF_rhoUx(i,j,k) = fluxvol*rho_g*sum(interp_Ju_xm(i,j,:)*U(i-stc1:i+stc2,j,k))
                 ! ym face
                 fluxVol=sum(interp_Jv_ym(i,j,:)*V(i,j-stc1:j+stc2,k)) * dt_uvw*dx(i)*dz
                 SLF_voly(subface2subcell_ym(:),i,j,k)=0.25_WP*fluxVol
                 SLF_liqy(subface2subcell_ym(:),i,j,k)=0.0_WP
                 SLF_gasy(subface2subcell_ym(:),i,j,k)=0.25_WP*fluxVol
                 SLF_rhoVy(i,j,k) = fluxvol*rho_g*sum(interp_Jv_ym(i,j,:)*V(i,j-stc1:j+stc2,k))
                 ! zm face
                 fluxVol=sum(interp_Jw_zm(i,j,:)*W(i,j,k-stc1:k+stc2)) * dt_uvw*dx(i)*dy(j)
                 SLF_volz(subface2subcell_zm(:),i,j,k)=0.25_WP*fluxVol
                 SLF_liqz(subface2subcell_zm(:),i,j,k)=0.0_WP
                 SLF_gasz(subface2subcell_zm(:),i,j,k)=0.25_WP*fluxVol
                 SLF_rhoWz(i,j,k) = fluxvol*rho_g*sum(interp_Jw_zm(i,j,:)*W(i,j,k-stc1:k+stc2))
              else
                 ! xm face
                 fluxVol=sum(interp_Ju_xm(i,j,:)*U(i-stc1:i+stc2,j,k)) * dt_uvw*dy(j)*dz
                 SLF_volx(subface2subcell_xm(:),i,j,k)=0.25_WP*fluxVol
                 SLF_liqx(subface2subcell_xm(:),i,j,k)=0.25_WP*fluxVol
                 SLF_gasx(subface2subcell_xm(:),i,j,k)=0.0_WP
                 SLF_rhoUx(i,j,k) = fluxvol*rho_l*sum(interp_Ju_xm(i,j,:)*U(i-stc1:i+stc2,j,k))
                 ! ym face
                 fluxVol=sum(interp_Jv_ym(i,j,:)*V(i,j-stc1:j+stc2,k)) * dt_uvw*dx(i)*dz
                 SLF_voly(subface2subcell_ym(:),i,j,k)=0.25_WP*fluxVol
                 SLF_liqy(subface2subcell_ym(:),i,j,k)=0.25_WP*fluxVol
                 SLF_gasy(subface2subcell_ym(:),i,j,k)=0.0_WP
                 SLF_rhoVy(i,j,k) = fluxvol*rho_l*sum(interp_Jv_ym(i,j,:)*V(i,j-stc1:j+stc2,k))
                 ! zm face
                 fluxVol=sum(interp_Jw_zm(i,j,:)*W(i,j,k-stc1:k+stc2)) * dt_uvw*dx(i)*dy(j)
                 SLF_volz(subface2subcell_zm(:),i,j,k)=0.25_WP*fluxVol
                 SLF_liqz(subface2subcell_zm(:),i,j,k)=0.25_WP*fluxVol
                 SLF_gasz(subface2subcell_zm(:),i,j,k)=0.0_WP
                 SLF_rhoWz(i,j,k) = fluxvol*rho_l*sum(interp_Jw_zm(i,j,:)*W(i,j,k-stc1:k+stc2))
              end if
           end if
        end do
     end do
  end do
 
  ! Communicate fluxes
  call communication_border_4d(SLF_volx,8,nx_+2,ny_+2,nz_+2,1)
  call communication_border_4d(SLF_voly,8,nx_+2,ny_+2,nz_+2,1)
  call communication_border_4d(SLF_volz,8,nx_+2,ny_+2,nz_+2,1)
  
  call communication_border_4d(SLF_liqx,8,nx_+2,ny_+2,nz_+2,1)
  call communication_border_4d(SLF_liqy,8,nx_+2,ny_+2,nz_+2,1)
  call communication_border_4d(SLF_liqz,8,nx_+2,ny_+2,nz_+2,1)

  call communication_border_4d(SLF_gasx,8,nx_+2,ny_+2,nz_+2,1)
  call communication_border_4d(SLF_gasy,8,nx_+2,ny_+2,nz_+2,1)
  call communication_border_4d(SLF_gasz,8,nx_+2,ny_+2,nz_+2,1)

  call summation_border_x(SLF_rhoUy,nx_+2,ny_+2,nz_+2,1)
  call summation_border_x(SLF_rhoUz,nx_+2,ny_+2,nz_+2,1)
  call communication_border_adv(SLF_rhoUx,nx_+2,ny_+2,nz_+2,1)
  call communication_border_adv(SLF_rhoUy,nx_+2,ny_+2,nz_+2,1)
  call communication_border_adv(SLF_rhoUz,nx_+2,ny_+2,nz_+2,1)

  call summation_border_y(SLF_rhoVx,nx_+2,ny_+2,nz_+2,1)
  call summation_border_y(SLF_rhoVz,nx_+2,ny_+2,nz_+2,1)
  call communication_border_adv(SLF_rhoVx,nx_+2,ny_+2,nz_+2,1)
  call communication_border_adv(SLF_rhoVy,nx_+2,ny_+2,nz_+2,1)
  call communication_border_adv(SLF_rhoVz,nx_+2,ny_+2,nz_+2,1)

  call summation_border_z(SLF_rhoWx,nx_+2,ny_+2,nz_+2,1)
  call summation_border_z(SLF_rhoWy,nx_+2,ny_+2,nz_+2,1)
  call communication_border_adv(SLF_rhoWx,nx_+2,ny_+2,nz_+2,1)
  call communication_border_adv(SLF_rhoWy,nx_+2,ny_+2,nz_+2,1)
  call communication_border_adv(SLF_rhoWz,nx_+2,ny_+2,nz_+2,1)

  do n=1,nscalar
     call communication_border_adv(SLF_SCx(:,:,:,n),nx_+2,ny_+2,nz_+2,1)
     call communication_border_adv(SLF_SCy(:,:,:,n),nx_+2,ny_+2,nz_+2,1)
     call communication_border_adv(SLF_SCz(:,:,:,n),nx_+2,ny_+2,nz_+2,1)
  end do

  call communication_border_4d_int(cmask_x,8,nx_+2,ny_+2,nz_+2,1)
  call communication_border_4d_int(cmask_y,8,nx_+2,ny_+2,nz_+2,1)
  call communication_border_4d_int(cmask_z,8,nx_+2,ny_+2,nz_+2,1)
  
  ! Compute divergence on subcells
  SCdiv=0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (minval(abs(band(i,j,k-1:k))).le.nband_CFL+1) then ! compute fluxes
              do s=1,8
                 SCdiv(s,i,j,k) = &
                      + (SLF_volx(s,i,j,k)-SLF_volx(sc_sx(s),i+sc_ip(s),j,k))*0.25_WP*dy(j)*dz    &
                      + (SLF_voly(s,i,j,k)-SLF_voly(sc_sy(s),i,j+sc_jp(s),k))*0.25_WP*dx(i)*dz    &
                      + (SLF_volz(s,i,j,k)-SLF_volz(sc_sz(s),i,j,k+sc_kp(s)))*0.25_WP*dx(i)*dy(j)
              end do
           end if
        end do
     end do
  end do
    
  return

contains
  ! ================================== !
  ! Function to compute additonal tets !
  ! needed to correct subface fluxes   !
  ! ================================== !
  subroutine correction_tets(i,j,k,pt,ind,tet_vert,tet_ind)
    implicit none
    integer, intent(in) :: i,j,k
    real(WP), dimension(3,22,3), intent(inout) :: pt
    integer,  dimension(3,22,3), intent(inout) :: ind
    real(WP), dimension(3,4,7,4,3), intent(inout) :: tet_vert
    integer,  dimension(3,4,7,4,3), intent(inout) :: tet_ind
    integer :: n,nn,nnn
    real(WP), dimension(4) :: Usub,Vsub,Wsub
    real(WP) :: vol

    ! Compute correction velocity
    call divgFree_vel(i,j,k,Usub,Vsub,Wsub)
   
    do dir=1,3 ! Loop over directions xm,ym,zm
       do n=1,4 ! Loop over subfaces
          
          ! Compute correction volume
          select case(dir)
          case (1); vol=0.25_WP*Usub(n)*dt_uvw*dy(j)*dz   -SLF_volx(subface2subcell_xm(n),i,j,k)
          case (2); vol=0.25_WP*Vsub(n)*dt_uvw*dx(i)*dz   -SLF_voly(subface2subcell_ym(n),i,j,k)
          case (3); vol=0.25_WP*Wsub(n)*dt_uvw*dx(i)*dy(j)-SLF_volz(subface2subcell_zm(n),i,j,k)
          end select          
         
          ! Compute additional vertex of correction tets
          pt(:,18+n,dir)=adjust_subfluxvol(i,j,k,dir,pt(:,correction_pts(:,n),dir),vol*dirSign(dir))
          ind(:,18+n,dir)=get_indices(pt(:,18+n,dir),(/i,j,k/))
          
          ! Add correction tets
          do nn=6,7
             do nnn=1,4
                tet_vert(:,nnn,nn,n,dir)=pt(:,pts2tets(n,nn,nnn),dir)
                tet_ind(:,nnn,nn,n,dir)=ind(:,pts2tets(n,nn,nnn),dir)
             end do
          end do
          
       end do
    end do

    return 
  end subroutine correction_tets
  
  ! ================================= !
  ! Function to compute divg-free     !
  ! subface velocities (Cervone 2011) !
  ! ================================= !
  subroutine divgFree_vel(i,j,k,Usub,Vsub,Wsub)
    implicit none
    integer, intent(in) :: i,j,k
    real(WP), dimension(4), intent(out) :: Usub,Vsub,Wsub
    real(WP) :: dt_uvwi
    real(WP), dimension(36) :: b
    real(WP), dimension(12) :: uA
    
    ! Known subcell area weighted velocities
    dt_uvwi=1.0_WP/dt_uvw
    b( 1)=SLF_volx(1,i  ,j,k)*dt_uvwi
    b( 2)=SLF_volx(2,i  ,j,k)*dt_uvwi
    b( 3)=SLF_volx(3,i  ,j,k)*dt_uvwi
    b( 4)=SLF_volx(4,i  ,j,k)*dt_uvwi
    b( 5)=SLF_volx(5,i  ,j,k)*dt_uvwi
    b( 6)=SLF_volx(6,i  ,j,k)*dt_uvwi
    b( 7)=SLF_volx(7,i  ,j,k)*dt_uvwi
    b( 8)=SLF_volx(8,i  ,j,k)*dt_uvwi
    b( 9)=SLF_volx(1,i+1,j,k)*dt_uvwi
    b(10)=SLF_volx(3,i+1,j,k)*dt_uvwi
    b(11)=SLF_volx(5,i+1,j,k)*dt_uvwi
    b(12)=SLF_volx(7,i+1,j,k)*dt_uvwi
    b(13)=SLF_voly(1,i,j  ,k)*dt_uvwi
    b(14)=SLF_voly(2,i,j  ,k)*dt_uvwi
    b(15)=SLF_voly(3,i,j  ,k)*dt_uvwi
    b(16)=SLF_voly(4,i,j  ,k)*dt_uvwi
    b(17)=SLF_voly(5,i,j  ,k)*dt_uvwi
    b(18)=SLF_voly(6,i,j  ,k)*dt_uvwi
    b(19)=SLF_voly(7,i,j  ,k)*dt_uvwi
    b(20)=SLF_voly(8,i,j  ,k)*dt_uvwi
    b(21)=SLF_voly(1,i,j+1,k)*dt_uvwi
    b(22)=SLF_voly(2,i,j+1,k)*dt_uvwi
    b(23)=SLF_voly(5,i,j+1,k)*dt_uvwi
    b(24)=SLF_voly(6,i,j+1,k)*dt_uvwi
    b(25)=SLF_volz(1,i,j,k  )*dt_uvwi
    b(26)=SLF_volz(2,i,j,k  )*dt_uvwi
    b(27)=SLF_volz(3,i,j,k  )*dt_uvwi
    b(28)=SLF_volz(4,i,j,k  )*dt_uvwi
    b(29)=SLF_volz(5,i,j,k  )*dt_uvwi
    b(30)=SLF_volz(6,i,j,k  )*dt_uvwi
    b(31)=SLF_volz(7,i,j,k  )*dt_uvwi
    b(32)=SLF_volz(8,i,j,k  )*dt_uvwi
    b(33)=SLF_volz(1,i,j,k+1)*dt_uvwi
    b(34)=SLF_volz(2,i,j,k+1)*dt_uvwi
    b(35)=SLF_volz(3,i,j,k+1)*dt_uvwi
    b(36)=SLF_volz(4,i,j,k+1)*dt_uvwi

    ! Compute divergence-free velocity*Areas
    uA=matmul(divgfree_M,b)
    
    ! Divide by sub-cell areas to get velocities
    Usub(1)=uA( 1)*4.0_WP*dyi(j)*dzi
    Usub(2)=uA( 2)*4.0_WP*dyi(j)*dzi
    Usub(3)=uA( 3)*4.0_WP*dyi(j)*dzi
    Usub(4)=uA( 4)*4.0_WP*dyi(j)*dzi
    Vsub(1)=uA( 5)*4.0_WP*dxi(i)*dzi
    Vsub(2)=uA( 6)*4.0_WP*dxi(i)*dzi
    Vsub(3)=uA( 7)*4.0_WP*dxi(i)*dzi
    Vsub(4)=uA( 8)*4.0_WP*dxi(i)*dzi
    Wsub(1)=uA( 9)*4.0_WP*dxi(i)*dyi(j)
    Wsub(2)=uA(10)*4.0_WP*dxi(i)*dyi(j)
    Wsub(3)=uA(11)*4.0_WP*dxi(i)*dyi(j)
    Wsub(4)=uA(12)*4.0_WP*dxi(i)*dyi(j)
    
  end subroutine divgFree_vel

  ! ========================= !
  ! Compute point needed for  !
  ! divg-free correction tets !
  ! ========================= !
  function adjust_subfluxvol(i,j,k,dir,pts,vol) result(Pt)
    implicit none
    integer, intent(in) :: i,j,k,dir
    real(WP), dimension(3,4), intent(in) :: pts
    real(WP), dimension(3) :: Pt
    real(WP), intent(in) :: vol
    real(WP), dimension(3,4) :: lpts
    real(WP), dimension(3) :: lPt,corPt

    ! Correction point initially average of other points
    Pt=0.25_WP*sum(pts(:,:),2)

    ! Move points to local coordinante system x_1,x_2,x_3 with x_1 normal to face
    select case(dir)
    case (1); lPt(2)=Pt(2); lPt(3)=Pt(3); lpts(1,:)=pts(1,:); lpts(2,:)=pts(2,:); lpts(3,:)=pts(3,:)
    case (2); lPt(2)=Pt(3); lPt(3)=Pt(1); lpts(1,:)=pts(2,:); lpts(2,:)=pts(3,:); lpts(3,:)=pts(1,:)
    case (3); lPt(2)=Pt(1); lPt(3)=Pt(2); lpts(1,:)=pts(3,:); lpts(2,:)=pts(1,:); lpts(3,:)=pts(2,:)
    end select
    
    ! Compute correction point's dir dimension
    corPt(dir)=-(6.0_WP*vol - (lpts(1,2)*(lpts(2,4) - lPt(2)) - lpts(1,4)*(lpts(2,2) - lPt(2)))*(lpts(3,1) - lPt(3)) + (lpts(1,2)*(lpts(3,4) - lPt(3)) - lpts(1,4)*(lpts(3,2) - lPt(3)))*(lpts(2,1) - lPt(2)) + (lpts(1,3)*(lpts(2,4) - lPt(2)) - lpts(1,4)*(lpts(2,3) - lPt(2)))*(lpts(3,1) - lPt(3)) - (lpts(1,3)*(lpts(3,4) - lPt(3)) - lpts(1,4)*(lpts(3,3) - lPt(3)))*(lpts(2,1) - lPt(2)) - lpts(1,1)*((lpts(2,2) - lPt(2))*(lpts(3,4) - lPt(3)) - (lpts(3,2) - lPt(3))*(lpts(2,4) - lPt(2))) + lpts(1,1)*((lpts(2,3) - lPt(2))*(lpts(3,4) - lPt(3)) - (lpts(3,3) - lPt(3))*(lpts(2,4) - lPt(2))))/((lpts(2,1) - lPt(2))*(lpts(3,2) - lpts(3,4)) - (lpts(2,2) - lpts(2,4))*(lpts(3,1) - lPt(3)) - (lpts(2,1) - lPt(2))*(lpts(3,3) - lpts(3,4)) + (lpts(3,1) - lPt(3))*(lpts(2,3) - lpts(2,4)) + (lpts(2,2) - lPt(2))*(lpts(3,4) - lPt(3)) - (lpts(3,2) - lPt(3))*(lpts(2,4) - lPt(2)) - (lpts(2,3) - lPt(2))*(lpts(3,4) - lPt(3)) + (lpts(3,3) - lPt(3))*(lpts(2,4) - lPt(2)))

    ! Limit correction to 1 meshsize (deals with issues when flux volume is significantly deformed and correction fails)
    if (abs(corPt(dir)-Pt(dir)).gt.meshsize(i,j,k)) then
       Pt(dir)=Pt(dir)+(corPt(dir)-Pt(dir))/(abs(corPt(dir)-Pt(dir)))*meshsize(i,j,k)
    else
       Pt(dir)=corPt(dir)
    end if
    
  end function adjust_subfluxvol

end subroutine multiphase_fluxes_calc



