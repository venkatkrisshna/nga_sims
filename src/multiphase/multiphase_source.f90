! ============================================== !
! Multiphase module for calculating source terms !
! ============================================== !
module multiphase_source
  use multiphase
  use multiphase_tools
  use bodyforce
  implicit none
  ! Dummy
end module multiphase_source


! =================================== !
! Gravitational force                 !
! Careful - only 1st order in time... !
! =================================== !
subroutine multiphase_gravity
  use multiphase_source
  use velocity
  use masks
  implicit none
  
  integer :: i,j,k
  
  ! Calculate gravitational force
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (mask_u(i,j).eq.0) srcUmid(i,j,k) = srcUmid(i,j,k)+dt_uvw*rho_Uold(i,j,k)*gravity(1)
           if (mask_v(i,j).eq.0) srcVmid(i,j,k) = srcVmid(i,j,k)+dt_uvw*rho_Vold(i,j,k)*gravity(2)
           if (mask_w(i,j).eq.0) srcWmid(i,j,k) = srcWmid(i,j,k)+dt_uvw*rho_Wold(i,j,k)*gravity(3)
        end do
     end do
  end do
  
  return
end subroutine multiphase_gravity

! =================================== !
!  Rotating frame of reference        !
!   - Coriolis and Centrifugal forces !
! =================================== !
subroutine multiphase_rotating
  use multiphase_source
  use velocity
  use masks
  implicit none
  
  integer :: i,j,k
  real(WP), dimension(3) :: coriolis, centrifugal
  real(WP), dimension(3) :: pos,vel,rad

  ! Calculate fictitious forces 
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! x-face 
           pos=(/ x (i), ym(j), zm(k) /)     ! Position of face
           rad=pos-rot_xo                    ! Radial vector from rot_xo to pos
           vel=get_velocity(pos,i,j,k)       ! Velocity at face
           ! Forces
           coriolis   (1)=- 2.0_WP*(rot_omega(2)*vel(3)-rot_omega(3)*vel(2))
           centrifugal(1)=- rot_omega(2)*(+rot_omega(1)*rad(2)-rot_omega(2)*rad(1)) &
                          + rot_omega(3)*(-rot_omega(1)*rad(3)+rot_omega(3)*rad(1))

           ! y-face 
           pos=(/ xm(i), y (j), zm(k) /)     ! Position of face
           rad=pos-rot_xo                    ! Radial vector from rot_xo to pos
           vel=get_velocity(pos,i,j,k)       ! Velocity at face
           ! Forces
           coriolis   (2)=+ 2.0_WP*(rot_omega(1)*vel(3)-rot_omega(3)*vel(1))
           centrifugal(2)=+ rot_omega(1)*(+rot_omega(1)*rad(2)-rot_omega(2)*rad(1)) &
                          - rot_omega(3)*(+rot_omega(2)*rad(3)-rot_omega(3)*rad(2))

           ! z-face 
           pos=(/ xm(i), ym(j), z (k) /)     ! Position of face
           rad=pos-rot_xo                    ! Radial vector from rot_xo to pos
           vel=get_velocity(pos,i,j,k)       ! Velocity at face
           ! Forces
           coriolis   (3)=- 2.0_WP*(rot_omega(1)*vel(2)-rot_omega(2)*vel(1))
           centrifugal(3)=- rot_omega(1)*(-rot_omega(1)*rad(3)+rot_omega(3)*rad(1)) &
                          + rot_omega(2)*(+rot_omega(2)*rad(3)-rot_omega(3)*rad(2))

           ! Add forces to source terms
           if (mask_u(i,j).eq.0) srcUmid(i,j,k) = srcUmid(i,j,k)+dt_uvw*rho_Uold(i,j,k)*(coriolis(1)+centrifugal(1))
           if (mask_v(i,j).eq.0) srcVmid(i,j,k) = srcVmid(i,j,k)+dt_uvw*rho_Vold(i,j,k)*(coriolis(2)+centrifugal(2))
           if (mask_w(i,j).eq.0) srcWmid(i,j,k) = srcWmid(i,j,k)+dt_uvw*rho_Wold(i,j,k)*(coriolis(3)+centrifugal(3))
        end do
     end do
  end do
  
  return
end subroutine multiphase_rotating


! ============================= !
! Immersed boundary source term !
! ============================= !
subroutine multiphase_ibsource
  use multiphase_source
  use ib
  use masks
  use velocity
  use interpolate
  use memory
  implicit none
  
  integer :: i,j,k
  real(WP) :: norm,my_vol,my_visc
  real(WP) :: my_rho,velnorm,theta
  real(WP), dimension(1:3) :: mynorm,vel,velib,locnorm
  real(WP) :: int_srcP,my_int
  
  ! If not used, return
  if (.not.use_ib) return
  
  ! Start the timer
  call timing_start('ib')
  
  ! Create Gib gradient
  call gradient_vector(Gib,tmp1,tmp2,tmp3)
  
  ! Create source term for pressure and convection first
  if (ibmove) then
     
     ! Zero integral of pressure source
     my_int=0.0_WP
     
     ! Loop over inner domain
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              
              ! Pressure cell
              if (SAp(i,j,k).gt.0.0_WP) then
                 
                 ! Get normal IB velocity
                 call ib_get_velocity(i,j,k,vel(1),vel(2),vel(3))
                 locnorm(1)=Nxib(i,j,k)
                 locnorm(2)=Nyib(i,j,k)
                 locnorm(3)=Nzib(i,j,k)
                 velnorm=dot_product(vel,locnorm)
                 
                 ! Get source term - no density here
                 my_vol=VFp(i,j,k)*dx(i)*dy(j)*dz
                 srcPmid(i,j,k)=srcPmid(i,j,k)+dt*SAp(i,j,k)*velnorm/(my_vol+epsilon(1.0_WP))
                 
                 ! Integrate source term to ensure zero integral
                 my_int=my_int+srcPmid(i,j,k)*my_vol
                 
              end if
              
              ! U cell
              if (SAu(i,j,k).gt.0.0_WP .and. mask_u(i,j).eq.0) then
                 
                 ! Create face normals
                 mynorm(1)=sum(     grad_x(i,j,:)*Gib (i-st2:i+st1,j,k))
                 mynorm(2)=sum(interp_sc_x(i,j,:)*tmp2(i-st2:i+st1,j,k))
                 mynorm(3)=sum(interp_sc_x(i,j,:)*tmp3(i-st2:i+st1,j,k))
                 norm=sqrt(mynorm(1)**2+mynorm(2)**2+mynorm(3)**2)+epsilon(1.0_WP)
                 mynorm=mynorm/norm
                 
                 ! Get normal IB velocity
                 call ib_get_velocity(i,j,k,vel(1),vel(2),vel(3))
                 velnorm=dot_product(vel,mynorm)
                 
                 ! Get source term
                 my_vol=VFu(i,j,k)*dxm(i-1)*dy(j)*dz
                 my_rho=rho_U(i,j,k)
                 srcUmid(i,j,k)=srcUmid(i,j,k)+dt_uvw*my_rho*SAu(i,j,k)*velnorm*vel(1)/(my_vol+epsilon(1.0_WP))
                 
              end if
              
              ! V cell
              if (SAv(i,j,k).gt.0.0_WP .and. mask_v(i,j).eq.0) then
                 
                 ! Create face normals
                 mynorm(1)=sum(interp_sc_y(i,j,:)*tmp1(i,j-st2:j+st1,k))
                 mynorm(2)=sum(     grad_y(i,j,:)*Gib (i,j-st2:j+st1,k))
                 mynorm(3)=sum(interp_sc_y(i,j,:)*tmp3(i,j-st2:j+st1,k))
                 norm=sqrt(mynorm(1)**2+mynorm(2)**2+mynorm(3)**2)+epsilon(1.0_WP)
                 mynorm=mynorm/norm
                 
                 ! Get normal IB velocity
                 call ib_get_velocity(i,j,k,vel(1),vel(2),vel(3))
                 velnorm=dot_product(vel,mynorm)
                 
                 ! Get source term
                 my_vol=VFv(i,j,k)*dx(i)*dym(j-1)*dz
                 my_rho=rho_V(i,j,k)
                 srcVmid(i,j,k)=srcVmid(i,j,k)+dt_uvw*my_rho*SAv(i,j,k)*velnorm*vel(2)/(my_vol+epsilon(1.0_WP))
                 
              end if
              
              ! W cell
              if (SAw(i,j,k).gt.0.0_WP .and. mask_w(i,j).eq.0) then
                 
                 ! Create face normals
                 mynorm(1)=sum(interp_sc_z(i,j,:)*tmp1(i,j,k-st2:k+st1))
                 mynorm(2)=sum(interp_sc_z(i,j,:)*tmp2(i,j,k-st2:k+st1))
                 mynorm(3)=sum(     grad_z(i,j,:)*Gib (i,j,k-st2:k+st1))
                 norm=sqrt(mynorm(1)**2+mynorm(2)**2+mynorm(3)**2)+epsilon(1.0_WP)
                 mynorm=mynorm/norm
                 
                 ! Get normal IB velocity
                 call ib_get_velocity(i,j,k,vel(1),vel(2),vel(3))
                 velnorm=dot_product(vel,mynorm)
                 
                 ! Get source term
                 my_vol=VFw(i,j,k)*dx(i)*dy(j)*dz
                 my_rho=rho_W(i,j,k)
                 srcWmid(i,j,k)=srcWmid(i,j,k)+dt_uvw*my_rho*SAw(i,j,k)*velnorm*vel(3)/(my_vol+epsilon(1.0_WP))
                 
              end if
              
           end do
        end do
     end do
     
     ! Remove integral of pressure source
     call parallel_sum(my_int,int_srcP)
     int_srcP=int_srcP/vol_total
     srcPmid=srcPmid-int_srcP
     
  end if
  
  ! Zero source terms
  VFx=0.0_WP
  VFy=0.0_WP
  VFz=0.0_WP
  
  ! Create cell-centered velocities
  call interpolate_velocities
  
  ! Create source terms for momentum
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           
           ! U momentum cell
           if (SAu(i,j,k).gt.0.0_WP.and.VFu(i,j,k).gt.0.0_WP) then
              call ib_get_velocity(i,j,k,velib(1),velib(2),velib(3))
              theta=hf2(G(i-1,j,k),G(i,j,k))
              my_visc=mu_g*mu_l/(mu_g*theta+mu_l*(1.0_WP-theta)+epsilon(1.0_WP))
              VFx(i,j,k)=my_visc*SAu(i,j,k)*(U(i,j,k)-velib(1))/VHu(i,j,k)
           end if
           
           ! V momentum cell
           if (SAv(i,j,k).gt.0.0_WP.and.VFv(i,j,k).gt.0.0_WP) then
              call ib_get_velocity(i,j,k,velib(1),velib(2),velib(3))
              theta=hf2(G(i,j-1,k),G(i,j,k))
              my_visc=mu_g*mu_l/(mu_g*theta+mu_l*(1.0_WP-theta)+epsilon(1.0_WP))
              VFy(i,j,k)=my_visc*SAv(i,j,k)*(V(i,j,k)-velib(2))/VHv(i,j,k)
           end if
           
           ! W momentum cell
           if (SAw(i,j,k).gt.0.0_WP.and.VFw(i,j,k).gt.0.0_WP) then
              call ib_get_velocity(i,j,k,velib(1),velib(2),velib(3))
              theta=hf2(G(i,j,k-1),G(i,j,k))
              my_visc=mu_g*mu_l/(mu_g*theta+mu_l*(1.0_WP-theta)+epsilon(1.0_WP))
              VFz(i,j,k)=my_visc*SAw(i,j,k)*(W(i,j,k)-velib(3))/VHw(i,j,k)
           end if
           
        end do
     end do
  end do
  
  ! Add to source
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! U source
           my_vol=VFu(i,j,k)*dxm(i-1)*dy(j)*dz
           if (mask_u(i,j).eq.0.and.my_vol.gt.0.0_WP) srcUmid(i,j,k)=srcUmid(i,j,k)-dt_uvw*VFx(i,j,k)/my_vol
           ! V source
           my_vol=VFv(i,j,k)*dx(i)*dym(j-1)*dz
           if (mask_v(i,j).eq.0.and.my_vol.gt.0.0_WP) srcVmid(i,j,k)=srcVmid(i,j,k)-dt_uvw*VFy(i,j,k)/my_vol
           ! W source
           my_vol=VFw(i,j,k)*dx(i)*dy(j)*dz
           if (mask_w(i,j).eq.0.and.my_vol.gt.0.0_WP) srcWmid(i,j,k)=srcWmid(i,j,k)-dt_uvw*VFz(i,j,k)/my_vol
        end do
     end do
  end do
  
  ! Stop the timer
  call timing_stop('ib')
  
  return
end subroutine multiphase_ibsource
