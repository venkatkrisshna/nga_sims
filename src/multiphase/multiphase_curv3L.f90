module multiphase_curv3L
  use multiphase
  implicit none

  integer, dimension(:,:,:), allocatable :: npts3L
  real(WP), dimension(:,:,:,:,:), allocatable :: pts3L
  integer  :: ac, nmon
  integer,  dimension(20,3) :: mono
  data mono( 1,:) / 0,0,0 /
  data mono( 2,:) / 1,0,0 /
  data mono( 3,:) / 0,1,0 /
  data mono( 4,:) / 0,0,1 /
  data mono( 5,:) / 2,0,0 /
  data mono( 6,:) / 1,1,0 /
  data mono( 7,:) / 0,2,0 /
  data mono( 8,:) / 1,0,1 / 
  data mono( 9,:) / 0,1,1 /
  data mono(10,:) / 0,0,2 /
  data mono(11,:) / 3,0,0 /
  data mono(12,:) / 2,1,0 /
  data mono(13,:) / 1,2,0 /
  data mono(14,:) / 0,3,0 /
  data mono(15,:) / 2,0,1 /
  data mono(16,:) / 1,1,1 /
  data mono(17,:) / 0,2,1 /
  data mono(18,:) / 1,0,2 /
  data mono(19,:) / 0,1,2 /
  data mono(20,:) / 0,0,3 /
  
contains

  ! ================================== !
  ! Place collection of points on PLIC !
  ! ================================== !
  subroutine PLIC2Pts(i,j,k,npts3L,pts3L)
    use compgeom_lookup
    use math
    implicit none
    integer, intent(in) :: i,j,k
    integer :: npts3L
    real(WP), dimension(3,8*6) :: pts3L 
    integer :: s,n,nn,ninter
    real(WP), dimension(3) :: cen
    real(WP) :: sdx2,sdy2,sdz2
    real(WP), dimension(3,8) :: cellverts
    real(WP), dimension(3,7) :: inter_pt
    real(WP), dimension(3) :: e1,e2,l,l0,middle
    real(WP) :: ldotn,d,norm
    
    ! Initialize pts in this cell
    npts3L=0
    pts3L=0.0_WP

    ! Loop over subcells
    do s=1,8

       if (VOF(s,i,j,k).le.VOFlo.or.VOF(s,i,j,k).ge.VOFhi) cycle ! no interface

       ! Cell center
       cen=(/ xm(i)+subx(s)*dx(i), &
              ym(j)+suby(s)*dy(j), &
              zm(k)+subz(s)*dz    /)

       ! Subcell size/2
       sdx2=0.25_WP*dx(i);
       sdy2=0.25_WP*dy(j);
       sdz2=0.25_WP*dz;

       ! Cell vertices
       cellverts(:,1)=cen+(/-sdx2,-sdy2,-sdz2/)
       cellverts(:,2)=cen+(/+sdx2,-sdy2,-sdz2/)
       cellverts(:,3)=cen+(/-sdx2,+sdy2,-sdz2/)
       cellverts(:,4)=cen+(/+sdx2,+sdy2,-sdz2/)
       cellverts(:,5)=cen+(/-sdx2,-sdy2,+sdz2/)
       cellverts(:,6)=cen+(/+sdx2,-sdy2,+sdz2/)
       cellverts(:,7)=cen+(/-sdx2,+sdy2,+sdz2/)
       cellverts(:,8)=cen+(/+sdx2,+sdy2,+sdz2/)

       ! Compute intersections of PLIC with cell boundaries
       ninter=0
       do n=1,12
          ! Get edge
          e1(:)=cellverts(:,verts2edge(1,n))
          e2(:)=cellverts(:,verts2edge(2,n))

          ! Equation of line  p=d*l+l0
          l0=e1
          l =e2-e1
          ldotn=dot_product(l,(/normx(s,i,j,k),normy(s,i,j,k),normz(s,i,j,k)/))
          if (abs(ldotn).gt.epsilon(0.0_WP)) then ! intersection of line and PLIC
             d=(dist(s,i,j,k)-dot_product(l0,(/normx(s,i,j,k),normy(s,i,j,k),normz(s,i,j,k)/))) &
                  / ldotn
             if (d.ge.0.and.d.le.1) then ! On edge
                ninter=ninter+1
                inter_pt(:,ninter)=l0+d*l
                ! Check that this point is unique
                d=huge(1.0_WP)
                do nn=1,ninter-1
                   d=min(d,sqrt(sum((inter_pt(:,nn)-inter_pt(:,ninter))**2)))
                end do
                if (d.lt.1e-12*min(dx(i),dy(j),dz)) then ! not unique
                   ninter=ninter-1
                end if
             end if
          end if
       end do

       ! Create middle of PLIC based on intersections
       middle(1)=sum(inter_pt(1,1:ninter))/real(ninter,WP)
       middle(2)=sum(inter_pt(2,1:ninter))/real(ninter,WP)
       middle(3)=sum(inter_pt(3,1:ninter))/real(ninter,WP)

       ! Add one point to cloud for each intersection
       do n=1,ninter
          npts3L=npts3L+1
          pts3L(:,npts3L)=middle+1.0_WP/sqrt(3.0_WP)*(inter_pt(:,n)-middle) ! Gauss Quadrature
       end do
       
    end do

    return
  end subroutine PLIC2Pts

end module multiphase_curv3L


subroutine multiphase_curv3L_init
  use multiphase_curv3L
  implicit none

  ! Read number of cells to include in stencil
  call parser_read('Number of adjacent cells',ac,1)
  call parser_read('Number of monomials',nmon,20)

  ! Allocate arrays
  allocate(npts3L(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(pts3L(3,8*6,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))

  return
end subroutine multiphase_curv3L_init


subroutine multiphase_curv3L_calc
  use multiphase_curv3L
  use math
  implicit none
  integer :: i,j,k,s
  integer :: ii,jj,kk,nn
  integer :: row,col
  real(WP), dimension(nmon,nmon) :: MTM  
  real(WP), dimension(nmon) :: MTb
  real(WP), dimension(nmon) :: a
  real(WP) :: xp,yp,zp
  real(WP), dimension(3) :: mynorm
  real(WP) :: ptdist,myptdist
  real(WP) :: Weight,tmp

  ! Debug
  real(WP), dimension(3,10000) :: ptsout
  integer :: nptsout
  nptsout=0
        
  curv = 0.0_WP
  
  ! Loop over domain and compute points
  pts3L=0.0_WP
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           if (VOFavg(i,j,k).gt.VOFlo.and.VOFavg(i,j,k).lt.VOFhi) then ! contains interface
              call PLIC2Pts(i,j,k,npts3L(i,j,k),pts3L(:,:,i,j,k))
           else
              npts3L(i,j,k)=0
           end if
        end do
     end do
  end do
  
  ! Create M matrixes
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if ( VOFavg(i,j,k).le.0.5_WP.and.maxval((/VOFavg(i-1,j,k),VOFavg(i,j-1,k),VOFavg(i,j,k-1)/)).gt.0.5_WP .or. &
                VOFavg(i,j,k).le.0.5_WP.and.maxval((/VOFavg(i+1,j,k),VOFavg(i,j+1,k),VOFavg(i,j,k+1)/)).gt.0.5_WP .or. &
                VOFavg(i,j,k).gt.0.5_WP.and.minval((/VOFavg(i-1,j,k),VOFavg(i,j-1,k),VOFavg(i,j,k-1)/)).lt.0.5_WP .or. &
                VOFavg(i,j,k).gt.0.5_WP.and.minval((/VOFavg(i+1,j,k),VOFavg(i,j+1,k),VOFavg(i,j,k+1)/)).lt.0.5_WP ) then
              ! used in ghost fluid method
              
              ! Average point on PLIC to compute curvature
              if (npts3L(i,j,k).gt.0) then
                 xp=sum(pts3L(1,1:npts3L(i,j,k),i,j,k))/npts3L(i,j,k)
                 yp=sum(pts3L(2,1:npts3L(i,j,k),i,j,k))/npts3L(i,j,k)
                 zp=sum(pts3L(3,1:npts3L(i,j,k),i,j,k))/npts3L(i,j,k)
              else ! Find point closest to cell center
                 ptdist=huge(1.0_WP)
                 do kk=k-ac,k+ac
                    do jj=max(jmin,j-ac),j+ac
                       do ii=i-ac,i+ac
                          do nn=1,npts3L(ii,jj,kk)
                             myptdist=sqrt((pts3L(1,nn,ii,jj,kk)-xm(i))**2 &
                                  +    (pts3L(2,nn,ii,jj,kk)-ym(j))**2 &
                                  +    (pts3L(3,nn,ii,jj,kk)-zm(k))**2 )
                             if (myptdist.lt.ptdist) then
                                ptdist=myptdist
                                xp=pts3L(1,nn,ii,jj,kk)
                                yp=pts3L(2,nn,ii,jj,kk)
                                zp=pts3L(3,nn,ii,jj,kk)
                             end if
                          end do
                       end do
                    end do
                 end do
              end if

              ! Zero matrices
              MTM=0.0_WP
              MTB=0.0_WP
              ! Loop over neighbors
              do kk=k-ac,k+ac
                 do jj=max(jmin,j-ac),j+ac
                    do ii=i-ac,i+ac
                       ! Add points
                       do nn=1,npts3L(ii,jj,kk)
                          
                          ! Interpolate normal to this location
                          mynorm=interp_normal(pts3L(:,nn,ii,jj,kk),ii,jj,kk)

                          do row=1,nmon

                             ! Points and gradient
                             ! ----------------------------------------------------------------
                             weight=exp(-((pts3L(1,nn,ii,jj,kk)-xp)**2 &
                                         +(pts3L(2,nn,ii,jj,kk)-yp)**2 &
                                         +(pts3L(3,nn,ii,jj,kk)-zp)**2)/(0.5_WP*meshsize(i,j,k))**2)
                             ! F=0 @ points on interface (In shifted reference frame)
                             do col=1,nmon
                                MTM(row,col) = MTM(row,col) + weight &
                                     *(pts3L(1,nn,ii,jj,kk)-xp)**mono(row,1) &
                                     *(pts3L(2,nn,ii,jj,kk)-yp)**mono(row,2) &
                                     *(pts3L(3,nn,ii,jj,kk)-zp)**mono(row,3) &
                                     *(pts3L(1,nn,ii,jj,kk)-xp)**mono(col,1) &
                                     *(pts3L(2,nn,ii,jj,kk)-yp)**mono(col,2) &
                                     *(pts3L(3,nn,ii,jj,kk)-zp)**mono(col,3)
                             end do
                             MTB(row) = MTB(row) + weight*0.0_WP
                             ! grad(F).normal=1 @ points on interface (In shifted reference frame)
                             weight=1e-5_WP*exp(-((pts3L(1,nn,ii,jj,kk)-xp)**2 &
                                                 +(pts3L(2,nn,ii,jj,kk)-yp)**2 &
                                                 +(pts3L(3,nn,ii,jj,kk)-zp)**2)/(0.5_WP*meshsize(i,j,k))**2)
                             do col=1,nmon
                                MTM(row,col) = MTM(row,col) + weight*( &
                                     +mynorm(1)*real(mono(row,1),WP)*(pts3L(1,nn,ii,jj,kk)-xp)**(max(0,mono(row,1)-1)) &
                                                                    *(pts3L(2,nn,ii,jj,kk)-yp)**(      mono(row,2)   ) &
                                                                    *(pts3L(3,nn,ii,jj,kk)-zp)**(      mono(row,3)   ) &
                                     +mynorm(2)*real(mono(row,2),WP)*(pts3L(1,nn,ii,jj,kk)-xp)**(      mono(row,1)   ) &
                                                                    *(pts3L(2,nn,ii,jj,kk)-yp)**(max(0,mono(row,2)-1)) &
                                                                    *(pts3L(3,nn,ii,jj,kk)-zp)**(      mono(row,3)   ) &
                                     +mynorm(3)*real(mono(row,3),WP)*(pts3L(1,nn,ii,jj,kk)-xp)**(      mono(row,1)   ) &
                                                                    *(pts3L(2,nn,ii,jj,kk)-yp)**(      mono(row,2)   ) &
                                                                    *(pts3L(3,nn,ii,jj,kk)-zp)**(max(0,mono(row,3)-1)) )*( &
                                     +mynorm(1)*real(mono(col,1),WP)*(pts3L(1,nn,ii,jj,kk)-xp)**(max(0,mono(col,1)-1)) &
                                                                    *(pts3L(2,nn,ii,jj,kk)-yp)**(      mono(col,2)   ) &
                                                                    *(pts3L(3,nn,ii,jj,kk)-zp)**(      mono(col,3)   ) &
                                     +mynorm(2)*real(mono(col,2),WP)*(pts3L(1,nn,ii,jj,kk)-xp)**(      mono(col,1)   ) &
                                                                    *(pts3L(2,nn,ii,jj,kk)-yp)**(max(0,mono(col,2)-1)) &
                                                                    *(pts3L(3,nn,ii,jj,kk)-zp)**(      mono(col,3)   ) &
                                     +mynorm(3)*real(mono(col,3),WP)*(pts3L(1,nn,ii,jj,kk)-xp)**(      mono(col,1)   ) &
                                                                    *(pts3L(2,nn,ii,jj,kk)-yp)**(      mono(col,2)   ) &
                                                                    *(pts3L(3,nn,ii,jj,kk)-zp)**(max(0,mono(col,3)-1)) )
                             end do
                             MTB(row) = MTB(row) + weight*1.0_WP*( &
                                  +mynorm(1)*real(mono(row,1),WP)*(pts3L(1,nn,ii,jj,kk)-xp)**(max(0,mono(row,1)-1)) &
                                                                 *(pts3L(2,nn,ii,jj,kk)-yp)**(      mono(row,2)   ) &
                                                                 *(pts3L(3,nn,ii,jj,kk)-zp)**(      mono(row,3)   ) &
                                  +mynorm(2)*real(mono(row,2),WP)*(pts3L(1,nn,ii,jj,kk)-xp)**(      mono(row,1)   ) &
                                                                 *(pts3L(2,nn,ii,jj,kk)-yp)**(max(0,mono(row,2)-1)) &
                                                                 *(pts3L(3,nn,ii,jj,kk)-zp)**(      mono(row,3)   ) &
                                  +mynorm(3)*real(mono(row,3),WP)*(pts3L(1,nn,ii,jj,kk)-xp)**(      mono(row,1)   ) &
                                                                 *(pts3L(2,nn,ii,jj,kk)-yp)**(      mono(row,2)   ) &
                                                                 *(pts3L(3,nn,ii,jj,kk)-zp)**(max(0,mono(row,3)-1)) )
                          end do

                          ! ! Save points for output
                          ! if ((i.eq.19.or.i.eq.20).and.j.eq.5.and.k.eq.5) then
                          !    nptsout=nptsout+1
                          !    ptsout(:,nptsout)=pts3l(:,nn,ii,jj,kk)
                          ! end if
                       end do
                    end do
                 end do
              end do

              ! ! Output points
              ! if (i.eq.19.and.j.eq.5.and.k.eq.5.and.nptsout.gt.0) then
              !    call pts2silo(nptsout,transpose(ptsout(:,1:nptsout)),'points19.silo')
              !    call pts2silo(1,(/xp,yp,zp/),'points19o.silo')
              !    print *,'19'
              !    print *,'xp=',xp,yp,zp
              ! end if
              ! if (i.eq.20.and.j.eq.5.and.k.eq.5.and.nptsout.gt.0) then
              !    call pts2silo(nptsout,transpose(ptsout(:,1:nptsout)),'points20.silo')
              !    call pts2silo(1,(/xp,yp,zp/),'points20o.silo')
              !    print *,'20'
              !    print *,'xp=',xp,yp,zp
              ! end if
              
              ! Least Squares Solution
              call solve_linear_system(MTM(1:nmon,1:nmon),MTB(1:nmon),a(1:nmon),nmon)

              ! Compute curvature (valid for 2nd or higher order polynomial and shifted coordinate system)
              curv(i,j,k)=2.0_WP*(a(3)**2*a(5) + a(2)**2*a(7) + a(4)**2*a(5) + a(4)**2*a(7) + a(2)**2*a(10) &
                   + a(3)**2*a(10) - a(2)*a(3)*a(6) - a(2)*a(4)*a(8) - a(3)*a(4)*a(9)) &
                   /(a(2)**2 + a(3)**2 + a(4)**2)**1.5_WP

              ! Clip the curvature
              curv(i,j,k)=max(min(curv(i,j,k),1.0_WP/meshsize(i,j,k)),-1.0_WP/meshsize(i,j,k))

                   
           else
              ! Do nothing
           end if
        end do
     end do
  end do

  ! Update curv in ghost cells
  call communication_border(curv)

  return

contains
  
  function interp_normal(pt,i,j,k) result(norm)
    use compgeom_lookup
    implicit none
    real(WP), dimension(3), intent(in) :: pt
    integer, intent(in) :: i,j,k
    real(WP), dimension(3) :: norm
    integer :: ss,ii,jj,kk
    real(WP) :: weight,myweight,mydist

    ! Initialize norm
    norm=0.0_WP
    weight=0.0_WP
    
    ! Loop over neighbors
    do kk=k-1,k+1
       do jj=j-1,j+1
          do ii=i-1,i+1
             do ss=1,8
                if (VOF(ss,ii,jj,kk).gt.VOFlo.and.VOF(ss,ii,jj,kk).lt.VOFhi) then 
                   ! Compute weighted average of normals
                   mydist= (pt(1)-xm(ii)+subx(ss)*dx(ii))**2 &
                        +  (pt(2)-ym(jj)+suby(ss)*dy(jj))**2 &
                        +  (pt(3)-zm(kk)+subz(ss)*dz    )**2 
                   myweight=exp(-mydist/meshsize(i,j,k)**2)
                   norm = norm + myweight*(/normx(ss,ii,jj,kk),normy(ss,ii,jj,kk),normz(ss,ii,jj,kk)/)
                end if
             end do
          end do
       end do
    end do

    ! Normalize normal
    norm = norm/sqrt(sum(norm(:)**2))

    return
  end function interp_normal
    
end subroutine multiphase_curv3L_calc


