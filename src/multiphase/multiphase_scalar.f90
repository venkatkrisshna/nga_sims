module multiphase_scalar
  use multiphase
  use scalar
  use scalar_weno3
  use metric_generic
  use memory
  implicit none

  real(WP), dimension(:,:,:), pointer :: FXvol,FYvol,FZvol,VOFnew

end module multiphase_scalar

! ===================== !
! Initialize the module !
! ===================== !
subroutine multiphase_scalar_init
  use multiphase_scalar
  use data
  implicit none
  
  integer :: isc

  ! Check for multiphase
  if (.not.use_multiphase) &
       call die('Must be multiphase simultion to use multiphase scalars')
  
  ! Determine scalar's phase from first letter in scalar name
  allocate(SCphase(nscalar))
  do isc=1,nscalar
     select case (SC_name(isc)(1:1))
     case ('g','G')
        SCphase(isc)=0.0_WP
     case ('l','L')
        SCphase(isc)=1.0_WP
     case default
        call die('Multiphase scalars must begin with either "g" or "l" to indicate phase')
     end select
  end do

  ! Initialize WENO module
  call scalar_weno3_init

  ! Initialize scalar monitor
  call monitor_create_file_step('multiphase_scalar',nscalar)
  do isc=1,nscalar
     call monitor_set_header(isc,'int_'//SC_name(isc),'r')
  end do
  call multiphase_scalar_monitor

  return
end subroutine multiphase_scalar_init


! ======================================= !
! Compute residual for multiphase scalars !
! ======================================= !
subroutine multiphase_scalar_residual
  use multiphase_scalar
  use multiphase_band
  use multiphase_tools
  use multiphase_fluxes
  use compgeom_lookup
  implicit none

  integer :: i,j,k,isc
  real(WP) :: rhs
  real(WP), parameter :: eps=1.0e-12_WP
  
  ! if (time-dt_uvw.eq.0.0_WP) then
  !    if (irank.eq.iroot) print *,'Skipping first step due to large divgence'
  !    ResSC(:,:,:,:) = 0.0_WP ! -2.0_WP*SC(:,:,:,:)+SCold(:,:,:,:)
  !    return
  ! end if

  ! Setup pointers to volume fluxes
  FXvol  => tmp12
  FYvol  => tmp13
  FZvol  => tmp14
  VOFnew => tmp15

  ! Loop over scalars
  do isc=1,nscalar

     ! Get WENO coefficients
     call scalar_weno3_coeff(isc)
     
     ! Compute fluxes
     do k=kmin_-st1,kmax_+st2
        do j=jmin_-st1,jmax_+st2
           do i=imin_-st1,imax_+st2  
              
              ! Convective fluxes -------------------------------

              ! X face
              if (maxval(abs(bandold(i-1:i,j,k))).le.nband_CFL) then
                 ! Geometric flux
                 FX   (i,j,k)=-SLF_SCx(i,j,k,isc)/(dt_uvw*dy(j)*dz)
                 if (nint(SCphase(isc)).eq.1) then
                    FXvol(i,j,k)=-sum(SLF_liqx(subcell2flux_Px_s(:),i,j,k))/(dt_uvw*dy(j)*dz)
                 else
                    FXvol(i,j,k)=-sum(SLF_gasx(subcell2flux_Px_s(:),i,j,k))/(dt_uvw*dy(j)*dz)
                 end if
              else
                 ! WENO flux
                 if ( (nint(SCphase(isc)).eq.0.and.VOFavg(i,j,k).lt.0.5_WP) .or. &
                      (nint(SCphase(isc)).eq.1.and.VOFavg(i,j,k).gt.0.5_WP) ) then
                    call multiphase_scalar_wenoX(i,j,k,isc)
                    FXvol(i,j,k)=-U(i,j,k)
                 else
                    FX   (i,j,k)=0.0_WP
                    FXvol(i,j,k)=0.0_WP
                 end if
              end if

              ! Y face
              if (maxval(abs(bandold(i,j-1:j,k))).le.nband_CFL) then
                 ! Geometric flux
                 FY   (i,j,k)=-SLF_SCy(i,j,k,isc)/(dt_uvw*dx(i)*dz)
                 if (nint(SCphase(isc)).eq.1) then
                    FYvol(i,j,k)=-sum(SLF_liqy(subcell2flux_Py_s(:),i,j,k))/(dt_uvw*dx(i)*dz)
                 else
                    FYvol(i,j,k)=-sum(SLF_gasy(subcell2flux_Py_s(:),i,j,k))/(dt_uvw*dx(i)*dz)
                 end if
              else
                 ! WENO flux
                 if ( (nint(SCphase(isc)).eq.0.and.VOFavg(i,j,k).lt.0.5_WP) .or. &
                      (nint(SCphase(isc)).eq.1.and.VOFavg(i,j,k).gt.0.5_WP) ) then
                    call multiphase_scalar_wenoY(i,j,k,isc)
                    FYvol(i,j,k)=-V(i,j,k)
                 else
                    FY   (i,j,k)=0.0_WP
                    FYvol(i,j,k)=0.0_WP
                 end if
              end if

              ! Z face
              if (maxval(abs(bandold(i,j,k-1:k))).le.nband_CFL) then
                 ! Geometric flux
                 FZ(i,j,k)=-SLF_SCz(i,j,k,isc)/(dt_uvw*dx(i)*dy(j))
                 if (nint(SCphase(isc)).eq.1) then
                    FZvol(i,j,k)=-sum(SLF_liqz(subcell2flux_Pz_s(:),i,j,k))/(dt_uvw*dx(i)*dy(j))
                 else
                    FZvol(i,j,k)=-sum(SLF_gasz(subcell2flux_Pz_s(:),i,j,k))/(dt_uvw*dx(i)*dy(j))
                 end if
              else
                 ! WENO flux
                 if ( (nint(SCphase(isc)).eq.0.and.VOFavg(i,j,k).lt.0.5_WP) .or. &
                      (nint(SCphase(isc)).eq.1.and.VOFavg(i,j,k).gt.0.5_WP) ) then
                    call multiphase_scalar_wenoZ(i,j,k,isc)
                    FZvol(i,j,k)=-W(i,j,k)
                 else
                    FZ   (i,j,k)=0.0_WP
                    FZvol(i,j,k)=0.0_WP
                 end if
              end if

              ! Viscous fluxes -------------------------------
              FX(i,j,k) = FX(i,j,k) &
                   + sum(interp_sc_x(i,j,:)*DIFF(i-st2:i+st1,j,k,isc)) * &
                     sum(grad_x(i,j,:)*SC(i-st2:i+st1,j,k,isc))
              FY(i,j,k) = FY(i,j,k) &
                   + sum(interp_sc_y(i,j,:)*DIFF(i,j-st2:j+st1,k,isc)) * &
                     sum(grad_y(i,j,:)*SC(i,j-st2:j+st1,k,isc))
              FZ(i,j,k) = FZ(i,j,k) &
                   + sum(interp_sc_z(i,j,:)*DIFF(i,j,k-st2:k+st1,isc)) * &
                     sum(grad_z(i,j,:)*SC(i,j,k-st2:k+st1,isc))
           end do
        end do
     end do
     
     ! Compute residual
     if (nint(SCphase(isc)).eq.0) then ! Gas scalar
        ! Compute new cell phase volume that is consistent with fluxes
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 rhs =+sum(div_u(i,j,k,:)*FXvol(i-st1:i+st2,j,k)) &
                      +sum(div_v(i,j,k,:)*FYvol(i,j-st1:j+st2,k)) &
                      +sum(div_w(i,j,k,:)*FZvol(i,j,k-st1:k+st2)) 
                 VOFnew(i,j,k)=1.0_WP-((1.0_WP-VOFavgold(i,j,k))+dt_uvw*rhs)
              end do
           end do
        end do
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 if (VOFavg(i,j,k).gt.VOFhi) then
                    ResSC(i,j,k,isc) =  -2.0_WP*SC(i,j,k,isc)+SCold(i,j,k,isc)
                 else
                    rhs =+sum(div_u(i,j,k,:)*FX(i-st1:i+st2,j,k)) &
                         +sum(div_v(i,j,k,:)*FY(i,j-st1:j+st2,k)) &
                         +sum(div_w(i,j,k,:)*FZ(i,j,k-st1:k+st2)) 
                    ResSC(i,j,k,isc) =  -2.0_WP*SC(i,j,k,isc)+SCold(i,j,k,isc) &
                        + ( SCold(i,j,k,isc)*(1.0_WP-VOFavgold(i,j,k)) + dt_uvw*rhs           &
                        + srcSCmid(i,j,k,isc) ) &
                        / ( sign(max(abs(1.0_WP-VOFavg(i,j,k)),eps*meshsize(i,j,k)),1.0_WP-VOFavg(i,j,k)))
                 end if
              end do
           end do
        end do
     else                             ! Liquid scalar
        ! Compute new cell phase volume that is consistent with fluxes
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 rhs =+sum(div_u(i,j,k,:)*FXvol(i-st1:i+st2,j,k)) &
                      +sum(div_v(i,j,k,:)*FYvol(i,j-st1:j+st2,k)) &
                      +sum(div_w(i,j,k,:)*FZvol(i,j,k-st1:k+st2)) 
                 VOFnew(i,j,k)=VOFavgold(i,j,k) + dt_uvw*rhs
              end do
           end do
        end do
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 if (VOFavg(i,j,k).lt.VOFlo) then
                    ResSC(i,j,k,isc) =  -2.0_WP*SC(i,j,k,isc)+SCold(i,j,k,isc)
                 else
                    rhs =+sum(div_u(i,j,k,:)*FX(i-st1:i+st2,j,k)) &
                         +sum(div_v(i,j,k,:)*FY(i,j-st1:j+st2,k)) &
                         +sum(div_w(i,j,k,:)*FZ(i,j,k-st1:k+st2))
                    ResSC(i,j,k,isc) =  -2.0_WP*SC(i,j,k,isc)+SCold(i,j,k,isc) &
                        + ( SCold(i,j,k,isc)*VOFavgold(i,j,k) + dt_uvw*rhs &
                        + srcSCmid(i,j,k,isc) ) / &
                        (sign(max(abs(VOFnew(i,j,k)),eps*meshsize(i,j,k)),VOFnew(i,j,k)))
                 end if
              end do
           end do
        end do
     end if

  end do
     
  return
end subroutine multiphase_scalar_residual

! ======================================= !
! Inverse operator for multiphase scalars !
!  - Assumes WENO3 scheme is being used   !
! ======================================= !
subroutine multiphase_scalar_operator(isc)
  use multiphase_scalar
  use compgeom_lookup
  use implicit
  implicit none

  integer, intent(in) :: isc
  integer  :: i,j,k
  real(WP) :: conv1,conv2,conv3,conv4
  real(WP) :: visc1,visc2
  real(WP) :: dt2,mydiag
  real(WP) :: cmask
  
  dt2 = dt/2.0_WP

  Ax=0.0_WP
  Ay=0.0_WP
  Az=0.0_WP
  
  ! If purely explicit return
  if (.not.implicit_any) return
  
  
  ! Get coefficients first
  call scalar_weno3_coeff(isc)

  ! X-direction
  if (implicit_x) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_

              cmask=0.25_WP*real(sum(cmask_x(subcell2flux_Px_s(:),i  ,j,k)),WP)
              conv1 = 0.5_WP*(rhoU(i  ,j,k) + abs(rhoU(i  ,j,k))) * cmask
              conv2 = 0.5_WP*(rhoU(i  ,j,k) - abs(rhoU(i  ,j,k))) * cmask
              cmask=0.25_WP*real(sum(cmask_x(subcell2flux_Px_s(:),i+1,j,k)),WP)
              conv3 = 0.5_WP*(rhoU(i+1,j,k) + abs(rhoU(i+1,j,k))) * cmask
              conv4 = 0.5_WP*(rhoU(i+1,j,k) - abs(rhoU(i+1,j,k))) * cmask

              visc1 = sum(interp_sc_x(i  ,j,:)*DIFF(i  -st2:i  +st1,j,k,isc))
              visc2 = sum(interp_sc_x(i+1,j,:)*DIFF(i+1-st2:i+1+st1,j,k,isc))

              Ax(j,k,i,-2) = + dt2 * div_u(i,j,k,0)*conv1*weno3_xp(i,j,k,-2)

              Ax(j,k,i,-1) = - dt2 * ( &
                   + div_u(i,j,k,0)*( - conv1*weno3_xp(i  ,j,k,-1) - conv2*weno3_xm(i  ,j,k,-1) + visc1*grad_x(i,j,-1)) &
                   + div_u(i,j,k,1)*( - conv3*weno3_xp(i+1,j,k,-2)))

              Ax(j,k,i, 0) = - dt2 * ( &
                   + div_u(i,j,k,0)*( - conv1*weno3_xp(i  ,j,k, 0) - conv2*weno3_xm(i  ,j,k, 0) + visc1*grad_x(i,j,0)) &
                   + div_u(i,j,k,1)*( - conv3*weno3_xp(i+1,j,k,-1) - conv4*weno3_xm(i+1,j,k,-1) + visc2*grad_x(i+1,j,-1)))

              Ax(j,k,i,+1) = - dt2 * ( &
                   + div_u(i,j,k,0)*( - conv2*weno3_xm(i  ,j,k,+1)) &
                   + div_u(i,j,k,1)*( - conv3*weno3_xp(i+1,j,k, 0) - conv4*weno3_xm(i+1,j,k, 0) + visc2*grad_x(i+1,j,0)))

              Ax(j,k,i,+2) = + dt2 * div_u(i,j,k,1)*conv4*weno3_xm(i+1,j,k,+1)

           end do
        end do
     end do
  end if

  ! Y-direction
  if (implicit_y) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              
              cmask=0.25_WP*real(sum(cmask_y(subcell2flux_Py_s(:),i,j  ,k)),WP)
              conv1 = 0.5_WP*(rhoV(i,j  ,k) + abs(rhoV(i,j  ,k))) * cmask
              conv2 = 0.5_WP*(rhoV(i,j  ,k) - abs(rhoV(i,j  ,k))) * cmask
              cmask=0.25_WP*real(sum(cmask_y(subcell2flux_Py_s(:),i,j+1,k)),WP)
              conv3 = 0.5_WP*(rhoV(i,j+1,k) + abs(rhoV(i,j+1,k))) * cmask
              conv4 = 0.5_WP*(rhoV(i,j+1,k) - abs(rhoV(i,j+1,k))) * cmask

              visc1 = sum(interp_sc_y(i,j  ,:)*DIFF(i,j  -st2:j  +st1,k,isc))
              visc2 = sum(interp_sc_y(i,j+1,:)*DIFF(i,j+1-st2:j+1+st1,k,isc))

              Ay(i,k,j,-2) = + dt2 * div_v(i,j,k,0)*conv1*weno3_yp(i,j,k,-2)

              Ay(i,k,j,-1) = - dt2*( &
                   + div_v(i,j,k,0)*( - conv1*weno3_yp(i,j  ,k,-1) - conv2*weno3_ym(i,j  ,k,-1) + visc1*grad_y(i,j,-1)) &
                   + div_v(i,j,k,1)*( - conv3*weno3_yp(i,j+1,k,-2)))

              Ay(i,k,j, 0) = - dt2 * ( &
                   + div_v(i,j,k,0)*( - conv1*weno3_yp(i,j  ,k, 0) - conv2*weno3_ym(i,j  ,k, 0) + visc1*grad_y(i,j,0)) &
                   + div_v(i,j,k,1)*( - conv3*weno3_yp(i,j+1,k,-1) - conv4*weno3_ym(i,j+1,k,-1) + visc2*grad_y(i,j+1,-1)))

              Ay(i,k,j,+1) = - dt2*( &
                   + div_v(i,j,k,0)*( - conv2*weno3_ym(i,j  ,k,+1)) &
                   + div_v(i,j,k,1)*( - conv3*weno3_yp(i,j+1,k, 0) - conv4*weno3_ym(i,j+1,k, 0) + visc2*grad_y(i,j+1,0)))

              Ay(i,k,j,+2) = dt2 * div_v(i,j,k,1)*conv4*weno3_ym(i,j+1,k,+1)

           end do
        end do
     end do
     ! ! Boundary conditions
     ! if (icyl.eq.1 .and. jproc.eq.1) then
     !    do k=kmin_,kmax_
     !       Ay(:,k,jmin+1,-1) = Ay(:,k,jmin+1,-1) + Ay(:,k,jmin+1,-2)
     !       Ay(:,k,jmin+1,-2) = 0.0_WP
     !       Ay(:,k,jmin, 0) = Ay(:,k,jmin, 0) + Ay(:,k,jmin,-1) + Ay(:,k,jmin,-2)
     !       Ay(:,k,jmin,-1) = 0.0_WP
     !       Ay(:,k,jmin,-2) = 0.0_WP
     !    end do
     ! end if
  end if

  ! Z-direction
  if (implicit_z) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_

              cmask=0.25_WP*real(sum(cmask_z(subcell2flux_Pz_s(:),i,j,k  )),WP)
              conv1 = 0.5_WP*(rhoW(i,j,k  ) + abs(rhoW(i,j,k  ))) * cmask
              conv2 = 0.5_WP*(rhoW(i,j,k  ) - abs(rhoW(i,j,k  ))) * cmask
              cmask=0.25_WP*real(sum(cmask_z(subcell2flux_Pz_s(:),i,j,k+1)),WP)
              conv3 = 0.5_WP*(rhoW(i,j,k+1) + abs(rhoW(i,j,k+1))) * cmask
              conv4 = 0.5_WP*(rhoW(i,j,k+1) - abs(rhoW(i,j,k+1))) * cmask

              visc1 = sum(interp_sc_z(i,j,:)*DIFF(i,j,k  -st2:k  +st1,isc))
              visc2 = sum(interp_sc_z(i,j,:)*DIFF(i,j,k+1-st2:k+1+st1,isc))

              Az(i,j,k,-2) = + dt2 * div_w(i,j,k,0)*conv1*weno3_zp(i,j,k,-2)

              Az(i,j,k,-1) = - dt2 * ( &
                   + div_w(i,j,k,0)*( - conv1*weno3_zp(i,j,k  ,-1) - conv2*weno3_zm(i,j,k,-1) + visc1*grad_z(i,j,-1)) &
                   + div_w(i,j,k,1)*( - conv3*weno3_zp(i,j,k+1,-2)))

              Az(i,j,k, 0) = - dt2 * ( &
                   + div_w(i,j,k,0)*( - conv1*weno3_zp(i,j,k  , 0) - conv2*weno3_zm(i,j,k  , 0) + visc1*grad_z(i,j,0)) &
                   + div_w(i,j,k,1)*( - conv3*weno3_zp(i,j,k+1,-1) - conv4*weno3_zm(i,j,k+1,-1) + visc2*grad_z(i,j,-1)))

              Az(i,j,k,+1) = - dt2 * ( &
                   + div_w(i,j,k,0)*( - conv2*weno3_zm(i,j,k  ,+1)) &
                   + div_w(i,j,k,1)*( - conv3*weno3_zp(i,j,k+1, 0) - conv4*weno3_zm(i,j,k+1, 0) + visc2*grad_z(i,j,0)))

              Az(i,j,k,+2) = + dt2 * div_w(i,j,k,1)*conv4*weno3_zm(i,j,k+1,+1)

           end do
        end do
     end do
  end if

  ! Create full diagonal
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           mydiag = 1.0_WP + Ax(j,k,i,0)+Ay(i,k,j,0)+Az(i,j,k,0)
           Ax(j,k,i,0) = mydiag
           Ay(i,k,j,0) = mydiag
           Az(i,j,k,0) = mydiag
        end do
     end do
  end do

  return
end subroutine multiphase_scalar_operator


! ====================================== !
! Compute inverse for multiphase scalars !
! ====================================== !
subroutine multiphase_scalar_inverse(isc)
  use multiphase_scalar
  use implicit
  implicit none

  integer, intent(in) :: isc
  integer  :: i,j,k

  
  ! If purely explicit return
  if (.not.implicit_any) return

  ! X-direction
  if (implicit_x) then

     ! Prepare RHS
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Rx(j,k,i) = ResSC(i,j,k,isc)
           end do
        end do
     end do

     ! Solve
     call linear_solver_x(5)

  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Rx(j,k,i) = ResSC(i,j,k,isc)
           end do
        end do
     end do
  end if

  ! Y-direction
  if (implicit_y) then

     ! Prepare RHS
     if (implicit_x) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Ry(i,k,j) = Ay(i,k,j,0) * Rx(j,k,i)
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Ry(i,k,j) =  Rx(j,k,i)
              end do
           end do
        end do
     end if

     ! Solve
     call linear_solver_y(5)

  else 
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Ry(i,k,j) = Rx(j,k,i)
           end do
        end do
     end do
  end if

  ! Z-direction
  if (implicit_z) then

     ! Prepare RHS
     if (implicit_x .or. implicit_y) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Rz(i,j,k) = Az(i,j,k,0) * Ry(i,k,j)
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Rz(i,j,k) = Ry(i,k,j)
              end do
           end do
        end do
     end if

     ! Solve
     call linear_solver_z(5)

     ! Get back the residual
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ResSC(i,j,k,isc) = Rz(i,j,k)
           end do
        end do
     end do

  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ResSC(i,j,k,isc) = Ry(i,k,j)
           end do
        end do
     end do
  end if

  return
end subroutine multiphase_scalar_inverse

! =============================== !
! Compute WENO3 fluxes for scalar !
! =============================== !
subroutine multiphase_scalar_wenoX(i,j,k,isc)
  use multiphase_scalar
  implicit none
  integer, intent(in) :: i,j,k,isc
  FX(i,j,k) = - 0.5_WP*(U(i,j,k)+abs(U(i,j,k))) * sum(weno3_xp(i,j,k,:)*SC(i-2:i  ,j,k,isc)) &
              - 0.5_WP*(U(i,j,k)-abs(U(i,j,k))) * sum(weno3_xm(i,j,k,:)*SC(i-1:i+1,j,k,isc)) 
  return 
end subroutine multiphase_scalar_wenoX

subroutine multiphase_scalar_wenoY(i,j,k,isc)
  use multiphase_scalar
  implicit none
  integer, intent(in) :: i,j,k,isc
  FY(i,j,k) = - 0.5_WP*(V(i,j,k)+abs(V(i,j,k))) * sum(weno3_yp(i,j,k,:)*SC(i,j-2:j  ,k,isc)) &
              - 0.5_WP*(V(i,j,k)-abs(V(i,j,k))) * sum(weno3_ym(i,j,k,:)*SC(i,j-1:j+1,k,isc)) 
  return 
end subroutine multiphase_scalar_wenoY

subroutine multiphase_scalar_wenoZ(i,j,k,isc)
  use multiphase_scalar
  implicit none
  integer, intent(in) :: i,j,k,isc
  FZ(i,j,k) = - 0.5_WP*(W(i,j,k)+abs(W(i,j,k))) * sum(weno3_zp(i,j,k,:)*SC(i,j,k-2:k  ,isc)) &
              - 0.5_WP*(W(i,j,k)-abs(W(i,j,k))) * sum(weno3_zm(i,j,k,:)*SC(i,j,k-1:k+1,isc)) 
  return 
end subroutine multiphase_scalar_wenoZ

! ========================== !
! Monitor multiphase scalars !
! ========================== !
subroutine multiphase_scalar_monitor
  use multiphase_scalar
  use fileio
  implicit none
  integer :: i,j,k,isc
  real(WP),dimension(nscalar) :: my_intSC,intSC
  integer :: n,iunit,phase,ierr
  character(len=str_medium) :: strtime,strisc
  real(WP) :: radius

  ! Check if needed
  if (trim(scalar_scheme).ne.'multiphase') return

  ! Compute int(SC)
  my_intSC=0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           my_intSC(:) = my_intSC(:) + SC(i,j,k,:) * dx(i)*dy(j)*dz* &
                (0.125_WP*sum(VOF(:,i,j,k))*SCphase(:) + (1.0_WP-0.125_WP*sum(VOF(:,i,j,k)))*(1.0_WP-SCphase(:)))
        end do
     end do
  end do
  
  ! Communicate
  call parallel_sum(my_intSC,intSC)


  ! Transfer values to monitor
  call monitor_select_file('multiphase_scalar')
  call monitor_set_array_values(intSC)


  return
end subroutine multiphase_scalar_monitor
