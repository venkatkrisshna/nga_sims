! ========================== !
! Compute curvature using    !
! least squares fit of ponts !
! on the PLIC interface      !
! ========================== !
module multiphase_curvLS
  use multiphase
  implicit none

  integer :: ac
  integer, dimension(:,:,:), allocatable :: nptsLS
  real(WP), dimension(:,:,:,:,:), allocatable :: ptsLS
  real(WP), dimension(:,:,:,:), allocatable :: ptsW
  integer :: order
  real(WP) :: GausSigma
  integer,  dimension(20,2) :: mono
  data mono( 1,:) / 0,0 / ! 0th order
  data mono( 2,:) / 1,0 /
  data mono( 3,:) / 0,1 / ! 1st order
  data mono( 4,:) / 2,0 /
  data mono( 5,:) / 1,1 /
  data mono( 6,:) / 0,2 / ! 2nd order
  data mono( 7,:) / 3,0 /
  data mono( 8,:) / 2,1 /
  data mono( 9,:) / 1,2 /
  data mono(10,:) / 0,3 / ! 3rd order
  data mono(11,:) / 4,0 /
  data mono(12,:) / 3,1 /
  data mono(13,:) / 2,2 /
  data mono(14,:) / 1,3 /
  data mono(15,:) / 0,4 / ! 4th order
  integer, dimension(2:4), parameter :: nmono_order=(/6,10,15/)
  integer :: nmono

  ! Debug
  integer, parameter :: iout=-27,jout=46,kout=5
  real(WP), dimension(:,:), allocatable :: ptsout,ptsRout
  integer :: nptsout
  
contains

  ! ================================== !
  ! Place collection of points on PLIC !
  ! ================================== !
  subroutine PLIC2Pts(i,j,k)
    use compgeom_lookup
    use math
    use multiphase_tools
    implicit none
    integer, intent(in) :: i,j,k
    integer :: s,n,nn,ninter 
    real(WP), dimension(3) :: cen
    real(WP) :: sdx2,sdy2,sdz2
    real(WP), dimension(3,8) :: cellverts
    real(WP), dimension(3,7) :: inter_pt
    real(WP), dimension(3) :: e1,e2,l,l0,middle
    real(WP) :: ldotn,d,area

    ! Check BC's
    if (yper.eq.0 .and. j.lt.jmin) return
    
    ! Loop over subcells
    do s=1,8

       if (VOF(s,i,j,k).le.VOFlo.or.VOF(s,i,j,k).ge.VOFhi) cycle ! no interface

       ! Cell center
       cen=(/ xsm(s,i),ysm(s,j),zsm(s,k) /)

       ! Subcell size/2
       sdx2=0.25_WP*dx(i);
       sdy2=0.25_WP*dy(j);
       sdz2=0.25_WP*dz;

       ! Cell vertices
       cellverts(:,1)=cen+(/-sdx2,-sdy2,-sdz2/)
       cellverts(:,2)=cen+(/+sdx2,-sdy2,-sdz2/)
       cellverts(:,3)=cen+(/-sdx2,+sdy2,-sdz2/)
       cellverts(:,4)=cen+(/+sdx2,+sdy2,-sdz2/)
       cellverts(:,5)=cen+(/-sdx2,-sdy2,+sdz2/)
       cellverts(:,6)=cen+(/+sdx2,-sdy2,+sdz2/)
       cellverts(:,7)=cen+(/-sdx2,+sdy2,+sdz2/)
       cellverts(:,8)=cen+(/+sdx2,+sdy2,+sdz2/)

       ! Compute intersections of PLIC with cell boundaries
       ninter=0
       do n=1,12
          ! Get edge
          e1(:)=cellverts(:,verts2edge(1,n))
          e2(:)=cellverts(:,verts2edge(2,n))

          ! Equation of line  p=d*l+l0
          l0=e1
          l =e2-e1
          ldotn=dot_product(l,(/normx(s,i,j,k),normy(s,i,j,k),normz(s,i,j,k)/))
          if (abs(ldotn).gt.epsilon(0.0_WP)) then ! intersection of line and PLIC
             d=(dist(s,i,j,k)-dot_product(l0,(/normx(s,i,j,k),normy(s,i,j,k),normz(s,i,j,k)/))) &
                  / ldotn
             if (d.ge.0.and.d.le.1) then ! On edge
                ninter=ninter+1
                inter_pt(:,ninter)=l0+d*l
                ! Check that this point is unique
                d=huge(1.0_WP)
                do nn=1,ninter-1
                   d=min(d,sqrt(sum((inter_pt(:,nn)-inter_pt(:,ninter))**2)))
                end do
                if (d.lt.1e-12*min(dx(i),dy(j),dz)) then ! not unique
                   ninter=ninter-1
                end if
             end if
          end if
       end do

       ! Create middle of PLIC based on intersections
       middle(1)=sum(inter_pt(1,1:ninter))/real(ninter,WP)
       middle(2)=sum(inter_pt(2,1:ninter))/real(ninter,WP)
       middle(3)=sum(inter_pt(3,1:ninter))/real(ninter,WP)

       ! Compute area of interface (used for weight)
       area=abs(area_polygon(ninter,inter_pt(:,1:ninter),.true.))
      
       ! Add one point to cloud for each intersection
       do n=1,ninter
          nptsLS(i,j,k)=nptsLS(i,j,k)+1
          ptsLS(:,nptsLS(i,j,k),i,j,k)=middle+1.0_WP/sqrt(3.0_WP)*(inter_pt(:,n)-middle) ! Gauss Quadrature
          ptsW(nptsLS(i,j,k),i,j,k)=area/(0.5_WP*meshsize(i,j,k))**2 ! Weight based on "area"
       end do
       
    end do

    return
  end subroutine PLIC2Pts
 
end module multiphase_curvLS

! ============== !
! Initialization !
! ============== !
subroutine multiphase_curvLS_init
  use multiphase_curvLS
  implicit none

  ! Read number of cells to include in stencil
  call parser_read('Number of adjacent cells',ac,1)
  call parser_read('Order of fit',order,2)
  call parser_read('Gausian sigma',GausSigma,1.0_WP)
  if (.not.(order.eq.2.or.order.eq.3.or.order.eq.4)) &
       call die('Least squares curvature order should be 2, 3, or 4')
  nmono=nmono_order(order)

  ! Allocate arrays
  allocate(nptsLS(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(ptsLS(3,8*6,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(ptsW(8*6,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))

  ! Debug
  allocate(ptsout(3,8*6*(2*ac+1)**3))
  allocate(ptsRout(3,8*6*(2*ac+1)**3))
  
  return
end subroutine multiphase_curvLS_init

! ================= !
! Compute curvature !
! ================= !
subroutine multiphase_curvLS_calc
  use multiphase_curvLS
  use math
  implicit none
  integer :: n,i,j,k
  integer :: nn,ii,jj,kk
  logical :: is_done
  real(WP), dimension(3) :: nv,t1,t2,ptR,Xo
  real(WP), dimension(3,3) :: Rot,iRot
  real(WP), dimension(nmono,nmono) :: MTM
  real(WP), dimension(nmono) :: MTB,a
  integer :: row,col
  real(WP) :: H1,H2,H11,H22,H12
  real(WP) :: weight,sig

  ! Initialize curvature
  curv=0.0_WP
  hasCurv=0.0_WP
  nptsout=0
  
  ! Loop over domain and compute points
  nptsLS=0
  ptsLS=0.0_WP
  ptsW =0.0_WP
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           if (VOFavg(i,j,k).gt.VOFlo.and.VOFavg(i,j,k).lt.VOFhi) then ! contains interface
              call PLIC2Pts(i,j,k)
           else
              nptsLS(i,j,k)=0
           end if
        end do
     end do
  end do

  ! Loop over cells used in ghost fluid method
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if ( VOFavg(i,j,k).le.0.5_WP.and.maxval((/VOFavg(i-1,j,k),VOFavg(i,j-1,k),VOFavg(i,j,k-1)/)).gt.0.5_WP .or. &
                VOFavg(i,j,k).le.0.5_WP.and.maxval((/VOFavg(i+1,j,k),VOFavg(i,j+1,k),VOFavg(i,j,k+1)/)).gt.0.5_WP .or. &
                VOFavg(i,j,k).gt.0.5_WP.and.minval((/VOFavg(i-1,j,k),VOFavg(i,j-1,k),VOFavg(i,j,k-1)/)).lt.0.5_WP .or. &
                VOFavg(i,j,k).gt.0.5_WP.and.minval((/VOFavg(i+1,j,k),VOFavg(i,j+1,k),VOFavg(i,j,k+1)/)).lt.0.5_WP ) then

              ! Form local coordinate system, nv,t1,t2
              ! --------------------------------------
              ! Compute average normal 
              nv(1)=sum(normx(:,i-1:i+1,j-1:j+1,k-1:k+1))
              nv(2)=sum(normy(:,i-1:i+1,j-1:j+1,k-1:k+1))
              nv(3)=sum(normz(:,i-1:i+1,j-1:j+1,k-1:k+1))
              if (sqrt(sum(nv(:)**2)).lt.1e-10) cycle ! Deal with interfaces aligned with cells
              nv=nv/sqrt(sum(nv(:)**2))

              is_done=.false.
              n=0
              do while(.not.is_done)
                 n=n+1
                 ! Form temp t2 vector
                 select case(n)
                 case(1); t2=0.0_WP; t2(3)=-1.0_WP
                 case(2); t2=0.0_WP; t2(2)=-1.0_WP
                 case(3); t2=0.0_WP; t2(1)=-1.0_WP
                 end select
                 ! Make sure t2 is not the same as n
                 if (abs(dot_product(nv,t2)).gt.0.6) cycle
                 ! Form real t1 & t2
                 t1=normalize(cross_product(nv,t2))
                 t2=normalize(cross_product(nv,t1))
                 is_done=.true.
              end do

              ! Construct rotation matrices: x'=Rot*x, x=iRot*x'
              Rot(1,:)=nv; Rot(2,:)=t1; Rot(3,:)=t2
              iRot=transpose(Rot)

              ! Cell center and size in rotated coordinate system
              Xo=matmul(Rot,(/xm(i),ym(j),zm(k)/));

              ! Fit polynomial to points in rotated coordinate system
              ! -----------------------------------------------------
              MTM=0.0_WP
              MTB=0.0_WP
              ! Loop over neighbors
              do kk=k-ac,k+ac
                 do jj=j-ac,j+ac
                    do ii=i-ac,i+ac
                       ! Add points
                       do nn=1,nptsLS(ii,jj,kk)
                          ! Point in rotated coordinate system
                          ptR=matmul(Rot,ptsLS(:,nn,ii,jj,kk))
                          ! Weight for this point
                          sig=GausSigma*meshsize(i,j,k)
                          weight=1.0_WP/(sig*sqrt(2.0_WP*pi)) &
                               *exp(-sum((ptR(:)-Xo(:))**2)/(2.0_WP*sig**2)) &
                               * ptsW(nn,ii,jj,kk)
                          ! Add point as constraint in least squares
                          ! x_n = sum_i (pt_t1(i)-t1o)^mono(i,1)*(pt_t2(i)-t2o)^mono(i,2) 
                          do row=1,nmono
                             do col=1,nmono
                                MTM(row,col) = MTM(row,col) + weight &
                                     * (ptR(2)-Xo(2))**mono(row,1) & ! t1
                                     * (ptR(3)-Xo(3))**mono(row,2) & ! t2
                                     * (ptR(2)-Xo(2))**mono(col,1) & ! t1
                                     * (ptR(3)-Xo(3))**mono(col,2)   ! t2
                             end do
                             MTB(row) = MTB(row) + weight &
                                  * (ptR(2)-Xo(2))**mono(row,1) & ! t1
                                  * (ptR(3)-Xo(3))**mono(row,2) & ! t2
                                  * (ptR(1)-Xo(1))                ! n
                          end do
                          ! Debug
                          if (i.eq.iout.and.j.eq.jout.and.k.eq.kout) then
                             nptsout=nptsout+1
                             ptsout(:,nptsout)=ptsLS(:,nn,ii,jj,kk)
                             ptsRout(:,nptsout)=ptR
                             print *,'ii,jj,kk,pt,w=',ii,jj,kk,ptsLS(:,nn,ii,jj,kk),ptsW(nn,ii,jj,kk)
                          end if
                       end do
                    end do
                 end do
              end do

              ! Least Squares Solution
              call solve_linear_system(MTM,MTB,a,nmono)

              ! Compute curvature
              H1=A(2)
              H2=A(3)
              H11=2.0_WP*A(4)
              H22=2.0_WP*A(6)
              H12=A(5)
              curv(i,j,k)=-(H11 + H22 + H11*H2**2 + H22*H1**2 - 2.0_WP*H12*H1*H2) &
                   / (1.0_WP + H1**2 + H2**2)**(1.5_WP)
              hasCurv(i,j,k)=1.0_WP

              ! Clip the curvature
              curv(i,j,k)=max(min(curv(i,j,k),1.0_WP/meshsize(i,j,k)),-1.0_WP/meshsize(i,j,k))

              ! Debug
              if (i.eq.iout.and.j.eq.jout.and.k.eq.kout) then
                 call pts2silo(nptsout,transpose(ptsout(:,1:nptsout)),'points.silo')
                 call pts2silo(nptsout,transpose(ptsRout(:,1:nptsout)),'pointsRot.silo')
                 print *,'a=',a
                 print *,'curv=',curv(i,j,k)
              end if

           end if
        end do
     end do
  end do

  ! Update curv in ghost cells
  call communication_border(curv)
              
  return
end subroutine multiphase_curvLS_calc
