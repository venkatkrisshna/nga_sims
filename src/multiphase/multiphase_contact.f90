module multiphase_contact
  use multiphase
  use multiphase_tools
  implicit none

  ! Logical
  logical :: use_contactangle
  
  ! Contact angle 
  real(WP) :: CAadv,CArec,CAtest
  real(WP), dimension(:,:,:,:), allocatable :: CA

  ! Contact point
  logical, dimension(:,:,:,:), allocatable :: hasCP
  real(WP), dimension(:,:,:,:), allocatable :: CPx,CPy,CPz


  ! y(jwall)=wall
  integer :: jwall

contains

  ! =============================== !
  ! Update contact point and normal !
  ! with specified contact angle    !
  ! =============================== !
  subroutine updateCPnNorm(s,i,j,k,CA)
    use multiphase_plic
    implicit none
    integer, intent(in) :: s,i,j,k
    real(WP), intent(in) :: CA
    !  real(WP) :: xtop,xbot,xmid

    ! Construct normal from CA
    call CA2norm(s,i,j,k,CA)

    ! Construct PLIC with new normal
    dist(s,i,j,k)=calc_dist(s,i,j,k, &
                      normx(s,i,j,k),normy(s,i,j,k),normz(s,i,j,k),VOF(s,i,j,k))

    ! Compute CP from PLIC
    call PLIC2CP(s,i,j,k)

    return
  end subroutine updateCPnNorm

  ! ===================== !
  ! Compute normal from a !
  ! contact angle         !
  ! ===================== !
  subroutine CA2norm(s,i,j,k,CA)
    use math
    implicit none
    integer,  intent(in) :: s,i,j,k
    real(WP), intent(in) :: CA
    real(WP), dimension(3) :: norm
  
    ! Construct normal from Ca
    if (CA.lt.epsilon(1.0_WP)) then ! CA = 0
       normx(s,i,j,k)= 0.0_WP
       normy(s,i,j,k)=+1.0_WP
       normz(s,i,j,k)= 0.0_WP
    else if (CA.gt.Pi-epsilon(1.0_WP)) then ! CA=pi
       normx(s,i,j,k)= 0.0_WP
       normy(s,i,j,k)=-1.0_WP
       normz(s,i,j,k)= 0.0_WP
    else 
       norm(1)=normx(s,i,j,k)
       norm(2)=sqrt(normx(s,i,j,k)**2+normz(s,i,j,k)**2)/(tan(CA)+sign(epsilon(1.0_WP),tan(CA)))
       norm(3)=normz(s,i,j,k)
       norm=norm/sqrt(sum(norm(:)**2))

       normx(s,i,j,k)=norm(1)
       normy(s,i,j,k)=norm(2)
       normz(s,i,j,k)=norm(3)
    end if

    return
  end subroutine CA2norm

  ! ======================= !
  ! Normal to Contact angle !
  !      0 <= CA <= pi      !
  ! ======================= !
  function norm2CA(s,i,j,k) result(CA)
    use math
    implicit none
    integer, intent(in) :: s,i,j,k
    real(WP) :: CA

    CA=atan2(sqrt(normx(s,i,j,k)**2+normz(s,i,j,k)**2),normy(s,i,j,k))

    if (CA.lt.0.0_WP) then ! Shift by Pi
       CA=CA+Pi
    end if
    
    return
  end function norm2CA

  ! ========================= !
  ! Computes closest point on !
  ! PLIC to cell center       !
  ! ========================= !
  subroutine PLIC2CP(s,i,j,k)
    implicit none
    integer, intent(in) :: s,i,j,k
    real(WP) :: t
    real(WP), dimension(2) :: A,B,P,AP,AB

    ! Find 2 points on PLIC
    if (abs(normx(s,i,j,k)).gt.abs(normz(s,i,j,k))) then
       A(2)=zs1(s,k); A(1)=(dist(s,i,j,k)-normy(s,i,j,k)*y(jwall)-normz(s,i,j,k)*A(2))/normx(s,i,j,k)
       B(2)=zs2(s,k); B(1)=(dist(s,i,j,k)-normy(s,i,j,k)*y(jwall)-normz(s,i,j,k)*B(2))/normx(s,i,j,k)
    else
       A(1)=xs1(s,i); A(2)=(dist(s,i,j,k)-normy(s,i,j,k)*y(jwall)-normx(s,i,j,k)*A(1))/normz(s,i,j,k)
       B(1)=xs2(s,i); B(2)=(dist(s,i,j,k)-normy(s,i,j,k)*y(jwall)-normx(s,i,j,k)*B(1))/normz(s,i,j,k)
    end if

    ! Cell center
    P=(/ xm(i)+subx(s)*dx(i),zm(k)+subz(s)*dz /)

    ! Define vectors
    AP=P-A
    AB=B-A
    t=(AP(1)*AB(1)+AP(2)*AB(2))/(AB(1)**2+AB(2)**2)

    ! Contact Point
    CPx(s,i,j,k)=A(1)+AB(1)*t
    CPy(s,i,j,k)=y(jwall)
    CPz(s,i,j,k)=A(2)+AB(2)*t

    return
  end subroutine PLIC2CP

  ! ==================== !
  ! Compute VOF given CA !
  ! and known CP & norm  !
  ! ==================== !
  function CA2VOF(s,i,j,k,CA) result(VOF)
    implicit none
    integer, intent(in) :: s,i,j,k
    real(WP), intent(in) :: CA
    real(WP) :: VOF
    real(WP), dimension(4)  :: plic_orig

    ! Store original PLIC
    plic_orig=(/normx(s,i,j,k),normy(s,i,j,k),normz(s,i,j,k),dist(s,i,j,k)/) 
    
    ! Construct PLIC from CP and CA
    call CA2norm(s,i,j,k,CA)
    dist(s,i,j,k)=normx(s,i,j,k)*CPx(s,i,j,k) &
         +        normy(s,i,j,k)*CPy(s,i,j,k) &
         +        normz(s,i,j,k)*CPz(s,i,j,k)

    ! Compute VOF
    VOF=PLIC2VOF(s,i,j,k)

    ! Reset PLIC
    normx(s,i,j,k)=plic_orig(1)
    normy(s,i,j,k)=plic_orig(2)
    normz(s,i,j,k)=plic_orig(3)
    dist (s,i,j,k)=plic_orig(4)

    return
  end function CA2VOF

  ! ================== !
  ! Compute PLIC from  !
  ! CP & VOF           !
  ! ================== !
  subroutine CPnVOF2PLIC(s,i,j,k)
    use math
    implicit none
    integer, intent(in) :: s,i,j,k
    integer :: it
    real(WP) :: myerror,uppB,lowB,myVOF,myCA
    integer, parameter :: itmax=1000
    real(WP), parameter :: min_error=1e-15_WP



    ! Initialize bisection on contact angle
    myerror=1.0_WP
    it=0
    if (CA2VOF(s,i,j,k,0.0_WP).lt.0.5_WP) then
       lowB=0.0_WP
       uppB=Pi
    else
       lowB=Pi
       uppB=0.0_WP
    end if

    do while (myerror.gt.min_error.and.it.lt.itmax)

       it=it+1

       ! Bisection method
       myCA =0.5_WP*(uppB+lowB)
       myVOF=CA2VOF(s,i,j,k,myCA)
              
       if (myVOF.lt.VOF(s,i,j,k)) then
          lowB=myCa
       else
          uppB=myCa
       end if

       ! Update error
       myerror=abs(VOF(s,i,j,k)-myVOF)

    end do
    
    ! Compute normal from CA
    call CA2norm(s,i,j,k,myCA)

    ! Compute PLIC that goes through CP
    dist(s,i,j,k)=normx(s,i,j,k)*CPx(s,i,j,k) &
         +        normy(s,i,j,k)*CPy(s,i,j,k) &
         +        normz(s,i,j,k)*CPz(s,i,j,k)
    
  end subroutine CPnVOF2PLIC

end module multiphase_contact

! ============== !
! Initialization !
! ============== !
subroutine multiphase_contact_init
  use multiphase_contact
  use math
  implicit none
  integer :: s,i,j,k

  ! Check if we are using contact angle 
  call parser_read('Use contact angle',use_contactangle,.false.)
  if (.not.use_contactangle) return

  ! Set jwall
  call parser_read('Contact angle j offset',jwall,0)
  jwall=jwall+jmin

  ! Read contact angles
  call parser_read('Advancing contact angle',CAadv)
  call parser_read('Receding contact angle',CArec)
  CAadv=CAadv*Pi/180_WP ! In radians
  CArec=CArec*Pi/180_WP

  ! Check advancing/receding contact angles
  if (CAadv.le.0.0_WP.or.CAadv.ge.pi) call die('Advancing contact angle must be between 0 and Pi')
  if (CArec.le.0.0_WP.or.CArec.ge.pi) call die('Receding contact angle must be between 0 and Pi')

  ! Allocate arrays
  allocate(CA   (8,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(CPx  (8,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(CPy  (8,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(CPz  (8,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(hasCP(8,imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))


  ! Determine initial contact points
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           do s=1,8
              if (VOF(s,i,j,k).gt.VOFlo.and.VOF(s,i,j,k).lt.VOFhi) then
                 call PLIC2CP(s,i,j,k)
                 hasCP(s,i,j,k)=.true.
              else
                 hasCP(s,i,j,k)=.false.
              end if
           end do
        end do
     end do
  end do

  return 
end subroutine multiphase_contact_init

! ======================= !
! Set interface normal on !
! bottom row of cells     !
! ======================= !
subroutine multiphase_contact_normal
  use multiphase_contact
  use multiphase_plic
  implicit none

  integer :: ss,s,i,j,k
  real(WP) :: VOFadv,VOFrec

  ! Check if we are using contact angle 
  if (.not.use_contactangle) return
  
  ! Update normal and contact point on bottom layer of cells
  if (jproc.eq.1) then
     j=jwall
     do k=kmin_,kmax_
        do i=imin_,imax_
           do ss=1,4 ! Loop over subcells on the bottom
              s=subface2subcell_y(ss)
              
              ! Only deal with cells with interface
              if (VOF(s,i,j,k).gt.VOFlo.and.VOF(s,i,j,k).lt.VOFhi) then

                 ! Create CP for cell if needed
                 if (.not.hasCP(s,i,j,k)) then
                    call PLIC2CP(s,i,j,k)
                    hasCP(s,i,j,k)=.true.
                 end if

                 ! Update normal with old CP & VOF
                 call CPnVOF2PLIC(s,i,j,k)

                 ! Compute VOF associated with advancing/receding contact angle            
                 VOFadv=CA2VOF(s,i,j,k,CAadv)
                 VOFrec=CA2VOF(s,i,j,k,CArec)

                if (VOF(s,i,j,k).ge.VOFrec.and.VOF(s,i,j,k).le.VOFadv) then
                    ! Update contact angle
                    CA(s,i,j,k)=norm2CA(s,i,j,k)
                 else
                    ! Update contact angle
                    if (VOF(s,i,j,k).ge.VOFadv) then
                       CA(s,i,j,k)=CAadv
                    else
                       CA(s,i,j,k)=CArec
                    end if

                    ! Update contact point and normal
                    call updateCPnNorm(s,i,j,k,CA(s,i,j,k))
                 end if
                 
                 ! Update PLIC with new normal
                 dist(s,i,j,k)=calc_dist(s,i,j,k, &
                      normx(s,i,j,k),normy(s,i,j,k),normz(s,i,j,k),VOF(s,i,j,k))

              else ! No interface
                 ! Reset CP logical
                 hasCP(s,i,j,k)=.false.
              end if
           end do
        end do
     end do
  end if

  ! Communicate normal
  do s=1,8
     call communication_border(normx(s,:,:,:))
     call communication_border(normy(s,:,:,:))
     call communication_border(normz(s,:,:,:))
     call boundary_symmetry_alldir(normx(s,:,:,:),normy(s,:,:,:),normz(s,:,:,:))
  end do

  return
end subroutine multiphase_contact_normal
