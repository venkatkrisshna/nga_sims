module multiphase_houc
  use multiphase_vof
  use scalar_houc
  implicit none
  
  ! Dummy
  
end module multiphase_houc


! ========================== !
! Initialize the houc module !
! ========================== !
subroutine multiphase_houc_init
  use multiphase_houc
  implicit none
  
  ! Initialize the scalar metrics
  call scalar_houc_init
  
  return
end subroutine multiphase_houc_init


! =========================================================== !
! Compute the residuals of the levelset equation              !
!                                                             !
! - velocity field @ n stored in Uold                         !
! - levelset field @ n stored in Gmid                         !
!                                                             !
! 3 working arrays of size (at least) (nx_+1)*(ny_+1)*(nz_+1) !
! =========================================================== !
subroutine multiphase_houc_residual
  use multiphase_houc
  use parallel
  use memory
  implicit none
  
  integer  :: i,j,k
  real(WP) :: rhs
  
  ! Now compute residual
  do k=kmin_-st1,kmax_+st2
     do j=jmin_-st1,jmax_+st2
        do i=imin_-st1,imax_+st2
           
           FX(i,j,k) = &
                -0.5_WP*(U(i,j,k)+abs(U(i,j,k)))*sum(houc_xp(i,j,:)*G(i-sthp1:i+sthp2,j,k)) &
                -0.5_WP*(U(i,j,k)-abs(U(i,j,k)))*sum(houc_xm(i,j,:)*G(i-sthm1:i+sthm2,j,k))
           
           FY(i,j,k) = &
                -0.5_WP*(V(i,j,k)+abs(V(i,j,k)))*sum(houc_yp(i,j,:)*G(i,j-sthp1:j+sthp2,k)) &
                -0.5_WP*(V(i,j,k)-abs(V(i,j,k)))*sum(houc_ym(i,j,:)*G(i,j-sthm1:j+sthm2,k))
           
           FZ(i,j,k) = &
                -0.5_WP*(W(i,j,k)+abs(W(i,j,k)))*sum(houc_zp(:)    *G(i,j,k-sthp1:k+sthp2)) &
                -0.5_WP*(W(i,j,k)-abs(W(i,j,k)))*sum(houc_zm(:)    *G(i,j,k-sthm1:k+sthm2))
           
        end do
     end do
  end do
  
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           rhs= sum(div_u(i,j,k,:)*FX(i-st1:i+st2,j,k))+&
                sum(div_v(i,j,k,:)*FY(i,j-st1:j+st2,k))+&
                sum(div_w(i,j,k,:)*FZ(i,j,k-st1:k+st2))
           resG(i,j,k) = -2.0_WP*G(i,j,k)+Gold(i,j,k) + ( Gold(i,j,k) + dt_old*rhs )
        end do
     end do
  end do
  
  return
end subroutine multiphase_houc_residual


! =========================================================== !
! Inverse the linear system obtained from the implicit scalar !
! transport equation                                          !
! =========================================================== !
subroutine multiphase_houc_inverse
  use multiphase_houc
  use parallel
  use memory
  use implicit
  use time_info
  implicit none
  
  integer  :: i,j,k,n,st
  real(WP) :: dt2
  
  dt2 = dt_old/2.0_WP
  
  ! If purely explicit return
  if (.not.implicit_any) return
  
  ! X-direction
  if (implicit_x .and. (npx.eq.1 .or. nhouc.le.3)) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              
              Ax(j,k,i,:) = 0.0_WP
              Ax(j,k,i,0) = 1.0_WP
              do st=-st1,st2
                 do n=-sthp1,sthp2
                    Ax(j,k,i,st+n) = Ax(j,k,i,st+n) + dt2*div_u(i,j,k,st)*0.5_WP*(U(i+st,j,k)+abs(U(i+st,j,k)))*houc_xp(i+st,j,n)
                 end do
                 do n=-sthm1,sthm2
                    Ax(j,k,i,st+n) = Ax(j,k,i,st+n) + dt2*div_u(i,j,k,st)*0.5_WP*(U(i+st,j,k)-abs(U(i+st,j,k)))*houc_xm(i+st,j,n)
                 end do
              end do
              
              Rx(j,k,i) = resG(i,j,k)
              
           end do
        end do
     end do
     call linear_solver_x(nhouc+2)
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Rx(j,k,i) = ResG(i,j,k)
           end do
        end do
     end do
  end if
  
  ! Y-direction
  if (implicit_y .and. (npy.eq.1 .or. nhouc.le.3)) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              
              Ay(i,k,j,:) = 0.0_WP
              Ay(i,k,j,0) = 1.0_WP
              do st=-st1,st2
                 do n=-sthp1,sthp2
                    Ay(i,k,j,st+n) = Ay(i,k,j,st+n) + dt2*div_v(i,j,k,st)*0.5_WP*(V(i,j+st,k)+abs(V(i,j+st,k)))*houc_yp(i,j+st,n)
                 end do
                 do n=-sthm1,sthm2
                    Ay(i,k,j,st+n) = Ay(i,k,j,st+n) + dt2*div_v(i,j,k,st)*0.5_WP*(V(i,j+st,k)-abs(V(i,j+st,k)))*houc_ym(i,j+st,n)
                 end do
              end do
              
              Ry(i,k,j) = Rx(j,k,i)
              
           end do
        end do
     end do
     if (icyl.eq.1 .and. jproc.eq.1) then
        do k=kmin_,kmax_
           do n=0,sthp1-1
              do st=-sthp1,-n-1
                 Ay(:,k,jmin+n,-n) = Ay(:,k,jmin+n,-n) + Ay(:,k,jmin+n,st)
                 Ay(:,k,jmin+n,st) = 0.0_WP
              end do
           end do
        end do
     end if
     call linear_solver_y(nhouc+2)
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Ry(i,k,j) = Rx(j,k,i)
           end do
        end do
     end do
  end if
  
  ! Z-direction
  if (implicit_z .and. (npz.eq.1 .or. nhouc.le.3)) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              
              Az(i,j,k,:) = 0.0_WP
              Az(i,j,k,0) = 1.0_WP
              do st=-st1,st2
                 do n=-sthp1,sthp2
                    Az(i,j,k,st+n) = Az(i,j,k,st+n) + dt2*div_w(i,j,k,st)*0.5_WP*(W(i,j,k+st)+abs(W(i,j,k+st)))*houc_zp(n)
                 end do
                 do n=-sthm1,sthm2
                    Az(i,j,k,st+n) = Az(i,j,k,st+n) + dt2*div_w(i,j,k,st)*0.5_WP*(W(i,j,k+st)-abs(W(i,j,k+st)))*houc_zm(n)
                 end do
              end do
              
              Rz(i,j,k) = Ry(i,k,j)
              
           end do
        end do
     end do
     call linear_solver_z(nhouc+2)
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ResG(i,j,k) = Rz(i,j,k)
           end do
        end do
     end do
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ResG(i,j,k) = Ry(i,k,j)
           end do
        end do
     end do
  end if
  
  return
end subroutine multiphase_houc_inverse
