! ================================ !
! Tools module: various operations !
! ================================ !
module multiphase_tools
  use multiphase
  use metric_generic
  use compgeom_lookup
  implicit none

contains

  ! Functions that return mesh locations for subcells
  ! ys2(s,j) |------------------------|
  !          |                        |
  !          |                        |
  !          |                        |
  ! ysm(s,j) |       subcell=s        |
  !          |                        |
  !          |                        |
  !          |                        |
  ! ys1(s,j) |------------------------|
  !       xs1(s,i)    xsm(s,i)     xs2(s,i)
  function xsm(s,i)
    implicit none
    real(WP) :: xsm
    integer, intent(in) :: s,i
    xsm=xm(i)+subx(s)*dx(i)
    return
  end function xsm
  
  function ysm(s,j)
    implicit none
    real(WP) :: ysm
    integer, intent(in) :: s,j
    ysm=ym(j)+suby(s)*dy(j)
    return
  end function ysm

  function zsm(s,k)
    implicit none
    real(WP) :: zsm
    integer, intent(in) :: s,k
    zsm=zm(k)+subz(s)*dz
    return
  end function zsm
  
  function xs1(s,i)
    implicit none
    real(WP) :: xs1
    integer, intent(in) :: s,i
    xs1=xm(i)+(subx(s) - 0.25_WP)*dx(i)
    return
  end function xs1

  function xs2(s,i)
    implicit none
    real(WP) :: xs2
    integer, intent(in) :: s,i
    xs2=xm(i)+(subx(s) + 0.25_WP)*dx(i)
    return
  end function xs2

  function ys1(s,j)
    implicit none
    real(WP) :: ys1
    integer, intent(in) :: s,j
    ys1=ym(j)+(suby(s) - 0.25_WP)*dy(j)
    return
  end function ys1

  function ys2(s,j)
    implicit none
    real(WP) :: ys2
    integer, intent(in) :: s,j
    ys2=ym(j)+(suby(s) + 0.25_WP)*dy(j)
    return
  end function ys2

  function zs1(s,k)
    implicit none
    real(WP) :: zs1
    integer, intent(in) :: s,k
    zs1=zm(k)+(subz(s) - 0.25_WP)*dz
    return
  end function zs1

  function zs2(s,k)
    implicit none
    real(WP) :: zs2
    integer, intent(in) :: s,k
    zs2=zm(k)+(subz(s) + 0.25_WP)*dz
    return
  end function zs2

  ! ============================= !
  ! Move vertex in time using RK4 !
  ! ============================= !
  function project(p1,mydt,myi,myj,myk) result(p2)
    use math
    implicit none
    real(WP), dimension(3) :: p2
    real(WP), dimension(3), intent(in) :: p1
    real(WP),               intent(in) :: mydt
    integer,                intent(in) :: myi,myj,myk
    real(WP), dimension(3) :: v1,v2,v3,v4
    v1=get_velocity(p1               ,myi,myj,myk)
    v2=get_velocity(p1+0.5_WP*mydt*v1,myi,myj,myk)
    v3=get_velocity(p1+0.5_WP*mydt*v2,myi,myj,myk)
    v4=get_velocity(p1+       mydt*v3,myi,myj,myk)
    p2=p1+mydt/6.0_WP*(v1+2.0_WP*v2+2.0_WP*v3+v4)
    return
  end function project

  ! ================================================ !
  ! Interpolate velocity to pos near cell (i0,j0,k0) !
  ! ================================================ !
  function get_velocity(pos,ii,jj,kk) result(vel)
    use masks
    implicit none
    real(WP), dimension(3) :: vel
    real(WP), dimension(3), intent(in) :: pos
    integer, intent(in) :: ii,jj,kk
    integer :: i0,j0,k0
    integer :: i,j,k
    real(WP) :: wx1,wy1,wz1
    real(WP) :: wx2,wy2,wz2

    ! clip starting ijk coordinates
    i0=max(min(imaxo_-1,ii),imino_)
    j0=max(min(jmaxo_-1,jj),jmino_)
    k0=max(min(kmaxo_-1,kk),kmino_)
    ! Interpolate U velocity ------------------------------
    ! Find right i index
    i=i0
    do while (pos(1)-x (i  ).lt.0.0_WP.and.i  .gt.imino_)
       i=i-1
    end do
    do while (pos(1)-x (i+1).ge.0.0_WP.and.i+1.lt.imaxo_)
       i=i+1
    end do
    ! Find right j index
    j=j0
    do while (pos(2)-ym(j  ).lt.0.0_WP.and.j  .gt.jmino_)
       j=j-1
    end do
    do while (pos(2)-ym(j+1).ge.0.0_WP.and.j+1.lt.jmaxo_)
       j=j+1
    end do
    ! Find right k index
    k=k0
    do while (pos(3)-zm(k  ).lt.0.0_WP.and.k  .gt.kmino_)
       k=k-1
    end do
    do while (pos(3)-zm(k+1).ge.0.0_WP.and.k+1.lt.kmaxo_)
       k=k+1
    end do
    ! Prepare tri-linear interpolation coefficients
    wx1=(pos(1)-x (i))/(x (i+1)-x (i)); wx2=1.0_WP-wx1
    if (mask_u(i,j).eq.3) then
       wy1=(pos(2)-y(j+1))/(ym(j+1)-y(j+1)); wy2=1.0_WP-wy1 ! Wall at top of j cell
    else
       wy1=(pos(2)-ym(j))/(ym(j+1)-ym(j)); wy2=1.0_WP-wy1
    end if
    wz1=(pos(3)-zm(k))/(zm(k+1)-zm(k)); wz2=1.0_WP-wz1
    ! Tri-linear interpolation of U
    vel(1)=wz1*(wy1*(wx1*U(i+1,j+1,k+1)  + &
         wx2*U(i  ,j+1,k+1)) + &
         wy2*(wx1*U(i+1,j  ,k+1)  + &
         wx2*U(i  ,j  ,k+1)))+ &
         wz2*(wy1*(wx1*U(i+1,j+1,k  )  + &
         wx2*U(i  ,j+1,k  )) + &
         wy2*(wx1*U(i+1,j  ,k  )  + &
         wx2*U(i  ,j  ,k  )))

    ! Interpolate V velocity ------------------------------
    ! Find right i index
    i=i0
    do while (pos(1)-xm(i  ).lt.0.0_WP.and.i  .gt.imino_)
       i=i-1
    end do
    do while (pos(1)-xm(i+1).ge.0.0_WP.and.i+1.lt.imaxo_)
       i=i+1
    end do
    ! Find right j index
    j=j0
    do while (pos(2)-y (j  ).lt.0.0_WP.and.j  .gt.jmino_)
       j=j-1
    end do
    do while (pos(2)-y (j+1).ge.0.0_WP.and.j+1.lt.jmaxo_)
       j=j+1
    end do
    ! Find right k index
    k=k0
    do while (pos(3)-zm(k  ).lt.0.0_WP.and.k  .gt.kmino_)
       k=k-1
    end do
    do while (pos(3)-zm(k+1).ge.0.0_WP.and.k+1.lt.kmaxo_)
       k=k+1
    end do
    ! Prepare tri-linear interpolation coefficients
    wx1=(pos(1)-xm(i))/(xm(i+1)-xm(i)); wx2=1.0_WP-wx1
    if (mask_u(i,j).eq.3) then
       wy1=0.0_WP; wy2=1.0_WP
    else       
       wy1=(pos(2)-y (j))/(y (j+1)-y (j)); wy2=1.0_WP-wy1
    end if
    wz1=(pos(3)-zm(k))/(zm(k+1)-zm(k)); wz2=1.0_WP-wz1
    ! Tri-linear interpolation of V
    vel(2)=wz1*(wy1*(wx1*V(i+1,j+1,k+1)  + &
         wx2*V(i  ,j+1,k+1)) + &
         wy2*(wx1*V(i+1,j  ,k+1)  + &
         wx2*V(i  ,j  ,k+1)))+ &
         wz2*(wy1*(wx1*V(i+1,j+1,k  )  + &
         wx2*V(i  ,j+1,k  )) + &
         wy2*(wx1*V(i+1,j  ,k  )  + &
         wx2*V(i  ,j  ,k  )))

    ! Interpolate W velocity ------------------------------
    ! Find right i index
    i=i0
    do while (pos(1)-xm(i  ).lt.0.0_WP.and.i  .gt.imino_)
       i=i-1
    end do
    do while (pos(1)-xm(i+1).ge.0.0_WP.and.i+1.lt.imaxo_)
       i=i+1
    end do
    ! Find right j index
    j=j0
    do while (pos(2)-ym(j  ).lt.0.0_WP.and.j  .gt.jmino_)
       j=j-1
    end do
    do while (pos(2)-ym(j+1).ge.0.0_WP.and.j+1.lt.jmaxo_)
       j=j+1
    end do
    ! Find right k index
    k=k0
    do while (pos(3)-z (k  ).lt.0.0_WP.and.k  .gt.kmino_)
       k=k-1
    end do
    do while (pos(3)-z (k+1).ge.0.0_WP.and.k+1.lt.kmaxo_)
       k=k+1
    end do
    ! Prepare tri-linear interpolation coefficients
    wx1 = (pos(1)-xm(i))/(xm(i+1)-xm(i)); wx2 = 1.0_WP - wx1
    wy1 = (pos(2)-ym(j))/(ym(j+1)-ym(j)); wy2 = 1.0_WP - wy1
    wz1 = (pos(3)-z (k))/(z (k+1)-z (k)); wz2 = 1.0_WP - wz1
    ! Tri-linear interpolation of W
    vel(3)=wz1*(wy1*(wx1*W(i+1,j+1,k+1)  + &
         wx2*W(i  ,j+1,k+1)) + &
         wy2*(wx1*W(i+1,j  ,k+1)  + &
         wx2*W(i  ,j  ,k+1)))+ &
         wz2*(wy1*(wx1*W(i+1,j+1,k  )  + &
         wx2*W(i  ,j+1,k  )) + &
         wy2*(wx1*W(i+1,j  ,k  )  + &
         wx2*W(i  ,j  ,k  )))
    return
  end function get_velocity

  ! ==================================================== !
  ! Interpolate scalars using mean and gradient in i,j,k !
  ! ==================================================== !
  ! Gas scalars
  function get_Gscalars(pos,i,j,k) result(mysc)
    use multiphase
    use scalar
    implicit none
    real(WP), dimension(nscalar) :: mysc
    real(WP), dimension(3), intent(in) :: pos
    integer, intent(in) :: i,j,k

    mysc(:) = SCold(i,j,k,:) &
         + gradSC(1,i,j,k,:)*(pos(1)-COMgas(1,i,j,k)) &
         + gradSC(2,i,j,k,:)*(pos(2)-COMgas(2,i,j,k)) &
         + gradSC(3,i,j,k,:)*(pos(3)-COMgas(3,i,j,k))

    return
  end function get_Gscalars
  ! Liquid scalars
  function get_Lscalars(pos,i,j,k) result(mysc)
    use multiphase
    use scalar
    implicit none
    real(WP), dimension(nscalar) :: mysc
    real(WP), dimension(3), intent(in) :: pos
    integer, intent(in) :: i,j,k

    mysc(:) = SCold(i,j,k,:) &
         + gradSC(1,i,j,k,:)*(pos(1)-COMliq(1,i,j,k)) &
         + gradSC(2,i,j,k,:)*(pos(2)-COMliq(2,i,j,k)) &
         + gradSC(3,i,j,k,:)*(pos(3)-COMliq(3,i,j,k))

    return
  end function get_Lscalars


  ! =============================== !
  ! Wetted area of x-face of P-cell !
  ! =============================== !
  function wetted_area_frac_x(i,j,k) result(wArea)
    implicit none
    integer, intent(in) :: i,j,k
    integer :: s,t,n,ss,ii,jj,kk
    real(WP) :: wArea,my_wArea
    integer, dimension(2) :: sub_i
    integer, dimension(4,2) :: sub_s
    real(WP), dimension(4,4,3) :: pts
    real(WP), dimension(3,3) :: tri

    ! Initalize wetted area
    wArea=0.0_WP

    ! Index of sub-cells on face
    sub_i(:)=(/i-1,i/)
    sub_s(1,:)=(/2,1/)
    sub_s(2,:)=(/4,3/)
    sub_s(3,:)=(/6,5/)
    sub_s(4,:)=(/8,7/)
    ! Points on corners of subface
    pts(1,1,:)=(/ x(i), y (j  ),z (k  ) /) ! Subface 1
    pts(1,2,:)=(/ x(i), ym(j  ),z (k  ) /)
    pts(1,3,:)=(/ x(i), y (j  ),zm(k  ) /)
    pts(1,4,:)=(/ x(i), ym(j  ),zm(k  ) /)
    pts(2,1,:)=(/ x(i), ym(j  ),z (k  ) /) ! Subface 2
    pts(2,2,:)=(/ x(i), y (j+1),z (k  ) /)
    pts(2,3,:)=(/ x(i), ym(j  ),zm(k  ) /)
    pts(2,4,:)=(/ x(i), y (j+1),zm(k  ) /)
    pts(3,1,:)=(/ x(i), y (j  ),zm(k  ) /) ! Subface 3
    pts(3,2,:)=(/ x(i), ym(j  ),zm(k  ) /)
    pts(3,3,:)=(/ x(i), y (j  ),z (k+1) /)
    pts(3,4,:)=(/ x(i), ym(j  ),z (k+1) /)
    pts(4,1,:)=(/ x(i), ym(j  ),zm(k  ) /) ! Subface 4
    pts(4,2,:)=(/ x(i), y (j+1),zm(k  ) /)
    pts(4,3,:)=(/ x(i), ym(j  ),z (k+1) /)
    pts(4,4,:)=(/ x(i), y (j+1),z (k+1) /)

    ! Loop over subfaces
    do s=1,4
       ! Loop over tris on subface
       do t=1,2
          do n=1,3
             tri(n,:)=pts(s,verts2tris(n,t),:)
          end do
          ! Loop over neighbors containing subface
          my_wArea=huge(1.0_WP)
          do n=1,2
             ! Index for this neighbor
             ss=sub_s(s,n)
             ii=sub_i(  n)
             jj=j
             kk=k
             ! Cut tri by interface and save minumum area
             my_wArea=min(my_wArea,tri2wArea(tri,ss,ii,jj,kk))
          end do
          wArea=wArea+my_wArea
       end do
    end do

    ! Normalize area
    wArea=wArea*dyi(j)*dzi

    return
  end function wetted_area_frac_x

  ! =============================== !
  ! Wetted area of y-face of P-cell !
  ! =============================== !
  function wetted_area_frac_y(i,j,k) result(wArea)
    implicit none
    integer, intent(in) :: i,j,k
    integer :: s,t,n,ss,ii,jj,kk
    real(WP) :: wArea,my_wArea
    integer, dimension(2) :: sub_j
    integer, dimension(4,2) :: sub_s
    real(WP), dimension(4,4,3) :: pts
    real(WP), dimension(3,3) :: tri

    ! Initalize wetted area
    wArea=0.0_WP

    ! Index of sub-cells on face
    sub_j(:)=(/j-1,j/)
    sub_s(1,:)=(/3,1/)
    sub_s(2,:)=(/4,2/)
    sub_s(3,:)=(/7,5/)
    sub_s(4,:)=(/8,6/)
    ! Points on corners of subface
    pts(1,1,:)=(/ x (i  ), y(j),z (k  ) /) ! Subface 1
    pts(1,2,:)=(/ xm(i  ), y(j),z (k  ) /)
    pts(1,3,:)=(/ x (i  ), y(j),zm(k  ) /)
    pts(1,4,:)=(/ xm(i  ), y(j),zm(k  ) /)
    pts(2,1,:)=(/ xm(i  ), y(j),z (k  ) /) ! Subface 2
    pts(2,2,:)=(/ x (i+1), y(j),z (k  ) /)
    pts(2,3,:)=(/ xm(i  ), y(j),zm(k  ) /)
    pts(2,4,:)=(/ x (i+1), y(j),zm(k  ) /)
    pts(3,1,:)=(/ x (i  ), y(j),zm(k  ) /) ! Subface 3
    pts(3,2,:)=(/ xm(i  ), y(j),zm(k  ) /)
    pts(3,3,:)=(/ x (i  ), y(j),z (k+1) /)
    pts(3,4,:)=(/ xm(i  ), y(j),z (k+1) /)
    pts(4,1,:)=(/ xm(i  ), y(j),zm(k  ) /) ! Subface 4
    pts(4,2,:)=(/ x (i+1), y(j),zm(k  ) /)
    pts(4,3,:)=(/ xm(i  ), y(j),z (k+1) /)
    pts(4,4,:)=(/ x (i+1), y(j),z (k+1) /)

    ! Loop over subfaces
    do s=1,4
       ! Loop over tris on subface
       do t=1,2
          do n=1,3
             tri(n,:)=pts(s,verts2tris(n,t),:)
          end do
          ! Loop over neighbors containing subface
          my_wArea=huge(1.0_WP)
          do n=1,2
             ! Index for this neighbor
             ss=sub_s(s,n)
             ii=i
             jj=sub_j(  n)
             kk=k
             ! Cut tri by interface and save minumum area
             my_wArea=min(my_wArea,tri2wArea(tri,ss,ii,jj,kk))
          end do
          wArea=wArea+my_wArea
       end do
    end do

    ! Normalize area
    wArea=wArea*dxi(i)*dzi

    return
  end function wetted_area_frac_y

  ! =============================== !
  ! Wetted area of z-face of P-cell !
  ! =============================== !
  function wetted_area_frac_z(i,j,k) result(wArea)
    implicit none
    integer, intent(in) :: i,j,k
    integer :: s,t,n,ss,ii,jj,kk
    real(WP) :: wArea,my_wArea
    integer, dimension(2) :: sub_k
    integer, dimension(4,2) :: sub_s
    real(WP), dimension(4,4,3) :: pts
    real(WP), dimension(3,3) :: tri

    ! Initalize wetted area
    wArea=0.0_WP

    ! Index of sub-cells on face
    sub_k(:)=(/k-1,k/)
    sub_s(1,:)=(/5,1/)
    sub_s(2,:)=(/6,2/)
    sub_s(3,:)=(/7,3/)
    sub_s(4,:)=(/8,4/)
    ! Points on corners of subface
    pts(1,1,:)=(/ x (i  ), y (j  ),z(k) /) ! Subface 1
    pts(1,2,:)=(/ xm(i  ), y (j  ),z(k) /)
    pts(1,3,:)=(/ x (i  ), ym(j  ),z(k) /)
    pts(1,4,:)=(/ xm(i  ), ym(j  ),z(k) /)
    pts(2,1,:)=(/ xm(i  ), y (j  ),z(k) /) ! Subface 2
    pts(2,2,:)=(/ x (i+1), y (j  ),z(k) /)
    pts(2,3,:)=(/ xm(i  ), ym(j  ),z(k) /)
    pts(2,4,:)=(/ x (i+1), ym(j  ),z(k) /)
    pts(3,1,:)=(/ x (i  ), ym(j  ),z(k) /) ! Subface 3
    pts(3,2,:)=(/ xm(i  ), ym(j  ),z(k) /)
    pts(3,3,:)=(/ x (i  ), y (j+1),z(k) /)
    pts(3,4,:)=(/ xm(i  ), y (j+1),z(k) /)
    pts(4,1,:)=(/ xm(i  ), ym(j  ),z(k) /) ! Subface 4
    pts(4,2,:)=(/ x (i+1), ym(j  ),z(k) /)
    pts(4,3,:)=(/ xm(i  ), y (j+1),z(k) /)
    pts(4,4,:)=(/ x (i+1), y (j+1),z(k) /)    
    ! Loop over subfaces
    do s=1,4
       ! Loop over tris on subface
       do t=1,2
          do n=1,3
             tri(n,:)=pts(s,verts2tris(n,t),:)
          end do
          ! Loop over neighbors containing subface
          my_wArea=huge(1.0_WP)
          do n=1,2
             ! Index for this neighbor
             ss=sub_s(s,n)
             ii=i 
             jj=j
             kk=sub_k(  n)
             ! Cut tri by interface and save minumum area
             my_wArea=min(my_wArea,tri2wArea(tri,ss,ii,jj,kk))
          end do
          wArea=wArea+my_wArea
       end do
    end do

    ! Normalize area
    wArea=wArea*dxi(i)*dyi(j)

    return
  end function wetted_area_frac_z


  ! =================== !
  ! Cut tri by PLIC and !
  ! return wetted area  !
  ! =================== !
  function tri2wArea(tri,s,i,j,k) result(wArea)
    implicit none
    ! I/O
    real(WP) :: wArea
    real(WP), dimension(3,3), intent(in) :: tri
    integer, intent(in) :: s,i,j,k
    ! Working variables
    integer :: n,nn
    integer :: case,v1,v2
    real(WP) :: mu
    real(WP), dimension(3) :: a,b,d
    real(WP), dimension(5,3) :: vert,newtri

    ! Cut by PLIC
    d(:) = normx(s,i,j,k)*tri(:,1) &
         + normy(s,i,j,k)*tri(:,2) &
         + normz(s,i,j,k)*tri(:,3) &
         - dist (s,i,j,k)

    ! Find cut case
    case=1+int(0.5_WP+sign(0.5_WP,d(1)))+&
         2*int(0.5_WP+sign(0.5_WP,d(2)))+&
         4*int(0.5_WP+sign(0.5_WP,d(3)))

    ! Copy vertices
    do n=1,3
       vert(n,:)=tri(n,:)
    end do

    ! Create interpolated vertices on cut plane
    do n=1,cuttri_nvert(case)
       v1=cuttri_v1(n,case); v2=cuttri_v2(n,case)
       mu=min(1.0_WP,max(0.0_WP,-d(v1)/(sign(abs(d(v2)-d(v1))+tiny(1.0_WP),d(v2)-d(v1)))))
       vert(3+n,:)=(1.0_WP-mu)*vert(v1,:)+mu*vert(v2,:)
    end do

    ! Compute new tris in liquid and compute area
    wArea=0.0_WP
    do n=cutTri_np(case)+1,cutTri_np(case)+cutTri_nn(case)
       ! For new tri
       do nn=1,3
          newtri(nn,:)=vert(cuttri_v(nn,n,case),:)
       end do
       ! Compute tri area
       a=newtri(1,:)-newtri(3,:)
       b=newtri(2,:)-newtri(3,:)
       wArea = wArea + 0.5_WP*abs( &
            + a(1)*(b(2)-b(3)) &
            - a(2)*(b(1)-b(3)) &
            + a(3)*(b(1)-b(2)))

    end do

    return
  end function tri2wArea

  ! ======================== !
  ! Compute liquid volume in !
  ! subcell based on PLIC    !
  ! ======================== !
  function PLIC2VOF(s,i,j,k) result(myVOF)
    use compgeom_lookup
    implicit none
    integer, intent(in) :: s,i,j,k
    real(WP) :: myVOF
    integer :: ntet,case,n,nn,v1,v2
    real(WP), dimension(3,4) :: tet,newtet
    real(WP), dimension(4) :: d
    real(WP), dimension(3,8) :: vert,cellvert
    real(WP) :: mu,my_vol
    real(WP), dimension(3) :: a,b,c    

    ! Initialize myVOF
    myVOF=0.0_WP

    ! Cell vertices
    cellvert(:,1)=(/xs1(s,i),ys1(s,j),zs1(s,k)/)
    cellvert(:,2)=(/xs2(s,i),ys1(s,j),zs1(s,k)/)
    cellvert(:,3)=(/xs1(s,i),ys2(s,j),zs1(s,k)/)
    cellvert(:,4)=(/xs2(s,i),ys2(s,j),zs1(s,k)/)
    cellvert(:,5)=(/xs1(s,i),ys1(s,j),zs2(s,k)/)
    cellvert(:,6)=(/xs2(s,i),ys1(s,j),zs2(s,k)/)
    cellvert(:,7)=(/xs1(s,i),ys2(s,j),zs2(s,k)/)
    cellvert(:,8)=(/xs2(s,i),ys2(s,j),zs2(s,k)/)

    ! Compute VOF on this subcell
    do ntet=1,5
       ! Create tet
        do nn=1,4
           tet(:,nn)=cellvert(:,verts2tets(nn,ntet))
        end do
        ! Cut by PLIC
        d(:) = normx(s,i,j,k)*tet(1,:) &
             + normy(s,i,j,k)*tet(2,:) &
             + normz(s,i,j,k)*tet(3,:) &
             - dist (s,i,j,k)
        ! Find cut case
        case=1+int(0.5_WP+sign(0.5_WP,d(1)))+&
             2*int(0.5_WP+sign(0.5_WP,d(2)))+&
             4*int(0.5_WP+sign(0.5_WP,d(3)))+&
             8*int(0.5_WP+sign(0.5_WP,d(4)))
        ! Copy vertices
        do n=1,4
           vert(:,n)=tet(:,n)
        end do
        ! Create interpolated vertices on cut plane
        do n=1,cut_nvert(case)
           v1=cut_v1(n,case); v2=cut_v2(n,case)
           mu=min(1.0_WP,max(0.0_WP,-d(v1)/(sign(abs(d(v2)-d(v1))+tiny(1.0_WP),d(v2)-d(v1)))))
           vert(:,4+n)=(1.0_WP-mu)*vert(:,v1)+mu*vert(:,v2)
        end do
        ! Create new tets in the liquid
        do n=cut_ntets(case),cut_nntet(case),-1
           ! Form new tet
           do nn=1,4
              newtet(:,nn)=vert(:,cut_vtet(nn,n,case))
           end do
           ! Compute tet volume
           a=newtet(:,1)-newtet(:,4)
           b=newtet(:,2)-newtet(:,4)
           c=newtet(:,3)-newtet(:,4)
           my_vol=abs(a(1)*(b(2)*c(3)-c(2)*b(3)) &
                -     a(2)*(b(1)*c(3)-c(1)*b(3)) &
                +     a(3)*(b(1)*c(2)-c(1)*b(2)))/6.0_WP
           ! Update liquid volume
           myVOF=myVOF+my_vol
        end do
     end do

     ! Finish computing VOF
     myVOF=myVOF/(0.125_WP*(dx(i)*dy(j)*dz))
    
    return
  end function PLIC2VOF

end module multiphase_tools


! ========================================= !
! Compute center of mass for velocity cells !
! ========================================= !
subroutine multiphase_centerofmass
  use multiphase_tools
  implicit none
  integer :: i,j,k,s
  integer :: n,nn,ntet,ii,jj,kk
  real(WP), dimension(:,:,:,:), pointer :: COMlx,COMly,COMlz,massl
  real(WP), dimension(:,:,:,:), pointer :: COMgx,COMgy,COMgz,massg
  real(WP) :: mymass,mymassl,mymassg
  real(WP), dimension(2) :: xsub,ysub,zsub
  real(WP), dimension(3,8) :: cellvert
  real(WP), dimension(3,4) :: tet
  integer :: case,v1,v2
  real(WP) :: mu,my_vol
  real(WP), dimension(4) :: d
  real(WP), dimension(3,8) :: vert
  real(WP), dimension(3,4) :: newtet
  real(WP), dimension(3) :: a,b,c

  ! Point to temp array and initialize to 0
  COMlx => tmp4d1; COMly => tmp4d2; COMlz => tmp4d3; massl => tmp4d4;
  COMgx => tmp4d5; COMgy => tmp4d6; COMgz => tmp4d7; massg => tmp4d8;
  ! Initialze arrays to zero
  COMlx=0.0_WP; COMly=0.0_WP; COMlz=0.0_WP; massl=0.0_WP
  COMgx=0.0_WP; COMgy=0.0_WP; COMgz=0.0_WP; massg=0.0_WP
  
  ! Loop over domain can compute COM and mass for each subcell
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           ! Loop over subcells 
           do s=1,8
              ! Subcell x,y,z bounds (compute once)
              xsub(1)=xs1(s,i); xsub(2)=xs2(s,i)
              ysub(1)=ys1(s,j); ysub(2)=ys2(s,j)
              zsub(1)=zs1(s,k); zsub(2)=zs2(s,k)
              ! Update COM and mass
              if (VOF(s,i,j,k).lt.VOFlo) then  ! Gas subcell
                 mymass = rho_g*(xsub(2)-xsub(1))*(ysub(2)-ysub(1))*(zsub(2)-zsub(1))
                 COMgx(s,i,j,k) = COMgx(s,i,j,k) + mymass*0.5_WP*sum(xsub(:))
                 COMgy(s,i,j,k) = COMgy(s,i,j,k) + mymass*0.5_WP*sum(ysub(:))
                 COMgz(s,i,j,k) = COMgz(s,i,j,k) + mymass*0.5_WP*sum(zsub(:))
                 massg(s,i,j,k) = massg(s,i,j,k) + mymass
              else if (VOF(s,i,j,k).gt.VOFhi) then ! Liquid subcell
                 mymass = rho_l*(xsub(2)-xsub(1))*(ysub(2)-ysub(1))*(zsub(2)-zsub(1))
                 COMlx(s,i,j,k) = COMlx(s,i,j,k) + mymass*0.5_WP*sum(xsub(:))
                 COMly(s,i,j,k) = COMly(s,i,j,k) + mymass*0.5_WP*sum(ysub(:))
                 COMlz(s,i,j,k) = COMlz(s,i,j,k) + mymass*0.5_WP*sum(zsub(:))
                 massl(s,i,j,k) = massl(s,i,j,k) + mymass
              else ! Gas & Liquid subcell
                 ! Vertices on subcell corners
                 n=0
                 do kk=1,2
                    do jj=1,2
                       do ii=1,2
                          n=n+1
                          cellvert(:,n)=(/xsub(ii),ysub(jj),zsub(kk)/)
                       end do
                    end do
                 end do
                 ! Break subcell into tets
                 do ntet=1,5
                    ! Create tet
                    do nn=1,4
                       tet(:,nn)=cellvert(:,verts2tets(nn,ntet))
                    end do
                    ! Cut by PLIC
                    d(:) = normx(s,i,j,k)*tet(1,:) &
                         + normy(s,i,j,k)*tet(2,:) &
                         + normz(s,i,j,k)*tet(3,:) &
                         - dist (s,i,j,k)
                    ! Find cut case
                    case=1+int(0.5_WP+sign(0.5_WP,d(1)))+&
                         2*int(0.5_WP+sign(0.5_WP,d(2)))+&
                         4*int(0.5_WP+sign(0.5_WP,d(3)))+&
                         8*int(0.5_WP+sign(0.5_WP,d(4)))
                    ! Copy vertices
                    do n=1,4
                       vert(:,n)=tet(:,n)
                    end do
                    ! Create interpolated vertices on cut plane
                    do n=1,cut_nvert(case)
                       v1=cut_v1(n,case); v2=cut_v2(n,case)
                       mu=min(1.0_WP,max(0.0_WP,-d(v1)/(sign(abs(d(v2)-d(v1))+tiny(1.0_WP),d(v2)-d(v1)))))
                       vert(:,4+n)=(1.0_WP-mu)*vert(:,v1)+mu*vert(:,v2)
                    end do
                    ! Create new tets in the liquid
                    do n=cut_ntets(case),cut_nntet(case),-1
                       ! Form new tet
                       do nn=1,4
                          newtet(:,nn)=vert(:,cut_vtet(nn,n,case))
                       end do
                       ! Compute tet volume
                       a=newtet(:,1)-newtet(:,4)
                       b=newtet(:,2)-newtet(:,4)
                       c=newtet(:,3)-newtet(:,4)
                       my_vol=abs(a(1)*(b(2)*c(3)-c(2)*b(3)) &
                            -     a(2)*(b(1)*c(3)-c(1)*b(3)) &
                            +     a(3)*(b(1)*c(2)-c(1)*b(2)))/6.0_WP
                       ! Update subcell COM
                       COMlx(s,i,j,k) = COMlx(s,i,j,k) + rho_l*my_vol*0.25_WP*sum(newtet(1,:))
                       COMly(s,i,j,k) = COMly(s,i,j,k) + rho_l*my_vol*0.25_WP*sum(newtet(2,:))
                       COMlz(s,i,j,k) = COMlz(s,i,j,k) + rho_l*my_vol*0.25_WP*sum(newtet(3,:))
                       massl(s,i,j,k) = massl(s,i,j,k) + rho_l*my_vol

                       
                    end do
                    ! Create new tets in the gas
                    do n=1,cut_nntet(case)-1
                       ! Form new tet
                       do nn=1,4
                          newtet(:,nn)=vert(:,cut_vtet(nn,n,case))
                       end do
                       ! Compute tet volume
                       a=newtet(:,1)-newtet(:,4)
                       b=newtet(:,2)-newtet(:,4)
                       c=newtet(:,3)-newtet(:,4)
                       my_vol=abs(a(1)*(b(2)*c(3)-c(2)*b(3)) &
                            -     a(2)*(b(1)*c(3)-c(1)*b(3)) &
                            +     a(3)*(b(1)*c(2)-c(1)*b(2)))/6.0_WP
                       ! Update subcell COM
                       COMgx(s,i,j,k) = COMgx(s,i,j,k) + rho_g*my_vol*0.25_WP*sum(newtet(1,:))
                       COMgy(s,i,j,k) = COMgy(s,i,j,k) + rho_g*my_vol*0.25_WP*sum(newtet(2,:))
                       COMgz(s,i,j,k) = COMgz(s,i,j,k) + rho_g*my_vol*0.25_WP*sum(newtet(3,:))
                       massg(s,i,j,k) = massg(s,i,j,k) + rho_g*my_vol
                    end do
                 end do
              end if
           end do
        end do
     end do
  end do

  ! Note at this point COMl and COMg have not been normalized by the mass
  ! That is the actuall center of mass is COMlx/massl,

  ! Combine subcell barycenters to find scalar barycenters
  COMliq=0.0_WP
  COMgas=0.0_WP
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           if (VOFavg(i,j,k).lt.VOFlo.or.VOFavg(i,j,k).gt.VOFhi) then ! Gas/liq cell
              COMliq(:,i,j,k)=(/ xm(i),ym(j),zm(k) /)
              COMgas(:,i,j,k)=(/ xm(i),ym(j),zm(k) /)
           else ! Mixed cell
              ! Zero mass in this subcell
              mymassl=0.0_WP
              mymassg=0.0_WP
              ! Loop over subcells
              do s=1,8
                 COMliq(1,i,j,k) = COMliq(1,i,j,k) + COMlx(s,i,j,k)
                 COMliq(2,i,j,k) = COMliq(2,i,j,k) + COMly(s,i,j,k)
                 COMliq(3,i,j,k) = COMliq(3,i,j,k) + COMlz(s,i,j,k)
                 mymassl = mymassl + massl(s,i,j,k)
                 COMgas(1,i,j,k) = COMgas(1,i,j,k) + COMgx(s,i,j,k)
                 COMgas(2,i,j,k) = COMgas(2,i,j,k) + COMgy(s,i,j,k)
                 COMgas(3,i,j,k) = COMgas(3,i,j,k) + COMgz(s,i,j,k)
                 mymassg = mymassg + massg(s,i,j,k)
              end do
              ! Finish computing COMs
              COMliq(:,i,j,k)=COMliq(:,i,j,k)/mymassl
              COMgas(:,i,j,k)=COMgas(:,i,j,k)/mymassg
           end if
        end do
     end do
  end do 
  
  ! Combine subcell barycenters to find velocity cell barycenters
  COMu=0.0_WP
  COMv=0.0_WP
  COMw=0.0_WP
  ! U cells
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_+1,imaxo_
           ! Zero mass in this subcell
           mymass=0.0_WP
           ! Loop over subcells
           do s=1,8
              COMu(1,i,j,k) = COMu(1,i,j,k) + COMlx(s,i+u2sub_i(s),j,k)+COMgx(s,i+u2sub_i(s),j,k)
              COMu(2,i,j,k) = COMu(2,i,j,k) + COMly(s,i+u2sub_i(s),j,k)+COMgy(s,i+u2sub_i(s),j,k)
              COMu(3,i,j,k) = COMu(3,i,j,k) + COMlz(s,i+u2sub_i(s),j,k)+COMgz(s,i+u2sub_i(s),j,k)
              mymass = mymass + massl(s,i+u2sub_i(s),j,k) + massg(s,i+u2sub_i(s),j,k)
           end do
           ! Finish computing COM
           COMu(:,i,j,k)=COMu(:,i,j,k)/mymass
        end do
     end do
  end do 
  ! V cells
  do k=kmino_,kmaxo_
     do j=jmino_+1,jmaxo_
        do i=imino_,imaxo_
           ! Zero mass in this subcell
           mymass=0.0_WP
           ! Loop over subcells
           do s=1,8
              COMv(1,i,j,k) = COMv(1,i,j,k) + COMlx(s,i,j+v2sub_j(s),k)+COMgx(s,i,j+v2sub_j(s),k)
              COMv(2,i,j,k) = COMv(2,i,j,k) + COMly(s,i,j+v2sub_j(s),k)+COMgy(s,i,j+v2sub_j(s),k)
              COMv(3,i,j,k) = COMv(3,i,j,k) + COMlz(s,i,j+v2sub_j(s),k)+COMgz(s,i,j+v2sub_j(s),k)
              mymass = mymass + massl(s,i,j+v2sub_j(s),k) + massg(s,i,j+v2sub_j(s),k)
           end do
           ! Finish computing COM
           COMv(:,i,j,k)=COMv(:,i,j,k)/mymass
        end do
     end do
  end do 
  ! W cells
  do k=kmino_+1,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           ! Zero mass in this subcell
           mymass=0.0_WP
           ! Loop over subcells
           do s=1,8
              COMw(1,i,j,k) = COMw(1,i,j,k) + COMlx(s,i,j,k+w2sub_k(s))+COMgx(s,i,j,k+w2sub_k(s))
              COMw(2,i,j,k) = COMw(2,i,j,k) + COMly(s,i,j,k+w2sub_k(s))+COMgy(s,i,j,k+w2sub_k(s))
              COMw(3,i,j,k) = COMw(3,i,j,k) + COMlz(s,i,j,k+w2sub_k(s))+COMgz(s,i,j,k+w2sub_k(s))
              mymass = mymass + massl(s,i,j,k+w2sub_k(s)) + massg(s,i,j,k+w2sub_k(s))
           end do
           ! Finish computing COM
           COMw(:,i,j,k)=COMw(:,i,j,k)/mymass
        end do
     end do
  end do

  return
end subroutine multiphase_centerofmass

! ============================================ !
! Gradients for second-order reconstruction    !
!  velocity: ur = u + gradU . (x-COMu)         !
!  scalars: SCr = SC + gradSC . (x-COMliq/gas) !
! ============================================ !
subroutine multiphase_reconstruct
  use multiphase
  use math
  implicit none
  integer :: i,j,k,isc
  ! integer :: ii,jj,kk
  integer :: dir
  ! integer :: row,col
  ! real(WP), dimension(3,3) ::   UA,VA,WA
  ! real(WP), dimension(3) :: sol,UB,VB,WB
  ! real(WP), dimension(3,3,nscalar) :: SCA
  ! real(WP), dimension(3,nscalar) :: SCB
  ! real(WP), dimension(3) :: Zs
  ! real(WP) :: Ys,weight

  ! Initialize values
  gradU  = 0.0_WP
  gradV  = 0.0_WP
  gradW  = 0.0_WP
  gradSC = 0.0_WP

  ! Use first order reconstructions
  ! return

    ! Use finite difference opperators
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Gradient of U
           gradU(1,i,j,k)=mingrad( &
                (u(i+1,j,k)-u(i  ,j,k))/(x (i+1)-x (i  )), &
                (u(i  ,j,k)-u(i-1,j,k))/(x (i  )-x (i-1)), &
                (u(i+1,j,k)-u(i-1,j,k))/(x (i+1)-x (i-1)))
           gradU(2,i,j,k)=mingrad( &
                (u(i,j+1,k)-u(i,j  ,k))/(ym(j+1)-ym(j  )), &
                (u(i,j  ,k)-u(i,j-1,k))/(ym(j  )-ym(j-1)), &
                (u(i,j+1,k)-u(i,j-1,k))/(ym(j+1)-ym(j-1)))
           gradU(3,i,j,k)=mingrad( &
                (u(i,j,k+1)-u(i,j,k  ))/(zm(k+1)-zm(k  )), &
                (u(i,j,k  )-u(i,j,k-1))/(zm(k  )-zm(k-1)), &
                (u(i,j,k+1)-u(i,j,k-1))/(zm(k+1)-zm(k-1)))
           ! Gradient of V
           gradV(1,i,j,k)=mingrad( &
                (v(i+1,j,k)-v(i  ,j,k))/(xm(i+1)-xm(i  )), &
                (v(i  ,j,k)-v(i-1,j,k))/(xm(i  )-xm(i-1)), &
                (v(i+1,j,k)-v(i-1,j,k))/(xm(i+1)-xm(i-1)))
           gradV(2,i,j,k)=mingrad( &
                (v(i,j+1,k)-v(i,j  ,k))/(y (j+1)-y (j  )), &
                (v(i,j  ,k)-v(i,j-1,k))/(y (j  )-y (j-1)), &
                (v(i,j+1,k)-v(i,j-1,k))/(y (j+1)-y (j-1)))
           gradV(3,i,j,k)=mingrad( &
                (v(i,j,k+1)-v(i,j,k  ))/(zm(k+1)-zm(k  )), &
                (v(i,j,k  )-v(i,j,k-1))/(zm(k  )-zm(k-1)), &
                (v(i,j,k+1)-v(i,j,k-1))/(zm(k+1)-zm(k-1)))
           ! Gradient of W
           gradW(1,i,j,k)=mingrad( &
                (w(i+1,j,k)-w(i  ,j,k))/(xm(i+1)-xm(i  )), &
                (w(i  ,j,k)-w(i-1,j,k))/(xm(i  )-xm(i-1)), &
                (w(i+1,j,k)-w(i-1,j,k))/(xm(i+1)-xm(i-1)))
           gradW(2,i,j,k)=mingrad( &
                (w(i,j+1,k)-w(i,j  ,k))/(ym(j+1)-ym(j  )), &
                (w(i,j  ,k)-w(i,j-1,k))/(ym(j  )-ym(j-1)), &
                (w(i,j+1,k)-w(i,j-1,k))/(ym(j+1)-ym(j-1)))
           gradW(3,i,j,k)=mingrad( &
                (w(i,j,k+1)-w(i,j,k  ))/(z (k+1)-z (k  )), &
                (w(i,j,k  )-w(i,j,k-1))/(z (k  )-z (k-1)), &
                (w(i,j,k+1)-w(i,j,k-1))/(z (k+1)-z (k-1)))
        end do
     end do
  end do


  ! ! Compute gradients using density weighted least squares
  ! do k=kmin_,kmax_
  !    do j=jmin_,jmax_
  !       do i=imin_,imax_
  !          ! Zero least squares matrices
  !          UA=0.0_WP; VA=0.0_WP; WA=0.0_WP; SCA=0.0_WP
  !          UB=0.0_WP; VB=0.0_WP; WB=0.0_WP; SCB=0.0_WP
  !          ! Loop over neighbors and compute density weighted least squares matrices
  !          do kk=k-1,k+1
  !             do jj=j-1,j+1
  !                do ii=i-1,i+1
  !                   ! Velocities
  !                   Zs=COMu(:,ii,jj,kk)-COMu(:,i,j,k); Ys=u(ii,jj,kk)-u(i,j,k); weight=rho_U(ii,jj,kk); call update_LS_mat(UA,UB,Zs,Ys,weight)
  !                   Zs=COMv(:,ii,jj,kk)-COMv(:,i,j,k); Ys=v(ii,jj,kk)-v(i,j,k); weight=rho_V(ii,jj,kk); call update_LS_mat(VA,VB,Zs,Ys,weight)
  !                   Zs=COMw(:,ii,jj,kk)-COMw(:,i,j,k); Ys=w(ii,jj,kk)-w(i,j,k); weight=rho_W(ii,jj,kk); call update_LS_mat(WA,WB,Zs,Ys,weight)
  !                   ! Scalars
  !                   do isc=1,nscalar
  !                      if (nint(SCphase(isc)).eq.0.and.max(VOFavg(i,j,k),VOFavg(ii,jj,kk)).lt.VOFhi) then ! Gas scalar in cell with some gas
  !                         Zs=COMgas(:,ii,jj,kk)-COMgas(:,i,j,k); Ys=SC(ii,jj,kk,isc)-SC(i,j,k,isc); weight=1.0_WP; call update_LS_mat(SCA,SCB,Zs,Ys,weight)
  !                      else if (nint(SCphase(isc)).eq.1.and.min(VOFavg(i,j,k),VOFavg(ii,jj,kk)).gt.VOFlo) then ! Liq scalar in cell with some liq
  !                         Zs=COMliq(:,ii,jj,kk)-COMliq(:,i,j,k); Ys=SC(ii,jj,kk,isc)-SC(i,j,k,isc); weight=1.0_WP; call update_LS_mat(SCA,SCB,Zs,Ys,weight)
  !                      end if
  !                   end do
  !                 end do
  !             end do
  !          end do
  !          ! Solve for reconstructions
  !          call solve_linear_system_safe(UA,UB,sol,3); gradU(:,i,j,k)=sol
  !          call solve_linear_system_safe(VA,VB,sol,3); gradV(:,i,j,k)=sol
  !          call solve_linear_system_safe(WA,WB,sol,3); gradW(:,i,j,k)=sol
  !          do isc=1,nscalar
  !             call solve_linear_system_safe(SCA(:,:,isc),SCB(:,isc),sol,3); gradSC(:,i,j,k,isc)=sol
  !          end do
  !       end do
  !    end do
  ! end do
 
  ! Communication
  do dir=1,3
     call boundary_update_border(gradU(dir,:,:,:),'+','ym')
     call boundary_update_border(gradV(dir,:,:,:),'+','ym')
     call boundary_update_border(gradW(dir,:,:,:),'+','ym')
     do isc=1,nscalar
        call boundary_update_border(gradSC(dir,:,:,:,isc),'+','ym')
     end do
  end do


  ! ! Try without 2nd order SC reconstruction
  !gradSC=0.0_WP
  
  return

contains
  ! Minimum of three gradients keeping their sign
  function mingrad(g1,g2,g3) result(g)
    implicit none
    real(WP), intent(in) :: g1,g2,g3
    real(WP) :: g
    g=g1
    if (abs(g2).lt.abs(g)) g=g2
    if (abs(g3).lt.abs(g)) g=g3
  end function mingrad

  ! ! Update Least Squares matrices by adding contribution from this data point
  ! subroutine update_LS_mat(A,B,Zs,Ys,W)
  !   implicit none
  !   real(WP), dimension(3,3), intent(inout) :: A
  !   real(WP), dimension(3), intent(inout) :: B
  !   real(WP), dimension(3), intent(in) :: Zs
  !   real(WP), intent(in) :: Ys,W
  !   integer :: col,row

  !   ! A matrix
  !    do col=1,3
  !       do row=1,3
  !          A(row,col) = A(row,col) + Zs(row)*Zs(col)*W
  !       end do
  !    end do
  !    ! B matrix
  !    do row=1,3
  !       B(row) = B(row) + Zs(row)*Ys*W
  !    end do
     
  !    return
  !  end subroutine update_LS_mat
end subroutine multiphase_reconstruct


