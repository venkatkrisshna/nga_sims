module inflow_file
  use inflow
  use parallel
  implicit none
  
  ! Communicator for reading inflow from file
  integer :: inflow_comm,inflowy_comm
  integer :: file_inflow,file_inflowy
  
  ! Name of inflow file and file id
  character(len=str_medium) :: filename,filenamey
  integer :: ifile,ifiley
  
  ! Variables
  integer :: inflow_nvar,inflowy_nvar
  character(len=str_short), dimension(:), allocatable :: inflow_name,inflowy_name
  real(WP), dimension(:,:,:), allocatable :: inflow_prev,inflow_next,inflowy_prev,inflowy_next
  
  ! Axis variables
  real(WP), dimension(:), allocatable :: Uaxis,Vaxis,Waxis
  real(WP), dimension(:), allocatable :: VOFaxis
  real(WP), dimension(:,:), allocatable :: SCaxis
  
  ! Mapping between inflow variables and real variables
  integer :: index_u,index_v,index_w
  integer :: index_ur,index_vr,index_wr
  integer :: index_ui,index_vi,index_wi
  integer :: index_VOF1,index_VOF2,index_VOF3,index_VOF4,index_VOF5,index_VOF6,index_VOF7,index_VOF8
  integer, dimension(:), allocatable :: index_sc
  integer :: indexy_u,indexy_v,indexy_w
  integer :: indexy_ur,indexy_vr,indexy_wr
  integer :: indexy_ui,indexy_vi,indexy_wi
  integer :: indexy_VOF1,indexy_VOF2,indexy_VOF3,indexy_VOF4,indexy_VOF5,indexy_VOF6,indexy_VOF7,indexy_VOF8
  integer, dimension(:), allocatable :: indexy_sc
  
  ! Grid from the inflow file
  integer :: inflow_icyl,inflowy_icyl
  integer :: inflow_ny,inflow_nz,inflowy_nx,inflowy_nz
  real(WP), dimension(:), pointer :: grid_y,grid_z,gridy_x,gridy_z
  real(WP), dimension(:), pointer :: grid_ym,grid_zm,gridy_xm,gridy_zm
  real(WP) :: inflow_Lz,inflowy_Lz
  
  ! Size of random phase step for mixing layer
  real(WP) :: walk_step,phase
  
  ! Number and size of timesteps
  integer :: inflow_ntime,inflowy_ntime
  real(WP) :: inflow_freq,inflowy_freq
  real(WP) :: inflow_time,inflowy_time
  
  ! Current timestep index and time interpolation coeff
  integer :: itime,itimey
  real(WP) :: interp_time,interp_timey
  
  ! Interpolation coefficients in space
  integer,  dimension(:,:), allocatable :: index_cc_y,index_cc_z
  integer,  dimension(:,:), allocatable :: index_fy_y,index_fy_z
  integer,  dimension(:,:), allocatable :: index_fz_y,index_fz_z
  real(WP), dimension(:,:), allocatable :: interp_cc_y,interp_cc_z
  real(WP), dimension(:,:), allocatable :: interp_fy_y,interp_fy_z
  real(WP), dimension(:,:), allocatable :: interp_fz_y,interp_fz_z
  integer,  dimension(:,:), allocatable :: indexy_cc_x,indexy_cc_z
  integer,  dimension(:,:), allocatable :: indexy_fx_x,indexy_fx_z
  integer,  dimension(:,:), allocatable :: indexy_fz_x,indexy_fz_z
  real(WP), dimension(:,:), allocatable :: interpy_cc_x,interpy_cc_z
  real(WP), dimension(:,:), allocatable :: interpy_fx_x,interpy_fx_z
  real(WP), dimension(:,:), allocatable :: interpy_fz_x,interpy_fz_z
  
contains
  
  ! Read the corresponding time from the file
  subroutine inflow_file_read
    use time_info
    implicit none
    
    integer :: itime_new,var,ierr,offset
    integer(kind=MPI_OFFSET_KIND) :: disp
    integer(kind=MPI_OFFSET_KIND) :: WP_MOK, inflow_nvar_MOK, str_short_MOK
    integer(kind=MPI_OFFSET_KIND) :: inflow_ny_MOK, inflow_nz_MOK, offset_MOK
    integer, dimension(MPI_STATUS_SIZE) :: status
    real(WP) :: tmp
    
    ! Get the corresponding time in the inflow modulo...
    tmp = mod(time/inflow_freq,real(inflow_ntime,WP))
    if (tmp.lt.0.0_WP) tmp = tmp + real(inflow_ntime,WP)
    
    ! Get the index of the timestep right after time
    itime_new = int(tmp) + 1
    
    ! Get the coeff of interpolation in time
    interp_time = real(itime_new,WP) - tmp
    
    ! Nothing to do if still same timestep index
    if (itime_new.eq.itime) return
    
    ! Read one or two time steps?
    if (itime_new.ne.mod(itime,inflow_ntime)+1) then
       ! Compute displacement
       offset = itime_new-2
       if (offset.lt.0) offset = inflow_ntime-1


        ! Resize integers in display so NGA can read bigger inflow files
        WP_MOK =          int(WP,           MPI_OFFSET_KIND)
        inflow_nvar_MOK = int(inflow_nvar,  MPI_OFFSET_KIND)
        str_short_MOK =   int(str_short,    MPI_OFFSET_KIND)
        inflow_ny_MOK =   int(inflow_ny,    MPI_OFFSET_KIND)
        inflow_nz_MOK =   int(inflow_nz,    MPI_OFFSET_KIND)
        offset_MOK =      int(offset,       MPI_OFFSET_KIND)

       disp = 4*4 + 2*WP_MOK + inflow_nvar_MOK*str_short_MOK + 4 + (inflow_ny_MOK+inflow_nz_MOK+2)*WP_MOK + offset_MOK*inflow_nvar_MOK*inflow_ny_MOK*inflow_nz_MOK*WP_MOK
       call MPI_FILE_SET_VIEW(ifile,disp,MPI_REAL_WP,MPI_REAL_WP,"native",mpi_info,ierr)
       
       ! Read the data
       do var=1,inflow_nvar
          call MPI_FILE_READ_ALL(ifile,inflow_next(:,:,var),inflow_ny*inflow_nz,MPI_REAL_WP,status,ierr)
       end do
    end if
    
    ! Compute next displacement
    if (itime_new.ne.itime+1) then
       offset = itime_new-1

        ! Resize integers in display so NGA can read bigger inflow files
        WP_MOK =          int(WP,           MPI_OFFSET_KIND)
        inflow_nvar_MOK = int(inflow_nvar,  MPI_OFFSET_KIND)
        str_short_MOK =   int(str_short,    MPI_OFFSET_KIND)
        inflow_ny_MOK =   int(inflow_ny,    MPI_OFFSET_KIND)
        inflow_nz_MOK =   int(inflow_nz,    MPI_OFFSET_KIND)
        offset_MOK =      int(offset,       MPI_OFFSET_KIND)

       disp = 4*4 + 2*WP_MOK + inflow_nvar_MOK*str_short_MOK + 4 + (inflow_ny_MOK+inflow_nz_MOK+2)*WP_MOK + offset_MOK*inflow_nvar_MOK*inflow_ny_MOK*inflow_nz_MOK*WP_MOK
       call MPI_FILE_SET_VIEW(ifile,disp,MPI_REAL_WP,MPI_REAL_WP,"native",mpi_info,ierr)
    end if
    
    ! Read the data
    itime = itime_new
    inflow_prev = inflow_next
    do var=1,inflow_nvar
       call MPI_FILE_READ_ALL(ifile,inflow_next(:,:,var),inflow_ny*inflow_nz,MPI_REAL_WP,status,ierr)
    end do
    
    return
  end subroutine inflow_file_read

  ! Read the corresponding time from the file
  subroutine inflowy_file_read
    use time_info
    implicit none
    
    integer :: itimey_new,var,ierr,offset
    integer(kind=MPI_OFFSET_KIND) :: disp
    integer(kind=MPI_OFFSET_KIND) :: WP_MOK, inflowy_nvar_MOK, str_short_MOK
    integer(kind=MPI_OFFSET_KIND) :: inflowy_nx_MOK, inflowy_nz_MOK, offset_MOK
    integer, dimension(MPI_STATUS_SIZE) :: status
    real(WP) :: tmp
    
    ! Get the corresponding time in the inflow modulo...
    tmp = mod(time/inflowy_freq,real(inflowy_ntime,WP))
    if (tmp.lt.0.0_WP) tmp = tmp + real(inflowy_ntime,WP)
    
    ! Get the index of the timestep right after time
    itimey_new = int(tmp) + 1
    
    ! Get the coeff of interpolation in time
    interp_timey = real(itimey_new,WP) - tmp
    
    ! Nothing to do if still same timestep index
    if (itimey_new.eq.itimey) return
    
    ! Read one or two time steps?
    if (itimey_new.ne.mod(itimey,inflowy_ntime)+1) then
       ! Compute displacement
       offset = itimey_new-2
       if (offset.lt.0) offset = inflowy_ntime-1

      ! Resize integers in display so NGA can read bigger inflow files
      WP_MOK =            int(WP,             MPI_OFFSET_KIND)
      inflowy_nvar_MOK =  int(inflowy_nvar,   MPI_OFFSET_KIND)
      str_short_MOK =     int(str_short,      MPI_OFFSET_KIND)
      inflowy_nx_MOK =    int(inflowy_nx,     MPI_OFFSET_KIND)
      inflowy_nz_MOK =    int(inflowy_nz,     MPI_OFFSET_KIND)
      offset_MOK =        int(offset,         MPI_OFFSET_KIND)

       disp = 4*4 + 2*WP_MOK + inflowy_nvar_MOK*str_short_MOK + 4 + (inflowy_nx_MOK+inflowy_nz_MOK+2)*WP_MOK + offset_MOK*inflowy_nvar_MOK*inflowy_nx_MOK*inflowy_nz_MOK*WP_MOK
       call MPI_FILE_SET_VIEW(ifiley,disp,MPI_REAL_WP,MPI_REAL_WP,"native",mpi_info,ierr)
       
       ! Read the data
       do var=1,inflowy_nvar
          call MPI_FILE_READ_ALL(ifiley,inflowy_next(:,:,var),inflowy_nx*inflowy_nz,MPI_REAL_WP,status,ierr)
       end do
    end if
    
    ! Compute next displacement
    if (itimey_new.ne.itimey+1) then
       offset = itimey_new-1

      ! Resize integers in display so NGA can read bigger inflow files
      WP_MOK =            int(WP,             MPI_OFFSET_KIND)
      inflowy_nvar_MOK =  int(inflowy_nvar,   MPI_OFFSET_KIND)
      str_short_MOK =     int(str_short,      MPI_OFFSET_KIND)
      inflowy_nx_MOK =    int(inflowy_nx,     MPI_OFFSET_KIND)
      inflowy_nz_MOK =    int(inflowy_nz,     MPI_OFFSET_KIND)
      offset_MOK =        int(offset,         MPI_OFFSET_KIND)

       disp = 4*4 + 2*WP_MOK + inflowy_nvar_MOK*str_short_MOK + 4 + (inflowy_nx_MOK+inflowy_nz_MOK+2)*WP_MOK + offset_MOK*inflowy_nvar_MOK*inflowy_nx_MOK*inflowy_nz_MOK*WP_MOK
       call MPI_FILE_SET_VIEW(ifiley,disp,MPI_REAL_WP,MPI_REAL_WP,"native",mpi_info,ierr)
    end if
    
    ! Read the data
    itimey = itimey_new
    inflowy_prev = inflowy_next
    do var=1,inflowy_nvar
       call MPI_FILE_READ_ALL(ifiley,inflowy_next(:,:,var),inflowy_nx*inflowy_nz,MPI_REAL_WP,status,ierr)
    end do
    
    return
  end subroutine inflowy_file_read
  
  
  ! Compute the interpolation coefficient for one given point
  ! ---------------------------------------------------------
  subroutine inflow_file_get_interp(yin,zin,yloc,zloc,j,k,wj,wk)
    use math
    implicit none
    
    real(WP),         intent(in)  :: yin,zin
    character(len=*), intent(in)  :: yloc,zloc
    real(WP),         intent(out) :: wj,wk
    integer,          intent(out) :: j,k
    
    real(WP), dimension(:), pointer :: mesh_y
    real(WP), dimension(:), pointer :: mesh_z
    real(WP) :: yp,zp,zi
    
    ! Prepare the mesh
    if (trim(yloc).eq.'ym') then
       mesh_y=>grid_ym
    else
       mesh_y=>grid_y
    end if
    if (trim(zloc).eq.'zm') then
       mesh_z=>grid_zm
    else
       mesh_z=>grid_z
    end if
    
    ! Get the points in the inflow config
    if (inflow_icyl.eq.1) then
       if (icyl.eq.1) then
          if (yin.lt.0.0_WP) then
             yp = -yin
             zp = zin+Pi
          else
             yp = yin
             zp = zin
          end if
       else
          yp = sqrt(yin**2+zin**2)
          if (zin/(yp+epsilon(1.0_WP)).ge.0.0_WP) then
             zp = acos(yin/(yp+epsilon(1.0_WP)))
          else
             zp = twoPi-acos(yin/(yp+epsilon(1.0_WP)))
          end if
       end if
    else
       if (icyl.eq.1) then
          yp = yin*cos(zin)
          zp = yin*sin(zin)
       else
          yp = yin
          zp = zin
       end if
    end if
    
    ! Take care of periodicity in inflow
    ! z always periodic
    !zp = mod(zp-grid_z(1),inflow_Lz)+grid_z(1)
    !if (zp.lt.grid_z(1)) zp = zp+inflow_Lz
    
    ! Find the point in y
    if (yp.lt.mesh_y(1)) then
       j = 1
       wj = 1.0_WP
    else if (yp.ge.mesh_y(inflow_ny)) then
       j = inflow_ny-1
       wj = 0.0_WP
    else
       loop1:do j=1,inflow_ny-1
          if (yp.lt.mesh_y(j+1)) exit loop1
       end do loop1
       wj = (mesh_y(j+1)-yp)/(mesh_y(j+1)-mesh_y(j))
    end if
    
    ! Find the point in z
    if (zp.lt.mesh_z(1)) then
       k  = inflow_nz
       zi = mesh_z(inflow_nz)-inflow_Lz
       wk = (mesh_z(1)-zp)/(mesh_z(1)-zi)
    else if (zp.ge.mesh_z(inflow_nz)) then
       k  = inflow_nz
       zi = mesh_z(1)+inflow_Lz
       wk = (zi-zp)/(zi-mesh_z(inflow_nz))
    else
       loop3:do k=1,inflow_nz-1
          if (zp.lt.mesh_z(k+1)) exit loop3
       end do loop3
       wk = (mesh_z(k+1)-zp)/(mesh_z(k+1)-mesh_z(k))
    end if
    
    return
  end subroutine inflow_file_get_interp
  

  ! Compute the interpolation coefficient for one given point
  ! ---------------------------------------------------------
  subroutine inflowy_file_get_interp(xin,zin,xloc,zloc,i,k,wi,wk)
    use math
    implicit none
    
    real(WP),         intent(in)  :: xin,zin
    character(len=*), intent(in)  :: xloc,zloc
    real(WP),         intent(out) :: wi,wk
    integer,          intent(out) :: i,k
    
    real(WP), dimension(:), pointer :: meshy_x
    real(WP), dimension(:), pointer :: meshy_z
    real(WP) :: xp,zp,zi
    
    ! Prepare the mesh
    if (trim(xloc).eq.'xm') then
       meshy_x=>gridy_xm
    else
       meshy_x=>gridy_x
    end if
    if (trim(zloc).eq.'zm') then
       meshy_z=>gridy_zm
    else
       meshy_z=>gridy_z
    end if
   
    ! Get the points in the inflow config
    ! Only Cartesian grid
    xp = xin
    zp = zin
     
    ! Take care of periodicity in inflow
    ! z always periodic
    zp = mod(zp-gridy_z(1),inflowy_Lz)+gridy_z(1)
    !if (zp.lt.grid_z(1)) zp = zp+inflow_Lz
    
    ! Find the point in y
    if (xp.lt.meshy_x(1)) then
       i = 1
       wi = 1.0_WP
    else if (xp.ge.meshy_x(inflowy_nx)) then
       i = inflowy_nx-1
       wi = 0.0_WP
    else
       loop1:do i=1,inflowy_nx-1
          if (xp.lt.meshy_x(i+1)) exit loop1
       end do loop1
       wi = (meshy_x(i+1)-xp)/(meshy_x(i+1)-meshy_x(i))
    end if
    
    ! Find the point in z
    if (zp.lt.meshy_z(1)) then
       k  = inflowy_nz
       zi = meshy_z(inflowy_nz)-inflowy_Lz
       wk = (meshy_z(1)-zp)/(meshy_z(1)-zi)
    else if (zp.ge.meshy_z(inflowy_nz)) then
       k  = inflowy_nz
       zi = meshy_z(1)+inflowy_Lz
       wk = (zi-zp)/(zi-meshy_z(inflowy_nz))
    else
       loop3:do k=1,inflowy_nz-1
          if (zp.lt.meshy_z(k+1)) exit loop3
       end do loop3
       wk = (meshy_z(k+1)-zp)/(meshy_z(k+1)-meshy_z(k))
    end if

    return
  end subroutine inflowy_file_get_interp
  
  ! Precompute the interpolation coefficients
  ! Include the ghost cells
  ! HERE axis is WRONG. Done later in "inflow_file_precompute"
  subroutine inflow_file_interp
    implicit none
    
    integer  :: j,k,nflow
    
    ! Different types of inflows
    do nflow=1,ninlet
       if (trim(inlet_type(nflow)).eq.'file') then
          
          ! Get the index and coeff
          do j=max(jmino_,inlet(nflow)%jmino),min(jmaxo_,inlet(nflow)%jmaxo)
             do k=kmino_,kmaxo_
                
                ! Cell centers
                call inflow_file_get_interp(ym(j),zm(k),'ym','zm', &
                     index_cc_y(j,k),index_cc_z(j,k),interp_cc_y(j,k),interp_cc_z(j,k))
                
                ! Face in y
                call inflow_file_get_interp(y(j) ,zm(k),'y' ,'zm', &
                     index_fy_y(j,k),index_fy_z(j,k),interp_fy_y(j,k),interp_fy_z(j,k))
                
                ! Face in z
                call inflow_file_get_interp(ym(j),z(k) ,'ym','z' , &
                     index_fz_y(j,k),index_fz_z(j,k),interp_fz_y(j,k),interp_fz_z(j,k))
                
             end do
          end do
       end if
    end do
    
    return
  end subroutine inflow_file_interp

  ! Precompute the interpolation coefficients
  ! Include the ghost cells
  ! HERE axis is WRONG. Done later in "inflow_file_precompute"
  subroutine inflowy_file_interp
    implicit none
    
    integer  :: i,k,nflow
    
    ! Different types of inflows
    do nflow=1,ninlety
       if (trim(inlety_type(nflow)).eq.'file') then
          

          ! Get the index and coeff
          do i=max(imino_,inlety(nflow)%imino),min(imaxo_,inlety(nflow)%imaxo)
             do k=kmino_,kmaxo_
                
                ! Cell centers
                call inflowy_file_get_interp(xm(i),zm(k),'xm','zm', &
                     indexy_cc_x(i,k),indexy_cc_z(i,k),interpy_cc_x(i,k),interpy_cc_z(i,k))
                
                ! Face in x
                call inflowy_file_get_interp(x(i) ,zm(k),'x' ,'zm', &
                     indexy_fx_x(i,k),indexy_fx_z(i,k),interpy_fx_x(i,k),interpy_fx_z(i,k))
                
                ! Face in z
                call inflowy_file_get_interp(xm(i),z(k) ,'xm','z' , &
                     indexy_fz_x(i,k),indexy_fz_z(i,k),interpy_fz_x(i,k),interpy_fz_z(i,k))
                
             end do
          end do
       end if
    end do
    
    return
  end subroutine inflowy_file_interp


end module inflow_file


! ===================== !
! Initialize the module !
! ===================== !
subroutine inflow_file_init
  use inflow_file
  use data
  use parser
  use math
  implicit none
  
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer, dimension(4) :: dims
  integer :: ierr,var,j,k,isc,nflow
  
  ! Split the communicator
  if (iproc.ne.1) then
     file_inflow = 0
     call MPI_COMM_SPLIT(comm,file_inflow,MPI_UNDEFINED,inflow_comm,ierr)
     return
  end if
  
  ! Does the processor have an type 'file' inlet?
  ! Split the communicator
  file_inflow = 0
  do nflow=1,ninlet
     if (trim(inlet_type(nflow)).eq.'file') then
        if (min(jmax_,inlet(nflow)%jmax)-max(jmin_,inlet(nflow)%jmin).gt.0) file_inflow = 1
     end if
  end do
  call MPI_COMM_SPLIT(comm,file_inflow,MPI_UNDEFINED,inflow_comm,ierr)
  
  ! Nothing to be done if no 'file' infow type
  if (file_inflow.ne.1) return

  ! Open the file to read
  call parser_read('Inflow file to read',filename)
  filename = trim(mpiiofs) // trim(filename)
  call MPI_FILE_OPEN(inflow_comm,filename,MPI_MODE_RDONLY,mpi_info,ifile,ierr)
  
  ! Read dimensions
  call MPI_FILE_READ_ALL(ifile,dims,4,MPI_INTEGER,status,ierr)
  call MPI_FILE_READ_ALL(ifile,inflow_freq,1,MPI_REAL_WP,status,ierr)
  call MPI_FILE_READ_ALL(ifile,inflow_time,1,MPI_REAL_WP,status,ierr)
  inflow_ntime = dims(1)
  inflow_ny    = dims(2)
  inflow_nz    = dims(3)
  inflow_nvar  = dims(4)
  
  ! Read variable names
  allocate(inflow_name(inflow_nvar))
  allocate(index_sc(inflow_nvar))
  index_u = 0
  index_v = 0
  index_w = 0
  index_VOF1 = 0
  index_VOF2 = 0
  index_VOF3 = 0
  index_VOF4 = 0
  index_VOF5 = 0
  index_VOF6 = 0
  index_VOF7 = 0
  index_VOF8 = 0
  index_sc = 0
  do var=1,inflow_nvar
     call MPI_FILE_READ_ALL(ifile,inflow_name(var),str_short,MPI_CHARACTER,status,ierr)
     ! Detect the name of the variable
     select case(trim(inflow_name(var)))
     case('U')
        index_u = var
     case('V')
        index_v = var
     case('W')
        index_w = var
     case('Ur')
        index_ur = var
     case('Vr')
        index_vr = var
     case('Wr')
        index_wr = var
     case('Ui')
        index_ui = var
     case('Vi')
         index_vi = var
     case('Wi')
        index_wi = var
     case('VOF1')
        index_VOF1 = var
     case('VOF2')
        index_VOF2 = var
     case('VOF3')
        index_VOF3 = var
     case('VOF4')
        index_VOF4 = var
     case('VOF5')
        index_VOF5 = var
     case('VOF6')
        index_VOF6 = var
     case('VOF7')
        index_VOF7 = var
     case('VOF8')
        index_VOF8 = var
     case default
        loop:do isc=1,nscalar
           if (trim(sc_name(isc)).eq.trim(inflow_name(var))) exit loop
        end do loop
        if (isc.eq.nscalar+1) then
           print*,inflow_name(var)
           call die('inflow_file_init: Wrong scalar name in inflow file')
        else
           index_sc(var) = isc
        end if
     end select
  end do
  
  ! Check the variables and set the initial phase
  phase = 0.0_WP
  call parser_read('Random phase step',walk_step,0.0_WP)
  walk_step = walk_step * twoPi/180.0_WP
  if (walk_step.eq.0.0_WP) then
     if (index_u.eq.0) call die('inflow_file_init: U not present in inflow file')
     if (index_v.eq.0 .and. index_vr.eq.0) call die('inflow_file_init: V or Vr not present in inflow file')
     if (index_w.eq.0 .and. index_wr.eq.0) call die('inflow_file_init: W or Wr not present in inflow file')
  else
     if (index_u.eq.0 .or. index_ur.eq.0 .or. index_ui.eq.0) &
          call die('inflow_file_init: U or Ur or Ui not present in inflow file')
     if (index_vr.eq.0 .or. index_vi.eq.0) call die('inflow_file_init: Vr or Vi not present in inflow file')
     if (index_wr.eq.0 .or. index_wi.eq.0) call die('inflow_file_init: Wr or Wi not present in inflow file')     
  end if
  
  ! Read the grid
  allocate(grid_y (inflow_ny+1))
  allocate(grid_ym(inflow_ny))
  allocate(grid_z (inflow_nz+1))
  allocate(grid_zm(inflow_nz))
  call MPI_FILE_READ_ALL(ifile,inflow_icyl,1,MPI_INTEGER,status,ierr)
  call MPI_FILE_READ_ALL(ifile,grid_y,inflow_ny+1,MPI_REAL_WP,status,ierr)
  call MPI_FILE_READ_ALL(ifile,grid_z,inflow_nz+1,MPI_REAL_WP,status,ierr)
  
  ! Take care of periodicity
  ! Shift to have first point at 0
  ! GB removed - grid_z = grid_z - grid_z(1)
  inflow_Lz = grid_z(inflow_nz+1)-grid_z(1)
  
  ! Create mid points
  do j=1,inflow_ny
     grid_ym(j) = 0.5*(grid_y(j)+grid_y(j+1))
  end do
  do k=1,inflow_nz
     grid_zm(k) = 0.5*(grid_z(k)+grid_z(k+1))
  end do
  
  ! Allocate arrays
  allocate(inflow_prev(inflow_ny,inflow_nz,inflow_nvar))
  allocate(inflow_next(inflow_ny,inflow_nz,inflow_nvar))
  
  allocate(index_cc_y(jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(index_cc_z(jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(index_fy_y(jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(index_fy_z(jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(index_fz_y(jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(index_fz_z(jmino_:jmaxo_,kmino_:kmaxo_))
  
  allocate(interp_cc_y(jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(interp_cc_z(jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(interp_fy_y(jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(interp_fy_z(jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(interp_fz_y(jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(interp_fz_z(jmino_:jmaxo_,kmino_:kmaxo_))
  
  ! Allocate the axis variables
  allocate(Uaxis(kmin:kmax),Vaxis(kmin:kmax),Waxis(kmin:kmax))
  allocate(VOFaxis(kmin:kmax))
  allocate(SCaxis(kmin:kmax,nscalar))
  
  ! Compute the interpolation coefficients
  call inflow_file_interp
  
  ! Read the first two time steps
  itime = -1
  call inflow_file_read
  
  return
end subroutine inflow_file_init


! y inflow ===========================
subroutine inflowy_file_init
  use inflow_file
  use data
  use parser
  use math
  use fileio
  implicit none
  
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer, dimension(4) :: dims
  integer :: ierr,var,i,k,isc,nflow

  ! Split the communicator
  if (jproc.ne.1) then
     file_inflowy = 0
     call MPI_COMM_SPLIT(comm,file_inflowy,MPI_UNDEFINED,inflowy_comm,ierr)
     return
  end if
  
  ! Does the processor have an type 'file' inlet?
  ! Split the communicator
  file_inflowy = 0
  do nflow=1,ninlety
     if (trim(inlety_type(nflow)).eq.'file') then
        if (min(imax_,inlety(nflow)%imax)-max(imin_,inlety(nflow)%imin).gt.0) file_inflowy = 1
     end if
  end do
  call MPI_COMM_SPLIT(comm,file_inflowy,MPI_UNDEFINED,inflowy_comm,ierr)

  ! Continue if processor contains 'file' inflow
  if (file_inflowy.eq.0) return
  
  ! Open the file to read
  call parser_read('Inflow y file to read',filenamey)
  filenamey = trim(mpiiofs) // trim(filenamey)
  call MPI_FILE_OPEN(inflowy_comm,filenamey,MPI_MODE_RDONLY,mpi_info,ifiley,ierr)

  ! Read dimensions
  call MPI_FILE_READ_ALL(ifiley,dims,4,MPI_INTEGER,status,ierr)
  call MPI_FILE_READ_ALL(ifiley,inflowy_freq,1,MPI_REAL_WP,status,ierr)
  call MPI_FILE_READ_ALL(ifiley,inflowy_time,1,MPI_REAL_WP,status,ierr)
  inflowy_ntime = dims(1)
  inflowy_nx    = dims(2)
  inflowy_nz    = dims(3)
  inflowy_nvar  = dims(4)

  ! Read variable names
  allocate(inflowy_name(inflowy_nvar))
  allocate(indexy_sc(inflowy_nvar))
  indexy_u   = 0
  indexy_v   = 0
  indexy_w   = 0
  indexy_VOF1= 0
  indexy_VOF2= 0
  indexy_VOF3= 0
  indexy_VOF4= 0
  indexy_VOF5= 0
  indexy_VOF6= 0
  indexy_VOF7= 0
  indexy_VOF8= 0
  indexy_sc  = 0
  do var=1,inflowy_nvar
     call MPI_FILE_READ_ALL(ifiley,inflowy_name(var),str_short,MPI_CHARACTER,status,ierr)
     ! Detect the name of the variable
     select case(trim(inflowy_name(var)))
     case('U')
        indexy_u = var
     case('V')
        indexy_v = var
     case('W')
        indexy_w = var
     case('Ur')
        indexy_ur = var
     case('Vr')
        indexy_vr = var
     case('Wr')
        indexy_wr = var
     case('Ui')
        indexy_ui = var
     case('Vi')
        indexy_vi = var
     case('Wi')
        indexy_wi = var
     case('VOF1')
        indexy_VOF1= var
     case('VOF2')
        indexy_VOF2= var
     case('VOF3')
        indexy_VOF3= var
     case('VOF4')
        indexy_VOF4= var
     case('VOF5')
        indexy_VOF5= var
     case('VOF6')
        indexy_VOF6= var
     case('VOF7')
        indexy_VOF7= var
     case('VOF8')
        indexy_VOF8= var
     case default
        loopy:do isc=1,nscalar
           if (trim(sc_name(isc)).eq.trim(inflowy_name(var))) exit loopy
        end do loopy
        if (isc.eq.nscalar+1) then
           call die('inflowy_file_init: Wrong scalar name in inflow y file')
        else
           indexy_sc(var) = isc
        end if
     end select
  end do

  ! Check the variables and set the initial phase
  phase = 0.0_WP
  call parser_read('Random phase step',walk_step,0.0_WP)
  walk_step = walk_step * twoPi/180.0_WP
  if (walk_step.eq.0.0_WP) then
     if (indexy_u.eq.0 .and. indexy_ur.eq.0) call die('inflowy_file_init: U or Ur not present in inflow y file')
     if (indexy_v.eq.0                     ) call die('inflowy_file_init: V not present in inflow y file')
     if (indexy_w.eq.0 .and. indexy_wr.eq.0) call die('inflowy_file_init: W or Wr not present in inflow y file')
  else
     if (index_ur.eq.0 .or. index_ui.eq.0) call die('inflowy_file_init: Ur or Ui not present in inflow y file')
     if (index_u.eq.0 .or. index_vr.eq.0 .or. index_vi.eq.0) &
          call die('inflowy_file_init: V or Vr or Vi not present in inflow y file')
     if (index_wr.eq.0 .or. index_wi.eq.0) call die('inflowy_file_init: Wr or Wi not present in inflow y file')
  end if

  ! Read the grid
  allocate(gridy_x (inflowy_nx+1))
  allocate(gridy_xm(inflowy_nx))
  allocate(gridy_z (inflowy_nz+1))
  allocate(gridy_zm(inflowy_nz))
  call MPI_FILE_READ_ALL(ifiley,inflowy_icyl,1,MPI_INTEGER,status,ierr)
  if (inflowy_icyl.eq.1) call die('inflow_file_init: Inflow y cannot be cylindrical')
  call MPI_FILE_READ_ALL(ifiley,gridy_x,inflowy_nx+1,MPI_REAL_WP,status,ierr)
  call MPI_FILE_READ_ALL(ifiley,gridy_z,inflowy_nz+1,MPI_REAL_WP,status,ierr)

  ! Take care of periodicity
  ! Shift to have first point at 0
  ! GB removed - grid_z = grid_z - grid_z(1)
  inflowy_Lz = gridy_z(inflowy_nz+1)-gridy_z(1)

  ! Create mid points
  do i=1,inflowy_nx
     gridy_xm(i) = 0.5*(gridy_x(i)+gridy_x(i+1))
  end do
  do k=1,inflowy_nz
     gridy_zm(k) = 0.5*(gridy_z(k)+gridy_z(k+1))
  end do

  ! Allocate arrays
  allocate(inflowy_prev(inflowy_nx,inflowy_nz,inflowy_nvar))
  allocate(inflowy_next(inflowy_nx,inflowy_nz,inflowy_nvar))

  allocate(indexy_cc_x(imino_:imaxo_,kmino_:kmaxo_))
  allocate(indexy_cc_z(imino_:imaxo_,kmino_:kmaxo_))
  allocate(indexy_fx_x(imino_:imaxo_,kmino_:kmaxo_))
  allocate(indexy_fx_z(imino_:imaxo_,kmino_:kmaxo_))
  allocate(indexy_fz_x(imino_:imaxo_,kmino_:kmaxo_))
  allocate(indexy_fz_z(imino_:imaxo_,kmino_:kmaxo_))

  allocate(interpy_cc_x(imino_:imaxo_,kmino_:kmaxo_))
  allocate(interpy_cc_z(imino_:imaxo_,kmino_:kmaxo_))
  allocate(interpy_fx_x(imino_:imaxo_,kmino_:kmaxo_))
  allocate(interpy_fx_z(imino_:imaxo_,kmino_:kmaxo_))
  allocate(interpy_fz_x(imino_:imaxo_,kmino_:kmaxo_))
  allocate(interpy_fz_z(imino_:imaxo_,kmino_:kmaxo_))

  ! Compute the interpolation coefficients
  call inflowy_file_interp

  ! Read the first two time steps
  itimey = -1
  call inflowy_file_read

  return
end subroutine inflowy_file_init

! =========================================================== !
! Precompute the U,V,W fields at the inlet for the given time !
! =========================================================== !
subroutine inflow_file_velocity
  use inflow_file
  use random
  implicit none
  
  real(WP) :: prev,next
  real(WP) :: wj,wk,rnd
  integer  :: j,k,jj,k1,k2
  real(WP) :: umean,wmean,utmp,wtmp
  integer  :: nflow,var,k0,kPi
  real(WP) :: theta
  
  ! Nothing to be done if no 'file' infow type
  ! Read if necessary the inflow file
  if (file_inflow.eq.1) then
     call inflow_file_read
  end if
  
  ! Random number for a random walk
  call random_number(rnd)
  if (rnd.ge.0.5_WP) then
     phase = phase + walk_step
  else
     phase = phase - walk_step
  end if
  
  ! Different types of inflows
  do nflow=1,ninlet
     if (trim(inlet_type(nflow)).eq.'file') then
        do j=max(jmino_,inlet(nflow)%jmino),min(jmaxo_,inlet(nflow)%jmaxo)
           do k=kmino_,kmaxo_
              
              ! First set to zero
              Uin(j,k) = 0.0_WP
              Vin(j,k) = 0.0_WP
              Win(j,k) = 0.0_WP
              
              do var=1,inflow_nvar
                 
                 ! Set angle to default
                 theta = 0.0_WP
                 
                 select case(trim(inflow_name(var)))
                 case('U')
                    ! Interpolation in space and time for: U
                    jj = index_cc_y(j,k)
                    k1 = index_cc_z(j,k); k2=mod(k1,inflow_nz)+1
                    wj = interp_cc_y(j,k)
                    wk = interp_cc_z(j,k)
                    prev =       wk *(wj*inflow_prev(jj,k1,var) + (1.0_WP-wj)*inflow_prev(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_prev(jj,k2,var) + (1.0_WP-wj)*inflow_prev(jj+1,k2,var))
                    next =       wk *(wj*inflow_next(jj,k1,var) + (1.0_WP-wj)*inflow_next(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_next(jj,k2,var) + (1.0_WP-wj)*inflow_next(jj+1,k2,var))
                    Uin(j,k) = Uin(j,k) + interp_time*prev + (1.0_WP-interp_time)*next
                    
                 case('Ur')
                    ! Interpolation in space and time for: Ur (real part of fluctuation)
                    jj = index_cc_y(j,k)
                    k1 = index_cc_z(j,k); k2=mod(k1,inflow_nz)+1
                    wj = interp_cc_y(j,k)
                    wk = interp_cc_z(j,k)
                    prev =       wk *(wj*inflow_prev(jj,k1,var) + (1.0_WP-wj)*inflow_prev(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_prev(jj,k2,var) + (1.0_WP-wj)*inflow_prev(jj+1,k2,var))
                    next =       wk *(wj*inflow_next(jj,k1,var) + (1.0_WP-wj)*inflow_next(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_next(jj,k2,var) + (1.0_WP-wj)*inflow_next(jj+1,k2,var))
                    Uin(j,k) = Uin(j,k) + cos(phase) * (interp_time*prev + (1.0_WP-interp_time)*next)
                    
                 case('Ui')
                    ! Interpolation in space and time for: Ui (imag part of fluctuation)
                    jj = index_cc_y(j,k)
                    k1 = index_cc_z(j,k); k2=mod(k1,inflow_nz)+1
                    wj = interp_cc_y(j,k)
                    wk = interp_cc_z(j,k)
                    prev =       wk *(wj*inflow_prev(jj,k1,var) + (1.0_WP-wj)*inflow_prev(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_prev(jj,k2,var) + (1.0_WP-wj)*inflow_prev(jj+1,k2,var))
                    next =       wk *(wj*inflow_next(jj,k1,var) + (1.0_WP-wj)*inflow_next(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_next(jj,k2,var) + (1.0_WP-wj)*inflow_next(jj+1,k2,var))
                    Uin(j,k) = Uin(j,k) - sin(phase) * (interp_time*prev + (1.0_WP-interp_time)*next)
                    
                 case('V')
                    ! Interpolation in space and time for: V
                    jj = index_fy_y(j,k)
                    k1 = index_fy_z(j,k); k2=mod(k1,inflow_nz)+1
                    wj = interp_fy_y(j,k)
                    wk = interp_fy_z(j,k)
                    prev =       wk *(wj*inflow_prev(jj,k1,var) + (1.0_WP-wj)*inflow_prev(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_prev(jj,k2,var) + (1.0_WP-wj)*inflow_prev(jj+1,k2,var))
                    next =       wk *(wj*inflow_next(jj,k1,var) + (1.0_WP-wj)*inflow_next(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_next(jj,k2,var) + (1.0_WP-wj)*inflow_next(jj+1,k2,var))
                    if ((inflow_icyl.eq.1) .and. (icyl.eq.0)) theta = grid_zm(k1)+(1.0_WP-wk)*inflow_Lz/real(inflow_nz,WP)
                    Vin(j,k) = Vin(j,k) + (interp_time*prev + (1.0_WP-interp_time)*next)*cos(theta)
                    Win(j,k) = Win(j,k) + (interp_time*prev + (1.0_WP-interp_time)*next)*sin(theta)
                    
                 case('Vr')
                    ! Interpolation in space and time for: Vr (real part of fluctuation)
                    jj = index_fy_y(j,k)
                    k1 = index_fy_z(j,k); k2=mod(k1,inflow_nz)+1
                    wj = interp_fy_y(j,k)
                    wk = interp_fy_z(j,k)
                    prev =       wk *(wj*inflow_prev(jj,k1,var) + (1.0_WP-wj)*inflow_prev(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_prev(jj,k2,var) + (1.0_WP-wj)*inflow_prev(jj+1,k2,var))
                    next =       wk *(wj*inflow_next(jj,k1,var) + (1.0_WP-wj)*inflow_next(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_next(jj,k2,var) + (1.0_WP-wj)*inflow_next(jj+1,k2,var))
                    Vin(j,k) = Vin(j,k) + cos(phase) * (interp_time*prev + (1.0_WP-interp_time)*next)
                    
                 case('Vi')
                    ! Interpolation in space and time for: Vi (imag part of fluctuation)
                    jj = index_fy_y(j,k)
                    k1 = index_fy_z(j,k); k2=mod(k1,inflow_nz)+1
                    wj = interp_fy_y(j,k)
                    wk = interp_fy_z(j,k)
                    prev =       wk *(wj*inflow_prev(jj,k1,var) + (1.0_WP-wj)*inflow_prev(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_prev(jj,k2,var) + (1.0_WP-wj)*inflow_prev(jj+1,k2,var))
                    next =       wk *(wj*inflow_next(jj,k1,var) + (1.0_WP-wj)*inflow_next(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_next(jj,k2,var) + (1.0_WP-wj)*inflow_next(jj+1,k2,var))
                    Vin(j,k) = Vin(j,k) - sin(phase) * ( interp_time*prev + (1.0_WP-interp_time)*next)
                    
                 case('W')
                    ! Interpolation in space and time for: W
                    jj = index_fz_y(j,k)
                    k1 = index_fz_z(j,k); k2=mod(k1,inflow_nz)+1
                    wj = interp_fz_y(j,k)
                    wk = interp_fz_z(j,k)
                    prev =       wk *(wj*inflow_prev(jj,k1,var) + (1.0_WP-wj)*inflow_prev(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_prev(jj,k2,var) + (1.0_WP-wj)*inflow_prev(jj+1,k2,var))
                    next =       wk *(wj*inflow_next(jj,k1,var) + (1.0_WP-wj)*inflow_next(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_next(jj,k2,var) + (1.0_WP-wj)*inflow_next(jj+1,k2,var))
                    if ((inflow_icyl.eq.1) .and. (icyl.eq.0)) theta = grid_z(k1)+(1.0_WP-wk)*inflow_Lz/real(inflow_nz,WP)
                    Vin(j,k) = Vin(j,k) - (interp_time*prev + (1.0_WP-interp_time)*next)*sin(theta)
                    Win(j,k) = Win(j,k) + (interp_time*prev + (1.0_WP-interp_time)*next)*cos(theta)
                    
                 case('Wr')
                    ! Interpolation in space and time for: Wr (real part of fluctuation)
                    jj = index_fz_y(j,k)
                    k1 = index_fz_z(j,k); k2=mod(k1,inflow_nz)+1
                    wj = interp_fz_y(j,k)
                    wk = interp_fz_z(j,k)
                    prev =       wk *(wj*inflow_prev(jj,k1,var) + (1.0_WP-wj)*inflow_prev(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_prev(jj,k2,var) + (1.0_WP-wj)*inflow_prev(jj+1,k2,var))
                    next =       wk *(wj*inflow_next(jj,k1,var) + (1.0_WP-wj)*inflow_next(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_next(jj,k2,var) + (1.0_WP-wj)*inflow_next(jj+1,k2,var))
                    Win(j,k) = Win(j,k) + cos(phase) * (interp_time*prev + (1.0_WP-interp_time)*next)
                    
                 case('Wi')
                    ! Interpolation in space and time for: Wi (imag part of fluctuation)
                    jj = index_fz_y(j,k)
                    k1 = index_fz_z(j,k); k2=mod(k1,inflow_nz)+1
                    wj = interp_fz_y(j,k)
                    wk = interp_fz_z(j,k)
                    prev =       wk *(wj*inflow_prev(jj,k1,var) + (1.0_WP-wj)*inflow_prev(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_prev(jj,k2,var) + (1.0_WP-wj)*inflow_prev(jj+1,k2,var))
                    next =       wk *(wj*inflow_next(jj,k1,var) + (1.0_WP-wj)*inflow_next(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_next(jj,k2,var) + (1.0_WP-wj)*inflow_next(jj+1,k2,var))
                    Win(j,k) = Win(j,k) - sin(phase) * ( interp_time*prev + (1.0_WP-interp_time)*next)
                    
                 case default
                    ! Interpolation in space and time for: scalars or level set
                    ! Done in inflow_file_scalar
                 end select
              end do
           end do
        end do
        
        ! Cylindrical case
        if (icyl.eq.1 .and. max(inlet(nflow)%jmino,jmino_).eq.jmino) then
           
           ! Get values around the axis
           call parallel_gather_dir(Uin(jmin,  kmin_:kmax_),Uaxis(kmin:kmax),'z')
           call parallel_gather_dir(Vin(jmin+1,kmin_:kmax_),Vaxis(kmin:kmax),'z')
           call parallel_gather_dir(Win(jmin,  kmin_:kmax_),Waxis(kmin:kmax),'z')
           
           do k=kmino_,kmaxo_
              k0 = kmin+modulo(k-kmin,nz)
              kPi = k0+nz/2
              if (kPi.gt.kmax) kPi = kPi-nz  
              
              Vin(jmin,k) = 0.5_WP*(Vaxis(k0)+Vaxis(kPi))
              do j=1,nover
                 Uin (jmin-j,k)   = + Uaxis(kPi)
                 Vin (jmin-j,k)   = - Vaxis(kPi)
                 Win (jmin-j,k)   = - Waxis(kPi)
              end do
           end do
        end if
     
     end if
        
     if (rescale_inflow) then
        ! Compute the mean velocities
        utmp = 0.0_WP
        wtmp = 0.0_WP
        do j = max(inlet(nflow)%jmin,jmin_),min(inlet(nflow)%jmax,jmax_)
           do k=kmin_,kmax_
              utmp = utmp + Uin(j,k)*dA(imin,j,k)
              wtmp = wtmp + Win(j,k)*dA(imin,j,k)
           end do
        end do
        call parallel_sum_dir(utmp,umean,'yz')
        call parallel_sum_dir(wtmp,wmean,'yz')
        umean = umean/inlet(nflow)%A
        wmean = wmean/inlet(nflow)%A
           
        ! Rescale the velocities by keeping constant the turbulent intensity
        do j = max(inlet(nflow)%jmino,jmino_),min(inlet(nflow)%jmaxo,jmaxo_)
           do k=kmino_,kmaxo_
              Uin(j,k) = Uin(j,k) * ubulk(nflow)/(umean+epsilon(umean))
              Win(j,k) = Win(j,k) * wbulk(nflow)/(wmean+epsilon(wmean))
           end do
        end do
     end if
        
  end do
  
  return
end subroutine inflow_file_velocity


! ============================================================= !
! Precompute the U,V,W fields at the inlet y for the given time !
! ============================================================= !
subroutine inflowy_file_velocity
  use inflow_file
  use random
  use time_info
  implicit none
  
  real(WP) :: prev,next
  real(WP) :: wi,wk,rnd
  integer  :: i,k,ii,k1,k2
  real(WP) :: vmean,wmean,vtmp,wtmp
  integer  :: nflow,var
  real(WP) :: theta
  
  ! Nothing to be done if no 'file' infow type
  ! Read if necessary the inflow file
  if (file_inflowy.eq.1) then
     call inflowy_file_read
  end if
  
  ! Random number for a random walk
  call random_number(rnd)
  if (rnd.ge.0.5_WP) then
     phase = phase + walk_step
  else
     phase = phase - walk_step
  end if

  ! Different types of inflows
  do nflow=1,ninlety
     if (trim(inlety_type(nflow)).eq.'file') then
        do i=max(imino_,inlety(nflow)%imino),min(imaxo_,inlety(nflow)%imaxo)
           do k=kmino_,kmaxo_
              
              ! First set to zero
              Uiny(i,k) = 0.0_WP
              Viny(i,k) = 0.0_WP
              Winy(i,k) = 0.0_WP
              
              do var=1,inflowy_nvar
                 
                 ! Set angle to default
                 theta = 0.0_WP
                 
                 select case(trim(inflowy_name(var)))
                 case('U')
                    ! Interpolation in space and time for: U
                    ii = indexy_fx_x(i,k)
                    k1 = indexy_fx_z(i,k); k2=mod(k1,inflowy_nz)+1
                    wi = interpy_fx_x(i,k)
                    wk = interpy_fx_z(i,k)
                    prev =       wk *(wi*inflowy_prev(ii,k1,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_prev(ii,k2,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k2,var))
                    next =       wk *(wi*inflowy_next(ii,k1,var) + (1.0_WP-wi)*inflowy_next(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_next(ii,k2,var) + (1.0_WP-wi)*inflowy_next(ii+1,k2,var))
                    Uiny(i,k) = Uiny(i,k) + interp_timey*prev + (1.0_WP-interp_timey)*next
                    
                 case('Ur')
                    ! Interpolation in space and time for: Ur (real part of fluctuation)
                    ii = indexy_fx_x(i,k)
                    k1 = indexy_fx_z(i,k); k2=mod(k1,inflowy_nz)+1
                    wi = interpy_fx_x(i,k)
                    wk = interpy_fx_z(i,k)
                    prev =       wk *(wi*inflowy_prev(ii,k1,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_prev(ii,k2,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k2,var))
                    next =       wk *(wi*inflowy_next(ii,k1,var) + (1.0_WP-wi)*inflowy_next(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_next(ii,k2,var) + (1.0_WP-wi)*inflowy_next(ii+1,k2,var))
                    Uiny(i,k) = Uiny(i,k) + cos(phase) * (interp_timey*prev + (1.0_WP-interp_timey)*next)
                    
                 case('Ui')
                    ! Interpolation in space and time for: Ui (imag part of fluctuation)
                    ii = indexy_fx_x(i,k)
                    k1 = indexy_fx_z(i,k); k2=mod(k1,inflowy_nz)+1
                    wi = interpy_fx_x(i,k)
                    wk = interpy_fx_z(i,k)
                    prev =       wk *(wi*inflowy_prev(ii,k1,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_prev(ii,k2,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k2,var))
                    next =       wk *(wi*inflowy_next(ii,k1,var) + (1.0_WP-wi)*inflowy_next(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_next(ii,k2,var) + (1.0_WP-wi)*inflowy_next(ii+1,k2,var))
                    Uiny(i,k) = Uiny(i,k) + sin(phase) * (interp_timey*prev + (1.0_WP-interp_timey)*next)
                    
                 case('V')
                    ! Interpolation in space and time for: V
                    ii = indexy_cc_x(i,k)
                    k1 = indexy_cc_z(i,k); k2=mod(k1,inflowy_nz)+1
                    wi = interpy_cc_x(i,k)
                    wk = interpy_cc_z(i,k)
                    prev =       wk *(wi*inflowy_prev(ii,k1,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_prev(ii,k2,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k2,var))
                    next =       wk *(wi*inflowy_next(ii,k1,var) + (1.0_WP-wi)*inflowy_next(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_next(ii,k2,var) + (1.0_WP-wi)*inflowy_next(ii+1,k2,var))
                    Viny(i,k) = Viny(i,k) + (interp_timey*prev + (1.0_WP-interp_timey)*next)
                    
                 case('Vr')
                    ! Interpolation in space and time for: Vr (real part of fluctuation)
                    ii = indexy_cc_x(i,k)
                    k1 = indexy_cc_z(i,k); k2=mod(k1,inflowy_nz)+1
                    wi = interpy_cc_x(i,k)
                    wk = interpy_cc_z(i,k)
                    prev =       wk *(wi*inflowy_prev(ii,k1,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_prev(ii,k2,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k2,var))
                    next =       wk *(wi*inflowy_next(ii,k1,var) + (1.0_WP-wi)*inflowy_next(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_next(ii,k2,var) + (1.0_WP-wi)*inflowy_next(ii+1,k2,var))
                    Viny(i,k) = Viny(i,k) + cos(phase) * (interp_timey*prev + (1.0_WP-interp_timey)*next)
                    
                 case('Vi')
                    ! Interpolation in space and time for: Vr (real part of fluctuation)
                    ii = indexy_cc_x(i,k)
                    k1 = indexy_cc_z(i,k); k2=mod(k1,inflowy_nz)+1
                    wi = interpy_cc_x(i,k)
                    wk = interpy_cc_z(i,k)
                    prev =       wk *(wi*inflowy_prev(ii,k1,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_prev(ii,k2,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k2,var))
                    next =       wk *(wi*inflowy_next(ii,k1,var) + (1.0_WP-wi)*inflowy_next(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_next(ii,k2,var) + (1.0_WP-wi)*inflowy_next(ii+1,k2,var))
                    Viny(i,k) = Viny(i,k) + sin(phase) * (interp_timey*prev + (1.0_WP-interp_timey)*next)
                    
                 case('W')
                    ! Interpolation in space and time for: W
                    ii = indexy_fz_x(i,k)
                    k1 = indexy_fz_z(i,k); k2=mod(k1,inflowy_nz)+1
                    wi = interpy_fz_x(i,k)
                    wk = interpy_fz_z(i,k)
                    prev =       wk *(wi*inflowy_prev(ii,k1,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_prev(ii,k2,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k2,var))
                    next =       wk *(wi*inflowy_next(ii,k1,var) + (1.0_WP-wi)*inflowy_next(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_next(ii,k2,var) + (1.0_WP-wi)*inflowy_next(ii+1,k2,var))
                    Winy(i,k) = Winy(i,k) + (interp_timey*prev + (1.0_WP-interp_timey)*next)*cos(theta)
                    
                 case('Wr')
                    ! Interpolation in space and time for: Wr (real part of fluctuation)
                    ii = indexy_fz_x(i,k)
                    k1 = indexy_fz_z(i,k); k2=mod(k1,inflowy_nz)+1
                    wi = interpy_fz_x(i,k)
                    wk = interpy_fz_z(i,k)
                    prev =       wk *(wi*inflowy_prev(ii,k1,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_prev(ii,k2,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k2,var))
                    next =       wk *(wi*inflowy_next(ii,k1,var) + (1.0_WP-wi)*inflowy_next(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_next(ii,k2,var) + (1.0_WP-wi)*inflowy_next(ii+1,k2,var))
                    Winy(i,k) = Winy(i,k) + cos(phase) * (interp_timey*prev + (1.0_WP-interp_timey)*next)
                    
                 case('Wi')
                    ! Interpolation in space and time for: Wi (imag part of fluctuation)
                    ii = indexy_fz_x(i,k)
                    k1 = indexy_fz_z(i,k); k2=mod(k1,inflowy_nz)+1
                    wi = interpy_fz_x(i,k)
                    wk = interpy_fz_z(i,k)
                    prev =       wk *(wi*inflowy_prev(ii,k1,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_prev(ii,k2,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k2,var))
                    next =       wk *(wi*inflowy_next(ii,k1,var) + (1.0_WP-wi)*inflowy_next(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_next(ii,k2,var) + (1.0_WP-wi)*inflowy_next(ii+1,k2,var))
                    Winy(i,k) = Winy(i,k) + sin(phase) * (interp_timey*prev + (1.0_WP-interp_timey)*next)
                    
                 case default
                    ! Interpolation in space and time for: scalars
                    ! Done in inflow_file_scalar
                 end select

                
              end do
           end do
        end do
    
     end if
        
     if (rescale_inflowy) then
        ! Compute the mean velocities
        vtmp = 0.0_WP
        wtmp = 0.0_WP
        do i = max(inlety(nflow)%imin,imin_),min(inlety(nflow)%imax,imax_)
           do k=kmin_,kmax_
              vtmp = vtmp + Vin(i,k)*dA(i,jmin,k)
              wtmp = wtmp + Win(i,k)*dA(i,jmin,k)
           end do
        end do
        call parallel_sum_dir(vtmp,vmean,'xz')
        call parallel_sum_dir(wtmp,wmean,'xz')
        vmean = vmean/inlety(nflow)%A
        wmean = wmean/inlety(nflow)%A
           
        ! Rescale the velocities by keeping constant the turbulent intensity
        do i = max(inlety(nflow)%imino,imino_),min(inlety(nflow)%imaxo,imaxo_)
           do k=kmino_,kmaxo_
              Vin(i,k) = Vin(i,k) * vbulky(nflow)/(vmean+epsilon(vmean))
              Win(i,k) = Win(i,k) * wbulky(nflow)/(wmean+epsilon(wmean))
           end do
        end do
     end if
        
  end do
  
  return
end subroutine inflowy_file_velocity

! ============================================================ !
! Precompute the scalar fields at the inlet for the given time !
! ============================================================ !
subroutine inflow_file_scalar
  use inflow_file
  implicit none
  
  real(WP) :: prev,next
  real(WP) :: wj,wk
  integer  :: j,k,jj,k1,k2
  integer  :: nflow,var,isc,k0,kPi
  
  ! Nothing to be done if no 'file' infow type
  if (file_inflow.ne.1) return
  
  ! Read if necessary the inflow file
  call inflow_file_read
  
  ! Different types of inflows
  do nflow=1,ninlet
     if (trim(inlet_type(nflow)).eq.'file') then
        do j=max(jmino_,inlet(nflow)%jmino),min(jmaxo_,inlet(nflow)%jmaxo)
           do k=kmino_,kmaxo_
              
              do var=1,inflow_nvar
                 select case(trim(inflow_name(var)))
                 case('U')
                    ! Interpolation in space and time for: U
                    ! Done in inflow_file_velocity
                 case('Ur')
                    ! Interpolation in space and time for: Ur
                    ! Done in inflow_file_velocity
                 case('Ui')
                    ! Interpolation in space and time for: Ui
                    ! Done in inflow_file_velocity
                 case('V')
                    ! Interpolation in space and time for: V
                    ! Done in inflow_file_velocity
                 case('Vr')
                    ! Interpolation in space and time for: Vr
                    ! Done in inflow_file_velocity
                 case('Vi')
                    ! Interpolation in space and time for: Vi
                    ! Done in inflow_file_velocity
                 case('W')
                    ! Interpolation in space and time for: W
                    ! Done in inflow_file_velocity
                 case('Wr')
                    ! Interpolation in space and time for: Wr
                    ! Done in inflow_file_velocity
                 case('Wi')
                    ! Interpolation in space and time for: Wi
                    ! Done in inflow_file_velocity
                 case('VOF1','VOF2','VOF3','VOF4','VOF5','VOF6','VOF7','VOF8')
                    ! Interpolation in space and time for: VOF
                    ! Done in inflow_file_VOF
                 case default
                    ! Interpolation in space and time for: scalars
                    isc = index_sc(var)
                    jj = index_cc_y(j,k)
                    k1 = index_cc_z(j,k); k2=mod(k1,inflow_nz)+1
                    wj = interp_cc_y(j,k)
                    wk = interp_cc_z(j,k)
                    prev =       wk *(wj*inflow_prev(jj,k1,var) + (1.0_WP-wj)*inflow_prev(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_prev(jj,k2,var) + (1.0_WP-wj)*inflow_prev(jj+1,k2,var))
                    next =       wk *(wj*inflow_next(jj,k1,var) + (1.0_WP-wj)*inflow_next(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_next(jj,k2,var) + (1.0_WP-wj)*inflow_next(jj+1,k2,var))
                    SCin(j,k,isc) = interp_time*prev + (1.0_WP-interp_time)*next
                 end select
              end do
           end do
        end do
        
        ! Cylindrical case
        if (icyl.eq.1 .and. max(inlet(nflow)%jmino,jmino_).eq.jmino) then
           
           do var=1,inflow_nvar
              isc = index_sc(var)
              if (isc.eq.0) cycle
              
              ! Get values around the axis
              call parallel_gather_dir(SCin(jmin,kmin_:kmax_,isc),SCaxis(kmin:kmax,isc),'z')
              
              do k=kmino_,kmaxo_
                 k0 = k
                 if (k.lt.kmin) k0 = k+nz
                 if (k.gt.kmax) k0 = k-nz
                 kPi = k0+nz/2
                 if (kPi.gt.kmax) kPi = kPi-nz  
                 
                 do j=1,nover
                    SCin(jmin-j,k,isc) = + SCaxis(kPi,isc)
                 end do
              end do
              
           end do
        end if
        
     end if
  end do
  
  return
end subroutine inflow_file_scalar


! ============================================================== !
! Precompute the scalar fields at the inlet y for the given time !
! ============================================================== !
subroutine inflowy_file_scalar
  use inflow_file
  implicit none
  
  real(WP) :: prev,next
  real(WP) :: wi,wk
  integer  :: i,k,ii,k1,k2
  integer  :: nflow,var,isc
  
  ! Nothing to be done if no 'file' infow type
  if (file_inflowy.ne.1) return
  
  ! Read if necessary the inflow file
  call inflowy_file_read
  
  ! Different types of inflows
  do nflow=1,ninlety
     if (trim(inlety_type(nflow)).eq.'file') then
        do i=max(imino_,inlety(nflow)%imino),min(imaxo_,inlety(nflow)%imaxo)
           do k=kmino_,kmaxo_
              
              do var=1,inflowy_nvar
                 select case(trim(inflowy_name(var)))
                 case('U')
                    ! Interpolation in space and time for: U
                    ! Done in inflowy_file_velocity
                 case('Ur')
                    ! Interpolation in space and time for: Ur
                    ! Done in inflowy_file_velocity
                 case('Ui')
                    ! Interpolation in space and time for: Ui
                    ! Done in inflowy_file_velocity
                 case('V')
                    ! Interpolation in space and time for: V
                    ! Done in inflowy_file_velocity
                 case('Vr')
                    ! Interpolation in space and time for: Vr
                    ! Done in inflowy_file_velocity
                 case('Vi')
                    ! Interpolation in space and time for: Vi
                    ! Done in inflowy_file_velocity
                 case('W')
                    ! Interpolation in space and time for: W
                    ! Done in inflowy_file_velocity
                 case('Wr')
                    ! Interpolation in space and time for: Wr
                    ! Done in inflowy_file_velocity
                 case('Wi')
                    ! Interpolation in space and time for: Wi
                    ! Done in inflowy_file_velocity
                 case('VOF1','VOF2','VOF3','VOF4','VOF5','VOF6','VOF7','VOF8')
                    ! Interpolation in space and time for: VOF
                    ! Done in inflowy_file_VOF
                 case default
                    ! Interpolation in space and time for: scalars
                    isc = indexy_sc(var)
                    ii = indexy_cc_x(i,k)
                    k1 = indexy_cc_z(i,k); k2=mod(k1,inflowy_nz)+1
                    wi = interpy_cc_x(i,k)
                    wk = interpy_cc_z(i,k)
                    prev =       wk *(wi*inflowy_prev(ii,k1,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_prev(ii,k2,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k2,var))
                    next =       wk *(wi*inflowy_next(ii,k1,var) + (1.0_WP-wi)*inflowy_next(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_next(ii,k2,var) + (1.0_WP-wi)*inflowy_next(ii+1,k2,var))
                    SCiny(i,k,isc) = interp_timey*prev + (1.0_WP-interp_timey)*next
                 end select
              end do
           end do
        end do
        
     end if
  end do
  
  return
end subroutine inflowy_file_scalar

! ======================================================== !
! Precompute the VOF field at the inlet for the given time !
! ======================================================== !
subroutine inflow_file_VOF
  use inflow_file
  implicit none
  
  real(WP) :: prev,next
  real(WP) :: wj,wk
  integer  :: j,k,jj,k1,k2
  integer  :: nflow,var,k0,kPi
  
  ! Nothing to be done if no 'file' infow type
  if (file_inflow.ne.1) return
  
  ! Read if necessary the inflow file
  call inflow_file_read
  
  ! Different types of inflows
  do nflow=1,ninlet
     if (trim(inlet_type(nflow)).eq.'file') then
        do j=max(jmino_,inlet(nflow)%jmino),min(jmaxo_,inlet(nflow)%jmaxo)
           do k=kmino_,kmaxo_
              
              do var=1,inflow_nvar
                 select case(trim(inflow_name(var)))
                 case('VOF1','VOF2','VOF3','VOF4','VOF5','VOF6','VOF7','VOF8')
                    ! Interpolation in space and time for: VOF
                    ! *** Interpolation of VOF in space is incorrect, needs to be done using geometric integration ***
                    jj = index_cc_y(j,k)
                    k1 = index_cc_z(j,k); k2=mod(k1,inflow_nz)+1
                    wj = interp_cc_y(j,k)
                    wk = interp_cc_z(j,k)
                    prev =       wk *(wj*inflow_prev(jj,k1,var) + (1.0_WP-wj)*inflow_prev(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_prev(jj,k2,var) + (1.0_WP-wj)*inflow_prev(jj+1,k2,var))
                    next =       wk *(wj*inflow_next(jj,k1,var) + (1.0_WP-wj)*inflow_next(jj+1,k1,var)) + &
                         (1.0_WP-wk)*(wj*inflow_next(jj,k2,var) + (1.0_WP-wj)*inflow_next(jj+1,k2,var))
                    select case(trim(inflow_name(var)))
                    case('VOF1'); VOF1in(j,k) = interp_time*prev + (1.0_WP-interp_time)*next
                    case('VOF2'); VOF2in(j,k) = interp_time*prev + (1.0_WP-interp_time)*next
                    case('VOF3'); VOF3in(j,k) = interp_time*prev + (1.0_WP-interp_time)*next
                    case('VOF4'); VOF4in(j,k) = interp_time*prev + (1.0_WP-interp_time)*next
                    case('VOF5'); VOF5in(j,k) = interp_time*prev + (1.0_WP-interp_time)*next
                    case('VOF6'); VOF6in(j,k) = interp_time*prev + (1.0_WP-interp_time)*next
                    case('VOF7'); VOF7in(j,k) = interp_time*prev + (1.0_WP-interp_time)*next
                    case('VOF8'); VOF8in(j,k) = interp_time*prev + (1.0_WP-interp_time)*next
                    end select
                 case default
                    ! Interpolation in space and time for: velocity or scalars
                    ! Done in inflow_file_velocity/scalar
                 end select
              end do
           end do
        end do
        
        ! Cylindrical case
        if (icyl.eq.1 .and. max(inlet(nflow)%jmino,jmino_).eq.jmino) then
           
           ! Get values around the axis
           call parallel_gather_dir(VOF1in(jmin,kmin_:kmax_),VOFaxis(kmin:kmax),'z')
           call parallel_gather_dir(VOF2in(jmin,kmin_:kmax_),VOFaxis(kmin:kmax),'z')
           call parallel_gather_dir(VOF3in(jmin,kmin_:kmax_),VOFaxis(kmin:kmax),'z')
           call parallel_gather_dir(VOF4in(jmin,kmin_:kmax_),VOFaxis(kmin:kmax),'z')
           call parallel_gather_dir(VOF5in(jmin,kmin_:kmax_),VOFaxis(kmin:kmax),'z')
           call parallel_gather_dir(VOF6in(jmin,kmin_:kmax_),VOFaxis(kmin:kmax),'z')
           call parallel_gather_dir(VOF7in(jmin,kmin_:kmax_),VOFaxis(kmin:kmax),'z')
           call parallel_gather_dir(VOF8in(jmin,kmin_:kmax_),VOFaxis(kmin:kmax),'z')

           do k=kmino_,kmaxo_
              k0 = k
              if (k.lt.kmin) k0 = k+nz
              if (k.gt.kmax) k0 = k-nz
              kPi = k0+nz/2
              if (kPi.gt.kmax) kPi = kPi-nz  
              
              do j=1,nover
                 VOF1in(jmin-j,k) = + VOFaxis(kPi)
                 VOF2in(jmin-j,k) = + VOFaxis(kPi)
                 VOF3in(jmin-j,k) = + VOFaxis(kPi)
                 VOF4in(jmin-j,k) = + VOFaxis(kPi)
                 VOF5in(jmin-j,k) = + VOFaxis(kPi)
                 VOF6in(jmin-j,k) = + VOFaxis(kPi)
                 VOF7in(jmin-j,k) = + VOFaxis(kPi)
                 VOF8in(jmin-j,k) = + VOFaxis(kPi)
              end do
           end do
           
        end if
        
     end if
  end do
  
  return
end subroutine inflow_file_VOF

! ======================================================== !
! Precompute the VOF field at the inlet for the given time !
! ======================================================== !
subroutine inflowy_file_VOF
  use inflow_file
  implicit none
  
  real(WP) :: prev,next
  real(WP) :: wi,wk
  integer  :: i,k,ii,k1,k2
  integer  :: nflow,var
  
  ! Nothing to be done if no 'file' infow type
  if (file_inflowy.ne.1) return
  
  ! Read if necessary the inflow file
  call inflowy_file_read
  
  ! Different types of inflows
  do nflow=1,ninlety
     if (trim(inlety_type(nflow)).eq.'file') then
        do i=max(imino_,inlety(nflow)%imino),min(imaxo_,inlety(nflow)%imaxo)
           do k=kmino_,kmaxo_
              
              do var=1,inflowy_nvar
                 select case(trim(inflowy_name(var)))
                 case('VOF1','VOF2','VOF3','VOF4','VOF5','VOF6','VOF7','VOF8')
                    ! Interpolation in space and time for: VOF
                    ii = indexy_cc_x(i,k)
                    k1 = indexy_cc_z(i,k); k2=mod(k1,inflowy_nz)+1
                    wi = interpy_cc_x(i,k)
                    wk = interpy_cc_z(i,k)
                    prev =       wk *(wi*inflowy_prev(ii,k1,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_prev(ii,k2,var) + (1.0_WP-wi)*inflowy_prev(ii+1,k2,var))
                    next =       wk *(wi*inflowy_next(ii,k1,var) + (1.0_WP-wi)*inflowy_next(ii+1,k1,var)) + &
                         (1.0_WP-wk)*(wi*inflowy_next(ii,k2,var) + (1.0_WP-wi)*inflowy_next(ii+1,k2,var))
                    select case(trim(inflowy_name(var)))
                    case ('VOF1'); VOF1iny(i,k) = interp_timey*prev + (1.0_WP-interp_timey)*next
                    case ('VOF2'); VOF2iny(i,k) = interp_timey*prev + (1.0_WP-interp_timey)*next
                    case ('VOF3'); VOF3iny(i,k) = interp_timey*prev + (1.0_WP-interp_timey)*next
                    case ('VOF4'); VOF4iny(i,k) = interp_timey*prev + (1.0_WP-interp_timey)*next
                    case ('VOF5'); VOF5iny(i,k) = interp_timey*prev + (1.0_WP-interp_timey)*next
                    case ('VOF6'); VOF6iny(i,k) = interp_timey*prev + (1.0_WP-interp_timey)*next
                    case ('VOF7'); VOF7iny(i,k) = interp_timey*prev + (1.0_WP-interp_timey)*next
                    case ('VOF8'); VOF8iny(i,k) = interp_timey*prev + (1.0_WP-interp_timey)*next
                    end select
                 case default
                    ! Interpolation in space and time for: velocity or scalars
                    ! Done in inflow_file_velocity/scalar
                 end select
              end do
           end do
        end do
                
     end if
  end do
  
  return
end subroutine inflowy_file_VOF
