! ======================================== !
! Inflow velocity recycling from a plane,  !
! for turbulent boundary layer simulations !
! Follow a simplified version of method by !
! Lund, Wu, Squires (1998) for now         !
!                                          !
!      Assumes that wall is at y=0         !
! ======================================== !
module inflow_recycle
  use inflow
  use parallel
  implicit none
  
  ! Flag
  logical :: use_recycling
  
  ! Momentum thickness
  real(WP) :: theta
  
  ! Recycling location
  real(WP) :: xrec
  integer  :: irec
  
  ! Work arrays
  real(WP), dimension(:,:), allocatable :: Urec
  real(WP), dimension(:,:), allocatable :: Vrec
  real(WP), dimension(:,:), allocatable :: Wrec
  real(WP), dimension(:,:), allocatable :: tmprec
  real(WP), dimension(:),   allocatable :: Umean
  real(WP), dimension(:),   allocatable :: ym_inflow
  real(WP), dimension(:),   allocatable :: y_inflow
  
end module inflow_recycle


! ===================================== !
! Initialization of recycling procedure !
! ===================================== !
subroutine inflow_recycle_init
  use inflow_recycle
  use parser
  implicit none
  
  integer :: nflow
  
  ! Do we use recycling?
  use_recycling=.false.
  do nflow=1,ninlet
     if (trim(inlet_type(nflow)).eq.'recycle') use_recycling=.true.
  end do
  if (.not.use_recycling) return
  
  ! Read in recycling parameters
  call parser_read('Recycling plane',xrec)
  call parser_read('Momentum thickness',theta)
  
  ! Find closest index to plane
  if (xrec.lt.x(imin).or.xrec.ge.x(imax+1)) call die("inflow_recycle_init: recycling plane should be in the domain")
  irec=imin
  do while (x(irec+1).le.xrec)
     irec=irec+1
  end do
  
  ! Allocate work arrays - semi-global for simplicity
  allocate(Urec(jmino:jmaxo,kmino:kmaxo)); Urec=0.0_WP
  allocate(Vrec(jmino:jmaxo,kmino:kmaxo)); Vrec=0.0_WP
  allocate(Wrec(jmino:jmaxo,kmino:kmaxo)); Wrec=0.0_WP
  allocate(tmprec(jmino:jmaxo,kmino:kmaxo)); tmprec=0.0_WP
  allocate(Umean(jmino:jmaxo)); Umean=0.0_WP
  allocate(ym_inflow(jmino:jmaxo)); ym_inflow=0.0_WP
  allocate(y_inflow(jmino:jmaxo+1)); y_inflow=0.0_WP
  
  return
end subroutine inflow_recycle_init


! ================== !
! Velocity recycling !
! ================== !
subroutine inflow_recycle_velocity
  use inflow_recycle
  use data
  use time_info
  implicit none
  
  integer :: j,k,ierr,jrec
  real(WP) :: theta_rec,gamma_rec
  real(WP) :: wy1,wy2,myUbulk,Utmp,Atmp,U_inf
  real(WP) :: ubulk_avg, ubulk_avg_in, timeAvg
  
  ! Do we use recycling?
  if (.not.use_recycling) return
  
  ! Gather the velocity from recycle plane to work arrays
  tmprec=0.0_WP
  if (irec.ge.imin_.and.irec.le.imax_) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           tmprec(j,k)=U(irec,j,k)
        end do
     end do
  end if
  call MPI_AllReduce(tmprec,Urec,nyo*nzo,MPI_REAL_WP,MPI_SUM,comm,ierr)
  tmprec=0.0_WP
  if (irec.ge.imin_.and.irec.le.imax_) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           tmprec(j,k)=V(irec,j,k)
        end do
     end do
  end if
  call MPI_AllReduce(tmprec,Vrec,nyo*nzo,MPI_REAL_WP,MPI_SUM,comm,ierr)
  tmprec=0.0_WP
  if (irec.ge.imin_.and.irec.le.imax_) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           tmprec(j,k)=W(irec,j,k)
        end do
     end do
  end if
  call MPI_AllReduce(tmprec,Wrec,nyo*nzo,MPI_REAL_WP,MPI_SUM,comm,ierr)

  ! Create average profile
  Umean=0.0_WP
  do j=jmin,jmax
     do k=kmin,kmax
        Umean(j)=Umean(j)+Urec(j,k)
     end do
  end do
  Umean=Umean/real(nz,WP)
  
  ! Compute free-stream velocity
  U_inf=Umean(jmax)
  if (U_inf.eq.0.0_WP) call die("inflow_recycle_velocity: cannot recycle with zero free-stream velocity")
  
  ! Compute momentum thickness
  theta_rec=0.0_WP
  do j=jmin+1,jmax
     theta_rec=theta_rec+(U_inf-Umean(j))*Umean(j)*(ym(j+1)-ym(j))/U_inf**2
  end do
  if (theta_rec.le.0.0_WP) then 
     theta_rec=theta
  end if
  
  ! Compute scaling factor
  gamma_rec=theta_rec/theta
  
  ! Create scaled mesh
  ym_inflow=ym*gamma_rec
  y_inflow =y *gamma_rec

  ! Introduce shift in z and handle periodic boundary conditions
  do k=kmino,kmaxo
     Urec(jmino:jmaxo,k)=+Urec(jmino:jmaxo,kmin+mod(k-kmin+nz/2,nz))
     Vrec(jmino:jmaxo,k)=+Vrec(jmino:jmaxo,kmin+mod(k-kmin+nz/2,nz))
     Wrec(jmino:jmaxo,k)=-Wrec(jmino:jmaxo,kmin+mod(k-kmin+nz/2,nz))
  end do

  ! Provide y boundary conditions for recycling
  do j=jmax+1,jmaxo
     Urec(j,:)=Urec(jmax,:)
  end do
  Vrec(jmax+1:jmaxo,:)=0.0_WP
  Wrec(jmax+1:jmaxo,:)=0.0_WP
  Urec(jmino:jmin-1,:)=0.0_WP
  Vrec(jmino:jmin-1,:)=0.0_WP
  Wrec(jmino:jmin-1,:)=0.0_WP

  ! Interpolate U/Wrec on ym to U/Win on ym_inflow
  do jrec=jmino_,jmaxo_
     if (ym_inflow(jrec).ge.ym(jmaxo)) then
        Uin(jrec,:)=Urec(jmaxo,kmino_:kmaxo_)
        Win(jrec,:)=Wrec(jmaxo,kmino_:kmaxo_)
     else
        j=jmino
        do while (ym(j+1).le.ym_inflow(jrec).and.j+1.lt.jmaxo)
           j=j+1
        end do
        ! Interpolate
        wy1=(ym_inflow(jrec)-ym(j))/(ym(j+1)-ym(j))
        Uin(jrec,:)=Urec(j,kmino_:kmaxo_)+(Urec(j+1,kmino_:kmaxo_)-Urec(j,kmino_:kmaxo_))*wy1
        Win(jrec,:)=Wrec(j,kmino_:kmaxo_)+(Wrec(j+1,kmino_:kmaxo_)-Wrec(j,kmino_:kmaxo_))*wy1
     end if
  end do
  
  ! Interpolate Vrec on y to Vin on y_inflow
  do jrec=jmino_,jmaxo_
     if (y_inflow(jrec).ge.y(jmaxo)) then
        Vin(jrec,:)=Vrec(jmaxo,kmino_:kmaxo_)
     else
        j=jmino
        do while (y(j+1).le.y_inflow(jrec).and.j+1.lt.jmaxo)
           j=j+1
        end do
        ! Interpolate
        wy1=(y_inflow(jrec)-y(j))/(y(j+1)-y(j))
        Vin(jrec,:)=Vrec(j,kmino_:kmaxo_)+(Vrec(j+1,kmino_:kmaxo_)-Vrec(j,kmino_:kmaxo_))*wy1
     end if
  end do
  
  ! Correct bulk velocity
  Utmp=0.0_WP
  Atmp=0.0_WP
  if (iproc.eq.1) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           Utmp=Utmp+dA(imin,j,k)*Uin(j,k)
           Atmp=Atmp+dA(imin,j,k)
        end do
     end do
  end if
  call MPI_AllReduce(Utmp,ubulk_avg,1,MPI_REAL_WP,MPI_SUM,comm,ierr)
  call MPI_AllReduce(Atmp,Utmp,1,MPI_REAL_WP,MPI_SUM,comm,ierr)
  ubulk_avg=ubulk_avg/Utmp+epsilon(1.0_WP)
  ubulk_avg_in=ubulk(1)
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        Uin(j,k)=Uin(j,k)*ubulk_avg_in/ubulk_avg
        Vin(j,k)=Vin(j,k)*ubulk_avg_in/ubulk_avg
        Win(j,k)=Win(j,k)*ubulk_avg_in/ubulk_avg
     end do
  end do
  
  return
end subroutine inflow_recycle_velocity
