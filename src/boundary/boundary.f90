module boundary
  use precision
  use geometry
  use partition
  implicit none

  ! Change of mass in the domain
  real(WP) :: masschange

  ! Momentum flux across boundaries
  real(WP) :: massflux,massflux_exit
  
  ! Added mass in the domain
  real(WP) :: massadded
  
  ! Mass correction
  real(WP) :: masscorrection
  
contains

  ! ========================================== !
  ! Compute change in mass flux across all BCs !
  ! ========================================== !
  subroutine boundary_massflux
    use data
    use parallel
    use masks
    implicit none

    real(WP) :: tmp
    integer :: i,j,k

    ! Return if periodic in x
    tmp = 0.0_WP
    massflux_exit = 0.0_WP
    if (xper.eq.1) return
    
    ! Left BC
    if (xper.ne.1 .and. iproc.eq.1) then
       do j=jmin_,jmax_
          do k=kmin_,kmax_
             tmp = tmp + rhoU(imin,j,k)*dA(imin,j,k)
          end do
       end do
    end if
    ! Right BC
    if (xper.ne.1 .and. iproc.eq.npx) then
       do j=jmin_,jmax_
          do k=kmin_,kmax_
             tmp = tmp - rhoU(imax+1,j,k)*dA(imax+1,j,k)
             massflux_exit = massflux_exit + rhoU(imax+1,j,k)*dA(imax+1,j,k)
          end do
       end do
    end if
    ! Lower BC
    ! Lower BC
    if (yper.ne.1 .and. jproc.eq.1) then
       do i=imin_,imax_
          do k=kmin_,kmax_
            tmp = tmp + rhoV(i,jmin,k)*dAy(i,jmin,k)
          end do
       end do
    end if
    ! Upper BC
    if (yper.ne.1 .and. jproc.eq.npy) then
       do i=imin_,imax_
          do k=kmin_,kmax_
             tmp = tmp - rhoV(i,jmax+1,k)*dAy(i,jmax+1,k)
          end do
       end do
    end if
    
    call parallel_sum(tmp,massflux)
    call parallel_sum(massflux_exit,tmp)
    massflux_exit=tmp
    
    return
  end subroutine boundary_massflux


  ! ======================================== !
  ! Compute mass of change inside the domain !
  ! ======================================== !
  subroutine boundary_masschange
    use combustion
    use time_info
    implicit none
    
    masschange = sum_dRHO/dt
    
    return
  end subroutine boundary_masschange
  
  
  ! ==================================== !
  ! Compute added mass inside the domain !
  ! ==================================== !
  subroutine boundary_massadded
    use velocity
    use time_info
    use parallel
    implicit none
    real(WP) :: tmp,sum_srcP
    integer :: i,j,k
    
    tmp = 0.0_WP
    do k=kmin_,kmax_
       do j=jmin_,jmax_
          do i=imin_,imax_
             tmp=tmp+srcPmid(i,j,k)*vol(i,j,k)
          end do
       end do
    end do
    call parallel_sum(tmp,sum_srcP)
    massadded = sum_srcP/dt
    
    return
  end subroutine boundary_massadded
  
  
end module boundary


! ============================== !
! Initialize the Boundary module !
! ============================== !
subroutine boundary_init
  use boundary
  use parallel
  use data
  use masks
  implicit none
  integer :: i,j
  
  ! Default treatment of the walls
  do j=jmino_,jmaxo_
     do i=imino_,imaxo_
        if (mask_u(i,j).eq.1) then
           U(i,j,:) = 0.0_WP
           rhoU(i,j,:) = 0.0_WP
        end if
        if (mask_v(i,j).eq.1) then
           V(i,j,:) = 0.0_WP
           rhoV(i,j,:) = 0.0_WP
        end if
        if (mask_w(i,j).eq.1) then
           W(i,j,:) = 0.0_WP
           rhoW(i,j,:) = 0.0_WP
        end if
        if (mask(i,j).eq.1) then
           if (nscalar.gt.0) SC(i,j,:,:) = 0.0_WP
           RHO(i,j,:) = 1.0_WP
           RHOold(i,j,:) = 1.0_WP
           dRHO(i,j,:) = 0.0_WP
        end if
     end do
  end do
  
  ! Inflow/Outflow if not periodic
  call inflow_init
  call outflow_init

  return
end subroutine boundary_init


! ========================== !
! Apply Dirichlet Conditions !
! ========================== !
subroutine boundary_dirichlet(vec,dir)
  use boundary
  use parallel
  implicit none
  
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: vec
  character(len=*), intent(in) :: dir
  integer :: i,j
  
  select case(trim(dir))
  case('-x')
     if (xper.eq.0 .and. iproc.eq.1) then
        do i=imino,imin
           vec(i,:,:) = 0.0_WP
        end do
     end if
  case('+x')
     if (xper.eq.0 .and. iproc.eq.npx) then
        do i=imax+1,imaxo
           vec(i,:,:) = 0.0_WP
        end do
     end if
  case('-y')
     if (yper.eq.0 .and. jproc.eq.1 .and. icyl.eq.0) then
        do j=jmino,jmin
           vec(:,j,:) = 0.0_WP
        end do
     end if
  case('+y')
     if (yper.eq.0 .and. jproc.eq.npy) then
        do j=jmax+1,jmaxo
           vec(:,j,:) = 0.0_WP
        end do
     end if
  case('-xm')
     if (xper.eq.0 .and. iproc.eq.1) then
        do i=imino,imin-1
           vec(i,:,:) = 0.0_WP
        end do
     end if
  case('+xm')
     if (xper.eq.0 .and. iproc.eq.npx) then
        do i=imax+1,imaxo
           vec(i,:,:) = 0.0_WP
        end do
     end if
  case('-ym')
     if (yper.eq.0 .and. jproc.eq.1 .and. icyl.eq.0) then
        do j=jmino,jmin-1
           vec(:,j,:) = 0.0_WP
        end do
     end if
  case('+ym')
     if (yper.eq.0 .and. jproc.eq.npy) then
        do j=jmax+1,jmaxo
           vec(:,j,:) = 0.0_WP
        end do
     end if
  end select
  
  return
end subroutine boundary_dirichlet


! ============================ !
! Apply Von Neumann Conditions !
! ============================ !
subroutine boundary_neumann(vec,dir)
  use boundary
  use parallel
  implicit none
  
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: vec
  character(len=*), intent(in) :: dir
  integer :: i,j
  
  select case(trim(dir))
  case('-x')
     if (xper.eq.0 .and. iproc.eq.1) then
        do i=imino,imin
           vec(i,:,:) = vec(imin+1,:,:)
        end do
     end if
  case('+x')
     if (xper.eq.0 .and. iproc.eq.npx) then
        do i=imax+1,imaxo
           vec(i,:,:) = vec(imax,:,:)
        end do
     end if
  case('-y')
     if (yper.eq.0 .and. jproc.eq.1 .and. icyl.eq.0) then
        do j=jmino,jmin
           vec(:,j,:) = vec(:,jmin+1,:)
        end do
     end if
  case('+y')
     if (yper.eq.0 .and. jproc.eq.npy) then
        do j=jmax+1,jmaxo
           vec(:,j,:) = vec(:,jmax,:)
        end do
     end if
  case('-xm')
     if (xper.eq.0 .and. iproc.eq.1) then
        do i=imino,imin-1
           vec(i,:,:) = vec(imin,:,:)
        end do
     end if
  case('+xm')
     if (xper.eq.0 .and. iproc.eq.npx) then
        do i=imax+1,imaxo
           vec(i,:,:) = vec(imax,:,:)
        end do
     end if
  case('-ym')
     if (yper.eq.0 .and. jproc.eq.1 .and. icyl.eq.0) then
        do j=jmino,jmin-1
           vec(:,j,:) = vec(:,jmin,:)
        end do
     end if
  case('+ym')
     if (yper.eq.0 .and. jproc.eq.npy) then
        do j=jmax+1,jmaxo
           vec(:,j,:) = vec(:,jmax,:)
        end do
     end if
  end select
  
  return
end subroutine boundary_neumann


! ============================ !
! Apply Von Neumann Conditions !
! ============================ !
subroutine boundary_neumann_int(vec,dir)
  use boundary
  use parallel
  implicit none
  
  integer, dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: vec
  character(len=*), intent(in) :: dir
  integer :: i,j
  
  select case(trim(dir))
  case('-x')
     if (xper.eq.0 .and. iproc.eq.1) then
        do i=imino,imin
           vec(i,:,:) = vec(imin+1,:,:)
        end do
     end if
  case('+x')
     if (xper.eq.0 .and. iproc.eq.npx) then
        do i=imax+1,imaxo
           vec(i,:,:) = vec(imax,:,:)
        end do
     end if
  case('-y')
     if (yper.eq.0 .and. jproc.eq.1 .and. icyl.eq.0) then
        do j=jmino,jmin
           vec(:,j,:) = vec(:,jmin+1,:)
        end do
     end if
  case('+y')
     if (yper.eq.0 .and. jproc.eq.npy) then
        do j=jmax+1,jmaxo
           vec(:,j,:) = vec(:,jmax,:)
        end do
     end if
  case('-xm')
     if (xper.eq.0 .and. iproc.eq.1) then
        do i=imino,imin-1
           vec(i,:,:) = vec(imin,:,:)
        end do
     end if
  case('+xm')
     if (xper.eq.0 .and. iproc.eq.npx) then
        do i=imax+1,imaxo
           vec(i,:,:) = vec(imax,:,:)
        end do
     end if
  case('-ym')
     if (yper.eq.0 .and. jproc.eq.1 .and. icyl.eq.0) then
        do j=jmino,jmin-1
           vec(:,j,:) = vec(:,jmin,:)
        end do
     end if
  case('+ym')
     if (yper.eq.0 .and. jproc.eq.npy) then
        do j=jmax+1,jmaxo
           vec(:,j,:) = vec(:,jmax,:)
        end do
     end if
  end select
  
  return
end subroutine boundary_neumann_int


! =========================== !
! Apply Reflective Conditions !
! =========================== !
subroutine boundary_reflective(vec,dir)
  use boundary
  use parallel
  implicit none
  
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: vec
  character(len=*), intent(in) :: dir
  integer :: i,j
  
  select case(trim(dir))
  case('-x')
     if (xper.eq.0 .and. iproc.eq.1) then
        do i=imino,imin
           vec(i,:,:) = -vec(imin+1,:,:)
        end do
     end if
  case('+x')
     if (xper.eq.0 .and. iproc.eq.npx) then
        do i=imax+1,imaxo
           vec(i,:,:) = -vec(imax,:,:)
        end do
     end if
  case('-y')
     if (yper.eq.0 .and. jproc.eq.1 .and. icyl.eq.0) then
        do j=jmino,jmin
           vec(:,j,:) = -vec(:,jmin+1,:)
        end do
     end if
  case('+y')
     if (yper.eq.0 .and. jproc.eq.npy) then
        do j=jmax+1,jmaxo
           vec(:,j,:) = -vec(:,jmax,:)
        end do
     end if
  case('-xm')
     if (xper.eq.0 .and. iproc.eq.1) then
        do i=imino,imin-1
           vec(i,:,:) = -vec(imin,:,:)
        end do
     end if
  case('+xm')
     if (xper.eq.0 .and. iproc.eq.npx) then
        do i=imax+1,imaxo
           vec(i,:,:) = -vec(imax,:,:)
        end do
     end if
  case('-ym')
     if (yper.eq.0 .and. jproc.eq.1 .and. icyl.eq.0) then
        do j=jmino,jmin-1
           vec(:,j,:) = -vec(:,jmin,:)
        end do
     end if
  case('+ym')
     if (yper.eq.0 .and. jproc.eq.npy) then
        do j=jmax+1,jmaxo
           vec(:,j,:) = -vec(:,jmax,:)
        end do
     end if
  end select
  
  return
end subroutine boundary_reflective


! ======================================== !
! Update the ghost cells                   !
! Not corresponding to boundary conditions !
! -> Domain decomposition                  !
! -> Periodic directions                   !
! -> Centerline                            !
! ======================================== !
subroutine boundary_update_border(A,sym,axis)
  use boundary
  use parallel
  implicit none
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: A
  character(len=*), intent(in) :: sym,axis
  real(WP) :: coeff

  ! Domain decomposition
  ! Periodic direction
  call communication_border(A)
  
  ! Centerline
  if (trim(sym).eq.'+') coeff = +1.0_WP
  if (trim(sym).eq.'-') coeff = -1.0_WP
  if (trim(axis).eq.'y')  call centerline_update_y (A,coeff)
  if (trim(axis).eq.'ym') call centerline_update_ym(A,coeff)

  return
end subroutine boundary_update_border

! ==================================== !
! Boundary update border for intergers !
! ==================================== !
subroutine boundary_update_border_int(A,sym,axis)
  use boundary
  use parallel
  implicit none
  integer, dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: A
  character(len=*), intent(in) :: sym,axis
  integer :: coeff

  ! Domain decomposition
  ! Periodic direction
  call communication_border_int(A)
  
  ! Centerline
  if (trim(sym).eq.'+') coeff = +1
  if (trim(sym).eq.'-') coeff = -1
  if (trim(axis).eq.'y')  call centerline_update_y_int (A,coeff)
  if (trim(axis).eq.'ym') call centerline_update_ym_int(A,coeff)

  return
end subroutine boundary_update_border_int


! =========================================== !
! Compute Dirichlet BCs for the new time step !
! And apply them to the velocity components   !
! -> Inlet                                    !
! =========================================== !
subroutine boundary_velocity_dirichlet
  use boundary
  use parallel
  use data
  implicit none

  ! Inflow Condition (left)
  call inflow_velocity

  ! Open boundary conditions
  ! Bottom
  if (.not.use_inlety)  call boundary_dirichlet(V,'-y')
  ! Top
  if (.not.use_outlety) call boundary_dirichlet(V,'+y')
  
  return
end subroutine boundary_velocity_dirichlet


! =========================================== !
! Compute Dirichlet BCs for the new time step !
! And apply them to the scalars               !
! -> Inlet                                    !
! =========================================== !
subroutine boundary_scalar_dirichlet
  use boundary
  use parallel
  use data
  implicit none

  ! Inflow condition (left)
  call inflow_scalar
  
  return
end subroutine boundary_scalar_dirichlet


! ======================================= !
! Compute Neumann BCs for each iteration  !
! And apply them to the momentum          !
! -> Open Boundaries                      !
! ======================================= !
subroutine boundary_momentum_neumann
  use boundary
  use parallel
  use data
  implicit none
  
  ! Open boundary conditions
  ! Bottom
  call boundary_neumann(rhoU,'-ym')
  call boundary_neumann(rhoW,'-ym')
  ! Top
  call boundary_neumann(rhoU,'+ym')
  call boundary_neumann(rhoW,'+ym')
  
  return
end subroutine boundary_momentum_neumann


! ======================================= !
! Compute Neumann BCs for each iteration  !
! And apply them to the velocity          !
! -> Open Boundaries                      !
! ======================================= !
subroutine boundary_velocity_neumann
  use boundary
  use parallel
  use data
  implicit none
  
  ! Open boundary conditions
  ! Bottom
  call boundary_neumann(U,'-ym')
  call boundary_neumann(W,'-ym')
  ! Top
  call boundary_neumann(U,'+ym')
  call boundary_neumann(W,'+ym')
  
  return
end subroutine boundary_velocity_neumann


! =========================================== !
! Compute Neumann BCs for each iteration      !
! And apply them to the scalar field          !
! -> Open Boundaries                          !
! =========================================== !
subroutine boundary_scalar_neumann
  use boundary
  use parallel
  use data
  implicit none
  integer :: isc
  
  ! Open boundary conditions
  ! Bottom
  do isc=1,nscalar
     call boundary_neumann(SC(:,:,:,isc),'-ym')
  end do
  ! Top
  do isc=1,nscalar
     call boundary_neumann(SC(:,:,:,isc),'+ym')
  end do
  
  return
end subroutine boundary_scalar_neumann


! =========================================== !
! Compute Neumann BCs for each iteration      !
! And apply them to the density field         !
! -> Open Boundaries                          !
! =========================================== !
subroutine boundary_density_neumann
  use boundary
  use parallel
  use data
  implicit none
  
  ! Open boundary conditions
  ! Bottom
  call boundary_neumann(RHO,'-ym')
  ! Top
  call boundary_neumann(RHO,'+ym')
  
  return
end subroutine boundary_density_neumann


! ========================================= !
! Compute Convective BCs for each iteration !
! And apply them to the velocity            !
! -> Outflow                                !
! ========================================= !
subroutine boundary_velocity_outflow
  use boundary
  use parallel
  implicit none
  
  ! Outflow conditions (right)
  call outflow_velocity
  
  return
end subroutine boundary_velocity_outflow


! ========================================== !
! Compute Convective BCs for each iteration  !
! And apply them to the velocity             !
! -> Outflow                                 !
! ========================================== !
subroutine boundary_scalar_outflow
  use boundary
  use parallel
  implicit none
  
  ! Outflow conditions (right)
  call outflow_scalar
  
  return
end subroutine boundary_scalar_outflow


! =========================================== !
! Compute Neumann BCs for each iteration      !
! And apply them to the scalar field          !
! -> Open Boundaries                          !
! =========================================== !
subroutine boundary_neumann_alldir(A)
  use boundary
  use parallel
  use data
  implicit none
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: A
  integer :: i,j
  
  ! Left
  if (xper.eq.0 .and. iproc.eq.1) then
     do i=imino,imin-1
        A(i,:,:)=A(imin,:,:)
     end do
  end if
  ! Right
  if (xper.eq.0 .and. iproc.eq.npx) then
     do i=imax+1,imaxo
        A(i,:,:)=A(imax,:,:)
     end do
  end if
  ! Bottom
  if (yper.eq.0 .and. jproc.eq.1 .and. icyl.eq.0) then
     do j=jmino,jmin-1
        A(:,j,:)=A(:,jmin,:)
     end do
  end if
  ! Top
  if (yper.eq.0 .and. jproc.eq.npy) then
     do j=jmax+1,jmaxo
        A(:,j,:)=A(:,jmax,:)
     end do
  end if
  
  return
end subroutine boundary_neumann_alldir


! ====================================================== !
! Neumann conditions in +/-xm and +/-ym for scalar field !
! ====================================================== !
subroutine boundary_neumann_alldir_adv(A,no)
  use boundary
  use parallel
  use data
  implicit none
  integer, intent(in) :: no
  real(WP), dimension(imin_-no:imax_+no,jmin_-no:jmax_+no,kmin_-no:kmax_+no) :: A
  integer :: i,j
  
  ! Left
  if (xper.eq.0 .and. iproc.eq.1) then
     do i=imin-no,imin-1
        A(i,:,:)=A(imin,:,:)
     end do
  end if
  ! Right
  if (xper.eq.0 .and. iproc.eq.npx) then
     do i=imax+1,imax+no
        A(i,:,:)=A(imax,:,:)
     end do
  end if
  ! Bottom
  if (yper.eq.0 .and. jproc.eq.1 .and. icyl.eq.0) then
     do j=jmin-no,jmin-1
        A(:,j,:)=A(:,jmin,:)
     end do
  end if
  ! Top
  if (yper.eq.0 .and. jproc.eq.npy) then
     do j=jmax+1,jmax+no
        A(:,j,:)=A(:,jmax,:)
     end do
  end if
  
  return
end subroutine boundary_neumann_alldir_adv


! ===================================== !
! Apply symmetry BCs for a vector field !
! ===================================== !
subroutine boundary_symmetry_alldir(Ax,Ay,Az)
  use boundary
  use parallel
  use data
  implicit none
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: Ax,Ay,Az
  integer :: i,j
  
  ! Left
  if (xper.eq.0 .and. iproc.eq.1) then
     do i=imino,imin-1
        Ax(i,:,:)=-Ax(imin,:,:)
        Ay(i,:,:)=+Ay(imin,:,:)
        Az(i,:,:)=+Az(imin,:,:)
     end do
  end if
  ! Right
  if (xper.eq.0 .and. iproc.eq.npx) then
     do i=imax+1,imaxo
        Ax(i,:,:)=-Ax(imax,:,:)
        Ay(i,:,:)=+Ay(imax,:,:)
        Az(i,:,:)=+Az(imax,:,:)
     end do
  end if
  ! Bottom
  if (yper.eq.0 .and. jproc.eq.1 .and. icyl.eq.0) then
     do j=jmino,jmin-1
        Ax(:,j,:)=+Ax(:,jmin,:)
        Ay(:,j,:)=-Ay(:,jmin,:)
        Az(:,j,:)=+Az(:,jmin,:)
     end do
  end if
  ! Top
  if (yper.eq.0 .and. jproc.eq.npy) then
     do j=jmax+1,jmaxo
        Ax(:,j,:)=+Ax(:,jmax,:)
        Ay(:,j,:)=-Ay(:,jmax,:)
        Az(:,j,:)=+Az(:,jmax,:)
     end do
  end if
  
  return
end subroutine boundary_symmetry_alldir


! =========================================== !
! Compute and correct mass fluxes             !
! Ensures Poisson solver convergence          !
! =========================================== !
subroutine boundary_velocity_massflux
  use boundary
  implicit none
  
  ! Compute total... 
  !   - mass flux across boundaries
  !   - change of mass inside the domain
  ! Correct outflow for mass consistency
  call outflow_clip_negative
  call boundary_massflux
  call boundary_masschange
  call boundary_massadded
  masscorrection = massflux-masschange+massadded
  call outflow_correction
  
  return
end subroutine boundary_velocity_massflux
