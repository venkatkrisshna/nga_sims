module outflow
  use data
  use parallel
  use boundary
  use borders
  implicit none
  
end module outflow


! ====================== !
! Initialize the outflow !
! ====================== !
subroutine outflow_init
  use outflow
  use parser
  implicit none

  ! Nothing to do if:
  ! -> no outlet
  ! -> not last proc in x
  ! -> periodic in x
  if (noutlet.eq.0 .or. iproc.ne.npx .or. xper.eq.1) return
  
  return     
end subroutine outflow_init


! ============================================================ !
! Convective outflow condition for velocity/momentum           !
! Update the exit plane                                        !
!                                                              !
! DOES NOT update the ghost cells                              !
! boundary_update_border REQUIRED after it (except if ntime=0) !
! ============================================================ !
subroutine outflow_velocity
  use velocity
  use outflow
  use metric_generic
  use time_info
  implicit none
  
  real(WP) :: u_a,tmp,alpha,sigma
  real(WP) :: rhox,rhoy,rhoz
  real(WP) :: newU,newV,newW
  integer :: i,j,k
  
  ! Nothing to do if:
  ! -> no outlet
  ! -> periodic in x
  if (noutlet.eq.0 .or. xper.eq.1) return

  ! Take u_a to be the maximum velocity at the exit plane
  if (iproc.eq.npx) then
     tmp = maxval(abs(U(imax,:,:)))
  else
     tmp = 0.0_WP
  end if
  call parallel_max(tmp,u_a)
  
  ! Outlow condition only if last cpu in x
  if (iproc.eq.npx) then
     
     if (ntime.ne.0) then
        ! Convective condition: u,t = -c u,x
        ! Implicit formulation for stability reasons
        sigma = dt_uvw*u_a*dxi(imax)
        alpha = (1.0_WP-0.5_WP*sigma)/(1.0_WP+0.5_WP*sigma)
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              newU = alpha * rhoUold(imax+1,j,k) &
                   + (1.0_WP-alpha) * 0.5_WP*(rhoU(imax,j,k)+rhoUold(imax,j,k))
              rhox = sum(interp_sc_x(imax+1,j,:)*RHOmid(imax+1-st2:imax+1+st1,j,k))
              do i=imax+1,imaxo
                 rhoU(i,j,k) = newU
                 U(i,j,k) = newU / rhox
              end do
           end do
        end do
        
        sigma = dt_uvw*u_a*dxmi(imax)
        alpha = (1.0_WP-0.5_WP*sigma)/(1.0_WP+0.5_WP*sigma)
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              newV = alpha * Vold(imax+1,j,k) &
                   + (1.0_WP-alpha) * 0.5_WP*(V(imax,j,k)+Vold(imax,j,k))
              newW = alpha * Wold(imax+1,j,k) &
                   + (1.0_WP-alpha) * 0.5_WP*(W(imax,j,k)+Wold(imax,j,k))
              rhoy = sum(interp_sc_y(imax+1,j,:)*RHOmid(imax+1,j-st2:j+st1,k))
              rhoz = sum(interp_sc_z(imax+1,j,:)*RHOmid(imax+1,j,k-st2:k+st1))
              do i=imax+1,imaxo
                 V(i,j,k) = newV
                 rhoV(i,j,k) = newV * rhoy
                 W(i,j,k) = newW
                 rhoW(i,j,k) = newW * rhoz
              end do
           end do
        end do
     else
        ! Neumann condition
        do i=imax+1,imaxo
           U(i,:,:) = U(imax,:,:)
           V(i,:,:) = V(imax,:,:)
           W(i,:,:) = W(imax,:,:)
           rhoU(i,:,:) = rhoU(imax,:,:)
           rhoV(i,:,:) = rhoV(imax,:,:)
           rhoW(i,:,:) = rhoW(imax,:,:)
        end do
     end if
  end if
  
  return
end subroutine outflow_velocity


! =========================================== !
! Convective outflow condition for scalars    !
! Update the exit plane                       !
!                                             !
! DOES update the ghost cells                 !
! NO boundary_update_border required after it !
! =========================================== !
subroutine outflow_scalar
  use scalar
  use outflow
  use time_info
  implicit none
  
  real(WP) :: u_a,tmp,alpha,sigma,newSC
  integer :: i,j,k,isc
  
  ! Nothing to do if:
  ! -> no outlet
  ! -> periodic in x
  if (noutlet.eq.0 .or. xper.eq.1) return

  ! Take u_a to be the maximum velocity at the exit plane
  if (iproc.eq.npx) then
     tmp = maxval(abs(U(imax,:,:)))
  else
     tmp = 0.0_WP
  end if
  call parallel_max(tmp,u_a)
  
  ! Outlow condition only if last cpu in x
  if (iproc.eq.npx) then
     
     if (ntime.ne.0) then
        ! Convective condition: u,t = -c u,x
        ! Implicit formulation for stability reasons
        sigma = dt*u_a*dxmi(imax)
        alpha = (1.0_WP-0.5_WP*sigma)/(1.0_WP+0.5_WP*sigma)
        do isc=1,nscalar
           do k=kmin_,kmax_
              do j=jmin_,jmax_
                 newSC = alpha * SCold(imax+1,j,k,isc) &
                      + (1.0_WP-alpha) * 0.5_WP*(SC(imax,j,k,isc)+SCold(imax,j,k,isc))
                 do i=imax+1,imaxo
                    SC(i,j,k,isc) = newSC
                 end do
              end do
           end do
        end do
     else
        ! Neumann condition
        do i=imax+1,imaxo
           SC(i,:,:,:) = SC(imax,:,:,:)
        end do
     end if
  end if
  
  return
end subroutine outflow_scalar


! ============================================================ !
! Apply a correction to the outflow to ensure mass consistency !
! Update the exit plane                                        !
!                                                              !
! DOES update the ghost cells                                  !
! NO boundary_update_border required after it                  !
! ============================================================ !
subroutine outflow_correction
  use outflow
  use velocity
  use metric_generic
  implicit none
  integer :: i,j,k,nflow
  real(WP) :: newU,rhoi,alpha
  real(WP), parameter :: eps=1.0e-10_WP
  
  ! Nothing to do if:
  ! -> no outlet
  ! -> not last proc in x
  ! -> periodic in x
  if (noutlet.eq.0 .or. iproc.ne.npx .or. xper.eq.1) return
  
  if (massflux_exit.lt.eps) then
     do k=kmino_,kmaxo_
        do nflow=1,noutlet
           do j=max(jmino_,outlet(nflow)%jmino),min(jmaxo_,outlet(nflow)%jmaxo)
              newU = rhoU(imax+1,j,k) + masscorrection/outlet_area
              rhoi = sum(interp_sc_x(imax+1,j,:)*RHOmid(imax+1-st2:imax+1+st1,j,k))
              do i=imax+1,imaxo
                 rhoU(i,j,k) = newU
                 U(i,j,k) = newU / rhoi
              end do
           end do
        end do
     end do
  else
     alpha = 1.0_WP + masscorrection/massflux_exit
     do k=kmino_,kmaxo_
        do nflow=1,noutlet
           do j=max(jmino_,outlet(nflow)%jmino),min(jmaxo_,outlet(nflow)%jmaxo)
              newU = rhoU(imax+1,j,k) * alpha
              rhoi = sum(interp_sc_x(imax+1,j,:)*RHOmid(imax+1-st2:imax+1+st1,j,k))
              do i=imax+1,imaxo
                 rhoU(i,j,k) = newU
                 U(i,j,k) = newU / rhoi
              end do
           end do
        end do
     end do
  end if
  
  return
end subroutine outflow_correction


! ================================================= !
! Apply a correction to negative outflow velocities !
! ================================================= !
subroutine outflow_clip_negative
  use outflow
  use velocity
  implicit none
  
  integer :: i,j,k,nflow
  real(WP) :: u_a,tmp
  real(WP), parameter :: alpha=0.9_WP
  
  ! Nothing to do if:
  ! -> no outlet
  ! -> periodic in x
  if (noutlet.eq.0 .or. xper.eq.1) return
  
  ! Take u_a to be the maximum velocity at the exit plane
  if (iproc.eq.npx) then
     tmp=maxval(U(imax,:,:))
  else
     tmp=0.0_WP
  end if
  call parallel_max(tmp,u_a)
  if (u_a.lt.epsilon(1.0_WP)) return
  
  ! Clip large negative outflow velocity
  if (iproc.eq.npx) then
     do k=kmino_,kmaxo_
        do nflow=1,noutlet
           do j=max(jmino_,outlet(nflow)%jmino),min(jmaxo_,outlet(nflow)%jmaxo)
              do i=imax+1,imaxo
                 if (U(i,j,k) .lt. -alpha*u_a) then
                    rhoU(i,j,k) = -rhoU(i,j,k)*alpha*u_a/U(i,j,k)
                    U(i,j,k) = -alpha*u_a
                 end if
              end do
           end do
        end do
     end do
  end if
  
  return
end subroutine outflow_clip_negative
