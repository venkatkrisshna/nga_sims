module inflow
  use boundary
  use borders
  implicit none

  ! Type of inlets
  character(len=str_medium), dimension(:), allocatable :: inlet_type,inlety_type

  ! Plane of the variables at the inflow
  real(WP), dimension(:,:),   allocatable :: Uin,Uiny
  real(WP), dimension(:,:),   allocatable :: Vin,Viny
  real(WP), dimension(:,:),   allocatable :: Win,Winy
  real(WP), dimension(:,:,:), allocatable :: SCin,SCiny
  real(WP), dimension(:,:),   allocatable :: VOF1in,VOF2in,VOF3in,VOF4in,VOF5in,VOF6in,VOF7in,VOF8in
  real(WP), dimension(:,:),   allocatable :: VOF1iny,VOF2iny,VOF3iny,VOF4iny,VOF5iny,VOF6iny,VOF7iny,VOF8iny
  
  ! Bulk velocities
  real(WP), dimension(:), allocatable :: ubulk,vbulk,wbulk
  real(WP), dimension(:), allocatable :: ubulky,vbulky,wbulky  
  
  ! Rescale inflow files
  logical :: rescale_inflow,rescale_inflowy
  logical :: use_freefall=.false.
  logical :: use_randfluct=.false.
  real(WP) :: rand_flucts
  
contains

  ! Read the velocity values from the input file
  subroutine inflow_read_velocity
    use parser
    implicit none

    integer  :: nflow,nu,nw,j
    real(WP) :: factor,ymin,ymax
    logical  :: read_bulk
    
    ! Rescaling of the inflow using bulk velocity
    call parser_read('Inflow file rescale',rescale_inflow,.false.)
    
    ! Inflows might need bulk velocity anyway
    read_bulk = rescale_inflow
    do nflow=1,ninlet
       if ( trim(inlet_type(nflow)).ne.'file' .and. &
            trim(inlet_type(nflow)).ne.'tanh' ) read_bulk = .true.
    end do
    
    ! Read bulk velocity if necessary
    if (read_bulk) then
       ! Read the values for 'bulk' and 'laminar' and rescaled 'file'
       call parser_getsize('Inlet u velocity',nu)
       call parser_getsize('Inlet w velocity',nw)
       if (nu.ne.ninlet .or. nw.ne.ninlet) &
            call die('inflow_read_velocity: not enough Inlet u/w velocities')
       allocate(ubulk(ninlet))
       allocate(wbulk(ninlet))
       call parser_read('Inlet u velocity',ubulk)
       call parser_read('Inlet w velocity',wbulk)
    end if
    
    ! Set the values
    do nflow=1,ninlet
       select case(trim(inlet_type(nflow)))

       ! Bulk velocity
       ! Constant velocity for each inflow
       case('bulk')
          do j=max(jmino_,inlet(nflow)%jmino),min(jmaxo_,inlet(nflow)%jmaxo)
             Uin(j,:) = ubulk(nflow)
             Vin(j,:) = 0.0_WP
             Win(j,:) = wbulk(nflow)
          end do

       ! Laminar velocity profile
       ! Quadratic profile for each inflow
       case('laminar')
          ymin = y(inlet(nflow)%jmin)
          ymax = y(inlet(nflow)%jmax+1)
          if (icyl.eq.1) then
             if (inlet(nflow)%jmin.eq.jmin) then
                ymin = -ymax
                factor = 8.0_WP
             else
                factor = 6.0_WP
             end if
          else
             factor = 6.0_WP
          end if
          
          do j=max(jmino_,inlet(nflow)%jmino),min(jmaxo_,inlet(nflow)%jmaxo)
             Uin(j,:) = factor*ubulk(nflow)*(ym(j)-ymin)*(ymax-ym(j))/(ymax-ymin)**2
             Vin(j,:) = 0.0_WP
             Win(j,:) = factor*wbulk(nflow)*(ym(j)-ymin)*(ymax-ym(j))/(ymax-ymin)**2
          end do
          
       ! Mean turbulent inflow 
       ! Power law function for each inflow
       ! TO BE CHANGED - Only for central pipe in cyl
       case('power')
          ymax = y(inlet(nflow)%jmax+1)
          factor = 60.0_WP/49.0_WP
          
          do j=max(jmino_,inlet(nflow)%jmino),min(jmaxo_,inlet(nflow)%jmaxo)
             Uin(j,:) = factor*ubulk(nflow)*(1.0_WP-ym(j)/ymax)**(1.0_WP/7.0_WP)
             Vin(j,:) = 0.0_WP
             Win(j,:) = 0.0_WP
          end do

       ! Inflow to keep freefalling liquid centered in domain
       ! will be dealt with later
       case('freefall')
          use_freefall=.true.
          do j=max(jmino_,inlet(nflow)%jmino),min(jmaxo_,inlet(nflow)%jmaxo)
             Uin(j,:) = ubulk(nflow)
          end do
          
       ! Tanh + noise profile
       ! Will be dealt with later
       case ('tanh')
          
       ! Profile from a file
       ! Will be dealt with later
       case('file')
          
       ! Recycling from a plane
       ! Will be dealt with later
       case('recycle')

       case default
          call die('inflow_read_velocity: unknown velocity inlet type')
       end select
    end do
    
    return
  end subroutine inflow_read_velocity

  ! Read the velocity values from the input file
  subroutine inflowy_read_velocity
    use parser
    implicit none

    integer  :: nflow,nv,nw,i
    logical  :: read_bulk
    
    ! Rescaling of the inflow using bulk velocity
    call parser_read('Inflow y file rescale',rescale_inflowy,.false.)
    
    ! Inflows might need bulk velocity anyway
    read_bulk = rescale_inflowy
    do nflow=1,ninlety
       if (trim(inlety_type(nflow)).ne.'file') read_bulk = .true.
    end do
    
    ! Read bulk velocity if necessary
    if (read_bulk) then
       ! Read the values for 'bulk' and 'laminar' and rescaled 'file'
       call parser_getsize('Inlet y v velocity',nv)
       call parser_getsize('Inlet y w velocity',nw)
       if (nv.ne.ninlety .or. nw.ne.ninlety) &
            call die('inflowy_read_velocity: not enough Inlet v/w velocities')
       allocate(vbulky(ninlety))
       allocate(wbulky(ninlety))
       call parser_read('Inlet y v velocity',vbulky)
       call parser_read('Inlet y w velocity',wbulky)
    end if
    
    ! Set the values
    do nflow=1,ninlety
       select case(trim(inlety_type(nflow)))

       ! Bulk velocity
       ! Constant velocity for each inflow
       case('bulk')
          do i=max(imino_,inlety(nflow)%imino),min(imaxo_,inlety(nflow)%imaxo)
             Uiny(i,:) = 0.0_WP
             Viny(i,:) = vbulky(nflow)
             Winy(i,:) = wbulky(nflow)
          end do
                    
       ! Profile from a file
       ! Will be dealt with later
       case('file')
          
       case default
          call die('inflowy_read_velocity: unknown y velocity inlet type')
       end select
    end do
    
    return
  end subroutine inflowy_read_velocity
  
  ! Prepare a tanh velocity profile for injection
  subroutine inflow_tanh_velocity
    use data
    implicit none
    
    integer  :: j,k,nflow
    real(WP) :: Ujet,Ucof
    real(WP) :: radius,theta,r
    real(WP) :: rand,amp
    
    do nflow=1,ninlet
       
       if (trim(inlet_type(nflow)).eq.'tanh') then
          
          ! Set parameters
          Ujet=1.0_WP
          Ucof=25.0_WP
          radius=0.5_WP
          theta=0.025_WP
          amp=0.0_WP
          
          ! Base profile
          do k=kmino_,kmaxo_
             do j=jmino_,jmaxo_
                r = sqrt(ym(j)**2+zm(k)**2)
                Uin(j,k) = 0.5_WP*(Ujet+Ucof)-0.5*(Ujet-Ucof)*tanh(0.25_WP*radius/theta*(r/radius-radius/r))
                Vin(j,k) = 0.0_WP
                Win(j,k) = 0.0_WP
             end do
          end do
          
          ! Add noise
          do k=kmino_,kmaxo_
             do j=jmino_,jmaxo_
                r = sqrt(ym(j)**2+zm(k)**2)
                if (r.lt.0.4_WP) then
                   call random_number(rand); rand=amp*(2.0_WP*rand-1.0_WP)
                   Uin(j,k) = Uin(j,k) + 0.2_WP*rand
                   call random_number(rand); rand=amp*(2.0_WP*rand-1.0_WP)
                   Vin(j,k) = Vin(j,k) + 0.2_WP*rand
                   call random_number(rand); rand=amp*(2.0_WP*rand-1.0_WP)
                   Win(j,k) = Win(j,k) + 0.2_WP*rand
                else if (r.ge.0.4_WP .and. r.le.0.6_WP) then
                   call random_number(rand); rand=amp*(2.0_WP*rand-1.0_WP)
                   Uin(j,k) = Uin(j,k) + 1.0_WP*rand
                   call random_number(rand); rand=amp*(2.0_WP*rand-1.0_WP)
                   Vin(j,k) = Vin(j,k) + 1.0_WP*rand
                   call random_number(rand); rand=amp*(2.0_WP*rand-1.0_WP)
                   Win(j,k) = Win(j,k) + 1.0_WP*rand
                end if
             end do
          end do
          
       end if
       
    end do
    
    return
  end subroutine inflow_tanh_velocity

  ! Prepare a freefall velocity for injection
  subroutine inflow_freefall_velocity
    use data
    implicit none
    integer :: i,j,k
    real(WP) :: myVel,myBCx,myLiq
    real(WP) ::   Vel,  BCx,  Liq
    real(WP), parameter :: alpha=0.7_WP
    
    ! Check if used
    if (.not.use_freefall) return
    
    ! Velocity and barycenter of droplet
    myVel=0.0_WP
    myBCx=0.0_WP
    myLiq=0.0_WP
    do k=kmin_,kmax_
       do j=jmin_,jmax_
          do i=imin_,imax_
             myVel=myVel+0.125_WP*sum(VOF(:,i,j,k))*dx(i)*dy(j)*dz*0.5_WP*(U(i,j,k)+U(i+1,j,k))
             myBCx=myBCx+0.125_WP*sum(VOF(:,i,j,k))*dx(i)*dy(j)*dz*xm(i)
             myLiq=myLiq+0.125_WP*sum(VOF(:,i,j,k))*dx(i)*dy(j)*dz
          end do
       end do
    end do
    call parallel_sum(myVel,Vel)
    call parallel_sum(myBCx,BCx)
    call parallel_sum(myLiq,Liq)
    Vel=Vel/(Liq+epsilon(1.0_WP))
    BCx=BCx/(Liq+epsilon(1.0_WP))

    ! Inflow velocity
    if (iproc.ne.1) return
    Uin=Uin-alpha*Vel
    where (Uin.lt.0.0_WP)
       Uin=0.0_WP
    end where
    Vin=0.0_WP
    Win=0.0_WP
    
    ! Monitor
    if (use_freefall) then
       call monitor_select_file("inflow")
       call monitor_set_single_value(1,Vel)
       call monitor_set_single_value(2,maxval(Uin))
    end if
    
    return
  end subroutine inflow_freefall_velocity

  ! Read the scalar values from the input file
  subroutine inflow_read_scalar
    use parser
    use data
    implicit none

    integer :: isc,j,nflow,n,ninput
    character(len=str_medium), dimension(:), allocatable :: list
    character(len=str_medium) :: name
    real(WP) :: val

    ! Return if no scalar
    if (nscalar.eq.0) return
    
    ! Read the input and check it
    allocate(list(nscalar*(ninlet+1)))
    call parser_getsize('Inlet scalar values',ninput)
    if (ninput.ne.(1+ninlet)*nscalar) then
       if (irank.eq.iroot) then
          print*,'ninlet=',ninlet
          print*,'ninput=',ninput,ninput/nscalar-1,nscalar
       end if
       call die('inflow_init: Wrong number of Inlet scalar values')
    else
       call parser_read('Inlet scalar values',list)
    end if
    
    do n=1,nscalar
       ! Find the right scalar
       loop:do isc=1,nscalar
          name = list((n-1)*(ninlet+1)+1)
          if (trim(SC_name(isc)).eq.trim(name)) exit loop
       end do loop
       if (isc.eq.nscalar+1) &
            call die('inflow_read_scalar: unknown scalar name in input')

       ! Set the values
       do nflow=1,ninlet
          read(list((n-1)*(ninlet+1)+nflow+1),*) val
          do j=max(jmino_,inlet(nflow)%jmino),min(jmaxo_,inlet(nflow)%jmaxo)
             SCin(j,:,isc) = val
          end do
       end do
    end do
    
    return
  end subroutine inflow_read_scalar

  ! Read the scalar values from the input y file
  subroutine inflowy_read_scalar
    use parser
    use data
    implicit none

    integer :: isc,i,nflow,n,ninput
    character(len=str_medium), dimension(:), allocatable :: list
    character(len=str_medium) :: name
    real(WP) :: val

    ! Return if no scalar
    if (nscalar.eq.0) return
    
    ! Read the input and check it
    allocate(list(nscalar*(ninlety+1)))
    call parser_getsize('Inlet y scalar values',ninput)
    if (ninput.ne.(1+ninlety)*nscalar) then
       call die('inflow_init: Wrong number of Inlet y scalar values')
    else
       call parser_read('Inlet y scalar values',list)
    end if
    
    do n=1,nscalar
       ! Find the right scalar
       loop:do isc=1,nscalar
          name = list((n-1)*(ninlety+1)+1)
          if (trim(SC_name(isc)).eq.trim(name)) exit loop
       end do loop
       if (isc.eq.nscalar+1) &
            call die('inflow_read_scalar: unknown scalar name in input y')

       ! Set the values
       do nflow=1,ninlety
          read(list((n-1)*(ninlety+1)+nflow+1),*) val
          do i=max(imino_,inlety(nflow)%imino),min(imaxo_,inlety(nflow)%imaxo)
             SCiny(i,:,isc) = val
          end do
       end do
    end do

    return
  end subroutine inflowy_read_scalar  

  ! Read VOF values from the input file
  subroutine inflow_read_VOF
    use parser
    use data
    implicit none

    integer :: j,nflow,ninput
    real(WP), dimension(:), allocatable :: VOFbulk
    logical  :: read_bulk

    ! Return if no multiphase
    if (.not.use_multiphase) return

    read_bulk = .false.
    do nflow=1,ninlet
       if ( trim(inlet_type(nflow)).ne.'file' .and. &
            trim(inlet_type(nflow)).ne.'freefall' ) read_bulk = .true.
    end do
    ! Read the input if necessary
    if (read_bulk) then
       allocate(VOFbulk(ninlet))
       call parser_getsize('Inlet VOF values',ninput)
       if (ninput.ne.ninlet) then
          call die('inflow_init: Wrong number of Inlet VOF values')
       else
          call parser_read('Inlet VOF values',VOFbulk)
       end if
    end if
    
    ! Set the values
    do nflow=1,ninlet
       select case(trim(inlet_type(nflow)))

       ! Constant VOF for each inflow
       case ('bulk','laminar','power','tanh')
          do j=max(jmino_,inlet(nflow)%jmino),min(jmaxo_,inlet(nflow)%jmaxo)
             VOF1in(j,:) = VOFbulk(nflow)
             VOF2in(j,:) = VOFbulk(nflow)
             VOF3in(j,:) = VOFbulk(nflow)
             VOF4in(j,:) = VOFbulk(nflow)
             VOF5in(j,:) = VOFbulk(nflow)
             VOF6in(j,:) = VOFbulk(nflow)
             VOF7in(j,:) = VOFbulk(nflow)
             VOF8in(j,:) = VOFbulk(nflow)
          end do
          
       ! Profile from a file
       ! Will be dealt with later
       case ('file')

       case ('default')
          call die('inflow_read_VOF: unknown VOF inlet type')
       end select
    end do
    
    return
  end subroutine inflow_read_VOF

  ! Read VOF values from the input file
  subroutine inflowy_read_VOF
    use parser
    use data
    implicit none

    integer :: i,nflow,ninput
    real(WP), dimension(:), allocatable :: VOFbulk
    logical  :: read_bulk

    ! Return if no multiphase
    if (.not.use_multiphase) return

    read_bulk = .false.
    do nflow=1,ninlety
       if (trim(inlety_type(nflow)).ne.'file') read_bulk = .true.
    end do
    ! Read the input if necessary
    if (read_bulk) then
       allocate(VOFbulk(ninlet))
       call parser_getsize('Inlet y VOF values',ninput)
       if (ninput.ne.ninlet) then
          call die('inflow_init: Wrong number of Inlet y VOF values')
       else
          call parser_read('Inlet y VOF values',VOFbulk)
       end if
    end if
    
    ! Set the values
    do nflow=1,ninlety
       select case(trim(inlety_type(nflow)))

       ! Constant VOF for each inflow
       case ('bulk')
          do i=max(imino_,inlety(nflow)%imino),min(imaxo_,inlety(nflow)%imaxo)
             VOF1iny(i,:) = VOFbulk(nflow)
             VOF2iny(i,:) = VOFbulk(nflow)
             VOF3iny(i,:) = VOFbulk(nflow)
             VOF4iny(i,:) = VOFbulk(nflow)
             VOF5iny(i,:) = VOFbulk(nflow)
             VOF6iny(i,:) = VOFbulk(nflow)
             VOF7iny(i,:) = VOFbulk(nflow)
             VOF8iny(i,:) = VOFbulk(nflow)
          end do
          
       ! Profile from a file
       ! Will be dealt with later
       case ('file')

       case ('default')
          call die('inflowy_read_VOF: unknown VOF inlet y type')
       end select
    end do
    
    return
  end subroutine inflowy_read_VOF
  
end module inflow


! ============================ !
! Initilaize the inflow        !
! -> split the communicator    !
! -> detect the inflows        !
! ============================ !
subroutine inflow_init
  use inflow
  use data
  use parser
  implicit none

  integer :: ninput,ierr

  ! Inflow x
  ! ======================================
  ! If periodic in x => no inflow
  if (xper.ne.1 .and. ninlet.ne.0) then
     ! Read the types of velocity inflow from the input file 
     call parser_getsize('Inlet velocity type',ninput)
     allocate(inlet_type(ninlet))
     if (ninput.eq.ninlet) then
        call parser_read('Inlet velocity type',inlet_type)
     else
        if (irank.eq.iroot) then
           print*,'ninlet=',ninlet
           print*,'ninput=',ninput
        end if
        call die('inflow_init: Wrong number of Inlet velocity type defined.')
     end if
     
     ! Allocate the inflow variables
     allocate(Uin    (jmino_:jmaxo_,kmino_:kmaxo_));           Uin=0.0_WP
     allocate(Vin    (jmino_:jmaxo_,kmino_:kmaxo_));           Vin=0.0_WP
     allocate(Win    (jmino_:jmaxo_,kmino_:kmaxo_));           Win=0.0_WP
     allocate(SCin   (jmino_:jmaxo_,kmino_:kmaxo_,nscalar));  SCin=0.0_WP
     allocate(VOF1in (jmino_:jmaxo_,kmino_:kmaxo_));        VOF1in=0.0_WP
     allocate(VOF2in (jmino_:jmaxo_,kmino_:kmaxo_));        VOF2in=0.0_WP
     allocate(VOF3in (jmino_:jmaxo_,kmino_:kmaxo_));        VOF3in=0.0_WP
     allocate(VOF4in (jmino_:jmaxo_,kmino_:kmaxo_));        VOF4in=0.0_WP
     allocate(VOF5in (jmino_:jmaxo_,kmino_:kmaxo_));        VOF5in=0.0_WP
     allocate(VOF6in (jmino_:jmaxo_,kmino_:kmaxo_));        VOF6in=0.0_WP
     allocate(VOF7in (jmino_:jmaxo_,kmino_:kmaxo_));        VOF7in=0.0_WP
     allocate(VOF8in (jmino_:jmaxo_,kmino_:kmaxo_));        VOF8in=0.0_WP

     ! Read the velocity values from the input file or inflow file
     ! Read the scalar values from the input file
     call inflow_read_velocity
     call inflow_read_scalar
     call inflow_read_VOF

     ! Initialize the inflow profile and read the first time step
     call inflow_file_init

     ! Initialize the velocity recycling routine
     call inflow_recycle_init

  end if
  call MPI_BARRIER(comm,ierr) ! Make all procs wait here
  
  ! Inflow y
  ! ======================================
  ! If periodic in y => no inflow
  if (yper.ne.1 .and. ninlety.ne.0) then
     
     ! Read the types of velocity inflow from the input file 
     call parser_getsize('Inlet y velocity type',ninput)
     allocate(inlety_type(ninlety))
     if (ninput.eq.ninlety) then
        call parser_read('Inlet y velocity type',inlety_type)
     else
        call die('inflow_init: Wrong number of Inlet y velocity type defined.')
     end if

     ! Allocate the inflow variables
     allocate(Uiny   (imino_:imaxo_,kmino_:kmaxo_));           Uiny=0.0_WP
     allocate(Viny   (imino_:imaxo_,kmino_:kmaxo_));           Viny=0.0_WP
     allocate(Winy   (imino_:imaxo_,kmino_:kmaxo_));           Winy=0.0_WP
     allocate(SCiny  (imino_:imaxo_,kmino_:kmaxo_,nscalar));  SCiny=0.0_WP
     allocate(VOF1iny(imino_:imaxo_,kmino_:kmaxo_));        VOF1iny=0.0_WP
     allocate(VOF2iny(imino_:imaxo_,kmino_:kmaxo_));        VOF2iny=0.0_WP
     allocate(VOF3iny(imino_:imaxo_,kmino_:kmaxo_));        VOF3iny=0.0_WP
     allocate(VOF4iny(imino_:imaxo_,kmino_:kmaxo_));        VOF4iny=0.0_WP
     allocate(VOF5iny(imino_:imaxo_,kmino_:kmaxo_));        VOF5iny=0.0_WP
     allocate(VOF6iny(imino_:imaxo_,kmino_:kmaxo_));        VOF6iny=0.0_WP
     allocate(VOF7iny(imino_:imaxo_,kmino_:kmaxo_));        VOF7iny=0.0_WP
     allocate(VOF8iny(imino_:imaxo_,kmino_:kmaxo_));        VOF8iny=0.0_WP

     ! Read the velocity values from the input file or inflow file
     ! Read the scalar values from the input file
     call inflowy_read_velocity
     call inflowy_read_scalar 
     call inflowy_read_VOF

     ! Initialize the inflow profile and read the first time step
     call inflowy_file_init

  end if
  call MPI_BARRIER(comm,ierr) ! Make all procs wait here
  
  ! Monitor freefall velocity
  if (use_freefall) then
     call monitor_create_file_step('inflow',2)
     call monitor_set_header(1,'Drop Velocity','r')
     call monitor_set_header(2,'Inflow Velocity','r')
  end if

  ! Random velocity fluctuations
  call parser_read('Random fluctuations',rand_flucts,0.0_WP)
  if (abs(rand_flucts).gt.epsilon(0.0_WP)) use_randfluct=.true.
  
  
  return
end subroutine inflow_init
 

! ======================================== !
! Compute the Inflow for the new time step !
! -> velocity components                   !
! -> momentum components                   !
! ======================================== !
subroutine inflow_velocity
  use inflow
  use velocity
  use data
  implicit none

  integer :: i,j,k,nflow
  real(WP) :: rndu,rndv,rndw
  
  ! Precompute the inflow profile from a recycling plane
  call inflow_recycle_velocity

  ! Precompute the inflow profile 
  call inflow_freefall_velocity
  
  ! Inflow x
  ! ======================================
  ! Nothing to do if:
  ! -> not the first proc in x
  ! -> periodic in x
  if (iproc.eq.1 .and. xper.ne.1) then

     ! Precompute the inflow profile for a tanh profile
     call inflow_tanh_velocity

     ! Precompute the inflow profile from a file
     call inflow_file_velocity
     
     ! Apply random fluctuations
     if (use_randfluct) then
        do k=kmin_,kmax_
           do nflow=1,ninlet
              do j=max(jmin_,inlet(nflow)%jmino),min(jmax_,inlet(nflow)%jmaxo)
                 ! Get random numbers
                 call random_number(rndu)
                 call random_number(rndv)
                 call random_number(rndw)
                 ! Add fluctuations to velocities
                 Uin(j,k)=Uin(j,k)+rand_flucts*(2.0_WP*rndu-1.0_WP)
                 Vin(j,k)=Vin(j,k)+rand_flucts*(2.0_WP*rndv-1.0_WP)
                 Win(j,k)=Win(j,k)+rand_flucts*(2.0_WP*rndw-1.0_WP)
              end do
           end do
        end do
     end if

     ! Different types of inflows
     do nflow=1,ninlet
        do j=max(jmino_,inlet(nflow)%jmino),min(jmaxo_,inlet(nflow)%jmaxo)
           ! All left ghost cells
           do i=imino,imin-1
              U(i,j,:)    = Uin(j,:)
              rhoU(i,j,:) = Uin(j,:) * RHOmid(imino,j,:)
              ! Linear interpolation to set V and W at x(imin)
              V(i,j,:)    = ( (xm(imin)-xm(i))*Vin(j,:) - (x(imin)-xm(i))*V(imin,j,:) )/(xm(imin)-x(imin))
              rhoV(i,j,:) = V(i,j,:) * RHOmid(imino,j,:)
              W(i,j,:)    = ( (xm(imin)-xm(i))*Win(j,:) - (x(imin)-xm(i))*W(imin,j,:) )/(xm(imin)-x(imin))
              rhoW(i,j,:) = W(i,j,:) * RHOmid(imino,j,:)
           end do
           ! Last grid point only for U/rhoU
           U(imin,j,:) = Uin(j,:)
           rhoU(imin,j,:) = Uin(j,:) * RHOmid(imino,j,:)
        end do
     end do
  end if

  ! Inflow y
  ! ======================================
  ! Nothing to do if:
  ! -> not the first proc in y
  ! -> periodic in y
  if (jproc.eq.1 .and. yper.ne.1) then

     ! Precompute the inflow profile from a file 
     call inflowy_file_velocity

     ! Different types of inflows
     do nflow=1,ninlety
        do i=max(imino_,inlety(nflow)%imino),min(imaxo_,inlety(nflow)%imaxo)
           ! All bottom ghost cells
           do j=jmino,jmin-1
              V(i,j,:)    = Viny(i,:)
              rhoV(i,j,:) = Viny(i,:) * RHOmid(i,jmino,:)
              ! Linear interpolation to set U and W at y(jmin)
              U(i,j,:)    = ( (ym(jmin)-ym(j))*Uiny(i,:) - (y(jmin)-ym(j))*U(i,jmin,:) )/(ym(jmin)-y(jmin))
              rhoU(i,j,:) = Uiny(i,:) * RHOmid(i,jmino,:)
              W(i,j,:)    = ( (ym(jmin)-ym(j))*Winy(i,:) - (y(jmin)-ym(j))*W(i,jmin,:) )/(ym(jmin)-y(jmin))
              rhoW(i,j,:) = Winy(i,:) * RHOmid(i,jmino,:)
           end do
           ! Last grid point only for V/rhoV
           V(i,jmin,:) = Viny(i,:)
           rhoV(i,jmin,:) = Viny(i,:) * RHOmid(i,jmino,:)
        end do
     end do
  end if

  return
end subroutine inflow_velocity


! ======================================== !
! Compute the Inflow for the new time step !
! -> scalars                               !
! ======================================== !
subroutine inflow_scalar
  use inflow
  use velocity
  use data
  implicit none

  integer :: i,j,nflow,isc

  ! Nothing to do if:
  ! -> not the first proc in x
  ! -> periodic in x
  if (iproc.ne.1 .or. xper.eq.1) then
     ! No x inflow
  else
     ! Precompute the inflow profile from a file
     call inflow_file_scalar

     ! Different types of inflows
     do nflow=1,ninlet
        do isc=1,nscalar
           do j=max(jmino_,inlet(nflow)%jmino),min(jmaxo_,inlet(nflow)%jmaxo)
              do i=imino,imin-1
                 SC(i,j,:,isc) = SCin(j,:,isc)
              end do
           end do
        end do
     end do
  end if

  ! Nothing to do if:
  ! -> not the first proc in y
  ! -> periodic in y
  if (jproc.ne.1 .or. yper.eq.1) then
     ! No y inflow
  else
     ! Precompute the inflow profile from a file
     call inflowy_file_scalar
     
     ! Different types of inflows
     do nflow=1,ninlety
        do isc=1,nscalar
           do i=max(imino_,inlety(nflow)%imino),min(imaxo_,inlety(nflow)%imaxo)
              do j=jmino,jmin-1
                 SC(i,jmin,:,isc) = SCiny(i,:,isc)
              end do
           end do
        end do
     end do
  end if
  
  return
end subroutine inflow_scalar

