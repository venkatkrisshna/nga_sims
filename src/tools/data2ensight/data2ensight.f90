program data2ensight
  use precision
  use string
  use fileio
  implicit none

  ! Data array with all the variables (velocity,pressure,...)
  integer :: nx,ny,nz,nvar
  character(len=str_short), dimension(:), allocatable :: names
  real(WP), dimension(:,:,:), allocatable :: data8
  real(SP), dimension(:,:,:), allocatable :: data4
  integer :: iunit,iunit2,ierr,ibuffer,var
  character(len=str_medium) :: filename1,filename2,directory
  real(WP) :: dt,time
  character(len=80) :: buffer
  
  ! Read file name from standard input
  print*,'========================================='
  print*,'| ARTS - data to ENSIGHT GOLD converter |'
  print*,'========================================='
  print*
  print "(a13,$)", " data file : "
  read "(a)", filename1
  print "(a21,$)", " ensight directory : "
  read "(a)", directory
  
  !call CREATE_FOLDER(trim(directory))
  call system("mkdir -p "//trim(directory))
  
  ! ** Open the data file to read **
  call BINARY_FILE_OPEN(iunit,trim(filename1),"r",ierr)
  
  ! Read sizes
  call BINARY_FILE_READ(iunit,nx,1,kind(nx),ierr)
  call BINARY_FILE_READ(iunit,ny,1,kind(ny),ierr)
  call BINARY_FILE_READ(iunit,nz,1,kind(nz),ierr)
  call BINARY_FILE_READ(iunit,nvar,1,kind(nvar),ierr)
  print*,'Grid :',nx,'x',ny,'x',nz
  
  ! Read additional stuff
  call BINARY_FILE_READ(iunit,dt,1,kind(dt),ierr)
  call BINARY_FILE_READ(iunit,time,1,kind(time),ierr)
  print*,'Data file at time :',time
  
  ! Read variable names
  allocate(names(nvar))
  do var=1,nvar
     call BINARY_FILE_READ(iunit,names(var),str_short,kind(names),ierr)
  end do
  print*,'Variables : ',names
  
  ! Allocate read and write buffers
  allocate(data8(nx,ny,nz))
  allocate(data4(nx,ny,nz))
  
  ! For each variable, read it then write the ensight data file
  do var=1,nvar
     ! Read the variable
     print*,'Doing ',names(var)
     call BINARY_FILE_READ(iunit,data8,nx*ny*nz,kind(data8),ierr)
     ! Convert the data to single precision
     data4=data8
     ! Write Ensight scalar file
     filename2 = trim(directory) // '/' // trim(names(var))
     call BINARY_FILE_OPEN(iunit2,trim(filename2),"w",ierr)
     buffer = trim(names(var))
     call BINARY_FILE_WRITE(iunit2,buffer,80,kind(buffer),ierr)
     buffer = 'part'
     call BINARY_FILE_WRITE(iunit2,buffer,80,kind(buffer),ierr)
     ibuffer = 1
     call BINARY_FILE_WRITE(iunit2,ibuffer,1,kind(ibuffer),ierr)
     buffer = 'block'
     call BINARY_FILE_WRITE(iunit2,buffer,80,kind(buffer),ierr)
     call BINARY_FILE_WRITE(iunit2,data4,nx*ny*nz,kind(data4),ierr)
     call BINARY_FILE_CLOSE(iunit2,ierr)
  end do
  call BINARY_FILE_CLOSE(iunit,ierr)
  

  ! ** Write the case file **
  iunit = iopen()
  filename2 = trim(directory) // '/arts.case'
  open(iunit,file=trim(filename2),form="formatted",iostat=ierr,status="REPLACE")
  ! Write the case
  write(iunit,'(a)') 'FORMAT'
  write(iunit,'(a)') 'type: ensight gold'
  write(iunit,'(a)') 'GEOMETRY'
  write(iunit,'(a)') 'model: 1 1 geometry'

  write(iunit,'(a)') 'VARIABLE'
  do var=1,nvar
     write(iunit,'(4a)') 'scalar per element: ', trim(names(var)),' ',trim(names(var))
  end do
  
  write(iunit,'(a)') 'TIME'
  write(iunit,'(a)') 'time set: 1'
  write(iunit,'(a)') 'number of steps: 1'
  write(iunit,'(a)') 'filename start number: 1'
  write(iunit,'(a)') 'filename increment: 1'
  write(iunit,'(a,ES12.5)') 'time values:',time
  ! Close the file
  close(iclose(iunit))
  
end program data2ensight
