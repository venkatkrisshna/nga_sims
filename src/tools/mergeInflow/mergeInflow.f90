program mergeInflow
  use precision
  use string
  use fileio
  implicit none

  integer :: iunit_in1,iunit_in2,iunit_out
  integer :: ierr,var,var2,n
  integer :: ntime,n1,n2,icyl,n1_in1,n2_in1,n1_in2,n2_in2
  integer :: n2read_in1,n2read_in2,ntime_in1,ntime_in2,nvar_in1,nvar_in2
  character(len=str_short), dimension(:), allocatable :: names_in1,names_in2,names2read_in1,names2read_in2
  real(WP), dimension(:,:,:), allocatable :: data_in1,data_in2
  real(WP), dimension(:), allocatable :: x1, x2
  character(len=str_medium) :: filein1,filein2,fileout
  real(WP) :: dt_in1,time_in1
  real(WP) :: dt_in2,time_in2

  ! Read file name from standard input or command line
  print*,'========================'
  print*,'| ARTS - Inflow Merger |'
  print*,'========================'
  print*
  print *, " Input inflow file 1 : "
  read "(a)", filein1
  print *, " Input inflow file 2 : "
  read "(a)", filein2
  print *, " Output inflow file : "
  read "(a)", fileout
    
  ! Open input inflow 1 and read header
  call BINARY_FILE_OPEN(iunit_in1,trim(filein1),"r",ierr)
  call BINARY_FILE_READ(iunit_in1,ntime_in1,1,kind(ntime_in1),ierr)
  call BINARY_FILE_READ(iunit_in1,n1_in1,1,kind(n1_in1),ierr)
  call BINARY_FILE_READ(iunit_in1,n2_in1,1,kind(n2_in1),ierr)
  call BINARY_FILE_READ(iunit_in1,nvar_in1,1,kind(nvar_in1),ierr)
  print*,'Inflow 1 ---'
  print *,' ntime:',ntime_in1,' - grid :',n1_in1,'x',n2_in1
  call BINARY_FILE_READ(iunit_in1,dt_in1,1,kind(dt_in1),ierr)
  call BINARY_FILE_READ(iunit_in1,time_in1,1,kind(time_in1),ierr)
  allocate(names_in1(nvar_in1))
  do var=1,nvar_in1
     call BINARY_FILE_READ(iunit_in1,names_in1(var),str_short,kind(names_in1),ierr)
  end do
  print*,' Variables: ',names_in1

  ! Open input inflow 2 and read header
  call BINARY_FILE_OPEN(iunit_in2,trim(filein2),"r",ierr)
  call BINARY_FILE_READ(iunit_in2,ntime_in2,1,kind(ntime_in2),ierr)
  call BINARY_FILE_READ(iunit_in2,n1_in2,1,kind(n1_in2),ierr)
  call BINARY_FILE_READ(iunit_in2,n2_in2,1,kind(n2_in2),ierr)
  call BINARY_FILE_READ(iunit_in2,nvar_in2,1,kind(nvar_in2),ierr)
  print*,'Inflow 2 ---'
  print *,' ntime:',ntime_in2,' - grid :',n1_in2,'x',n2_in2
  call BINARY_FILE_READ(iunit_in2,dt_in2,1,kind(dt_in2),ierr)
  call BINARY_FILE_READ(iunit_in2,time_in2,1,kind(time_in2),ierr)
  allocate(names_in2(nvar_in2))
  do var=1,nvar_in2
     call BINARY_FILE_READ(iunit_in2,names_in2(var),str_short,kind(names_in2),ierr)
  end do
  print*,' Variables: ',names_in2

  ! Check if inflows are the same size
  if (n1_in1.ne.n1_in2 .or. n2_in1.ne.n2_in2) &
       stop "Input inflow grids must be the same size"
  n1=n1_in1; n2=n2_in2

  ! Allocate arrays
  allocate(x1(n1+1))
  allocate(x2(n2+1))
  allocate(data_in1(nvar_in1,n1,n2))
  allocate(data_in2(nvar_in2,n1,n2))

  ! Read grids (assuming they are identical)
  call BINARY_FILE_READ(iunit_in1,icyl,1,kind(icyl),ierr)
  call BINARY_FILE_READ(iunit_in1,x1,n1+1,kind(x1),ierr)
  call BINARY_FILE_READ(iunit_in1,x2,n2+1,kind(x2),ierr)
  call BINARY_FILE_READ(iunit_in2,icyl,1,kind(icyl),ierr)
  call BINARY_FILE_READ(iunit_in2,x1,n1+1,kind(x1),ierr)
  call BINARY_FILE_READ(iunit_in2,x2,n2+1,kind(x2),ierr)

  ! ** Ask what to do **
  print*
  print*, "Enter number of variables to be read from inflow 1"
  read "(i1)", n2read_in1
  allocate(names2read_in1(n2read_in1))
  do n=1,n2read_in1
     print *,'Enter variable ',n
     read "(a)", names2read_in1(n)
  end do
  print*
  print*, "Enter number of variables to be read from inflow 2"
  read "(i1)", n2read_in2
  allocate(names2read_in2(n2read_in2))
  do n=1,n2read_in2
     print *,'Enter variable ',n
     read "(a)", names2read_in2(n)
  end do

  ! Open output inflow file & write header
  call BINARY_FILE_OPEN(iunit_out,trim(fileout),"w",ierr)
  call BINARY_FILE_WRITE(iunit_out,max(ntime_in1,ntime_in2),1,kind(ntime_in2),ierr)
  call BINARY_FILE_WRITE(iunit_out,n1,1,kind(n1),ierr)
  call BINARY_FILE_WRITE(iunit_out,n2,1,kind(n2),ierr)
  call BINARY_FILE_WRITE(iunit_out,n2read_in1+n2read_in2,1,kind(n2read_in1),ierr)
  call BINARY_FILE_WRITE(iunit_out,min(dt_in1,dt_in2),1,kind(dt_in1),ierr)
  call BINARY_FILE_WRITE(iunit_out,min(time_in1,time_in2),1,kind(time_in1),ierr)
  ! Write variable names
  do var=1,nvar_in1
     do var2=1,n2read_in1
        if (trim(names_in1(var))==trim(names2read_in1(var2))) then
           call BINARY_FILE_WRITE(iunit_out,names_in1(var),str_short,kind(names2read_in1),ierr)
        end if
     end do
  end do
  do var=1,nvar_in2
     do var2=1,n2read_in2
        if (trim(names_in2(var))==trim(names2read_in2(var2))) then
           call BINARY_FILE_WRITE(iunit_out,names_in2(var),str_short,kind(names2read_in1),ierr)
        end if
     end do
  end do
  ! Write mesh
  call BINARY_FILE_WRITE(iunit_out,icyl,1,kind(icyl),ierr)     
  call BINARY_FILE_WRITE(iunit_out,x1,n1+1,kind(x1),ierr)
  call BINARY_FILE_WRITE(iunit_out,x2,n2+1,kind(x2),ierr)

  ! Write data
  ntime=max(ntime_in1,ntime_in2)
  do n=1,ntime
     print *,'Working on ',n,' out of ',ntime
     
     ! Write variables in inflow 1
     do var=1,nvar_in1
        ! Read data (or use previously read data)
        if (n.le.ntime_in1) &
             call BINARY_FILE_READ (iunit_in1,data_in1(var,:,:),n1*n2,kind(data_in1),ierr)
        ! Write if requested
        do var2=1,n2read_in1
           if (trim(names_in1(var))==trim(names2read_in1(var2))) then
              call BINARY_FILE_WRITE(iunit_out,data_in1(var,:,:),n1*n2,kind(data_in1),ierr)
           end if
        end do
        
     end do
     ! Write variables in inflow 2
     do var=1,nvar_in2
        ! Read data (or use previously read data)
        if (n.le.ntime_in2) &
             call BINARY_FILE_READ (iunit_in2,data_in2(var,:,:),n1*n2,kind(data_in2),ierr)
        ! Write if requested
        do var2=1,n2read_in2
           if (trim(names_in2(var))==trim(names2read_in2(var2))) then
              call BINARY_FILE_WRITE(iunit_out,data_in2(var,:,:),n1*n2,kind(data_in2),ierr)
           end if
        end do
     end do
  end do
  call BINARY_FILE_CLOSE(iunit_in1,ierr)
  call BINARY_FILE_CLOSE(iunit_in2,ierr)
  call BINARY_FILE_CLOSE(iunit_out,ierr)
     
end program mergeInflow
