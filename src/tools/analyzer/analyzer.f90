module analyzer
  use string
  use precision
  implicit none
  
  ! Sizes
  integer :: nx,ny,nz,np
  integer :: xper,yper,zper
  integer :: icyl
  integer :: nvar
  real(WP) :: dt,time
  character(len=str_short), dimension(:), allocatable :: names
  character(len=str_medium) :: config
  integer, dimension(:,:), allocatable :: mask
  
  ! Data
  real(WP), dimension(:,:,:), allocatable :: data
  
  ! Mesh
  real(WP), dimension(:), pointer :: x,xm
  real(WP), dimension(:), pointer :: y,ym
  real(WP), dimension(:), pointer :: z,zm
  
  ! Data
  !real(WP), dimension(:,:,:), allocatable :: Y_C7H16,T,Sm,ND,Wchem,ZMIX,CHI
  
  ! File number
  !character(len=str_medium) :: numb
  
  ! Data
  real(SP), dimension(:,:,:), allocatable :: U,V,W,E,ZMIX
  real(SP), dimension(:), allocatable :: buf1,buf2,buf3
  real(SP), dimension(:,:), allocatable :: pos,vel
  
  ! Mapping
  integer, dimension(:,:), allocatable :: map
  integer, dimension(:,:), allocatable :: connect
  
end module analyzer

subroutine fb_analyze
  use analyzer
  implicit none
  
  integer :: n,i,j,k,iunit,ierr,ip,jp,kp
  character(len=str_medium) :: file
  real(SP) :: norm
  real(SP), dimension(256) :: xout,xmout
  real(SP), dimension(256) ::  Umean, Vmean, Wmean
  real(SP), dimension(256) :: U2mean,V2mean,W2mean
  real(SP), dimension(256) :: UVmean,UWmean,VWmean
  real(SP), dimension(256) ::  Emean,E2mean,EUmean
  real(SP), dimension(256) :: Ugmean,Upmean,countg,countp
  real(SP), dimension(256) :: um,vm,wm,cp
  real(SP), dimension(256) :: u2m,v2m,w2m
  real(SP), dimension(256) :: upm,ugm,cpp,cpg
  
  ! Read geometry
  call fb_geometry
  
  ! Prepare buffers
  allocate(buf1(nx*ny*nz))
  allocate(buf2(nx*ny*nz))
  allocate(buf3(nx*ny*nz))
  allocate(U(nx,ny,nz))
  allocate(V(nx,ny,nz))
  allocate(W(nx,ny,nz))
  allocate(E(nx,ny,nz))
  allocate(pos(np,3))
  allocate(vel(np,3))
  
  ! Zero arrays
  Umean=0.0_SP;Vmean=0.0_SP;Wmean=0.0_SP
  U2mean=0.0_SP;V2mean=0.0_SP;W2mean=0.0_SP
  UVmean=0.0_SP;UWmean=0.0_SP;VWmean=0.0_SP
  Emean=0.0_SP;E2mean=0.0_SP;EUmean=0.0_SP
  Upmean=0.0_SP;Ugmean=0.0_SP;countg=0.0_SP;countp=0.0_SP
  um=0.0_SP;vm=0.0_SP;wm=0.0_SP;cp=0.0_SP;cpp=0.0_SP;cpg=0.0_SP
  u2m=0.0_SP;v2m=0.0_SP;w2m=0.0_SP;upm=0.0_SP;ugm=0.0_SP
  
  ! Loop over files
  do n=1,127
     
     ! Read velocity
     file='cleanEnsight/V/V.'
     write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') n+150
     call fb_read_vector(file,U,V,W)
     
     ! Read gas fraction
     file='cleanEnsight/EPSFRHO/EPSFRHO.'
     write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') n+150
     call fb_read_scalar(file,E)
     
     ! Read particles
     file='cleanEnsight/particles/particles.'
     write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') n+150
     call fb_read_particle(file)
     file='cleanEnsight/particles/velocity.'
     write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') n+150
     call fb_read_particle_vel(file)

     ! Compute statistics
     do i=1,nx
        Umean(i)=Umean(i)+sum(U(i,:,:))
        Vmean(i)=Vmean(i)+sum(V(i,:,:))
        Wmean(i)=Wmean(i)+sum(W(i,:,:))
        U2mean(i)=U2mean(i)+sum(U(i,:,:)**2)
        V2mean(i)=V2mean(i)+sum(V(i,:,:)**2)
        W2mean(i)=W2mean(i)+sum(W(i,:,:)**2)
        UVmean(i)=UVmean(i)+sum(U(i,:,:)*V(i,:,:))
        UWmean(i)=UWmean(i)+sum(U(i,:,:)*W(i,:,:))
        VWmean(i)=VWmean(i)+sum(V(i,:,:)*W(i,:,:))
        Emean(i)=Emean(i)+sum(E(i,:,:))
        E2mean(i)=E2mean(i)+sum(E(i,:,:)**2)
        EUmean(i)=EUmean(i)+sum(E(i,:,:)*U(i,:,:))
     end do
     
     ! Conditional statistics
     do i=1,nx
        do j=1,ny
           do k=1,nz
              if (E(i,j,k).gt.0.8_SP) then
                 Ugmean(i)=Ugmean(i)+U(i,j,k)
                 countg(i)=countg(i)+1.0_SP
              else
                 Upmean(i)=Upmean(i)+U(i,j,k)
                 countp(i)=countp(i)+1.0_SP
              end if
           end do
        end do
     end do
     
     ! Particle statistics
     do i=1,np
        ip=min(max(floor(pos(i,1)*256.0_SP/2.0_SP)+1,1),256)
        jp=min(max(floor(pos(i,2)*128.0_SP/1.0_SP)+1,1),128)
        kp=min(max(floor(pos(i,3)*128.0_SP/1.0_SP)+1,1),128)
        um(ip)=um(ip)+vel(i,1)
        vm(ip)=vm(ip)+vel(i,2)
        wm(ip)=wm(ip)+vel(i,3)
        u2m(ip)=u2m(ip)+vel(i,1)**2
        v2m(ip)=v2m(ip)+vel(i,2)**2
        w2m(ip)=w2m(ip)+vel(i,3)**2
        cp(ip)=cp(ip)+1.0_SP
        if (E(ip,jp,kp).gt.0.8_SP) then
           ugm(ip)=ugm(ip)+vel(i,1)
           cpg(ip)=cpg(ip)+1.0_SP
        else
           upm(ip)=upm(ip)+vel(i,1)
           cpp(ip)=cpp(ip)+1.0_SP
        end if
     end do
     
  end do
  
  ! Normalize
  norm=127.0_SP*128.0_SP*128.0_SP
  Umean=Umean/norm*1.0_SP
  Vmean=Vmean/norm*1.0_SP
  Wmean=Wmean/norm*1.0_SP
  U2mean=U2mean/norm*1.0_SP*1.0_SP
  V2mean=V2mean/norm*1.0_SP*1.0_SP
  W2mean=W2mean/norm*1.0_SP*1.0_SP
  UVmean=UVmean/norm*1.0_SP*1.0_SP
  UWmean=UWmean/norm*1.0_SP*1.0_SP
  VWmean=VWmean/norm*1.0_SP*1.0_SP
  Emean=Emean/norm*1.00_SP
  E2mean=E2mean/norm*1.00_SP*1.00_SP
  EUmean=EUmean/norm*1.00_SP*1.0_SP
  Ugmean=Ugmean/(countg+epsilon(1.0_SP))*1.0_SP
  Upmean=Upmean/(countp+epsilon(1.0_SP))*1.0_SP
  um=um/(cp+epsilon(1.0_SP))
  vm=vm/(cp+epsilon(1.0_SP))
  wm=wm/(cp+epsilon(1.0_SP))
  u2m=u2m/(cp+epsilon(1.0_SP))
  v2m=v2m/(cp+epsilon(1.0_SP))
  w2m=w2m/(cp+epsilon(1.0_SP))
  upm=upm/(cpp+epsilon(1.0_SP))
  ugm=ugm/(cpg+epsilon(1.0_SP))
  
  ! Prepare xout
  do n=1,nx
     xmout(n)=(real(n,SP)-0.5_SP)/128.0_SP
     xout(n) =(real(n,SP)-1.0_SP)/128.0_SP
  end do
  
  ! Print to file
  iunit=25
  open(iunit,file='Umean',form="formatted",iostat=ierr)
  do n=1,256
     write(iunit,'(10000ES20.12)') xout(n),xmout(n),Umean(n),Vmean(n),Wmean(n),U2mean(n)-Umean(n)**2,V2mean(n)-Vmean(n)**2,W2mean(n)-Wmean(n)**2,UVmean(n)-(Umean(n)*Vmean(n)),UWmean(n)-(Umean(n)*Wmean(n)),VWmean(n)-(Vmean(n)*Wmean(n)),Emean(n),E2mean(n)-Emean(n)**2,EUmean(n)-(Emean(n)*Umean(n)),Ugmean(n),Upmean(n),um(n),vm(n),wm(n),u2m(n)-um(n)**2,v2m(n)-vm(n)**2,w2m(n)-wm(n)**2,ugm(n),upm(n)
  end do
  
  return
end subroutine fb_analyze


subroutine fb_geometry
  use analyzer
  implicit none
  
  character(len=str_medium) :: file
  integer  :: unit,ierr,ibuf,n
  real(SP) :: rbuf
  character(len=80) :: cbuf
  
  ! Sizes
  nx=256
  ny=128
  nz=128
  np=11904336
  
  ! Allocate x-y-z-connect-map
  allocate(x((nx+1)*(ny+1)*(nz+1)))
  allocate(y((nx+1)*(ny+1)*(nz+1)))
  allocate(z((nx+1)*(ny+1)*(nz+1)))
  allocate(connect(8,nx*ny*nz))
  allocate(map(nx*ny*nz,3))
  
  ! Open file
  file='cleanEnsight/geometry'
  call BINARY_FILE_OPEN(unit,trim(file),'r',ierr)
  
  ! Read crap
  call BINARY_FILE_READ(unit,cbuf,80,kind(cbuf),ierr)
  call BINARY_FILE_READ(unit,cbuf,80,kind(cbuf),ierr)
  call BINARY_FILE_READ(unit,cbuf,80,kind(cbuf),ierr)
  call BINARY_FILE_READ(unit,cbuf,80,kind(cbuf),ierr)
  call BINARY_FILE_READ(unit,cbuf,80,kind(cbuf),ierr)
  call BINARY_FILE_READ(unit,cbuf,80,kind(cbuf),ierr)
  call BINARY_FILE_READ(unit,rbuf, 1,kind(rbuf),ierr)
  call BINARY_FILE_READ(unit,rbuf, 1,kind(rbuf),ierr)
  call BINARY_FILE_READ(unit,rbuf, 1,kind(rbuf),ierr)
  call BINARY_FILE_READ(unit,rbuf, 1,kind(rbuf),ierr)
  call BINARY_FILE_READ(unit,rbuf, 1,kind(rbuf),ierr)
  call BINARY_FILE_READ(unit,rbuf, 1,kind(rbuf),ierr)
  call BINARY_FILE_READ(unit,cbuf,80,kind(cbuf),ierr)
  call BINARY_FILE_READ(unit,ibuf, 1,kind(ibuf),ierr)
  call BINARY_FILE_READ(unit,cbuf,80,kind(cbuf),ierr)
  call BINARY_FILE_READ(unit,cbuf,80,kind(cbuf),ierr)
  call BINARY_FILE_READ(unit,ibuf, 1,kind(ibuf),ierr)
  
  ! Read node position
  call BINARY_FILE_READ(unit,x,(nx+1)*(ny+1)*(nz+1),kind(x),ierr)
  call BINARY_FILE_READ(unit,x,(nx+1)*(ny+1)*(nz+1),kind(x),ierr)
  call BINARY_FILE_READ(unit,y,(nx+1)*(ny+1)*(nz+1),kind(y),ierr)
  call BINARY_FILE_READ(unit,z,(nx+1)*(ny+1)*(nz+1),kind(z),ierr)
  
  ! Read connectivity
  call BINARY_FILE_READ(unit,cbuf,80,kind(cbuf),ierr)
  call BINARY_FILE_READ(unit,ibuf, 1,kind(ibuf),ierr)
  call BINARY_FILE_READ(unit,connect(:,1),nx*ny*nz,kind(ibuf),ierr)
  call BINARY_FILE_READ(unit,connect,8*nx*ny*nz,kind(ibuf),ierr)
  y=y+0.5_SP
  z=z+0.5_SP
  ! Close file
  call BINARY_FILE_CLOSE(unit,ierr)
  
  ! Create mapping
  do n=1,nx*ny*nz
     map(n,1)=int(128.0_SP*maxval(x(connect(:,n))))
     map(n,2)=int(128.0_SP*maxval(y(connect(:,n))))
     map(n,3)=int(128.0_SP*maxval(z(connect(:,n))))
  end do
  
  ! Deallocate
  deallocate(x,y,z,connect)
  
  return
end subroutine fb_geometry


subroutine fb_read_particle(file)
  use analyzer
  implicit none
  
  integer :: unit,ierr,ibuf,i
  character(len=str_medium) :: file
  character(len=80) :: cbuf
  
  print*,'Reading ',trim(file),' ...'
  
  ! Open file
  call BINARY_FILE_OPEN(unit,trim(file),'r',ierr)
  
  ! Read crap
  call BINARY_FILE_READ(unit,cbuf,80,kind(cbuf),ierr)
  call BINARY_FILE_READ(unit,cbuf,80,kind(cbuf),ierr)
  call BINARY_FILE_READ(unit,cbuf,80,kind(cbuf),ierr)
  call BINARY_FILE_READ(unit,ibuf, 1,kind(ibuf),ierr)
  call BINARY_FILE_READ(unit,pos(:,1),np,kind(ibuf),ierr)
  
  ! Read position
  do i=1,np
     call BINARY_FILE_READ(unit,pos(i,1),1,kind(pos),ierr)
     call BINARY_FILE_READ(unit,pos(i,2),1,kind(pos),ierr)
     call BINARY_FILE_READ(unit,pos(i,3),1,kind(pos),ierr)
     pos(i,2:3)=pos(i,2:3)+0.5_SP
  end do
  
  ! Close file
  call BINARY_FILE_CLOSE(unit,ierr)
  
  return
end subroutine fb_read_particle


subroutine fb_read_particle_vel(file)
  use analyzer
  implicit none
  
  integer :: unit,ierr,ibuf,i
  character(len=str_medium) :: file
  character(len=80) :: cbuf
  
  print*,'Reading ',trim(file),' ...'
  
  ! Open file
  call BINARY_FILE_OPEN(unit,trim(file),'r',ierr)
  
  ! Read crap
  call BINARY_FILE_READ(unit,cbuf,80,kind(cbuf),ierr)
  
  ! Read velocity
  do i=1,np
     call BINARY_FILE_READ(unit,vel(i,1),1,kind(vel),ierr)
     call BINARY_FILE_READ(unit,vel(i,2),1,kind(vel),ierr)
     call BINARY_FILE_READ(unit,vel(i,3),1,kind(vel),ierr)
  end do
  
  ! Close file
  call BINARY_FILE_CLOSE(unit,ierr)
  
  return
end subroutine fb_read_particle_vel


subroutine fb_read_vector(file,A1,A2,A3)
  use analyzer
  implicit none
  
  integer :: unit,ierr,ibuf
  character(len=str_medium) :: file
  character(len=80) :: cbuf
  real(SP), dimension(1:nx,1:ny,1:nz) :: A1,A2,A3
  
  print*,'Reading ',trim(file),' ...'
  
  ! Open file
  call BINARY_FILE_OPEN(unit,trim(file),'r',ierr)
  
  ! Read crap
  call BINARY_FILE_READ(unit,cbuf,80,kind(cbuf),ierr)
  call BINARY_FILE_READ(unit,cbuf,80,kind(cbuf),ierr)
  call BINARY_FILE_READ(unit,ibuf, 1,kind(ibuf),ierr)
  call BINARY_FILE_READ(unit,cbuf,80,kind(cbuf),ierr)
  
  ! Read vector data
  call BINARY_FILE_READ(unit,buf1,nx*ny*nz,kind(buf1),ierr)
  call fb_sort(buf1,A1)
  call BINARY_FILE_READ(unit,buf2,nx*ny*nz,kind(buf1),ierr)
  call fb_sort(buf2,A2)
  call BINARY_FILE_READ(unit,buf3,nx*ny*nz,kind(buf1),ierr)
  call fb_sort(buf3,A3)
  
  ! Close file
  call BINARY_FILE_CLOSE(unit,ierr)
  
  return
end subroutine fb_read_vector


subroutine fb_read_scalar(file,A1)
  use analyzer
  implicit none
  
  integer :: unit,ierr,ibuf
  character(len=str_medium) :: file
  character(len=80) :: cbuf
  real(SP), dimension(1:nx,1:ny,1:nz) :: A1
  
  print*,'Reading ',trim(file),' ...'
  
  ! Open file
  call BINARY_FILE_OPEN(unit,trim(file),'r',ierr)
  
  ! Read crap
  call BINARY_FILE_READ(unit,cbuf,80,kind(cbuf),ierr)
  call BINARY_FILE_READ(unit,cbuf,80,kind(cbuf),ierr)
  call BINARY_FILE_READ(unit,ibuf, 1,kind(ibuf),ierr)
  call BINARY_FILE_READ(unit,cbuf,80,kind(cbuf),ierr)
  
  ! Read vector data
  call BINARY_FILE_READ(unit,buf1,nx*ny*nz,kind(buf1),ierr)
  call fb_sort(buf1,A1)
  
  ! Close file
  call BINARY_FILE_CLOSE(unit,ierr)
  
  return
end subroutine fb_read_scalar


subroutine fb_sort(bufin,bufout)
  use analyzer
  implicit none
  
  real(SP), dimension(1:nx*ny*nz)     :: bufin
  real(SP), dimension(1:nx,1:ny,1:nz) :: bufout
  integer :: n
  
  do n=1,nx*ny*nz
     bufout(map(n,1),map(n,2),map(n,3))=bufin(n)
  end do
  
  return
end subroutine fb_sort


program analyzer_prog
  implicit none
  
  !call fb_analyze
  !call read_input
  call ml_analyze
  
end program analyzer_prog


subroutine ml_analyze
  use analyzer
  use fileio
  implicit none
  
  ! I/O
  character(len=str_medium) :: fconfig,fdata
  integer :: iunit,statfile
  integer :: i,j,k,var,ierr
  
  ! Mixing layer stats
  real(WP) :: disp_thick,mom_thick,vort_thick,lambda,nu
  real(WP) :: Re_mom,Re_vort,Re_lambda,U1,U2,tmp
  real(WP), dimension(:), allocatable :: Umean ,Vmean ,Wmean
  real(WP), dimension(:), allocatable :: Umean2,Vmean2,Wmean2
  real(WP) :: up2,dupdx2,uprime1,uprime2,max_der,tke
  
  ! Read in viscosity
  print "(a27,$)", " Kinematic viscosity : "
  read "(a)", nu
  
  ! Read the source config file
  print "(a27,$)", " Config file : "
  read "(a)", fconfig
  call BINARY_FILE_OPEN(iunit,trim(fconfig),"r",ierr)
  call BINARY_FILE_READ(iunit,config,str_medium,kind(config),ierr)
  call BINARY_FILE_READ(iunit,icyl,1,kind(icyl),ierr)
  call BINARY_FILE_READ(iunit,xper,1,kind(xper),ierr)
  call BINARY_FILE_READ(iunit,yper,1,kind(yper),ierr)
  call BINARY_FILE_READ(iunit,zper,1,kind(zper),ierr)
  call BINARY_FILE_READ(iunit,nx,1,kind(nx),ierr)
  call BINARY_FILE_READ(iunit,ny,1,kind(ny),ierr)
  call BINARY_FILE_READ(iunit,nz,1,kind(nz),ierr)
  print*,'Source file'
  print*,'Config : ',config
  print*,'Grid :',nx,'x',ny,'x',nz
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(mask(nx,ny))
  call BINARY_FILE_READ(iunit,x,nx+1,kind(x),ierr)
  call BINARY_FILE_READ(iunit,y,ny+1,kind(y),ierr)
  call BINARY_FILE_READ(iunit,z,nz+1,kind(z),ierr)
  call BINARY_FILE_READ(iunit,mask,nx*ny,kind(mask),ierr)
  call BINARY_FILE_CLOSE(iunit,ierr)
  allocate(xm(nx),ym(ny),zm(nz))
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Read the data file
  print "(a27,$)", " Data file : "
  read "(a)", fdata
  call BINARY_FILE_OPEN(iunit,trim(fdata),"r",ierr)
  call BINARY_FILE_READ(iunit,nx,1,kind(nx),ierr)
  call BINARY_FILE_READ(iunit,ny,1,kind(ny),ierr)
  call BINARY_FILE_READ(iunit,nz,1,kind(nz),ierr)
  call BINARY_FILE_READ(iunit,nvar,1,kind(nvar),ierr)
  call BINARY_FILE_READ(iunit,dt,1,kind(dt),ierr)
  call BINARY_FILE_READ(iunit,time,1,kind(time),ierr)
  allocate(names(nvar))
  do var=1,nvar
     call BINARY_FILE_READ(iunit,names(var),str_short,kind(names),ierr)
  end do
  print*,'Variables in data file: ',names
  allocate(data(nx,ny,nz))
  allocate(U(nx,ny,nz))
  allocate(V(nx,ny,nz))
  allocate(W(nx,ny,nz))
  allocate(ZMIX(nx,ny,nz))
  do var=1,nvar
     call BINARY_FILE_READ(iunit,data,nx*ny*nz,kind(data),ierr)
     select case (trim(adjustl(names(var))))
     case ('U')
        U=data
     case ('V')
        V=data
     case ('W')
        W=data
     case ('ZMIX')
        ZMIX=data
     end select
  end do
  deallocate(data)
  call BINARY_FILE_CLOSE(iunit,ierr)
  
  ! Generate statistics - mean flow
  allocate(Umean (1:ny)); allocate(Vmean (1:ny)); allocate(Wmean (1:ny))
  allocate(Umean2(1:ny)); allocate(Vmean2(1:ny)); allocate(Wmean2(1:ny))
  do j=1,ny
     Umean(j)  = sum(U(1:nx,j,1:nz))
     Vmean(j)  = sum(V(1:nx,j,1:nz))
     Wmean(j)  = sum(W(1:nx,j,1:nz))
     Umean2(j) = sum(U(1:nx,j,1:nz)**2)
     Vmean2(j) = sum(V(1:nx,j,1:nz)**2)
     Wmean2(j) = sum(W(1:nx,j,1:nz)**2)
  end do
  Umean =Umean /real(nx*nz,WP); Vmean =Vmean /real(nx*nz,WP); Wmean =Wmean /real(nx*nz,WP)
  Umean2=Umean2/real(nx*nz,WP); Vmean2=Vmean2/real(nx*nz,WP); Wmean2=Wmean2/real(nx*nz,WP)
  U1=Umean(ny); U2=Umean(1)
  
  ! Various thicknesses
  disp_thick = 0.0_WP
  mom_thick  = 0.0_WP
  do j=1,ny
     disp_thick = disp_thick + (0.5_WP*(U1-U2)-abs(Umean(j)-0.5_WP*(U1+U2))) * (y(j+1)-y(j))
     mom_thick  = mom_thick  + (U1-Umean(j))*(Umean(j)-U2)*(y(j+1)-y(j))
  end do
  mom_thick  = mom_thick/((U1-U2)**2+epsilon(1.0_WP))
  disp_thick = 2.0_WP*disp_thick/(U1-U2+epsilon(1.0_WP))
  re_mom = mom_thick*(U1-U2)/nu
  
  ! Calculate the vorticity thickness and Re_vort
  max_der = 0.0_WP
  do j=1,ny-1
     tmp = abs(Umean(j+1)-Umean(j))*(ym(j+1)-ym(j))
     if (tmp.gt.max_der) max_der = tmp
  end do
  vort_thick = (U1-U2)/(max_der+epsilon(1.0_WP))
  re_vort = vort_thick*(U1-U2)/nu
  
  ! Calculate the Taylor microscale (lambda) and Re_lambda
  up2    = 0.0_WP
  dupdx2 = 0.0_WP
  j = ny/2
  do k=1,nz
     do i=1,nx-1
        up2 = up2 + ((U(i,j,k)+U(i+1,j,k)-2.0_WP*Umean(j))/2.0_WP)**2
        dupdx2 = dupdx2 + ((U(i+1,j,k)-U(i,j,k))/(x(i+1)-x(i)))**2
     end do
  end do
  lambda = sqrt(up2/(dupdx2+epsilon(1.0_WP)))
  re_lambda = lambda*up2**(0.5)/nu
  
  ! TKE
  tke=0.0_WP
  do k=1,nz
     do j=1,ny
        do i=1,nx
           tke = tke + (U(i,j,k)-Umean(j))**2 + (V(i,j,k)-Vmean(j))**2 + (W(i,j,k)-Wmean(j))**2
        end do
     end do
  end do
  tke=tke/real(nx*ny*nz,WP)
  
  ! Output to file the various profiles
  statfile=iopen()
  open(statfile,file="ml_stat", form="formatted",iostat=ierr,status="REPLACE")
  write(statfile,'(7(a12,x))') 'ym','Umean','Vmean','Wmean','Uvar','Vvar','Wvar'
  do j=1,ny
     write(statfile,'(7(ES12.3,x))') ym(j),Umean(j),Vmean(j),Wmean(j),Umean2(j)-Umean(j)**2,Vmean2(j)-Vmean(j)**2,Wmean2(j)-Wmean(j)**2
  end do
  close(iclose(statfile))
  
  return
end subroutine ml_analyze


!!$subroutine read_input
!!$  use analyzer
!!$  implicit none
!!$  
!!$  character(len=str_medium) :: file_format
!!$  
!!$  ! Ask the user what to do
!!$  print*,'========================'
!!$  print*,'| ARTS - data Analyzer |'
!!$  print*,'========================'
!!$  print*
!!$  print "(a15,$)", " File format : "
!!$  read "(a)", file_format
!!$  select case (trim(file_format))
!!$  case ('FB')
!!$     call fb_read
!!$     call fb_analyze
!!$  case ('ensight')
!!$     call read_ensight_info
!!$     call analyze_it
!!$  case ('vortex')
!!$     call read_vortex_info
!!$  case ('taylor')
!!$     call read_taylor_info
!!$  case ('scalar')
!!$     call read_scalar_info
!!$  case default
!!$     stop "Unknown file format - exiting"
!!$  end select
!!$  
!!$  return
!!$end subroutine read_input
!!$
!!$
!!$subroutine read_vortex_info
!!$  use analyzer
!!$  use parser
!!$  use fileio
!!$  implicit none
!!$  
!!$  real(WP), dimension(:,:,:,:), allocatable :: data1,data2
!!$  real(WP) :: L2errU,L2errV,L2errW
!!$  real(WP) :: LIerrU,LIerrV,LIerrW
!!$  real(WP) :: normU,normV,normW
!!$  integer  :: ierr,var,nvar,i,j,k,iunit
!!$  real(WP) :: dt,time
!!$  character(len=str_short), dimension(:), allocatable :: names  
!!$  
!!$  ! Read the first files
!!$  call BINARY_FILE_OPEN(iunit,'data.init',"r",ierr)
!!$  ! Read sizes
!!$  call BINARY_FILE_READ(iunit,nx,1,kind(nx),ierr)
!!$  call BINARY_FILE_READ(iunit,ny,1,kind(ny),ierr)
!!$  call BINARY_FILE_READ(iunit,nz,1,kind(nz),ierr)
!!$  call BINARY_FILE_READ(iunit,nvar,1,kind(nvar),ierr)
!!$  ! Read additional stuff
!!$  call BINARY_FILE_READ(iunit,dt,1,kind(dt),ierr)
!!$  call BINARY_FILE_READ(iunit,time,1,kind(time),ierr)
!!$  ! Read variable names
!!$  allocate(names(nvar))
!!$  do var=1,nvar
!!$     call BINARY_FILE_READ(iunit,names(var),str_short,kind(names),ierr)
!!$  end do
!!$  ! Read data field
!!$  allocate(data1(nx,ny,nz,nvar))
!!$  do var=1,nvar
!!$     call BINARY_FILE_READ(iunit,data1(:,:,:,var),nx*ny*nz,kind(data1),ierr)
!!$  end do
!!$  call BINARY_FILE_CLOSE(iunit,ierr)
!!$
!!$  ! Read the first files
!!$  call BINARY_FILE_OPEN(iunit,'data',"r",ierr)
!!$  ! Read sizes
!!$  call BINARY_FILE_READ(iunit,nx,1,kind(nx),ierr)
!!$  call BINARY_FILE_READ(iunit,ny,1,kind(ny),ierr)
!!$  call BINARY_FILE_READ(iunit,nz,1,kind(nz),ierr)
!!$  call BINARY_FILE_READ(iunit,nvar,1,kind(nvar),ierr)
!!$  ! Read additional stuff
!!$  call BINARY_FILE_READ(iunit,dt,1,kind(dt),ierr)
!!$  call BINARY_FILE_READ(iunit,time,1,kind(time),ierr)
!!$  ! Read variable names
!!$  allocate(names(nvar))
!!$  do var=1,nvar
!!$     call BINARY_FILE_READ(iunit,names(var),str_short,kind(names),ierr)
!!$  end do
!!$  ! Read data field
!!$  allocate(data2(nx,ny,nz,nvar))
!!$  do var=1,nvar
!!$     call BINARY_FILE_READ(iunit,data2(:,:,:,var),nx*ny*nz,kind(data2),ierr)
!!$  end do
!!$  call BINARY_FILE_CLOSE(iunit,ierr)
!!$  
!!$  ! Compute stuff
!!$  normU = 0.0_WP
!!$  normV = 0.0_WP
!!$  normW = 0.0_WP
!!$  L2errU = 0.0_WP
!!$  L2errV = 0.0_WP
!!$  L2errW = 0.0_WP
!!$  LIerrU = 0.0_WP
!!$  LIerrV = 0.0_WP
!!$  LIerrW = 0.0_WP
!!$  do k=1,nz
!!$     do j=1,ny
!!$        do i=1,nx
!!$           normU=normU+data1(i,j,k,1)**2
!!$           normV=normV+data1(i,j,k,2)**2
!!$           normW=normW+data1(i,j,k,3)**2
!!$           L2errU = L2errU+(data2(i,j,k,1)-data1(i,j,k,1))**2
!!$           L2errV = L2errV+(data2(i,j,k,2)-data1(i,j,k,2))**2
!!$           L2errW = L2errW+(data2(i,j,k,3)-data1(i,j,k,3))**2
!!$           LIerrU = max(LIerrU,abs(data2(i,j,k,1)-data1(i,j,k,1)))
!!$           LIerrV = max(LIerrV,abs(data2(i,j,k,2)-data1(i,j,k,2)))
!!$           LIerrW = max(LIerrW,abs(data2(i,j,k,3)-data1(i,j,k,3)))
!!$        end do
!!$     end do
!!$  end do
!!$  normU=sqrt(normU/(nx*ny*nz))
!!$  normV=sqrt(normV/(nx*ny*nz))
!!$  normW=sqrt(normW/(nx*ny*nz))
!!$  L2errU=sqrt(L2errU/(nx*ny*nz))/(normU+epsilon(normU))
!!$  L2errV=sqrt(L2errV/(nx*ny*nz))/(normV+epsilon(normV))
!!$  L2errW=sqrt(L2errW/(nx*ny*nz))/(normW+epsilon(normW))
!!$  LIerrU=LIerrU/(maxval(abs(data1(:,:,:,1)))+epsilon(1.0_WP))
!!$  LIerrV=LIerrV/(maxval(abs(data1(:,:,:,2)))+epsilon(1.0_WP))
!!$  LIerrW=LIerrW/(maxval(abs(data1(:,:,:,3)))+epsilon(1.0_WP))
!!$  print*,'Mesh size:',nx,ny,nz
!!$  print*,'U error:',L2errU,LIerrU
!!$  print*,'V error:',L2errV,LIerrV
!!$  print*,'W error:',L2errW,LIerrW
!!$  
!!$  return
!!$end subroutine read_vortex_info
!!$
!!$
!!$subroutine read_taylor_info
!!$  use analyzer
!!$  use parser
!!$  use fileio
!!$  implicit none
!!$  
!!$  real(WP), dimension(:,:,:,:), allocatable :: data1,data2
!!$  real(WP) :: num,den,decay
!!$  integer :: ierr,var,nvar,i,j,k,iunit
!!$  real(WP) :: dt,time0,time
!!$  character(len=str_short), dimension(:), allocatable :: names  
!!$  
!!$  ! Read the first files
!!$  call BINARY_FILE_OPEN(iunit,'data.init',"r",ierr)
!!$  ! Read sizes
!!$  call BINARY_FILE_READ(iunit,nx,1,kind(nx),ierr)
!!$  call BINARY_FILE_READ(iunit,ny,1,kind(ny),ierr)
!!$  call BINARY_FILE_READ(iunit,nz,1,kind(nz),ierr)
!!$  call BINARY_FILE_READ(iunit,nvar,1,kind(nvar),ierr)
!!$  ! Read additional stuff
!!$  call BINARY_FILE_READ(iunit,dt,1,kind(dt),ierr)
!!$  call BINARY_FILE_READ(iunit,time0,1,kind(time0),ierr)
!!$  ! Read variable names
!!$  allocate(names(nvar))
!!$  do var=1,nvar
!!$     call BINARY_FILE_READ(iunit,names(var),str_short,kind(names),ierr)
!!$  end do
!!$  ! Read data field
!!$  allocate(data1(nx,ny,nz,nvar))
!!$  do var=1,nvar
!!$     call BINARY_FILE_READ(iunit,data1(:,:,:,var),nx*ny*nz,kind(data1),ierr)
!!$  end do
!!$  call BINARY_FILE_CLOSE(iunit,ierr)
!!$
!!$  ! Read the first files
!!$  call BINARY_FILE_OPEN(iunit,'data',"r",ierr)
!!$  ! Read sizes
!!$  call BINARY_FILE_READ(iunit,nx,1,kind(nx),ierr)
!!$  call BINARY_FILE_READ(iunit,ny,1,kind(ny),ierr)
!!$  call BINARY_FILE_READ(iunit,nz,1,kind(nz),ierr)
!!$  call BINARY_FILE_READ(iunit,nvar,1,kind(nvar),ierr)
!!$  ! Read additional stuff
!!$  call BINARY_FILE_READ(iunit,dt,1,kind(dt),ierr)
!!$  call BINARY_FILE_READ(iunit,time,1,kind(time),ierr)
!!$  ! Read variable names
!!$  allocate(names(nvar))
!!$  do var=1,nvar
!!$     call BINARY_FILE_READ(iunit,names(var),str_short,kind(names),ierr)
!!$  end do
!!$  ! Read data field
!!$  allocate(data2(nx,ny,nz,nvar))
!!$  do var=1,nvar
!!$     call BINARY_FILE_READ(iunit,data2(:,:,:,var),nx*ny*nz,kind(data2),ierr)
!!$  end do
!!$  call BINARY_FILE_CLOSE(iunit,ierr)
!!$  
!!$  ! Compute stuff
!!$  num = 0.0_WP
!!$  den = 0.0_WP
!!$  do var=1,3
!!$     do k=1,nz
!!$        do j=1,ny
!!$           do i=1,nx
!!$              num=num+data1(i,j,k,var)*data2(i,j,k,var)
!!$              den=den+data1(i,j,k,var)*data1(i,j,k,var)
!!$           end do
!!$        end do
!!$     end do
!!$  end do
!!$  decay = num/den
!!$  print*,'Decay:',decay
!!$  
!!$  return
!!$end subroutine read_taylor_info
!!$
!!$
!!$subroutine read_scalar_info
!!$  use analyzer
!!$  use parser
!!$  use fileio
!!$  implicit none
!!$  
!!$  real(WP), dimension(:,:,:,:), allocatable :: data1,data2
!!$  real(WP) :: err,norm
!!$  integer :: ierr,var,nvar,i,j,k,iunit
!!$  real(WP) :: dt,time
!!$  character(len=str_short), dimension(:), allocatable :: names  
!!$  
!!$  ! Read the first files
!!$  call BINARY_FILE_OPEN(iunit,'data.init',"r",ierr)
!!$  ! Read sizes
!!$  call BINARY_FILE_READ(iunit,nx,1,kind(nx),ierr)
!!$  call BINARY_FILE_READ(iunit,ny,1,kind(ny),ierr)
!!$  call BINARY_FILE_READ(iunit,nz,1,kind(nz),ierr)
!!$  call BINARY_FILE_READ(iunit,nvar,1,kind(nvar),ierr)
!!$  ! Read additional stuff
!!$  call BINARY_FILE_READ(iunit,dt,1,kind(dt),ierr)
!!$  call BINARY_FILE_READ(iunit,time,1,kind(time),ierr)
!!$  ! Read variable names
!!$  allocate(names(nvar))
!!$  do var=1,nvar
!!$     call BINARY_FILE_READ(iunit,names(var),str_short,kind(names),ierr)
!!$  end do
!!$  ! Read data field
!!$  allocate(data1(nx,ny,nz,nvar))
!!$  do var=1,nvar
!!$     call BINARY_FILE_READ(iunit,data1(:,:,:,var),nx*ny*nz,kind(data1),ierr)
!!$  end do
!!$  call BINARY_FILE_CLOSE(iunit,ierr)
!!$  
!!$  ! Read the first files
!!$  call BINARY_FILE_OPEN(iunit,'data',"r",ierr)
!!$  ! Read sizes
!!$  call BINARY_FILE_READ(iunit,nx,1,kind(nx),ierr)
!!$  call BINARY_FILE_READ(iunit,ny,1,kind(ny),ierr)
!!$  call BINARY_FILE_READ(iunit,nz,1,kind(nz),ierr)
!!$  call BINARY_FILE_READ(iunit,nvar,1,kind(nvar),ierr)
!!$  ! Read additional stuff
!!$  call BINARY_FILE_READ(iunit,dt,1,kind(dt),ierr)
!!$  call BINARY_FILE_READ(iunit,time,1,kind(time),ierr)
!!$  ! Read variable names
!!$  allocate(names(nvar))
!!$  do var=1,nvar
!!$     call BINARY_FILE_READ(iunit,names(var),str_short,kind(names),ierr)
!!$  end do
!!$  ! Read data field
!!$  allocate(data2(nx,ny,nz,nvar))
!!$  do var=1,nvar
!!$     call BINARY_FILE_READ(iunit,data2(:,:,:,var),nx*ny*nz,kind(data2),ierr)
!!$  end do
!!$  call BINARY_FILE_CLOSE(iunit,ierr)
!!$  
!!$  ! Compute stuff
!!$  norm = 0.0_WP
!!$  err  = 0.0_WP
!!$  do k=1,nz
!!$     do j=1,ny
!!$        do i=1,nx
!!$           norm=norm+data1(i,j,k,5)**2
!!$           err = err+(data2(i,j,k,5)-data1(i,j,k,5))**2
!!$        end do
!!$     end do
!!$  end do
!!$  norm=sqrt(norm/(nx*ny*nz))
!!$  err =sqrt(err /(nx*ny*nz))/norm
!!$  print*,'Mesh size:',nx,ny,nz
!!$  print*,'error:',err
!!$  
!!$  return
!!$end subroutine read_scalar_info
!!$
!!$
!!$subroutine read_ensight_info
!!$  use analyzer
!!$  use parser
!!$  use fileio
!!$  implicit none
!!$  
!!$  character(len=str_medium) :: directory
!!$  character(len=str_medium) :: case_file
!!$  character(len=str_medium) :: geom_file
!!$  real(WP), dimension(:), allocatable :: time
!!$  real(SP), dimension(:,:,:), allocatable :: buffersp
!!$  integer, dimension(:), allocatable :: ntime
!!$  integer :: nfile,inc,i,ierr,iunit
!!$  character(len=80) :: buffer
!!$  integer :: ibuffer
!!$  real(SP) :: rbuffer
!!$  real(WP), dimension(:), allocatable :: xsp,ysp,zsp
!!$  real(WP) :: mytime
!!$  integer :: myntime
!!$  character(len=str_medium) :: ans,name,file
!!$  
!!$  ! Find the files
!!$  print "(a34,$)", " Ensight directory : [ensight-3D] "
!!$  read "(a)", directory
!!$  if (trim(directory).eq.'') directory='ensight-3D'
!!$  
!!$  ! Read the case file
!!$  call parser_init
!!$  case_file = trim(directory) // '/case'
!!$  call parser_parsefile(case_file)
!!$  call parser_read('number of steps',nfile)
!!$  print "(a24,i5,/)", " Number of file found : ",nfile
!!$  allocate(time(nfile))
!!$  allocate(ntime(nfile))
!!$  call parser_read('time values',time)
!!$  call parser_read('filename start number',ntime(1))
!!$  call parser_read('filename increment',inc)
!!$  do i=2,nfile
!!$     ntime(i)=ntime(i-1)+inc
!!$  end do
!!$  
!!$  ! Read the geometry file
!!$  geom_file = trim(directory) // '/geometry'
!!$  call BINARY_FILE_OPEN(iunit,trim(geom_file),"r",ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$  call BINARY_FILE_READ(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$  call BINARY_FILE_READ(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$  call BINARY_FILE_READ(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$  call BINARY_FILE_READ(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$  call BINARY_FILE_READ(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,ibuffer,1,kind(ibuffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,nx,1,kind(nx),ierr)
!!$  nx=nx-1
!!$  call BINARY_FILE_READ(iunit,ny,1,kind(ny),ierr)
!!$  ny=ny-1
!!$  call BINARY_FILE_READ(iunit,nz,1,kind(nz),ierr)
!!$  nz=nz-1
!!$  allocate(xsp(1:nx+1))
!!$  allocate(x(1:nx+1))
!!$  call BINARY_FILE_READ(iunit,xsp,nx+1,kind(xsp),ierr)
!!$  x = xsp
!!$  allocate(ysp(1:ny+1))
!!$  allocate(y(1:ny+1))
!!$  call BINARY_FILE_READ(iunit,ysp,ny+1,kind(ysp),ierr)
!!$  y = ysp
!!$  allocate(zsp(1:nz+1))
!!$  allocate(z(1:nz+1))
!!$  call BINARY_FILE_READ(iunit,zsp,nz+1,kind(zsp),ierr)
!!$  z = zsp
!!$  call BINARY_FILE_CLOSE(iunit,ierr)
!!$  
!!$  ! Print stuff
!!$  print "(a,3i6)", " Domain size :",nx,ny,nz
!!$  
!!$  ! Read the data files
!!$  print "(a,ES12.3)", " Initial time :",time(1)
!!$  print "(a,ES12.3)", " Final time :",time(nfile)
!!$  print "(a,ES12.3)", " Time increment :",time(2)-time(1)
!!$  ans='n'
!!$  do while (trim(ans).eq.'n' .or. trim(ans).eq.'N' .or. trim(ans).eq.'no')
!!$     print "(a,$)", " File to process : "
!!$     read "(i6)", myntime
!!$     !myntime = max(min(myntime,nfile),1)
!!$     !mytime = time(myntime)
!!$     print "(a,i6,ES12.3)", " Selected time :",myntime,mytime
!!$     print "(a,$)", " Is it ok ? [y/N] "
!!$     read "(a)", ans
!!$     if (trim(ans).ne.'Y' .and. trim(ans).ne.'y' .and. trim(ans).ne.'yes') ans='n'
!!$  end do
!!$  write(numb(1:6),'(i6.6)') myntime
!!$  allocate(buffersp(1:nx,1:ny,1:nz))
!!$  
!!$  ! Y_C7H16,T,Sm,ND,Wchem,Z,CHI
!!$  name = 'Y_C7H16'
!!$  file = trim(adjustl(directory))//'/'//trim(name)//'/'//trim(name)//'.'//trim(adjustl(numb))
!!$  print "(a,a,a,a,a)", " Doing ",trim(adjustl(name))," (",trim(adjustl(file)),")"
!!$  call BINARY_FILE_OPEN(iunit,trim(file),"r",ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,ibuffer,1,kind(ibuffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffersp,nx*ny*nz,kind(buffersp),ierr)
!!$  call BINARY_FILE_CLOSE(iunit,ierr)
!!$  allocate(Y_C7H16(1:nx,1:ny,1:nz))
!!$  Y_C7H16=buffersp
!!$  
!!$  name = 'T'
!!$  file = trim(adjustl(directory))//'/'//trim(name)//'/'//trim(name)//'.'//trim(adjustl(numb))
!!$  print "(a,a,a,a,a)", " Doing ",trim(adjustl(name))," (",trim(adjustl(file)),")"
!!$  call BINARY_FILE_OPEN(iunit,trim(file),"r",ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,ibuffer,1,kind(ibuffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffersp,nx*ny*nz,kind(buffersp),ierr)
!!$  call BINARY_FILE_CLOSE(iunit,ierr)
!!$  allocate(T(1:nx,1:ny,1:nz))
!!$  T=buffersp
!!$  
!!$  name = 'Sm'
!!$  file = trim(adjustl(directory))//'/'//trim(name)//'/'//trim(name)//'.'//trim(adjustl(numb))
!!$  print "(a,a,a,a,a)", " Doing ",trim(adjustl(name))," (",trim(adjustl(file)),")"
!!$  call BINARY_FILE_OPEN(iunit,trim(file),"r",ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,ibuffer,1,kind(ibuffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffersp,nx*ny*nz,kind(buffersp),ierr)
!!$  call BINARY_FILE_CLOSE(iunit,ierr)
!!$  allocate(Sm(1:nx,1:ny,1:nz))
!!$  Sm=buffersp
!!$  
!!$  name = 'ND'
!!$  file = trim(adjustl(directory))//'/'//trim(name)//'/'//trim(name)//'.'//trim(adjustl(numb))
!!$  print "(a,a,a,a,a)", " Doing ",trim(adjustl(name))," (",trim(adjustl(file)),")"
!!$  call BINARY_FILE_OPEN(iunit,trim(file),"r",ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,ibuffer,1,kind(ibuffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffersp,nx*ny*nz,kind(buffersp),ierr)
!!$  call BINARY_FILE_CLOSE(iunit,ierr)
!!$  allocate(ND(1:nx,1:ny,1:nz))
!!$  ND=buffersp
!!$  
!!$  name = 'Wchem'
!!$  file = trim(adjustl(directory))//'/'//trim(name)//'/'//trim(name)//'.'//trim(adjustl(numb))
!!$  print "(a,a,a,a,a)", " Doing ",trim(adjustl(name))," (",trim(adjustl(file)),")"
!!$  call BINARY_FILE_OPEN(iunit,trim(file),"r",ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,ibuffer,1,kind(ibuffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffersp,nx*ny*nz,kind(buffersp),ierr)
!!$  call BINARY_FILE_CLOSE(iunit,ierr)
!!$  allocate(Wchem(1:nx,1:ny,1:nz))
!!$  Wchem=buffersp
!!$  
!!$  name = 'ZMIX'
!!$  file = trim(adjustl(directory))//'/'//trim(name)//'/'//trim(name)//'.'//trim(adjustl(numb))
!!$  print "(a,a,a,a,a)", " Doing ",trim(adjustl(name))," (",trim(adjustl(file)),")"
!!$  call BINARY_FILE_OPEN(iunit,trim(file),"r",ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,ibuffer,1,kind(ibuffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffersp,nx*ny*nz,kind(buffersp),ierr)
!!$  call BINARY_FILE_CLOSE(iunit,ierr)
!!$  allocate(ZMIX(1:nx,1:ny,1:nz))
!!$  ZMIX=buffersp
!!$  
!!$  name = 'CHI'
!!$  file = trim(adjustl(directory))//'/'//trim(name)//'/'//trim(name)//'.'//trim(adjustl(numb))
!!$  print "(a,a,a,a,a)", " Doing ",trim(adjustl(name))," (",trim(adjustl(file)),")"
!!$  call BINARY_FILE_OPEN(iunit,trim(file),"r",ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,ibuffer,1,kind(ibuffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffer,80,kind(buffer),ierr)
!!$  call BINARY_FILE_READ(iunit,buffersp,nx*ny*nz,kind(buffersp),ierr)
!!$  call BINARY_FILE_CLOSE(iunit,ierr)
!!$  allocate(CHI(1:nx,1:ny,1:nz))
!!$  CHI=buffersp
!!$  
!!$  return
!!$end subroutine read_ensight_info
!!$
!!$
!!$subroutine analyze_it
!!$  use analyzer
!!$  use parser
!!$  use fileio
!!$  implicit none
!!$  
!!$  character(len=str_medium) :: name
!!$  ! CHI
!!$  real(WP), dimension(:,:), allocatable :: pdf_CHI
!!$  integer :: nbin_CHI
!!$  ! ZMIX
!!$  real(WP), dimension(:,:), allocatable :: pdf_ZMIX
!!$  integer :: nbin_ZMIX
!!$  ! CHI_ZMIX
!!$  real(WP), dimension(:,:), allocatable :: pdf_CHI_ZMIX
!!$  ! CHI
!!$  real(WP), dimension(:,:), allocatable :: pdf_CHI_cond1
!!$  real(WP), dimension(:,:), allocatable :: pdf_CHI_cond2
!!$  real(WP), dimension(:,:), allocatable :: pdf_CHI_cond3
!!$  real(WP), dimension(:,:), allocatable :: mean_CHI_of_Z
!!$  ! Sm
!!$  real(WP), dimension(:,:), allocatable :: mean_Sm_of_Z
!!$  
!!$  ! CHI
!!$  name = "CHI"
!!$  nbin_CHI=200
!!$  allocate(pdf_CHI(2,nbin_CHI))
!!$  call get_1dpdf(CHI,nbin_CHI,pdf_CHI,name)
!!$  
!!$  ! ZMIX
!!$  name = "ZMIX"
!!$  nbin_ZMIX=200
!!$  allocate(pdf_ZMIX(2,nbin_ZMIX))
!!$  call get_1dpdf(ZMIX,nbin_ZMIX,pdf_ZMIX,name)
!!$  
!!$  ! Joint pdf CHI ZMIX
!!$  name = "CHI_ZMIX"
!!$  nbin_ZMIX=200
!!$  nbin_CHI=200
!!$  allocate(pdf_CHI_ZMIX(nbin_CHI,nbin_ZMIX))
!!$  call get_2dpdf(CHI,ZMIX,nbin_CHI,nbin_ZMIX,pdf_CHI_ZMIX,name)
!!$  
!!$  ! Conditional pdf CHI @ ZMIX=0.03
!!$  name = "CHI@Z=0.03"
!!$  nbin_CHI=200
!!$  allocate(pdf_CHI_cond1(2,nbin_CHI))
!!$  call get_1dpdfc(CHI,nbin_CHI,pdf_CHI_cond1,name,ZMIX,0.06_WP,0.01_WP)
!!$  
!!$  ! Conditional pdf CHI @ ZMIX=0.06
!!$  name = "CHI@Z=0.06"
!!$  nbin_CHI=200
!!$  allocate(pdf_CHI_cond2(2,nbin_CHI))
!!$  call get_1dpdfc(CHI,nbin_CHI,pdf_CHI_cond2,name,ZMIX,0.06_WP,0.01_WP)
!!$  
!!$  ! Conditional pdf CHI @ ZMIX=0.12
!!$  name = "CHI@Z=0.12"
!!$  nbin_CHI=200
!!$  allocate(pdf_CHI_cond3(2,nbin_CHI))
!!$  call get_1dpdfc(CHI,nbin_CHI,pdf_CHI_cond3,name,ZMIX,0.12_WP,0.01_WP)
!!$  
!!$  ! Conditional mean of CHI as a function of Z
!!$  name = "CHI_of_Z"
!!$  nbin_ZMIX=200
!!$  allocate(mean_CHI_of_Z(2,nbin_ZMIX))
!!$  call get_condmean(CHI,ZMIX,nbin_ZMIX,mean_CHI_of_Z,name)
!!$  
!!$  ! Conditional mean of Sm as a function of Z
!!$  name = "Sm_of_Z"
!!$  nbin_ZMIX=200
!!$  allocate(mean_Sm_of_Z(2,nbin_ZMIX))
!!$  call get_condmean(Sm,ZMIX,nbin_ZMIX,mean_Sm_of_Z,name)
!!$  
!!$  return
!!$end subroutine analyze_it
!!$
!!$
!!$subroutine get_condmean(S1,S2,nbin,mean,what)
!!$  use analyzer
!!$  use parser
!!$  use fileio
!!$  implicit none
!!$  
!!$  integer :: iunit,i,j,k,p
!!$  character(len=str_medium) :: file,what
!!$  integer :: nbin
!!$  real(WP), dimension(1:2,1:nbin) :: mean
!!$  real(WP), dimension(:), allocatable :: counter
!!$  real(WP) :: minS,maxS
!!$  real(WP), dimension(1:nx,1:ny,1:nz) :: S1,S2
!!$  
!!$  ! Compute cond mean
!!$  print "(a,a)", " Computing conditional mean of ",trim(adjustl(what))
!!$  minS=0.0_WP!minval(S2)
!!$  maxS=1.0_WP!maxval(S2)
!!$  mean = 0.0_WP
!!$  allocate(counter(nbin))
!!$  counter = 0.0_WP
!!$  do k=1,nz
!!$     do j=1,ny
!!$        do i=1,nx
!!$           p = floor((S2(i,j,k)-minS)/(maxS-minS)*(nbin-1))+1
!!$           mean(2,p) = mean(2,p) + S1(i,j,k)
!!$           counter(p) = counter(p) + 1.0_WP
!!$        end do
!!$     end do
!!$  end do
!!$  
!!$  ! Dump results to a file
!!$  file='mean'//'-'//trim(adjustl(what))//'.'//trim(adjustl(numb))
!!$  iunit=iopen()
!!$  open(iunit,file=file,form='formatted')
!!$  do p=1,nbin
!!$     mean(1,p) = real((p-1),WP)/real((nbin-1),WP)
!!$     if (counter(p)>0.0_WP) write(iunit,'(ES12.3,x,ES12.3)') mean(1,p),mean(2,p)/counter(p)
!!$  end do
!!$  close(iclose(iunit))
!!$  
!!$  return
!!$end subroutine get_condmean
!!$
!!$
!subroutine get_1dpdf(S,nbin,pdf,what)
!  use analyzer
!  use parser
!  use fileio
!  implicit none
!  
!  integer :: iunit,i,j,k,p
!  character(len=str_medium) :: file,what
!  integer :: nbin
!  real(WP), dimension(1:2,1:nbin) :: pdf
!  real(WP) :: minS,maxS
!  real(WP), dimension(1:nx,1:ny,1:nz) :: S
!  
!  ! Compute pdf
!  print "(a,a)", " Computing pdf of ",trim(adjustl(what))
!  minS=minval(S)
!  maxS=maxval(S)
!  pdf = 0.0_WP
!  do k=1,nz
!     do j=1,ny
!        do i=1,nx
!           p = floor((S(i,j,k)-minS)/(maxS-minS)*(nbin-1))+1
!           pdf(2,p) = pdf(2,p) + 1.0_WP/real(nx*ny*nz,WP)
!        end do
!     end do
!  end do
!  
!  ! Dump results to a file
!  file='pdf'//'-'//trim(adjustl(what))//'.'//trim(adjustl(numb))
!  iunit=iopen()
!  open(iunit,file=file,form='formatted')
!  do p=1,nbin
!     pdf(1,p) = real((p-1),WP)/real((nbin-1),WP)
!     write(iunit,'(ES12.3,x,ES12.3)') pdf(1,p),pdf(2,p)
!  end do
!  close(iclose(iunit))
!  
!  return
!end subroutine get_1dpdf
!!$
!!$
!!$subroutine get_1dpdfc(S,nbin,pdf,what,Sc,c,cc)
!!$  use analyzer
!!$  use parser
!!$  use fileio
!!$  implicit none
!!$  
!!$  integer :: iunit,i,j,k,p
!!$  character(len=str_medium) :: file,what
!!$  integer :: nbin
!!$  real(WP), dimension(1:2,1:nbin) :: pdf
!!$  real(WP) :: minS,maxS
!!$  real(WP), dimension(1:nx,1:ny,1:nz) :: S
!!$  real(WP) :: c,cc
!!$  real(WP), dimension(1:nx,1:ny,1:nz) :: Sc
!!$  
!!$  ! Compute pdf
!!$  print "(a,a)", " Computing conditional pdf of ",trim(adjustl(what))
!!$  minS=minval(S)
!!$  maxS=maxval(S)
!!$  pdf = 0.0_WP
!!$  do k=1,nz
!!$     do j=1,ny
!!$        do i=1,nx
!!$           p = floor((S(i,j,k)-minS)/(maxS-minS)*(nbin-1))+1
!!$           if (abs(Sc(i,j,k)-c)<cc) pdf(2,p) = pdf(2,p) + 1.0_WP
!!$        end do
!!$     end do
!!$  end do
!!$  pdf(2,:)=pdf(2,:)/sum(pdf(2,:))
!!$  
!!$  ! Dump results to a file
!!$  file='pdf'//'-'//trim(adjustl(what))//'.'//trim(adjustl(numb))
!!$  iunit=iopen()
!!$  open(iunit,file=file,form='formatted')
!!$  do p=1,nbin
!!$     pdf(1,p) = real((p-1),WP)/real((nbin-1),WP)
!!$     write(iunit,'(ES12.3,x,ES12.3)') pdf(1,p),pdf(2,p)
!!$  end do
!!$  close(iclose(iunit))
!!$  
!!$  return
!!$end subroutine get_1dpdfc
!!$
!!$
!!$subroutine get_2dpdf(S1,S2,nbin1,nbin2,pdf,what)
!!$  use analyzer
!!$  use parser
!!$  use fileio
!!$  implicit none
!!$  
!!$  integer :: iunit,i,j,k,p1,p2
!!$  character(len=str_medium) :: file,what
!!$  integer :: nbin1,nbin2
!!$  real(WP), dimension(1:nbin1) :: bin1
!!$  real(WP), dimension(1:nbin2) :: bin2
!!$  real(WP), dimension(1:nbin1,1:nbin2) :: pdf
!!$  real(WP) :: minS1,maxS1,minS2,maxS2
!!$  real(WP), dimension(1:nx,1:ny,1:nz) :: S1,S2
!!$  
!!$  ! Compute pdf
!!$  print "(a,a)", " Computing joint pdf of ",trim(adjustl(what))
!!$  minS1=minval(S1)
!!$  maxS1=maxval(S1)
!!$  minS2=minval(S2)
!!$  maxS2=maxval(S2)
!!$  pdf = 0.0_WP
!!$  do k=1,nz
!!$     do j=1,ny
!!$        do i=1,nx
!!$           p1 = floor((S1(i,j,k)-minS1)/(maxS1-minS1)*(nbin1-1))+1
!!$           p2 = floor((S2(i,j,k)-minS2)/(maxS2-minS2)*(nbin2-1))+1
!!$           pdf(p1,p2) = pdf(p1,p2) + 1.0_WP/real(nx*ny*nz,WP)
!!$        end do
!!$     end do
!!$  end do
!!$  
!!$  ! Dump results to a file
!!$  file='pdf'//'-'//trim(adjustl(what))//'.'//trim(adjustl(numb))
!!$  iunit=iopen()
!!$  open(iunit,file=file,form='formatted')
!!$  do p1=1,nbin1
!!$     bin1(p1) = real((p1-1),WP)/real((nbin1-1),WP)
!!$  end do
!!$  do p2=1,nbin2
!!$     bin2(p2) = real((p2-1),WP)/real((nbin2-1),WP)
!!$  end do
!!$  do p2=1,nbin2
!!$     do p1=1,nbin1
!!$        write(iunit,'(ES12.3,$)') pdf(p1,p2)
!!$     end do
!!$     write(iunit,'(/)')
!!$  end do
!!$  close(iclose(iunit))
!!$  
!!$  return
!!$end subroutine get_2dpdf
