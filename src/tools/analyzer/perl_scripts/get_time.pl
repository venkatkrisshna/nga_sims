#!/usr/bin/perl -w

open IN, "<$ARGV[0]" or die "$!";
@data = <IN>;
@data = &trim(@data);
close(IN);
foreach (@data){
    $_=~s/time values://;
    $_ = &trim($_);
    if ($_=~/^([0-9Ee\+\-\.]+)\s+([0-9Ee\+\-\.]+)\s+([0-9Ee\+\-\.]+)$/){
	push @times, $1;
	push @times, $2;
	push @times, $3;
    }
    elsif ($_=~/^([0-9Ee\+\-\.]+)\s+([0-9Ee\+\-\.]+)$/){
	print $_;
	push @times, $1;
	push @times, $2;
    }
    elsif ($_=~/^([0-9Ee\+\-\.]+)$/){
	print $_;
	push @times, $1;
    }
}

open OUT, ">time.txt";
$i=1;
foreach (@times){
    print OUT "time\($i\) = $times[$i-1]\n";
    $i++;
}
close(OUT);




	
    
sub trim {
    my @out = @_;
    for (@out) {
        s/^\s+//;
        s/\s+$//;
	s/\s+/ /g;
    }
    return wantarray ? @out : $out[0];
}
