program bs2chemtable
  use parser
  use precision
  use string
  implicit none
  
  ! Filename of input file
  character(len=str_medium) :: name
  
  ! Parse the input file
  call get_command_argument(1,name)
  call parser_init
  call parser_parsefile(name)
  
  ! Initialize the BS chemistry
  call bs_init
  
  ! Create the table
  call bs_generate
  
  ! Dump it
  call bs_dump_chemtable
  
end program bs2chemtable

