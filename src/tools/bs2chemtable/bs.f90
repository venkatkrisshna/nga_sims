module bs
  use precision
  use string
  implicit none
  
  ! BS parameters ---------------------------
  
  ! 1 = RHS => Z=1
  ! 2 = LHS => Z=0
  
  ! RHS and LHS temperatures
  real(WP) :: T_1,T_2
  
  ! Adiabatic flame temperature
  real(WP) :: T_flame
  
  ! RHS and LHS mass fractions
  real(WP) :: Y_f_1,Y_o_2
  
  ! Molecular masses
  real(WP) :: W_f,W_o,W_d,W_p
  
  ! Stoichiometric coefficients
  real(WP) :: nu_f,nu_o,nu_d,nu_p
  real(WP) :: r_st
  ! Stoichiometric Mixture Fraction
  real(WP) :: Z_st
  
  ! Ideal gas constant
  real(WP) :: R_cst
  
  ! Ambient pressure
  real(WP) :: P
  
  ! Burning or mixing
  integer :: burning
  
  ! Constant properties
  integer :: cst_prop
  real(WP) :: VISC_cst
  real(WP) :: DIFF_cst
  
  ! Flamelet --------------------------------
  
  ! Discretized Z axis
  integer :: n
  real(WP), dimension(:), pointer :: Z
  
  ! Temperature
  real(WP), dimension(:), pointer :: T
  
  ! Density
  real(WP), dimension(:), pointer :: RHO
  
  ! Viscosity (mu)
  real(WP), dimension(:), pointer :: VISC
  
  ! Diffusivity
  real(WP), dimension(:), pointer :: DIFF
  
  ! Mass fractions
  real(WP), dimension(:), pointer :: Yf
  real(WP), dimension(:), pointer :: Yo
  real(WP), dimension(:), pointer :: Yp
  real(WP), dimension(:), pointer :: Yd
  
  ! Molecular weight of the mixture
  real(WP), dimension(:), pointer :: W
  
  ! Chemtable file --------------------------
  
  ! File name
  character(len=str_medium) :: filename
  
  ! Coordinates of the chemtable
  integer :: n1,n2,n3
  real(WP), dimension(:), pointer :: x1,x2,x3
  
  ! Mask of the chemtable
  integer, dimension(:,:,:), allocatable :: chem_mask
  
  ! Names in the chemtable
  integer :: nvar_chem
  character(len=str_medium), dimension(:), allocatable :: names_chem
  
  ! Chemtable model
  character(len=str_medium) :: combModel
  
  ! Table of variables
  real(WP), dimension(:,:,:,:), pointer :: table
  
end module bs

subroutine bs_init
  use bs
  use parser
  implicit none
  
  integer :: i
  
  ! Initialize the chemtable parameters ----------------------------------------
  ! Dimensions
  call parser_read('Number of points',n)
  n1 = n
  n2 = 2
  n3 = 2
  ! Mesh
  allocate(x1(n1),x2(n2),x3(n3))
  do i=1,n1
     x1(i) = real(i-1,WP)/real(n1-1,WP)
  end do
  x2 = 0.0_WP
  x3 = 0.0_WP
  ! Mask
  allocate(chem_mask(n1,n2,n3))
  chem_mask = 0
  ! Combustion model
  combModel = 'Burke-Schumann'
  ! Names
  nvar_chem = 9
  allocate(names_chem(nvar_chem))
  names_chem(1) = 'T'
  names_chem(2) = 'RHO'
  names_chem(3) = 'W'
  names_chem(4) = 'Yo'
  names_chem(5) = 'Yf'
  names_chem(6) = 'Yd'
  names_chem(7) = 'Yp'
  names_chem(8) = 'VISC'
  names_chem(9) = 'DIFF'
  ! Data
  allocate(table(n1,n2,n3,nvar_chem))
  
  ! Read the parameters from the input file
  call parser_read('Burning',burning,1)
  call parser_read('Constant properties',cst_prop,1)
  if (cst_prop.eq.1) then
     call parser_read('Viscosity',VISC_cst)
     call parser_read('Diffusivity',DIFF_cst)
  else
     stop 'If you want variable properties, code it yourself.'
  end if
  call parser_read('Temperature at Z=0',T_2)
  call parser_read('Temperature at Z=1',T_1)
  call parser_read('Oxidizer mass fraction at Z=0',Y_o_2)
  call parser_read('Fuel mass fraction at Z=1',Y_f_1)
  call parser_read('Adiabatic flame temperature',T_flame)
  nu_f = -1.0_WP
  call parser_read('Stoichiometric coefficient of oxidizer',nu_o)
  nu_o = -abs(nu_o)
  nu_p = 1.0_WP
  nu_d = 0.0_WP
  call parser_read('Molecular mass of fuel',W_f)
  call parser_read('Molecular mass of oxidizer',W_o)
  W_p = W_f + abs(nu_o)*W_o
  call parser_read('Molecular mass of diluent',W_d)
  r_st = (nu_o*W_o)/(nu_f*W_f)
  Z_st = 1.0_WP/(1.0_WP+r_st*Y_f_1/Y_o_2)
  call parser_read('Ideal gas constant',R_cst,8.314472_WP)
  call parser_read('Pressure',P,1.0133e5_WP)
  call parser_read('Chemtable file', filename)
  
  ! Prepare variables
  Z    => x1
  T    => table(:,1,1,1)
  RHO  => table(:,1,1,2)
  W    => table(:,1,1,3)
  Yo   => table(:,1,1,4)
  Yf   => table(:,1,1,5)
  Yd   => table(:,1,1,6)
  Yp   => table(:,1,1,7)
  VISC => table(:,1,1,8)
  DIFF => table(:,1,1,9)
  
  ! Initialize cst prop
  if (cst_prop.eq.1) then
     VISC = VISC_cst
     DIFF = DIFF_cst
  end if
  
  return
end subroutine bs_init

subroutine bs_generate
  use bs
  implicit none
  
  integer :: i
  
  ! Generate the flamelet
  if (burning.eq.1) then
     do i=1,n
        if (Z(i)<Z_st) then
           ! Temperature
           T(i) = T_2 + Z(i)*(T_1-T_2) + (Z(i)/Z_st)*T_flame
           ! Mass fractions
           Yf(i) = 0.0_WP
           Yo(i) = Y_o_2*(1.0_WP-Z(i)/Z_st)
           Yp(i) = Y_f_1*Z_st*W_p/W_f*Z(i)/Z_st
           Yd(i) = 1.0_WP-(Yf(i)+Yo(i)+Yp(i))
           ! Molecular mass
           W(i) = 1.0_WP/(Yf(i)/W_f+Yo(i)/W_o+Yp(i)/W_p+Yd(i)/W_d)
           ! Density
           RHO(i) = P*W(i)/(R_cst*T(i))
        else
           ! Temperature
           T(i) = T_2 + Z(i)*(T_1-T_2) + (1.0_WP-Z(i))/(1.0_WP-Z_st)*T_flame
           ! Mass fractions
           Yf(i) = Y_f_1*(Z(i)-Z_st)/(1.0_WP-Z_st)
           Yo(i) = 0.0_WP
           Yp(i) = Y_f_1*Z_st*W_p/W_f*(1.0_WP-Z(i))/(1.0_WP-Z_st)
           Yd(i) = 1.0_WP-(Yf(i)+Yo(i)+Yp(i))
           ! Molecular mass
           W(i) = 1.0_WP/(Yf(i)/W_f+Yo(i)/W_o+Yp(i)/W_p+Yd(i)/W_d)
           ! Density
           RHO(i) = P*W(i)/(R_cst*T(i))
        end if
     end do
  else
     do i=1,n
        ! Temperature
        T(i) = T_2 + Z(i)*(T_1-T_2)
        ! Mass fractions
        Yf(i) = Z(i)*Y_f_1
        Yo(i) = Y_o_2*(1.0_WP-Z(i))
        Yp(i) = 0.0_WP
        Yd(i) = 1.0_WP-(Yf(i)+Yo(i)+Yp(i))
        ! Molecular mass
        W(i) = 1.0_WP/(Yf(i)/W_f+Yo(i)/W_o+Yp(i)/W_p+Yd(i)/W_d)
        ! Density
        RHO(i) = P*W(i)/(R_cst*T(i))
     end do
  end if
  
  return
end subroutine bs_generate

subroutine bs_dump_chemtable
  use bs
  use fileio
  implicit none
  
  integer :: var,i,iunit,ierr
  
  ! Extend the chemtable on x2 and x3 ------------------------------------------
  do var=1,nvar_chem
     do i=1,n
        table(i,:,:,var) = table(i,1,1,var)
     end do
  end do
  
  ! Write the file -------------------------------------------------------------
  call BINARY_FILE_OPEN(iunit,trim(filename),"w",ierr)
  call BINARY_FILE_WRITE(iunit,n1,1,kind(n1),ierr)
  call BINARY_FILE_WRITE(iunit,n2,1,kind(n2),ierr)
  call BINARY_FILE_WRITE(iunit,n3,1,kind(n3),ierr)
  call BINARY_FILE_WRITE(iunit,nvar_chem,1,kind(nvar_chem),ierr)
  ! Write the axis coordinates
  call BINARY_FILE_WRITE(iunit,x1,n1,kind(x1),ierr)
  call BINARY_FILE_WRITE(iunit,x2,n2,kind(x2),ierr)
  call BINARY_FILE_WRITE(iunit,x3,n3,kind(x3),ierr)
  ! Write the mask of the chemtable
  call BINARY_FILE_WRITE(iunit,chem_mask,n1*n2*n3,kind(chem_mask),ierr)
  ! Write additional stuff
  call BINARY_FILE_WRITE(iunit,combModel,str_medium,kind(combModel),ierr)
  ! Write variable names
  do var=1,nvar_chem
     call BINARY_FILE_WRITE(iunit,names_chem(var),str_medium,kind(names_chem),ierr)
  end do
  ! Write data field
  do var=1,nvar_chem
     call BINARY_FILE_WRITE(iunit,table(:,:,:,var),n1*n2*n3,kind(table),ierr)
  end do
  call BINARY_FILE_CLOSE(iunit,ierr)
  
  ! Dump the chemtable in ASCI at the same time --------------------------------
  iunit=iopen()
  filename=trim(filename)//'.txt'
  open(iunit,file=filename,form="formatted",iostat=ierr,status="REPLACE")
  filename = 'Z'
  write(iunit,'(10(x,a12))') filename,names_chem(:)
  do i=1,n
     write(iunit,'(10(e12.5,x))') Z(i),table(i,1,1,:)
  end do
  close(iunit)
  ierr=iclose(iunit)
  
  return
end subroutine bs_dump_chemtable
