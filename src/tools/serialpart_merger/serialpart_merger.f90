program serialpart_merger
  use precision
  use string
  use fileio
  implicit none

  type part_type
     integer*8 :: id
     ! Position (Cartesian)
     real(WP) :: x
     real(WP) :: y
     real(WP) :: z
     ! Velocity (Cartesian)
     real(WP) :: u
     real(WP) :: v
     real(WP) :: w
     ! Diameter
     real(WP) :: d
     ! Time step size
     real(WP) :: dt
     ! Collision force
     real(WP), dimension(3) :: Acol
     ! Collision torque
     ! Position (mesh)
     integer :: i
     integer :: j
     integer :: k
     ! Control parameter
     integer :: stop
  end type part_type

  integer :: iunit,ierr
  character(len=str_medium) :: filename,filename2
  character(len=str_medium) :: dirname,buffer
  integer :: nproc,proc
  integer :: npart,npart_,npart_read,part_size
  real(WP) :: dt,time,value
  type(part_type), dimension(:), allocatable :: part,part_buf
  logical :: file_is_there

  ! Read file name from standard input
  print*,'======================'
  print*,'| Serial part merger |'
  print*,'======================'
  print*
  print "(a,$)", " serial part directory : "
  read "(a)", dirname
  print "(a,$)", " part file to write : "
  read "(a)", filename2
  print "(a,$)", " number of procs : "
  read "(i9)", nproc
  print "(a,$)", " number of particles : "
  read "(i9)", npart_read

  ! Check if there is a file to read                  
  filename = trim(adjustl(dirname)) // '/part.000001'
  inquire(file=filename,exist=file_is_there)
  if (.not.file_is_there) then
     stop 'No serial file'
  end if

  ! Each processor reads its own particle file
  npart=0
  allocate(part(npart_read))
  do proc=1,nproc

        print *, 'processor ',proc,' reading'

        ! Set filename 
        write(buffer,'(i6.6)') proc
        filename = trim(adjustl(dirname)) // '/part.' // trim(adjustl(buffer))

        ! Open file        
        call BINARY_FILE_OPEN(iunit,trim(filename),"r",ierr)

        ! Read local number of particles
        call binary_file_read(iunit,npart_,1,kind(npart_),ierr)
        call binary_file_read(iunit,part_size,1,kind(part_size),ierr)                                     
        call binary_file_read(iunit,dt,1,kind(dt),ierr)                                         
        call binary_file_read(iunit,time,1,kind(time),ierr)

        ! Total number of particles
        npart=npart+npart_
        
        ! Read particles
        if (npart_>0) then
           ! Allocate buf part file
           !allocate(part_buf(1:npart))
           !if (npart.gt.npart_) part_buf(1:npart-npart_)=part
           !call binary_file_read(iunit,part_buf(npart-npart_+1:npart),npart_,part_size,ierr)
           call binary_file_read(iunit,part(1+npart-npart_:npart),npart_,part_size,ierr)
           !deallocate(part)
           !allocate(part(1:npart))
           !part=part_buf
           !deallocate(part_buf)
        end if

        ! Close the file
        call binary_file_close(iunit,ierr)
     
  end do

  ! Total number of particles
  print *, npart,' particles'
  if (npart.ne.npart_read) then
     print *, 'Error reading, npart read not correct'
  else
     print *, 'Writing',trim(filename2)
     ! Open new file to write
     call BINARY_FILE_OPEN(iunit,trim(filename2),"w",ierr)
     call BINARY_FILE_WRITE(iunit,npart,1,kind(npart),ierr)
     call BINARY_FILE_WRITE(iunit,part_size,1,kind(part_size),ierr)
     call BINARY_FILE_WRITE(iunit,dt,1,kind(dt),ierr)
     call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr)
     call BINARY_FILE_WRITE(iunit,part,npart,part_size,ierr)
     call BINARY_FILE_CLOSE(iunit,ierr)
  end if
  
end program serialpart_merger
