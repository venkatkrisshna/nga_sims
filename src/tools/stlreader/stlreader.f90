program stlreader
  use stlreader_parallel
  use precision
  use string
  use fileio
  use compgeom
  use parser
  use math
  implicit none
  
  ! General
  integer :: ierr,iunit,i,j,k,n
  real(WP), parameter :: eps=1.0e-9_WP

  ! STL file
  character(len=str_medium) :: stl_file
  character(len=80) :: cbuff
  character(len= 2) :: padding
  integer  :: nt
  real(SP) :: fltbuf
  type triangle_type
     real(WP), dimension(3) :: norm
     real(WP), dimension(3) :: v1
     real(WP), dimension(3) :: v2
     real(WP), dimension(3) :: v3
  end type triangle_type
  type(triangle_type), dimension(:), allocatable :: t
  real(WP) :: xmin,xmax,ymin,ymax,zmin,zmax
  real(WP), dimension(3) :: shift,scaling
  real(WP) :: sign_switch
  logical :: swap_xy,swap_yz,swap_zx
  logical :: swap_yx,swap_zy,swap_xz
  
  ! Config file
  character(len=str_medium) :: cfg_file
  character(len=str_medium) :: config
  integer :: nx,ny,nz
  integer :: xper,yper,zper
  integer :: icyl
  real(WP), dimension(:), allocatable :: x,y,z
  integer, dimension(:,:), allocatable :: mask
  
  ! IB file
  integer :: nod,count,nn,myloc
  character(len=str_short) :: ODname
  character(len=str_medium) :: optdata
  real(WP), dimension(:,:,:), allocatable :: Gib,Gib_global
  real(WP) :: dt,time,mydist,newdist,tmp,ang
  real(WP), dimension(3) :: c,mynorm,myproj,newproj,v1,v2
  real(WP), dimension(3,1500) :: proj,norm
  real(WP), dimension(1500) :: proj_count
  
  ! Volume masking
  logical :: done,found
  integer :: nloop,ntag,ii,jj,kk
  
  ! Input processing
  character(len=str_medium) :: input_name

  ! Parallel environment
  integer :: nz_, rem
  integer :: kmin_,kmax_
  integer :: imin_=1, jmin_=1
  integer :: imin=1, jmin=1, kmin=1

  ! Parallel write
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer(kind=MPI_Offset_kind) :: WP_MOK, str_MOK
  logical :: file_is_there
  integer, dimension(4) :: dims
  integer(kind=MPI_Offset_kind) :: disp
  integer :: ifile, data_size
  integer :: view
  integer, dimension(3) :: gsizes, lsizes, start
  
  ! Progress monitoring
  integer :: togo,prog
  integer :: iratio_new,iratio_old
  
  ! Parse the command line
  call get_command_argument(1,input_name)
  
  ! Initialize the parser
  call parser_init
  
  ! Read the input file
  call parser_parsefile(input_name)

  ! Initialize parallel environment
  call stlreader_parallel_init
  if (irank.eq.iroot) print*,'Working with',nproc,'processors'

  ! ============================================================
  ! Read stl file
  call parser_readchar('STL file',stl_file)
  
  ! Open the STL file to read
  if (irank.eq.iroot) print *,'Opening the STL file: ',stl_file
  call BINARY_FILE_OPEN(iunit,trim(stl_file),"r",ierr)
  
  ! Read sizes
  call BINARY_FILE_READ(iunit,cbuff,80,kind(cbuff),ierr)
  call BINARY_FILE_READ(iunit,nt,1,kind(nt),ierr)
  if (irank.eq.iroot) print*,'STL name :',trim(cbuff)
  if (irank.eq.iroot) print*,'# of triangles :',nt
  
  ! Read triangles
  allocate(t(nt))
  do n=1,nt
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%norm(1)=real(fltbuf,WP)
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%norm(2)=real(fltbuf,WP)
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%norm(3)=real(fltbuf,WP)
     
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%v1(1)=real(fltbuf,WP)
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%v1(2)=real(fltbuf,WP)
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%v1(3)=real(fltbuf,WP)
     
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%v2(1)=real(fltbuf,WP)
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%v2(2)=real(fltbuf,WP)
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%v2(3)=real(fltbuf,WP)
     
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%v3(1)=real(fltbuf,WP)
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%v3(2)=real(fltbuf,WP)
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%v3(3)=real(fltbuf,WP)
          
     call BINARY_FILE_READ(iunit,padding,2,kind(padding),ierr)
  end do
  
  ! Output extents
  xmin=+huge(1.0_WP);ymin=+huge(1.0_WP);zmin=+huge(1.0_WP)
  xmax=-huge(1.0_WP);ymax=-huge(1.0_WP);zmax=-huge(1.0_WP)
  do n=1,nt
     if (t(n)%v1(1).lt.xmin) xmin=t(n)%v1(1)
     if (t(n)%v2(1).lt.xmin) xmin=t(n)%v2(1)
     if (t(n)%v3(1).lt.xmin) xmin=t(n)%v3(1)
     
     if (t(n)%v1(2).lt.ymin) ymin=t(n)%v1(2)
     if (t(n)%v2(2).lt.ymin) ymin=t(n)%v2(2)
     if (t(n)%v3(2).lt.ymin) ymin=t(n)%v3(2)
     
     if (t(n)%v1(3).lt.zmin) zmin=t(n)%v1(3)
     if (t(n)%v2(3).lt.zmin) zmin=t(n)%v2(3)
     if (t(n)%v3(3).lt.zmin) zmin=t(n)%v3(3)
     
     if (t(n)%v1(1).gt.xmax) xmax=t(n)%v1(1)
     if (t(n)%v2(1).gt.xmax) xmax=t(n)%v2(1)
     if (t(n)%v3(1).gt.xmax) xmax=t(n)%v3(1)
     
     if (t(n)%v1(2).gt.ymax) ymax=t(n)%v1(2)
     if (t(n)%v2(2).gt.ymax) ymax=t(n)%v2(2)
     if (t(n)%v3(2).gt.ymax) ymax=t(n)%v3(2)
     
     if (t(n)%v1(3).gt.zmax) zmax=t(n)%v1(3)
     if (t(n)%v2(3).gt.zmax) zmax=t(n)%v2(3)
     if (t(n)%v3(3).gt.zmax) zmax=t(n)%v3(3)
  end do
  if (irank.eq.iroot) print*,'Original extents - x :',xmin,'-->',xmax
  if (irank.eq.iroot) print*,'Original extents - y :',ymin,'-->',ymax
  if (irank.eq.iroot) print*,'Original extents - z :',zmin,'-->',zmax
  
  ! Read geometric transform info
  call parser_read('Rescale',scaling)
  call parser_read('Translate',shift)
  
  ! Transform the STL geometry
  if (irank.eq.iroot) print*,'Rescaling and translating...'
  do n=1,nt
     t(n)%v1=t(n)%v1*scaling+shift
     t(n)%v2=t(n)%v2*scaling+shift
     t(n)%v3=t(n)%v3*scaling+shift
     t(n)%norm=normalize(t(n)%norm*scaling)
  end do
     
  ! Direction swapping: x->y
  call parser_read('Swap x->y',swap_xy,.false.)
  call parser_read('Swap y->x',swap_yx,.false.)
  if (swap_xy.or.swap_yx) then
     if (irank.eq.iroot) print*,'Swapping x <=> y...'
     do n=1,nt
        tmp=t(n)%v1(1);t(n)%v1(1)=t(n)%v1(2);t(n)%v1(2)=tmp
        tmp=t(n)%v2(1);t(n)%v2(1)=t(n)%v2(2);t(n)%v2(2)=tmp
        tmp=t(n)%v3(1);t(n)%v3(1)=t(n)%v3(2);t(n)%v3(2)=tmp
        tmp=t(n)%norm(1);t(n)%norm(1)=t(n)%norm(2);t(n)%norm(2)=tmp
     end do
  end if
  
  ! Direction swapping: y->z
  call parser_read('Swap y->z',swap_yz,.false.)
  call parser_read('Swap z->y',swap_zy,.false.)
  if (swap_yz.or.swap_zy) then
     if (irank.eq.iroot) print*,'Swapping y <=> z...'
     do n=1,nt 
        tmp=t(n)%v1(2);t(n)%v1(2)=t(n)%v1(3);t(n)%v1(3)=tmp
        tmp=t(n)%v2(2);t(n)%v2(2)=t(n)%v2(3);t(n)%v2(3)=tmp
        tmp=t(n)%v3(2);t(n)%v3(2)=t(n)%v3(3);t(n)%v3(3)=tmp
        tmp=t(n)%norm(2);t(n)%norm(2)=t(n)%norm(3);t(n)%norm(3)=tmp
     end do
  end if
  
  ! Direction swapping: z->x
  call parser_read('Swap x->z',swap_xz,.false.)
  call parser_read('Swap z->x',swap_zx,.false.)
  if (swap_xz.or.swap_zx) then
     if (irank.eq.iroot) print*,'Swapping z <=> x...'
     do n=1,nt
        tmp=t(n)%v1(1);t(n)%v1(1)=t(n)%v1(3);t(n)%v1(3)=tmp
        tmp=t(n)%v2(1);t(n)%v2(1)=t(n)%v2(3);t(n)%v2(3)=tmp
        tmp=t(n)%v3(1);t(n)%v3(1)=t(n)%v3(3);t(n)%v3(3)=tmp
        tmp=t(n)%norm(1);t(n)%norm(1)=t(n)%norm(3);t(n)%norm(3)=tmp
     end do
  end if
  
  ! Output extents
  xmin=+huge(1.0_WP);ymin=+huge(1.0_WP);zmin=+huge(1.0_WP)
  xmax=-huge(1.0_WP);ymax=-huge(1.0_WP);zmax=-huge(1.0_WP)
  do n=1,nt
     if (t(n)%v1(1).lt.xmin) xmin=t(n)%v1(1)
     if (t(n)%v2(1).lt.xmin) xmin=t(n)%v2(1)
     if (t(n)%v3(1).lt.xmin) xmin=t(n)%v3(1)
     
     if (t(n)%v1(2).lt.ymin) ymin=t(n)%v1(2)
     if (t(n)%v2(2).lt.ymin) ymin=t(n)%v2(2)
     if (t(n)%v3(2).lt.ymin) ymin=t(n)%v3(2)
     
     if (t(n)%v1(3).lt.zmin) zmin=t(n)%v1(3)
     if (t(n)%v2(3).lt.zmin) zmin=t(n)%v2(3)
     if (t(n)%v3(3).lt.zmin) zmin=t(n)%v3(3)
     
     if (t(n)%v1(1).gt.xmax) xmax=t(n)%v1(1)
     if (t(n)%v2(1).gt.xmax) xmax=t(n)%v2(1)
     if (t(n)%v3(1).gt.xmax) xmax=t(n)%v3(1)
     
     if (t(n)%v1(2).gt.ymax) ymax=t(n)%v1(2)
     if (t(n)%v2(2).gt.ymax) ymax=t(n)%v2(2)
     if (t(n)%v3(2).gt.ymax) ymax=t(n)%v3(2)
     
     if (t(n)%v1(3).gt.zmax) zmax=t(n)%v1(3)
     if (t(n)%v2(3).gt.zmax) zmax=t(n)%v2(3)
     if (t(n)%v3(3).gt.zmax) zmax=t(n)%v3(3)
  end do
  if (irank.eq.iroot) print*,'New extents - x :',xmin,'-->',xmax
  if (irank.eq.iroot) print*,'New extents - y :',ymin,'-->',ymax
  if (irank.eq.iroot) print*,'New extents - z :',zmin,'-->',zmax
  
  ! Check if swap distance fucntion sign
  call parser_read('Switch sign',sign_switch,+1.0_WP)
  
  ! Close the file
  call BINARY_FILE_CLOSE(iunit,ierr)
  ! ============================================================
  
  ! ============================================================
  ! Read config file name
  call parser_readchar('Config file',cfg_file)
  
  ! Open the config file to read
  if (irank.eq.iroot) print *,'Opening the config file: ',cfg_file
  call BINARY_FILE_OPEN(iunit,trim(cfg_file),"r",ierr)
  
  ! Read sizes
  call BINARY_FILE_READ(iunit,config,str_medium,kind(config),ierr)
  call BINARY_FILE_READ(iunit,icyl,1,kind(icyl),ierr)
  call BINARY_FILE_READ(iunit,xper,1,kind(xper),ierr)
  call BINARY_FILE_READ(iunit,yper,1,kind(yper),ierr)
  call BINARY_FILE_READ(iunit,zper,1,kind(zper),ierr)
  call BINARY_FILE_READ(iunit,nx,1,kind(nx),ierr)
  call BINARY_FILE_READ(iunit,ny,1,kind(ny),ierr)
  call BINARY_FILE_READ(iunit,nz,1,kind(nz),ierr)
  if (irank.eq.iroot) print*,'Config : ',config
  if (irank.eq.iroot) print*,'Grid :',nx,'x',ny,'x',nz
  
  ! Read grid field
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(mask(nx,ny))
  call BINARY_FILE_READ(iunit,x,nx+1,kind(x),ierr)
  call BINARY_FILE_READ(iunit,y,ny+1,kind(y),ierr)
  call BINARY_FILE_READ(iunit,z,nz+1,kind(z),ierr)
  call BINARY_FILE_READ(iunit,mask,nx*ny,kind(mask),ierr)
  if (irank.eq.iroot) print*,'Mesh extents - x :',x(1),'-->',x(nx+1)
  if (irank.eq.iroot) print*,'Mesh extents - y :',y(1),'-->',y(ny+1)
  if (irank.eq.iroot) print*,'Mesh extents - z :',z(1),'-->',z(nz+1)
  
  ! Close the file
  call BINARY_FILE_CLOSE(iunit,ierr)
  ! ============================================================
  
  ! ============================================================
  ! Project mesh onto triangles

  if (nz.lt.nproc) then
     stop 'nz must be larger than # procs'
  end if

  ! Local number of points  
  nz_=nz/nproc
  rem=mod(nz,nproc)
  if (irank.le.rem) nz_=nz_+1 
  kmin_=kmin+(irank-1)*(nz/nproc)+min(irank-1,rem)
  kmax_=kmin_+nz_-1
  
  ! Allocate distance array
  allocate(Gib(1:nx,1:ny,kmin_:kmax_))
  Gib=0.0_WP
  
  ! Prepare counter
  prog=0
  togo=nx*ny*nz_
  iratio_old=-1
  
  ! Compute projection
  if (irank.eq.iroot) print*
  if (irank.eq.iroot) print*,'Computing distance to triangles...'
  do k=kmin_,kmax_
     do j=1,ny
        do i=1,nx
           
           ! Prepare projections
           count=0
           mydist=huge(1.0_WP)
           
           ! Get centroid information
           c=(/0.5_WP*(x(i)+x(i+1)),0.5_WP*(y(j)+y(j+1)),0.5_WP*(z(k)+z(k+1))/)
           
           ! Loop over triangles and check distance
           do n=1,nt

              ! Normalize triangle normal
              tmp=sqrt(dot_product(t(n)%norm,t(n)%norm))
              t(n)%norm=t(n)%norm/tmp

              call triproj(c,t(n)%v1,t(n)%v2,t(n)%v3,newproj)
              newdist=sqrt(dot_product(c-newproj,c-newproj))
              
              ! Check point
              if (newdist.lt.mydist-eps) then
                 ! new closest point
                 mydist=newdist
                 myproj=newproj
                 mynorm=t(n)%norm
              else if (newdist.lt.mydist+eps) then
                 ! Choose better normal
                 if ( sqrt(abs(dot_product(t(n)%norm,(newproj-c)/sqrt(dot_product(newproj-c,newproj-c))))) .gt. &
                      sqrt(abs(dot_product(mynorm,   (myproj -c)/sqrt(dot_product(myproj -c,myproj -c))))) ) then
                    mynorm=t(n)%norm
                    myproj=newproj
                    mydist=newdist
                 end if
              end if

           end do
           
           ! Postprocess distance
           Gib(i,j,k)=mydist

           ! Get sign based on normal
           tmp=dot_product(myproj-c,mynorm)
           if (tmp.gt.0.0_WP) Gib(i,j,k)=-Gib(i,j,k)
           Gib(i,j,k)=sign_switch*Gib(i,j,k)
           
           ! Add point to counter
           if (irank.eq.iroot) then
              prog=prog+1
              iratio_new=int(real(prog,WP)/real(togo,WP)*100.0_WP)
              if (iratio_new.gt.iratio_old) then
                 iratio_old=iratio_new
                 write(*,'(i3,x,a1)') iratio_new,'%'
              end if
           end if
           
        end do
     end do
  end do

  ! ============================================================
  ! Open the file to write
  call parser_readchar('Optdata file',optdata)
  inquire(file=optdata,exist=file_is_there)
  if (file_is_there .and. irank.eq.iroot) call MPI_FILE_DELETE(optdata,mpi_info,ierr)
  call MPI_FILE_OPEN(comm,optdata,IOR(MPI_MODE_WRONLY,MPI_MODE_CREATE),mpi_info,ifile,ierr)
  
  ! Write header
  if (irank.eq.iroot) then
     ! Write dimensions
     call MPI_FILE_WRITE(ifile,nx ,1,MPI_INTEGER,status,ierr)
     call MPI_FILE_WRITE(ifile,ny ,1,MPI_INTEGER,status,ierr)
     call MPI_FILE_WRITE(ifile,nz ,1,MPI_INTEGER,status,ierr)
     nod=1
     call MPI_FILE_WRITE(ifile,nod,1,MPI_INTEGER,status,ierr)
     ! Write additional stuff
     dt=0.0_WP; time=0.0_WP
     call MPI_FILE_WRITE(ifile,dt,1,MPI_REAL_WP,status,ierr)
     call MPI_FILE_WRITE(ifile,time,1,MPI_REAL_WP,status,ierr)
     ! Write variable names
     ODname='Gib'
     call MPI_FILE_WRITE(ifile,ODname,str_short,MPI_CHARACTER,status,ierr)
  end if
  
  ! Size of local arrays
  data_size = nx*ny*nz_
  
  ! Resize some integers so MPI can read even the biggest files
  WP_MOK    = int(WP,          MPI_Offset_kind)
  str_MOK   = int(str_short,   MPI_Offset_kind)
  
  ! Write the data
  ! Define global(g) and local(l) sizes
  gsizes(1) = nx
  gsizes(2) = ny
  gsizes(3) = nz
  lsizes(1) = nx
  lsizes(2) = ny
  lsizes(3) = nz_
  ! Define starting points
  start(1) = imin_-imin
  start(2) = jmin_-jmin
  start(3) = kmin_-kmin
  ! Create the view
  call MPI_TYPE_CREATE_SUBARRAY(3,gsizes,lsizes,start,MPI_ORDER_FORTRAN,MPI_REAL_WP,view,ierr)
  call MPI_TYPE_COMMIT(view,ierr)
  disp = 4*4 + str_MOK + 2*WP_MOK
  call MPI_FILE_SET_VIEW(ifile,disp,MPI_REAL_WP,view,"native",mpi_info,ierr)
  call MPI_FILE_WRITE_ALL(ifile,Gib,data_size,MPI_REAL_WP,status,ierr)
  
  ! Close the file
  call MPI_FILE_CLOSE(ifile,ierr)

  ! ============================================================

  ! Finalize parallel environment
  call stlreader_parallel_final

end program stlreader
