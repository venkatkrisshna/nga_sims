module stlreader_parallel
  implicit none
  
  integer :: nproc,irank,iroot
  integer :: mpi_info
  integer :: comm, comm_1D
  integer :: MPI_REAL_WP,MPI_REAL_SP
  include 'mpif.h'
  
contains 
  
  !=========================!
  !        MPI Kill         !
  !=========================!
  subroutine parallel_kill(error_text)
    implicit none
    integer :: ierr
    character(len=*), intent(in), optional :: error_text
    
    ! Specify who sends the abort signal and what it means
    if (present(error_text)) then
       write(*,'(a,i3,a,/,a)') '[',irank,'] initiated general abort signal due to the following error:',trim(error_text)
    else
       write(*,'(a,i3,a)') '[',irank,'] initiated general abort signal due to an unknown error'
    end if
    
    ! Call general abort
    call MPI_ABORT(MPI_COMM_WORLD,0,ierr)
    
    return
  end subroutine parallel_kill
  
end module stlreader_parallel

! ====================== !
!     Initialization     !
! ====================== !
subroutine stlreader_parallel_init
  use stlreader_parallel
  use parser
  use string
  implicit none
  
  character(len=str_short) :: mpiiofs
  integer :: ierr
  integer :: size_real,size_dp
  
  ! Initialize a first basic MPI environment
  call MPI_INIT(ierr)
  
  ! Duplicate communicator
  call MPI_COMM_DUP(MPI_COMM_WORLD,comm,ierr)
  
  ! Get size and rank
  call MPI_COMM_RANK(comm,irank,ierr)
  call MPI_COMM_SIZE(comm,nproc,ierr) 
  irank = irank + 1
  iroot = 1
  
  ! Set MPI working precision - WP
  call MPI_TYPE_SIZE(MPI_REAL,size_real,ierr)
  call MPI_TYPE_SIZE(MPI_DOUBLE_PRECISION,size_dp,ierr)
  if (WP .eq. size_real) then
     MPI_REAL_WP = MPI_REAL
  else if (WP .eq. size_dp) then
     MPI_REAL_WP = MPI_DOUBLE_PRECISION
  else
     call parallel_kill('Error in parallel_init: no WP equivalent in MPI')
  end if
  
  ! Set MPI single precision
  call MPI_TYPE_SIZE(MPI_REAL,size_real,ierr)
  call MPI_TYPE_SIZE(MPI_DOUBLE_PRECISION,size_dp,ierr)
  if (SP .eq. size_real) then
     MPI_REAL_SP = MPI_REAL
  else if (SP .eq. size_dp) then
     MPI_REAL_SP = MPI_DOUBLE_PRECISION
  else
     call parallel_kill('Error in parallel_init: no SP equivalent in MPI')
  end if
  
  ! MPII/O hints
  mpiiofs = "lustre:"
  call MPI_INFO_CREATE(mpi_info,ierr)
  call MPI_INFO_SET(mpi_info,"romio_ds_write","disable",ierr)
  
  return
end subroutine stlreader_parallel_init

!=========================!
!         Finalize        !
!=========================!
subroutine stlreader_parallel_final
  use stlreader_parallel
  implicit none
  integer :: ierr
  
  ! Finalize parallel environment
  call MPI_FINALIZE(ierr)
  
  return
end subroutine stlreader_parallel_final






