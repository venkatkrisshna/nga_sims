program part2inflow
  use precision
  use string
  use fileio
  use lpt_mod
  use quicksort
  implicit none
  
  integer :: ipart,punit,iunit,ierr
  character(len=str_medium) :: pfile,ifile
  real(WP) :: dt,time,inflow_time
  integer :: part_size
  real(WP), dimension(:), allocatable :: x
  integer, dimension(:), allocatable :: i
  real(WP) :: umax,umin
  
  ! Read file name from standard input
  print*,'============================================'
  print*,'| ARTS - particle file to inflow converter |'
  print*,'============================================'
  print*
  print "(a,$)", " part file : "
  read "(a)", pfile
  print "(a,$)", " part inflow file : "
  read "(a)", ifile
  
  ! Part file ----------------------------------------------------
  call BINARY_FILE_OPEN(punit,trim(pfile),"r",ierr)
  call BINARY_FILE_READ(punit,npart,1,kind(npart),ierr)
  call BINARY_FILE_READ(punit,part_size,1,kind(part_size),ierr)
  call BINARY_FILE_READ(punit,dt,1,kind(dt),ierr)
  call BINARY_FILE_READ(punit,time,1,kind(time),ierr)
  if (part_size.ne.part_kind) stop "Particle type unreadable"
  allocate(part(npart))
  do ipart=1,npart
     call BINARY_FILE_READ(punit,part(ipart),1,part_size,ierr)
  end do
  call BINARY_FILE_CLOSE(punit,ierr)
  
  ! Sort the drops based on x position
  allocate(x(npart))
  allocate(i(npart))
  umin = +huge(1.0_WP)
  umax = -huge(1.0_WP)
  do ipart=1,npart
     x(ipart) = part(ipart)%x
     i(ipart) = ipart
     umin = min(part(ipart)%u,umin)
     umax = max(part(ipart)%u,umax)
  end do
  print*,'Maximum u velocity: Umax =',umax
  print*,'Minimum u velocity: Umin =',umin
  call quick_sort(x,i)
  
  ! Inflow file --------------------------------------------------
  inflow_time = x(npart)-x(1)
  print*,'Predicted x-extent of domain =',inflow_time
  print "(a18,$)", " exact x-extent : "
  read (*,*) inflow_time
  call BINARY_FILE_OPEN(iunit,trim(ifile),"w",ierr)
  call BINARY_FILE_WRITE(iunit,npart,1,kind(npart),ierr)
  call BINARY_FILE_WRITE(iunit,part_kind,1,kind(part_kind),ierr)
  call BINARY_FILE_WRITE(iunit,inflow_time,1,kind(inflow_time),ierr)
  do ipart=1,npart
     call BINARY_FILE_WRITE(iunit,part(i(ipart)),1,part_kind,ierr)
  end do
  call BINARY_FILE_CLOSE(iunit,ierr)
  
end program part2inflow
