! ============================== !
! Computes the multiphase errors !
! between two data files         !
! ============================== !
program multiphaseError
  use precision
  use string
  use fileio
  implicit none

  real(WP) :: shapeErr,boundErr,massErr
  real(WP) :: mass1,mass2
  integer :: i,j,k
  integer :: nx,ny,nz,nvar
  character(len=str_short), dimension(:), allocatable :: names1,names2
  real(WP), dimension(:,:,:), allocatable :: data1,data2
  integer :: iunit1,iunit2,ierr,var
  character(len=str_medium) :: configfile,datafile1,datafile2
  character(len=str_medium) :: sim
  real(WP) :: dt,time,vol
  integer :: icyl,xper,yper,zper
  real(WP), dimension(:), allocatable :: x,y,z
  real(WP), dimension(:,:), allocatable :: mask

  ! Read file names from standard input
  print*,'==============================='
  print*,'| Multiphase error calculator |'
  print*,'==============================='
  print*
  print *, " Config file : "
  read "(a)", configfile
  print *, " 1st data file : "
  read "(a)", datafile1
  print *, " 2nd data file : "
  read "(a)", datafile2

  ! Read config file
  call BINARY_FILE_OPEN(iunit1,trim(configfile),"r",ierr)
  call BINARY_FILE_READ(iunit1,sim,str_medium,kind(sim),ierr)
  call BINARY_FILE_READ(iunit1,icyl,1,kind(icyl),ierr)
  call BINARY_FILE_READ(iunit1,xper,1,kind(xper),ierr)
  call BINARY_FILE_READ(iunit1,yper,1,kind(yper),ierr)
  call BINARY_FILE_READ(iunit1,zper,1,kind(zper),ierr)
  call BINARY_FILE_READ(iunit1,nx,1,kind(nx),ierr)
  call BINARY_FILE_READ(iunit1,ny,1,kind(ny),ierr)
  call BINARY_FILE_READ(iunit1,nz,1,kind(nz),ierr)
  print*,'Grid :',nx,'x',ny,'x',nz
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(mask(nx,ny))
  call BINARY_FILE_READ(iunit1,x,nx+1,kind(x),ierr)
  call BINARY_FILE_READ(iunit1,y,ny+1,kind(y),ierr)
  call BINARY_FILE_READ(iunit1,z,nz+1,kind(z),ierr)
  call BINARY_FILE_READ(iunit1,mask,nx*ny,kind(mask),ierr)
  call BINARY_FILE_CLOSE(iunit1,ierr)
  
  ! Open the data files to read
  call BINARY_FILE_OPEN(iunit1,trim(datafile1) ,"r",ierr)
  call BINARY_FILE_OPEN(iunit2,trim(datafile2) ,"r",ierr)
  
  ! Read sizes
  call BINARY_FILE_READ(iunit1,nx,1,kind(nx),ierr)
  call BINARY_FILE_READ(iunit1,ny,1,kind(ny),ierr)
  call BINARY_FILE_READ(iunit1,nz,1,kind(nz),ierr)
  call BINARY_FILE_READ(iunit1,nvar,1,kind(nvar),ierr)
  print*,'Data file 1 Grid :',nx,'x',ny,'x',nz
  call BINARY_FILE_READ(iunit2,nx,1,kind(nx),ierr)
  call BINARY_FILE_READ(iunit2,ny,1,kind(ny),ierr)
  call BINARY_FILE_READ(iunit2,nz,1,kind(nz),ierr)
  call BINARY_FILE_READ(iunit2,nvar,1,kind(nvar),ierr)
  print*,'Data file 2 Grid :',nx,'x',ny,'x',nz
  
  ! Read additional stuff
  call BINARY_FILE_READ(iunit1,dt,1,kind(dt),ierr)
  call BINARY_FILE_READ(iunit1,time,1,kind(time),ierr)
  print*,'Data file 1 at time :',time
  call BINARY_FILE_READ(iunit2,dt,1,kind(dt),ierr)
  call BINARY_FILE_READ(iunit2,time,1,kind(time),ierr)
  print*,'Data file 2 at time :',time
  
  ! Read variable names
  allocate(names1(nvar))
  allocate(names2(nvar))
  do var=1,nvar
     call BINARY_FILE_READ(iunit1,names1(var),str_short,kind(names1),ierr)
     call BINARY_FILE_READ(iunit2,names2(var),str_short,kind(names2),ierr)
  end do
  print*,'Variables in data file 1 : ',names1
  print*,'Variables in data file 2 : ',names1

  ! Allocate arrays
  allocate(data1(nx,ny,nz))
  allocate(data2(nx,ny,nz))

  ! Initialize errors
  mass1=0.0_WP
  mass2=0.0_WP
  shapeErr=0.0_WP
  boundErr=0.0_WP
  
  ! Read data and compute errors for VOF variables
  do var=1,nvar
     call BINARY_FILE_READ(iunit1,data1,nx*ny*nz,kind(data1),ierr)
     call BINARY_FILE_READ(iunit2,data2,nx*ny*nz,kind(data2),ierr)
     ! Check if VOF
     if (index(names1(var),'VOF').gt.0) then ! VOF variable
        ! Check for 2D vs 3D
        if (min(nx,ny,nz).gt.1) then ! 3D
           do k=1,nz
              do j=1,ny
                 do i=1,nx
                    ! Volume of subcell
                    vol=0.125_WP*(x(i+1)-x(i))*(y(j+1)-y(j))*(z(k+1)-z(k))
                    mass1=mass1+vol*data1(i,j,k)
                    mass2=mass2+vol*data2(i,j,k)
                    boundErr=max(boundErr,max(-vol*data2(i,j,k),vol*(data2(i,j,k)-1)))
                    shapeErr=shapeErr+vol*abs(data1(i,j,k)-data2(i,j,k))
                 end do
              end do
           end do
        else ! 2D
           if (nz.ne.1) stop '2D on xz or yz planes not implemented'
           if ( trim(names1(var)).eq.'VOF1' .or. &
                trim(names1(var)).eq.'VOF2' .or. &
                trim(names1(var)).eq.'VOF3' .or. &
                trim(names1(var)).eq.'VOF4' ) then
              do j=1,ny
                 do i=1,nx
                    ! Volume of subcell
                    vol=0.25_WP*(x(i+1)-x(i))*(y(j+1)-y(j))
                    mass1=mass1+vol*data1(i,j,1)
                    mass2=mass2+vol*data2(i,j,1)
                    boundErr=max(boundErr,max(-vol*data2(i,j,1),vol*(data2(i,j,1)-1)))
                    shapeErr=shapeErr+vol*abs(data1(i,j,1)-data2(i,j,1))
                 end do
              end do
           end if
        end if
     end if
  end do
  massErr=mass2-mass1
  
  ! Close the data files
  call BINARY_FILE_CLOSE(iunit1,ierr)
  call BINARY_FILE_CLOSE(iunit2,ierr)

  ! Display errors
  print *,'Shape error = ',shapeErr
  print *,'Mass  error = ',massErr
  print *,'Bound error = ',boundErr
  
end program multiphaseError
