program inflow2visit
  use precision
  use string
  use fileio 
  implicit none

  include "silo_f9x.inc"

  ! Data array with all the variables (velocity,pressure,...)
  integer :: n,j,k,icyl
  integer :: nt,ny,nz,nvar
  character(len=str_short), dimension(:), allocatable :: names
  real(WP), dimension(:,:,:,:), allocatable :: data
  integer :: iunit,iunit2,ierr,ibuffer,var
  character(len=str_medium) :: filename1,filename2,directory
  real(WP) :: dt,time
  real(WP) :: Lx,Ly,Lz
  real(WP), dimension(:), allocatable :: t,y,z

  ! Silo
  integer :: dbfile, optlistid
  
  ! Read file name from standard input
  print*,'===================================='
  print*,'| ARTS - inflow to visit converter |'
  print*,'===================================='
  print*
  print *, " inflow file : "
  read "(a)", filename1
  print *, " visit file (e.g. inflow.silo) : "
  read "(a)", filename2

  ! Open the data file to read
  call BINARY_FILE_OPEN(iunit,trim(filename1),"r",ierr)
  
  ! Read sizes
  call BINARY_FILE_READ(iunit,nt,1,kind(nt),ierr)
  call BINARY_FILE_READ(iunit,ny,1,kind(ny),ierr)
  call BINARY_FILE_READ(iunit,nz,1,kind(nz),ierr)
  call BINARY_FILE_READ(iunit,nvar,1,kind(nvar),ierr)
  print*,'Grid size : nt x ny x nz =',nt,'x',ny,'x',nz
  
  ! Read time infor
  allocate(t(nt+1))
  call BINARY_FILE_READ(iunit,dt,1,kind(dt),ierr)
  call BINARY_FILE_READ(iunit,time,1,kind(time),ierr)
  do n=1,nt+1
     t(n)=real(n-1,WP)*dt
  end do
  
  ! Read variable names
  allocate(names(nvar))
  do var=1,nvar
     call BINARY_FILE_READ(iunit,names(var),str_short,kind(names),ierr)
  end do
  print*,'Variables : ',names

  ! Read the grid
  allocate(y(ny+1))
  allocate(z(nz+1))
  call BINARY_FILE_READ(iunit,icyl,1,kind(icyl),ierr)
  call BINARY_FILE_READ(iunit,y,ny+1,kind(y),ierr)
  call BINARY_FILE_READ(iunit,z,nz+1,kind(z),ierr)
  
  ! Allocate data buffer
  allocate(data(nt,ny,nz,nvar))

  ! Read data
  print *,'Reading data'
  do n=1,nt
     print *,n,' out of ',nt
     do var=1,nvar
        call BINARY_FILE_READ(iunit,data(n,:,:,var),ny*nz,kind(data),ierr)
     end do
  end do

  ! Close data file
  call BINARY_FILE_CLOSE(iunit,ierr)


  ! Create silo database
  ierr=dbcreate(filename2,len_trim(filename2),DB_CLOBBER,DB_LOCAL, &
       "Silo database created with NGA",30,DB_HDF5,dbfile)
  if(dbfile.eq.-1) stop 'Could not create Silo file!'
     
  ! Write mesh and time info
  ierr = dbmkoptlist(2, optlistid)
  ierr = dbaddiopt(optlistid, DBOPT_CYCLE, 0)
  ierr = dbadddopt(optlistid, DBOPT_DTIME, time)
  ierr = dbputqm (dbfile,"Mesh",4,"xm",2,"ym",2,"zm",2,real(t,SP),real(y,SP),real(z,SP), &
       (/nt+1,ny+1,nz+1/),3,DB_FLOAT, DB_COLLINEAR, optlistid, ierr)
  ierr = dbfreeoptlist(optlistid)
   
  ! For each variable, read it then write the ensight data file
  do var=1,nvar
     ! Read the variable
     print*,'Writing ',names(var)
     ! Write to silo file
     ierr = dbputqv1(dbfile,trim(names(var)),len_trim(names(var)),"Mesh", 4, &
          real(data(:,:,:,var),SP),(/nt,ny,nz/),3,DB_F77NULL,0,DB_FLOAT,DB_ZONECENT,DB_F77NULL,ierr)
  end do

  ! Close silo database
  ierr = dbclose(dbfile)
    
end program inflow2visit
