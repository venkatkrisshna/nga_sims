program data2inflow
  use precision
  use string
  use fileio
  use parser
  use cli_reader
  implicit none

  character(len=str_medium) :: filename1,filename2,filename3
  character(len=str_medium) :: config
  integer  :: iunit1,iunit2,iunit3,ierr
  integer  :: nx,ny,nz,nvar
  integer  :: xper,yper,zper,icyl
  real(WP) :: U0,dt,time
  integer  :: var,i
  real(WP), dimension(:),       allocatable :: x,y,z
  integer,  dimension(:,:),     allocatable :: mask
  real(WP), dimension(:,:,:,:), allocatable :: data
  character(len=str_short), dimension(:), allocatable :: names
  
  
  ! Read file name from standard input
  print*,'=================================='
  print*,'| ARTS - data to inflow converter |'
  print*,'=================================='
  print*
  print "(a15,$)", " config file : "
  read "(a)", filename1
  print "(a13,$)", " data file : "
  read "(a)", filename2
  print "(a15,$)", " inflow file : "
  read "(a)", filename3
  print "(a23,$)", " convective velocity : "
  read (*,*) U0

  ! ** Open the config file to read **
  call BINARY_FILE_OPEN(iunit1,trim(filename1),"r",ierr)

  ! Read the header
  call BINARY_FILE_READ(iunit1,config,str_medium,kind(config),ierr)
  call BINARY_FILE_READ(iunit1,icyl,1,kind(icyl),ierr)
  call BINARY_FILE_READ(iunit1,xper,1,kind(xper),ierr)
  call BINARY_FILE_READ(iunit1,yper,1,kind(yper),ierr)
  call BINARY_FILE_READ(iunit1,zper,1,kind(zper),ierr)
  call BINARY_FILE_READ(iunit1,nx,1,kind(nx),ierr)
  call BINARY_FILE_READ(iunit1,ny,1,kind(ny),ierr)
  call BINARY_FILE_READ(iunit1,nz,1,kind(nz),ierr)
  print*,'Config :',nx,'x',ny,'x',nz
  
  ! Read the grid
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(mask(nx,ny))
  call BINARY_FILE_READ(iunit1,x,nx+1,kind(x),ierr)
  call BINARY_FILE_READ(iunit1,y,ny+1,kind(y),ierr)
  call BINARY_FILE_READ(iunit1,z,nz+1,kind(z),ierr)
  call BINARY_FILE_READ(iunit1,mask,nx*ny,kind(mask),ierr)
  
  ! Close the config file
  call BINARY_FILE_CLOSE(iunit1,ierr)
  
  
  ! ** Open the data file to read **
  call BINARY_FILE_OPEN(iunit2,trim(filename2),"r",ierr)
  
  ! Read the header
  call BINARY_FILE_READ(iunit2,nx,1,kind(nx),ierr)
  call BINARY_FILE_READ(iunit2,ny,1,kind(ny),ierr)
  call BINARY_FILE_READ(iunit2,nz,1,kind(nz),ierr)
  call BINARY_FILE_READ(iunit2,nvar,1,kind(nvar),ierr)
  call BINARY_FILE_READ(iunit2,dt,1,kind(dt),ierr)
  call BINARY_FILE_READ(iunit2,time,1,kind(time),ierr)
  print*,'Data :  ',nx,'x',ny,'x',nz
  
  ! Read variable names
  allocate(names(nvar))
  do var=1,nvar
     call BINARY_FILE_READ(iunit2,names(var),str_short,kind(names),ierr)
  end do
  print*,'Variables : ',names
  
  ! Allocate arrays
  allocate(data(nx,ny,nz,nvar))
  
  
  ! ** Open the inflow file to write **
  call BINARY_FILE_OPEN(iunit3,trim(filename3),"w",ierr)
  
  ! Write sizes
  time = (x(nx+1)-x(1))/U0
  dt = time/real(nx,WP)
  print*,'time',time
  print*,'dt',dt
  call BINARY_FILE_WRITE(iunit3,nx,1,kind(nx),ierr)
  call BINARY_FILE_WRITE(iunit3,ny,1,kind(ny),ierr)
  call BINARY_FILE_WRITE(iunit3,nz,1,kind(nz),ierr)
  call BINARY_FILE_WRITE(iunit3,nvar,1,kind(nvar),ierr)
  call BINARY_FILE_WRITE(iunit3,dt,1,kind(dt),ierr)
  call BINARY_FILE_WRITE(iunit3,time,1,kind(time),ierr)
  do var=1,nvar
     call BINARY_FILE_WRITE(iunit3,names(var),str_short,kind(names),ierr)
  end do
  call BINARY_FILE_WRITE(iunit3,icyl,1,kind(icyl),ierr)
  call BINARY_FILE_WRITE(iunit3,y,ny+1,kind(y),ierr)
  call BINARY_FILE_WRITE(iunit3,z,nz+1,kind(z),ierr)
  
  
  ! Read data field
  do var=1,nvar
     ! Read 
     call BINARY_FILE_READ (iunit2,data(:,:,:,var),nx*ny*nz,kind(data),ierr)
     ! Add convective velocity
     if (trim(adjustl(names(var))).eq.'U') data(:,:,:,var) = data(:,:,:,var) + U0
     print*,trim(adjustl(names(var))),minval(data(:,:,:,var)),maxval(data(:,:,:,var))
  end do
  
  ! Write inflow field
  do i=1,nx
     do var=1,nvar
        call BINARY_FILE_WRITE(iunit3,data(i,:,:,var),ny*nz,kind(data),ierr)
     end do
  end do
  
  ! Close the files
  call BINARY_FILE_CLOSE(iunit2,ierr)
  call BINARY_FILE_CLOSE(iunit3,ierr)
  

end program data2inflow
