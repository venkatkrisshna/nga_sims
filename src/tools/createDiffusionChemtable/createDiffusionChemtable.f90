program createDiffusionChemtable
  use diffusion_table
  use parser
  use precision
  use string
  implicit none

  integer :: n
  
  ! Filename of input file
  character(len=str_medium) :: name

  ! -------------------------------------------------------

  ! Parse the input file
  call get_command_argument(1,name)
  call parser_init
  call parser_parsefile(name)
  
  ! Read the list of files
  call parser_getsize("List of Flamelets", nfiles)
  allocate(files(nfiles))
  call parser_read("List of Flamelets", files)
  
  ! Initialize the modules
  call diffusion_flamelet_init
  call diffusion_table_init

  ! -------------------------------------------------------

  print*,''
  print*,'** Files in the table **'
  do n=1,nfiles
     write(*,'(a)') trim(files(n))
     ! Read the file
     call diffusion_flamelet_readfile(files(n))
     ! Convolute with PDF
     call diffusion_table_convolute(n)
     ! Deallocate the data array
     call diffusion_flamelet_cleanup
  end do
  
  ! Change the variables names
  call diffusion_table_convert_names
  ! Compute the table
  call diffusion_table_setup
  ! Print some statistics
  call diffusion_table_stats
  ! Write the table
  call diffusion_table_write

end program createDiffusionChemtable
