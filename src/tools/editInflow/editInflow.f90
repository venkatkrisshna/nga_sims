program editInflow
  use precision
  use string
  use fileio
  implicit none

  ! Data array with all the variables (velocity,pressure,...)
  integer :: n,nn,ntime,n1,n2,nvar,icyl
  character(len=str_short), dimension(:), pointer :: names
  real(WP), dimension(:,:), pointer :: data
  real(WP), dimension(:), pointer :: x1, x2
  integer :: iunit1,iunit2,ierr,var,choice
  character(len=str_medium) :: filename1,filename2
  character(len=str_short) :: varname
  real(WP) :: dt,time,varvalue
  real(WP) :: D

  ! For case (8)
  ! Array to hold the most positive number (mpn) for U,V,W
  real(WP), dimension(:), allocatable :: mpn
  ! Array to hold the most negative number (mnn) for U,V,W
  real(WP), dimension(:), allocatable :: mnn
  ! Array to hold the smallest magnitude number (smn) for U,V,W
  real(WP), dimension(:), allocatable :: smn
  ! Array to hold ntime values for the returned information
  integer, dimension(:), allocatable :: ntime_mpn, ntime_mnn, ntime_smn
  ! Counter to loop through data array
  integer :: j,k

  ! Read file name from standard input or command line
  print*,'========================'
  print*,'| ARTS - Inflow Editor |'
  print*,'========================'
  print*
  print *, " Inflow file before edition : "
  read "(a)", filename1
  print *, " Inflow file after edition : "
  read "(a)", filename2

  ! ** Open the data file to read **
  call BINARY_FILE_OPEN(iunit1,trim(filename1),"r",ierr)

  ! Read sizes
  call BINARY_FILE_READ(iunit1,ntime,1,kind(ntime),ierr)
  call BINARY_FILE_READ(iunit1,n1,1,kind(n1),ierr)
  call BINARY_FILE_READ(iunit1,n2,1,kind(n2),ierr)
  call BINARY_FILE_READ(iunit1,nvar,1,kind(nvar),ierr)
  print*,'ntime:',ntime,' - Grid :',n1,'x',n2

  ! Read additional stuff
  call BINARY_FILE_READ(iunit1,dt,1,kind(dt),ierr)
  call BINARY_FILE_READ(iunit1,time,1,kind(time),ierr)
  print*,'Data file at time :',time

  ! Read variable names
  allocate(names(nvar))
  do var=1,nvar
     call BINARY_FILE_READ(iunit1,names(var),str_short,kind(names),ierr)
  end do
  print*,'Variables : ',names

  ! Allocate arrays
  allocate(x1(n1+1))
  allocate(x2(n2+1))
  allocate(data(n1,n2))

  ! Read grid
  call BINARY_FILE_READ(iunit1,icyl,1,kind(icyl),ierr)
  call BINARY_FILE_READ(iunit1,x1,n1+1,kind(x1),ierr)
  call BINARY_FILE_READ(iunit1,x2,n2+1,kind(x2),ierr)

  ! ** Ask what to do **
  print*
  print*, " 1. Print Min/Max of the grid"
  print*, " 2. Print Min/Max of variable"
  print*, " 3. Add variable"
  print*, " 4. Delete variable"
  print*, " 5. Reset time"
  print*, " 6. Empty variable list"
  print*, ' 7. Add constant to a variable'
  print*, ' 8. Print Min/Max of variable in entire file'
  print*, ' 9. Rescale inflow'
  print*, '10. Add VOF for jet case'
  print*, 'Choice : '
  read "(i2)", choice
print *,choice
  ! Case dependent operation
  select case(choice)

  case(1) ! Print grid
     print *,'First direction'
     print *,minval(x1),maxval(x1)
     print *,'Second direction'
     print *,minval(x2),maxval(x2)

  case(2) ! Print min/max of all variables
     do n=1,ntime
        print *, '==== n=',n,' ===='
        do var=1,nvar
           call BINARY_FILE_READ(iunit1,data,n1*n2,kind(data),ierr)
           print*,names(var)," min: ",minval(data)," at ",minloc(data)
           print*,names(var)," max: ",maxval(data)," at ",maxloc(data)
        end do
     end do

  case (3) ! Add variable
     print "(a16,$)", "Variable name : "
     read "(a)", varname
     print "(a16,$)", "Default value : "
     read(*,*) varvalue
     call BINARY_FILE_OPEN(iunit2,trim(filename2),"w",ierr)
     call BINARY_FILE_WRITE(iunit2,ntime,1,kind(ntime),ierr)
     call BINARY_FILE_WRITE(iunit2,n1,1,kind(n1),ierr)
     call BINARY_FILE_WRITE(iunit2,n2,1,kind(n2),ierr)
     call BINARY_FILE_WRITE(iunit2,nvar+1,1,kind(nvar),ierr)
     call BINARY_FILE_WRITE(iunit2,dt,1,kind(dt),ierr)
     call BINARY_FILE_WRITE(iunit2,time,1,kind(time),ierr)
     do var=1,nvar
        call BINARY_FILE_WRITE(iunit2,names(var),str_short,kind(names),ierr)
     end do
     call BINARY_FILE_WRITE(iunit2,varname,str_short,kind(varname),ierr)
     call BINARY_FILE_WRITE(iunit2,icyl,1,kind(icyl),ierr)     
     call BINARY_FILE_WRITE(iunit2,x1,n1+1,kind(x1),ierr)
     call BINARY_FILE_WRITE(iunit2,x2,n2+1,kind(x2),ierr)
     do n=1,ntime
        do var=1,nvar
           call BINARY_FILE_READ (iunit1,data,n1*n2,kind(data),ierr)
           call BINARY_FILE_WRITE(iunit2,data,n1*n2,kind(data),ierr)
        end do
        data = varvalue
        call BINARY_FILE_WRITE(iunit2,data,n1*n2,kind(data),ierr)
     end do
     call BINARY_FILE_CLOSE(iunit2,ierr)

  case (4) ! Delete variable
     print "(a16,$)", "Variable name : "
     read "(a)", varname
     call BINARY_FILE_OPEN(iunit2,trim(filename2),"w",ierr)
     call BINARY_FILE_WRITE(iunit2,ntime,1,kind(ntime),ierr)
     call BINARY_FILE_WRITE(iunit2,n1,1,kind(n1),ierr)
     call BINARY_FILE_WRITE(iunit2,n2,1,kind(n2),ierr)
     call BINARY_FILE_WRITE(iunit2,nvar-1,1,kind(nvar),ierr)
     call BINARY_FILE_WRITE(iunit2,dt,1,kind(dt),ierr)
     call BINARY_FILE_WRITE(iunit2,time,1,kind(time),ierr)
     do var=1,nvar
        if (trim(adjustl(names(var))).ne.trim(adjustl(varname))) &
             call BINARY_FILE_WRITE(iunit2,names(var),str_short,kind(names),ierr)
     end do
     call BINARY_FILE_WRITE(iunit2,icyl,1,kind(icyl),ierr)     
     call BINARY_FILE_WRITE(iunit2,x1,n1+1,kind(x1),ierr)
     call BINARY_FILE_WRITE(iunit2,x2,n2+1,kind(x2),ierr)
     do n=1,ntime
        do var=1,nvar
           call BINARY_FILE_READ (iunit1,data,n1*n2,kind(data),ierr)
           if (trim(adjustl(names(var))).ne.trim(adjustl(varname))) &
                call BINARY_FILE_WRITE(iunit2,data,n1*n2,kind(data),ierr)
        end do
     end do
     call BINARY_FILE_CLOSE(iunit2,ierr)

  case (5) ! Reset time to zero
     time = 0.0_WP
     call BINARY_FILE_OPEN(iunit2,trim(filename2),"w",ierr)
     call BINARY_FILE_WRITE(iunit2,ntime,1,kind(ntime),ierr)
     call BINARY_FILE_WRITE(iunit2,n1,1,kind(n1),ierr)
     call BINARY_FILE_WRITE(iunit2,n2,1,kind(n2),ierr)
     call BINARY_FILE_WRITE(iunit2,nvar+1,1,kind(nvar),ierr)
     call BINARY_FILE_WRITE(iunit2,dt,1,kind(dt),ierr)
     call BINARY_FILE_WRITE(iunit2,time,1,kind(time),ierr)
     do var=1,nvar
        call BINARY_FILE_WRITE(iunit2,names(var),str_short,kind(names),ierr)
     end do
     call BINARY_FILE_WRITE(iunit2,icyl,1,kind(icyl),ierr)     
     call BINARY_FILE_WRITE(iunit2,x1,n1+1,kind(x1),ierr)
     call BINARY_FILE_WRITE(iunit2,x2,n2+1,kind(x2),ierr)
     do n=1,ntime
        do var=1,nvar
           call BINARY_FILE_READ (iunit1,data,n1*n2,kind(data),ierr)
           call BINARY_FILE_WRITE(iunit2,data,n1*n2,kind(data),ierr)
        end do
     end do
     call BINARY_FILE_CLOSE(iunit2,ierr)

  case (6) ! Empty variable list
     nvar = 0
     call BINARY_FILE_OPEN(iunit2,trim(filename2),"w",ierr)
     call BINARY_FILE_WRITE(iunit2,ntime,1,kind(ntime),ierr)
     call BINARY_FILE_WRITE(iunit2,n1,1,kind(n1),ierr)
     call BINARY_FILE_WRITE(iunit2,n2,1,kind(n2),ierr)
     call BINARY_FILE_WRITE(iunit2,nvar+1,1,kind(nvar),ierr)
     call BINARY_FILE_WRITE(iunit2,dt,1,kind(dt),ierr)
     call BINARY_FILE_WRITE(iunit2,time,1,kind(time),ierr)
     call BINARY_FILE_WRITE(iunit2,icyl,1,kind(icyl),ierr)     
     call BINARY_FILE_WRITE(iunit2,x1,n1+1,kind(x1),ierr)
     call BINARY_FILE_WRITE(iunit2,x2,n2+1,kind(x2),ierr)

  case (7) ! Add constant to a variable
     print "(a16,$)", "Variable name : "
     read "(a)", varname
     print "(a13,$)", "Added value : "
     read(*,*) varvalue
     call BINARY_FILE_OPEN(iunit2,trim(filename2),"w",ierr)
     call BINARY_FILE_WRITE(iunit2,ntime,1,kind(ntime),ierr)
     call BINARY_FILE_WRITE(iunit2,n1,1,kind(n1),ierr)
     call BINARY_FILE_WRITE(iunit2,n2,1,kind(n2),ierr)
     call BINARY_FILE_WRITE(iunit2,nvar,1,kind(nvar),ierr)
     call BINARY_FILE_WRITE(iunit2,dt,1,kind(dt),ierr)
     call BINARY_FILE_WRITE(iunit2,time,1,kind(time),ierr)
     do var=1,nvar
        call BINARY_FILE_WRITE(iunit2,names(var),str_short,kind(names),ierr)
     end do
     call BINARY_FILE_WRITE(iunit2,icyl,1,kind(icyl),ierr)
     call BINARY_FILE_WRITE(iunit2,x1,n1+1,kind(x1),ierr)
     call BINARY_FILE_WRITE(iunit2,x2,n2+1,kind(x2),ierr)
     do n=1,ntime
        do var=1,nvar
           if(trim(adjustl(names(var))) .ne. trim(adjustl(varname))) then
              call BINARY_FILE_READ (iunit1,data,n1*n2,kind(data),ierr)
              call BINARY_FILE_WRITE(iunit2,data,n1*n2,kind(data),ierr)
           else
              call BINARY_FILE_READ(iunit1,data,n1*n2,kind(data),ierr)
              data = data + varvalue
              call BINARY_FILE_WRITE(iunit2,data,n1*n2,kind(data),ierr)
           end if
        end do
     end do
     call BINARY_FILE_CLOSE(iunit2,ierr)

  case (8) ! Return information on entire inflow file
     ! Will return most positive number, most negative number, 
     ! and smallest magnitude number for each variable
     allocate(mpn(nvar))
     allocate(mnn(nvar))
     allocate(smn(nvar))
     allocate(ntime_mpn(nvar))
     allocate(ntime_mnn(nvar))
     allocate(ntime_smn(nvar))
     ! Appropriately initialize to force a value assignment
     do var = 1,nvar
        mpn(var) = -100000000000.0
        mnn(var) = -mpn(var)
        smn(var) = mnn(var)
     end do
     ! Search entire inflow for MPN, MNN, SMN.
     do n=1,ntime
        do var=1,nvar
           call BINARY_FILE_READ (iunit1,data,n1*n2,kind(data),ierr)
           if(mpn(var) .lt. maxval(data)) then
              mpn(var) = maxval(data)
              ntime_mpn(var) = n
           end if
           if(mnn(var) .gt. minval(data)) then
              mnn(var) = minval(data)
              ntime_mnn(var) = n
           end if
           if(smn(var) .gt. minval(abs(data))) then
              smn(var) = minval(abs(data))
              ntime_smn(var) = n
           end if
        end do
     end do
     ! Print out result
     do var = 1, nvar
        print*,names(var)," Most Positive Number : ",mpn(var)," at ntime : ",ntime_mpn(var)
        print*,names(var)," Most Negative Number : ",mnn(var)," at ntime : ",ntime_mnn(var)
        print*,names(var)," Smallest Mag Number :  ",smn(var)," at ntime : ",ntime_smn(var)
        print*, ' ==================================================================================== '
     end do
     
  case (9) ! Rescale inflow
     print *,'Current mesh size --------------------'
     print *,'First direction ',minval(x1),maxval(x1)
     print *,'Second direction',minval(x2),maxval(x2)
     print *,'Time step       ',dt
     print *,'New mesh size ------------------------'
     print *,'First direction'
     read (*,*) x1(1),x1(n1+1)
     print *,'Second direction'
     read (*,*) x2(1),x2(n2+1)
     print *,'Time step'
     read (*,*) dt

     ! Create new grid
     do n=2,n1
        x1(n)=x1(n-1)+(x1(n1+1)-x1(1))/real(n1,WP)
     end do
     do n=2,n2
        x2(n)=x2(n-1)+(x2(n2+1)-x2(1))/real(n2,WP)
     end do
     
     call BINARY_FILE_OPEN(iunit2,trim(filename2),"w",ierr)
     call BINARY_FILE_WRITE(iunit2,ntime,1,kind(ntime),ierr)
     call BINARY_FILE_WRITE(iunit2,n1,1,kind(n1),ierr)
     call BINARY_FILE_WRITE(iunit2,n2,1,kind(n2),ierr)
     call BINARY_FILE_WRITE(iunit2,nvar,1,kind(nvar),ierr)
     call BINARY_FILE_WRITE(iunit2,dt,1,kind(dt),ierr)
     call BINARY_FILE_WRITE(iunit2,time,1,kind(time),ierr)
     do var=1,nvar
        call BINARY_FILE_WRITE(iunit2,names(var),str_short,kind(names),ierr)
     end do
     call BINARY_FILE_WRITE(iunit2,icyl,1,kind(icyl),ierr)     
     call BINARY_FILE_WRITE(iunit2,x1,n1+1,kind(x1),ierr)
     call BINARY_FILE_WRITE(iunit2,x2,n2+1,kind(x2),ierr)
     print *,'Writing'
     do n=1,ntime
        print *,n,' out of ',ntime
        do var=1,nvar
           call BINARY_FILE_READ (iunit1,data,n1*n2,kind(data),ierr)
           call BINARY_FILE_WRITE(iunit2,data,n1*n2,kind(data),ierr)
        end do
     end do
     call BINARY_FILE_CLOSE(iunit2,ierr)
     
  case (10) ! Add VOF for jet 
     print *, "Diameter : "
     read(*,*) D
     call BINARY_FILE_OPEN(iunit2,trim(filename2),"w",ierr)
     call BINARY_FILE_WRITE(iunit2,ntime,1,kind(ntime),ierr)
     call BINARY_FILE_WRITE(iunit2,n1,1,kind(n1),ierr)
     call BINARY_FILE_WRITE(iunit2,n2,1,kind(n2),ierr)
     call BINARY_FILE_WRITE(iunit2,nvar+8,1,kind(nvar),ierr)
     call BINARY_FILE_WRITE(iunit2,dt,1,kind(dt),ierr)
     call BINARY_FILE_WRITE(iunit2,time,1,kind(time),ierr)
     do var=1,nvar
        call BINARY_FILE_WRITE(iunit2,names(var),str_short,kind(names),ierr)
     end do
     do n=1,8
        write(varname,'(A3,I1)') 'VOF',n
        call BINARY_FILE_WRITE(iunit2,varname,str_short,kind(names),ierr)
     end do
     call BINARY_FILE_WRITE(iunit2,icyl,1,kind(icyl),ierr)     
     call BINARY_FILE_WRITE(iunit2,x1,n1+1,kind(x1),ierr)
     call BINARY_FILE_WRITE(iunit2,x2,n2+1,kind(x2),ierr)
     do n=1,ntime
        do var=1,nvar
           call BINARY_FILE_READ (iunit1,data,n1*n2,kind(data),ierr)
           call BINARY_FILE_WRITE(iunit2,data,n1*n2,kind(data),ierr)
        end do
        if (icyl.eq.0) then
           do k=1,n2
              do j=1,n1
                 if (sqrt(x1(j)**2+x2(k)**2).lt.0.5_WP*D) then
                    data(j,k)=1.0_WP
                 else
                    data(j,k)=0.0_WP
                 end if
              end do
           end do
        else
           do j=1,n1
              if (x1(j).lt.0.5_WP*D) then
                 data(j,:)=1.0_WP
              else
                 data(j,:)=0.0_WP
              end if
           end do
        end if
        do nn=1,8
           call BINARY_FILE_WRITE(iunit2,data,n1*n2,kind(data),ierr)
        end do
     end do
     call BINARY_FILE_CLOSE(iunit2,ierr)

  case default
     stop "Unknown choice"
  end select

  ! Close the files
  call BINARY_FILE_CLOSE(iunit1,ierr)

end program editInflow
