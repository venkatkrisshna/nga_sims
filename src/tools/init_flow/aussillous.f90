module aussillous
  use string
  use precision
  use param
  implicit none
  
  ! Length and diameter of the domain
  real(WP) :: Lx,Ly,Lz
  real(WP) :: dx,dy,dz
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: RHO
  real(WP), dimension(:,:,:), pointer :: dRHO
  
  ! Initial velocity
  real(WP) :: Wmean,VFR
  
end module aussillous


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine aussillous_grid
  use aussillous
  use parser
  implicit none
  
  integer :: i,j,k
  
  ! Read in mesh size
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  
  ! Read in domain size
  call parser_read('Lx',Lx); dx=Lx/real(nx-2,WP)
  call parser_read('Ly',Ly); dy=Ly/real(ny-2,WP)
  call parser_read('Lz',Lz); dz=Lz/real(nz  ,WP)
  
  ! Set the periodicity
  xper=0
  yper=0
  zper=1
  
  ! Cartesian
  icyl=0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*dx-0.5_WP*Lx-dx
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*dy-dy
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*dz
  end do
  
  ! Create the mid points 
  do i=1,nx
     xm(i)= 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)= 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask=0
  mask(1,:)=1; mask(nx,:)=1
  mask(:,1)=1; mask(:,ny)=1
  
  return
end subroutine aussillous_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine aussillous_data
  use aussillous
  use parser
  implicit none
  
  integer :: i,j
  real(WP) :: rho0
  
  ! Allocate the array data
  nvar = 6
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U    => data(:,:,:,1); names(1) = 'U'
  V    => data(:,:,:,2); names(2) = 'V'
  W    => data(:,:,:,3); names(3) = 'W'
  P    => data(:,:,:,4); names(4) = 'P'
  RHO  => data(:,:,:,5); names(5) = 'RHO'
  dRHO => data(:,:,:,6); names(6) = 'dRHO'
  
  ! Create them
  call parser_read('Volume flow rate',VFR)
  Wmean=VFR/(Lx*Ly)
  U=0.0_WP
  V=0.0_WP
  do j=1,ny
     do i=1,nx
        if (mask(i,j).eq.0) then
           W(i,j,:)=Wmean
        else
           W(i,j,:)=0.0_WP
        end if
     end do
  end do
  P=0.0_WP
  call parser_read('Density',rho0)
  RHO=rho0
  dRHO=0.0_WP
  
  return
end subroutine aussillous_data


! ======================================== !
! Generate an initial droplet distribution !
! ======================================== !
subroutine aussillous_part
  use aussillous
  use math
  use lpt_mod
  use random
  use parser
  implicit none
  
  integer  :: iunit,ierr
  integer  :: np,i,ix,iy,iz
  integer  :: npartx,nparty,npartz
  real(WP) :: Hbed
  real(WP) :: VFavg,Volp,Volbed,sumVolp
  real(WP) :: rand,Lp,Lpx,Lpy,mean_d
  real(WP) :: d_mean
  real(WP) :: dt,time
  character(len=str_medium) :: part_init
  real(WP), dimension(:), pointer :: dtmp
  logical :: use_lpt

  ! Check whether lpt is used
  call parser_is_defined('Init part file',use_lpt)
  if (.not.use_lpt) return
  
  ! Initialize the random number generator
  call random_init
  
  ! Read average volume fraction and bed height
  call parser_read('Bed height',Hbed)
  call parser_read('Avg. volume fraction',VFavg)
  
  ! Read particle diameter
  call parser_read('Particle mean diameter',d_mean)
  
  ! First get bed volume
  Volbed = Hbed*Lx*Lz
  
  ! Estimate number of particles
  Volp = Pi/6.0_WP*d_mean**3
  np = int(VFavg*Volbed/Volp)
  
  ! Allocate particle array
  allocate(part(1:np))
  
  ! Mean interparticle distance
  Lp = (Volp/VFavg)**(1.0_WP/3.0_WP)
  
  ! Add particles
  do i=1,np
     part(i)%dt  =0.0_WP
     part(i)%Acol=0.0_WP
     part(i)%Tcol=0.0_WP
     part(i)%wx  =0.0_WP
     part(i)%wy  =0.0_WP
     part(i)%wz  =0.0_WP
     part(i)%id  =int(i,kind=8)
     part(i)%stop=0
     part(i)%d   =d_mean
     part(i)%u   =0.0_WP
     part(i)%v   =0.0_WP
     part(i)%w   =0.0_WP
     call random_number(rand)
     part(i)%x   =Lx*(rand-0.5_WP)
     call random_number(rand)
     part(i)%y   =Hbed*rand
     call random_number(rand)
     part(i)%z   =Lz*rand
  end do
  
  ! Effective volume fraction
  VFavg = real(np,WP)*Volp/Volbed
  
  ! Output
  print*,'Number of particles: ',np
  print*,'Interparticle spacing: ',Lp
  print*,'Effective mean diameter: ',mean_d
  print*,'Effective volume fraction: ',VFavg
  
  ! Generate the initial droplet file
  call parser_read('Init part file',part_init)
  call BINARY_FILE_OPEN(iunit,trim(part_init),"w",ierr)
  call BINARY_FILE_WRITE(iunit,np,1,kind(np),ierr)
  call BINARY_FILE_WRITE(iunit,part_kind,1,kind(part_kind),ierr)
  dt = 0.0_WP
  call BINARY_FILE_WRITE(iunit,dt,1,kind(dt),ierr)
  time = 0.0_WP
  call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr)
  do i=1,np
     call BINARY_FILE_WRITE(iunit,part(i),1,part_kind,ierr)
  end do
  call BINARY_FILE_CLOSE(iunit,ierr)
  
  return
end subroutine aussillous_part
