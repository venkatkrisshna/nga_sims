module drop_coll
  use string
  use precision
  use param
  implicit none

  ! Length of the domain
  real(WP) :: Lx,Lz,Ly

  ! Pointers to variable in data
  real(WP), dimension(:,:,:),   pointer :: U
  real(WP), dimension(:,:,:),   pointer :: V
  real(WP), dimension(:,:,:),   pointer :: W
  real(WP), dimension(:,:,:,:), pointer :: VOF
  real(WP), dimension(:,:,:),   pointer :: ZMIX

end module drop_coll

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine drop_coll_grid
  use drop_coll
  use parser
  implicit none

  integer :: i,j,k

  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  call parser_read('Lz',Lz)

  ! Cartesian and periodicity
  icyl = 0
  xper = 1
  yper = 1
  zper = 1
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create a uniform grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*Lx/real(nx,WP)-0.5_WP*Lx
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*Ly/real(ny,WP)-0.5_WP*Ly
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*Lz/real(nz,WP)-0.5_WP*Lz
  end do
  
  ! Create the mid points
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine drop_coll_grid


subroutine drop_coll_data
  use drop_coll
  use parser
  use random
  use math
  implicit none
  
  integer :: s,i,j,k
  integer :: ii,jj,kk
  real(WP) :: d1,d2,dist1p,dist1m,dist2p,dist2m
  real(WP), dimension(3) :: cent1,cent2
  real(WP), dimension(3) :: U1,U2
  integer, parameter :: nF=20
  real(WP) :: myx,myy,myz
  
  ! Allocate the array inflow
  nvar = 11 ! 12
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U => data(:,:,:,1); names(1) = 'U'
  V => data(:,:,:,2); names(2) = 'V'
  W => data(:,:,:,3); names(3) = 'W'
  VOF  => data(:,:,:, 4:11);
  names( 4) = 'VOF1'
  names( 5) = 'VOF2'
  names( 6) = 'VOF3'
  names( 7) = 'VOF4'
  names( 8) = 'VOF5'
  names( 9) = 'VOF6'
  names(10) = 'VOF7'
  names(11) = 'VOF8'
  !ZMIX => data(:,:,:,12); names(5) = 'ZMIX'
  data = 0.0_WP
  
  ! Read parameters
  call parser_read('Drop 1 diameter',d1)
  call parser_read('Drop 1 center',cent1(1:3))
  call parser_read('Drop 1 velocity',U1(1:3))
  call parser_read('Drop 2 diameter',d2)
  call parser_read('Drop 2 center',cent2(1:3))
  call parser_read('Drop 2 velocity',U2(1:3))
  
  ! Set level set and velocity
  do k=1,nz
     do j=1,ny
        do i=1,nx
           
           ! Drop 1 =======================================================================
           
           ! X face
           dist1p = sqrt((xm(i  )-cent1(1))**2 + (ym(j)-cent1(2))**2 + (zm(k)-cent1(3))**2)
           if (i.gt.1) then
              dist1m = sqrt((xm(i-1)-cent1(1))**2 + (ym(j)-cent1(2))**2 + (zm(k)-cent1(3))**2)
           else
              dist1m=dist1p
           end if
           if (dist1p.le.0.5_WP*d1 .or. dist1m.le.0.5_WP*d1) U(i,j,k) = U1(1)
           
           ! Y face
           dist1p = sqrt((xm(i)-cent1(1))**2 + (ym(j  )-cent1(2))**2 + (zm(k)-cent1(3))**2)
           if (j.gt.1) then
              dist1m = sqrt((xm(i)-cent1(1))**2 + (ym(j-1)-cent1(2))**2 + (zm(k)-cent1(3))**2)
           else
              dist1m = dist1p
           end if
           if (dist1p.le.0.5_WP*d1 .or. dist1m.le.0.5_WP*d1) V(i,j,k) = U1(2)
           
           ! Z face
           dist1p = sqrt((xm(i)-cent1(1))**2 + (ym(j)-cent1(2))**2 + (zm(k  )-cent1(3))**2)
           if (k.gt.1) then
              dist1m = sqrt((xm(i)-cent1(1))**2 + (ym(j)-cent1(2))**2 + (zm(k-1)-cent1(3))**2)
           else
              dist1m = dist1p
           end if
           if (dist1p.le.0.5_WP*d1 .or. dist1m.le.0.5_WP*d1) W(i,j,k) = U1(3)
           
           ! Scalar
           !if (dist1p.le.0.5_WP*d1) ZMIX(i,j,k)=+1.0_WP
           
           ! Drop 2 =======================================================================
           
           ! X face
           dist2p = sqrt((xm(i  )-cent2(1))**2 + (ym(j)-cent2(2))**2 + (zm(k)-cent2(3))**2)
           if (i.gt.1) then
              dist2m = sqrt((xm(i-1)-cent2(1))**2 + (ym(j)-cent2(2))**2 + (zm(k)-cent2(3))**2)
           else
              dist2m = dist2p
           end if
           if (dist2p.le.0.5_WP*d2 .or. dist2m.le.0.5_WP*d2) U(i,j,k) = U2(1)
           
           ! Y face
           dist2p = sqrt((xm(i)-cent2(1))**2 + (ym(j  )-cent2(2))**2 + (zm(k)-cent2(3))**2)
           if (j.gt.1) then 
              dist2m = sqrt((xm(i)-cent2(1))**2 + (ym(j-1)-cent2(2))**2 + (zm(k)-cent2(3))**2)
           else
              dist2m = dist2p
           end if
           if (dist2p.le.0.5_WP*d2 .or. dist2m.le.0.5_WP*d2) V(i,j,k) = U2(2)
           
           ! Z face
           dist2p = sqrt((xm(i)-cent2(1))**2 + (ym(j)-cent2(2))**2 + (zm(k  )-cent2(3))**2)
           if (k.gt.1) then
              dist2m = sqrt((xm(i)-cent2(1))**2 + (ym(j)-cent2(2))**2 + (zm(k-1)-cent2(3))**2)
           else
              dist2m = dist2p
           end if
           if (dist2p.le.0.5_WP*d2 .or. dist2m.le.0.5_WP*d2) W(i,j,k) = U2(3)
           
           ! Scalar
           !if (dist2p.le.0.5_WP*d2) ZMIX(i,j,k)=-1.0_WP
           
           ! Distance ====================================================================
           do s=1,8
              VOF(i,j,k,s)=0.0_WP
              do kk=1,nF
                 do jj=1,nF
                    do ii=1,nF
                       myx=xs1(s,i)+(real(ii,WP)-0.5_WP)/real(nf,WP)*(xs2(s,i)-xs1(s,i))
                       myy=ys1(s,j)+(real(jj,WP)-0.5_WP)/real(nf,WP)*(ys2(s,j)-ys1(s,j))
                       myz=zs1(s,k)+(real(kk,WP)-0.5_WP)/real(nf,WP)*(zs2(s,k)-zs1(s,k))
                       if (max(0.5_WP*d1-sqrt((myx-cent1(1))**2 + (myy-cent1(2))**2 + (myz-cent1(3))**2), &
                            0.5_WP*d2-sqrt((myx-cent2(1))**2 + (myy-cent2(2))**2 + (myz-cent2(3))**2)).gt.0.0_WP) then
                          VOF(i,j,k,s)=VOF(i,j,k,s)+1.0_WP/real(nF,WP)**3
                       end if
                    end do
                 end do
              end do
           end do
        end do
     end do
  end do

  
  return
end subroutine drop_coll_data
