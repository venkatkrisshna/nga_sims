module splitter
  use precision
  use param
  implicit none

  ! dimensions of simulation domain
  real(WP) :: Hg,Hg2,y0,x0,e,el,delta,e_frac,n_bl,n_e,nBL
  real(WP) :: Lx,Ly,Lz,dx,dy,dz,angle,dy1,dy2
  real(WP), dimension(:), pointer :: dy_up,dx_up
  
  ! phase initial velocities
  real(WP) :: Ul,Ug,M
  real(WP) :: rhog,rhol,mug
 
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: VOF1
  real(WP), dimension(:,:,:), pointer :: VOF2
  real(WP), dimension(:,:,:), pointer :: VOF3
  real(WP), dimension(:,:,:), pointer :: VOF4
  real(WP), dimension(:,:,:), pointer :: VOF5
  real(WP), dimension(:,:,:), pointer :: VOF6
  real(WP), dimension(:,:,:), pointer :: VOF7
  real(WP), dimension(:,:,:), pointer :: VOF8
  
  real(WP), dimension(:,:,:), pointer :: VOF
  
  ! indices for grid creation
  real(WP) :: bot1,top1,bot2,top2
  integer  :: bot1j,top1j,bot2j,top2j,n_upper
  real(WP) :: stretch
  character(len=str_medium) :: anchor

end module splitter

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine splitter_grid
  use splitter
  use parser
  use math
  implicit none

  integer :: i,j,k,gwcnt,scnt

  ! Read in the domain vars
  call parser_read('Liquid height',y0)
  call parser_read('Liquid initial length',x0)
  call parser_read('Liquid angle',angle)
  call parser_read('Gas height',Hg)
  call parser_read('Splitter plate th',e)
  call parser_read('Splitter plate len',el)
  call parser_read('Cells in splitter plate',n_e)
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  call parser_read('Cells in boundary layer',n_bl)
  call parser_read('Upper ny',n_upper)
!  call parser_read('Liquid velocity',Ul)
  call parser_read('Gas velocity',Ug)
  call parser_read('Gas density',rhog)
  call parser_read('Liquid density',rhol)
  call parser_read('Gas viscosity',mug)
  call parser_read('Dynamic pressure ratio',M)
  call parser_read('Gas vorticity th',delta,0.0_WP)
  if (delta.eq.0.0_WP) delta = 5.2_WP*Hg/sqrt(Hg*Ug*rhog/mug)
 
  ! Liquid velocity
  Ul = sqrt(rhog*Ug*Ug/(M*rhol))
  ! Temporary ny
  ny = ny-n_upper
 ! nx = nx-n_upper
  ! domain parameters
  dy1 = e/n_e
  dy2 = delta/n_bl
  !dy = min(dy1, dy2)
  dy = dy2
  Ly = real(ny,WP)*dy
  Hg2 = Ly-(y0+Hg+e)
  ! Enforce an AR of 1 for each grid cell
  dx = dy
  dz = dy
  Lx = real(nx,WP)*dx
  Lz = real(nz,WP)*dz

  nBL = delta/dy
  print *,'grid:',nx,ny,nz
  print *,'lengths:',Lx,Ly,Lz
  print *, 'delta = ',delta
  print *, 'nBL = ',nBL
  print *,'----------------------------------------------'

  ! Set the periodicity
  xper = 0
  yper = 0
  zper = 1

  ! Cartesian
  icyl = 0

  ! Allocate the arrays
  allocate(x(nx+n_upper+1),y(ny+n_upper+1),z(nz+1))
  allocate(xm(nx+n_upper),ym(ny+n_upper),zm(nz))
  allocate(mask(nx+n_upper,ny+n_upper))

  ! Create the grid
  do i = 1,nx+1
     x(i) = real(i-1,WP)*Lx/real(nx)
  end do
  do j = 1,ny+1
     y(j) = real(j-1,WP)*Ly/real(ny)
  end do
  do k = 1,nz+1
     z(k) = real(k-1,WP)*Lz/real(nz)-0.5_WP*Lz
  end do

  ! Create the mid points
  do i = 1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j = 1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k = 1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do


  ! Stretch the upper domain
  if (n_upper.gt.0) then
     call parser_read('Stretching ratio',stretch)
     allocate(dy_up(n_upper))
!     allocate(dx_up(n_upper))
     ! First stretched cell
     dy_up(1) = stretch*(y(ny+1)-y(ny))
!     dx_up(1) = stretch*(x(nx+1)-x(nx))
     ! Get the rest of the cells
     do j=2,n_upper
        dy_up(j) = stretch*dy_up(j-1)
!        dx_up(j) = stretch*dx_up(j-1)
     end do
     ! Create the rest of the grid
     do j=ny+2,ny+n_upper+1
        y(j) = y(j-1)+dy_up(j-ny-1)
     end do
!     do i=nx+2,nx+n_upper+1
!        x(i) = x(i-1)+dx_up(i-nx-1)
!     end do
     ! Create the mid points
     do j=ny+1,ny+n_upper
        ym(j) = 0.5_WP*(y(j)+y(j+1))
     end do
!     do i=nx+1,nx+n_upper
!        xm(i) = 0.5_WP*(x(i)+x(i+1))
!     end do

     ! Get new domain params
     Ly = Ly+sum(dy_up)
!     Lx = Lx+sum(dx_up)
     ny = ny+n_upper
!     nx = nx+n_upper
     print*,'domain including stretching: Ly = ',Ly,'ny = ',ny
     print*,'domain including stretching: Lx = ',Lx,'nx = ',nx
  end if
  ! Create the masks
  mask = 0
  ! wall at bottom of domain
 ! mask(:,1) = 1
  
  return
end subroutine splitter_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine splitter_data
  use splitter
  use parser
  use fileio
  use math
  implicit none

  integer :: i,j,k,iunit,ii,jj,kk
  real(WP) :: eps,myx,myy,myz,dist,norm
  integer, parameter :: nF=20
  
  ! Allocate the array data
  nvar = 11
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U    => data(:,:,:, 1); names( 1) = 'U'
  V    => data(:,:,:, 2); names( 2) = 'V'
  W    => data(:,:,:, 3); names( 3) = 'W'
  VOF1 => data(:,:,:, 4); names( 4) = 'VOF1'
  VOF2 => data(:,:,:, 5); names( 5) = 'VOF2'
  VOF3 => data(:,:,:, 6); names( 6) = 'VOF3'
  VOF4 => data(:,:,:, 7); names( 7) = 'VOF4'
  VOF5 => data(:,:,:, 8); names( 8) = 'VOF5'
  VOF6 => data(:,:,:, 9); names( 9) = 'VOF6'
  VOF7 => data(:,:,:,10); names(10) = 'VOF7'
  VOF8 => data(:,:,:,11); names(11) = 'VOF8'

  ! Create them
  U    = 0.0_WP
  V    = 0.0_WP
  W    = 0.0_WP
  VOF1 = 0.0_WP
  VOF2 = 0.0_WP
  VOF3 = 0.0_WP
  VOF4 = 0.0_WP
  VOF5 = 0.0_WP
  VOF6 = 0.0_WP
  VOF7 = 0.0_WP
  VOF8 = 0.0_WP
  
  call parser_read('Interface anchor',anchor,'middle')

  ! Where to put the level set?
!  if (trim(adjustl(anchor)).eq.'top') then
!     e_frac = 1.0_WP
!  else
     e_frac = 0.5_WP
!  end if
! y0 + tan(7/180.0*M_PI)*x - tan(7/180.0*M_PI)*x0  && x < x0
     
     do k=1,nz
        do j=1,ny
           do i=1,nx
              if (ym(j).le.y0.and.xm(i).le.x0) then
                 VOF1(i,j,k) = 0.0_WP
                 VOF2(i,j,k) = 0.0_WP
                 VOF3(i,j,k) = 0.0_WP
                 VOF4(i,j,k) = 0.0_WP
                 VOF5(i,j,k) = 0.0_WP
                 VOF6(i,j,k) = 0.0_WP
                 VOF7(i,j,k) = 0.0_WP
                 VOF8(i,j,k) = 0.0_WP
              end if
           end do
        end do
     end do
    
  ! initialize the velocity profile in the domain

  ! Create velocity profile
  do j=1,ny
    do i=1,nx
     if (ym(j).le.y0.and.xm(i).le.x0) then
        U(i,j,:) = Ul
     else if (ym(j).gt.y0.and.ym(j).le.(y0+0.5_WP*Hg).and.xm(i).le.x0) then
        U(i,j,:) = Ug*tanh(2.0_WP*(ym(j)-y0)/delta)*cos(angle/180.0_WP*Pi)
        V(i,j,:) = -1.0_WP*Ug*tanh(2.0_WP*(ym(j)-y0)/delta)*sin(angle/180.0_WP*Pi)
     else if (ym(j).gt.(y0+0.5_WP*Hg).and.ym(j).le.(y0+Hg).and.xm(i).le.x0) then
        U(i,j,:) = Ug*tanh(2.0_WP*abs((ym(j)-(y0+Hg)))/delta)*cos(angle/180.0_WP*Pi)
        V(i,j,:) = -1.0_WP*Ug*tanh(2.0_WP*abs((ym(j)-(y0+Hg)))/delta)*sin(angle/180.0_WP*Pi)
     else if (ym(j).gt.(y0+Hg).and.xm(i).le.x0) then
        U(i,j,:) = 0.1_WP*Ug*tanh(2.0_WP*(ym(j)-(y0+Hg))/delta)
        V(i,j,:) = 0.0_WP
     end if
    end do
  end do
  

  ! Dump interface to a file for checking
  !iunit=iopen()
  !open(iunit,file='testVOF',form='formatted')
  !do j=1,ny
  !   write(iunit,*) ym(j),VOF(1,j,1)
  !end do
  !close(iclose(iunit))
  
  return
end subroutine splitter_data


!============================!
 subroutine splitter_inflow 
!============================!

  use splitter
  use parser
  use fileio
  use math
  implicit none

  integer :: j,k,iunit,jj
  logical :: use_inflow
  integer, parameter :: nF=20
  real(WP) :: dist,myy,myz

  ! Only if not periodic
  if (xper.eq.1) return

  ! Generate the inflow
  nvar_inflow = 4
  ntime = 2

  ! Allocate some arrays
  allocate(inflow(ntime,ny,nz,nvar_inflow))
  allocate(names_inflow(nvar_inflow))

  ! Link the pointers
  U     => inflow(:,:,:,1); names_inflow(1) = 'U'
  V     => inflow(:,:,:,2); names_inflow(2) = 'V'
  W     => inflow(:,:,:,3); names_inflow(3) = 'W'
  VOF   => inflow(:,:,:,4); names_inflow(4) = 'VOF'

  ! Compute time grid
  time_inflow = 0.0_WP
  dt_inflow   = 1.0_WP

  ! initialize the velocities
  U   = 0.0_WP
  V   = 0.0_WP
  W   = 0.0_WP
  VOF = 0.0_WP

  ! prepare the velocity
  call parser_read('Gas velocity',Ug)
  call parser_read('Dynamic pressure ratio',M)

  ! Liquid velocity
 Ul = sqrt(rhog*Ug*Ug/(M*rhol))
  ! initialize the velocity profile at the inlet
  do j=1,ny
     ! liquid profile
     if (ym(j).le.y0) then
        U(:,j,:) = Ul
     ! gas profile
     else if ((ym(j).ge.y0) .and. (ym(j).le.(y0+0.5_WP*Hg))) then
        U(:,j,:) = Ug*tanh(2.0_WP*(ym(j)-y0)/delta)*cos(angle/180.0_WP*Pi)
        V(:,j,:) = -1.0_WP*Ug*tanh(2.0_WP*(ym(j)-y0)/delta)*sin(angle/180.0_WP*Pi)
     else if ((ym(j).gt.(y0+0.5_WP*Hg)) .and. (ym(j).le.(y0+Hg))) then
        U(:,j,:) = Ug*tanh(2.0_WP*abs((ym(j)-(y0+Hg)))/delta)*cos(angle/180.0_WP*Pi)
        V(:,j,:) = -1.0_WP*Ug*tanh(2.0_WP*abs((ym(j)-(y0+Hg)))/delta)*sin(angle/180.0_WP*Pi)
     ! upper stream
     else if (ym(j).gt.(y0+Hg+dy)) then
        U(:,j,:) = 0.05_WP*Ug*tanh(2.0_WP*(ym(j)-(y0+Hg+dy))/delta)
        V(:,j,:) = 0.0_WP
     end if
  end do
  
  
  ! VOF inflow

   do j=1,ny
      if (ym(j).le.y0) then
         VOF(:,j,:)=1.0_WP
      end if
   end do
   
  print *,'max/min U vals : ',maxval(U),minval(U)
  
  ! Dump profile to a file for checking
  iunit=iopen()
  open(iunit,file='testinflow',form='formatted')
  do k=1,nz
     do j=1,ny
        write(iunit,*) ym(j),zm(k),U(1,j,k)
     end do
  end do
  close(iclose(iunit))

  return
end subroutine splitter_inflow


! ========================== !
! create the optdata for SGS !
! ========================== !
subroutine splitter_optdata
  use splitter
  implicit none
  
  ! Allocate arrays
  !nod = 2
  !allocate(OD(nx,ny,nz,nod))
  !allocate(OD_names(nod))
  ! Link the pointers
  !OD_names(1) = 'LM_VEL'
  !OD_names(2) = 'MM_VEL'
  !OD(:,:,:,1)=0.0_WP
  !OD(:,:,:,2)=0.0_WP
  
  return
end subroutine splitter_optdata
