module freefalling_drop
  use string
  use precision
  use param
  implicit none
   
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: VOF

end module freefalling_drop

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine freefalling_drop_grid
  use freefalling_drop
  use parser
  use math
  use string
  implicit none
  
  integer :: i,j,k
  real(WP) :: Lx,Ly,Lz
  real(WP) :: dx,dy,dz
  real(WP) :: x_shift
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  call parser_read('Lz',Lz)
  call parser_read('xper',xper)
  call parser_read('yper',yper)
  call parser_read('zper',zper)
  call parser_read('Domain shift',x_shift)

  ! Cartesian
  icyl=0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  dx=Lx/real(nx,WP)
  dy=Ly/real(ny,WP)
  dz=Lz/real(nz,WP)
  do i=1,nx+1
     x(i) = real(i-1,WP)*dx-0.5_WP*(real(nx,WP)*dx)+x_shift
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*dy-0.5_WP*(real(ny,WP)*dy)
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*dz-0.5_WP*(real(nz,WP)*dz)
  end do
  
  ! Create the cell centered grid
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine freefalling_drop_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine freefalling_drop_data
  use freefalling_drop
  use parser
  use math
  implicit none
  
  integer :: i,j,k,ii,jj,kk,nF
  real(WP) :: G,x_here,y_here,z_here,Diam,tmp_VOF

  ! Allocate the array data
  nvar = 4
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U   => data(:,:,:,1); names(1) = 'U'
  V   => data(:,:,:,2); names(2) = 'V'
  W   => data(:,:,:,3); names(3) = 'W'
  VOF => data(:,:,:,4); names(4) = 'VOF'
  
  ! Initialize
  U = 0.0_WP
  V = 0.0_WP
  W = 0.0_WP

  ! Read Diameter
  call parser_read('Drop diameter',Diam)
  
  ! Calculate VOF from level set on fine mesh within each cell
  nF=20
  do k=1,nz
     do j=1,ny
        do i=1,nx
           ! Make VOF on fine mesh 
           tmp_VOF=0.0_WP
           do kk=1,nF
              do jj=1,nF
                 do ii=1,nF
                    x_here=x(i)+real(ii,WP)/real(nf+1,WP)*(x(i+1)-x(i))
                    y_here=y(j)+real(jj,WP)/real(nf+1,WP)*(y(j+1)-y(j))
                    z_here=z(k)+real(kk,WP)/real(nf+1,WP)*(z(k+1)-z(k))
                    G=0.5_WP*Diam-sqrt(x_here**2+y_here**2+z_here**2)
                    if (G.gt.0.0_WP) then
                       tmp_VOF=tmp_VOF+1.0_WP
                    end if
                 end do
              end do
           end do
           ! Calculate VOF from fine mesh
           VOF(i,j,k)=tmp_VOF/real(nF,WP)**3
        end do
     end do
  end do
           
  return
end subroutine freefalling_drop_data

