module deformation_3d
  use string
  use precision
  use param
  implicit none
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:),   pointer :: U
  real(WP), dimension(:,:,:),   pointer :: V
  real(WP), dimension(:,:,:),   pointer :: W
  real(WP), dimension(:,:,:,:), pointer :: VOF
  
end module deformation_3d

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine deformation_3d_grid
  use deformation_3d
  use parser
  implicit none
  integer :: i,j,k
  
  ! Read in the size of the domain
  call parser_read('Number of points',nx)
  ny = nx
  nz = nx
  
  ! Set the periodicity
  xper = 1
  yper = 1
  zper = 1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1) ,y(ny+1) ,z(nz+1))
  allocate(xm(nx+1),ym(ny+1),zm(nz+1))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)/real(nx,WP)
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)/real(nx,WP)
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)/real(nx,WP)
  end do
  
  ! Create the mid points
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine deformation_3d_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine deformation_3d_data
  use deformation_3d
  use parser
  use math
  use compgeom
  implicit none
  
  integer :: s,i,j,k
  integer ::ii,jj,kk
  integer :: nf
  real(WP) :: G,myx,myy,myz
  
  ! Allocate the array data
  nvar = 11
  allocate(data(imin_:imax_,jmin_:jmax_,kmin_:kmax_,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U    (imin_:imax_,jmin_:jmax_,kmin_:kmax_) => data(:,:,:, 1); names( 1) = 'U'
  V    (imin_:imax_,jmin_:jmax_,kmin_:kmax_) => data(:,:,:, 2); names( 2) = 'V'
  W    (imin_:imax_,jmin_:jmax_,kmin_:kmax_) => data(:,:,:, 3); names( 3) = 'W'
  VOF  (imin_:imax_,jmin_:jmax_,kmin_:kmax_,1:8) => data(:,:,:, 4:11);
  names( 4) = 'VOF1'
  names( 5) = 'VOF2'
  names( 6) = 'VOF3'
  names( 7) = 'VOF4'
  names( 8) = 'VOF5'
  names( 9) = 'VOF6'
  names(10) = 'VOF7'
  names(11) = 'VOF8'
    
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           
           ! Set velocity
           U(i,j,k) = +2.0_WP*sin(   Pi*x (i))**2*sin(twoPi*ym(j))   *sin(twoPi*zm(k))   
           V(i,j,k) = -       sin(twoPi*xm(i))   *sin(   Pi*y (j))**2*sin(twoPi*zm(k))   
           W(i,j,k) = -       sin(twoPi*xm(i))   *sin(twoPi*ym(j))   *sin(   Pi*z (k))**2

           ! Set VOF
           G  = 0.15_WP-sqrt((xm(i)-0.35_WP)**2+(ym(j)-0.35_WP)**2+(zm(k)-0.35_WP)**2)
           if (G.gt.2.0_WP*min((x(i+1)-x(i)),y(j+1)-y(j),z(k+1)-z(k))) then ! Liquid
              VOF(i,j,k,:)=1.0_WP
           else if (G.lt.-2.0_WP*min((x(i+1)-x(i)),y(j+1)-y(j),z(k+1)-z(k))) then ! Gas
              VOF(i,j,k,:)=0.0_WP
           else ! Close to interface
              nf=20
              ! Compute VOF close to the interface
              do s=1,8
                 ! Use finer mesh within the cartesian cell
                 VOF(i,j,k,s)=0.0_WP
                 do kk=1,nF
                    do jj=1,nF
                       do ii=1,nF
                          myx=xs1(s,i)+(real(ii,WP)-0.5_WP)/real(nf,WP)*(xs2(s,i)-xs1(s,i))
                          myy=ys1(s,j)+(real(jj,WP)-0.5_WP)/real(nf,WP)*(ys2(s,j)-ys1(s,j))
                          myz=zs1(s,k)+(real(kk,WP)-0.5_WP)/real(nf,WP)*(zs2(s,k)-zs1(s,k))
                          G  = 0.15_WP-sqrt((myx-0.35_WP)**2+(myy-0.35_WP)**2+(myz-0.35_WP)**2)
                          if (G.gt.0.0_WP) then
                             VOF(i,j,k,s)=VOF(i,j,k,s)+1.0_WP
                          end if
                       end do
                    end do
                 end do
                 VOF(i,j,k,s)=VOF(i,j,k,s)/real(nF,WP)**3
              end do
           end if
        end do
     end do
  end do

  return
end subroutine deformation_3d_data
