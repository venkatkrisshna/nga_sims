module HITA
  use string
  use precision
  use param
  implicit none

  
  ! Mesh size
  real(WP) :: Length, npl, ratio, Lx,Ly,Lz
  
  ! Pointers to variable in data
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  !real(WP), dimension(:,:,:), pointer :: VOF
  
end module HITA


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine HITA_grid
  use HITA
  use parser
  use math
  implicit none
  
  integer :: i,j,k
  
  ! Read parameters
  call parser_read('Length',Length)
  call parser_read('ratio',ratio)
  call parser_read('npl',npl)
  
  ! Create a rectangular prism with uniform square meshing
  Lx = ratio*Length
  Ly = Length
  Lz = Length
  nx = npl*ratio
  ny = npl
  nz = npl
  !call parser_read('nx',nx)
  !call parser_read('ny',ny)
  !call parser_read('nz',nz)
  !call parser_read('Lx',Lx)
  !call parser_read('Ly',Ly)
  !call parser_read('Lz',Lz)
  
  ! Set the periodicity
  xper = 0
  yper = 1
  zper = 1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*Lx/real(nx,WP)
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*Ly/real(ny,WP) - 0.5_WP*Ly
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*Lz/real(nz,WP) - 0.5_WP*Lz
  end do
  
  ! Create the mid points
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine HITA_grid

! ========================= !
! Create the variable array !
! ========================= !
subroutine HITA_data
  use HITA
  use precision
  use string
  use fileio
  use parser
  use cli_reader
  use math
  implicit none
    
  ! Allocate the array data
  nvar = 4
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U => data(:,:,:,1); names(1) = 'U'
  V => data(:,:,:,2); names(2) = 'V'
  W => data(:,:,:,3); names(3) = 'W'
  P => data(:,:,:,4); names(4) = 'P'
  
  ! Create the arrays
  U   = 0.0_WP
  V   = 0.0_WP
  W   = 0.0_WP

 print*,'periodicity:',xper,yper,zper
  
  
end subroutine HITA_data

