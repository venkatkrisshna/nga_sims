module filter_test
  use string
  use precision
  use param
  implicit none
  
  ! Mesh
  real(WP) :: dx,dy,dz
  real(WP) :: sx,sy
  
  ! Orientation
  character(len=str_medium) :: orientation
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: ZMIX
  
end module filter_test


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine filter_grid
  use filter_test
  use math
  use parser
  implicit none
  
  integer :: i,j,k
  
  ! Read the number of points
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  
  ! Set the periodicity
  xper = 1
  yper = 1
  zper = 1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  dx = twoPi/real(nx,WP)
  dy = twoPi/real(ny,WP)
  dz = twoPi/real(nz,WP)

  do i=1,nx+1
     x(i) = real(i-1,WP)*dx
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*dy
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*dz
  end do
  
  ! Create the mid points
  do i=1,nx
     xm(i)= 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)= 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do

  ! Center
  x = x-Pi
  y = y-Pi
  z = z-Pi

  xm = xm-Pi
  ym = ym-Pi
  zm = zm-Pi
  
  
  ! Create the masks
  mask = 0
  
  return
end subroutine filter_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine filter_data
  use filter_test
  use parser
  use math
  implicit none
  
  integer :: i,j,k, loop, lx,ly,lz
  
  ! Read in the orientation
  call parser_read('Orientation',orientation)
  
  ! Allocate the array data
  nvar = 4
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U    => data(:,:,:,1); names(1) = 'U'
  V    => data(:,:,:,2); names(2) = 'V'
  W    => data(:,:,:,3); names(3) = 'W'
  P    => data(:,:,:,4); names(4) = 'P'

  U = 0.0_WP
  V = 0.0_WP
  W = 0.0_WP
  P = 0.0_WP
  
  ! Create the fields
  select case (trim(orientation))
  case('x')
    do i=1,nx
      do loop=1,nx/2
        U(i,:,:) = U(i,:,:) +sin(loop*x(i))
      end do
    end do
  case('y')
    do j=1,ny
      do loop=1,ny/2
        V(:,j,:) = V(:,j,:) +sin(loop*y(j))
      end do
    end do
  case('z')
    do k=1,nz
      do loop=1,nz/2
        W(:,:,k) = W(:,:,k) +sin(loop*z(k))
      end do
    end do
  case('3D')
    do k=1,nz
      do j=1,ny
        do i=1,nx
          do lz=1,nz/2
            do ly=1,ny/2
              do lx=1,nx/2
                U(i,j,k) = U(i,j,k)-sin(lx*x(i))*lx**2 -sin(ly*y(j))*ly**2 &
                                    -sin(lz*z(k))*lz**2
              end do
            end do
          end do
        end do
      end do
    end do
  end select

  return
end subroutine filter_data
