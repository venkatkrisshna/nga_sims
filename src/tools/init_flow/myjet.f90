module myjet
  use string
  use precision
  use param
  implicit none
  
  ! Domain
  real(WP) :: Lx,Ly,Lz
  real(WP) :: dx,dy,dz
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  
end module myjet

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine myjet_grid
  use myjet
  use parser
  use math
  implicit none
  integer :: i,j,k
  
  ! Cartesian or cylindrical
  icyl=0
  
  ! Periodicity
  xper=0
  yper=0!1
  zper=1
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('Lx',Lx)
  dx=Lx/real(nx,WP)
  call parser_read('ny',ny)
  call parser_read('Ly',Ly)
  dy=Ly/real(ny,WP)
  dz=min(dx,dy); Lz=dz; nz=1
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i)=real(i-1,WP)*dx
  end do
  do j=1,ny+1
     y(j)=real(j-1,WP)*dy
  end do
  do k=1,nz+1
     z(k)=real(k-1,WP)*dz-0.5_WP*Lz
  end do
  
  ! Create the cell centered grid
  do i=1,nx
     xm(i)=0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)=0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)=0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask=0
  
  return
end subroutine myjet_grid


! =================== !
! Generate the inflow !
! =================== !
subroutine myjet_inflow
  use myjet
  use parser
  implicit none
  
  real(WP) :: Yjet,Djet,Ujet
  integer :: j,k
  
  ! Only if not periodic
  if (xper.eq.1) return
  
  ! Generate an inflow file with levelset
  nvar_inflow = 3
  ntime = 2
  
  ! Allocate some arrays
  allocate(inflow(ntime,ny,nz,nvar_inflow))
  allocate(names_inflow(nvar_inflow))
  
  ! Link the pointers
  U => inflow(:,:,:,1); names_inflow(1) = 'U'
  V => inflow(:,:,:,2); names_inflow(2) = 'V'
  W => inflow(:,:,:,3); names_inflow(3) = 'W'
  
  ! Compute time grid
  time_inflow = 0.0_WP
  dt_inflow   = 1.0_WP
  
  ! Profiles parameters
  Yjet=Ly/2.0_WP
  call parser_read('Djet',Djet,0.05_WP)
  call parser_read('Ujet',Ujet,1.0_WP)
  
  ! Prepare the profiles
  V=0.0_WP
  W=0.0_WP
  do j=1,ny
     do k=1,nz
        U(:,j,k)=Ujet*exp(-(ym(j)-Yjet)**2/Djet**2)
     end do
  end do
  
  return
end subroutine myjet_inflow


! ========================= !
! Create the variable array !
! ========================= !
subroutine myjet_data
  use myjet
  use parser
  use math
  use string
  implicit none
  
  ! Allocate the array data
  nvar=4
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U => data(:,:,:,1); names(1) = 'U'
  V => data(:,:,:,2); names(2) = 'V'
  W => data(:,:,:,3); names(3) = 'W'
  P => data(:,:,:,4); names(4) = 'P'
  
  ! Velocity
  U=0.0_WP
  V=0.0_WP
  W=0.0_WP
  P=0.0_WP
  
  return
end subroutine myjet_data
