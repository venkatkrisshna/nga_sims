! ========================================= !
! Flow initialization for bedload transport !
! Follows configuration of Aussillous et al !
! Journal of Fluid Mechanics 2013           !
! ========================================= !
module bedload
  use precision
  use param
  implicit none
  
  ! Length and diameter of the domain
  real(WP) :: Lx,Ly,Lz
  real(WP) :: dx,dy,dz
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: RHO
  real(WP), dimension(:,:,:), pointer :: dRHO
  
end module bedload


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine bedload_grid
  use bedload
  use parser
  implicit none
  
  integer :: i,j,k
  
  ! Read in the size of the domain - Ly is channel height (walls are added after)
  call parser_read('nx',nx); call parser_read('Lx',Lx); dx=Lx/real(nx,WP)
  call parser_read('ny',ny); call parser_read('Ly',Ly); dy=Ly/real(ny,WP); ny=ny+2 
  call parser_read('nz',nz); call parser_read('Lz',Lz); dz=Lz/real(nz,WP)
  
  ! Set the periodicity
  xper=1
  yper=0
  zper=1
  
  ! Cartesian
  icyl=0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*dx-0.5_WP*Lx
  end do
  do j=1,ny+1
     ! First cell in the wall
     y(j) = real(j-2,WP)*dy
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*dz-0.5_WP*Lz
  end do
  
  ! Create the cell centered grid
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  mask(:,1)  = 1
  mask(:,ny) = 1
  
  return
end subroutine bedload_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine bedload_data
  use bedload
  use parser
  use math
  use random
  implicit none
  
  integer :: i,j,k
  real(WP) :: Usup,delta

  ! Bulk flow
  call parser_read('Superficial flow',Usup)
    
  ! Allocate the array data
  nvar = 6
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U    => data(:,:,:,1); names(1) = 'U'
  V    => data(:,:,:,2); names(2) = 'V'
  W    => data(:,:,:,3); names(3) = 'W'
  P    => data(:,:,:,4); names(4) = 'P'
  RHO  => data(:,:,:,5); names(5) = 'RHO'
  dRHO => data(:,:,:,6); names(6) = 'dRHO'
  
  ! Initialize
  U    = 0.0_WP
  V    = 0.0_WP
  W    = 0.0_WP
  P    = 0.0_WP
  RHO  = 0.0_WP
  dRHO = 0.0_WP
  
  ! Characteristic width
  delta=0.5_WP*Ly
  
  ! Initialize the velocity with Poiseuille profile
  do j=1,ny
     U(:,j,:) = 3.0_WP*Usup*ym(j)/delta-3.0_WP*Usup*ym(j)**2/(2.0_WP*delta**2)
  end do
  
  return
end subroutine bedload_data


! ========================================= !
! Generate an initial particle distribution !
! ========================================= !
subroutine bedload_part
  use bedload
  use parser
  use lpt_mod
  use random
  implicit none
  
  character(len=str_medium) :: part_init
  logical :: use_lpt
  integer :: iunit,ierr
  integer :: i,np
  integer :: ip,jp,kp
  real(WP) :: dp,rand
  real(WP) :: dt,time

  ! Check whether lpt is used
  call parser_is_defined('Init part file',use_lpt)
  if (.not.use_lpt) return
  
  ! Read particle diameter
  call parser_read('Particle diameter',dp)
  
  ! Get number of particles
  call parser_read('Particle number',np)
  
  ! Allocate particle array
  allocate(part(1:np))
  
  ! Distribute particles
  do i=1,np
     
     ! Set particle size
     part(i)%d = dp
     
     ! X location
     call random_number(rand)
     part(i)%x = Lx*(rand-0.5_WP)
     
     ! No walls
     if (nz.eq.1) then
        call random_number(rand)
        part(i)%y = Ly*rand
        part(i)%z = 0.0_WP
     else
        call random_number(rand)
        part(i)%y = Ly*rand
        call random_number(rand)
        part(i)%z = Lz*(rand-0.5_WP)
     end if
  end do
  
  ! Set particle parameters
  do i=1,np
     
     ! Set various parameters for the particle
     part(i)%dt  =0.0_WP
     part(i)%Acol=0.0_WP
     part(i)%Tcol=0.0_WP
     part(i)%wx  =0.0_WP
     part(i)%wy  =0.0_WP
     part(i)%wz  =0.0_WP
     part(i)%id  =int8(i)
     part(i)%stop=0
     
     ! Velocity
     part(i)%u = 0.0_WP
     part(i)%v = 0.0_WP
     part(i)%w = 0.0_WP
     
  end do
  
  ! Generate the initial particle file
  call parser_read('Init part file',part_init)
  call BINARY_FILE_OPEN(iunit,trim(part_init),"w",ierr)
  call BINARY_FILE_WRITE(iunit,np,1,kind(np),ierr)
  call BINARY_FILE_WRITE(iunit,part_kind,1,kind(part_kind),ierr)
  dt = 0.0_WP
  call BINARY_FILE_WRITE(iunit,dt,1,kind(dt),ierr)
  time = 0.0_WP
  call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr)
  do i=1,np
     call BINARY_FILE_WRITE(iunit,part(i),1,part_kind,ierr)
  end do
  call BINARY_FILE_CLOSE(iunit,ierr)
  
  return
end subroutine bedload_part
