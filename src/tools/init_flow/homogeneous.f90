module homogeneous
  use precision
  use param
  use parser
  use string
  implicit none
  
! This module creates an homogeneous reactor with size 1 in all direction to check 
  
  ! Length of the domain
  real(WP) :: L
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: RHO
  real(WP), dimension(:,:,:), pointer :: dRHO
  real(WP), dimension(:,:,:), pointer :: T
  real(WP), dimension(:,:,:), pointer :: enthalpy

end module homogeneous

! ==================== !
! Create the grid/mesh !
! ====================! 
subroutine homogeneous_grid
  use homogeneous
  implicit none
  
  integer :: i,j,k
  real(WP) :: dx,dy,dz

  ! Read in the size of the domain
  call parser_read('Domain size',L)
  nx = 1
  ny = 1
  nz = 1

  ! Periodicity in all directions
  xper = 1
  yper = 1
  zper = 1  

  ! Set to Cartesian grid
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  ! Controls whether there is a wall
  allocate(mask(nx,ny))
  
  ! Create the grid
  dx = L/nx
  do i=1,nx+1
     x(i) = (i-1)*L/nx
  end do
  dy = L/nx
  do j=1,ny+1
     y(j) = (j-1)*L/ny
  end do
  dz = L/nz
  do k=1,nz+1
     z(k) = (k-1)*L/nz
  end do
  
  ! Create the mid points
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do

  ! Create the masks
  ! Zero needed for periodic case. Set them to one for walls in domain
  mask = 0 
  
  return
end subroutine homogeneous_grid

! ========================= !
! Create the variable array !
! ========================= !
subroutine homogeneous_data
  use homogeneous
  implicit none

  character(len=str_medium), dimension(:), pointer :: spname
  integer :: i
  integer :: Nsp,NT,isc_1
  logical :: isdefined,isspecies
  real(WP), parameter :: R_cst = 8.314
  real(WP) :: val,T_init,P_init
  real(WP), dimension(:,:,:), pointer :: sum_Y,Wmix
  real(WP), dimension(:), pointer :: W_sp,Cp_sp,h_sp

  ! Get number of unsteady species
  call getnspecies(Nsp)
  
  ! Set number of variables
  nvar = Nsp + 8
  isc_1= 9
  
  ! Allocate the array data + misc.
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  allocate(sum_Y(nx,ny,nz),Wmix(nx,ny,nz))
  allocate(W_sp(Nsp),h_sp(Nsp),Cp_sp(Nsp))
  
  ! Link the pointers
  U       => data(:,:,:,1); names(1) = 'U'
  V       => data(:,:,:,2); names(2) = 'V'
  W       => data(:,:,:,3); names(3) = 'W'
  P       => data(:,:,:,4); names(4) = 'P'
  RHO     => data(:,:,:,5); names(5) = 'RHO'
  dRHO    => data(:,:,:,6); names(6) = 'dRHO'
  T       => data(:,:,:,7); names(7) = 'T'
  ENTHALPY=> data(:,:,:,8); names(8) = 'ENTHALPY'
  
  ! Zero all fields
  data = 0.0_WP
  
  ! Initialize scalar names
  allocate(spname(Nsp))
  call GETSPECIESNAMES(spname)
  do i=isc_1,isc_1+Nsp-1
     names(i)=trim(spname(i-isc_1+1))
  end do

  ! Set temperature as Nsp+1 scalar
  NT=Nsp+1
  
  ! Initialize scalar fields (mass fractions)
  isspecies = .false.
  do i=isc_1,isc_1+Nsp-1
     names(i) = trim(spname(i-isc_1+1))
     ! Check if this scalar is defined
     call parser_is_defined('Gas ' // trim(names(i)), isdefined)
     if (isdefined) then
        isspecies=.true.
        call parser_read('Gas ' // trim(names(i)), val)
        data(:,:,:,i) = val
     end if
  end do
  if (.not.isspecies) then
     write (*,*) '------------------------------------------'
     write(*,*) 'No species initialized: check input file !'
  end if

  ! Normalize mass fractions
  sum_Y = 0.0_WP
  do i = isc_1,isc_1+Nsp-1
     sum_Y = sum_Y + data(:,:,:,i)
  end do
  if ((maxval(sum_Y).ne.1.0_WP).or.(minval(sum_Y).ne.1.0_WP)) then
     write (*,*) 'Rescaling mass fractions to make sum 1.0:',sum_Y
     do i = isc_1,isc_1+Nsp-1
        data(:,:,:,i)=data(:,:,:,i)/sum_Y
     end do
  end if

  ! --- Initial temperature --- !
  call parser_read('Temperature',T_init,300.0_WP)
  !data(:,:,:,isc_1+NT) = T_init
  T(:,:,:) = T_init

  ! --- Initial mixture molecular weight --- !                                                                      
  call getmolarmass(W_sp)
  Wmix=0.0_WP
  do i = isc_1,isc_1+Nsp-1
     Wmix=real(data(:,:,:,i)/W_sp(i-isc_1+1),WP)+Wmix
  end do
  Wmix=1/Wmix
  write (*,*) 'Mean W mixture  : ', sum(Wmix)/real(nx*ny*nz,WP)

  ! --- Initial thermodynamic pressure --- !                                                                        
  call parser_read('Pressure',P_init,101300.0_WP)

  ! --- Initial density ( from ideal gas assumption) --- !                                                          
  RHO = P_init*Wmix/(T_init*R_cst)
  write (*,*) 'Mean density : ', sum(RHO)/real(nx*ny*nz,WP)

  ! --- Initial enthalpy --- !                                                                                      
  enthalpy = 0.0_WP
  call COMPTHERMODATA(h_sp,Cp_sp,T_init)
  do i = isc_1,isc_1+Nsp-1
     ENTHALPY = ENTHALPY + h_sp(i-isc_1+1)*RHO*T
  end do
  write (*,*) 'Mean enthalpy : ', sum(ENTHALPY)/real(nx*ny*nz,WP)

  return
end subroutine homogeneous_data

