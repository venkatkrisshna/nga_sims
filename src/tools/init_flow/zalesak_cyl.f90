module zalesak_cyl
  use string
  use precision
  use param
  implicit none
  
  ! Length of the domain
  real(WP) :: L
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: G
  
end module zalesak_cyl

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine zalesak_cyl_grid
  use zalesak_cyl
  use parser
  implicit none
  integer :: i,j,k
  real(WP) :: twopi,zL,xL

  ! Create 2*pi
  twopi = 2.0_WP * acos(-1.0_WP)
  zL = twopi
  
  ! Read in the size of the domain
  call parser_read('Number of points in y',ny)
  call parser_read('Number of points in z',nz)
  call parser_read('Radius',L)
  nx = 1
  xL = L/real(ny,WP)
  
  ! Set the periodicity
  xper = 1
  yper = 0
  zper = 1
  
  ! Cylindrical
  icyl = 1
  
  ! Allocate the arrays
  allocate(x(nx+1) ,y(ny+1) ,z(nz+1))
  allocate(xm(nx+1),ym(ny+1),zm(nz+1))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*xL/real(nx,WP) - 0.5_WP*xL
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*L/real(ny,WP)
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*zL/real(nz,WP)
  end do
  
  ! Create the mid points
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine zalesak_cyl_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine zalesak_cyl_data
  use zalesak_cyl
  use parser
  implicit none
  integer :: i,j,k
  real(WP), dimension(3) :: xyz
  real(WP), dimension(:), allocatable :: center
  real(WP) :: radius,width,height,thick
  real(WP) :: ang_vel
  real(WP) :: levelset_notched_circle
  
  ! Read input
  call parser_read('Initial G thickness',thick,-1.0_WP)
  call parser_getsize('Circle center',i)
  allocate(center(i))
  call parser_read('Circle center',center)
  call parser_read('Circle radius',radius)
  call parser_read('Notch width',width)
  call parser_read('Notch height',height)
  call parser_read('Angular velocity',ang_vel)
  
  ! Allocate the array data
  nvar = 4
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U => data(:,:,:,1); names(1) = 'U'
  V => data(:,:,:,2); names(2) = 'V'
  W => data(:,:,:,3); names(3) = 'W'
  G => data(:,:,:,4); names(4) = 'G'
  
  ! Set velocity
  do k=1,nz
     do j=1,ny
        do i=1,nx
           U(i,j,k) =  0.0_WP
           V(i,j,k) =  0.0_WP
           W(i,j,k) = ang_vel*ym(j)
        end do
     end do
  end do
  
  ! Set G
  do k=1,nz
     do j=1,ny
        do i=1,nx
           xyz = (/ym(j)*cos(zm(k)),ym(j)*sin(zm(k)),0.0_WP/)
           G(i,j,k) = levelset_notched_circle(xyz,center,radius,width,height)
        end do
     end do
  end do
  
  ! Convert to tanh profile
  if (thick.gt.0.0_WP) then
     do k=1,nz
        do j=1,ny
           do i=1,nx
              G(i,j,k) = 0.5_WP*(tanh(0.5_WP*G(i,j,k)/thick)+1.0_WP)
           end do
        end do
     end do
  end if
  
  return
end subroutine zalesak_cyl_data
