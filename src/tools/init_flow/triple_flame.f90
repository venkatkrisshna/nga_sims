module triple_flame
  use precision
  use param
  implicit none

  ! Length of the domain (assumed square)
  real(WP) :: L
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: RHO
  real(WP), dimension(:,:,:), pointer :: dRHO
  real(WP), dimension(:,:,:), pointer :: T
  real(WP), dimension(:,:,:), pointer :: ENTHALPY

  ! Chemistry-specific variables
  integer :: Nsp
  character(len=str_medium), dimension(:), pointer :: spname
  real(WP), dimension(:,:), pointer :: Yin
  real(WP) :: Tin

  ! Constants
  real(WP), parameter :: R_cst = 8.314_WP
  
end module triple_flame

! ------------------------------------------------------------------ !
! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine triple_flame_grid
  use triple_flame
  use parser
  implicit none
  
  integer  :: i,j,k
  real(WP) :: dx
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  ny = nx
  nz = 1
  call parser_read('Length',L)
  dx = L/real(nx,WP)
  
  ! Set the periodicity
  xper = 0
  yper = 0
  zper = 1

  ! Cartesian
  icyl = 0

  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = (i-1)*dx
  end do
  do j=1,ny+1
     y(j) = (j-1)*dx
  end do
  do k=1,nz+1
     z(k) = (k-1)*dx
  end do
  
  ! Center the domain
  y = y-0.5_WP*(y(ny+1)-y(1))
  z = z-0.5_WP*(z(nz+1)-z(1))
  
  ! Create the mid points 
  do i=1,nx
     xm(i)= 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)= 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! No mask in domain
  mask = 0

  return
end subroutine triple_flame_grid


! ------------------------------------------------------------------ !
! ========================= !
! Create the variable array !
! ========================= !
subroutine triple_flame_data
  use triple_flame
  use parser
  implicit none

  ! Miscellaneous
  integer  :: i,j,k,iO2,iCO2,iN2,iH2O,jloc
  integer :: isc_1,nf,ns,Nreac,Ntot
  real(WP) :: Xair,NT,AA,BB,CC,mm,pp,qq,acoeff,bcoeff
  real(WP) :: dx,xloc,ksiR,ksi,phiL,phiR,P_init,phi
  real(WP) :: Wf,Wo,WR,WL
  real(WP), dimension(:), pointer :: Wb
  real(WP), dimension(:,:,:), pointer :: Wmix
  character(len=str_short) :: check
  real(WP), dimension(:), pointer :: Wsp,Cpsp,hsp
  ! Optional velocity/composition fluctuations
  real(WP) :: fluct,rand
  ! Composition of fuel stream
  real(WP), dimension(:), pointer :: spf 
  ! Index of relevant fuel species in species array
  character(len=str_short), dimension(:), pointer :: spinit
  integer, dimension(:), pointer :: isp
  ! Atomic formula of fuel components
  real(WP), dimension(:,:), pointer :: spc 
  real(WP), dimension(:), pointer :: spctmp 
  ! Composition temp arrays
  real(WP), dimension(:,:), pointer :: Yb
  real(WP), dimension(:), pointer :: Yf,Yo,YR,YL
  real(WP) :: Tb
  ! Mixture fractions variables
  real(WP), dimension(3) :: Za
  real(WP) :: Zo,Zf,ZR,ZL 
  ! Flame front position
  real(WP) :: loc
  
  ! Various fixed quantities of interest
  Xair = 0.79_WP/0.21_WP
  
  ! Init random module
  call random_init
       
  ! Get number of species
  call getnspecies_all(Ntot)
  
  ! Get number of unsteady species
  call getnspecies(Nsp)
  
  ! Set number of variables
  nvar = Nsp + 8
  isc_1= 9
  
  ! Allocate the array data
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U       => data(:,:,:,1); names(1) = 'U'
  V       => data(:,:,:,2); names(2) = 'V'
  W       => data(:,:,:,3); names(3) = 'W'
  P       => data(:,:,:,4); names(4) = 'P'
  RHO     => data(:,:,:,5); names(5) = 'RHO'
  dRHO    => data(:,:,:,6); names(6) = 'dRHO'
  T       => data(:,:,:,7); names(7) = 'T'
  ENTHALPY=> data(:,:,:,8); names(8) = 'ENTHALPY'
  
  ! Zero all fields
  data = 0.0_WP
  
  ! Initialize scalar names
  allocate(spname(Ntot))
  call getspeciesnames(spname)
  do i=isc_1,isc_1+Nsp-1
     names(i)=trim(spname(i-isc_1+1))
  end do
 
  ! Set temperature as Nsp+1 scalar
  NT=Nsp+1
  
  ! Get molecular masses of species
  allocate(Wsp(Ntot))
  call getmolarmass(Wsp)
  
  ! Get number of reactions
  call getnreactions(Nreac)

  ! Allocate Cp and h for unsteady species
  allocate(Cpsp(Nsp))
  allocate(hsp(Nsp))

  ! Get fuel components and other relevant species 
  call parser_getsize('Fuels',nf)

  ! Allocate arrays
  allocate(Yin(ny,Nsp))
  allocate(Yb(ny,Nsp))
  allocate(Wb(ny))
  allocate(Wmix(nx,ny,nz))
  allocate(Yo(Nsp))
  allocate(Yf(Nsp))
  allocate(YL(Nsp))
  allocate(YR(Nsp))
  allocate(spf(nf)) 
  allocate(spinit(nf+4))
  allocate(spc(nf,3)) 
  allocate(spctmp(nf*3))

  ! Get info from input file
  call parser_read('Fuels',spinit(1:nf))
  call parser_read('Fuel composition',spf(nf)) ! Mole fractions!
  call parser_read('Inert',spinit(nf+1),'N2')
  call parser_read('Fuels formula',spctmp)

  ! Recast linear in matrix
  k = 1
  do i=1,nf
     do j=1,3
        spc(i,j) = spctmp(k)
        k = k+1
     end do
  end do
 
  spf = spf/sum(spf)
  spinit(nf+2) = 'O2'
  spinit(nf+3) = 'CO2'
  spinit(nf+4) = 'H2O'
  ns = nf+4
  iN2 = nf+1
  iO2 = nf+2
  iCO2 = nf+3
  iH2O = nf+4

  ! Find indices of species relevant for initialization
  allocate(isp(ns))
  isp = 0
  do i=isc_1,isc_1+Nsp-1
     do j=1,ns
        check = spinit(j)
        if (trim(names(i)).eq.trim(check)) isp(j) = i-isc_1+1
     end do
  end do
  
  ! Check everything has been found
  do j=1,ns
     if (isp(j)==0) then
        print*,"Didn't find species ",spinit(j)
        stop
     end if
  end do
  
  do i=1,nf
     print*, '----',i,'---- (',Nsp,')'
     print*, 'fuels   ',spinit(i)
     print*, 'ifuels ',isp(i)
     print*,'spc ',spc(i,:)
     print*,'spf ',spf(i)
  end do

  ! Global reaction
  ! Look for it in the form C_AH_BO_C + m O2 -> p CO2 + q H2O
  AA = 0.0_WP
  BB = 0.0_WP
  CC = 0.0_WP
  do i=1,nf
     AA = AA + spf(i)*spc(i,1)
     BB = BB + spf(i)*spc(i,2)
     CC = CC + spf(i)*spc(i,3)
  end do
  mm = AA + BB/4.0_WP - CC/2.0_WP
  pp = AA
  qq = BB/2.0_WP
     
  ! Get inflow conditions
  call parser_read('Phi left',phiL)
  call parser_read('Phi right',phiR)
  
  ! Oxidizer limit stream
  Yo = 0.0_WP
  Yo(isp(iO2)) = 0.21_WP
  Yo(isp(iN2)) = 0.79_WP
  Wo = 0.21_WP*0.032_WP+0.79_WP*0.028_WP
  Yo = Yo*Wsp/Wo

  ! Fuel limit stream
  Yf = 0.0_WP
  Wf = 0.0_WP
  do i=1,nf
     Yf(isp(i)) = spf(i)
     Wf = Wf + Wsp(isp(i))*spf(i)
  end do
  Yf = Yf*Wsp/Wf

  ! Get corresponding composition on the left
  YL = 0.0_WP
  YL(isp(iO2)) = 1.0_WP/(1.0_WP+Xair+phiL/mm)
  YL(isp(iN2)) = 1.0_WP - YL(isp(iO2))  
  do i=1,nf
     YL(isp(i)) = phiL/mm*YL(isp(iO2))*spf(i)
     YL(isp(iN2)) = YL(isp(iN2)) - YL(isp(i)) 
  end do
  ! Molar mass
  WL = 0.0_WP
  do i=1,Nsp
     WL = WL+YL(i)*Wsp(i)
  end do
  ! Get mass fractions
  YL = YL*Wsp/WL  
  ! Get corresponding Bilger mixture fraction on the left
  ZL = 0.0_WP
  Za = 0.0_WP ! 1:C, 2:H, 3:O
  ! Start with oxygen species from oxidizer
  Za(3) = YL(isp(iO2))
  ! Keep track of mixture fraction of pure oxidizer
  Zo = - 1.0_WP/11.0_WP*Za(3)/16.0_WP
  ! Add contributions from the fuel
  do i=1,nf
     Za(1) = Za(1) + spc(i,1)*12.0_WP/Wsp(isp(i))*YL(isp(i))
     Za(2) = Za(2) + spc(i,2)*1.0_WP/Wsp(isp(i))*YL(isp(i))
     Za(3) = Za(3) + spc(i,3)*16.0_WP/Wsp(isp(i))*YL(isp(i))
  end do
  ! Get Bilger mixture fraction
  ZL = 1.0_WP/7.0_WP*Za(1)/12.0_WP + 1.0_WP/16.0_WP*Za(2)/1.0_WP - 1.0_WP/11.0_WP*Za(3)/16.0_WP

  ! Get corresponding composition on the right
  YR = 0.0_WP
  YR(isp(iO2)) = 1/(1+Xair+phiR/mm)
  YR(isp(iN2)) = 1.0_WP - YR(isp(iO2))  
  do i=1,nf
     YR(isp(i)) = phiR/mm*YR(isp(iO2))*spf(i)
     YR(isp(iN2)) = YR(isp(iN2)) - YR(isp(i)) 
  end do
  ! Molar mass
  WR = 0.0_WP
  do i=1,Nsp
     WR = WR+YR(i)*Wsp(i)
  end do
  ! Get mass fractions
  YR = YR*Wsp/WR  
  ! Get corresponding Bilger mixture fraction on the right
  ZR = 0.0_WP
  Za = 0.0_WP ! 1:C, 2:H, 3:O
  do i=1,nf
     Za(1) = Za(1) + spc(i,1)*12.0_WP/Wsp(isp(i))*YR(isp(i))
     Za(2) = Za(2) + spc(i,2)*1.0_WP/Wsp(isp(i))*YR(isp(i))
     Za(3) = Za(3) + spc(i,3)*16.0_WP/Wsp(isp(i))*YR(isp(i))
  end do
  ! Keep track of mixture fraction of pure fuel
  Zf = 1.0_WP/7.0_WP*Za(1)/12.0_WP + 1.0_WP/16.0_WP*Za(2)/1.0_WP - 1.0_WP/11.0_WP*Za(3)/16.0_WP
  ! Add oxygen species to it
  Za(3) = YR(isp(iO2))
  ! Get Bilger mixture fraction
  ZR = 1.0_WP/7.0_WP*Za(1)/12.0_WP + 1.0_WP/16.0_WP*Za(2)/1.0_WP - 1.0_WP/11.0_WP*Za(3)/16.0_WP

  ! Get normalized mixture fraction value on the fuel side
  ksiR = (ZR-Zo)/(Zf-Zo)
    
  ! Compute parameters for tanh profile
  bcoeff = atanh(ksiR-1.0_WP)
  acoeff = 2.0_WP/L*(atanh(2*ksiR-1)-bcoeff)

  ! Initialize all points along x=0 line (mass fractions)
  do j=1,ny
     ksi = 0.5_WP * tanh(acoeff*ym(j)+bcoeff) + 0.5_WP
     Yin(ny-j+1,:) = (1.0_WP-ksi)*YL + ksi*YR
     phi = mm*0.032_WP/Wf*Yin(ny-j+1,isp(1))/max(1e-15_WP,Yin(ny-j+1,isp(iO2)))
     print*,ym(ny-j+1),phi,ksi
  end do
  
  ! Burnt stoichiometric mixture (outlet initial composition)
  Yb = 0.0_WP
  Yb(:,isp(iCO2)) = pp/(pp+qq+mm*Xair)
  Yb(:,isp(iH2O)) = qq/(pp+qq+mm*Xair)
  Yb(:,isp(iN2)) = mm*Xair/(pp+qq+mm*Xair)
  Wb = 0.0_WP
  do i=1,Nsp
     Wb = Wb + Yb(:,i)*Wsp(i)
  end do
  ! Get mass fractions
  do j=1,ny
     Yb(ny-j+1,:) = Yb(ny-j+1,:)*Wsp/Wb(j)
  end do
  
  ! Inlet and outlet temperatures
  call parser_read('Inlet temperature',Tin)
  call parser_read('Outlet temperature',Tb)
  
  ! Initialize species data
  call parser_read('Front location',loc,0.25_WP)
  xloc = xm(int(loc*real(nx,WP)))
  dx   = 4.0_WP*L/real(nx,WP)
  do i=1,nx
     do j=isc_1,isc_1+Nsp-1
        jloc = j-isc_1+1
        do k=1,nz
           data(i,:,k,j) = Yin(:,jloc) + (Yb(:,jloc) - Yin(:,jloc)) * 0.5_WP*(1.0_WP+tanh((xm(i)-xloc)/dx))
        end do
     end do
     T(i,:,:) = Tin + (Tb - Tin) * 0.5_WP*(1.0_WP+tanh((xm(i)-xloc)/dx))
  end do
  
  ! Allow for velocity fluctuations
  call parser_read('Velocity fluctuations',fluct,0.0_WP)
  do k=1,nz
     do j=1,ny
        do i=1,nx
           call random_number(rand)
           if (nx.gt.1) U(i,j,k)=U(i,j,k)+fluct*(2.0_WP*rand-1.0_WP)
           call random_number(rand)
           if (ny.gt.1) V(i,j,k)=V(i,j,k)+fluct*(2.0_WP*rand-1.0_WP)
           call random_number(rand)
           if (nz.gt.1) W(i,j,k)=W(i,j,k)+fluct*(2.0_WP*rand-1.0_WP)
        end do
     end do
  end do

  ! --- Initial mixture molecular weight --- !                                                                      
  Wmix=0.0_WP
  do i = isc_1,isc_1+Nsp-1
     Wmix=real(data(:,:,:,i)/Wsp(i-isc_1+1),WP)+Wmix
  end do
  Wmix=1/Wmix
 
  ! --- Initial thermodynamic pressure --- !                                                                        
  call parser_read('Pressure',P_init,101300.0_WP)

  ! --- Initial density ( from ideal gas assumption) --- !                                                          
  RHO = P_init*Wmix/(T*R_cst)
 
  ! --- Initial enthalpy --- !                                                                                      
  enthalpy = 0.0_WP
  do i=1,nx
     do j=1,ny
        call compthermodata(hsp,Cpsp,T(i,j,1))
        do k = isc_1,isc_1+Nsp-1
           ENTHALPY(i,j,:) = ENTHALPY(i,j,:) + hsp(k-isc_1+1)*RHO(i,j,:)*T(i,j,:)
        end do
     end do
  end do

  return
end subroutine triple_flame_data


! =================== !
! Generate the inflow !
! =================== !
subroutine triple_flame_inflow
  use triple_flame
  use parser
  implicit none
  
  integer :: i,j
  logical :: isdef
  real(WP) :: Uin
  character(len=str_medium) :: name
  
  ! Test if required
  call parser_is_defined('Init inflow file',isdef)
  if (.not.isdef) return

  ! Generate an inflow file for velocity, species and temperature
  nvar_inflow = 3 + Nsp + 1
  ntime = 2
  
  ! Get velocity value from input file
  call parser_read('U inflow',Uin)

  ! Allocate some arrays
  allocate(inflow(ntime,ny,nz,nvar_inflow))
  inflow = 0.0_WP
  allocate(names_inflow(nvar_inflow))
    
  ! Link the pointers, create variable names
  U => inflow(:,:,:,1); names_inflow(1) = 'U'
  V => inflow(:,:,:,2); names_inflow(2) = 'V'
  W => inflow(:,:,:,3); names_inflow(3) = 'W'
  do i=1,Nsp
     name=spname(i)
     names_inflow(3+i) = trim(name(1:str_short))
  end do
  names_inflow(3+Nsp+1) = "T"

  ! Compute time grid
  time_inflow = 0.0_WP
  dt_inflow   = 1.0_WP

  ! Velocity inflow
  U = Uin
  V = 0.0_WP
  W = 0.0_WP

  ! Scalar inflow
  do j=1,ny
     do i=1,Nsp
        inflow(:,j,:,3+i) = Yin(j,i)
     end do
     inflow(:,j,:,3+Nsp+1) = Tin
  end do
  
  return
end subroutine triple_flame_inflow
