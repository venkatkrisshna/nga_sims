module vortex_particle
  use string
  use precision
  use param
  implicit none
  
  ! Size of the domain
  real(WP) :: Lx,Ly,Lz
  real(WP) :: dx,dy,dz
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: RHO
  real(WP), dimension(:,:,:), pointer :: dRHO
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  
end module vortex_particle


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine vortex_particle_grid
  use vortex_particle
  use parser
  use math
  implicit none
  
  integer :: i,j,k
  
  ! Cartesian geometry
  icyl=0
  
  ! Periodicity
  xper=0
  yper=1
  zper=1
  
  ! Domain size
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  call parser_read('Lz',Lz)
  dx=Lx/real(nx,WP)
  dy=Ly/real(ny,WP)
  dz=Lz/real(nz,WP)
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i)=real(i-2,WP)*dx
  end do
  do j=1,ny+1
     y(j)=real(j-1,WP)*dy-0.5_WP*Ly
  end do
  do k=1,nz+1
     z(k)=real(k-1,WP)*dz-0.5_WP*Lz
  end do
  
  ! Create the cell centered grid
  do i=1,nx
     xm(i)=0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)=0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)=0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask=0
  mask(1,:)=1
  
  return
end subroutine vortex_particle_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine vortex_particle_data
  use vortex_particle
  use parser
  use math
  implicit none
  
  integer  :: i,j,k
  real(WP) :: vheight,vint,vradius,vscal
  real(WP) :: s
  
  ! Allocate the array data
  nvar=6
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U    => data(:,:,:,1); names(1) = 'U'
  V    => data(:,:,:,2); names(2) = 'V'
  W    => data(:,:,:,3); names(3) = 'W'
  P    => data(:,:,:,4); names(4) = 'P'
  RHO  => data(:,:,:,5); names(5) = 'RHO'
  dRHO => data(:,:,:,6); names(6) = 'dRHO'
  
  ! Initialize
  U = 0.0_WP
  V = 0.0_WP
  W = 0.0_WP
  P = 0.0_WP
  RHO=1.0_WP
  dRHO=0.0_WP
  
  ! Generate the vortex
  call parser_read('Vortex height',vheight)
  call parser_read('Vortex radius',vradius)
  call parser_read('Vortex intensity',vint)
  call parser_read('Vortex scaling',vscal)
  do k=1,nz
     do j=1,ny
        do i=1,nx
           ! U
           !s = sqrt((x(i)-vheight)**2+(sqrt(ym(j)**2+zm(k)**2)-vradius)**2)+epsilon(1.0_WP)
           !U(i,j,k) = +1.0_WP/(twoPi*s)*(1.0_WP-exp(-s**2/vint**2))*(sqrt(ym(j)**2+zm(k)**2)-vradius)/s
           ! V
           !s = sqrt((xm(i)-vheight)**2+(sqrt(y(j)**2+zm(k)**2)-vradius)**2)+epsilon(1.0_WP)
           !V(i,j,k) = -1.0_WP/(twoPi*s)*(1.0_WP-exp(-s**2/vint**2))*(xm(i)-vheight)/s*y(j)/(sqrt(y(j)**2+zm(k)**2)+epsilon(1.0_WP))
           ! W
           !s = sqrt((xm(i)-vheight)**2+(sqrt(ym(j)**2+z(k)**2)-vradius)**2)+epsilon(1.0_WP)
           !W(i,j,k) = -1.0_WP/(twoPi*s)*(1.0_WP-exp(-s**2/vint**2))*(xm(i)-vheight)/s*z(k)/(sqrt(ym(j)**2+z(k)**2)+epsilon(1.0_WP))
        end do
     end do
  end do
  
  do k=1,nz
     do j=1,ny
        do i=1,nx
           if (x(i).gt.0.01_WP) then
              ! U
              s = sqrt((x(i)-vheight)**2+(sqrt(ym(j)**2)-vradius)**2)+epsilon(1.0_WP)
              U(i,j,k) = +vscal/(twoPi*s)*(1.0_WP-exp(-s**2/vint**2))*(sqrt(ym(j)**2)-vradius)/s
              ! V
              s = sqrt((xm(i)-vheight)**2+(sqrt(y(j)**2)-vradius)**2)+epsilon(1.0_WP)
              V(i,j,k) = -vscal/(twoPi*s)*(1.0_WP-exp(-s**2/vint**2))*(xm(i)-vheight)/s*y(j)/(sqrt(y(j)**2)+epsilon(1.0_WP))
           end if
        end do
     end do
  end do
  
  return
end subroutine vortex_particle_data


! ========================================= !
! Generate an initial particle distribution !
! ========================================= !
subroutine vortex_particle_part
  use vortex_particle
  use parser
  use lpt_mod
  use random
  use math
  implicit none
  
  logical :: use_lpt
  integer :: iunit,ierr
  integer :: i,np
  real(WP) :: H,d,rand,dt,time,Vbed
  character(len=str_medium) :: part_init
  
  ! Check whether lpt is used
  call parser_is_defined('Init part file',use_lpt)
  if (.not.use_lpt) return
  
  ! Initialize the random number generator
  call random_init
  
  ! Read diameter and bed height
  call parser_read('Bed height',H)
  call parser_read('Particle mean diameter',d)
  
  ! Compute number of particles
  np=floor(0.63*H*Ly*Lz/((Pi/6.0_WP)*d**3))
  print*,'np=',np
  
  ! Allocate data
  allocate(part(1:np))
  
  ! Set particle parameters
  do i=1,np
     ! Diameter
     part(i)%d   =d
     ! Random position
     call random_number(rand)
     part(i)%x   =x(2)+rand*(x(nx+1)-x(2))
     call random_number(rand)
     part(i)%y   =y(1)+rand*(y(ny+1)-y(1))
     call random_number(rand)
     part(i)%z   =z(1)+rand*(z(nz+1)-z(1))
     ! Set various parameters for the particle
     part(i)%dt  =0.0_WP
     part(i)%Acol=0.0_WP
     part(i)%Tcol=0.0_WP
     part(i)%wx  =0.0_WP
     part(i)%wy  =0.0_WP
     part(i)%wz  =0.0_WP
     part(i)%id  =int(i,kind=8)
     part(i)%stop=0
     part(i)%u   =0.0_WP
     part(i)%v   =0.0_WP
     part(i)%w   =0.0_WP
  end do
  
  ! Generate the initial particle file
  call parser_read('Init part file',part_init)
  call BINARY_FILE_OPEN(iunit,trim(part_init),"w",ierr)
  call BINARY_FILE_WRITE(iunit,np,1,kind(np),ierr)
  call BINARY_FILE_WRITE(iunit,part_kind,1,kind(part_kind),ierr)
  dt = 0.0_WP
  call BINARY_FILE_WRITE(iunit,dt,1,kind(dt),ierr)
  time = 0.0_WP
  call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr)
  do i=1,np
     call BINARY_FILE_WRITE(iunit,part(i),1,part_kind,ierr)
  end do
  call BINARY_FILE_CLOSE(iunit,ierr)

  return
end subroutine vortex_particle_part
