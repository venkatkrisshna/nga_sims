! ================================================================== !
! Random divergence-free velocity field using random stream function !
! ================================================================== !
module random_vel
  use string
  use precision
  use param
  implicit none
  
  ! Domain
  real(WP) :: L,Lx,Ly,Lz
  character(len=str_short) :: plane
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:),   pointer :: U
  real(WP), dimension(:,:,:),   pointer :: V
  real(WP), dimension(:,:,:),   pointer :: W
  real(WP), dimension(:,:,:),   pointer :: P
  real(WP), dimension(:,:,:),   pointer :: f
  real(WP), dimension(:,:,:,:), pointer :: VOF

end module random_vel

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine random_vel_grid
  use random_vel
  use parser
  use math
  implicit none
  
  integer :: i,j,k
  integer :: np
  real(WP) :: delta
  
  ! Read in the size of the domain
  call parser_read('Domain size',L,2.0_WP*pi) 
  call parser_read('Grid points',np)
  call parser_read('Domain plane',plane,'xy')

  ! Grid points
  select case(trim(plane))
  case ('xy')
     nx=np; ny=np; nz=1
  case ('xz')
     nx=np; ny= 1; nz=np
  case ('yz')
     nx= 1; ny=np; nz=np
  case default
     stop 'Unknown domain plane.  Specify xy, xz, or yz'
  end select
     
  ! Cartesian
  icyl = 0

  ! Peroidic
  xper=1
  yper=1
  zper=1
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))

  ! Domain size
  delta=L/real(np,WP)
  Lx=delta*real(nx,WP)
  Ly=delta*real(ny,WP)
  Lz=delta*real(nz,WP)

  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*delta-0.5_WP*Lx
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*delta-0.5_WP*Ly
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*delta-0.5_WP*Lz
  end do
  
  ! Create the cell centered grid
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Masks
  mask=0
  
  return
end subroutine random_vel_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine random_vel_data
  use random_vel
  use random
  use parser
  use math
  use compgeom_lookup
  implicit none

  integer :: c,i,j,k
  real(WP) :: dxi,dyi,dzi,rhoUmean,rhoVmean,rhoWmean,KE
  character(len=str_medium) :: sim_type,interface_type
  real(WP), dimension(:,:,:), allocatable :: S
  real(WP), dimension(:,:,:), allocatable :: rho_U,rho_V,rho_W
  real(WP), dimension(:,:,:,:), allocatable :: rho
  real(WP) :: rho_l,rho_g
  real(WP) :: myrho,myU,myV,myW,myvol
  
  ! Single or multiphase?
  call parser_read('Sim type',sim_type)

  ! Link the pointers
  select case(trim(sim_type))
  case ('single-phase')
     ! Allocate the array data
     nvar = 4
     allocate(data(nx,ny,nz,nvar))
     allocate(names(nvar))
     U => data(:,:,:,1); names(1) = 'U'
     V => data(:,:,:,2); names(2) = 'V'
     W => data(:,:,:,3); names(3) = 'W'
     P => data(:,:,:,4); names(4) = 'P'
  case ('multiphase')
     ! Allocate the array data
     nvar = 12
     allocate(data(nx,ny,nz,nvar))
     allocate(names(nvar))
     U      => data(:,:,:,1);  names(1)  = 'U'
     V      => data(:,:,:,2);  names(2)  = 'V'
     W      => data(:,:,:,3);  names(3)  = 'W'
     f      => data(:,:,:,4);  names(4)  = 'l_f'
     VOF  => data(:,:,:, 5:12);
     names( 5) = 'VOF1'
     names( 6) = 'VOF2'
     names( 7) = 'VOF3'
     names( 8) = 'VOF4'
     names( 9) = 'VOF5'
     names(10) = 'VOF6'
     names(11) = 'VOF7'
     names(12) = 'VOF8'
  case default
     stop "Unknown simulation type! Enter 'single-phase' or&
          & 'multiphase'"
  end select

  ! Initialize pressure or interface
  select case(trim(sim_type))
  case ('single-phase')
     P = 0.0_WP
  case ('multiphase')
     call parser_read('Interface type',interface_type)
     select case (trim(interface_type))
     case ('none')
        VOF = 0.0_WP
     case ('plane_x')
        if (plane.ne.'xy'.and.plane.ne.'xz') stop 'Use plane_y or&
             & plane_z'
        VOF=0.0_WP
        VOF(floor(0.25_WP*real(nx,WP)):floor(0.75_WP*real(nx,WP)),:,:&
             &,:) = 1.0_WP
     case ('plane_y')
        if (plane.ne.'xy'.and.plane.ne.'yz') stop 'Use plane_x or&
             & plane_z'
        VOF=0.0_WP
        VOF(:,floor(0.25_WP*real(ny,WP)):floor(0.75_WP*real(ny,WP)),:&
             &,:) = 1.0_WP
     case ('plane_z')
        if (plane.ne.'xz'.and.plane.ne.'xz') stop 'Use plane_x or&
             & plane_y'
        VOF=0.0_WP
        VOF(:,:,floor(0.25_WP*real(nz,WP)):floor(0.75_WP*real(nz,WP))&
             &,:) = 1.0_WP
     case ('square')
        VOF=0.0_WP
        if (plane.eq.'xy') then
           VOF(floor(0.25_WP*real(nx,WP)):ceiling(0.75_WP*real(nx&
                &,WP)),floor(0.25_WP*real(ny,WP)):ceiling(0.75_WP&
                &*real(ny,WP)),:,:) = 1.0_WP
        else
           stop "Square not programed for plane xz or yz cases"
        end if
     case default
        stop "Unknown interface type! Enter 'none', 'plane_x',&
             & 'plane_y', 'plane_z', or square"
     end select
     f=0.125_WP*sum(VOF,4)
  case default
     stop "Unknown simulation type! Enter 'single-phase' or&
          & 'multiphase'"
  end select

  ! Create random stream funciton on cell vertices
  allocate(S(1:nx+1,1:ny+1,1:nz+1))
  !call init_random_seed()
  do k=1,nz
     do j=1,ny
        do i=1,nx
           call random_number(S(i,j,k))
        end do
     end do
  end do

  ! Update ghost cells
  S(nx+1,:,:) = S(1,:,:) ! Right
  S(:,ny+1,:) = S(:,1,:) ! Top
  S(:,:,nz+1) = S(:,:,1) ! Front

  ! Compute velocity field from random stream function 
  ! u=ds/dy; v=-ds/dx; w=0
  dxi=1.0_WP/(x(2)-x(1))
  dyi=1.0_WP/(y(2)-y(1))
  dzi=1.0_WP/(z(2)-z(1))
  select case(trim(plane))
  case ('xy')
     do j=1,ny
        do i=1,nx
           U(i,j,:) = +(S(i,j+1,1)-S(i,j,1))*dyi
           V(i,j,:) = -(S(i+1,j,1)-S(i,j,1))*dxi
           W(i,j,:) = 0.0_WP
        end do
     end do
  case ('xz')
     do k=1,nz
        do i=1,nx
           U(i,:,k) = +(S(i,1,k+1)-S(i,1,k))*dzi
           V(i,:,k) = 0.0_WP
           W(i,:,k) = -(S(i+1,1,k)-S(i,1,k))*dxi
        end do
     end do
  case ('yz')
     do k=1,nz
        do j=1,ny
           U(:,j,k) = 0.0_WP
           V(:,j,k) = +(S(1,j,k+1)-S(1,j,k))*dzi
           W(:,j,k) = -(S(1,j+1,k)-S(1,j,k))*dyi
        end do
     end do
  case default
     stop 'Unknown plane'
  end select

  ! Construct density field
  select case(trim(sim_type))
  case ('single-phase')
     call parser_read('Density',rho_g)
     ! Compute density in velocity cells
     allocate(rho(0:nx,0:ny,0:nz,8)); rho=rho_g
     allocate(rho_U(nx,ny,nz)); rho_U=rho_g
     allocate(rho_V(nx,ny,nz)); rho_V=rho_g
     allocate(rho_W(nx,ny,nz)); rho_W=rho_g
  case ('multiphase')
     call parser_read('Liquid density',rho_l)
     call parser_read('Gas density',rho_g)
     allocate(rho(0:nx,0:ny,0:nz,8))
     do k=1,nz
        do j=1,ny
           do i=1,nx
              rho(i,j,k,:)=rho_l*VOF(i,j,k,:)+rho_g*(1.0_WP-VOF(i,j,k&
                   &,:))
           end do
        end do
     end do
     ! Communicate
     rho(0,:,:,:)=rho(nx, :, :,:)
     rho(:,0,:,:)=rho( :,ny, :,:)
     rho(:,:,0,:)=rho( :, :,nz,:)

     ! Compute density in velocity cells
     allocate(rho_U(nx,ny,nz)); rho_U=0.0_WP
     allocate(rho_V(nx,ny,nz)); rho_V=0.0_WP
     allocate(rho_W(nx,ny,nz)); rho_W=0.0_WP
     do k=1,nz
        do j=1,ny
           do i=1,nx
              do c=1,8
                 rho_U(i,j,k) = rho_U(i,j,k) + 0.125_WP*rho(i&
                      &+subcell2Ucell_i(c),j,k,c)
                 rho_V(i,j,k) = rho_V(i,j,k) + 0.125_WP*rho(i,j&
                      &+subcell2Vcell_j(c),k,c)
                 rho_W(i,j,k) = rho_W(i,j,k) + 0.125_WP*rho(i,j,k&
                      &+subcell2Wcell_k(c),c)
              end do
           end do
        end do
     end do
     
  case default
     stop "Unknown simulation type"
  end select

  ! Shift velocity field such that it has zero mean momentum
  U=U-sum(rho_U*U)/sum(rho_U)
  V=V-sum(rho_V*V)/sum(rho_V)
  W=W-sum(rho_W*W)/sum(rho_W)
  
  ! rhoUmean = sum(rho_U*U)/size(U)
  ! rhoVmean = sum(rho_V*V)/size(V)
  ! rhoWmean = sum(rho_W*W)/size(W)
  ! U=U-rhoUmean/rho_U
  ! V=V-rhoVmean/rho_V
  ! W=W-rhoWmean/rho_W
  ! rhoUmean=sum(rho_U*U)/size(U)
  ! rhoVmean=sum(rho_V*V)/size(V)
  ! rhoWmean=sum(rho_W*W)/size(W)

  ! Normalize velocity field such that it has unity mean kinetic energy 
  ! e.g., 1/2<U^2+V^2>=1
  KE=0.0_WP
  do k=1,nz
     do j=1,ny
        do i=1,nx
           myrho=0.125_WP*sum(rho(i,j,k,:))
           myU  =0.5_WP*(U(i,j,k)+U(mod(i,nx)+1,j,k))
           myV  =0.5_WP*(V(i,j,k)+V(i,mod(j,ny)+1,k))
           myW  =0.5_WP*(W(i,j,k)+W(i,j,mod(k,nz)+1))
           myvol=(x(i+1)-x(i))*(y(j+1)-y(j))*(z(k+1)-z(k))
           KE = KE + myvol * 0.5_WP*myrho*( myU**2 + myV**2 + myW**2 )
        end do
     end do
  end do
  U=U/sqrt(KE)
  V=V/sqrt(KE)
  W=W/sqrt(KE)


! contains
!   subroutine init_random_seed()
!     use iso_fortran_env, only: int64
!     implicit none
!     integer, allocatable :: seed(:)
!     integer :: i, n, un, istat, dt(8), pid
!     integer(int64) :: t

!     call random_seed(size = n)
!     allocate(seed(n))
!     ! First try if the OS provides a random number generator
!     open(newunit=un, file="/dev/urandom", access="stream", &
!          form="unformatted", action="read", status="old", iostat=istat)
!     if (istat == 0) then
!        read(un) seed
!        close(un)
!     else
!        ! Fallback to XOR:ing the current time and pid. The PID is
!        ! useful in case one launches multiple instances of the same
!        ! program in parallel.
!        call system_clock(t)
!        if (t == 0) then
!           call date_and_time(values=dt)
!           t = (dt(1) - 1970) * 365_int64 * 24 * 60 * 60 * 1000 &
!                + dt(2) * 31_int64 * 24 * 60 * 60 * 1000 &
!                + dt(3) * 24_int64 * 60 * 60 * 1000 &
!                + dt(5) * 60 * 60 * 1000 &
!                + dt(6) * 60 * 1000 + dt(7) * 1000 &
!                + dt(8)
!        end if
!        pid = getpid()
!        t = ieor(t, int(pid, kind(t)))
!        do i = 1, n
!           seed(i) = lcg(t)
!        end do
!     end if
!     call random_seed(put=seed)
!   end subroutine init_random_seed

!   ! This simple PRNG might not be good enough for real work, but is
!   ! sufficient for seeding a better PRNG.
!   function lcg(s)
!     use iso_fortran_env, only: int64
!     integer :: lcg
!     integer(int64) :: s
!     if (s == 0) then
!        s = 104729
!     else
!        s = mod(s, 4294967296_int64)
!     end if
!     s = mod(s * 279470273_int64, 4294967291_int64)
!     lcg = int(mod(s, int(huge(0), int64)), kind(0))
!   end function lcg

end subroutine random_vel_data
