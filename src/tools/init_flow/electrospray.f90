module electrospray
  use string
  use precision
  use param
  implicit none
  
  ! Domain
  real(WP) :: Lx,Ly,Lz
  real(WP) :: dx,dy,dz
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:),   pointer :: U
  real(WP), dimension(:,:,:),   pointer :: V
  real(WP), dimension(:,:,:),   pointer :: W
  real(WP), dimension(:,:,:,:), pointer :: VOF
  real(WP), dimension(:,:,:),   pointer :: L_Q
  
end module electrospray

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine electrospray_grid
  use electrospray
  use parser
  use math
  implicit none
  integer :: i,j,k
  
  ! Cartesian or cylindrical
  call parser_read('icyl',icyl)
  
  ! Periodicity
  call parser_read('xper',xper)
  if (icyl.eq.0) then
     yper = 1
  else
     yper = 0
  end if
  zper = 1
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('Lx',Lx)
  dx = Lx/real(nx,WP)
  call parser_read('ny',ny)
  call parser_read('Ly',Ly)
  dy = Ly/real(ny,WP)
  call parser_read('nz',nz)
  if (icyl.eq.0) then
     call parser_read('Lz',Lz)
  else
     Lz = twoPi
  end if
  dz = Lz/real(nz,WP)
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*dx
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*dy-0.5_WP*Ly
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*dz-0.5_WP*Lz
  end do
  
  ! Create the cell centered grid
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine electrospray_grid


! =================== !
! Generate the inflow !
! =================== !
subroutine electrospray_inflow
  use electrospray
  use parser
  implicit none
  
  real(WP) :: Djet,hcof,Ujet,Ucof,Uout,d,Dcof,dg,dl,Q
  real(WP) :: vof_tmp,myy,myz
  integer :: s,j,k,jj,kk
  integer, parameter :: nF=20
  
  ! Only if not periodic
  if (xper.eq.1) return
  
  ! Generate an inflow file with levelset
  nvar_inflow = 12
  ntime = 2
  
  ! Allocate some arrays
  allocate(inflow(ntime,jmin_:jmax_,kmin_:kmax_,nvar_inflow))
  allocate(names_inflow(nvar_inflow))
  
  ! Link the pointers
  U    (1:ntime,jmin_:jmax_,kmin_:kmax_) => inflow(:,:,:, 1); names_inflow( 1) = 'U'
  V    (1:ntime,jmin_:jmax_,kmin_:kmax_) => inflow(:,:,:, 2); names_inflow( 2) = 'V'
  W    (1:ntime,jmin_:jmax_,kmin_:kmax_) => inflow(:,:,:, 3); names_inflow( 3) = 'W'
  VOF  (1:ntime,jmin_:jmax_,kmin_:kmax_,1:8) => inflow(:,:,:, 4:11);
  names_inflow( 4) = 'VOF1'
  names_inflow( 5) = 'VOF2'
  names_inflow( 6) = 'VOF3'
  names_inflow( 7) = 'VOF4'
  names_inflow( 8) = 'VOF5'
  names_inflow( 9) = 'VOF6'
  names_inflow(10) = 'VOF7'
  names_inflow(11) = 'VOF8'
  L_Q => inflow(:,:,:, 12); names_inflow( 12) = 'L_Q'
    
  ! Compute time grid
  time_inflow = 0.0_WP
  dt_inflow   = 1.0_WP
  
  ! Profiles parameters
  call parser_read('Djet',Djet,0.0080_WP)
  call parser_read('Dcof',Dcof,0.0082_WP)
  call parser_read('hcof',hcof,0.0017_WP)
  call parser_read('Ujet',Ujet)
  call parser_read('Ucof',Ucof)
  call parser_read('Uout',Uout)
  call parser_read('Charge density',Q)
  
  ! Set profile thickness to mesh size
  dl=2.0_WP*dx
  dg=2.0_WP*dx
  
  ! Prepare the profiles
  V = 0.0_WP
  W = 0.0_WP
  do j=jmin_,jmax_
     do k=kmin_,kmax_

        ! Generate the distance from the pole
        if (icyl.eq.0) then
           d = sqrt(ym(j)**2+zm(k)**2)
        else
           d = ym(j)
        end if
        ! Define the velocity
        if (     d.le.0.5_WP*Djet) then
           ! Inside the liquid jet
           U(:,j,k) = Ujet*erf((0.5_WP*Djet-d)/dl)
        else if (d.gt.0.5_WP*Djet.and.d.le.0.5_WP*Dcof) then
           ! Inside the lip
           U(:,j,k) = 0.0_WP
        else if (d.gt.0.5_WP*Dcof .and. d.le.0.5_WP*Dcof+hcof) then
           ! Inside the gas coflow
           U(:,j,k) = min(Ucof*erf((d-0.5_WP*Dcof)/dg),Ucof*erf((0.5_WP*Dcof+hcof-d)/dg) )
        else if (d.gt.0.5_WP*Dcof+hcof) then
           ! Outside injector - add a coflow for entrainment
           U(:,j,k) = Uout
        end if
        ! Create VOF profile
        ! Loop over subcells
        do s=1,8
           if (icyl.eq.0) then
              ! Use finer mesh within the subcell
              vof_tmp=0.0_WP
              do kk=1,nF
                 do jj=1,nF
                    myy=ys1(s,j)+real(jj,WP)/real(nf+1,WP)*(ys2(s,j)-ys1(s,j))
                    myz=zs1(s,k)+real(kk,WP)/real(nf+1,WP)*(zs2(s,k)-zs1(s,k))
                    if (0.5_WP*Djet.gt.sqrt(myy**2+myz**2)) then
                       vof_tmp=vof_tmp+1.0_WP
                    end if
                 end do
              end do
              VOF(:,j,k,s)=vof_tmp/real(nF,WP)**2
           else
              VOF(:,j,k,s) = ((min(0.5_WP*Djet,ys2(s,j)))**2-ys1(s,j)**2)/(ys2(s,j)**2-ys1(s,j)**2)
           end if
        end do
     end do
  end do

  ! Charge density
  L_Q=Q
  
  return
end subroutine electrospray_inflow


! ========================= !
! Create the variable array !
! ========================= !
subroutine electrospray_data
  use electrospray
  use parser
  use math
  use string
  implicit none
  
  real(WP) :: thick,Djet,hcof,Ujet,Ucof,Uout,d,dg,dl,Dcof,Q
  integer :: s,i,j,k,ii,jj,kk
  real(WP) :: myx,myy,myz
  integer, parameter :: nF=20
  
  ! Allocate the array data
  nvar = 12
  allocate(data(imin_:imax_,jmin_:jmax_,kmin_:kmax_,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U    (imin_:imax_,jmin_:jmax_,kmin_:kmax_) => data(:,:,:, 1); names( 1) = 'U'
  V    (imin_:imax_,jmin_:jmax_,kmin_:kmax_) => data(:,:,:, 2); names( 2) = 'V'
  W    (imin_:imax_,jmin_:jmax_,kmin_:kmax_) => data(:,:,:, 3); names( 3) = 'W'
  VOF  (imin_:imax_,jmin_:jmax_,kmin_:kmax_,1:8) => data(:,:,:, 4:11);
  names( 4) = 'VOF1'
  names( 5) = 'VOF2'
  names( 6) = 'VOF3'
  names( 7) = 'VOF4'
  names( 8) = 'VOF5'
  names( 9) = 'VOF6'
  names(10) = 'VOF7'
  names(11) = 'VOF8'
  L_Q => data(:,:,:, 12); names( 12) = 'L_Q'
  
  ! Set the initial fields
  thick = 0.5_WP*dx
  call parser_read('Djet',Djet)
  call parser_read('Dcof',Dcof)
  call parser_read('hcof',hcof)
  call parser_read('Ujet',Ujet)
  call parser_read('Ucof',Ucof)
  call parser_read('Uout',Uout)
  call parser_read('Charge density',Q)

  ! Set profile thickness to mesh size
  dl=2.0_WP*dx
  dg=2.0_WP*dx

  ! Prepare the profiles
  V=0.0_WP
  W=0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_

           ! Generate the distance from cylinder
           if (icyl.eq.0) then
              d = sqrt(ym(j)**2+zm(k)**2)
           else
              d = ym(j)
           end if
           
           ! Define the velocity
           if (     d.le.0.5_WP*Djet) then
              ! Inside the liquid jet
              U(:,j,k) = Ujet*erf((0.5_WP*Djet-d)/dl)
           else if (d.gt.0.5_WP*Djet.and.d.le.0.5_WP*Dcof) then
              ! Inside the lip
              U(:,j,k) = 0.0_WP
           else if (d.gt.0.5_WP*Dcof .and. d.le.0.5_WP*Dcof+hcof) then
              ! Inside the gas coflow
              U(:,j,k) = min(Ucof*erf((d-0.5_WP*Dcof)/dg),Ucof*erf((0.5_WP*Dcof+hcof-d)/dg) )
           else if (d.gt.0.5_WP*Dcof+hcof) then
              ! Outside injector - add a coflow for entrainment
              U(:,j,k) = Uout
           end if

           ! Distance to center of sphere
           if (icyl.eq.0) then
              d = sqrt(ym(j)**2+zm(k)**2+xm(i)**2)
           else
              d = sqrt(ym(j)**2+xm(i)**2)
           end if
           
           ! Check if close to interface           
           if (abs(d-0.5_WP*Djet).le.2.0_WP*dx) then
              ! Compute VOF close to the interface
              do s=1,8
                 if (icyl.eq.0) then
                    ! Use finer mesh within the cartesian cell
                    VOF(i,j,k,s)=0.0_WP
                    do kk=1,nF
                       do jj=1,nF
                          do ii=1,nF
                             myx=xs1(s,i)+(real(ii,WP)-0.5_WP)/real(nf,WP)*(xs2(s,i)-xs1(s,i))
                             myy=ys1(s,j)+(real(jj,WP)-0.5_WP)/real(nf,WP)*(ys2(s,j)-ys1(s,j))
                             myz=zs1(s,k)+(real(kk,WP)-0.5_WP)/real(nf,WP)*(zs2(s,k)-zs1(s,k))
                             if (0.5_WP*Djet.gt.sqrt(myx**2+myy**2+myz**2)) then
                                VOF(i,j,k,s)=VOF(i,j,k,s)+1.0_WP
                             end if
                          end do
                       end do
                    end do
                    VOF(i,j,k,s)=VOF(i,j,k,s)/real(nF,WP)**3
                 else
                    ! Use finer mesh within the cylindrical cell
                    VOF(s,i,j,k)=0.0_WP
                    do jj=1,nF
                       do ii=1,nF
                          myx=xs1(s,i)+(real(ii,WP)-0.5_WP)/real(nf,WP)*(xs2(s,i)-xs1(s,i))
                          myy=ys1(s,j)+(real(jj,WP)-0.5_WP)/real(nf,WP)*(ys2(s,j)-ys1(s,j))
                          if (0.5_WP*Djet.gt.sqrt(myx**2+myy**2)) then
                             VOF(i,j,k,s)=VOF(s,i,j,k)+1.0_WP
                          end if
                       end do
                    end do
                    VOF(i,j,k,s)=VOF(i,j,k,s)/real(nF,WP)**2
                 end if
              end do
           else
              ! Set VOF directly
              if (d.gt.0.5_WP*Djet) then
                 VOF(i,j,k,:)=0.0_WP
              else
                 VOF(i,j,k,:)=1.0_WP
              end if
           end if
        end do
     end do
  end do

  ! Charge density
  L_Q=Q

  ! Make velocity in gas=0
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Velocity
           U(i,j,k)=U(i,j,k)*0.125_WP*sum(VOF(i,j,k,:))
        end do
     end do
  end do
  
  return
end subroutine electrospray_data
