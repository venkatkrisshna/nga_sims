module vonkarman
  use precision
  use param
  use fileio
  implicit none

  ! Length in x, y, z directions
  real(WP) :: Lx, Ly

  ! Radius and position of the cylinder
  real(WP) :: diam,R
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: Gib

end module vonkarman


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine vonkarman_grid
  use vonkarman
  use parser
  implicit none
  
  integer :: i,j,k

  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  nz = 1
  
  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  
  call parser_read('Cylinder diameter',diam)
  R = 0.5_WP*diam
  
  ! Set Periodicity
  xper = 0
  yper = 1
  zper = 1
  
  ! Cylindrical
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))

  do i=1,nx+1
     x(i) = real(i-1,WP)*Lx/real(nx,WP) - 0.5_WP*Lx
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*Ly/real(ny,WP) - 0.5_WP*Ly
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)/real(nz,WP) - 0.5_WP
  end do

  ! Create the mid points 
  do i=1,nx
     xm(i)= 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)= 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do

  mask = 0
  
  return
end subroutine vonkarman_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine vonkarman_data
  use vonkarman
  use parser
  implicit none

  integer :: i,j,k
  
  ! Allocate the array data
  nvar = 4
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))


  nod = 1
  allocate(OD(nx,ny,nz,nod))
  allocate(OD_names(nod))
  
  ! Link the pointers
  U => data(:,:,:,1); names(1) = 'U'
  V => data(:,:,:,2); names(2) = 'V'
  W => data(:,:,:,3); names(3) = 'W'
  P => data(:,:,:,4); names(4) = 'P'

  Gib => OD(:,:,:,1); OD_names(1) = 'Gib'
  
  ! Create them
  U = 0.0_WP
  V = 0.0_WP
  W = 0.0_WP
  P = 0.0_WP
  
  do i=1,nx
    do j=1,ny
      do k=1,nz
        Gib(i,j,k) = sqrt(xm(i)**2+ym(j)**2+zm(k)**2)-R
      end do
    end do
  end do
  
  return
end subroutine vonkarman_data
