! ============================ !
! Grid and data initialization !
! for a hydraulic jump problem !
! ============================ !
module hydrojump
  use string
  use precision
  use param
  implicit none
  
  ! Domain
  real(WP) :: Lx,Ly,Lz
  real(WP) :: dx,dy,dz
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: VOF
  
  ! Flow parameters
  real(WP) :: H1,H2,L,U1,BL
  
end module hydrojump

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine hydrojump_grid
  use hydrojump
  use parser
  use math
  use string
  implicit none
  
  integer :: i,j,k
  
  ! Read in the size of the domain
  call parser_read('nx',nx); call parser_read('Lx',Lx); dx=Lx/real(nx,WP)
  call parser_read('ny',ny); call parser_read('Ly',Ly); dy=Ly/real(ny,WP)
  call parser_read('nz',nz); call parser_read('Lz',Lz); dz=Lz/real(nz,WP)
  xper=0; yper=0; zper=1
  
  ! Cartesian
  icyl=0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*dx-0.5_WP*Lx
  end do
  do j=1,ny+1
     ! First cell in the wall
     y(j) = real(j-2,WP)*dy
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*dz-0.5_WP*Lz
  end do
  
  ! Create the cell centered grid
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  mask(:,1) = 1
  
  return
end subroutine hydrojump_grid


! ============================ !
! Create the initial data file !
! ============================ !
subroutine hydrojump_data
  use hydrojump
  use parser
  implicit none
  
  integer :: i,j,k
  real(WP) :: height
  
  ! Allocate the array data
  nvar=4
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U  =>data(:,:,:,1); names(1)='U'
  V  =>data(:,:,:,2); names(2)='V'
  W  =>data(:,:,:,3); names(3)='W'
  VOF=>data(:,:,:,4); names(4)='VOF'
  
  ! Initialize the jump
  call parser_read('H1',H1)
  call parser_read('H2',H2)
  call parser_read('L' ,L )
  do k=1,nz
     do j=1,ny
        do i=1,nx
           height=0.5_WP*(H1+H2)+0.5_WP*(H2-H1)*tanh(2.0_WP*x(i)/L)
           VOF(i,j,k)=min(max((height-y(j))/dy,0.0_WP),1.0_WP)
        end do
     end do
  end do
  
  ! Initialize the velocity field
  call parser_read('U1',U1)
  call parser_read('BL',BL)
  V=0.0_WP
  W=0.0_WP
  do k=1,nz
     do j=1,ny
        do i=1,nx
           height=0.5_WP*(H1+H2)+0.5_WP*(H2-H1)*tanh(2.0_WP*x(i)/L)
           if (VOF(i,j,k).eq.0.0_WP) then
              ! Above the liquid
              U(:,j,k)=0.0_WP
           else
              ! Inside the liquid
              U(i,j,k) = U1*H1/height*tanh(y(j)/BL)
           end if
        end do
     end do
  end do
  
  return
end subroutine hydrojump_data


! ============================ !
! Inflow velocity and VOF data !
! ============================ !
subroutine hydrojump_inflow
  use hydrojump
  use parser
  implicit none
  
  integer :: j,k
  
  ! Generate an inflow
  nvar_inflow = 4
  ntime = 2
  
  ! Allocate some arrays
  allocate(inflow(ntime,ny,nz,nvar_inflow))
  allocate(names_inflow(nvar_inflow))
  
  ! Link the pointers
  U   => inflow(:,:,:,1); names_inflow(1) = 'U'
  V   => inflow(:,:,:,2); names_inflow(2) = 'V'
  W   => inflow(:,:,:,3); names_inflow(3) = 'W'
  VOF => inflow(:,:,:,4); names_inflow(4) = 'VOF'
  
  ! Compute time grid
  time_inflow = 0.0_WP
  dt_inflow   = 1.0_WP
  
  ! Prepare the inflow profiles
  V = 0.0_WP
  W = 0.0_WP
  do j=1,ny
     do k=1,nz
        ! Injection height
        VOF(:,j,k)=min(max((H1-y(j))/dy,0.0_WP),1.0_WP)
        ! Injection velocity
        if (VOF(1,j,k).eq.0.0_WP) then
           ! Above the liquid
           U(:,j,k)=0.0_WP
        else
           ! Inside the liquid
           U(:,j,k) = U1*tanh(y(j)/BL)
        end if
     end do
  end do
  
  return
end subroutine hydrojump_inflow
