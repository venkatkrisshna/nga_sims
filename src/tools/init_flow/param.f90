module param
  use precision
  use string
  use parallel
  implicit none

  ! --- CONFIG FILE ---

  ! Cylindrical or cartesian
  integer :: icyl
  ! Number of grid points
  integer :: nx,ny,nz
  integer(KIND=8) :: ncell
  ! Periodicity
  integer :: xper,yper,zper

  ! Grid/mesh variables for data file
  real(WP), dimension(:), pointer :: x
  real(WP), dimension(:), pointer :: y
  real(WP), dimension(:), pointer :: z
  real(WP), dimension(:), pointer :: xm
  real(WP), dimension(:), pointer :: ym
  real(WP), dimension(:), pointer :: zm
  integer, dimension(:,:), pointer :: mask

  ! --- DATA FILE ---

  ! Data array with all the variables (velocity,pressure,...)
  integer :: nvar
  character(len=str_short), dimension(:), allocatable, target :: names
  real(WP), dimension(:,:,:,:), allocatable, target :: data
  
  ! --- OPTDATA FILE ---
  
  ! Data array with all the optional variables
  integer :: nod
  character(len=str_short), dimension(:), allocatable, target :: OD_names
  real(WP), dimension(:,:,:,:), allocatable, target :: OD
  
  ! --- INFLOW FILE ---

  ! Inflow grid related parameters
  integer :: ntime
  real(WP), dimension(:), allocatable, target :: t_inflow
  
  ! Number of time steps
  real(WP) :: dt_inflow,time_inflow

  ! Inflow array with all variables
  integer :: nvar_inflow
  character(len=str_short), dimension(:), allocatable, target :: names_inflow
  real(WP), dimension(:,:,:,:), allocatable, target :: inflow

 ! --- INFLOW Y FILE ---

  ! Inflow grid related parameters
  integer :: ntimey
  real(WP), dimension(:), allocatable, target :: t_inflowy
  
  ! Number of time steps
  real(WP) :: dt_inflowy,time_inflowy

  ! Inflow array with all variables
  integer :: nvar_inflowy
  character(len=str_short), dimension(:), allocatable, target :: names_inflowy
  real(WP), dimension(:,:,:,:), allocatable, target :: inflowy
  
  ! --- CHEMTABLE ---

  ! Coordinates of the chemtable
  integer :: n1,n2,n3
  real(WP), dimension(:), allocatable, target :: x1,x2,x3
  ! Mask of the chemtable
  integer, dimension(:,:,:), allocatable, target :: chem_mask

  ! Names in the chemtable
  integer :: nvar_chem
  character(len=str_medium), dimension(:), allocatable, target :: names_chem

  ! Chemtable model
  character(len=str_medium) :: combModel

  ! Table of variables
  real(WP), dimension(:,:,:,:), allocatable, target :: table

  ! --- PARALLEL WRITE --- 
  integer :: nx_,ny_,nz_ 
  integer :: nxo_,nyo_,nzo_ 
  integer :: imin,imax,imin_,imax_,imino_,imaxo_
  integer :: jmin,jmax,jmin_,jmax_,jmino_,jmaxo_
  integer :: kmin,kmax,kmin_,kmax_,kmino_,kmaxo_

  type MPI_IO_VAR
     character(len=str_short) :: name
     real(WP), dimension(:,:,:), pointer :: var
     integer :: view
  end type MPI_IO_VAR

  ! Array with each variables
  integer :: MPI_IO_NVARS
  type(MPI_IO_VAR), dimension(:), pointer :: MPI_IO_DATA

  ! Subcell x,y,z location compared to cell center
  ! e.g. x_sub=xm(i)+subx(s)*dx(i) is the x-coord of the s subcell
  real(WP), dimension(8) :: subx,suby,subz 
  data subx(1:8) / -0.25_WP,  0.25_WP, -0.25_WP,  0.25_WP, -0.25_WP,  0.25_WP, -0.25_WP,  0.25_WP /
  data suby(1:8) / -0.25_WP, -0.25_WP,  0.25_WP,  0.25_WP, -0.25_WP, -0.25_WP,  0.25_WP,  0.25_WP /
  data subz(1:8) / -0.25_WP, -0.25_WP, -0.25_WP, -0.25_WP,  0.25_WP,  0.25_WP,  0.25_WP,  0.25_WP /
  
contains
  ! Functions that return mesh locations for subcells

  ! ys2(s,j) |------------------------|
  !          |                        |
  !          |                        |
  !          |                        |
  !          |       subcell=s        |
  !          |                        |
  !          |                        |
  !          |                        |
  ! ys1(s,j) |------------------------|
  !       xs1(s,i)                 xs2(s,i)
  function xs1(s,i)
    implicit none
    real(WP) :: xs1
    integer, intent(in) :: s,i
    xs1=xm(i)+(subx(s) - 0.25_WP)*(x(i+1)-x(i))
    return
  end function xs1

  function xs2(s,i)
    implicit none
    real(WP) :: xs2
    integer, intent(in) :: s,i
    xs2=xm(i)+(subx(s) + 0.25_WP)*(x(i+1)-x(i))
    return
  end function xs2

  function ys1(s,j)
    implicit none
    real(WP) :: ys1
    integer, intent(in) :: s,j
    ys1=ym(j)+(suby(s) - 0.25_WP)*(y(j+1)-y(j))
    return
  end function ys1

  function ys2(s,j)
    implicit none
    real(WP) :: ys2
    integer, intent(in) :: s,j
    ys2=ym(j)+(suby(s) + 0.25_WP)*(y(j+1)-y(j))
    return
  end function ys2

  function zs1(s,k)
    implicit none
    real(WP) :: zs1
    integer, intent(in) :: s,k
    zs1=zm(k)+(subz(s) - 0.25_WP)*(z(k+1)-z(k))
    return
  end function zs1

  function zs2(s,k)
    implicit none
    real(WP) :: zs2
    integer, intent(in) :: s,k
    zs2=zm(k)+(subz(s) + 0.25_WP)*(z(k+1)-z(k))
    return
  end function zs2
    
  ! =============================== !
  ! Write the mesh file to the disk !
  ! =============================== !
  subroutine param_write_config(simulation)
    use parser
    implicit none

    character(len=str_medium) :: simulation
    integer :: ierr,iunit
    character(len=str_medium) :: filename

    ! Open the mesh file
    call parser_read('Init config file',filename)
    call BINARY_FILE_OPEN(iunit,trim(filename),"w",ierr)

    ! Write the mesh
    call BINARY_FILE_WRITE(iunit,simulation,str_medium,kind(simulation),ierr)
    call BINARY_FILE_WRITE(iunit,icyl,1,kind(icyl),ierr)
    call BINARY_FILE_WRITE(iunit,xper,1,kind(xper),ierr)
    call BINARY_FILE_WRITE(iunit,yper,1,kind(yper),ierr)
    call BINARY_FILE_WRITE(iunit,zper,1,kind(zper),ierr)
    call BINARY_FILE_WRITE(iunit,nx,1,kind(nx),ierr)
    call BINARY_FILE_WRITE(iunit,ny,1,kind(ny),ierr)
    call BINARY_FILE_WRITE(iunit,nz,1,kind(nz),ierr)
    call BINARY_FILE_WRITE(iunit,x,nx+1,kind(x),ierr)
    call BINARY_FILE_WRITE(iunit,y,ny+1,kind(y),ierr)
    call BINARY_FILE_WRITE(iunit,z,nz+1,kind(z),ierr)
    call BINARY_FILE_WRITE(iunit,mask,nx*ny,kind(mask),ierr)

    ! Close the file
    call BINARY_FILE_CLOSE(iunit,ierr)

    return
  end subroutine param_write_config

  ! =============================== !
  ! Write the mesh file to the disk !
  !       (parallel version)        !
  ! =============================== !
  subroutine param_parwrite_config(simulation)
    use parser
    use parallel
    implicit none

    character(len=str_medium) :: simulation
    integer :: ierr,iunit
    character(len=str_medium) :: filename

    if (irank.ne.iroot) return

    ! Open the mesh file
    call parser_read('Init config file',filename)
    call BINARY_FILE_OPEN(iunit,trim(filename),"w",ierr)

    ! Write the mesh
    call BINARY_FILE_WRITE(iunit,simulation,str_medium,kind(simulation),ierr)
    call BINARY_FILE_WRITE(iunit,icyl,1,kind(icyl),ierr)
    call BINARY_FILE_WRITE(iunit,xper,1,kind(xper),ierr)
    call BINARY_FILE_WRITE(iunit,yper,1,kind(yper),ierr)
    call BINARY_FILE_WRITE(iunit,zper,1,kind(zper),ierr)
    call BINARY_FILE_WRITE(iunit,nx,1,kind(nx),ierr)
    call BINARY_FILE_WRITE(iunit,ny,1,kind(ny),ierr)
    call BINARY_FILE_WRITE(iunit,nz,1,kind(nz),ierr)
    call BINARY_FILE_WRITE(iunit,x,nx+1,kind(x),ierr)
    call BINARY_FILE_WRITE(iunit,y,ny+1,kind(y),ierr)
    call BINARY_FILE_WRITE(iunit,z,nz+1,kind(z),ierr)
    call BINARY_FILE_WRITE(iunit,mask,nx*ny,kind(mask),ierr)

    ! Close the file
    call BINARY_FILE_CLOSE(iunit,ierr)

    return
  end subroutine param_parwrite_config

  ! =============================== !
  ! Write the data file to the disk !
  ! =============================== !
  subroutine param_write_data
    use parser
    implicit none

    integer :: ierr,iunit
    integer :: var
    character(len=str_medium) :: filename
    real(WP) :: dt,time
    
    ! Create number of cells
    ncell=int(nx,8)*int(ny,8)*int(nz,8)
    
    ! Open the data file
    call parser_read('Init data file',filename)
    call BINARY_FILE_OPEN(iunit,trim(filename),"w",ierr)

    ! Write sizes
    call BINARY_FILE_WRITE(iunit,nx,1,kind(nx),ierr)
    call BINARY_FILE_WRITE(iunit,ny,1,kind(ny),ierr)
    call BINARY_FILE_WRITE(iunit,nz,1,kind(nz),ierr)
    call BINARY_FILE_WRITE(iunit,nvar,1,kind(nvar),ierr)
    ! Write additional stuff
    dt = 0.0_WP
    time = 0.0_WP
    call BINARY_FILE_WRITE(iunit,dt,1,kind(dt),ierr)
    call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr)
    ! Write variable names
    do var=1,nvar
       call BINARY_FILE_WRITE(iunit,names(var),str_short,kind(names),ierr)
    end do
    ! Write data field
    do var=1,nvar
       call BINARY_BIGFILE_WRITE(iunit,data(:,:,:,var),ncell,kind(data),ierr)
    end do
    
    ! Close the file
    call BINARY_FILE_CLOSE(iunit,ierr)

    return
  end subroutine param_write_data

  ! =============================== !
  ! Write the data file to the disk !
  !       (parallel version)        !
  ! =============================== !
  subroutine param_parwrite_data
    use parser
    implicit none

    integer :: var
    character(len=str_medium) :: filename

    ! Link to data
    MPI_IO_NVARS = nvar
    allocate(MPI_IO_DATA(MPI_IO_NVARS))
    do var=1,MPI_IO_NVARS
       MPI_IO_DATA(var)%var  => data(imin_:imax_,jmin_:jmax_,kmin_:kmax_,var)
       MPI_IO_DATA(var)%name =  names(var)
    end do

    ! Get filename
    call parser_read('Init data file',filename)
    filename = trim(mpiiofs) // trim(filename)

    ! Write data file
    call param_write_file(filename)

    deallocate(MPI_IO_DATA)

    return
  end subroutine param_parwrite_data  
  
  ! =============================== !
  ! Write the data file to the disk !
  ! =============================== !
  subroutine param_write_optdata
    use parser
    implicit none
    
    integer :: ierr,iunit
    integer :: var
    character(len=str_medium) :: filename
    real(WP) :: dt,time
    
    ! If optdata then continue
    if (.not.allocated(OD)) return
    
    ! Create number of cells
    ncell=int(nx,8)*int(ny,8)*int(nz,8)
    
    ! Open the data file
    call parser_read('Init optional data',filename)
    call BINARY_FILE_OPEN(iunit,trim(filename),"w",ierr)
    
    ! Write sizes
    call BINARY_FILE_WRITE(iunit,nx,1,kind(nx),ierr)
    call BINARY_FILE_WRITE(iunit,ny,1,kind(ny),ierr)
    call BINARY_FILE_WRITE(iunit,nz,1,kind(nz),ierr)
    call BINARY_FILE_WRITE(iunit,nod,1,kind(nod),ierr)
    ! Write additional stuff
    dt = 0.0_WP
    time = 0.0_WP
    call BINARY_FILE_WRITE(iunit,dt,1,kind(dt),ierr)
    call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr)
    ! Write variable names
    do var=1,nod
       call BINARY_FILE_WRITE(iunit,OD_names(var),str_short,kind(OD_names),ierr)
    end do
    ! Write data field
    do var=1,nod
       call BINARY_BIGFILE_WRITE(iunit,OD(:,:,:,var),ncell,kind(OD),ierr)
    end do
    
    ! Close the file
    call BINARY_FILE_CLOSE(iunit,ierr)
    
    return
  end subroutine param_write_optdata
  
  ! =============================== !
  ! Write the data file to the disk !
  !       (parallel version)        !
  ! =============================== !
  subroutine param_parwrite_optdata
    use parser
    implicit none

    integer :: var
    character(len=str_medium) :: filename
  
    ! If optdata then continue
    if (.not.allocated(OD)) return

    ! Link to data
    MPI_IO_NVARS = nod
    allocate(MPI_IO_DATA(MPI_IO_NVARS))
    do var=1,MPI_IO_NVARS
       MPI_IO_DATA(var)%var  => OD(imin_:imax_,jmin_:jmax_,kmin_:kmax_,var)
       MPI_IO_DATA(var)%name =  OD_names(var)
    end do

    ! Get filename
    call parser_read('Init optional data',filename)
    filename = trim(mpiiofs) // trim(filename)

    ! Write data file
    call param_write_file(filename)

    deallocate(MPI_IO_DATA)
    
    return
  end subroutine param_parwrite_optdata

  ! =============================== !
  !  Parallel write of data files   !
  ! =============================== !
  subroutine param_write_file(filename)
    implicit none
    
    character(len=str_medium), intent(in) :: filename
    real(WP) :: dt,time
    integer :: ifile,ierr,var,data_size
    integer, dimension(3) :: gsizes, lsizes, start
    Integer, dimension(MPI_STATUS_SIZE) :: status
    integer, dimension(4) :: dims
    integer(kind=MPI_Offset_kind) :: disp
    integer(kind=MPI_Offset_kind) :: nx_MOK, ny_MOK, nz_MOK
    integer(kind=MPI_Offset_kind) :: WP_MOK, var_MOK, str_MOK
    integer(kind=MPI_Offset_kind) :: NVARS_MOK
    logical :: file_is_there  

    ! Open the file to write
    inquire(file=filename,exist=file_is_there)
    if (file_is_there .and. irank.eq.iroot) call MPI_FILE_DELETE(filename,mpi_info,ierr)
    call MPI_FILE_OPEN(comm,filename,IOR(MPI_MODE_WRONLY,MPI_MODE_CREATE),mpi_info,ifile,ierr)

    ! Write header
    if (irank.eq.iroot) then
       ! Write dimensions
       dims(1) = nx
       dims(2) = ny
       dims(3) = nz
       dims(4) = MPI_IO_NVARS
       call MPI_FILE_WRITE(ifile,dims,4,MPI_INTEGER,status,ierr)
       ! Write additional stuff
       dt = 0.0_WP
       time = 0.0_WP
       call MPI_FILE_WRITE(ifile,dt,1,MPI_REAL_WP,status,ierr)
       call MPI_FILE_WRITE(ifile,time,1,MPI_REAL_WP,status,ierr)
       ! Write variable names
       do var=1,MPI_IO_NVARS
          call MPI_FILE_WRITE(ifile,MPI_IO_DATA(var)%name,str_short,MPI_CHARACTER,status,ierr)
       end do
    end if

    ! Size of local arrays
    data_size = nx_*ny_*nz_

    ! Define global(g) and local(l) sizes
    gsizes(1) = nx
    gsizes(2) = ny
    gsizes(3) = nz
    
    lsizes(1) = nx_
    lsizes(2) = ny_
    lsizes(3) = nz_
    
    ! Define starting points
    start(1) = imin_-imin
    start(2) = jmin_-jmin
    start(3) = kmin_-kmin
    
    ! Define the view for each variable
    do var=1,MPI_IO_NVARS
       call MPI_TYPE_CREATE_SUBARRAY(3,gsizes,lsizes,start,&
            MPI_ORDER_FORTRAN,MPI_REAL_WP,MPI_IO_DATA(var)%view,ierr)
       call MPI_TYPE_COMMIT(MPI_IO_DATA(var)%view,ierr)
    end do

    ! Resize some integers so MPI can read even the biggest files
    nx_MOK    = int(nx,          MPI_Offset_kind)
    ny_MOK    = int(ny,          MPI_Offset_kind)
    nz_MOK    = int(nz,          MPI_Offset_kind)
    WP_MOK    = int(WP,          MPI_Offset_kind)
    str_MOK   = int(str_short,   MPI_Offset_kind)
    NVARS_MOK = int(MPI_IO_NVARS,MPI_Offset_kind)

    ! Write the data for each variable
    do var=1,MPI_IO_NVARS
       var_MOK = int(var,MPI_Offset_kind)
       disp = 4*4 + str_MOK*NVARS_MOK + 2*WP_MOK + & 
            nx_MOK*ny_MOK*nz_MOK*WP_MOK*(var_MOK-1)
       call MPI_FILE_SET_VIEW(ifile,disp,MPI_REAL_WP,MPI_IO_DATA(var)%view, &
            "native",mpi_info,ierr)
       call MPI_FILE_WRITE_ALL(ifile,MPI_IO_DATA(var)%var,data_size, &
            MPI_REAL_WP,status,ierr)
    end do

    ! Close the file
    call MPI_FILE_CLOSE(ifile,ierr)

  end subroutine param_write_file
  
  ! ================================= !
  ! Write the inflow file to the disk !
  ! ================================= !
  subroutine param_write_inflow
    use parser
    implicit none
    
    integer :: ierr,iunit,var,n
    character(len=str_medium) :: filename
    
    ! If inflow then continue
    if (.not.allocated(inflow)) return
    
    ! Read filename and frequency
    call parser_read('Init inflow file',filename)
    call BINARY_FILE_OPEN(iunit,trim(filename),"w",ierr)
    
    ! Write sizes
    call BINARY_FILE_WRITE(iunit,ntime,1,kind(ntime),ierr)
    call BINARY_FILE_WRITE(iunit,ny,1,kind(ny),ierr)
    call BINARY_FILE_WRITE(iunit,nz,1,kind(nz),ierr)
    call BINARY_FILE_WRITE(iunit,nvar_inflow,1,kind(nvar_inflow),ierr)
    
    ! Write additional stuff
    call BINARY_FILE_WRITE(iunit,dt_inflow,1,kind(dt_inflow),ierr)
    call BINARY_FILE_WRITE(iunit,time_inflow,1,kind(time_inflow),ierr)
    ! Write variable names
    do var=1,nvar_inflow
       call BINARY_FILE_WRITE(iunit,names_inflow(var),str_short,kind(names_inflow),ierr)
    end do
    ! Write the grid
    call BINARY_FILE_WRITE(iunit,icyl,1,kind(icyl),ierr)
    call BINARY_FILE_WRITE(iunit,y,ny+1,kind(y),ierr)
    call BINARY_FILE_WRITE(iunit,z,nz+1,kind(z),ierr)
    ! Write data field
    do n=1,ntime
       do var=1,nvar_inflow
          call BINARY_FILE_WRITE(iunit,inflow(n,:,:,var),ny*nz,kind(inflow),ierr)
       end do
    end do
    
    ! Close the file
    call BINARY_FILE_CLOSE(iunit,ierr)
    
    return
  end subroutine param_write_inflow

  ! =================================== !
  ! Write the inflow y file to the disk !
  ! =================================== !
  subroutine param_write_inflowy
    use parser
    implicit none
    
    integer :: ierr,iunit,n,var
    character(len=str_medium) :: filename
    
    ! If inflow then continue
    if (.not.allocated(inflowy)) return
    
    ! Read filename and frequency
    call parser_read('Init inflow y file',filename)
    call BINARY_FILE_OPEN(iunit,trim(filename),"w",ierr)
    
    ! Write sizes
    call BINARY_FILE_WRITE(iunit,ntimey,1,kind(ntime),ierr)
    call BINARY_FILE_WRITE(iunit,nx,1,kind(ny),ierr)
    call BINARY_FILE_WRITE(iunit,nz,1,kind(nz),ierr)
    call BINARY_FILE_WRITE(iunit,nvar_inflowy,1,kind(nvar_inflow),ierr)
    
    ! Write additional stuff
    call BINARY_FILE_WRITE(iunit,dt_inflowy,1,kind(dt_inflow),ierr)
    call BINARY_FILE_WRITE(iunit,time_inflowy,1,kind(time_inflow),ierr)
    ! Write variable names
    do var=1,nvar_inflowy
       call BINARY_FILE_WRITE(iunit,names_inflowy(var),str_short,kind(names_inflowy),ierr)
    end do
    ! Write the grid
    call BINARY_FILE_WRITE(iunit,icyl,1,kind(icyl),ierr)
    call BINARY_FILE_WRITE(iunit,x,nx+1,kind(x),ierr)
    call BINARY_FILE_WRITE(iunit,z,nz+1,kind(z),ierr)
    ! Write data field
    do n=1,ntimey
       do var=1,nvar_inflowy
          call BINARY_FILE_WRITE(iunit,inflowy(n,:,:,var),nx*nz,kind(inflowy),ierr)
       end do
    end do
    
    ! Close the file
    call BINARY_FILE_CLOSE(iunit,ierr)
    
    return
  end subroutine param_write_inflowy

  ! ================================= !
  ! Write the inflow file to the disk !
  !        (parallel version)         !
  ! ================================= !
  subroutine param_parwrite_inflow
    use parser
    implicit none

    integer :: n,j,k,file_inflow
    integer :: inflow_comm,inflow_irank
    real(WP), dimension(:,:,:), pointer :: buf
    integer, dimension(:), pointer :: fileview
    integer :: ifile,ierr,var,data_size
    integer, dimension(3) :: gsizes, lsizes, start
    integer, dimension(MPI_STATUS_SIZE) :: status
    integer, dimension(4) :: dims
    integer(kind=MPI_Offset_kind) :: disp
    integer(kind=MPI_Offset_kind) :: nx_MOK, ny_MOK, nz_MOK
    integer(kind=MPI_Offset_kind) :: WP_MOK, str_MOK
    integer(kind=MPI_Offset_kind) :: NVARS_MOK
    integer(kind=MPI_Offset_kind) :: n_MOK
    logical :: file_is_there  
    character(len=str_medium) :: filename

    ! If inflow then continue
    if (.not.allocated(inflow)) return

    ! Allocate buffer
    allocate(buf(jmin_:jmax_,kmin_:kmax_,nvar_inflow))
    allocate(fileview(ntime))

    ! Split the communicator
    if (iproc.ne.1) then
       file_inflow = 0
       call MPI_COMM_SPLIT(comm,file_inflow,MPI_UNDEFINED,inflow_comm,ierr)
       return
    else
       file_inflow = 1
       call MPI_COMM_SPLIT(comm,file_inflow,MPI_UNDEFINED,inflow_comm,ierr)
    end if

    ! Create processor ranks in inflow_comm communicator
    call MPI_COMM_RANK(inflow_comm,inflow_irank,ierr)
    inflow_irank = inflow_irank + 1

    ! Get filename
    call parser_read('Init inflow file',filename)
    filename = trim(mpiiofs) // trim(filename)
    ! Open the file to write
    inquire(file=filename,exist=file_is_there)
    if (file_is_there .and. irank.eq.iroot) call MPI_FILE_DELETE(filename,mpi_info,ierr)
    call MPI_FILE_OPEN(inflow_comm,filename,IOR(MPI_MODE_WRONLY,MPI_MODE_CREATE),mpi_info,ifile,ierr)
    
    ! Write header
    if (inflow_irank.eq.1) then
       ! Write dimensions
       dims(1) = ntime
       dims(2) = ny
       dims(3) = nz
       dims(4) = nvar_inflow
       call MPI_FILE_WRITE(ifile,dims,4,MPI_INTEGER,status,ierr)
       ! Write additional stuff
       call MPI_FILE_WRITE(ifile,dt_inflow,1,MPI_REAL_WP,status,ierr)
       call MPI_FILE_WRITE(ifile,time_inflow,1,MPI_REAL_WP,status,ierr)
       ! Write variable names
       do var=1,nvar_inflow
          call MPI_FILE_WRITE(ifile,names_inflow(var),str_short,MPI_CHARACTER,status,ierr)
       end do
       ! Write the grid
       call MPI_FILE_WRITE(ifile,icyl,1,MPI_INTEGER,status,ierr)
       call MPI_FILE_WRITE(ifile,y(jmin:jmax+1),ny+1,MPI_REAL_WP,status,ierr)
       call MPI_FILE_WRITE(ifile,z(kmin:kmax+1),nz+1,MPI_REAL_WP,status,ierr)
    end if

    ! Global sizes
    gsizes(1) = ny
    gsizes(2) = nz
    gsizes(3) = nvar_inflow
    ! Local sizes
    lsizes(1) = ny_
    lsizes(2) = nz_
    lsizes(3) = nvar_inflow
    ! Starting index
    start(1) = jmin_-jmin
    start(2) = kmin_-kmin
    start(3) = 0
    ! Define the size
    data_size = lsizes(1)*lsizes(2)*lsizes(3)
    ! Predefine the view  
    do n=1,ntime
       call MPI_TYPE_CREATE_SUBARRAY(3,gsizes,lsizes,start,&
            MPI_ORDER_FORTRAN,MPI_REAL_WP,fileview(n),ierr)
       call MPI_TYPE_COMMIT(fileview(n),ierr)
    end do

    ! Resize some integers so MPI can read even the biggest files
    nx_MOK    = int(nx,          MPI_Offset_kind)
    ny_MOK    = int(ny,          MPI_Offset_kind)
    nz_MOK    = int(nz,          MPI_Offset_kind)
    WP_MOK    = int(WP,          MPI_Offset_kind)
    str_MOK   = int(str_short,   MPI_Offset_kind)
    NVARS_MOK = int(nvar_inflow, MPI_Offset_kind)

    do n=1,ntime

       ! Transfer all the quantities...
       do var=1,nvar_inflow
          do k=kmin_,kmax_
             do j=jmin_,jmax_
                buf(j,k,var) = inflow(n,j,k,var)
             end do
          end do
       end do

       ! Write the data
       n_MOK = int(n,MPI_Offset_kind)
       disp = 4*4 + 2*WP_MOK + NVARS_MOK*str_MOK + 4 + (ny_MOK+nz_MOK+2)*WP_MOK &
            + (n_MOK-1)*NVARS_MOK*ny_MOK*nz_MOK*WP_MOK          
       call MPI_FILE_SET_VIEW(ifile,disp,MPI_REAL_WP,fileview(n), &
            "native",mpi_info,ierr)
       call MPI_FILE_WRITE_ALL(ifile,buf,data_size, &
            MPI_REAL_WP,status,ierr)

    end do

    ! Close the file
    call MPI_FILE_CLOSE(ifile,ierr)

    return
  end subroutine param_parwrite_inflow  

  ! ================================= !
  ! Write the inflow file to the disk !
  !        (parallel version)         !
  ! ================================= !
  subroutine param_parwrite_inflowy
    use parser
    implicit none

    integer :: n,i,k,file_inflowy
    integer :: inflowy_comm,inflowy_irank
    real(WP), dimension(:,:,:), pointer :: buf
    integer, dimension(:), pointer :: fileview
    integer :: ifiley,ierr,var,data_size
    integer, dimension(3) :: gsizes, lsizes, start
    integer, dimension(MPI_STATUS_SIZE) :: status
    integer, dimension(4) :: dims
    integer(kind=MPI_Offset_kind) :: disp
    integer(kind=MPI_Offset_kind) :: nx_MOK, ny_MOK, nz_MOK
    integer(kind=MPI_Offset_kind) :: WP_MOK, str_MOK
    integer(kind=MPI_Offset_kind) :: NVARS_MOK
    integer(kind=MPI_Offset_kind) :: n_MOK
    logical :: file_is_there  
    character(len=str_medium) :: filenamey

    ! If inflow then continue
    if (.not.allocated(inflowy)) return

    ! Allocate buffer
    allocate(buf(imin_:imax_,kmin_:kmax_,nvar_inflowy))
    allocate(fileview(ntimey))

    ! Split the communicator
    if (jproc.ne.1) then
       file_inflowy = 0
       call MPI_COMM_SPLIT(comm,file_inflowy,MPI_UNDEFINED,inflowy_comm,ierr)
       return
    else
       file_inflowy = 1
       call MPI_COMM_SPLIT(comm,file_inflowy,MPI_UNDEFINED,inflowy_comm,ierr)
    end if

    ! Create processor ranks in inflow_comm communicator
    call MPI_COMM_RANK(inflowy_comm,inflowy_irank,ierr)
    inflowy_irank = inflowy_irank + 1

    ! Get filename
    call parser_read('Init inflow y file',filenamey)
    filenamey = trim(mpiiofs) // trim(filenamey)
    ! Open the file to write
    inquire(file=filenamey,exist=file_is_there)
    if (file_is_there .and. irank.eq.iroot) call MPI_FILE_DELETE(filenamey,mpi_info,ierr)
    call MPI_FILE_OPEN(inflowy_comm,filenamey,IOR(MPI_MODE_WRONLY,MPI_MODE_CREATE),mpi_info,ifiley,ierr)
    
    ! Write header
    if (inflowy_irank.eq.1) then
       ! Write dimensions
       dims(1) = ntimey
       dims(2) = nx
       dims(3) = nz
       dims(4) = nvar_inflowy
       call MPI_FILE_WRITE(ifiley,dims,4,MPI_INTEGER,status,ierr)
       ! Write additional stuff
       call MPI_FILE_WRITE(ifiley,dt_inflowy,1,MPI_REAL_WP,status,ierr)
       call MPI_FILE_WRITE(ifiley,time_inflowy,1,MPI_REAL_WP,status,ierr)
       ! Write variable names
       do var=1,nvar_inflowy
          call MPI_FILE_WRITE(ifiley,names_inflowy(var),str_short,MPI_CHARACTER,status,ierr)
       end do
       ! Write the grid
       call MPI_FILE_WRITE(ifiley,icyl,1,MPI_INTEGER,status,ierr)
       call MPI_FILE_WRITE(ifiley,x(imin:imax+1),nx+1,MPI_REAL_WP,status,ierr)
       call MPI_FILE_WRITE(ifiley,z(kmin:kmax+1),nz+1,MPI_REAL_WP,status,ierr)
    end if

    ! Global sizes
    gsizes(1) = nx
    gsizes(2) = nz
    gsizes(3) = nvar_inflowy
    ! Local sizes
    lsizes(1) = nx_
    lsizes(2) = nz_
    lsizes(3) = nvar_inflowy
    ! Starting index
    start(1) = imin_-imin
    start(2) = kmin_-kmin
    start(3) = 0
    ! Define the size
    data_size = lsizes(1)*lsizes(2)*lsizes(3)
    ! Predefine the view  
    do n=1,ntimey
       call MPI_TYPE_CREATE_SUBARRAY(3,gsizes,lsizes,start,&
            MPI_ORDER_FORTRAN,MPI_REAL_WP,fileview(n),ierr)
       call MPI_TYPE_COMMIT(fileview(n),ierr)
    end do

    ! Resize some integers so MPI can read even the biggest files
    nx_MOK    = int(nx,           MPI_Offset_kind)
    ny_MOK    = int(ny,           MPI_Offset_kind)
    nz_MOK    = int(nz,           MPI_Offset_kind)
    WP_MOK    = int(WP,           MPI_Offset_kind)
    str_MOK   = int(str_short,    MPI_Offset_kind)
    NVARS_MOK = int(nvar_inflowy, MPI_Offset_kind)

    do n=1,ntimey

       ! Transfer all the quantities...
       do var=1,nvar_inflowy
          do k=kmin_,kmax_
             do i=imin_,imax_
                buf(i,k,var) = inflowy(n,i,k,var)
             end do
          end do
       end do

       ! Write the data
       n_MOK = int(n,MPI_Offset_kind)
       disp = 4*4 + 2*WP_MOK + NVARS_MOK*str_MOK + 4 + (nx_MOK+nz_MOK+2)*WP_MOK &
            + (n_MOK-1)*NVARS_MOK*nx_MOK*nz_MOK*WP_MOK          
       call MPI_FILE_SET_VIEW(ifiley,disp,MPI_REAL_WP,fileview(n), &
            "native",mpi_info,ierr)
       call MPI_FILE_WRITE_ALL(ifiley,buf,data_size, &
            MPI_REAL_WP,status,ierr)

    end do

    ! Close the file
    call MPI_FILE_CLOSE(ifiley,ierr)

    return
  end subroutine param_parwrite_inflowy
  
  ! ==================================== !
  ! Write the chemtable file to the disk !
  ! ==================================== !
  subroutine param_write_chemtable
    use parser
    implicit none
    
    integer :: ierr,var,iunit
    character(len=str_medium) :: filename
    
    ! If chemtable then continue
    if (.not.allocated(table)) return
    
    ! Open the data file
    call parser_read('Init chemtable file', filename)
    call BINARY_FILE_OPEN(iunit,trim(filename),"w",ierr)
    
    ! Write sizes
    call BINARY_FILE_WRITE(iunit,n1,1,kind(n1),ierr)
    call BINARY_FILE_WRITE(iunit,n2,1,kind(n2),ierr)
    call BINARY_FILE_WRITE(iunit,n3,1,kind(n3),ierr)
    call BINARY_FILE_WRITE(iunit,nvar_chem,1,kind(nvar_chem),ierr)
    ! Write the axis coordinates
    call BINARY_FILE_WRITE(iunit,x1,n1,kind(x1),ierr)
    call BINARY_FILE_WRITE(iunit,x2,n2,kind(x2),ierr)
    call BINARY_FILE_WRITE(iunit,x3,n3,kind(x3),ierr)
    ! Write the mask of the chemtable
    call BINARY_FILE_WRITE(iunit,chem_mask,n1*n2*n3,kind(chem_mask),ierr)
    ! Write additional stuff
    call BINARY_FILE_WRITE(iunit,combModel,str_medium,kind(combModel),ierr)
    ! Write variable names
    do var=1,nvar_chem
       call BINARY_FILE_WRITE(iunit,names_chem(var),str_medium,kind(names_chem),ierr)
    end do
    ! Write data field
    do var=1,nvar_chem
       call BINARY_FILE_WRITE(iunit,table(:,:,:,var),n1*n2*n3,kind(table),ierr)
    end do
    
    ! Close the file
    call BINARY_FILE_CLOSE(iunit,ierr)
    
    return
  end subroutine param_write_chemtable

  ! ==================================== !
  ! Write the chemtable file to the disk !
  !         (parallel version)           !
  ! ==================================== !
  subroutine param_parwrite_chemtable
    use parser
    implicit none
    
    ! If chemtable then continue
    if (.not.allocated(table)) return
    
    stop 'Parallel write of chemtable not programed'
    
    return
  end subroutine param_parwrite_chemtable
  
end module param
