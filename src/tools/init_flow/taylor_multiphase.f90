module taylor_multiphase
  use string
  use precision
  use param
  implicit none
  
  ! Mesh
  real(WP) :: dx,dy,dz
  real(WP) :: sx,sy
  
  ! Orientation
  character(len=str_medium) :: orientation
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:,:), pointer :: VOF
  
end module taylor_multiphase


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine taylor_multiphase_grid
  use taylor_multiphase
  use math
  use parser
  implicit none
  
  integer :: i,j,k
  
  ! Read the number of points
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  
  ! Read the stretching
  call parser_read('Stretching x',sx)
  call parser_read('Stretching y',sy)
  
  ! Set the periodicity
  xper = 1
  yper = 1
  zper = 1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  dx = twoPi/real(nx,WP)
  dy = twoPi/real(ny,WP)
  dz = twoPi/real(nz,WP)
  do i=1,nx+1
     x(i) = real(i-1,WP)*dx
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*dy
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*dz
  end do
  
  ! Apply stretching
  x = x + sx*sin(x)
  y = y + sy*sin(y)
  
  ! Create the mid points
  do i=1,nx
     xm(i)= 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)= 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine taylor_multiphase_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine taylor_multiphase_data
  use taylor_multiphase
  use parser
  use math
  implicit none
  
  integer :: i,j,k
    
  ! Allocate the array data
  nvar = 11
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U    => data(:,:,:,1); names(1) = 'U'
  V    => data(:,:,:,2); names(2) = 'V'
  W    => data(:,:,:,3); names(3) = 'W'
  VOF  => data(:,:,:, 4:11);
  names( 4) = 'VOF1'
  names( 5) = 'VOF2'
  names( 6) = 'VOF3'
  names( 7) = 'VOF4'
  names( 8) = 'VOF5'
  names( 9) = 'VOF6'
  names(10) = 'VOF7'
  names(11) = 'VOF8'

  ! Create the velocity field
  do k=1,nz
     do j=1,ny
        do i=1,nx
           U(i,j,k) =  cos(x (i))*sin(ym(j))
           V(i,j,k) = -sin(xm(i))*cos(y (j))
           W(i,j,k) = 0.0_WP
        end do
     end do
  end do

  ! Create the VOF field
  VOF=0.0_WP
  do k=1,nz
     do j=1,ny
        do i=1,nx
           if (sqrt((xm(i)-0.5_WP*Pi)**2+(ym(j)-0.5_WP*Pi)**2)<0.5_WP) then
              VOF(i,j,k,:)=1.0_WP
           end if
        end do
     end do
  end do
  
  return
end subroutine taylor_multiphase_data

