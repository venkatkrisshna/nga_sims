module fbmixing
  use precision
  use param
  implicit none

  ! Length and diameter of the domain
  real(WP) :: Lx,Ly,Lz
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: RHO
  real(WP), dimension(:,:,:), pointer :: dRHO
  
end module fbmixing


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine fbmixing_grid
  use fbmixing
  use parser
  implicit none
  
  integer :: i,j,k
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  
  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  call parser_read('Lz',Lz)

  ! Set the periodicity
  call parser_read('Periodic in X',xper)
  yper=1
  zper=1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*Lx/real(nx,WP)
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*Ly/real(ny,WP) - 0.5_WP*Ly
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*Lz/real(nz,WP) - 0.5_WP*Lz
  end do
  
  ! Create the mid points 
  do i=1,nx
     xm(i)= 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)= 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine fbmixing_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine fbmixing_data
  use fbmixing
  use parser
  implicit none
  
  real(WP) :: meanU,meanV,meanW
  
  ! Allocate the array data
  nvar = 6
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U    => data(:,:,:,1); names(1) = 'U'
  V    => data(:,:,:,2); names(2) = 'V'
  W    => data(:,:,:,3); names(3) = 'W'
  P    => data(:,:,:,4); names(4) = 'P'
  RHO  => data(:,:,:,5); names(5) = 'RHO'
  dRHO => data(:,:,:,6); names(6) = 'dRHO'
  
  ! Initialize fluid velocity
  call parser_read('Mean U Velocity',meanU,0.0_WP)
  call parser_read('Mean V Velocity',meanV,0.0_WP)
  call parser_read('Mean W Velocity',meanW,0.0_WP)
  
  ! Create them
  U    = meanU
  V    = meanV
  W    = meanW
  P    = 0.0_WP
  RHO  = 0.0_WP
  dRHO = 0.0_WP
  
  return
end subroutine fbmixing_data


! ========================================= !
! Generate an initial particle distribution !
! ========================================= !
subroutine fbmixing_part
  use fbmixing
  use parser
  use lpt_mod
  use random
  implicit none

  character(len=str_medium) :: distro
  character(len=str_medium) :: part_init
  logical :: use_lpt
  integer :: iunit,ierr
  integer :: i,np
  real(WP) :: Hbed,Dbed,VFavg
  real(WP) :: Lp,mean_d
  real(WP) :: uin,vin,win,amp
  real(WP) :: dt,time,rand

  ! Check whether lpt is used
  call parser_is_defined('Init part file',use_lpt)
  if (.not.use_lpt) return

  ! Particle distribution
  call parser_read('Particle distribution',distro)

  ! Read average volume fraction and bed height
  call parser_read('Bed height',Hbed)
  call parser_read('Bed diameter',Dbed,0.0_WP)
  call parser_read('Avg. volume fraction',VFavg)

  ! Velocity parameters
  call parser_read('Particle u velocity',uin,0.0_WP)
  call parser_read('Particle v velocity',vin,0.0_WP)
  call parser_read('Particle w velocity',win,0.0_WP)
  call parser_read('Particle fluctuations',amp,0.0_WP)

  ! Distribute particles / assign diameter
  select case (trim(distro))
  case ('uniform')
     call fbmixing_uniform(Hbed,Dbed,VFavg,np,Lp,mean_d)
  case ('random')
     call fbmixing_random(Hbed,Dbed,VFavg,np,Lp,mean_d)
  case default
     stop 'Unknown particle distribution'
  end select

  ! Initialize the random number generator
  call random_init

  ! Set particle parameters
  do i=1,np
     
     ! Set various parameters for the particle
     part(i)%dt  =0.0_WP
     part(i)%Acol=0.0_WP
     part(i)%Tcol=0.0_WP
     part(i)%wx  =0.0_WP
     part(i)%wy  =0.0_WP
     part(i)%wz  =0.0_WP
     part(i)%id  =int(i,kind=8)
     part(i)%stop=0
     
     ! Give it a velocity
     if (nx.gt.1) then
        part(i)%u = uin
        if (amp.gt.0.0_WP) then
           call random_number(rand)
           rand=2.0_WP*rand-1.0_WP
           part(i)%u = part(i)%u + amp*rand
        end if
     else
        part(i)%u = 0.0_WP
     end if
     if (ny.gt.1) then
        part(i)%v = vin
        if (amp.gt.0.0_WP) then
           call random_number(rand)
           rand=2.0_WP*rand-1.0_WP
           part(i)%v = part(i)%v + amp*rand
        end if
     else
        part(i)%v = 0.0_WP
     end if
     if (nz.gt.1) then
        part(i)%w = win
        if (amp.gt.0.0_WP) then
           call random_number(rand)
           rand=2.0_WP*rand-1.0_WP
           part(i)%w = part(i)%w + amp*rand
        end if
     else
        part(i)%w = 0.0_WP
     end if
     
  end do
  
  ! Output
  print*,'Number of particles: ',np
  print*,'Interparticle spacing: ',Lp
  print*,'Effective mean diameter: ',mean_d
  print*,'Effective volume fraction: ',VFavg

  ! Generate the initial particle file
  call parser_read('Init part file',part_init)
  call BINARY_FILE_OPEN(iunit,trim(part_init),"w",ierr)
  call BINARY_FILE_WRITE(iunit,np,1,kind(np),ierr)
  call BINARY_FILE_WRITE(iunit,part_kind,1,kind(part_kind),ierr)
  dt = 0.0_WP
  call BINARY_FILE_WRITE(iunit,dt,1,kind(dt),ierr)
  time = 0.0_WP
  call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr)
  do i=1,np
     call BINARY_FILE_WRITE(iunit,part(i),1,part_kind,ierr)
  end do
  call BINARY_FILE_CLOSE(iunit,ierr)

  return
end subroutine fbmixing_part


! ==================== !
! Uniform distribution !
! ==================== !
subroutine fbmixing_uniform(Hbed,Dbed,VFavg,np,Lp,mean_d)
  use fbmixing
  use lpt_mod
  use parser
  use math
  use random
  implicit none
  
  integer :: i
  integer, intent(out) :: np
  integer  :: ix,iy,iz,pcount
  integer  :: npartx,nparty,npartz
  real(WP), intent(in) :: Hbed,Dbed,VFavg
  real(WP), intent(out) :: Lp,mean_d
  real(WP) :: Volp,rp,Lpy,Lpz,rand
  real(WP), dimension(:), pointer :: xtmp,ytmp,ztmp
  
  ! Read particle diameter
  call parser_read('Particle mean diameter',mean_d)
  
  ! Particle volume
  Volp = Pi/6.0_WP*mean_d**3

  ! Mean interparticle distance
  Lp = (Volp/VFavg)**(1.0_WP/3.0_WP)
  npartx = int(Hbed/Lp)
  Lp = Hbed/real(npartx,WP)
  if (Dbed.gt.0.0_WP .and. nz.eq.1) then
     ! Include walls 2D
     nparty = int(Dbed/Lp)
     Lpy = Dbed/real(nparty,WP)
  else
     nparty = int(Ly/Lp)
     Lpy = Ly/real(nparty,WP)
  end if
  npartz = int(Lz/Lp)
  Lpz = Lz/real(npartz,WP)
  np = npartx*nparty*npartz

  ! Allocate temporary particle arrays
  allocate(xtmp(1:np))
  allocate(ytmp(1:np))
  allocate(ztmp(1:np))

  ! Distribute particles across entire domain
  pcount=0
  do i=1,np
     ix = (i-1)/(nparty*npartz)
     iy = (i-1-nparty*npartz*ix)/npartz
     iz = i-1-nparty*npartz*ix-npartz*iy
     xtmp(i) = x(1)+(ix+0.5_WP)*Lp
     if (Dbed.gt.0.0_WP .and. nz.eq.1) then
        ! Include walls 2D
        ytmp(i) = -0.5_WP*Dbed+(iy+0.5_WP)*Lpy
     else
        ytmp(i) = y(1)+(iy+0.5_WP)*Lpy
     end if
     ztmp(i) = z(1)+(iz+0.5_WP)*Lpz

     ! Detect particles outside diameter
     if (Dbed.gt.0.0_WP) then
        rp=sqrt(ytmp(i)**2+ztmp(i)**2)+0.5_WP*mean_d
        if (rp.ge.0.5_WP*Dbed) pcount=pcount+1
     end if
  end do

  ! Allocate particle array
  allocate(part(1:np-pcount))
  
  ! Distribute particles
  pcount=0
  do i=1,np
     ix = (i-1)/(nparty*npartz)
     iy = (i-1-nparty*npartz*ix)/npartz
     iz = i-1-nparty*npartz*ix-npartz*iy
     xtmp(i) = x(1)+(ix+0.5_WP)*Lp
     if (Dbed.gt.0.0_WP .and. nz.eq.1) then
        ! Include walls 2D
        ytmp(i) = -0.5_WP*Dbed+(iy+0.5_WP)*Lpy
     else
        ytmp(i) = y(1)+(iy+0.5_WP)*Lpy
     end if
     ztmp(i) = z(1)+(iz+0.5_WP)*Lpz

     ! Detect particles outside diameter
     if (Dbed.gt.0.0_WP) then
        rp=sqrt(ytmp(i)**2+ztmp(i)**2)+0.5_WP*mean_d
        if (rp.lt.0.5_WP*Dbed) then
           pcount=pcount+1
           part(pcount)%d=mean_d
           part(pcount)%x=xtmp(i)
           part(pcount)%y=ytmp(i)
           part(pcount)%z=ztmp(i)
        end if
     else
        pcount=pcount+1
        part(pcount)%d=mean_d
        part(pcount)%x=xtmp(i)
        part(pcount)%y=ytmp(i)
        part(pcount)%z=ztmp(i)
     end if
  end do

  ! Update number of particles
  np=pcount
 
  return
end subroutine fbmixing_uniform


! =================== !
! Random distribution !
! =================== !
subroutine fbmixing_random(Hbed,Dbed,VFavg,np,Lp,mean_d)
  use fbmixing
  use lpt_mod
  use parser
  use math
  use random
  implicit none
  
  integer :: i
  integer, intent(out) :: np
  real(WP), intent(in) :: Hbed,Dbed
  real(WP), intent(out) :: Lp,mean_d
  real(WP), intent(inout) :: VFavg
  real(WP) :: Volp,Volbed,sumVolp,rp,rand
  real(WP) :: d_mean,d_min,d_max,d_sd,d_shift
  real(WP), dimension(:), pointer :: dtmp
  
  ! Read particle diameter
  call parser_read('Particle mean diameter',d_mean)
  call parser_read('Particle standard deviation',d_sd,0.0_WP)
  call parser_read('Particle min diameter',d_min,0.0_WP)
  call parser_read('Particle max diameter',d_max,0.0_WP)
  call parser_read('Particle diameter shift',d_shift,0.0_WP)

  ! Initialize the random number generator
  call random_init
  
  ! First get bed volume
  if (Dbed.gt.0.0_WP) then
     ! Include walls
     if (nz.eq.1) then
        ! 2D cartesian
        Volbed = Hbed*Dbed*Lz
     else
        ! 3D cylinder
        Volbed = Hbed*Pi*0.25_WP*Dbed**2
     end if
  else
     ! No walls
     Volbed = Hbed*Ly*Lz
  end if
  
  ! Estimate number of particles
  Volp = Pi/6.0_WP*d_mean**3
  np = 5*int(VFavg*Volbed/Volp)
  
  ! Allocate temporary particle size array
  allocate(dtmp(np))

  ! Generate particles randomly until volume is sufficient
  np=0
  mean_d=0.0_WP
  sumVolp=0.0_WP
  do while (sumVolp.lt.VFavg*Volbed)
     
     ! Add a particle
     np=np+1
     
     ! Set particle size distribution (compact support Gaussian)
     !dtmp(np)=random_normal(m=d_mean,sd=d_sd)
     !if (d_sd.gt.0.0_WP) then
     !   do while (dtmp(np).gt.d_max.or.dtmp(np).lt.d_min)
     !      dtmp(np)=random_normal(m=d_mean,sd=d_sd)
     !   end do
     !else
     !   dtmp(np)d_mean
     !end if
     ! Set particle size distribution (compact support lognormal)
     dtmp(np)=random_lognormal(m=d_mean,sd=d_sd)+d_shift
     if (d_sd.gt.0.0_WP) then
        do while (dtmp(np).gt.d_max+epsilon(1.0_WP).or.dtmp(np).lt.d_min-epsilon(1.0_WP))
           dtmp(np)=random_lognormal(m=d_mean,sd=d_sd)+d_shift
        end do
     else
        dtmp(np)=d_mean
     end if
     
     ! Add up total particle volume
     sumVolp=sumVolp+Pi/6.0_WP*dtmp(np)**3.0_WP
     
     ! Compute mean particle diameter
     mean_d=mean_d+dtmp(np)
     
  end do

  ! Mean particle diameter
  mean_d = mean_d/np
  
  ! Mean particle volume
  Volp = sumVolp/np

  ! Mean interparticle distance
  Lp = (Volp/VFavg)**(1.0_WP/3.0_WP)

  ! Allocate particle array
  allocate(part(1:np))
  
  ! Effective volume fraction
  VFavg = sumVolp/Volbed
  
  ! Distribute particles
  do i=1,np
     ! Set particle size
     part(i)%d = dtmp(i)

     call random_number(rand)
     part(i)%x = Hbed*rand
     if (Dbed.gt.0.0_WP) then
        ! Include walls
        if (nz.eq.1) then
           ! 2D cartesian
           call random_number(rand)
           rand = rand-0.5_WP
           part(i)%y = Dbed*rand
           part(i)%z = 0.0_WP
        else
           ! 3D cylinder
           rp=huge(1.0_WP)
           do while (rp.ge.0.5_WP*Dbed)
              call random_number(rand)
              rand = rand-0.5_WP
              part(i)%y = Dbed*rand
              call random_number(rand)
              rand = rand-0.5_WP
              part(i)%z = Dbed*rand
              rp=sqrt(part(i)%y**2+part(i)%z**2)+0.5_WP*part(i)%d
           end do
        end if
     else
        ! No walls
        if (nz.eq.1) then
           call random_number(rand)
           rand = rand-0.5_WP
           part(i)%y = Ly*rand
           part(i)%z = 0.0_WP
        else
           call random_number(rand)
           rand = rand-0.5_WP
           part(i)%y = Ly*rand
           call random_number(rand)
           rand = rand-0.5_WP
           part(i)%z = Lz*rand
        end if
     end if
  end do
 
  return
end subroutine fbmixing_random


! ====================== !
! Create the IB variable !
! ====================== !
subroutine fbmixing_optdata
  use fbmixing
  use parser
  implicit none
  
  real(WP) :: Dbed,Dfb,h1,h2,a,b,c,d,r
  integer :: i,j,k
  
  ! Gib opt data
  call parser_read('Bed diameter',Dbed,0.0_WP)
  call parser_read('Freeboard diameter',Dfb,0.0_WP)
  if (Dbed.gt.0.0_WP) then
     nod = 1
     allocate(OD(nx,ny,nz,nod))
     allocate(OD_names(nod))
     OD_names(1) = 'Gib'
  end if
  
  ! Select geometry
  if (Dbed.gt.0.0_WP .and. Dfb.eq.0.0_WP) then
     ! Cylinder only
     do k=1,nz
        do j=1,ny
           OD(:,j,k,1)=0.5_WP*Dbed-sqrt(zm(k)**2+ym(j)**2)
        end do
     end do
  else if (Dbed.gt.0.0_WP .and. Dfb.gt.0.0_WP) then
     ! Cylinder & freeboard
     call parser_read('Bed end',h1)
     call parser_read('Freeboard begin',h2)
     a=0.5_WP*(Dfb-Dbed)/(h2-h1)
     b=0.5_WP/(h2-h1)*(h2*Dbed-h1*Dfb)
     c=-1.0_WP/a
     d=0.5_WP*Dbed+h1/a
     do k=1,nz
        do j=1,ny
           do i=1,nx
              r=sqrt(zm(k)**2+ym(j)**2)
              if (xm(i).lt.h1) then
                 OD(i,j,k,1)=0.5_WP*Dbed-r
              !else if (xm(i).ge.h1 .and. xm(i).lt.(c*r+d)) then
              !   OD(i,j,k,1)=sqrt()
              else if (xm(i).ge.h1 .and. xm(i).lt.h2) then
                 OD(i,j,k,1)=a*xm(i)+b-r
              else if (xm(i).ge.h2) then
                 OD(i,j,k,1)=0.5_WP*Dfb-r
              end if
           end do
        end do
     end do
  end if
  
  return
end subroutine fbmixing_optdata


! =================== !
! Generate the inflow !
! =================== !
subroutine fbmixing_inflow
  use fbmixing
  use parser
  use math
  use fileio
  implicit none
  
  integer :: i,j,k,nhole,nhole_eff,iunit
  real(WP) :: dhole,dbed,Lh
  real(WP) :: y0,z0,sig
  
  ! Only if not periodic
  if (xper.eq.1) return
  
  ! If no holes return
  call parser_read('Number of holes',nhole,0)
  if (nhole.eq.0) return
  
  ! Generate an inflow file
  nvar_inflow = 3
  ntime = 2
  
  ! Allocate some arrays
  allocate(inflow(ntime,ny,nz,nvar_inflow))
  allocate(names_inflow(nvar_inflow))
  
  ! Link the pointers
  U => inflow(:,:,:,1); names_inflow(1) = 'U'
  V => inflow(:,:,:,2); names_inflow(2) = 'V'
  W => inflow(:,:,:,3); names_inflow(3) = 'W'
  
  ! Compute time grid
  time_inflow = 0.0_WP
  dt_inflow   = 1.0_WP
  
  ! Read input
  call parser_read('Hole diameter',dhole,0.0_WP)
  call parser_read('Bed diameter',dbed,0.0_WP)
  
  ! Exit if hole diameter cannot be resolved
  if (dhole.lt.Lx/real(nx,WP)) stop 'Hole diameter is too small!'
  
  ! Initialize velocity
  U = 0.0_WP
  V = 0.0_WP
  W = 0.0_WP
  
  ! Gas jet width
  sig=0.5_WP*dhole
  
  ! Total number of holes
  nhole_eff=0
  
  ! Generate Gaussian jets
  if (nz.eq.1) then
     
     ! 2D
     Lh=Ly/real(nhole,WP)
     y0=0.0_WP
     z0=0.0_WP
     do i=1,nhole
        if (mod(nhole,2).eq.0) then
           ! Even number of holes
           if (i.eq.1) then
              y0=0.5_WP*Lh
           else
              y0=y0+(-1.0_WP)**(real(i,WP)-1.0_WP)*Lh*(real(i,WP)-1.0_WP)
           end if
        else
           ! Odd number of holes
           y0=y0+(-1.0_WP)**(real(i,WP)-1.0_WP)*Lh*(real(i,WP)-1.0_WP)
        end if
        if (Dbed.gt.0.0_WP) then
           ! Check if hole is outside IB
           if (abs(y0).ge.0.5_WP*(dbed-dhole)) cycle
        else
           ! Check if hole is outside domain
           if (abs(y0).gt.0.5_WP*(Ly-dhole)) cycle
        end if
        nhole_eff=nhole_eff+1
        do j=1,ny
           U(:,j,:)=U(:,j,:)+exp(-((ym(j)-y0)**2)/((2.0_WP*sig**2)))
           if (Dbed.gt.0.0_WP) then
              ! Remove velocity outside IB
              if (abs(ym(j)).ge.0.5_WP*dbed) U(:,j,:)=0.0_WP
           end if
        end do
     end do
     
  else
     
     ! 3D
     Lh=Ly/sqrt(real(nhole,WP))
     y0=-0.5_WP*Ly+0.5_WP*Lh
     z0=-0.5_WP*Lz+0.5_WP*Lh
     do i=1,nhole
        if (i.gt.1) then
           if (mod(real(i-1,WP),sqrt(real(nhole,WP))).eq.0.0_WP) then
              y0=-0.5_WP*Ly+0.5_WP*Lh
              z0=z0+Lh
           else
              y0=y0+Lh
           end if
        end if
        if (Dbed.gt.0.0_WP) then
           ! Check if hole is outside IB
           if (sqrt(y0**2+z0**2).ge.0.5_WP*dbed-dhole) cycle
        else
           ! Check if hole is outside domain
           if (abs(y0).gt.0.5_WP*(Ly-dhole) .or. abs(z0).gt.0.5_WP*(Lz-dhole)) cycle
        end if
        nhole_eff=nhole_eff+1
        do k=1,nz
           do j=1,ny
              U(:,j,k)=U(:,j,k)+exp(-((ym(j)-y0)**2/(2.0_WP*sig**2)+(zm(k)-z0)**2/(2.0_WP*sig**2)))
              if (Dbed.gt.0.0_WP) then
                 ! Remove velocity outside IB
                 if (sqrt(ym(j)**2+zm(k)**2).ge.0.5_WP*(dbed-dhole)) U(:,j,k)=0.0_WP
              end if
           end do
        end do
     end do
  end if
  
  ! Print out effective number of holes
  print*,'Number of holes:',nhole_eff
  
  ! Dump profile to a file
  iunit=iopen()
  open(iunit,file='testinflow',form='formatted')
  do k=1,nz
     do j=1,ny
        write(iunit,*) ym(j),zm(k),U(1,j,k)
     end do
  end do
  close(iclose(iunit))

  return
end subroutine fbmixing_inflow
