! =========================== !
! Tools for DG initialization !
! =========================== !
module dg
  use string
  use precision
  use param
  use polynomial
  implicit none
  
  ! DG order
  integer :: pdg
  integer :: ndof
  
  ! DG basis
  integer,  dimension(:,:), pointer :: basis
  type(poly), dimension(:), pointer ::   phi
  type(poly), dimension(:), pointer ::  dphi
  type(poly), dimension(:), pointer :: ddphi
  
  ! Quadrature
  integer :: nquad
  real(WP), dimension(:),   pointer :: ql,qw
  real(WP), dimension(:,:), pointer :: ab
  
contains
  
  ! Evaluate basis function
  function DGbasis_eval(pos)
    implicit none
    real(WP), dimension(3), intent(in) :: pos
    real(WP), dimension(ndof) :: DGbasis_eval
    real(WP), dimension(3) :: tmp
    integer :: n,d,i
    do n=1,ndof
       tmp=0.0_WP
       ! Loop over directions
       do d=1,3
          ! Evaluate polynomial
          do i=1,phi(basis(n,d))%n 
             tmp(d)=tmp(d)+phi(basis(n,d))%a(i)*pos(d)**phi(basis(n,d))%p(i)
          end do
       end do
       DGbasis_eval(n)=product(tmp)
    end do
  end function DGbasis_eval
  
end module dg

! =================== !
! Initialize DG field !
! =================== !
subroutine dg_init(func)
  use dg
  use string
  use parser
  use quadrature
  use legendre
  implicit none
  
  real(WP), external :: func
  integer, parameter :: dim=3
  integer :: i,j,k
  integer :: ii,jj,kk
  real(WP), dimension(:), pointer :: phi_t
  real(WP), dimension(:), pointer :: loc
  real(WP) :: xq,yq,zq
  real(WP) :: func_here
  real(WP), dimension(:), pointer :: Mmtx,Mmtxi
  type(poly) :: polytmp
  integer :: count,dir,n
  real(WP), parameter :: xmin=-1.0_WP
  real(WP), parameter :: xmax=+1.0_WP
  real(WP) :: cls_eps,eps
  
  ! Get DG order
  call parser_read('Init DG order',pdg)
  
  ! Read hyperbolic tangent width factor
  call parser_read('CLS epsilon',cls_eps,0.5_WP) 
  
  ! Initialize DG basis -------------------------------------
  ndof=legendre_ndof(pdg)
  allocate(basis(ndof,dim))
  allocate(  phi(0:pdg))
  allocate( dphi(0:pdg))
  allocate(ddphi(0:pdg))
  call legendre_generate(pdg,ndof,basis,phi,dphi,ddphi)
  
  ! Form opt data header info -------------------------------
  nod=ndof
  allocate(OD(nx,ny,nz,nod))
  allocate(OD_names(nod))
  do i=1,ndof
     write(OD_names(i),'(A2,I0)') 'DG',i
  end do
  
  ! Intialize quadrature ------------------------------------
  nquad=ceiling(0.5_WP*real(3*pdg,WP))
  allocate(ql(nquad))
  allocate(qw(nquad))
  allocate(ab(nquad,1:2))
  call rjacobi(nquad,0.0_WP,0.0_WP,ab)
  call lobatto(nquad-2,ab,-1.0_WP,1.0_WP,ql,qw)
  deallocate(ab); nullify(ab)
  ql=0.5_WP*(ql+1.0_WP)
  qw=0.5_WP*qw
  qw=qw/sum(qw)
  
  ! Construct mass matrix -----------------------------------
  allocate(Mmtx (ndof))
  allocate(Mmtxi(ndof))
  nullify(polytmp%a); nullify(polytmp%p)
  do i=1,ndof
     ! Initialize mass matrix
     Mmtx(i)=1.0_WP
     ! Compute integral of phi_i * phi_i over [xmin,xmax]^3
     do dir=1,dim
        ! Allocate polytmp
        polytmp%n=phi(basis(i,dir))%n*phi(basis(i,dir))%n
        allocate(polytmp%a(polytmp%n))
        allocate(polytmp%p(polytmp%n))
        ! Form phi_i*phi_i in direction dir in polytmp
        count=0
        do ii=1,phi(basis(i,dir))%n
           do jj=1,phi(basis(i,dir))%n
              count=count+1
              polytmp%a(count)=phi(basis(i,dir))%a(ii)*phi(basis(i,dir))%a(jj)
              polytmp%p(count)=phi(basis(i,dir))%p(ii)+phi(basis(i,dir))%p(jj)
           end do
        end do
        ! Compute integral
        Mmtx(i)=Mmtx(i)*integral(polytmp,xmin,xmax)
        ! Deallocate temporary polynomial
        polytmp%n=0
        deallocate(polytmp%a)
        deallocate(polytmp%p)
     end do
     Mmtxi(i)=1.0_WP/Mmtx(i)
  end do
  
  ! Solve RHS using quadrature ------------------------------
  allocate(phi_t(ndof))
  allocate(loc(dim))
  OD=0.0_WP
  ! Loop over quadrature
  do ii=1,nquad
     loc(1)=(ql(ii)-0.5_WP)*2.0_WP
     do jj=1,nquad 
        loc(2)=(ql(jj)-0.5_WP)*2.0_WP
        do kk=1,nquad
           loc(3)=(ql(kk)-0.5_WP)*2.0_WP
           ! Calculate test function here
           phi_t=DGbasis_eval(loc)
           ! Loop over domain
           do k=1,nz 
              zq=zm(k)+(ql(kk)-0.5_WP)*(z(k+1)-z(k))
              do j=1,ny
                 yq=ym(j)+(ql(jj)-0.5_WP)*(y(j+1)-y(j))
                 do i=1,nx
                    xq=xm(i)+(ql(ii)-0.5_WP)*(x(i+1)-x(i))
                    ! Calculate epsilon here
                    eps=cls_eps*min(x(i+1)-x(i),y(j+1)-y(j),z(k+1)-z(k))
                    ! Get distance function value here
                    func_here=func(xq,yq,zq,i,j,k)
                    ! Make distance function into hyperbolic tangent
                    func_here=0.5_WP*(tanh(0.5_WP*func_here/eps)+1.0_WP)
                    ! Update quadrature
                    OD(i,j,k,1:ndof)=OD(i,j,k,1:ndof)+qw(ii)*qw(jj)*qw(kk)*func_here*phi_t(1:ndof)*8.0_WP
                 end do
              end do
           end do
        end do
     end do
  end do
  
  ! Finish calculation of weights ---------------------------
  do k=1,nz 
     do j=1,ny
        do i=1,nx
           OD(i,j,k,:)=OD(i,j,k,:)*Mmtxi(:)
        end do
     end do
  end do

  return
end subroutine dg_init
