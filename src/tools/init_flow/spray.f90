! -------------------------- !
! Test module for spray      !
! - conducts wall collisions !
! - I/O operations           !
! - 0D integrations          !
! -------------------------- !
module spray
  use precision
  use math
  use param
  use string
  implicit none
  
  ! Length/diameter of the domain
  real(WP) :: L
  
  ! Case
  character(len=str_medium) :: case
  
  ! Spray file
  character(len=str_medium) :: part_init
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: ZMIX
  
end module spray


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine spray_grid
  use spray
  use parser
  implicit none
  
  integer :: i,j,k
  
  ! Read in the size of the domain
  call parser_read('Domain size',L)
  
  ! Read in the mesh size
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  call parser_read('cyl',icyl)
  
  ! Set the periodicity
  xper = 0
  yper = 0
  zper = 1
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Generate the mesh
  do i=1,nx+1
     x(i) = (i-1)*L/nx
  end do
  do j=1,ny+1
     y(j) = (j-1)*L/ny
  end do
  if (icyl.eq.1) then
     do k=1,nz+1
        z(k) = (k-1)*twoPi/nz
     end do
  else
     do k=1,nz+1
        z(k) = (k-1)*L/nz
     end do
  end if
     
  ! Create the mid points 
  do i=1,nx
     xm(i)= 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)= 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Read in the case
  call parser_read('case',case)
  
  ! Generate the mask based on the case
  mask=0
  select case (trim(adjustl(case)))
  case ('wall-x1') ! Straight collision with wall in +x
     if (icyl.eq.0) then
        mask(2*nx/4:3*nx/4,ny/4:3*ny/4)=1
     else
        mask(2*nx/4:3*nx/4,:ny/2)=1
     end if
  case ('wall-x2') ! Straight collision with wall in -x
     if (icyl.eq.0) then
        mask(nx/4:2*nx/4,ny/4:3*ny/4)=1
     else
        mask(nx/4:2*nx/4,:ny/2)=1
     end if
  case ('wall-y1') ! Straight collision with wall in +y
     if (icyl.eq.0) then
        mask(:,3*ny/4:)=1
     else
        mask(:,3*ny/4:)=1
     end if
  case ('wall-y2') ! Straight collision with wall in -y
     if (icyl.eq.0) then
        mask(:,:1*ny/4)=1
     else
        mask(:,:1*ny/4)=1
     end if
  case ('wall-corner') ! Corner collision
     if (icyl.eq.0) then
        mask(:,ny/2:)=1
        mask(2*nx/4:3*nx/4,ny/4:3*ny/4)=1
     else
        mask(:,ny/2:)=1
        mask(2*nx/4:3*nx/4,ny/4:3*ny/4)=1
     end if
  case default
     xper = 1
     yper = 1
     zper = 1
     mask(:,:)=0
  end select
  
  return
end subroutine spray_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine spray_data
  use spray
  use parser
  use lpt_mod
  implicit none
  
  integer :: iunit,ierr,i,j,k,count
  real(WP) :: dt,time,Zmix_input
  
  ! Allocate the array data
  nvar = 5
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U => data(:,:,:,1); names(1) = 'U'
  V => data(:,:,:,2); names(2) = 'V'
  W => data(:,:,:,3); names(3) = 'W'
  P => data(:,:,:,4); names(4) = 'P'
  ZMIX => data(:,:,:,5); names(5) = 'ZMIX'
  
  ! Initialize the arrays
  U = 0.0_WP
  V = 0.0_WP
  W = 0.0_WP
  P = 0.0_WP
  call parser_read('Zmix',zmix_input)
  ZMIX = zmix_input
  
!!$  ! Generate one droplet based on the case
!!$  npart = 1
!!$  allocate(part(npart))
!!$  part(1)%m       = 1.0_WP
!!$  part(1)%d       = 1.0_WP
!!$  part(1)%rho     = 1.0_WP
!!$  part(1)%T       = 0.0_WP
!!$  part(1)%dmdt    = 0.0_WP
!!$  part(1)%beta    = 0.0_WP
!!$  part(1)%tbreak  = 0.0_WP
!!$  part(1)%nparcel = 1.0_WP
!!$  part(1)%dt      = 0.0_WP
!!$  part(1)%i       = 0
!!$  part(1)%j       = 0
!!$  part(1)%k       = 0
!!$  part(1)%tag     = 0
!!$  part(1)%stop    = 0
!!$  select case (trim(adjustl(case)))
!!$  case ('wall-x1') ! Straight collision with wall in +x
!!$     if (icyl.eq.0) then
!!$        part(1)%x       = L/4.0_WP
!!$        part(1)%y       = L/2.0_WP
!!$        part(1)%z       = L/2.0_WP
!!$        part(1)%u       = 0.5_WP
!!$        part(1)%v       = 0.2_WP
!!$        part(1)%w       = 0.2_WP
!!$     else
!!$        part(1)%x       = L/4.0_WP
!!$        part(1)%y       = 0.0_WP
!!$        part(1)%z       = 0.0_WP
!!$        part(1)%u       = 0.5_WP
!!$        part(1)%v       = 0.5_WP
!!$        part(1)%w       = 0.5_WP
!!$     end if
!!$  case ('wall-x2') ! Straight collision with wall in -x
!!$     if (icyl.eq.0) then
!!$        part(1)%x       = 3.0_WP*L/4.0_WP
!!$        part(1)%y       = L/2.0_WP
!!$        part(1)%z       = L/2.0_WP
!!$        part(1)%u       = -0.5_WP
!!$        part(1)%v       = 0.2_WP
!!$        part(1)%w       = 0.2_WP
!!$     else
!!$        part(1)%x       = 3.0_WP*L/4.0_WP
!!$        part(1)%y       = 0.0_WP
!!$        part(1)%z       = 0.0_WP
!!$        part(1)%u       = -0.5_WP
!!$        part(1)%v       = 0.5_WP
!!$        part(1)%w       = 0.5_WP
!!$     end if
!!$  case ('wall-y1') ! Straight collision with wall in +y
!!$     if (icyl.eq.0) then
!!$        part(1)%x       = L/2.0_WP
!!$        part(1)%y       = L/2.0_WP
!!$        part(1)%z       = L/2.0_WP
!!$        part(1)%u       = 0.2_WP
!!$        part(1)%v       = 0.5_WP
!!$        part(1)%w       = 0.2_WP
!!$     else
!!$        part(1)%x       = L/2.0_WP
!!$        part(1)%y       = L/2.0_WP
!!$        part(1)%z       = 0.0_WP
!!$        part(1)%u       = 0.1_WP
!!$        part(1)%v       = 0.5_WP
!!$        part(1)%w       = 0.5_WP
!!$     end if
!!$  case ('wall-y2') ! Straight collision with wall in -y
!!$     if (icyl.eq.0) then
!!$        part(1)%x       = L/2.0_WP
!!$        part(1)%y       = L/2.0_WP
!!$        part(1)%z       = L/2.0_WP
!!$        part(1)%u       = 0.2_WP
!!$        part(1)%v       = -0.5_WP
!!$        part(1)%w       = 0.2_WP
!!$     else
!!$        part(1)%x       = L/2.0_WP
!!$        part(1)%y       = L/2.0_WP
!!$        part(1)%z       = 0.0_WP
!!$        part(1)%u       = 0.2_WP
!!$        part(1)%v       = -0.5_WP
!!$        part(1)%w       = 0.2_WP
!!$     end if
!!$  case ('wall-corner') ! Corner collision
!!$     if (icyl.eq.0) then
!!$        part(1)%x       = L/4.0_WP!+0.0001_WP
!!$        part(1)%y       = L/4.0_WP
!!$        part(1)%z       = L/2.0_WP
!!$        part(1)%u       = 0.5_WP
!!$        part(1)%v       = 0.5_WP
!!$        part(1)%w       = 0.2_WP
!!$     else
!!$        part(1)%x       = L/4.0_WP
!!$        part(1)%y       = L/4.0_WP
!!$        part(1)%z       = 0.0_WP
!!$        part(1)%u       = 0.5_WP
!!$        part(1)%v       = 0.5_WP
!!$        part(1)%w       = 0.2_WP
!!$     end if
!!$  case ('evap') ! Evaporation
!!$     part(1)%d       = 10.0E-6_WP
!!$     part(1)%rho     = 664.0_WP
!!$     part(1)%m       = part(1)%rho*PI/6.0_WP*(part(1)%d)**3
!!$     part(1)%T       = 300.0_WP
!!$     part(1)%dmdt    = 0.0_WP
!!$     part(1)%beta    = 0.0_WP
!!$     part(1)%tbreak  = 0.0_WP
!!$     part(1)%nparcel = 1.0_WP
!!$     part(1)%dt      = 0.0_WP
!!$     part(1)%i       = 0
!!$     part(1)%j       = 0
!!$     part(1)%k       = 0
!!$     part(1)%tag     = 0
!!$     part(1)%stop    = 0
!!$     part(1)%x       = L/2.0_WP
!!$     part(1)%y       = L/2.0_WP
!!$     part(1)%z       = L/2.0_WP
!!$     part(1)%u       = 0.0_WP
!!$     part(1)%v       = 0.0_WP
!!$     part(1)%w       = 0.0_WP
!!$  case ('entrainment-1') ! Entrainment of air by droplet
!!$     npart = nx*ny*nz
!!$     deallocate(part)
!!$     allocate(part(npart))
!!$     count = 0
!!$     do i=1,nx
!!$        do j=1,ny
!!$           do k=1,nz
!!$              count = count+1
!!$              part(count)%d       = 0.001_WP
!!$              part(count)%rho     = 600.0_WP
!!$              part(count)%m       = part(count)%rho*PI/6.0_WP*(part(count)%d)**3
!!$              part(count)%T       = 300.0_WP
!!$              part(count)%dmdt    = 0.0_WP
!!$              part(count)%beta    = 0.0_WP
!!$              part(count)%tbreak  = 0.0_WP
!!$              part(count)%nparcel = 1.0_WP
!!$              part(count)%dt      = 0.0_WP
!!$              part(count)%i       = 0
!!$              part(count)%j       = 0
!!$              part(count)%k       = 0
!!$              part(count)%tag     = count
!!$              part(count)%stop    = 0
!!$              part(count)%x       = xm(i)
!!$              part(count)%y       = ym(j)
!!$              part(count)%z       = zm(k)
!!$              part(count)%u       = 0.1_WP
!!$              part(count)%v       = 0.0_WP
!!$              part(count)%w       = 0.0_WP
!!$           end do
!!$        end do
!!$     end do
!!$  case default ! Any unknown case
!!$     stop 'Unknown case'
!!$  end select
!!$  
!!$  ! Generate the initial droplet file
!!$  call parser_read('Init part file',part_init)
!!$  call BINARY_FILE_OPEN(iunit,trim(part_init),"w",ierr)
!!$  call BINARY_FILE_WRITE(iunit,npart,1,kind(npart),ierr)
!!$  call BINARY_FILE_WRITE(iunit,part_kind,1,kind(part_kind),ierr)
!!$  dt = 0.0_WP
!!$  call BINARY_FILE_WRITE(iunit,dt,1,kind(dt),ierr)
!!$  time = 0.0_WP
!!$  call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr)
!!$  do i=1,npart
!!$     call BINARY_FILE_WRITE(iunit,part(i),1,part_kind,ierr)
!!$  end do
!!$  call BINARY_FILE_CLOSE(iunit,ierr)
  
  return
end subroutine spray_data
  
