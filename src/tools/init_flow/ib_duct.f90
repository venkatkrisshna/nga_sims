module ib_duct
  use precision
  use param
  implicit none

  ! Length and diameter of the domain
  real(WP) :: Lx,Ly,Lz
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: RHO
  real(WP), dimension(:,:,:), pointer :: dRHO

  ! Initial velocity
  real(WP) :: Umean
  
end module ib_duct


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine ib_duct_grid
  use ib_duct
  use parser
  implicit none
  
  integer :: i,j,k
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  
  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  call parser_read('Lz',Lz)

  ! Set the periodicity
  call parser_read('Periodic in X',xper)
  yper=1
  zper=1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*Lx/real(nx,WP)
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*Ly/real(ny,WP) - 0.5_WP*Ly
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*Lz/real(nz,WP) - 0.5_WP*Lz
  end do
  
  ! Create the mid points 
  do i=1,nx
     xm(i)= 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)= 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine ib_duct_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine ib_duct_data
  use ib_duct
  use parser
  implicit none

  ! Allocate the array data
  nvar = 6
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Read in mean velocity
  call parser_read('Mean U Velocity',Umean,0.0_WP)

  ! Link the pointers
  U    => data(:,:,:,1); names(1) = 'U'
  V    => data(:,:,:,2); names(2) = 'V'
  W    => data(:,:,:,3); names(3) = 'W'
  P    => data(:,:,:,4); names(4) = 'P'
  RHO  => data(:,:,:,5); names(5) = 'RHO'
  dRHO => data(:,:,:,6); names(6) = 'dRHO'
  
  ! Create them
  U    = Umean
  V    = 0.0_WP
  W    = 0.0_WP
  P    = 0.0_WP
  RHO  = 0.0_WP
  dRHO = 0.0_WP
  
  return
end subroutine ib_duct_data


! ====================== !
! Create the IB variable !
! ====================== !
subroutine ib_duct_optdata
  use ib_duct
  use parser
  implicit none
  
  real(WP) :: width, depth
  real(WP) :: ysq1,ysq2,zsq1,zsq2
  integer :: j,k

  ! Read duct parameters
  call parser_read('Width',width)
  call Parser_read('Depth',depth,0.0_WP)
  
  ! Gib opt data
  nod = 1
  allocate(OD(nx,ny,nz,nod))
  allocate(OD_names(nod))
  OD_names(1) = 'Gib'
  
  ! Create geometry
  ysq1=-0.5_WP*width
  ysq2= 0.5_WP*width
  zsq1=-0.5_WP*depth
  zsq2= 0.5_WP*depth
  if (nz.eq.1) then
     ! 2D
     do j=1,ny
        if (ym(j).ge.0.0_WP) then
           ! Right of duct
           OD(:,j,:,1)=ysq2-ym(j)
        else if (ym(j).lt.0.0_WP) then
           ! Left of duct
           OD(:,j,:,1)=ym(j)-ysq1
        end if
     end do
  else
     ! 3D
     do k=1,nz
        do j=1,ny
           if (ym(j).gt.ysq2) then
              ! Right of duct
              if (zm(k).gt.zsq2) then
                 ! Back right corner
                 OD(:,j,k,1)=-sqrt((zm(k)-zsq2)**2+(ym(j)-ysq2)**2)
              else if (zm(k).lt.zsq1) then
                 ! Front right corner
                 OD(:,j,k,1)=-sqrt((zm(k)-zsq1)**2+(ym(j)-ysq2)**2)
              else
                 ! Center right
                 OD(:,j,k,1)=-ym(j)+ysq2
              end if
           else if (ym(j).lt.ysq1) then
              ! Left of duct
              if (zm(k).gt.zsq2) then
                 ! Back left corner
                 OD(:,j,k,1)=-sqrt((zm(k)-zsq2)**2+(ym(j)-ysq1)**2)
              else if (zm(k).lt.zsq1) then
                 ! Front left corner
                 OD(:,j,k,1)=-sqrt((zm(k)-zsq1)**2+(ym(j)-ysq1)**2)
              else
                 ! Center left
                 OD(:,j,k,1)=ym(j)-ysq1
              end if
           else
              ! Center of duct
              if (zm(k).gt.zsq2) then
                 ! Back center corner
                 OD(:,j,k,1)=-zm(k)+zsq2
              else if (zm(k).lt.zsq1) then
                 ! Bottom center corner
                 OD(:,j,k,1)=zm(k)-zsq1
              else
                 ! Center
                 OD(:,j,k,1)=min(zm(k)-zsq1,-zm(k)+zsq2,ym(j)-ysq1,-ym(j)+ysq2)
              end if
           end if
        end do
     end do
  end if

  return
end subroutine ib_duct_optdata


! ======================================== !
! Generate an initial droplet distribution !
! ======================================== !
subroutine ib_duct_part
  use ib_duct
  use math
  use lpt_mod
  use random
  use parser
  implicit none
  
  integer  :: iunit,ierr
  integer  :: np,i,ix,iy,iz
  integer  :: npartx,nparty,npartz
  real(WP) :: Hbed,width,depth
  real(WP) :: VFavg,Volp,Volbed,sumVolp
  real(WP) :: rand,Lp,Lpy,Lpz,mean_d
  real(WP) :: d_mean,d_min,d_max,d_sd,d_shift
  real(WP) :: dt,time
  real(WP) :: uin,vin,win,amp
  character(len=str_medium) :: part_init
  character(len=str_medium) :: distro
  real(WP), dimension(:), pointer :: dtmp
  logical :: use_lpt

  ! Check whether lpt is used
  call parser_is_defined('Init part file',use_lpt)
  if (.not.use_lpt) return
  
  ! Read average volume fraction
  call parser_read('Avg. volume fraction',VFavg,0.0_WP)

  ! Finite number of particles
  if (VFavg.gt.0.0_WP) then

     ! Initialize the random number generator
     call random_init

     ! Read duct parameters
     call parser_read('Bed height',Hbed,Lx)
     call parser_read('Width',width)
     call Parser_read('Depth',depth,Lz)
  
     ! Read particle diameter
     call parser_read('Particle mean diameter',d_mean)
     call parser_read('Particle standard deviation',d_sd,0.0_WP)
     call parser_read('Particle min diameter',d_min,0.0_WP)
     call parser_read('Particle max diameter',d_max,0.0_WP)
     call parser_read('Particle diameter shift',d_shift,0.0_WP)
     
     ! Velocity parameters
     call parser_read('Particle u velocity',uin,0.0_WP)
     call parser_read('Particle v velocity',vin,0.0_WP)
     call parser_read('Particle w velocity',win,0.0_WP)
     call parser_read('Particle fluctuations',amp,0.0_WP)
  
     ! Particle distribution
     call parser_read('Particle distribution',distro)
  
     ! First get bed volume
     Volbed = Hbed*width*depth

     ! Estimate number of particles
     Volp = Pi/6.0_WP*d_mean**3
     np = 5*int(VFavg*Volbed/Volp)
 
     ! Allocate temporary particle size array
     allocate(dtmp(np))

     ! Generate particles randomly until volume is sufficient
     np=0
     mean_d=0.0_WP
     sumVolp=0.0_WP
     do while (sumVolp.lt.VFavg*Volbed)
        
        ! Add a particle
        np=np+1
        
        ! Set particle size distribution (compact support Gaussian)
        !dtmp(np)=random_normal(m=d_mean,sd=d_sd)
        !if (d_sd.gt.0.0_WP) then
        !   do while (dtmp(np).gt.d_max.or.dtmp(np).lt.d_min)
        !      dtmp(np)=random_normal(m=d_mean,sd=d_sd)
        !   end do
        !else
        !   dtmp(np)d_mean
        !end if
        ! Set particle size distribution (compact support lognormal)
        dtmp(np)=random_lognormal(m=d_mean,sd=d_sd)+d_shift
        if (d_sd.gt.0.0_WP) then
           do while (dtmp(np).gt.d_max+epsilon(1.0_WP).or.dtmp(np).lt.d_min-epsilon(1.0_WP))
              dtmp(np)=random_lognormal(m=d_mean,sd=d_sd)+d_shift
           end do
        else
           dtmp(np)=d_mean
        end if
        
        ! Add up total particle volume
        if (trim(distro).eq.'uniform') then
           sumVolp=sumVolp+Pi/6.0_WP*d_mean**3.0_WP
        else
           sumVolp=sumVolp+Pi/6.0_WP*dtmp(np)**3.0_WP
        end if
        
        ! Compute mean particle diameter
        mean_d=mean_d+dtmp(np)
        
     end do
  
     ! Mean particle diameter
     mean_d = mean_d/np
     
     ! Mean particle volume
     Volp = sumVolp/np
     
     ! Mean interparticle distance
     select case (trim(distro))
     case ('random')
        Lp = (Volp/VFavg)**(1.0_WP/3.0_WP)
     case ('uniform')
        Lp = (Volp/VFavg)**(1.0_WP/3.0_WP)
        npartx = int(Hbed/Lp)
        Lp = Hbed/real(npartx,WP)
        nparty = int(width/Lp)
        Lpy = width/real(nparty,WP)
        npartz = int(depth/Lp)
        Lpz = depth/real(npartz,WP)
        np = npartx*nparty*npartz
     case default
        stop 'Unknown particle distribution'
     end select
     
     ! Allocate particle array
     allocate(part(1:np))
     
     ! Add particles
     do i=1,np
        
        ! Set various parameters for the particle
        part(i)%dt  =0.0_WP
        part(i)%Acol=0.0_WP
        part(i)%Tcol=0.0_WP
        part(i)%wx  =0.0_WP
        part(i)%wy  =0.0_WP
        part(i)%wz  =0.0_WP
        part(i)%id  =int(i,kind=8)
        part(i)%stop=0
        
        ! Set particle size
        if (trim(distro).eq.'uniform') then
           part(i)%d = d_mean
        else
           part(i)%d = dtmp(i)
        end if
        
        ! Give it a velocity
        if (nx.gt.1) then
           part(i)%u = uin
           if (amp.gt.0.0_WP) then
              call random_number(rand)
              rand=2.0_WP*rand-1.0_WP
              part(i)%u = part(i)%u + amp*rand
           end if
        else
           part(i)%u = 0.0_WP
        end if
        if (ny.gt.1) then
           part(i)%v = vin
           if (amp.gt.0.0_WP) then
              call random_number(rand)
              rand=2.0_WP*rand-1.0_WP
              part(i)%v = part(i)%v + amp*rand
           end if
        else
           part(i)%v = 0.0_WP
        end if
        if (nz.gt.1) then
           part(i)%w = win
           if (amp.gt.0.0_WP) then
              call random_number(rand)
              rand=2.0_WP*rand-1.0_WP
              part(i)%w = part(i)%w + amp*rand
           end if
        else
           part(i)%w = 0.0_WP
        end if
        
     end do
     
     ! Effective volume fraction
     VFavg = sumVolp/Volbed
     
     ! Output
     print*,'Number of particles: ',np
     print*,'Interparticle spacing: ',Lp
     print*,'Effective mean diameter: ',mean_d
     print*,'Effective volume fraction: ',VFavg
     
     ! Distribute particles
     select case (trim(distro))
     case ('random')
        ! Random distribution
        do i=1,np
           call random_number(rand)
           part(i)%x = Hbed*rand
           if (nz.eq.1) then
              part(i)%y=huge(1.0_WP)
              do while(abs(part(i)%y)+0.6_WP*part(i)%d.gt.0.5_WP*width)
                 call random_number(rand)
                 rand = rand-0.5_WP
                 part(i)%y = Lx*rand
                 part(i)%z = 0.0_WP
              end do
           else
              part(i)%y=huge(1.0_WP)
              part(i)%z=huge(1.0_WP)
              do while(abs(part(i)%y)+0.6_WP*part(i)%d.gt.0.5_WP*width)
                 call random_number(rand)
                 rand = rand-0.5_WP
                 part(i)%y = Ly*rand
              end do
              do while(abs(part(i)%z)+0.6_WP*part(i)%d.gt.0.5_WP*depth)
                 call random_number(rand)
                 rand = rand-0.5_WP
                 part(i)%z = Lz*rand
              end do
           end if
        end do
     case ('uniform')
        ! Uniform distribution
        do i=1,np
           ix = (i-1)/(nparty*npartz)
           iy = (i-1-nparty*npartz*ix)/npartz
           iz = i-1-nparty*npartz*ix-npartz*iy
           part(i)%x = x(1)+(ix+0.5_WP)*Lp
           part(i)%y = -0.5_WP*width+(iy+0.5_WP)*Lpy
           if (nz.eq.1) then
              part(i)%z = 0.0_WP
           else
              part(i)%z = -0.5_WP*depth+(iz+0.5_WP)*Lpz
           end if
        end do
     end select

  else
     ! Begin with no particles
     np=0
  end if

  ! Generate the initial droplet file
  call parser_read('Init part file',part_init)
  call BINARY_FILE_OPEN(iunit,trim(part_init),"w",ierr)
  call BINARY_FILE_WRITE(iunit,np,1,kind(np),ierr)
  call BINARY_FILE_WRITE(iunit,part_kind,1,kind(part_kind),ierr)
  dt = 0.0_WP
  call BINARY_FILE_WRITE(iunit,dt,1,kind(dt),ierr)
  time = 0.0_WP
  call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr)
  do i=1,np
     call BINARY_FILE_WRITE(iunit,part(i),1,part_kind,ierr)
  end do
  call BINARY_FILE_CLOSE(iunit,ierr)
 
  return
end subroutine ib_duct_part
