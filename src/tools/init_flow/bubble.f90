module bubble
  use string
  use precision
  use param
  implicit none
  
  ! Domain
  real(WP) :: Lx,Ly,Lz
  logical  :: two_dim
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:),   pointer :: U
  real(WP), dimension(:,:,:),   pointer :: V
  real(WP), dimension(:,:,:),   pointer :: W
  real(WP), dimension(:,:,:,:), pointer :: VOF
  
end module bubble

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine bubble_grid
  use bubble
  use parser
  implicit none
  
  integer :: i,j,k
  
  ! Read in the size of the domain
  call parser_read('nx',nx) 
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  call parser_read('Lz',Lz)
  call parser_read('xper',xper)
  call parser_read('yper',yper)
  call parser_read('zper',zper)
  if (nz.eq.1) then
     Lz=Lx/real(nx,WP)
     two_dim=.true.
  else
     two_dim=.false.
  end if
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*Lx/real(nx,WP)-0.5_WP*Lx
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*Ly/real(ny,WP)
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*Lz/real(nz,WP)-0.5_WP*Lz
  end do
  
  ! Create the cell centered grid
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Masks
  mask=0
  
  return
end subroutine bubble_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine bubble_data
  use bubble
  use parser
  use math
  implicit none
  real(WP) :: R1,R2,pheight
  real(WP), dimension(3) :: bc1,bc2
  integer :: s,i,j,k
  integer, parameter :: nF=20
  integer :: ii,jj,kk
  real(WP) :: myx,myy,myz
  real(WP) :: dx
  
  ! Allocate the array data
  nvar = 11
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U      => data(:,:,:,1);  names(1)  = 'U'
  V      => data(:,:,:,2);  names(2)  = 'V'
  W      => data(:,:,:,3);  names(3)  = 'W'
  VOF  => data(:,:,:, 4:11);
  names( 4) = 'VOF1'
  names( 5) = 'VOF2'
  names( 6) = 'VOF3'
  names( 7) = 'VOF4'
  names( 8) = 'VOF5'
  names( 9) = 'VOF6'
  names(10) = 'VOF7'
  names(11) = 'VOF8'

  ! Initialize
  U   = 0.0_WP
  V   = 0.0_WP
  W   = 0.0_WP
  VOF = 0.0_WP

  ! Set spacing (assumed uniform)
  dx = x(2) - x(1)
  
  ! Set the initial distance field
  call parser_read('Bubble 1 radius',R1,0.0_WP)
  if (R1.gt.0.0_WP) call parser_read('Bubble 1 center',bc1)
  call parser_read('Bubble 2 radius',R2,0.0_WP)
  if (R2.gt.0.0_WP) call parser_read('Bubble 2 center',bc2)
  call parser_read('Pool height',pheight,0.0_WP)
  
  do k=1,nz
     do j=1,ny
        do i=1,nx

           ! Above pool
           if (ym(j) .ge. pheight) then
              VOF(i,j,k,:)=0.0_WP
              cycle 
           end if

           ! Check if close to the interface
           if (min(abs(d1(xm(i),ym(j),zm(k))),abs(d2(xm(i),ym(j),zm(k)))) &
                .le.2.0_WP*max(x(i+1)-x(i),y(j+1)-y(j),z(k+1)-z(k))) then
              ! Loop over subcells
              do s=1,8
                 ! Compute VOF on finer mesh
                 VOF(i,j,k,s)=0.0_WP
                 do kk=1,nF
                    do jj=1,nF
                       do ii=1,nF
                          ! x,y,z position
                          myx=xs1(s,i)+(real(ii,WP)-0.5_WP)/real(nf,WP)*(xs2(s,i)-xs1(s,i))
                          myy=ys1(s,j)+(real(jj,WP)-0.5_WP)/real(nf,WP)*(ys2(s,j)-ys1(s,j))
                          myz=zs1(s,k)+(real(kk,WP)-0.5_WP)/real(nf,WP)*(zs2(s,k)-zs1(s,k))
                          ! In bubble?
                          if (max(d1(myx,myy,myz),d2(myx,myy,myz)).lt.0.0_WP) then
                             VOF(i,j,k,s)=VOF(i,j,k,s)+1.0_WP ! outside bubble
                          end if
                       end do
                    end do
                 end do
                 VOF(i,j,k,s)=VOF(i,j,k,s)/real(nF,WP)**3
              end do
           else ! away from interface
              if (max(d1(x(i),y(j),z(k)),d2(x(i),y(j),z(k))).lt.0.0_WP) then
                 VOF(i,j,k,:)=1.0_WP ! outside bubble
              else
                 VOF(i,j,k,:)=0.0_WP ! inside bubble
              end if
           end if
        end do
     end do
  end do

  return

contains
  ! Distance to 1st bubble interface 
  function d1(x,y,z)
    implicit none
    real(WP), intent(in) :: x,y,z
    real(WP) :: d1
    d1 = R1 - sqrt((x-bc1(1))**2+(y-bc1(2))**2+(z-bc1(3))**2)
  end function d1
  ! Distance to 2nd bubble interface 
  function d2(x,y,z)
    implicit none
    real(WP), intent(in) :: x,y,z
    real(WP) :: d2
    d2 = R2 - sqrt((x-bc2(1))**2+(y-bc2(2))**2+(z-bc2(3))**2)
  end function d2
  
end subroutine bubble_data
  
