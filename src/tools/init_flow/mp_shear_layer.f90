module mp_shear_layer
  use string
  use precision
  use param
  implicit none
  
  ! Domain
  real(WP) :: Lx,Ly,Lz
  real(WP) :: dx,dy,dz
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: G
  
end module mp_shear_layer

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine mp_shear_layer_grid
  use mp_shear_layer
  use parser
  use math
  implicit none
  
  integer :: i,j,k
  logical :: wall_bottom,wall_top
  real(WP) :: l_lip,h_lip
  
  ! Cartesian
  icyl=0
  
  ! Periodicity in z only
  xper=0;yper=0;zper=1
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('Lx',Lx)
  dx=Lx/real(nx,WP)
  call parser_read('ny',ny)
  call parser_read('Ly',Ly)
  dy=Ly/real(ny,WP)
  call parser_read('nz',nz)
  call parser_read('Lz',Lz)
  if (nz.eq.1) then
     dz=dx
  else
     dz=Lz/real(nz,WP)
  end if
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*dx
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*dy-0.5_WP*Ly
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*dz-0.5_WP*Lz
  end do
  
  ! Create the cell centered grid
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  call parser_read('Bottom wall',wall_bottom,.false.)
  if (wall_bottom) mask(:, 1)=1
  call parser_read('Top wall'   ,wall_top   ,.false.)
  if (wall_top   ) mask(:,ny)=1
  call parser_read('Lip length'   ,l_lip)
  call parser_read('Lip thickness',h_lip)
  do j=1,ny
     do i=1,nx
        if (xm(i).le.l_lip.and.abs(ym(j)).le.0.5_WP*h_lip) mask(i,j)=1
     end do
  end do
  
  return
end subroutine mp_shear_layer_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine mp_shear_layer_data
  use mp_shear_layer
  use parser
  use string
  implicit none
  
  integer  :: i,j,k
  
  ! Allocate the array data
  nvar = 4
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U => data(:,:,:,1); names(1) = 'U'
  V => data(:,:,:,2); names(2) = 'V'
  W => data(:,:,:,3); names(3) = 'W'
  G => data(:,:,:,4); names(4) = 'G'
  
  ! Zero velocity
  U = 0.0_WP
  V = 0.0_WP
  W = 0.0_WP
  
  ! Level set
  G=0.0_WP
  do k=1,nz
     do j=1,ny
        do i=1,nx
           G(i,j,k) = -ym(j)
        end do
     end do
  end do
  
  return
end subroutine mp_shear_layer_data
