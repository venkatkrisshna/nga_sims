module partfilter
  use precision
  use param
  implicit none

  ! Length and diameter of the domain
  real(WP) :: Lx,Ly,Lz
  
  ! Filter region
  integer :: fi1,fi2,fj1,fj2,fk1,fk2
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: RHO
  real(WP), dimension(:,:,:), pointer :: dRHO
  
  ! Pointer to variable in optdata
  real(WP), dimension(:,:,:), pointer :: iperm
  real(WP), dimension(:,:,:), pointer :: poros

end module partfilter


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine partfilter_grid
  use partfilter
  use parser
  implicit none
  
  integer :: i,j,k
  logical :: use_walls
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  
  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  call parser_read('Lz',Lz)
  
  call parser_read('Use walls',use_walls)
  
  ! Set the periodicity
  xper=0
  if (use_walls) then
     yper=0
  else
     yper=1
  end if
  zper=1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create mesh
  do i=1,nx+1
     x(i) = real(i-1,WP)*Lx/real(nx,WP)
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*Ly/real(ny,WP)-0.5_WP*Ly
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*Lz/real(nz,WP)-0.5_WP*Lz
  end do
  
  ! Create the mid points 
  do i=1,nx
     xm(i)= 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)= 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask=0
  if (use_walls) then
     mask(:, 1)=1
     mask(:,ny)=1
  end if
  
  ! Define filter region in terms of indices
  fi1=int(nx*0.4_WP); fi2=int(nx*0.6_WP)
  fj1=1; fj2=ny
  fk1=1; fk2=nz
  
  return
end subroutine partfilter_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine partfilter_data
  use partfilter
  use parser
  implicit none
  
  real(WP) :: permeability,porosity
  
  ! Allocate the array data
  nvar = 6
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))

  ! Link the pointers
  U    => data(:,:,:,1); names(1) = 'U'
  V    => data(:,:,:,2); names(2) = 'V'
  W    => data(:,:,:,3); names(3) = 'W'
  P    => data(:,:,:,4); names(4) = 'P'
  RHO  => data(:,:,:,5); names(5) = 'RHO'
  dRHO => data(:,:,:,6); names(6) = 'dRHO'
  
  ! Create them
  U    = 0.0_WP
  V    = 0.0_WP
  W    = 0.0_WP
  P    = 0.0_WP
  RHO  = 0.0_WP
  dRHO = 0.0_WP
  
  ! Optional data file for porous coefficient
  nod = 2
  allocate(OD(nx,ny,nz,nod))
  allocate(OD_names(nod))
  
  ! Link the pointer
  iperm  => OD(:,:,:,1); OD_names(1) = 'IPERM'
  poros  => OD(:,:,:,2); OD_names(2) = 'POROS'
  
  ! Create porosity field
  call parser_read('Permeability',permeability)
  call parser_read('Porosity',porosity)
  iperm=0.0_WP; poros=1.0_WP
  if (permeability.gt.0.0_WP) then
     iperm(fi1:fi2,fj1:fj2,fk1:fk2)=1.0_WP/permeability
     poros(fi1:fi2,fj1:fj2,fk1:fk2)=porosity
  end if
  
  return
end subroutine partfilter_data


! ========================================= !
! Generate an initial particle distribution !
! ========================================= !
subroutine partfilter_part
  use partfilter
  use parser
  use lpt_mod
  use random
  use math
  implicit none
  
  character(len=str_medium) :: part_init
  logical :: use_lpt
  integer :: iunit,ierr
  integer :: i,np
  real(WP) :: VFavg
  real(WP) :: dt,time,rand
  real(WP) :: fx1,fx2,fy1,fy2,fz1,fz2
  real(WP) :: fvol,pvol,fpd
  
  ! Check whether lpt is used
  call parser_is_defined('Init part file',use_lpt)
  if (.not.use_lpt) return
  
  ! Read average volume fraction and bed height
  call parser_read('Avg. volume fraction',VFavg)
  call parser_read('Filter part diameter',fpd)
  
  ! Initialize the random number generator
  call random_init
  
  ! Compute volume of filter region
  fx1=x(fi1); fx2=x(fi2+1)
  fy1=y(fj1); fy2=y(fj2+1)
  fz1=z(fk1); fz2=z(fk2+1)
  fvol=(fx2-fx1)*(fy2-fy1)*(fz2-fz1)
  
  ! Estimate number of particles
  pvol = Pi/6.0_WP*fpd**3
  np = int(VFavg*fvol/pvol)
  
  ! Allocate particle size array
  allocate(part(np))
  
  ! Set particle parameters
  do i=1,np
     
     ! Set various parameters for the particle
     part(i)%dt  =0.0_WP
     part(i)%d   =fpd
     part(i)%u   =0.0_WP
     part(i)%v   =0.0_WP
     part(i)%w   =0.0_WP
     part(i)%Acol=0.0_WP
     part(i)%Tcol=0.0_WP
     part(i)%wx  =0.0_WP
     part(i)%wy  =0.0_WP
     part(i)%wz  =0.0_WP
     part(i)%id  =int(0,kind=8) ! Inactive particle
     part(i)%stop=0
     
     ! Distribute particles
     call random_number(rand)
     part(i)%x = (fx2-fx1)*rand+fx1
     call random_number(rand)
     part(i)%y = (fy2-fy1)*rand+fy1
     call random_number(rand)
     part(i)%z = (fz2-fz1)*rand+fz1
     
  end do
  
  ! Output
  print*,'Number of particles: ',np
  print*,'Effective mean diameter: ',fpd
  print*,'Effective volume fraction: ',VFavg
  
  ! Generate the initial particle file
  call parser_read('Init part file',part_init)
  call BINARY_FILE_OPEN(iunit,trim(part_init),"w",ierr)
  call BINARY_FILE_WRITE(iunit,np,1,kind(np),ierr)
  call BINARY_FILE_WRITE(iunit,part_kind,1,kind(part_kind),ierr)
  dt = 0.0_WP
  call BINARY_FILE_WRITE(iunit,dt,1,kind(dt),ierr)
  time = 0.0_WP
  call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr)
  do i=1,np
     call BINARY_FILE_WRITE(iunit,part(i),1,part_kind,ierr)
  end do
  call BINARY_FILE_CLOSE(iunit,ierr)
  
  return
end subroutine partfilter_part
