module ib_stl
  use precision
  use param
  implicit none

  ! Domain dimensions
  real(WP) :: dx,dy,dz
  real(WP) :: Lx,Ly,Lz,eps
  real(WP) :: x0,y0,z0

  ! Pointers to variable in data
  real(WP), dimension(:,:,:),   pointer :: U
  real(WP), dimension(:,:,:),   pointer :: V
  real(WP), dimension(:,:,:),   pointer :: W
  real(WP), dimension(:,:,:),   pointer :: P
  real(WP), dimension(:,:,:,:), pointer :: VOF

  ! Type of data file
  character(str_medium) :: data_type,inflow_type

end module ib_stl


! =============== !
! Create the mesh !
! =============== !
subroutine ib_stl_grid
  use ib_stl
  use parser
  implicit none
  integer :: i,j,k

  ! Read in data type
  call parser_read('Data type',data_type)

  ! Flow solver mesh =============
  ! Periodicity
  call parser_read('xper',xper)
  call parser_read('yper',yper)
  zper=1

  ! Cartesian
  icyl=0

  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)

  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  call parser_read('Lz',Lz)

  dx = Lx/real(nx,WP)
  dy = Ly/real(ny,WP)
  dz = Lz/real(nz,WP)

  call parser_read('x0',x0,0.5_WP*Lx)
  call parser_read('y0',y0,0.5_WP*Ly)
  call parser_read('z0',z0,0.5_WP*Lz)

  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))

  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*dx-x0
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*dy-y0
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*dz-z0
  end do

  ! Create the cell centered grid
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do

  ! Create the masks
  mask = 0

  return
end subroutine ib_stl_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine ib_stl_data
  use ib_stl
  use parser
  implicit none

  integer :: i,j,k
  real(WP) :: Lfront,Rfront

  ! Initialize based on data type
  select case (trim(adjustl(data_type)))
  case ('cold')
     ! Allocate the array data
     nvar = 4
     allocate(data(imin_:imax_,jmin_:jmax_,kmin_:kmax_,nvar))
     allocate(names(nvar))
     ! Link the pointers
     U => data(:,:,:,1); names(1) = 'U'
     V => data(:,:,:,2); names(2) = 'V'
     W => data(:,:,:,3); names(3) = 'W'
     P => data(:,:,:,4); names(4) = 'P'
     ! Create them
     U = 0.0_WP
     V = 0.0_WP
     W = 0.0_WP
     P = 0.0_WP
  case ('multiphase')
     ! Allocate the array data
     nvar = 4
     allocate(data(imin_:imax_,jmin_:jmax_,kmin_:kmax_,nvar))
     allocate(names(nvar))
     ! Link the pointers
     U => data(:,:,:,1); names(1) = 'U'
     V => data(:,:,:,2); names(2) = 'V'
     W => data(:,:,:,3); names(3) = 'W'
     VOF  => data(:,:,:, 4:11);
     names( 4) = 'VOF1'
     names( 5) = 'VOF2'
     names( 6) = 'VOF3'
     names( 7) = 'VOF4'
     names( 8) = 'VOF5'
     names( 9) = 'VOF6'
     names(10) = 'VOF7'
     names(11) = 'VOF8'

     ! Create them
     U = 0.0_WP
     V = 0.0_WP
     W = 0.0_WP
     VOF = -1.0_WP
     ! Set the initial distance field
     call parser_read('Front location',Lfront)
     call parser_read('Front radius',Rfront)
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (xm(i).le.Lfront) then
                 if (sqrt(ym(j)**2+zm(k)**2).le.Rfront) then
                    VOF(i-imin_+1,j-jmin_+1,k-kmin_+1,:)=+1.0_WP
                 end if
              end if
           end do
        end do
     end do
  case default
     stop "Unknown data type in ib_stl_data"
  end select

  return
end subroutine ib_stl_data

! =================== !
! Generate the inflow !
! =================== !
subroutine ib_stl_inflow
  use ib_stl
  use parser
  implicit none

  real(WP) :: Din,Uin,Dco,Hco,Uco
  real(WP) :: Ytb,Utop,Ubot
  real(WP) :: d
  integer :: j,k
  logical :: isdef

  ! Test if required
  call parser_is_defined('Init inflow file',isdef)
  if (.not.isdef) return

  ! Generate inflow based on data type
  select case (trim(adjustl(data_type)))
  case ('cold')
     nvar_inflow = 3
     ntime = 2
  case ('multiphase')
     ! Generate an inflow file 
     nvar_inflow = 11
     ntime = 2
  case default
     stop "Unknown data type in ib_stl_data"
  end select

  ! Allocate some arrays
  allocate(inflow(ntime,jmin_:jmax_,kmin_:kmax_,nvar_inflow))
  allocate(names_inflow(nvar_inflow))

  ! Link the pointers
  U => inflow(:,:,:,1); names_inflow(1) = 'U'
  V => inflow(:,:,:,2); names_inflow(2) = 'V'
  W => inflow(:,:,:,3); names_inflow(3) = 'W'
  if (trim(adjustl(data_type)).eq.'multiphase') then
     VOF  => inflow(:,:,:, 4:11);
     names_inflow( 4) = 'VOF1'
     names_inflow( 5) = 'VOF2'
     names_inflow( 6) = 'VOF3'
     names_inflow( 7) = 'VOF4'
     names_inflow( 8) = 'VOF5'
     names_inflow( 9) = 'VOF6'
     names_inflow(10) = 'VOF7'
     names_inflow(11) = 'VOF8'
  end if

  ! Compute time grid
  time_inflow = 0.0_WP
  dt_inflow   = 1.0_WP

  ! Create inflow based on inflow type
  call parser_read('Inflow type',inflow_type)
  select case (trim(adjustl(inflow_type)))
  case ('jet')
     ! Profiles parameters
     call parser_read('D inflow',Din)
     call parser_read('U inflow',Uin)

     ! Prepare the velocity profiles
     V = 0.0_WP
     W = 0.0_WP
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           ! Distance from centerline
           d = sqrt(ym(j)**2+zm(k)**2)
           ! Define the velocity
           if (d.le.0.5_WP*Din) then
              U  (:,j-jmin_+1,k-kmin_+1) = Uin
           else
              U  (:,j-jmin_+1,k-kmin_+1) = 0.0_WP
           end if
        end do
     end do

     ! Prepare VOF
     if (trim(adjustl(data_type)).eq.'multiphase') then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              ! Distance from centerline
              d = sqrt(ym(j)**2+zm(k)**2)
              ! Define the velocity
              if (d.le.0.5_WP*Din) then
                 VOF(:,j-jmin_+1,k-kmin_+1,:) = 1.0_WP
              else
                 VOF(:,j-jmin_+1,k-kmin_+1,:) = 0.0_WP
              end if
           end do
        end do
     end if

  case ('jet with coflow')
     ! Profiles parameters
     call parser_read('D inflow',Din)
     call parser_read('U inflow',Uin)
     call parser_read('D coflow',Dco)
     call parser_read('H coflow',Hco)
     call parser_read('U coflow',Uco)

     ! Prepare the velocity profiles
     V = 0.0_WP
     W = 0.0_WP
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           ! Distance from centerline
           d = sqrt(ym(j)**2+zm(k)**2)
           ! Define the velocity
           if (d.le.0.5_WP*Din) then
              U  (:,j-jmin_+1,k-kmin_+1) = Uin
           else if (d.gt.0.5_WP*Dco .and. d.le.0.5_WP*Dco+Hco) then
              U  (:,j-jmin_+1,k-kmin_+1) = Uco
           else
              U  (:,j-jmin_+1,k-kmin_+1) = 0.0_WP
           end if
        end do
     end do

     ! Prepare VOF
     if (trim(adjustl(data_type)).eq.'multiphase') then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              ! Distance from centerline
              d = sqrt(ym(j)**2+zm(k)**2)
              ! Define the velocity
              if (d.le.0.5_WP*Din) then
                 VOF(:,j-jmin_+1,k-kmin_+1,:) = 1.0_WP
              else
                 VOF(:,j-jmin_+1,k-kmin_+1,:) = 0.0_WP
              end if
           end do
        end do
     end if

  case ('top/bottom')
     if (trim(adjustl(data_type)).eq.'multiphase') &
          stop "top/bottom inflow type not programmed for multiphase data type"
     ! Profiles parameters
     call parser_read('Y top/bottom',Ytb)
     call parser_read('U top',Utop)
     call parser_read('U bottom',Ubot)
     do j=jmin_,jmax_
        if (ym(j).gt.Ytb) then
           U(:,j,:)=Utop
        else
           U(:,j,:)=Ubot
        end if
     end do

  case default
     stop "Unknown inflow type in ib_stl_inflow"
  end select

  return
end subroutine ib_stl_inflow
