module sandiaflame
  use precision
  use param
  implicit none
  
  ! Length and radius of the domain
  real(WP) :: Lp,Lz
  
  ! Configuration of the Sandia TNF flames
  real(WP), parameter :: Rjet1=0.00000_WP !< Jet inner radius (main jet)
  real(WP), parameter :: Rjet2=0.00360_WP !< Jet outer radius (main jet)
  real(WP), parameter :: Rcof1=0.00385_WP !< Coflow inner radius (pilot)
  real(WP), parameter :: Rcof2=0.00910_WP !< Coflow outer radius (pilot)
  real(WP), parameter :: Rout1=0.00945_WP !< Outside flow inner radius (wind tunnel coflow)
  real(WP), parameter :: Rout2=1.00000_WP !< Outside flow outer radius (wind tunnel coflow)
  ! Note wind tunnel is 30cmx30cm, so Rout2 isn't right - using arbitrary large value
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: RHO
  real(WP), dimension(:,:,:), pointer :: dRHO
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: ZMIX
  
end module sandiaflame


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine sandiaflame_grid
  use sandiaflame
  use parser
  use math
  implicit none

  integer :: i,j,k,ny_uni,nx_uni
  real(WP) :: dx,dy,Luni,Runi,Sx,Sy
  
  ! Read in the grid size
  call parser_read('dy',dy)
  call parser_read('dx',dx)
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  
  ! Read in dense region
  call parser_read('Dense length',Luni)
  call parser_read('Dense radius',Runi)
  
  ! Read in stretching ratio
  call parser_read('Stretching x',Sx)
  call parser_read('Stretching y',Sy)
  
  ! Read in pipe length
  call parser_read('Pipe length',Lp)
  
  ! Set the periodicity
  xper = 0
  yper = 0
  zper = 1
  
  ! Cylindrical
  icyl = 1
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  nx_uni=int(Luni/dx)
  if (nx_uni.ge.nx) stop 'Erroneous mesh parameters in x'
  ny_uni=int(Runi/dy)
  if (ny_uni.ge.ny) stop 'Erroneous mesh parameters in y'
  do i=1,nx_uni+1
     x(i) = real(i-1,WP)*dx-Lp
  end do
  do i=nx_uni+2,nx+1
     x(i) = (1.0_WP+Sx)*x(i-1)-Sx*x(i-2)
  end do
  print*,'Domain extent in x:',x(1),x(nx+1)
  do j=1,ny_uni+1
     y(j) = real(j-1,WP)*dy
  end do
  do j=ny_uni+2,ny+1
     y(j) = (1.0_WP+Sy)*y(j-1)-Sy*y(j-2)
  end do
  print*,'Domain extent in y:',y(1),y(ny+1)
  if (nz.eq.1) then
     Lz=min(dx,dy)
  else
     Lz=twoPi
  end if
  do k=1,nz+1
     z(k) = real(k-1,WP)*Lz/real(nz,WP)
  end do
  
  ! Create the mid points
  do i=1,nx
     xm(i)= 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)= 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask=0
  do i=1,nx
     do j=1,ny
        ! Pipe region
        if (xm(i).lt.0.0_WP) then
           ! Outer jet wall
           if (ym(j).gt.Rjet2.and.ym(j).lt.Rcof1) mask(i,j)=1
           ! Outer coflow wall
           if (ym(j).gt.Rcof2.and.ym(j).lt.Rout1) mask(i,j)=1
        end if
     end do
  end do
  
  return
end subroutine sandiaflame_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine sandiaflame_data
  use sandiaflame
  use parser
  implicit none
  
  real(WP):: rho0
  
  ! Allocate the array data
  nvar = 7
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  RHO  => data(:,:,:,1); names(1) = 'RHO'
  dRHO => data(:,:,:,2); names(2) = 'dRHO'
  U    => data(:,:,:,3); names(3) = 'U'
  V    => data(:,:,:,4); names(4) = 'V'
  W    => data(:,:,:,5); names(5) = 'W'
  P    => data(:,:,:,6); names(6) = 'P'
  ZMIX => data(:,:,:,7); names(7) = 'ZMIX'
  
  ! Create initial field
  call parser_read('Initial density',rho0,1.0_WP)
  RHO  = rho0
  dRHO = 0.0_WP
  U    = 0.0_WP
  V    = 0.0_WP
  W    = 0.0_WP
  P    = 0.0_WP
  ZMIX = 0.0_WP
  
  return
end subroutine sandiaflame_data

