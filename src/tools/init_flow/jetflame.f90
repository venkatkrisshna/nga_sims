module jetflame
  use precision
  use param
  implicit none
  
  ! Length and diameter of the domain
  real(WP) :: L,diam
  
  ! Length and diameter of the pipe
  real(WP) :: pipe_L,pipe_diam1,pipe_diam2,pipe_diam3,pipe_diam4
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: RHO
  real(WP), dimension(:,:,:), pointer :: dRHO
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: ZMIX
  
end module jetflame


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine jetflame_grid
  use jetflame
  use parser
  use math
  implicit none

  integer :: i,j,k
  real(WP) :: Lz
  real(WP) :: alpha_x,alpha_y
  real(WP) :: beta_x,beta_y
  real(WP) :: gamma_x,gamma_y
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  
  call parser_read('Full length',L)
  call parser_read('Full diameter',diam)
  
  call parser_read('Pipe length',pipe_L)
  call parser_read('Pipe inner diameter',pipe_diam1)
  call parser_read('Pipe outer diameter',pipe_diam2)
  call parser_read('Pilot outer diameter',pipe_diam3)
  call parser_read('Pilot thickness',pipe_diam4)
  
  call parser_read('Max stretching ratio x',beta_x)
  call parser_read('Max stretching ratio y',beta_y)
  
  ! Set the periodicity
  xper = 0
  yper = 0
  zper = 1
  
  ! Cylindrical
  icyl = 1
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  alpha_x = dlog(beta_x)/(1.0_WP-1.0_WP/real(nx,WP))
  gamma_x = dexp(alpha_x/real(nx,WP)) !streching ratio (not used)
  if (gamma_x.eq.1.0_WP) then
     do i=1,nx+1
        x(i) = real(i-1,WP)*L/real(nx,WP)
     end do
  else
     do i=1,nx+1
        x(i) = L*(dexp(alpha_x*real((i-1),WP)/real(nx,WP))-1.0_WP)/(dexp(alpha_x)-1.0_WP)
     end do
  end if
  
  alpha_y = dlog(beta_y)/(1.0_WP-1.0_WP/real(ny,WP))
  gamma_y = dexp(alpha_y/real(ny,WP)) !streching ratio (not used)
  if (gamma_y.eq.1) then
     do j=1,ny+1
        y(j) = real(j-1,WP)*diam/real(2*ny,WP)
     end do
  else
     y(1)=0.0_WP
     do j=2,ny+1
        y(j) = 0.5_WP*diam*(dexp(alpha_y*real((j-1),WP)/real(ny,WP))-1.0_WP)/(dexp(alpha_y)-1.0_WP)
     end do
  end if
  
  if (nz.eq.1) then
     Lz=twoPi/real(max(10*nx,10*ny),WP)
  else
     Lz=twoPi
  end if
  do k=1,nz+1
     z(k) = real(k-1,WP)*Lz/real(nz,WP)
  end do
  
  ! Create the mid points
  do i=1,nx
     xm(i)= 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)= 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  do i=1,nx
     do j=1,ny
        if ( (2.0_WP*ym(j).ge.pipe_diam1) .and. &
             (2.0_WP*ym(j).le.pipe_diam2) .and. &
             (xm(i).le.pipe_L) ) then
           mask(i,j) = 1
        else if ( (2.0_WP*ym(j).ge.pipe_diam3) .and. &
                  (2.0_WP*ym(j).le.pipe_diam4) .and. &
                  (xm(i).le.pipe_L) ) then
           mask(i,j) = 1
        end if
     end do
  end do
  
  return
end subroutine jetflame_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine jetflame_data
  use jetflame
  use parser
  implicit none
  
  real(WP):: rho0
  
  ! Allocate the array data
  nvar = 7
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  RHO  => data(:,:,:,1); names(1) = 'RHO'
  dRHO => data(:,:,:,2); names(2) = 'dRHO'
  U    => data(:,:,:,3); names(3) = 'U'
  V    => data(:,:,:,4); names(4) = 'V'
  W    => data(:,:,:,5); names(5) = 'W'
  P    => data(:,:,:,6); names(6) = 'P'
  ZMIX => data(:,:,:,7); names(7) = 'ZMIX'
  
  ! Create initial field
  call parser_read('Initial density',rho0,1.0_WP)
  RHO  = rho0
  dRHO = 0.0_WP
  U    = 0.0_WP
  V    = 0.0_WP
  W    = 0.0_WP
  P    = 0.0_WP
  ZMIX = 0.0_WP
  
  return
end subroutine jetflame_data

