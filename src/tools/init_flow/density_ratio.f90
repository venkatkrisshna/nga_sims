module density_ratio
  use string
  use precision
  use param
  implicit none
  
  ! Domain size
  real(WP) :: L,dx
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: VOF
  real(WP), dimension(:,:,:), pointer :: normx
  real(WP), dimension(:,:,:), pointer :: normy
  real(WP), dimension(:,:,:), pointer :: normz

end module density_ratio

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine density_ratio_grid
  use density_ratio
  use parser
  use math
  use string
  implicit none
  
  integer :: i,j,k
  character(len=str_medium) :: direct
  
  ! Read in the size of the domain
  L=1.0_WP
  call parser_read('nx',nx)
  call parser_read('Direction',direct,'xy')
  select case (trim(adjustl(direct)))
  case ('xy','yx','XY','YX')
     nx=nx; ny=nx; nz=1; dx=L/real(nx,WP)
  case ('yz','zy','YZ','ZY')
     ny=nx; nz=nx; nx=1; dx=L/real(ny,WP)
  case ('zx','xz','ZX','XZ')
     nx=nx; nz=nx; ny=1; dx=L/real(nz,WP)
  end select
  xper=1; yper=1; zper=1
  
  ! Cartesian
  icyl=0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*dx-0.5_WP*(real(nx,WP)*dx)
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*dx-0.5_WP*(real(ny,WP)*dx)
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*dx-0.5_WP*(real(nz,WP)*dx)
  end do
  
  ! Create the cell centered grid
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine density_ratio_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine density_ratio_data
  use density_ratio
  use parser
  use math
  implicit none
  
  integer :: i,j,k,ii,jj,kk,nF
  character(len=str_medium) :: interfaceshape
  real(WP) :: G,x_here,y_here,z_here,norm,tmp_VOF

  ! Allocate the array data
  nvar = 7
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U   => data(:,:,:,1); names(1) = 'U'
  V   => data(:,:,:,2); names(2) = 'V'
  W   => data(:,:,:,3); names(3) = 'W'
  VOF => data(:,:,:,4); names(4) = 'VOF'
  normx => data(:,:,:,5); names(5) = 'normx'; normx=0.0_WP
  normy => data(:,:,:,6); names(6) = 'normy'; normy=0.0_WP
  normz => data(:,:,:,7); names(7) = 'normz'; normz=0.0_WP
  
  ! Initialize
  U = 0.0_WP
  V = 0.0_WP
  W = 0.0_WP
  
  ! Calculate VOF from level set on fine mesh within each cell
  call parser_read('Interface shape',interfaceshape)
  nF=20
  select case (trim(interfaceshape))
  case ('drop')
     do k=1,nz
        do j=1,ny
           do i=1,nx
              ! Make VOF on fine mesh 
              tmp_VOF=0.0_WP
              do kk=1,nF
                 do jj=1,nF
                    do ii=1,nF
                       x_here=x(i)+real(ii,WP)/real(nf+1,WP)*(x(i+1)-x(i))
                       y_here=y(j)+real(jj,WP)/real(nf+1,WP)*(y(j+1)-y(j))
                       z_here=z(k)+real(kk,WP)/real(nf+1,WP)*(z(k+1)-z(k))
                       G=0.1_WP*L-sqrt(x_here**2+y_here**2+z_here**2)
                       if (G.gt.0.0_WP) then
                          tmp_VOF=tmp_VOF+1.0_WP
                       end if
                    end do
                 end do
              end do
              ! Calculate VOF from fine mesh
              VOF(i,j,k)=tmp_VOF/real(nF,WP)**3
              ! Normal
              norm=sqrt(x(i)**2+y(j)**2+z(k)**2)+epsilon(1.0_WP)
              normx(i,j,k)=x(i)/norm
              normy(i,j,k)=y(j)/norm
              normz(i,j,k)=z(k)/norm
           end do
        end do
     end do
  case ('bubble')
     do k=1,nz
        do j=1,ny
           do i=1,nx
              ! Make VOF on fine mesh
              tmp_VOF=0.0_WP
              do jj=1,nF
                 do ii=1,nF
                    x_here=x(i)+real(ii,WP)/real(nf+1,WP)*(x(i+1)-x(i))
                    y_here=y(j)+real(jj,WP)/real(nf+1,WP)*(y(j+1)-y(j))
                    G=sqrt(x_here**2+y_here**2)-0.1_WP*L
                    if (G.gt.0.0_WP) then
                       tmp_VOF=tmp_VOF+1.0_WP
                    end if
                 end do
              end do
              ! Calculate VOF from fine mesh
              VOF(i,j,k)=tmp_VOF/real(nF,WP)**2
           end do
        end do
     end do
  case ('plane')
     do k=1,nz
        do j=1,ny
           do i=1,nx
              ! Make VOF on fine mesh
              tmp_VOF=0.0_WP
              do jj=1,nF
                 do ii=1,nF
                    x_here=x(i)+real(ii,WP)/real(nf+1,WP)*(x(i+1)-x(i))
                    y_here=y(j)+real(jj,WP)/real(nf+1,WP)*(y(j+1)-y(j))
                    G=min(x_here+0.1_WP,-x_here+0.1_WP)
                    if (G.gt.0.0_WP) then
                       tmp_VOF=tmp_VOF+1.0_WP
                    end if
                 end do
              end do
              ! Calculate VOF from fine mesh
              VOF(i,j,k)=tmp_VOF/real(nF,WP)**2
           end do
        end do
     end do
  case ('plane y')
     do k=1,nz
        do j=1,ny
           do i=1,nx
              ! Make VOF on fine mesh
              tmp_VOF=0.0_WP
              do jj=1,nF
                 do ii=1,nF
                    x_here=x(i)+real(ii,WP)/real(nf+1,WP)*(x(i+1)-x(i))
                    y_here=y(j)+real(jj,WP)/real(nf+1,WP)*(y(j+1)-y(j))
                    G=min(y_here+0.1_WP,-y_here+0.1_WP)
                    if (G.gt.0.0_WP) then
                       tmp_VOF=tmp_VOF+1.0_WP
                    end if
                 end do
              end do
              ! Calculate VOF from fine mesh
              VOF(i,j,k)=tmp_VOF/real(nF,WP)**2
           end do
        end do
     end do
  case default
     stop "Interface shape not implemented yet."
  end select
  
  ! Set velocity
  do k=1,nz
     do j=1,ny
        do i=1,nx
           if (maxval(VOF(max(1,i-2):min(nx,i+1),max(1,j-1):min(ny,j+1),max(1,k-1):min(nz,k+1) )).ne.0.0_WP) U(i,j,k)=+1.0_WP
           !if (maxval(VOF(max(1,i-1):min(nx,i+1),max(1,j-2):min(ny,j+1),max(1,k-1):min(nz,k+1) )).ne.0.0_WP) V(i,j,k)=+1.0_WP
           !if (maxval(VOF(max(1,i-1):min(nx,i+1),max(1,j-1):min(ny,j+1),max(1,k-2):min(nz,k+1) )).ne.0.0_WP) W(i,j,k)=+1.0_WP
        end do
     end do
  end do
  
  return
end subroutine density_ratio_data

