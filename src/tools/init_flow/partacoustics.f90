module partacoustics
  use precision
  use param
  implicit none
  
  ! Length and diameter of the domain
  real(WP) :: Lx,Ly,Lz
  real(WP) :: dx,dy,dz
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: RHO
  real(WP), dimension(:,:,:), pointer :: dRHO
  
end module partacoustics


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine partacoustics_grid
  use partacoustics
  use parser
  implicit none
  
  integer :: i,j,k
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  
  call parser_read('Lx',Lx); dx=Lx/real(nx,WP)
  call parser_read('Ly',Ly); dy=Ly/real(ny,WP)
  call parser_read('Lz',Lz); dz=Lz/real(nz,WP)
  
  ! Handle periodicity
  xper=0; zper=1
  call parser_read('yper',yper)
  if (yper.eq.0) ny=ny+2
  
  ! Cartesian
  icyl=0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create mesh
  do i=1,nx+1
     x(i) = real(i-1,WP)*dx
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*dy
  end do
  if (yper.eq.0) y=y-dy
  do k=1,nz+1
     z(k) = real(k-1,WP)*dz-0.5_WP*Lz
  end do
  
  ! Create the mid points 
  do i=1,nx
     xm(i)= 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)= 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask=0
  if (yper.eq.0) then
     mask(:, 1)=1
     mask(:,ny)=1
  end if
  
  return
end subroutine partacoustics_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine partacoustics_data
  use partacoustics
  use parser
  implicit none
  
  ! Allocate the array data
  nvar = 6
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U    => data(:,:,:,1); names(1) = 'U'
  V    => data(:,:,:,2); names(2) = 'V'
  W    => data(:,:,:,3); names(3) = 'W'
  P    => data(:,:,:,4); names(4) = 'P'
  RHO  => data(:,:,:,5); names(5) = 'RHO'
  dRHO => data(:,:,:,6); names(6) = 'dRHO'
  
  ! Create them
  U    = 0.0_WP
  V    = 0.0_WP
  W    = 0.0_WP
  P    = 0.0_WP
  RHO  = 0.0_WP
  dRHO = 0.0_WP
  
  return
end subroutine partacoustics_data


! ========================================= !
! Generate an initial particle distribution !
! ========================================= !
subroutine partacoustics_part
  use partacoustics
  use parser
  use lpt_mod
  use random
  use math
  implicit none
  
  character(len=str_medium) :: part_init
  logical :: use_lpt
  
  ! Check whether lpt is used
  !call parser_is_defined('Init part file',use_lpt)
  use_lpt=.false.
  if (.not.use_lpt) return
  
  return
end subroutine partacoustics_part
