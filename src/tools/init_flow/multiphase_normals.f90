module multiphase_normals
  use string
  use precision
  use param
  implicit none
  
  ! Domain geometry
  real(WP) :: Lx,Ly,Lz
  real(WP) :: dx,dy,dz
  real(WP) :: sx,sy
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: G
  
end module multiphase_normals


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine multiphase_normals_grid
  use multiphase_normals
  use parser
  use math
  implicit none
  
  integer  :: i,j,k
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny,nx)
  call parser_read('nz',nz,1)
  
  call parser_read('Lx',Lx,1.0_WP)
  call parser_read('Ly',Ly,1.0_WP)
  call parser_read('Lz',Lz,1.0_WP/real(nx,WP))
  
  call parser_read('Stretching x',sx)
  call parser_read('Stretching y',sy)
  
  ! Set the periodicity
  xper = 1
  yper = 1
  zper = 1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  dx = Lx/real(nx,WP)
  dy = Ly/real(ny,WP)
  dz = Lz/real(nz,WP)
  do i=1,nx+1
     x(i) = real(i-1,WP)*dx-0.5_WP*Lx
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*dy-0.5_WP*Ly
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*dz-0.5_WP*Lz
  end do
  
  ! Apply stretching
  x = x + sx*sin(twoPi*x/Lx)
  y = y + sy*sin(twoPi*y/Ly)
  
  ! Create the mid points
  do i=1,nx
     xm(i)= 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)= 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine multiphase_normals_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine multiphase_normals_data
  use multiphase_normals
  use parser
  use math
  implicit none
  
  integer  :: i,j,k
  real(WP) :: angle,radius,mode,amp,thick
  
  ! Allocate the array data
  nvar = 4
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U => data(:,:,:,1); names(1) = 'U'
  V => data(:,:,:,2); names(2) = 'V'
  W => data(:,:,:,3); names(3) = 'W'
  G => data(:,:,:,4); names(4) = 'G'
  
  ! Initialize
  U = 0.0_WP
  V = 0.0_WP
  W = 0.0_WP
  G = huge(1.0_WP)
  
  ! Set the distance field
  call parser_read('Drop radius',radius)
  call parser_read('Mode',mode)
  call parser_read('Amplitude',amp)
  do k=1,nz
     do j=1,ny
        do i=1,nx
           angle = arctan(xm(i),ym(j))
           G(i,j,k) = radius-amp*cos(mode*angle) - sqrt(xm(i)**2+ym(j)**2)
        end do
     end do
  end do
  
  ! Convert to tanh profile
  call parser_read('Initial G thickness',thick)
  if (thick.gt.0.0_WP) then
     do k=1,nz
        do j=1,ny
           do i=1,nx
              G(i,j,k) = 0.5_WP*(tanh(0.5_WP*G(i,j,k)/thick)+1.0_WP)
           end do
        end do
     end do
  end if
  
  return
end subroutine multiphase_normals_data
