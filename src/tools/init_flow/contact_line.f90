module contact_line
  use string
  use precision
  use param
  implicit none
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: VOF
  
end module contact_line

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine contact_line_grid
  use contact_line
  use parser
  use math
  implicit none
  
  integer :: i,j,k
  real(WP) :: Lx,Ly,Lz
  
  ! Read in the number of grid points
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  xper = 1 
  yper = 0
  zper = 1

  ! Read in the domain size
  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  call parser_read('Lz',Lz)

  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = Lx*real(i-1,WP)/real(nx,WP)-0.5_WP*Lx
  end do
  do j=1,ny+1
     y(j) = Ly*real(j-1,WP)/real(ny,WP)
  end do
  do k=1,nz+1
     z(k) = Lz*real(k-1,WP)/real(nz,WP)-0.5_WP*Lz
  end do
  
  ! Create the cell centered grid
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  mask(:,1)=1
  
  return
end subroutine contact_line_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine contact_line_data
  use contact_line
  use parser
  use math
  implicit none
  
  integer :: i,j,k,ii,jj
  real(WP) :: dx,G,myx,myy,D
  integer, parameter :: nF=50
  
  ! Allocate the array data
  nvar = 4
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U     => data(:,:,:,1); names(1) = 'U'
  V     => data(:,:,:,2); names(2) = 'V'
  W     => data(:,:,:,3); names(3) = 'W'
  VOF   => data(:,:,:,4); names(4) = 'VOF'
  
  ! Initialize
  U   = 0.0_WP
  V   = 0.0_WP
  W   = 0.0_WP
  VOF = 0.0_WP  

  ! Setup the interface
  call parser_read('Drop diameter',D)
  do k=1,nz
     do j=1,ny
        do i=1,nx
           dx=min(x(i+1)-x(i),y(j+1)-y(j),z(k+1)-z(k))
           G = 0.5_WP*D-sqrt(xm(i)**2+ym(j)**2)
           if      ( G.gt.+2.0_WP*dx ) then ! Liquid phase
              VOF(i,j,k) = 1.0_WP
           else if ( G.lt.-2.0_WP*dx ) then ! Gas phase
              VOF(i,j,k) = 0.0_WP
           else                      ! Near interface 
              do jj=1,nF
                 do ii=1,nF
                    myx = x(i)+real(ii,WP)/real(nf+1,WP)*(x(i+1)-x(i))
                    myy = y(j)+real(jj,WP)/real(nf+1,WP)*(y(j+1)-y(j))
                    G = 0.5_WP*D-sqrt(myx**2+myy**2)
                    if (G.gt.0.0_WP) VOF(i,j,k)=VOF(i,j,k)+1.0_WP
                 end do
              end do
              VOF(i,j,k)=VOF(i,j,k)/real(nF,WP)**2
           end if                    
        end do
     end do
  end do
    
  return
end subroutine contact_line_data
