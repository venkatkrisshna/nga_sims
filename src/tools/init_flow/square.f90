module square
  use string
  use precision
  use param
  implicit none
  
  ! Size of the domain
  real(WP) :: Lx,Ly,Lz,H,dx,dy,dz
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: Z1
  real(WP), dimension(:,:,:), pointer :: Z2
  
end module square

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine square_grid
  use square
  use parser
  implicit none
  
  integer :: i,j,k
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  call parser_read('H',H)
  dx = Lx/real(nx,WP)
  dy = Ly/real(ny,WP)
  Ly=Ly+2.0_WP*dy
  ny=ny+2
  dz = min(dx,dy)
  Lz = real(nz,WP)*dz
  
  ! Set the periodicity
  xper = 0
  yper = 0
  zper = 1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*dx-5.0_WP*H
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*dy-0.5_WP*Ly
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*dz-0.5_WP*Lz
  end do
  
  ! Generate cell centered locations
  do i=1,nx
     xm(i)= 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)= 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks - square
  !mask = 0
  !do j=1,ny
  !   do i=1,nx
  !      if (xm(i).gt.0.0_WP   .and.xm(i).lt.H.and.  &
  !          ym(j).gt.-0.5_WP*H.and.ym(j).lt.0.5_WP*H) then
  !         mask(i,j)=1
  !      end if
  !   end do
  !end do
  
  ! Create the masks - square with fin
  !mask = 0
  !do j=1,ny
  !   do i=1,nx
  !      if (xm(i).gt.0.0_WP   .and.xm(i).lt.H.and.  &
  !          ym(j).gt.-0.5_WP*H.and.ym(j).lt.0.5_WP*H) then
  !         mask(i,j)=1
  !      end if
  !      if (xm(i).gt.H   .and.xm(i).lt.2.0_WP*H.and.  &
  !          ym(j).gt.-0.1_WP*H.and.ym(j).lt.0.1_WP*H) then
  !         mask(i,j)=1
  !      end if
  !   end do
  !end do
  
  ! Create the masks - V
  !mask = 0
  !do j=1,ny
  !   do i=1,nx
  !      if (xm(i).gt.0.0_WP.and.xm(i).lt.H) then
  !         if (abs(+ym(j)-xm(i)).lt.1.8_WP*dx) mask(i,j)=1
  !         if (abs(-ym(j)-xm(i)).lt.1.8_WP*dx) mask(i,j)=1
  !      end if
  !      if (xm(i).gt.H.and.xm(i).lt.2.0_WP*H) then
  !         if (abs(+ym(j)-H).lt.dx) mask(i,j)=1
  !         if (abs(-ym(j)-H).lt.dx) mask(i,j)=1
  !      end if
  !   end do
  !end do
  
  ! Create the masks - canister
  mask = 0
  do j=1,ny
     do i=1,nx
        if (xm(i).gt.0.0_WP.and.xm(i).lt.2.1_WP*dx) then
           if (abs(ym(j)).lt.H) mask(i,j)=1
        end if
        if (xm(i).gt.0.0_WP.and.xm(i).lt.2.0_WP*H) then
           if (abs(+ym(j)-H).lt.dx) mask(i,j)=1
           if (abs(-ym(j)-H).lt.dx) mask(i,j)=1
        end if
     end do
  end do
  
  ! Set side walls
  mask(:, 1)=1
  mask(:,ny)=1
  
  return
end subroutine square_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine square_data
  use square
  use parser
  implicit none
  
  integer :: i,j
  
  ! Allocate the array data
  nvar = 6
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U => data(:,:,:,1); names(1) = 'U'
  V => data(:,:,:,2); names(2) = 'V'
  W => data(:,:,:,3); names(3) = 'W'
  P => data(:,:,:,4); names(4) = 'P'
  Z1=> data(:,:,:,5); names(5) = 'Z1'
  Z2=> data(:,:,:,6); names(6) = 'Z2'
  
  ! Set to zero
  U = 0.0_WP
  V = 0.0_WP
  W = 0.0_WP
  P = 0.0_WP
  Z1= 1.0_WP
  Z2= 300.0_WP
  do j=1,ny
     do i=1,nx
        if (xm(i).gt.0.0_WP.and.xm(i).lt.2.0_WP*H.and.&
            ym(j).gt.-H.and.ym(j).lt.H) then
           Z2(i,j,:)=2500.0_WP
        end if
     end do
  end do
  
  return
end subroutine square_data
