module ib_pipe
  use precision
  use param
  implicit none
  
  ! Domain dimensions
  real(WP) :: L,D
  real(WP) :: dx,dy,dz
  real(WP) :: Lx,Ly,Lz
  
  ! Mean U velocity
  real(WP) :: Umean
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  
  ! Pointers to variable in optdata
  real(WP), dimension(:,:,:), pointer :: Gib
  
end module ib_pipe

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine ib_pipe_grid
  use ib_pipe
  use parser
  use math
  implicit none
  
  integer :: i,j,k
  
  ! Mesh points
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  
  ! Length
  call parser_read('Length',L)
  call parser_read('Diameter',D)
  
  ! Set the periodicity
  xper = 1
  yper = 1
  zper = 1
  
  ! Cartesian
  icyl = 0
  
  ! Adapt mesh for ib
  dx = L/real(nx,WP)
  dy = D/real(ny,WP)
  dz = D/real(nz,WP)
  Lx = L; nx = nx
  Ly = D+2.0_WP*dy; ny = ny+2
  Lz = D+2.0_WP*dz; nz = nz+2
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid in x and z
  do i=1,nx+1
     x(i) = real(i-1,WP)*dx
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*dy-0.5_WP*Ly
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*dz-0.5_WP*Lz
  end do
  
  ! Create the mid points
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine ib_pipe_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine ib_pipe_data
  use ib_pipe
  use parser
  use math
  implicit none
  
  integer  :: i,j,k,laminar
  real(WP) :: rnd,amp
  
  ! Initialize the random number generator
  call random_init
  
  ! Read the means
  call parser_read('Bulk velocity',Umean)
  
  ! Read in the amplitude of random numbers
  call parser_read('Fluct. amp.',amp)
  call parser_read('Laminar initial flow',laminar)
  
  ! Allocate the array data
  nvar = 4
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U => data(:,:,:,1); names(1) = 'U'
  V => data(:,:,:,2); names(2) = 'V'
  W => data(:,:,:,3); names(3) = 'W'
  P => data(:,:,:,4); names(4) = 'P'
  
  ! Create them
  U = 0.0_WP
  V = 0.0_WP
  W = 0.0_WP
  P = 0.0_WP
  
  ! Bulk velocity + random
  do k=1,nz
     do j=1,ny
        do i=1,nx
           call random_number(rnd); rnd=0.0_WP
           U(i,j,k) = Umean*(1.0_WP+(rnd-0.5_WP)*amp)
           V(i,j,k) = Umean*(rnd-0.5_WP)*amp
           W(i,j,k) = Umean*(rnd-0.5_WP)*amp
        end do
     end do
  end do
  
  ! For faster transition
  if (laminar.ne.1) then
     do k=1,nz
        do j=1,ny
           do i=1,nx
              U(i,j,k) = U(i,j,k) + amp*Umean*cos(8.0_WP*twoPi*zm(k)/Lz)*cos(8.0_WP*twoPi*ym(j)/Ly)
              V(i,j,k) = V(i,j,k) + amp*Umean*cos(8.0_WP*twoPi*xm(i)/Lx)
              W(i,j,k) = W(i,j,k) + amp*Umean*cos(8.0_WP*twoPi*xm(i)/Lx)
           end do
        end do
     end do
  end if
  
  return
end subroutine ib_pipe_data


! ====================== !
! Create the IB variable !
! ====================== !
subroutine ib_pipe_optdata
  use ib_pipe
  use parser
  implicit none
  
  integer :: i,j,k
  
  ! Allocate opt data
  nod = 1
  allocate(OD(nx,ny,nz,nod))
  allocate(OD_names(nod))
  
  ! Link the pointers
  Gib => OD(:,:,:,1); OD_names(1) = 'Gib'
  
  ! Cylinder only
  do k=1,nz
     do j=1,ny
        do i=1,nx
           Gib(i,j,k)=0.5_WP*D-sqrt(zm(k)**2+ym(j)**2)
        end do
     end do
  end do
  
  return
end subroutine ib_pipe_optdata
