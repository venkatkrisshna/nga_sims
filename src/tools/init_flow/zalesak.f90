module zalesak
  use string
  use precision
  use param
  implicit none
  
  ! Length of the domain
  real(WP) :: L
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: VOF1
  real(WP), dimension(:,:,:), pointer :: VOF2
  real(WP), dimension(:,:,:), pointer :: VOF3
  real(WP), dimension(:,:,:), pointer :: VOF4
  real(WP), dimension(:,:,:), pointer :: VOF5
  real(WP), dimension(:,:,:), pointer :: VOF6
  real(WP), dimension(:,:,:), pointer :: VOF7
  real(WP), dimension(:,:,:), pointer :: VOF8
  
end module zalesak

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine zalesak_grid
  use zalesak
  use parser
  implicit none
  integer :: i,j,k
  
  ! Read in the size of the domain
  call parser_read('Number of points',nx)
  call parser_read('Domain size',L)
  ny = nx
  nz = 1
  
  ! Set the periodicity
  xper = 1
  yper = 1
  zper = 1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1) ,y(ny+1) ,z(nz+1))
  allocate(xm(nx+1),ym(ny+1),zm(nz+1))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*L/real(nx,WP) - 0.5_WP*L
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*L/real(ny,WP) - 0.5_WP*L
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*L/real(nx,WP) - 0.5_WP*L
  end do
  
  ! Create the mid points
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine zalesak_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine zalesak_data
  use zalesak
  use parser
  implicit none
  integer :: i,j,k,ii,jj
  real(WP), dimension(3) :: xyz
  real(WP), dimension(:), allocatable :: center
  real(WP) :: radius,width,height
  real(WP) :: ang_vel
  real(WP) :: levelset_notched_circle
  
  ! Read input
  call parser_getsize('Circle center',i)
  allocate(center(i))
  call parser_read('Circle center',center)
  call parser_read('Circle radius',radius)
  call parser_read('Notch width',width)
  call parser_read('Notch height',height)
  call parser_read('Angular velocity',ang_vel)
  
  ! Allocate the array data
  nvar = 11
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U    => data(:,:,:, 1); names( 1) = 'U'
  V    => data(:,:,:, 2); names( 2) = 'V'
  W    => data(:,:,:, 3); names( 3) = 'W'
  VOF1 => data(:,:,:, 4); names( 4) = 'VOF1'
  VOF2 => data(:,:,:, 5); names( 5) = 'VOF2'
  VOF3 => data(:,:,:, 6); names( 6) = 'VOF3'
  VOF4 => data(:,:,:, 7); names( 7) = 'VOF4'
  VOF5 => data(:,:,:, 8); names( 8) = 'VOF5'
  VOF6 => data(:,:,:, 9); names( 9) = 'VOF6'
  VOF7 => data(:,:,:,10); names(10) = 'VOF7'
  VOF8 => data(:,:,:,11); names(11) = 'VOF8'
  
  ! Set velocity
  do k=1,nz
     do j=1,ny
        do i=1,nx
           U(i,j,k) =-ang_vel*ym(j)
           V(i,j,k) =+ang_vel*xm(i)
           W(i,j,k) = 0.0_WP
        end do
     end do
  end do
  
  ! Set interface
  do k=1,nz
     do j=1,ny
        do i=1,nx
           ! VOF initialization on fine mesh
           VOF1(i,j,k)=0.0_WP
           VOF2(i,j,k)=0.0_WP
           VOF3(i,j,k)=0.0_WP
           VOF4(i,j,k)=0.0_WP
           VOF5(i,j,k)=0.0_WP
           VOF6(i,j,k)=0.0_WP
           VOF7(i,j,k)=0.0_WP
           VOF8(i,j,k)=0.0_WP
           do jj=1,50
              do ii=1,50
                 xyz=(/x (i)+real(ii,WP)/51.0_WP*(xm(i)-x(i)),y (j)+real(jj,WP)/51.0_WP*(ym(j)-y(j)),0.0_WP/)
                 if (levelset_notched_circle(xyz,center,radius,width,height).gt.0.0_WP) VOF1(i,j,k)=VOF1(i,j,k)+4.0e-4_WP
                 xyz=(/xm(i)+real(ii,WP)/51.0_WP*(xm(i)-x(i)),y (j)+real(jj,WP)/51.0_WP*(ym(j)-y(j)),0.0_WP/)
                 if (levelset_notched_circle(xyz,center,radius,width,height).gt.0.0_WP) VOF2(i,j,k)=VOF2(i,j,k)+4.0e-4_WP
                 xyz=(/x (i)+real(ii,WP)/51.0_WP*(xm(i)-x(i)),ym(j)+real(jj,WP)/51.0_WP*(ym(j)-y(j)),0.0_WP/)
                 if (levelset_notched_circle(xyz,center,radius,width,height).gt.0.0_WP) VOF3(i,j,k)=VOF3(i,j,k)+4.0e-4_WP
                 xyz=(/xm(i)+real(ii,WP)/51.0_WP*(xm(i)-x(i)),ym(j)+real(jj,WP)/51.0_WP*(ym(j)-y(j)),0.0_WP/)
                 if (levelset_notched_circle(xyz,center,radius,width,height).gt.0.0_WP) VOF4(i,j,k)=VOF4(i,j,k)+4.0e-4_WP
                 xyz=(/x (i)+real(ii,WP)/51.0_WP*(xm(i)-x(i)),y (j)+real(jj,WP)/51.0_WP*(ym(j)-y(j)),0.0_WP/)
                 if (levelset_notched_circle(xyz,center,radius,width,height).gt.0.0_WP) VOF5(i,j,k)=VOF5(i,j,k)+4.0e-4_WP
                 xyz=(/xm(i)+real(ii,WP)/51.0_WP*(xm(i)-x(i)),y (j)+real(jj,WP)/51.0_WP*(ym(j)-y(j)),0.0_WP/)
                 if (levelset_notched_circle(xyz,center,radius,width,height).gt.0.0_WP) VOF6(i,j,k)=VOF6(i,j,k)+4.0e-4_WP
                 xyz=(/x (i)+real(ii,WP)/51.0_WP*(xm(i)-x(i)),ym(j)+real(jj,WP)/51.0_WP*(ym(j)-y(j)),0.0_WP/)
                 if (levelset_notched_circle(xyz,center,radius,width,height).gt.0.0_WP) VOF7(i,j,k)=VOF7(i,j,k)+4.0e-4_WP
                 xyz=(/xm(i)+real(ii,WP)/51.0_WP*(xm(i)-x(i)),ym(j)+real(jj,WP)/51.0_WP*(ym(j)-y(j)),0.0_WP/)
                 if (levelset_notched_circle(xyz,center,radius,width,height).gt.0.0_WP) VOF8(i,j,k)=VOF8(i,j,k)+4.0e-4_WP
              end do
           end do
        end do
     end do
  end do
  
  return
end subroutine zalesak_data


function levelset_notched_circle(xyz,center,radius,width,height)
  use precision
  implicit none
  
  real(WP) :: levelset_notched_circle
  real(WP), dimension(3), intent(in) :: xyz,center
  real(WP), intent(in) :: radius,width,height
  real(WP) :: c,b,b1,b2,h1,h2
  
  c = radius-sqrt(sum((xyz-center)**2))
  b1 = center(1)-0.5_WP*width
  b2 = center(1)+0.5_WP*width
  h1 = center(2)-radius*cos(asin(0.5_WP*width/radius))
  h2 = center(2)-radius+height
  
  if     (c>=0.0_WP.and.xyz(1)<=b1.and.xyz(2)<=h2) then
     b = b1-xyz(1)
     levelset_notched_circle = min(c,b)
  elseif (c>=0.0_WP.and.xyz(1)>=b2.and.xyz(2)<=h2) then
     b = xyz(1)-b2
     levelset_notched_circle = min(c,b)
  elseif (c>=0.0_WP.and.xyz(1)>=b1.and.xyz(1)<=b2.and.xyz(2)>=h2) then
     b = xyz(2)-h2
     levelset_notched_circle = min(c,b)
  elseif (c>=0.0_WP.and.xyz(1)<=b1.and.xyz(2)>=h2) then
     b = sqrt(sum((xyz-(/b1,h2,0.0_WP/))**2))
     levelset_notched_circle = min(c,b)
  elseif (c>=0.0_WP.and.xyz(1)>=b2.and.xyz(2)>=h2) then
     b = sqrt(sum((xyz-(/b2,h2,0.0_WP/))**2))
     levelset_notched_circle = min(c,b)
  elseif (xyz(1)>=b1.and.xyz(1)<=b2.and.xyz(2)<=h2.and.xyz(2)>=h1) then
     levelset_notched_circle = -min(abs(xyz(1)-b1),abs(xyz(1)-b2),abs(xyz(2)-h2)) 
  elseif (xyz(1)>=b1.and.xyz(1)<=b2.and.xyz(2)<=h1) then
     levelset_notched_circle = -min(sqrt(sum((xyz-(/b1,h1,0.0_WP/))**2)),sqrt(sum((xyz-(/b2,h1,0.0_WP/))**2)))
  else
     levelset_notched_circle = c  
  endif
  
  return
end function levelset_notched_circle
