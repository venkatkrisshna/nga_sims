module spray_combustion
  use precision
  use param
  implicit none
  
  ! Mesh size
  real(WP) :: Lx,Ly,Lz
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: RHO
  real(WP), dimension(:,:,:), pointer :: dRHO
  real(WP), dimension(:,:,:), pointer :: Yf
  real(WP), dimension(:,:,:), pointer :: Yo
  real(WP), dimension(:,:,:), pointer :: Yp
  real(WP), dimension(:,:,:), pointer :: T
  
end module spray_combustion


! =============== !
! Create the grid !
! =============== !
subroutine spray_combustion_grid
  use spray_combustion
  use parser
  use math
  implicit none
  
  integer :: i,j,k
  
  ! Read parameters
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  call parser_read('Lz',Lz)
  
  ! Set the periodicity
  xper = 0
  yper = 1
  zper = 1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*Lx/real(nx,WP)
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*Ly/real(ny,WP) - 0.5_WP*Ly
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*Lz/real(nz,WP) - 0.5_WP*Lz
  end do
  
  ! Create the mid points
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine spray_combustion_grid


! =============== !
! Create the data !
! =============== !
subroutine spray_combustion_data
  use spray_combustion
  use parser
  implicit none
  
  real(WP) :: Yf0,Yo0,Yp0,T0,U0,Wmol,P0,Yd0,W_p
  real(WP), parameter :: R_cst=8.314472_WP
  real(WP), parameter :: W_f=100.0e-3_WP
  real(WP), parameter :: W_o= 32.0e-3_WP
  real(WP), parameter :: W_d= 28.0e-3_WP
  
  ! Allocate the array data
  nvar = 10
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U    => data(:,:,:, 1); names( 1) = 'U'
  V    => data(:,:,:, 2); names( 2) = 'V'
  W    => data(:,:,:, 3); names( 3) = 'W'
  P    => data(:,:,:, 4); names( 4) = 'P'
  RHO  => data(:,:,:, 5); names( 5) = 'RHO'
  dRHO => data(:,:,:, 6); names( 6) = 'dRHO'
  Yf   => data(:,:,:, 7); names( 7) = 'Yf'
  Yo   => data(:,:,:, 8); names( 8) = 'Yo'
  Yp   => data(:,:,:, 9); names( 9) = 'Yp'
  T    => data(:,:,:,10); names(10) = 'T'
  
  ! Initialize
  data=0.0_WP
  call parser_read('Initial Yf',Yf0)
  call parser_read('Initial Yo',Yo0)
  call parser_read('Initial Yp',Yp0)
  call parser_read('Initial T',T0)
  call parser_read('Initial U',U0)
  call parser_read('Initial P',P0)
  U=U0
  T=T0
  Yf=Yf0
  Yo=Yo0
  Yp=Yp0
  Yd0=1.0_WP-Yf0-Yo0-Yp0
  W_p=W_f+11.0_WP*W_o
  Wmol = 1.0_WP/(Yf0/W_f+Yo0/W_o+Yp0/W_p+Yd0/W_d)
  RHO = P0*Wmol/(R_cst*T0)
  
  return
end subroutine spray_combustion_data
