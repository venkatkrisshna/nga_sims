module HITA_VOF
  use string
  use precision
  use param
  implicit none

  
  ! Mesh size
  real(WP) :: Lx,Ly,Lz, ratio

  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: VOF1
  real(WP), dimension(:,:,:), pointer :: VOF2
  real(WP), dimension(:,:,:), pointer :: VOF3
  real(WP), dimension(:,:,:), pointer :: VOF4
  real(WP), dimension(:,:,:), pointer :: VOF5
  real(WP), dimension(:,:,:), pointer :: VOF6
  real(WP), dimension(:,:,:), pointer :: VOF7
  real(WP), dimension(:,:,:), pointer :: VOF8
  
end module HITA_VOF


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine HITA_VOF_grid
  use HITA_VOF
  use parser
  use math
  implicit none 
  integer :: i,j,k
  real(WP) :: Length
  
  ! Read parameters
  call parser_read('Lx',Lx)
  call parser_read('ratio',ratio)

  if(Lx.le.1e-12_WP) then
    Length = 2.0_WP*pi
    Lx = ratio*Length
    Ly = Length
    Lz = Length
  else
    call parser_read('Ly',Ly)
    call parser_read('Lz',Lz)
    Lx = Length
    Ly = Length
    Lz = Length
  end if

  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  
  ! Set the periodicity
  xper = 0
  yper = 1
  zper = 1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*Lx/real(nx,WP)
  end do
  do j=1,ny+1
     !y(j) = real(j-1,WP)*Ly/real(ny,WP) - 0.5_WP*Ly
     y(j) = real(j-1,WP)*Ly/real(ny,WP)
  end do
  do k=1,nz+1
     !z(k) = real(k-1,WP)*Lz/real(nz,WP) - 0.5_WP*Lz
     z(k) = real(k-1,WP)*Lz/real(nz,WP)
  end do
  
  ! Create the mid points
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine HITA_VOF_grid

! ========================= !
! Create the variable array !
! ========================= !
subroutine HITA_VOF_data
  use HITA_VOF
  use precision
  use string
  use fileio
  use parser
  use cli_reader
  use math
  implicit none
  real(WP)  :: U_conv
    
  ! Allocate the array data
  nvar = 12
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  call parser_read('Convective Velocity', U_conv)

  ! Link the pointers
  U    => data(:,:,:,1); names(1) = 'U'
  V    => data(:,:,:,2); names(2) = 'V'
  W    => data(:,:,:,3); names(3) = 'W'
  P    => data(:,:,:,4); names(4) = 'P'
  VOF1 => data(:,:,:,5); names(5) = 'VOF1'
  VOF2 => data(:,:,:,6); names(6) = 'VOF2'
  VOF3 => data(:,:,:,7); names(7) = 'VOF3'
  VOF4 => data(:,:,:,8); names(8) = 'VOF4'
  VOF5 => data(:,:,:,9); names(9) = 'VOF5'
  VOF6 => data(:,:,:,10); names(10) = 'VOF6'
  VOF7 => data(:,:,:,11); names(11) = 'VOF7'
  VOF8 => data(:,:,:,12); names(12) = 'VOF8'
  
  ! Create the arrays
  U   = U_conv
  V   = 0.0_WP
  W   = 0.0_WP
  VOF1(:,:,:) = 0.0_WP
  VOF2(:,:,:) = 0.0_WP
  VOF3(:,:,:) = 0.0_WP
  VOF4(:,:,:) = 0.0_WP
  VOF5(:,:,:) = 0.0_WP
  VOF6(:,:,:) = 0.0_WP
  VOF7(:,:,:) = 0.0_WP
  VOF8(:,:,:) = 0.0_WP

  ! Fill bottom half with liquid
  VOF1(:,1:ny/2,:) = 1.0_WP
  VOF2(:,1:ny/2,:) = 1.0_WP
  VOF3(:,1:ny/2,:) = 1.0_WP
  VOF4(:,1:ny/2,:) = 1.0_WP
  VOF5(:,1:ny/2,:) = 1.0_WP
  VOF6(:,1:ny/2,:) = 1.0_WP
  VOF7(:,1:ny/2,:) = 1.0_WP
  VOF8(:,1:ny/2,:) = 1.0_WP

  print*,'periodicity:',xper,yper,zper
  
  
end subroutine HITA_VOF_data

