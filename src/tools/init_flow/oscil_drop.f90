module oscil_drop
  use string
  use precision
  use param
  implicit none
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:),   pointer :: U
  real(WP), dimension(:,:,:),   pointer :: V
  real(WP), dimension(:,:,:),   pointer :: W
  real(WP), dimension(:,:,:,:), pointer :: VOF
  
end module oscil_drop

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine oscil_drop_grid
  use oscil_drop
  use parser
  use math
  implicit none
  
  integer :: i,j,k
  real(WP) :: Lx,Ly,Lz
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  call parser_read('Lz',Lz)
  xper = 1
  yper = 1
  zper = 1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = Lx*real(i-1,WP)/real(nx,WP)-0.5_WP*Lx
  end do
  do j=1,ny+1
     y(j) = Ly*real(j-1,WP)/real(ny,WP)-0.5_WP*Ly
  end do
  do k=1,nz+1
     z(k) = Lz*real(k-1,WP)/real(nz,WP)-0.5_WP*Lz
  end do
  
  ! Create the cell centered grid
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine oscil_drop_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine oscil_drop_data
  use oscil_drop
  use parser
  use math
  implicit none
  
  integer :: s,i,j,k,ii,jj,kk
  real(WP) :: myx,myy,myz
  integer, parameter :: nF=50
  real(WP) :: dx_,G
  real(WP) :: x_1, x_2
  real(WP) :: x_min,x_max,y_min,y_max,yint
  integer :: xcycl,ycycl
  logical :: xdone,ydone
  real(WP) :: Rx,Ry

  ! Read the drop dimensions
  call parser_read('Drop x radius',Rx)
  call parser_read('Drop y radius',Ry)

  ! Allocate the array data
  nvar = 11
  allocate(data(imin_:imax_,jmin_:jmax_,kmin_:kmax_,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U    (imin_:imax_,jmin_:jmax_,kmin_:kmax_) => data(:,:,:, 1); names( 1) = 'U'
  V    (imin_:imax_,jmin_:jmax_,kmin_:kmax_) => data(:,:,:, 2); names( 2) = 'V'
  W    (imin_:imax_,jmin_:jmax_,kmin_:kmax_) => data(:,:,:, 3); names( 3) = 'W'
  VOF  (imin_:imax_,jmin_:jmax_,kmin_:kmax_,1:8) => data(:,:,:, 4:11);
  names( 4) = 'VOF1'
  names( 5) = 'VOF2'
  names( 6) = 'VOF3'
  names( 7) = 'VOF4'
  names( 8) = 'VOF5'
  names( 9) = 'VOF6'
  names(10) = 'VOF7'
  names(11) = 'VOF8'
  
  ! Initialize
  U   = 0.0_WP
  V   = 0.0_WP
  W   = 0.0_WP
  VOF = 0.0_WP

  ! Check if 2D or 3D
  if (nz.gt.1) then ! 3D
     ! Setup the interface 
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_

              dx_=max(x(i+1)-x(i),y(j+1)-y(j),z(k+1)-z(k))
              G = 1.0_WP-(xm(i)**2/Rx**2+ym(j)**2/Ry**2)
              if      ( G.lt.-2.0_WP*dx_) then ! Liquid phase
                 VOF(i,j,k,:) = 1.0_WP
              else if ( G.gt.+2.0_WP*dx_) then ! Gas phase
                 VOF(i,j,k,:) = 0.0_WP
              else                             ! Near interface
                 do s=1,8
                    do kk=1,nF
                       do jj=1,nF
                          do ii=1,nF
                             myx=xs1(s,i)+(real(ii,WP)-0.5_WP)/real(nf,WP)*(xs2(s,i)-xs1(s,i))
                             myy=ys1(s,j)+(real(jj,WP)-0.5_WP)/real(nf,WP)*(ys2(s,j)-ys1(s,j))
                             myz=zs1(s,k)+(real(kk,WP)-0.5_WP)/real(nf,WP)*(zs2(s,k)-zs1(s,k))
                             G = 1.0_WP-(myx**2/Rx**2+ym(j)**2/Ry**2)
                             if (G.lt.0.0_WP) VOF(i,j,k,s)=VOF(i,j,k,s)+1.0_WP
                          end do
                       end do
                    end do
                    VOF(i,j,k,s)=VOF(i,j,k,s)/real(nF,WP)**3
                 end do
              end if
           end do
        end do
     end do

  else ! 2D use exact integrations
     
     ! Setup the interface 
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_

              ! Loop over subcells
              do s=1,8
                 xdone=.false.
                 xcycl=1
                 do while (.not.xdone)
                    ! Split cell into parts located within the 1st quadrant
                    if ((xs1(s,i))*(xs2(s,i)).lt.0.0_WP) then
                       ! Cell needs to be split into two
                       if (xcycl.eq.1) then
                          x_min=0.0_WP
                          x_max=abs(xs1(s,i))
                          xcycl=2
                       else
                          x_min=0.0_WP
                          x_max=abs(xs2(s,i))
                          xdone=.true.
                       end if
                    else
                       x_min=min(abs(xs1(s,i)),abs(xs2(s,i)))
                       x_max=max(abs(xs1(s,i)),abs(xs2(s,i)))
                       xdone=.true.
                    end if

                    ydone=.false.
                    ycycl=1
                    do while (.not.ydone)
                       if ((ys1(s,j))*(ys2(s,j)).lt.0.0_WP) then
                          if (ycycl.eq.1) then
                             y_min=0.0_WP
                             y_max=abs(ys1(s,j))
                             ycycl=2
                          else
                             y_min=0.0_WP
                             y_max=abs(ys2(s,j))
                             ydone=.true.
                          end if
                       else
                          y_min=min(abs(ys1(s,j)),abs(ys2(s,j)))
                          y_max=max(abs(ys1(s,j)),abs(ys2(s,j)))
                          ydone=.true.
                       end if

                       ! Find Integration bounds
                       if (sqrt(Ry**2*(1-x_min**2/Rx**2)).gt.0.0_WP) then
                          yint=sqrt(Ry**2*(1-x_min**2/Rx**2))
                          if (yint.lt.y_min) then 
                             ! Cell in gas phase
                             VOF(i,j,k,s)=VOF(i,j,k,s)+0.0_WP
                             cycle
                          else if (yint.lt.y_max) then
                             ! Intersection on left face
                             x_1=x_min
                          else
                             ! Intersection on top face
                             x_1=sqrt(Rx**2*(1-y_max**2/Ry**2))
                          end if
                       else
                          ! Cell in gas phase
                          VOF(i,j,k,s)=VOF(i,j,k,s)+0.0_WP
                          cycle
                       end if

                       if (sqrt(Ry**2*(1-x_max**2/Rx**2)).gt.0.0_WP) then
                          yint=sqrt(Ry**2*(1-x_max**2/Rx**2))
                          if (yint.gt.y_max) then 
                             ! Cell in liquid phase
                             VOF(i,j,k,s)=VOF(i,j,k,s)+((x_max-x_min)*(y_max-y_min))/((xs2(s,i)-xs1(s,i))*(ys2(s,j)-ys1(s,j)))
                             cycle
                          else if (yint.gt.y_min) then
                             ! Intersection on right face
                             x_2=x_max
                          else
                             ! Intersection on bottom face
                             x_2=sqrt(Rx**2*(1-y_min**2/Ry**2))
                          end if
                       else
                          ! Intersection on bottom face
                          x_2=sqrt(Rx**2*(1-y_min**2/Ry**2))
                       end if

                       ! Integrate 
                       VOF(i,j,k,s)= VOF(i,j,k,s) &
                            + ( (x_1-x_min)*(y_max-y_min) & ! Part to left of x_1
                                ! Integrate from x1 to x2
                            + ((Rx*Ry*asin(x_2/Rx))/2.0_WP + (Ry*x_2*sqrt(Rx**2 - x_2**2))/(2.0_WP*Rx)) &
                            - ((Rx*Ry*asin(x_1/Rx))/2.0_WP + (Ry*x_1*sqrt(Rx**2 - x_1**2))/(2.0_WP*Rx)) &
                                ! Remove region below cell and between x1 and x2
                            - (y_min*(x_2-x_1)) &
                            ) &
                                ! Normalize by area of this cell
                            / ((xs2(s,i)-xs1(s,i))*(ys2(s,j)-ys1(s,j)))
                    end do
                 end do
              end do
           end do
        end do
     end do
  end if

  return
end subroutine oscil_drop_data
