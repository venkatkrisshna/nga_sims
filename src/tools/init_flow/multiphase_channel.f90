module multiphase_channel
  use string
  use precision
  use param
  implicit none
  
  ! Domain
  real(WP) :: Htot
  real(WP) :: Hliq,Hgas
  real(WP) :: Lx,dy,Lz
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: G
  
end module multiphase_channel

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine multiphase_channel_grid
  use multiphase_channel
  use parser
  use math
  implicit none
  
  integer  :: i,j,k

  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  nz=1
  call parser_read('Total height',Htot)
  call parser_read('Gas height',Hgas)
  icyl=0
  dy=Htot/real(ny-2,WP)
  Lz=dy
  Lx=real(nx,WP)*dy

  ! Periodicity
  call parser_read('xper',xper)
  yper = 0
  zper = 1
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*Lx/real(nx,WP)
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*Htot/real(ny,WP)-0.5_WP*Htot
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*Lz/real(nz,WP)-0.5_WP*Lz
  end do
  ! Create the cell centered grid
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  mask(:,1)=1
  mask(:,ny)=1

  return
end subroutine multiphase_channel_grid


! =================== !
! Generate the inflow !
! =================== !
subroutine multiphase_channel_inflow
  use multiphase_channel
  use parser
  implicit none
  
  real(WP) :: thick,Ui,mu_liq,mu_gas,a,rtmp
  integer  :: j,k

  ! Only if not periodic
  if (xper.eq.1) return
  
  ! Generate an inflow file with levelset
  nvar_inflow = 4
  ntime = 2
  
  ! Allocate some arrays
  allocate(inflow(ntime,ny,nz,nvar_inflow))
  allocate(names_inflow(nvar_inflow))
  
  ! Link the pointers
  U => inflow(:,:,:,1); names_inflow(1) = 'U'
  V => inflow(:,:,:,2); names_inflow(2) = 'V'
  W => inflow(:,:,:,3); names_inflow(3) = 'W'
  G => inflow(:,:,:,4); names_inflow(4) = 'G'
  
  ! Compute time grid
  time_inflow = 0.0_WP
  dt_inflow   = 1.0_WP

  ! Case parameters
  call parser_read('Liquid viscosity',mu_liq)
  call parser_read('Gas viscosity',mu_gas)
  call parser_read('Pressure gradient',a)
  Hliq=0.5_WP*(Htot-Hgas)
  Ui=0.5_WP*a*Hliq*(Hliq+Hgas)/mu_liq

  ! Initialize the distance
  do j=1,ny
     do k=1,nz
        G(:,j,k)=abs(ym(j))-0.5_WP*Hgas
     end do
  end do
  
  ! Initialize the velocity
  V=0.0_WP
  W=0.0_WP
  do k=1,nz
     do j=1,ny
        rtmp=-0.5_WP*a*(ym(j)**2-(Hliq+0.5_WP*Hgas)**2)
        if (G(1,j,k).lt.0.0_WP) then
           ! in the gas
           U(:,j,k)=rtmp/mu_gas+0.5_WP*a*Hliq*(1.0_WP/mu_liq-1.0_WP/mu_gas)*(Hliq+Hgas)
        else
           ! in the liquid
           U(:,j,k)=rtmp/mu_liq
        end if
     end do
  end do
  
  ! Convert to tanh profile
  call parser_read('Initial G thickness',thick,-1.0_WP)
  if (thick.gt.0.0_WP) then
     do k=1,nz
        do j=1,ny
           G(:,j,k) = 0.5_WP*(tanh(0.5_WP*G(:,j,k)/thick)+1.0_WP)
        end do
     end do
  end if

  return
end subroutine multiphase_channel_inflow


! ========================= !
! Create the variable array !
! ========================= !
subroutine multiphase_channel_data
  use multiphase_channel
  use parser
  implicit none
  
  real(WP) :: thick,Ui,mu_liq,mu_gas,a,rtmp
  integer  :: i,j,k
  
  ! Allocate the array data
  nvar = 4
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U => data(:,:,:,1); names(1) = 'U'
  V => data(:,:,:,2); names(2) = 'V'
  W => data(:,:,:,3); names(3) = 'W'
  G => data(:,:,:,4); names(4) = 'G'
  
  ! Create them
  U = 0.0_WP
  V = 0.0_WP
  W = 0.0_WP
  G = 0.0_WP
  
  ! Case parameters
  call parser_read('Liquid viscosity',mu_liq)
  call parser_read('Gas viscosity',mu_gas)
  call parser_read('Pressure gradient',a)
  Hliq=0.5_WP*(Htot-Hgas)
  Ui=0.5_WP*a*Hliq*(Hliq+Hgas)/mu_liq

  ! Initialize the distance
  do i=1,nx
     do j=1,ny
        do k=1,nz
           G(i,j,k)=abs(ym(j))-0.5_WP*Hgas
        end do
     end do
  end do
  
  ! Initialize the velocity
  do k=1,nz
     do j=1,ny
        do i=1,nx
           rtmp=-0.5_WP*a*(ym(j)**2-(Hliq+0.5_WP*Hgas)**2)
           if (G(i,j,k).lt.0.0_WP) then
              ! in the gas
              U(i,j,k)=rtmp/mu_gas+0.5_WP*a*Hliq*(1.0_WP/mu_liq-1.0_WP/mu_gas)*(Hliq+Hgas)
           else
              ! in the liquid
              U(i,j,k)=rtmp/mu_liq
           end if
        end do
     end do
  end do
  
  ! Convert to tanh profile
  call parser_read('Initial G thickness',thick,-1.0_WP)
  if (thick.gt.0.0_WP) then
     do k=1,nz
        do j=1,ny
           do i=1,nx
              G(i,j,k) = 0.5_WP*(tanh(0.5_WP*G(i,j,k)/thick)+1.0_WP)
           end do
        end do
     end do
  end if
  
  return
end subroutine multiphase_channel_data
