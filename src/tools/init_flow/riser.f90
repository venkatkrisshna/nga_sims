module riser
  use precision
  use param
  use parallel
  implicit none

  ! Length and diameter of the domain
  real(WP) :: Lx,Ly,Lz
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: RHO
  real(WP), dimension(:,:,:), pointer :: dRHO
  
end module riser


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine riser_grid
  use riser
  use parser
  implicit none
  
  integer :: i,j,k
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  
  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  call parser_read('Lz',Lz)

  ! Set the periodicity
  xper=1
  yper=1
  zper=1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*Lx/real(nx,WP)
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*Ly/real(ny,WP) - 0.5_WP*Ly
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*Lz/real(nz,WP) - 0.5_WP*Lz
  end do
  
  ! Create the mid points 
  do i=1,nx
     xm(i)= 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)= 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine riser_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine riser_data
  use riser
  use parser
  implicit none

  ! Allocate the array data
  nvar = 6
  allocate(data(imin_:imax_,jmin_:jmax_,kmin_:kmax_,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U    => data(:,:,:,1); names(1) = 'U'
  V    => data(:,:,:,2); names(2) = 'V'
  W    => data(:,:,:,3); names(3) = 'W'
  P    => data(:,:,:,4); names(4) = 'P'
  RHO  => data(:,:,:,5); names(5) = 'RHO'
  dRHO => data(:,:,:,6); names(6) = 'dRHO'
  
  ! Create them
  U    = 0.0_WP
  V    = 0.0_WP
  W    = 0.0_WP
  P    = 0.0_WP
  RHO  = 0.0_WP
  dRHO = 0.0_WP
  
  return
end subroutine riser_data


! ========================================= !
! Generate an initial particle distribution !
! ========================================= !
subroutine riser_part
  use riser
  use parser
  use lpt_mod
  use random
  implicit none

  character(len=str_medium) :: distro
  character(len=str_medium) :: part_init,filename
  logical :: use_lpt
  integer :: iunit,ierr
  integer :: i,np,np_,n
  real(WP) :: VFavg
  real(WP) :: mean_d
  real(WP) :: uin,vin,win,amp
  real(WP) :: dt,time,rand,buf
  real(WP) :: meanVp,meanWp,meanVp2,meanWp2

  ! Check whether lpt is used
  call parser_is_defined('Init part file',use_lpt)
  if (.not.use_lpt) return

  ! Particle distribution
  call parser_read('Particle distribution',distro)

  ! Read average volume fraction and bed height
  call parser_read('Avg. volume fraction',VFavg)

  ! Velocity parameters
  call parser_read('Particle u velocity',uin,0.0_WP)
  call parser_read('Particle v velocity',vin,0.0_WP)
  call parser_read('Particle w velocity',win,0.0_WP)
  call parser_read('Particle fluctuations',amp,0.0_WP)

  ! Distribute particles / assign diameter
  select case (trim(distro))
  case ('random')
     call riser_random(VFavg,np,np_,mean_d)
  case default
     stop 'Unknown particle distribution'
  end select

  ! Initialize the random number generator
  call random_init

  ! Initialize mean Vp & Wp
  meanVp=0.0_WP
  meanWp=0.0_WP

  ! Set particle parameters
  do i=1,np_
     
     ! Set various parameters for the particle
     part(i)%dt  =0.0_WP
     part(i)%Acol=0.0_WP
     part(i)%Tcol=0.0_WP
     part(i)%wx=0.0_WP
     part(i)%wy=0.0_WP
     part(i)%wz=0.0_WP
     part(i)%id  =int8((irank-1)*np_+i)
     part(i)%stop=0
     
     ! Give it a velocity
     if (nx.gt.1) then
        part(i)%u = uin
        if (amp.gt.0.0_WP) then
           call random_number(rand)
           rand=2.0_WP*rand-1.0_WP
           part(i)%u = part(i)%u + amp*rand
        end if
     else
        part(i)%u = 0.0_WP
     end if
     if (ny.gt.1) then
        part(i)%v = vin
        if (amp.gt.0.0_WP) then
           call random_number(rand)
           rand=2.0_WP*rand-1.0_WP
           part(i)%v = part(i)%v + amp*rand
        end if
     else
        part(i)%v = 0.0_WP
     end if
     if (nz.gt.1) then
        part(i)%w = win
        if (amp.gt.0.0_WP) then
           call random_number(rand)
           rand=2.0_WP*rand-1.0_WP
           part(i)%w = part(i)%w + amp*rand
        end if
     else
        part(i)%w = 0.0_WP
     end if

     meanVp=meanVp+part(i)%v
     meanWp=meanWp+part(i)%w
     
  end do

  ! Normalize
  call parallel_sum(meanVp,buf); meanVp=buf
  call parallel_sum(meanWp,buf); meanWp=buf
  meanVp=meanVp/real(np,WP)
  meanWp=meanWp/real(np,WP)

  ! Initialize new Vp & Wp
  meanVp2=0.0_WP
  meanWp2=0.0_WP

  do i=1,np_
     part(i)%v=part(i)%v-meanVp
     part(i)%w=part(i)%w-meanWp

     meanVp2=meanVp2+part(i)%v
     meanWp2=meanWp2+part(i)%w
  end do

  ! Normalize
  call parallel_sum(meanVp2,buf); meanVp2=buf
  call parallel_sum(meanWp2,buf); meanWp2=buf
  meanVp2=meanVp2/real(np,WP)
  meanWp2=meanWp2/real(np,WP)
  
  ! Output
  if (irank.eq.iroot) then
     print*,'Number of particles: ',np
     print*,'Effective mean diameter: ',mean_d
     print*,'Effective volume fraction: ',VFavg
  end if

  ! Generate the initial particle file
  call parser_read('Init part file',part_init)
  
  ! Add serial tag to dirname
  part_init = trim(adjustl(part_init))//'_serial'

  ! Create directory
  if (irank.eq.iroot) then
     call CREATE_FOLDER(trim(part_init))
  end if
  call MPI_BARRIER(comm,ierr)

  ! Loop over processors, in chunks of 100
  do i=0,(nproc-1)/100+1
     if (irank/100.eq.i) then
        
        ! Create filename
        filename = trim(adjustl(part_init)) // "/part."
        write(filename(len_trim(filename)+1:len_trim(filename)+6),'(i6.6)') irank
        
        ! Open the file to write
        call BINARY_FILE_OPEN(iunit,trim(adjustl(filename)),"w",ierr)
        
        ! Write header corresponding to proc
        call BINARY_FILE_WRITE(iunit,np_,1,kind(np_),ierr)                                      
        call BINARY_FILE_WRITE(iunit,part_kind,1,kind(part_kind),ierr)
        dt=0.0_WP
        call BINARY_FILE_WRITE(iunit,dt,1,kind(dt),ierr)   
        time=0.0_WP
        call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr) 

        ! Write local particles
        call BINARY_FILE_WRITE(iunit,part,np_,part_kind,ierr)
        
        ! Close the file 
        call BINARY_FILE_CLOSE(iunit,ierr)
     end if

     ! Synchronize                                                                             
     call MPI_BARRIER(comm,ierr)
  end do

  return
end subroutine riser_part


! =================== !
! Random distribution !
! =================== !
subroutine riser_random(VFavg,np,np_,mean_d)
  use riser
  use lpt_mod
  use parser
  use math
  use random
  implicit none
  
  integer :: i
  integer, intent(out) :: np,np_
  real(WP), intent(out) :: mean_d
  real(WP), intent(in) :: VFavg
  real(WP) :: Volp,Volbed,sumVolp,rp,rand
  
  ! Read particle diameter
  call parser_read('Particle mean diameter',mean_d)

  ! Initialize the random number generator
  call random_init
  
  ! First get bed volume
  Volbed = Lx*Ly*Lz
  
  ! Determine number of particles
  Volp = Pi/6.0_WP*mean_d**3
  np = int(VFavg*Volbed/Volp)
  np_ = ceiling(real(np,WP)/real(nproc,WP))
  if (irank.eq.nproc) np_=np-(nproc-1)*ceiling(real(np,WP)/real(nproc,WP))

  ! Allocate particle array
  allocate(part(1:np_))
  
  ! Distribute particles
  do i=1,np_
     ! Set particle size
     part(i)%d = mean_d

     call random_number(rand)
     rand=rand*(x(imax_+1)-x(imin_))
     part(i)%x = x(imin_)+rand
     call random_number(rand)
     rand=rand*(y(jmax_+1)-y(jmin_))
     part(i)%y = y(jmin_)+rand
     call random_number(rand)
     rand=rand*(z(kmax_+1)-z(kmin_))
     part(i)%z = z(kmin_)+rand
  end do
 
  return
end subroutine riser_random
