module atom_angled
  use string
  use precision
  use param
  implicit none
  
  ! Domain
  real(WP) :: Lx,Ly,Lz
  real(WP) :: dx,dy,dz
  real(WP) :: bellrad
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:),   pointer :: U
  real(WP), dimension(:,:,:),   pointer :: V
  real(WP), dimension(:,:,:),   pointer :: W
  real(WP), dimension(:,:,:,:), pointer :: VOF
  real(WP), dimension(:,:,:),   pointer :: L_Q
  !real(WP), dimension(:,:,:),   pointer :: LsID

end module atom_angled

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine atom_angled_grid
  use atom_angled
  use parser
  use math
  implicit none
  integer :: i,j,k
  real(WP) :: xJet

  ! Cylindrical
  icyl=0
  
  ! Periodicity
  xper=0
  yper=0
  zper=1
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('Lx',Lx)
  dx = Lx/real(nx,WP)
  call parser_read('ny',ny)
  call parser_read('Ly',Ly)
  dy = Ly/real(ny,WP)
  call parser_read('nz',nz)
  call parser_read('Lz',Lz)
  dz = Lz/real(nz,WP)
  call parser_read('Bell radius',bellrad)
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  call parser_read('x jet percent',xJet)
  do i=1,nx+1
     x(i) = real(i-1,WP)*dx-xJet*Lx
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*dy
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*dz-0.5_WP*Lz
  end do
  
  ! Create the cell centered grid
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine atom_angled_grid


! =================== !
! Generate the inflow !
! =================== !
subroutine atom_angled_inflow
  use atom_angled
  use parser
  use math
  implicit none
  
  real(WP) :: Djet,Ujet,thetaJet,d,Rjet,r
  real(WP) :: vof_tmp,myx,myz
  integer :: s,i,k,ii,kk
  integer, parameter :: nF=10

  ! Inflow x
  ! =============================
  
  ! Generate an inflow file with levelset
  nvar_inflow = 11
  ntime = 2
  
  ! Allocate some arrays
  allocate(inflow(ntime,jmin_:jmax_,kmin_:kmax_,nvar_inflow))
  allocate(names_inflow(nvar_inflow))
  
  ! Link the pointers
  U    (1:ntime,jmin_:jmax_,kmin_:kmax_) => inflow(:,:,:, 1); names_inflow( 1) = 'U'
  V    (1:ntime,jmin_:jmax_,kmin_:kmax_) => inflow(:,:,:, 2); names_inflow( 2) = 'V'
  W    (1:ntime,jmin_:jmax_,kmin_:kmax_) => inflow(:,:,:, 3); names_inflow( 3) = 'W'
  VOF  (1:ntime,jmin_:jmax_,kmin_:kmax_,1:8) => inflow(:,:,:, 4:11);
  names_inflow( 4) = 'VOF1'
  names_inflow( 5) = 'VOF2'
  names_inflow( 6) = 'VOF3'
  names_inflow( 7) = 'VOF4'
  names_inflow( 8) = 'VOF5'
  names_inflow( 9) = 'VOF6'
  names_inflow(10) = 'VOF7'
  names_inflow(11) = 'VOF8'
  ! L_Q => inflow(:,:,:, 12); names_inflow( 12) = 'L_Q'
  ! LsID (1:ntime,jmin_:jmax_,kmin_:kmax_) => inflow(:,:,:, 12); names_inflow( 12) = 'LsID'
  ! Compute time grid
  time_inflow = 0.0_WP
  dt_inflow   = 1.0_WP

  U  =0.0_WP
  V  =0.0_WP
  W  =0.0_WP
  VOF=0.0_WP

  ! Inflow y
  ! =============================
  nvar_inflowy = 11
  ntimey = 2

  ! Allocate some arrays
  allocate(inflowy(ntimey,imin_:imax_,kmin_:kmax_,nvar_inflowy))
  allocate(names_inflowy(nvar_inflowy))

  ! Link the pointers
  U    (1:ntime,imin_:imax_,kmin_:kmax_) => inflowy(:,:,:, 1); names_inflowy( 1) = 'U'
  V    (1:ntime,imin_:imax_,kmin_:kmax_) => inflowy(:,:,:, 2); names_inflowy( 2) = 'V'
  W    (1:ntime,imin_:imax_,kmin_:kmax_) => inflowy(:,:,:, 3); names_inflowy( 3) = 'W'
  VOF  (1:ntime,imin_:imax_,kmin_:kmax_,1:8) => inflowy(:,:,:, 4:11);
  names_inflowy( 4) = 'VOF1'
  names_inflowy( 5) = 'VOF2'
  names_inflowy( 6) = 'VOF3'
  names_inflowy( 7) = 'VOF4'
  names_inflowy( 8) = 'VOF5'
  names_inflowy( 9) = 'VOF6'
  names_inflowy(10) = 'VOF7'
  names_inflowy(11) = 'VOF8'

  ! Compute time grid
  time_inflowy = 0.0_WP
  dt_inflowy   = 1.0_WP
  
  ! Profiles parameters
  call parser_read('Djet',Djet,0.0080_WP)
  call parser_read('Ujet',Ujet)
  call parser_read('ThetaJet',thetaJet)
  
  ! Prepare the profiles
  U = 0.0_WP
  V = 0.0_WP
  W = 0.0_WP
  VOF=0.0_WP
  !LsID=1.0_WP
  do k=kmin_,kmax_
     do i=imin_,imax_

        ! Generate the distance from the jet center
        ! d = sqrt(xm(i)**2+zm(k)**2)
        Rjet=0.5_WP*Djet
        d=xm(i)**2/(Rjet/cos(ThetaJet*pi/180.0_WP))**2+zm(k)**2/Rjet**2
        ! Define the velocity
       ! if (     d.le.1.0_WP) then
           ! Inside the liquid jet
       !    U(:,i,k) = Ujet*sin(thetaJet*pi/180.0_WP)
       !    V(:,i,k) = Ujet*cos(thetaJet*pi/180.0_WP)
       ! else
           ! Outside injector - add a coflow for entrainment
           !U(:,j,k) = Uout
        !end if
        ! Create VOF profile
        ! Loop over subcells
        do s=1,8
           ! Use finer mesh within the subcell
           vof_tmp=0.0_WP
           do kk=1,nF
              do ii=1,nF
                 myx=xs1(s,i)+real(ii,WP)/real(nf+1,WP)*(xs2(s,i)-xs1(s,i))
                 myz=zs1(s,k)+real(kk,WP)/real(nf+1,WP)*(zs2(s,k)-zs1(s,k))
                 d=myx**2/(Rjet/cos(ThetaJet*pi/180.0_WP))**2+myz**2/Rjet**2
                 if (d.le.1.0_WP) then
                    vof_tmp=vof_tmp+1.0_WP
                 end if
              end do
           end do
           VOF(:,i,k,s)=vof_tmp/real(nF,WP)**2
        end do
        ! Redefine velocity
        if (any(VOF(:,i,k,:).gt.epsilon(1.0_WP))) then
                r = (x(i)**2/(Rjet/cos(ThetaJet*pi/180.0_WP))**2+zm(k)**2/Rjet**2)
                if (r.lt.1) &
                        U(:,i,k) = Ujet*sin(thetaJet*pi/180.0_WP)*2*(1-r)
                r =(xm(i)**2/(Rjet/cos(ThetaJet*pi/180.0_WP))**2+zm(k)**2/Rjet**2)
                if (r.lt.1) &
                        V(:,i,k) = Ujet*cos(thetaJet*pi/180.0_WP)*2*(1-r)
        end if
     end do
  end do

  ! Charge density
  !L_Q=Q
  
  return
end subroutine atom_angled_inflow


! ========================= !
! Create the variable array !
! ========================= !
subroutine atom_angled_data
  use atom_angled
  use parser
  use math
  use string
  implicit none
  
  real(WP) :: Djet,Ujet,thetaJet,d,Rjet,vof_tmp,r
  integer :: s,i,j,k,ii,jj,kk
  real(WP) :: myx,myy,myz
  integer, parameter :: nF=20
  
  ! Allocate the array data
  nvar = 11
  allocate(data(imin_:imax_,jmin_:jmax_,kmin_:kmax_,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U    (imin_:imax_,jmin_:jmax_,kmin_:kmax_) => data(:,:,:, 1); names( 1) = 'U'
  V    (imin_:imax_,jmin_:jmax_,kmin_:kmax_) => data(:,:,:, 2); names( 2) = 'V'
  W    (imin_:imax_,jmin_:jmax_,kmin_:kmax_) => data(:,:,:, 3); names( 3) = 'W'
  VOF  (imin_:imax_,jmin_:jmax_,kmin_:kmax_,1:8) => data(:,:,:, 4:11);
  names( 4) = 'VOF1'
  names( 5) = 'VOF2'
  names( 6) = 'VOF3'
  names( 7) = 'VOF4'
  names( 8) = 'VOF5'
  names( 9) = 'VOF6'
  names(10) = 'VOF7'
  names(11) = 'VOF8'
  ! L_Q => data(:,:,:, 12); names( 12) = 'L_Q'
  ! LsID (imin_:imax_,jmin_:jmax_,kmin_:kmax_) => data(:,:,:, 12); names( 12) = 'LsID'

  ! Set initial condition to zero
  U  =0.0_WP
  V  =0.0_WP
  W  =0.0_WP
  VOF=0.0_WP

  ! Profiles parameters
  call parser_read('Djet',Djet,0.0080_WP)
  call parser_read('Ujet',Ujet)
  call parser_read('ThetaJet',thetaJet)

  ! Prepare the profiles
  U = 0.0_WP
  V = 0.0_WP
  W = 0.0_WP
  VOF=0.0_WP
  !LsID=1.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
     do i=imin_,imax_

        ! Generate the distance from the jet center
        ! d = sqrt(xm(i)**2+zm(k)**2)
        Rjet=0.5_WP*Djet
        d=xm(i)**2/(Rjet/cos(ThetaJet*pi/180.0_WP))**2+zm(k)**2/Rjet**2+ym(j)**2/Rjet**2
        ! Define the velocity
        if (     d.le.1.0_WP) then
           ! Inside the liquid jet
           ! U(i,j,k) = Ujet*sin(thetaJet*pi/180.0_WP)*(abs(xm(i))-Rjet)
           ! V(i,j,k) = Ujet*cos(thetaJet*pi/180.0_WP)*(abs(xm(i))-Rjet)
        else
           ! Outside injector - add a coflow for entrainment
           !U(:,j,k) = Uout
        end if
        ! Create VOF profile
        ! Loop over subcells
        do s=1,8
           ! Use finer mesh within the subcell
           vof_tmp=0.0_WP
           do kk=1,nF
              do jj=1,nF
              do ii=1,nF
                 myx=xs1(s,i)+real(ii,WP)/real(nf+1,WP)*(xs2(s,i)-xs1(s,i))
                 myy=ys1(s,j)+real(jj,WP)/real(nf+1,WP)*(ys2(s,j)-ys1(s,j))
                 myz=zs1(s,k)+real(kk,WP)/real(nf+1,WP)*(zs2(s,k)-zs1(s,k))
                 d=myx**2/(Rjet/cos(ThetaJet*pi/180.0_WP))**2+myz**2/Rjet**2+myy**2/Rjet**2
                 if (d.le.1.0_WP) then
                    vof_tmp=vof_tmp+1.0_WP
                 end if
              end do
              end do
           end do
           VOF(i,j,k,s)=vof_tmp/real(nF,WP)**3
        end do
        ! Redefine velocity
        if (any(VOF(i,j,k,:).gt.epsilon(1.0_WP))) then
                r = (x (i)**2/(Rjet/cos(ThetaJet*pi/180.0_WP))**2+zm(k)**2/Rjet**2)
                if (r.lt.1) &
                        U(i,j,k) = Ujet*sin(thetaJet*pi/180.0_WP)*2*(1-r)
                r = (xm(i)**2/(Rjet/cos(ThetaJet*pi/180.0_WP))**2+zm(k)**2/Rjet**2)
                if (r.lt.1) &
                        V(i,j,k) = Ujet*cos(thetaJet*pi/180.0_WP)*2*(1-r)
        end if
        !if (any(VOF(max(imin_,i-1):i,j,k,:).gt.epsilon(1.0_WP))) &
        !        U(i,j,k) = Ujet*sin(thetaJet*pi/180.0_WP)*(abs(x (i))-Rjet)/Rjet
        !if (any(VOF(i,max(jmin_,j-1):j,k,:).gt.epsilon(1.0_WP))) &
        !        V(i,j,k) = Ujet*cos(thetaJet*pi/180.0_WP)*(abs(xm(i))-Rjet)/Rjet
     end do
     end do
  end do
  
  ! ! Set the initial fields
  ! thick = 0.5_WP*dx
  ! call parser_read('Djet',Djet)
  ! call parser_read('Dcof',Dcof)
  ! call parser_read('hcof',hcof)
  ! call parser_read('Ujet',Ujet)
  ! call parser_read('Ucof',Ucof)
  ! call parser_read('Uout',Uout)
  ! !call parser_read('Charge density',Q)

  ! ! Set profile thickness to mesh size
  ! dl=2.0_WP*dx
  ! dg=2.0_WP*dx

  ! ! Prepare the profiles
  ! V=0.0_WP
  ! W=0.0_WP
  ! LsID=1.0_WP
  ! do k=kmin_,kmax_
  !    do j=jmin_,jmax_
  !       do i=imin_,imax_

  !          ! Generate the distance from cylinder
  !          if (icyl.eq.0) then
  !             d = sqrt(ym(j)**2+zm(k)**2)
  !          else
  !             d = ym(j)
  !          end if
           
  !          ! Define the velocity
  !          if (     d.le.0.5_WP*Djet) then
  !             ! Inside the liquid jet
  !             U(:,j,k) = Ujet*erf((0.5_WP*Djet-d)/dl)
  !          else if (d.gt.0.5_WP*Djet.and.d.le.0.5_WP*Dcof) then
  !             ! Inside the lip
  !             U(:,j,k) = 0.0_WP
  !          else if (d.gt.0.5_WP*Dcof .and. d.le.0.5_WP*Dcof+hcof) then
  !             ! Inside the gas coflow
  !             U(:,j,k) = min(Ucof*erf((d-0.5_WP*Dcof)/dg),Ucof*erf((0.5_WP*Dcof+hcof-d)/dg) )
  !          else if (d.gt.0.5_WP*Dcof+hcof) then
  !             ! Outside injector - add a coflow for entrainment
  !             U(:,j,k) = Uout
  !          end if

  !          ! Distance to center of sphere
  !          if (icyl.eq.0) then
  !             d = sqrt(ym(j)**2+zm(k)**2+xm(i)**2)
  !          else
  !             d = sqrt(ym(j)**2+xm(i)**2)
  !          end if
           
  !          ! Check if close to interface           
  !          if (abs(d-0.5_WP*Djet).le.2.0_WP*dx) then
  !             ! Compute VOF close to the interface
  !             do s=1,8
  !                if (icyl.eq.0) then
  !                   ! Use finer mesh within the cartesian cell
  !                   VOF(i,j,k,s)=0.0_WP
  !                   do kk=1,nF
  !                      do jj=1,nF
  !                         do ii=1,nF
  !                            myx=xs1(s,i)+(real(ii,WP)-0.5_WP)/real(nf,WP)*(xs2(s,i)-xs1(s,i))
  !                            myy=ys1(s,j)+(real(jj,WP)-0.5_WP)/real(nf,WP)*(ys2(s,j)-ys1(s,j))
  !                            myz=zs1(s,k)+(real(kk,WP)-0.5_WP)/real(nf,WP)*(zs2(s,k)-zs1(s,k))
  !                            if (0.5_WP*Djet.gt.sqrt(myx**2+myy**2+myz**2)) then
  !                               VOF(i,j,k,s)=VOF(i,j,k,s)+1.0_WP
  !                            end if
  !                         end do
  !                      end do
  !                   end do
  !                   VOF(i,j,k,s)=VOF(i,j,k,s)/real(nF,WP)**3
  !                else
  !                   ! Use finer mesh within the cylindrical cell
  !                   VOF(s,i,j,k)=0.0_WP
  !                   do jj=1,nF
  !                      do ii=1,nF
  !                         myx=xs1(s,i)+(real(ii,WP)-0.5_WP)/real(nf,WP)*(xs2(s,i)-xs1(s,i))
  !                         myy=ys1(s,j)+(real(jj,WP)-0.5_WP)/real(nf,WP)*(ys2(s,j)-ys1(s,j))
  !                         if (0.5_WP*Djet.gt.sqrt(myx**2+myy**2)) then
  !                            VOF(i,j,k,s)=VOF(s,i,j,k)+1.0_WP
  !                         end if
  !                      end do
  !                   end do
  !                   VOF(i,j,k,s)=VOF(i,j,k,s)/real(nF,WP)**2
  !                end if
  !             end do
  !          else
  !             ! Set VOF directly
  !             if (d.gt.0.5_WP*Djet) then
  !                VOF(i,j,k,:)=0.0_WP
  !             else
  !                VOF(i,j,k,:)=1.0_WP
  !             end if
  !          end if
  !       end do
  !    end do
  ! end do

  ! ! Charge density
  ! !L_Q=Q

  ! ! Make velocity in gas=0
  ! do k=kmin_,kmax_
  !    do j=jmin_,jmax_
  !       do i=imin_,imax_
  !          ! Velocity
  !          U(i,j,k)=U(i,j,k)*0.125_WP*sum(VOF(i,j,k,:))
  !       end do
  !    end do
  ! end do
  
  return
end subroutine atom_angled_data
