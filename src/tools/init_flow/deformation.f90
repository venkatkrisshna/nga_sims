module deformation
  use string
  use precision
  use param
  implicit none
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:),   pointer :: U
  real(WP), dimension(:,:,:),   pointer :: V
  real(WP), dimension(:,:,:),   pointer :: W
  real(WP), dimension(:,:,:,:), pointer :: VOF
  
end module deformation

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine deformation_grid
  use deformation
  use parser
  implicit none
  
  integer :: i,j,k
  
  ! Read in the size of the domain
  call parser_read('Number of points',nx)
  ny = nx
  nz = 1
  
  ! Set the periodicity
  xper = 1
  yper = 1
  zper = 1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1) ,y(ny+1) ,z(nz+1))
  allocate(xm(nx+1),ym(ny+1),zm(nz+1))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)/real(nx,WP)
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)/real(nx,WP)
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)/real(nx,WP)
  end do
  
  ! Create the mid points
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine deformation_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine deformation_data
  use deformation
  use parser
  use math
  implicit none
  
  integer  :: s,i,j,k,ii,jj
  integer, parameter :: nF=100
  real(WP),parameter :: r=0.15_WP
  real(WP) :: d,myx,myy,dx
  
  ! Allocate the array data
  nvar = 11
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U      => data(:,:,:,1);  names(1)  = 'U'
  V      => data(:,:,:,2);  names(2)  = 'V'
  W      => data(:,:,:,3);  names(3)  = 'W'
  VOF  => data(:,:,:, 4:11);
  names( 4) = 'VOF1'
  names( 5) = 'VOF2'
  names( 6) = 'VOF3'
  names( 7) = 'VOF4'
  names( 8) = 'VOF5'
  names( 9) = 'VOF6'
  names(10) = 'VOF7'
  names(11) = 'VOF8'
  
  ! Set velocity
  do k=1,nz
     do j=1,ny
        do i=1,nx
           U(i,j,k) = -2.0_WP*sin(Pi*x(i))**2*sin(Pi*ym(j))*cos(Pi*ym(j))
           V(i,j,k) =  2.0_WP*sin(Pi*y(j))**2*sin(Pi*xm(i))*cos(Pi*xm(i))
           W(i,j,k) =  0.0_WP
        end do
     end do
  end do

  U=1.0_WP
  V=0.0_WP

  ! Set VOF field on fine mesh
  do k=1,nz
     do j=1,ny
        do i=1,nx
           d=sqrt((xm(i)-0.5_WP)**2+(ym(j)-0.75_WP)**2)
           dx=min(x(i+1)-x(i),y(j+1)-y(j))
           if (d.gt.r+2.0_WP*dx) then
              ! Outside drop
              VOF(i,j,k,:)=0.0_WP
           else if (d.lt.r-2.0_WP*dx) then
              ! Inside drop
              VOF(i,j,k,:)=1.0_WP
           else
              ! Close to interface
              do s=1,8 ! Loop over subcells
                 VOF(i,j,k,s)=0.0_WP
                 do jj=1,nF
                    do ii=1,nF
                       ! x,y position
                       myx=xs1(s,i)+(real(ii,WP)-0.5_WP)/real(nf,WP)*(xs2(s,i)-xs1(s,i))
                       myy=ys1(s,j)+(real(jj,WP)-0.5_WP)/real(nf,WP)*(ys2(s,j)-ys1(s,j))
                       d=sqrt((myx-0.5_WP)**2+(myy-0.75_WP)**2)
                       if (d.lt.r) then
                          VOF(i,j,k,s)=VOF(i,j,k,s)+1.0_WP
                       end if
                    end do
                 end do
                 VOF(i,j,k,s)=VOF(i,j,k,s)/real(nF,WP)**2
              end do
           end if
        end do
     end do
  end do

  return
end subroutine deformation_data
