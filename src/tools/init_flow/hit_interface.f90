module hit_interface
  use string
  use precision
  use param
  implicit none

  ! Length of the domain
  real(WP) :: Ly
  integer :: ny2
  real(WP), dimension(:), pointer :: y2

  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: VOF

  character(len=str_medium) :: filename1,filename2
  character(len=str_medium) :: config
  integer  :: iunit1,iunit2,ierr
  real(WP) :: dt,time
  integer  :: i,j,k,var

  ! Which type of config?
  logical :: double_height,gamma_rho
  real(WP) :: rhol,rhog


end module hit_interface

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine hit_interface_grid
  use hit_interface
  use string
  use fileio
  use parser
  use cli_reader
  use math
  use precision
  implicit none

  call parser_read('HIT config file', filename1)
  call parser_read('nx',nx)
  ny = nx
  nz = nx
  ! ** Open the config file to read **
  call BINARY_FILE_OPEN(iunit1,trim(filename1),"r",ierr)

  ! Read the header
  call BINARY_FILE_READ(iunit1,config,str_medium,kind(config),ierr)
  call BINARY_FILE_READ(iunit1,icyl,1,kind(icyl),ierr)
  call BINARY_FILE_READ(iunit1,xper,1,kind(xper),ierr)
  call BINARY_FILE_READ(iunit1,yper,1,kind(yper),ierr)
  call BINARY_FILE_READ(iunit1,zper,1,kind(zper),ierr)
  call BINARY_FILE_READ(iunit1,nx,1,kind(nx),ierr)
  call BINARY_FILE_READ(iunit1,ny,1,kind(ny),ierr)
  call BINARY_FILE_READ(iunit1,nz,1,kind(nz),ierr)

  ! Read the grid
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  call BINARY_FILE_READ(iunit1,x,nx+1,kind(x),ierr)
  call BINARY_FILE_READ(iunit1,y,ny+1,kind(y),ierr)
  call BINARY_FILE_READ(iunit1,z,nz+1,kind(z),ierr)
  call BINARY_FILE_READ(iunit1,mask,nx*ny,kind(mask),ierr)
  ! Close the config file
  call BINARY_FILE_CLOSE(iunit1,ierr)

  ! Create the mid points
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  Ly = y(ny+1)

  ! Double domain height if non-unity density ratio
  call parser_read('Liquid density',rhol)
  call parser_read('Gas density',   rhog)
  call parser_read('Double height',double_height,.false.)
  call parser_read('Density interface',gamma_rho,.false.)
  if (rhol.ne.rhog) yper=0
  if (gamma_rho)    yper=0
  if (double_height) then
     Ly = 2.0_WP*y(ny+1)
     ny2=2*ny
     deallocate(ym,y,mask); allocate(ym(ny2),y(ny2+1),mask(nx,ny2))
     do j=1,ny2+1
        y(j) = (j-1)*Ly/real(ny2,WP)
     end do
     do j=1,ny2
        ym(j) = 0.5_WP*(y(j)+y(j+1))
     end do
     mask=0
  end if

end subroutine hit_interface_grid

! ========================= !
! Create the variable array !
! ========================= !
subroutine hit_interface_data
  use hit_interface
  use precision
  use string
  use fileio
  use parser
  use cli_reader
  use math
  implicit none

  real(WP) :: G
  logical :: run_slab,run_blob

  ! ** Open the data file to read **
  call parser_read('HIT data file',filename2)
  call BINARY_FILE_OPEN(iunit2,trim(filename2),"r",ierr)
  
  ! Read the header
  call BINARY_FILE_READ(iunit2,nx,1,kind(nx),ierr)
  call BINARY_FILE_READ(iunit2,ny,1,kind(ny),ierr)
  call BINARY_FILE_READ(iunit2,nz,1,kind(nz),ierr)
  call BINARY_FILE_READ(iunit2,nvar,1,kind(nvar),ierr)
  call BINARY_FILE_READ(iunit2,dt,1,kind(dt),ierr)
  call BINARY_FILE_READ(iunit2,time,1,kind(time),ierr)
  
  ! Read variable names
  allocate(names(nvar))
  do var=1,nvar
     call BINARY_FILE_READ(iunit2,names(var),str_short,kind(names),ierr)
  end do
  
  ! Allocate arrays
  if (.not.double_height) then
     allocate(data(nx,nx,nx,nvar))
     U => data(:,:,:,1); 
     V => data(:,:,:,2); 
     W => data(:,:,:,3); 
     P => data(:,:,:,4);
     VOF => data(:,:,:,5);
     ! Read data field
     do var=1,nvar
        ! Read 
        call BINARY_FILE_READ(iunit2,data(:,:,:,var),nx*ny*nz,kind(data),ierr)
     end do
  else
     allocate(data(nx,ny2,nz,nvar))
     U => data(:,:,:,1); 
     V => data(:,:,:,2); 
     W => data(:,:,:,3); 
     P => data(:,:,:,4);
     VOF => data(:,:,:,5);
     ! Read data field
     do var=1,nvar
        ! Read 
        call BINARY_FILE_READ(iunit2,data(:,ny+1:ny2,:,var),nx*ny*nz,kind(data),ierr)
     end do
     ny=ny2
     data(:,1:ny/2,:,:)=data(:,ny/2+1:ny,:,:)
  end if
  
  ! Close the data file
  call BINARY_FILE_CLOSE(iunit2,ierr)

  VOF=0.0_WP
  call parser_read('Initialize slab',run_slab,.false.)
  call parser_read('Initialize blob',run_blob,.false.)
  if (run_slab) then

     VOF(:,ny/4:3*ny/4,:) = 1.0_WP

  else if (run_blob) then
     
     call compute_blob(VOF)

  else
     
     VOF(:,1:ny/2,:) = 1.0_WP     
     ! Set velocity in lower half to zero
     if (yper.eq.0) then
        U(:,1:ny/2  ,:) = 0.0_WP
        V(:,1:ny/2+1,:) = 0.0_WP
        W(:,1:ny/2  ,:) = 0.0_WP
        P(:,1:ny/2  ,:) = 0.0_WP
     end if
     
  endif

  print*,'periodicity:',xper,yper,zper
  
  return
end subroutine hit_interface_data


subroutine compute_blob(vol_frac)
  use hit_interface
  use parser
  use random
  use compgeom
  implicit none
  include 'fftw3.f'

  real(WP), dimension(nx,ny,nz), intent(inout) :: vol_frac

  ! Spectrum computation
  real(WP) :: ke,kd,ks,dk,ksk0ratio,kc,kcksratio,kk,kx,ky,kz,kk2
  integer  :: nk

  ! Other
  integer :: ik,iunit,dim
  complex(WP) :: ii=(0.0_WP,1.0_WP)
  real(WP) :: rand!,pi

  ! Fourier coefficients
  integer(KIND=8) :: plan_r2c,plan_c2r
  complex(WP), dimension(:,:,:), pointer :: Cbuf
  real(WP), dimension(:,:,:), pointer :: Rbuf,work
  real(WP) :: f_phi
  
  real(WP), dimension(3,8) :: pos
  real(WP), dimension(  8) :: val
  real(WP), dimension(  3) :: com
  real(WP), dimension(  3) :: coms
  real(WP), dimension(  3) :: nvec
  real(WP) :: my_surf

  ! Create important stuff
  !pi = acos(-1.0_WP)
  dk = 2.0_WP*pi/Ly

  ! Initialize the random number generator
  call random_init

  ! Initialize in similar manner to Eswaran and Pope 1988
  call parser_read('ks/ko',ksk0ratio)
  ks = ksk0ratio*dk
  call parser_read('kc/ks',kcksratio)
  kc = kcksratio*ks

  ! Inverse Fourier transform
  nk = nx/2+1
  allocate(Cbuf(nk,ny,nz))
  allocate(Rbuf(nx,ny,nz))
  call dfftw_plan_dft_c2r_3d(plan_c2r,nx,ny,nz,Cbuf,Rbuf,FFTW_ESTIMATE)
  call dfftw_plan_dft_r2c_3d(plan_r2c,nx,ny,nz,Rbuf,Cbuf,FFTW_ESTIMATE)

  ! Compute the Fourier coefficients
  do k=1,nz
     do j=1,ny
        do i=1,nk

           ! Wavenumbers
           kx=real(i-1,WP)*dk
           ky=real(j-1,WP)*dk
           if (j.gt.nk) ky=-real(nx+1-j,WP)*dk
           kz=real(k-1,WP)*dk
           if (k.gt.nk) kz=-real(nx+1-k,WP)*dk
           kk =sqrt(kx**2+ky**2+kz**2)
           kk2=sqrt(kx**2+ky**2)

           ! Compute the Fourier coefficients
           if ((ks-dk/2.0_WP.le.kk).and.(kk.le.ks+dk/2.0_WP)) then
              f_phi = 1.0_WP
           else
              f_phi = 0.0_WP
           end if
           call random_number(rand)
           if (kk.lt.1e-10) then
              Cbuf(i,j,k) = 0.0_WP
           else
              Cbuf(i,j,k) = sqrt(f_phi/(4.0_WP*pi*kk**2))*exp(ii*2.0_WP*pi*rand)
           end if

        end do
     end do
  end do

  ! Oddball
  do k=2,nz
     do j=nk+1,ny
        Cbuf(1,j,k)=conjg(Cbuf(1,ny+2-j,nz+2-k))
     end do
  end do
  do k=nk+1,nz
     Cbuf(1,1,k)=conjg(Cbuf(1,1,nz+2-k))
  end do

  ! Inverse Fourier transform
  call dfftw_execute(plan_c2r)

  ! Force 'double-delta' pdf on scalar field
  do k=1,nz
     do j=1,ny
        do i=1,nx
           if (Rbuf(i,j,k).le.0.0_WP) then
              Rbuf(i,j,k) = 0.0_WP
           else
              Rbuf(i,j,k) = 1.0_WP
           end if
        end do
     end do
  end do

  ! Fourier Transform and filter to smooth
  call dfftw_execute(plan_r2c)
  
  do k=1,nz
     do j=1,ny
        do i=1,nk
           ! Wavenumbers
           kx=real(i-1,WP)*dk
           ky=real(j-1,WP)*dk
           if (j.gt.nk) ky=-real(nx+1-j,WP)*dk
           kz=real(k-1,WP)*dk
           if (k.gt.nk) kz=-real(nx+1-k,WP)*dk
           kk =sqrt(kx**2+ky**2+kz**2)
           kk2=sqrt(kx**2+ky**2)

           ! Filter to remove high wavenumber components
           if (kk.le.kc) then
              Cbuf(i,j,k) = Cbuf(i,j,k) * 1.0_WP
           else
              Cbuf(i,j,k) = Cbuf(i,j,k) * (kc/kk)**2
           end if

        end do
     end do
  end do

  ! Oddball
  do k=2,nz
     do j=nk+1,ny
        Cbuf(1,j,k)=conjg(Cbuf(1,ny+2-j,nz+2-k))
     end do
  end do
  do k=nk+1,nz
     Cbuf(1,1,k)=conjg(Cbuf(1,1,nz+2-k))
  end do

  ! Fourier Transform back to real
  call dfftw_execute(plan_c2r)
  
  ! Destroy the plans
  call dfftw_destroy_plan(plan_c2r)
  call dfftw_destroy_plan(plan_r2c)
  
  ! Extract 0.5 isosurface for VOF initialization
  allocate(work(1:nx+1,1:ny+1,1:nz+1))
  work(1:nx,1:ny,1:nz)=Rbuf
  work(nx+1,:,:)=work(1,:,:)
  work(:,ny+1,:)=work(:,1,:)
  work(:,:,nz+1)=work(:,:,1)
  do k=1,nz
     do j=1,ny
        do i=1,nx
           pos=reshape((/ &
                x(i  ),y(j  ),z(k  ), &
                x(i+1),y(j  ),z(k  ), &
                x(i  ),y(j+1),z(k  ), &
                x(i+1),y(j+1),z(k  ), &
                x(i  ),y(j  ),z(k+1), &
                x(i+1),y(j  ),z(k+1), &
                x(i  ),y(j+1),z(k+1), &
                x(i+1),y(j+1),z(k+1) /),(/3,8/))
           val=(/work(i  ,j  ,k  ), &
                work(i+1,j  ,k  ), &
                work(i  ,j+1,k  ), &
                work(i+1,j+1,k  ), &
                work(i  ,j  ,k+1), &
                work(i+1,j  ,k+1), &
                work(i  ,j+1,k+1), &
                work(i+1,j+1,k+1)/)
           call cut_3cube(0.5_WP,pos,val,vol_frac(i,j,k),com,my_surf,coms,nvec)
           vol_frac(i,j,k)=vol_frac(i,j,k)/((x(i+1)-x(i))*(y(j+1)-y(j))*(z(k+1)-z(k)))
        end do
     end do
  end do
 
  return
end subroutine compute_blob


