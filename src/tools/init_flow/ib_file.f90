module ib_file
  use precision
  use param
  use fileio
  implicit none

  ! Box dimensions
  real(WP) :: Lx,Ly,Lz

  ! Particle diameter
  real(WP) :: diam
  
  ! initial Gfield
  real(WP) :: G_rad,G_rad1,x0,y0,z0,G_rad_motion,G_rad_z
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: G
  real(WP), dimension(:,:,:), pointer :: S

end module ib_file


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine ib_file_grid
  use ib_file
  use parser
  use math
  implicit none

  integer :: i,j,k
  real(WP):: dx,dy,dz,Lp

  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)

  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  call parser_read('Lz',Lz)

  call parser_read('xper',xper,1)
  call parser_read('yper',yper,1)
  call parser_read('zper',zper,1)

  ! Cartesian
  icyl = 0

  ! Read ib_file parameters
  call parser_read('IB rad0',G_rad,0.0_WP)
  call parser_read('IB rad1',G_rad1,0.0_WP)
  call parser_read('IB radius motion',G_rad_motion,0.0_WP)
  call parser_read('IB radius motionz',G_rad_z,0.0_WP)

  call parser_read('Initial center x', x0,0.5_WP*Lx)
  if(nx.eq.1) x0=0.0_WP
  call parser_read('Initial center y', y0,0.5_WP*Ly)
  if(ny.eq.1) y0=0.0_WP
  call parser_read('Initial center z', z0,0.0_WP*Lz)
  if(nz.eq.1) z0=0.0_WP  

  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))

  ! Mesh
  dx = Lx/real(nx,WP)
  dy = Ly/real(ny,WP)
  dz = Lz/real(nz,WP)

  if(nx.eq.1) then
     do i=0,nx
        x(i+1)=dx*real(i,WP)-0.5_WP*Lx
     end do
  else
     do i=0,nx
        x(i+1)=dx*real(i,WP)
     end do
  end if
  if(ny.eq.1) then
     do j=0,ny
        y(j+1)=dy*real(j,WP)-0.5_WP*Ly
     end do
  else
     do j=0,ny
        y(j+1)=dy*real(j,WP)
     end do
  end if
  if(nz.eq.1) then
     do k=0,nz
        z(k+1)=dz*real(k,WP)-0.5_WP*Lz
     end do
  else
     do k=0,nz
        z(k+1)=dz*real(k,WP)
     end do
  end if

  ! Create the mid points 
  do i=1,nx
     xm(i)= 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)= 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do

  ! Create the masks
  mask = 0
  if(xper.ne.1) then
     mask(1,:)=1;mask(nx,:)=1
  end if
  if(yper.ne.1) then
     mask(:,1)=1;mask(:,ny)=1
  end if


  return
end subroutine ib_file_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine ib_file_data
  use ib_file
  use parser
  use ib_velocity
  implicit none

  integer :: i,j,k

  ! Allocate the array data
  nvar = 4
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  ! Link the pointers
  U => data(:,:,:,1); names(1) = 'U'
  V => data(:,:,:,2); names(2) = 'V'
  W => data(:,:,:,3); names(3) = 'W'
  P => data(:,:,:,4); names(4) = 'P'

  ! Gib opt data
  nod = 1
  allocate(OD(nx,ny,nz,nod))
  allocate(OD_names(nod))
  G => OD(:,:,:,1); OD_names(1) = 'Gib'

  ! Create them
  U=0.0_WP; V=0.0_WP; W=0.0_WP; P = 0.0_WP
  
  G_center(1) = x0; G_center(2) = y0; G_center(3) = z0
  G_radius = G_rad; G_radius1 = G_rad1 
  G_radius_motion = G_rad_motion; G_radius_motion_z = G_rad_z

  do k=1,nz
     do j=1,ny
        do i=1,nx
           G(i,j,k) = ibfile_velocity_Gib((/xm(i),ym(j),zm(k)/),0.0_WP)
        end do
     end do
  end do

  return
end subroutine ib_file_data
