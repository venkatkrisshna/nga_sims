module jetcyl
  use precision
  use param
  implicit none
  
  ! Length and diameter of the domain
  real(WP) :: L,diam
  ! Length and diameter of the pipe
  real(WP) :: pipe_L,pipe_diam
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P

end module jetcyl

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine jetcyl_grid
  use jetcyl
  use parser
  implicit none

  integer :: i,j,k
  real(WP) :: Pi2

  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)

  call parser_read('Full length',L)
  call parser_read('Full diameter',diam)

  call parser_read('Pipe length',pipe_L)
  call parser_read('Pipe diameter',pipe_diam)

  ! Set the periodicity
  xper = 0
  yper = 0
  zper = 1

  ! Cylindrical
  icyl = 1

  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))

  ! Create the grid
  do i=1,nx+1
     x(i) = (i-1)*L/nx
  end do
  do j=1,ny+1
     y(j) = (j-1)*diam/(2.0_WP*ny)
  end do
  Pi2 = 2.0_WP*acos(-1.0_WP)
  do k=1,nz+1
     z(k) = (k-1)*Pi2/nz
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do

  ! Create the mid points
  do i=1,nx
     xm(i)= 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)= 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do

  ! Create the masks
  mask = 0
  do i=1,nx-1
     do j=1,ny
        if ((2.0_WP*y(j).ge.pipe_diam).and.(x(i+1).le.pipe_L)) then
           mask(i,j) = 1
        end if
     end do
  end do
  mask(nx,:) = mask(nx-1,:)
  
  return
end subroutine jetcyl_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine jetcyl_data
  use jetcyl
  implicit none

  ! Allocate the array data
  nvar = 4
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U => data(:,:,:,1); names(1) = 'U'
  V => data(:,:,:,2); names(2) = 'V'
  W => data(:,:,:,3); names(3) = 'W'
  P => data(:,:,:,4); names(4) = 'P'
  
  ! Create them
  U = 0.0_WP
  V = 0.0_WP
  W = 0.0_WP
  P = 0.0_WP
  
  return
end subroutine jetcyl_data

