module kelvin_helmholtz
  use string
  use precision
  use param
  implicit none

  ! Domain
  real(WP) :: Lx,Ly,Lz
  real(WP) :: dx,dy,dz

  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: VOF

  ! Fluctuations
  real(WP) :: epsi ! Fluctuation amplitude
  real(WP), dimension(:,:), pointer :: U_fluct 
  real(WP), dimension(:,:), pointer :: V_fluct 
  real(WP), dimension(:,:), pointer :: W_fluct 

  ! Flow Parameters
  real(WP) :: Re_l, Re_g, We_l, We_g, S, F, alpha
  real(WP) :: U_l, U_g
  real(WP) :: m, r
  real(WP) :: delta_l, delta_g

  contains 
    ! ========================================== !
    !  Function to evaluate a Chebychev          !
    !  polynomial represented by m coefficients  !
    !  (c) on a domain [a,b] at location x       !
    ! ========================================== !
    function chebev(a,b,c,m,x) 
      implicit none
      ! Inputs
      integer  :: m
      real(WP) :: a,b,x,c(m)
      ! Variables
      integer  :: j
      real(WP) :: d,dd,sv,y,y2
      ! Output
      real(WP) :: chebev

      d=0.0_WP; dd=0.0_WP
      y=(2.0_WP*x-a-b)/(b-a)
      y2=2.0_WP*y
      do j=m,2,-1 ! Clenshaw's Reccurence
         sv=d
         d=y2*d-dd+c(j)
         dd=sv
      end do
      chebev=y*d-dd+c(1)

      return
    end function chebev

end module kelvin_helmholtz

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine kelvin_helmholtz_grid
  use kelvin_helmholtz
  use parser
  use math
  implicit none
  integer :: i,j,k

  ! Cartesian or cylindrical
  icyl=0

  ! Periodicity
  xper = 1
  yper = 0
  zper = 1

  ! Read in the size of the domain
  call parser_read('nx',nx)
  ny=nx
  nz=1
  call parser_read('Wavenumber',alpha)
  Lx=2.0_WP*Pi/alpha
  dx = Lx/real(nx,WP)
  Ly=Lx
  dy = Ly/real(ny,WP)
  Lz=dx
  dz=dx

  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))

  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*dx
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*dy-0.5_WP*Ly
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*dz
  end do

  ! Create the cell centered grid
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do

  ! Create the masks
  mask = 0

  ! mask(:, 1)  = 1
  ! mask(:,ny) = 1

  return
end subroutine kelvin_helmholtz_grid

! ========================= !
! Create the variable array !
! ========================= !
subroutine kelvin_helmholtz_data
  use kelvin_helmholtz
  use parser
  use math
  use string
  implicit none

  real(WP) :: mu_l,mu_g,rho_l,rho_g,sigma
  integer :: i,j

  ! Allocate the array data
  nvar = 4
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))

  ! Link the pointers
  U => data(:,:,:,1); names(1) = 'U'
  V => data(:,:,:,2); names(2) = 'V'
  W => data(:,:,:,3); names(3) = 'W'
  VOF => data(:,:,:,4); names(4) = 'VOF'

  ! Prepare the profiles
  call parser_read('U gas',U_g)
  call parser_read('BL Thickness',delta_g)
  call parser_read('Fluct. amp',epsi)
  call parser_read('Liquid density',rho_l)
  call parser_read('Gas density',rho_g)
  call parser_read('Liquid viscosity',mu_l)
  call parser_read('Gas viscosity',mu_g)
  call parser_read('Surface tension',sigma)

  ! Flow Parameters
  delta_l=delta_g
  U_l=U_g * mu_g/mu_l * delta_l/delta_g
  m=mu_g/mu_l
  r=rho_g/rho_l
  Re_l=rho_l*U_g*delta_g/mu_l
  Re_g=rho_g*U_g*delta_g/mu_g
  We_l=min(huge(1.0_WP),rho_l*U_g**2*delta_g/sigma)
  We_g=min(huge(1.0_WP),rho_g*U_g**2*delta_g/sigma)
  S=sigma/(rho_l*U_g*delta_g)
  F=0.0_WP*(rho_l-rho_g)*delta_g/(rho_l*U_g**2)

  ! Print parameters
  write(*,'(A,ES20.10)') ' m=    ',m
  write(*,'(A,ES20.10)') ' r=    ',r
  write(*,'(A,ES20.10)') ' Re_l= ',Re_l
  write(*,'(A,ES20.10)') ' Re_g= ',Re_g
  write(*,'(A,ES20.10)') ' We_l= ',We_l
  write(*,'(A,ES20.10)') ' We_g= ',We_g
  write(*,'(A,ES20.10)') ' L=    ',Ly*0.5_WP
  write(*,'(A,2ES20.10)')' dl,dg=',delta_l,delta_g
  write(*,'(A,2ES20.10)')' Ul,Ug=',U_l,U_g
  write(*,'(A,ES20.10)') ' alpha=',alpha
  write(*,'(A,ES20.10)') ' S    =',S
  write(*,'(A,ES20.10)') ' F    =',F


  ! Mean ===================================
  do j=1,ny
     if (ym(j).gt.0.0_WP) then
        U(1,j,1)=U_g*erf(ym(j)/delta_g)
     else
        U(1,j,1)=U_l*erf(ym(j)/delta_l)
     end if
  end do
  do i=2,nx
     U(i,:,1)=U(1,:,1)
  end do
  V = 0.0_WP
  W = 0.0_WP

  ! Fluctuations ============================
  ! Allocate arrays
  allocate(U_fluct(nx,ny))
  allocate(V_fluct(nx,ny))
  allocate(W_fluct(nx,ny))

  ! Caluculate fluctations & compute VOF
  call orr_sommerfeld

  ! Update velocity
  do i=1,nx
     do j=1,ny
        U  (i,j,:) = U  (i,j,:) +   U_fluct(i,j)
        V  (i,j,:) = V  (i,j,:) +   V_fluct(i,j)
        W  (i,j,:) = W  (i,j,:) +   W_fluct(i,j)
     end do
  end do

  return
end subroutine kelvin_helmholtz_data

! ================================================ !
! Calc. fluctuations using Orr-Sommerfeld equation !
! Ref: Boomkamp 1997, Chebyshev Collocation Method !
!  for Solving Two-Phase Flow Stability Problems   !
! ================================================ !
subroutine orr_sommerfeld
  use kelvin_helmholtz
  use math
  use parser
  use fileio
  implicit none

  ! Flow Field stats
  real(WP), dimension(:), pointer :: Ubase_l, Ubase_g   ! [m/s]
  real(WP), dimension(:), pointer :: Up_l,    Up_g      ! [1/s]
  real(WP), dimension(:), pointer :: Upp_l,   Upp_g     ! [1/m*s]

  ! Temporal Orr Sommerfeld
  integer :: nos
  real(WP), dimension(:), pointer :: zos, yos_l, yos_g
  real(WP),    dimension(:,:), pointer :: D1_g,D2_g,D3_g,D4_g
  real(WP),    dimension(:,:), pointer :: D1_l,D2_l,D3_l,D4_l
  real(WP),    dimension(:,:), pointer :: Id, T,Gos,Tp,tt
  complex(WP), dimension(:,:), pointer :: A,B,C
  complex(WP), dimension(:,:), pointer :: Ain,Bin
  complex(WP), dimension(:),   pointer :: phi_l, phi_g
  real(WP),    dimension(:),   pointer :: weight

  ! Orr-Sommerfeld - lapack solver
  integer :: ierr,lwork
  integer,     dimension(:),   pointer :: iwork
  complex(WP), dimension(:),   pointer :: work
  real(WP),    dimension(:),   pointer :: rwork
  complex(WP), dimension(:),   pointer :: eigval_a,eigval_b  ! a/b = lambda = eigenvalue
  complex(WP), dimension(:,:), pointer :: eigvec_l           ! left eigenvector
  complex(WP), dimension(:,:), pointer :: eigvec_r           ! right eigenvector

  ! Complex constants
  complex(WP), parameter :: zero = (0.0_WP,0.0_WP)
  complex(WP), parameter :: one  = (1.0_WP,0.0_WP)
  complex(WP), parameter :: ii   = (0.0_WP,1.0_WP)

  ! Fluctuation calculation
  real(WP) :: phi_i, phi_r
  real(WP), dimension(:), pointer :: wreal_l, wimag_l, wreal_g, wimag_g

  ! Various
  integer  :: i,j
  integer  :: iliq_min, iliq_max
  integer  :: igas_min, igas_max
  integer  :: igas, iliq
  integer  :: iunit,iunit2
  real(WP) :: stretch, fac, power
  real(WP), dimension(:), pointer :: test
  character(1000)      :: plot_string 
  complex(WP)         :: residual
  real(WP)            :: residual_real,residual_imag
  complex(WP), dimension(:),   pointer :: sol
  character(300) :: simulation
  complex(WP) :: omega      
  real(WP) :: speed,speed_,Umin,Umax,tmp
  integer  :: jj,mode_index
  complex(WP) :: omega_
  real(WP) :: growthrate
  real(WP) :: I1,I2,area,xl,xr
  integer  :: case1,case2

  ! Read number of grid points on each side of interface
  call parser_read('Orr-Sommerfeld points',nos)
  ! stretch=1.5_WP*delta_g
  ! fac=0.001_WP
  call parser_read('Stretch',stretch)
  ! print *, 'Stretch=',stretch
  stretch=stretch*delta_g
  fac=0.001_WP

  simulation='mixing_layer'
  !simulation='poiseuille'; stretch=1.0_WP
  

  ! Create grids
  !
  !        ^ y=y_max    ^ z(nos)=-1     ^ yos_g(nos)=y_max
  !        |            |               |
  !        |            |               |
  !        |            v z(1)=1        v yos_g(1)=0
  !   -----|----------------------------------------------Interface---
  !        |            ^ z(nos)=-1     ^ yos_l(nos)=0
  !        |            |               |
  !        |            |               |
  !        v y=y_min    v z(1)=1        v yos_l(1)=y_min
  !
  
  allocate(zos  (nos))
  allocate(yos_l(nos))
  allocate(yos_g(nos))
  do j=1,nos
     zos(j) = cos(Pi*real(j-1,WP)/real(nos-1,WP))
  end do
  do j=1,nos
     ! Liquid
     yos_l(j)=stretch*(-zos(j)-1.0_WP)*0.5_WP
     !yos_l(j)=delta_g*6.0_WP*(-zos(j)-1.0_WP)*0.5_WP
     ! yos_l(j)=stretch*( log((1.0_WP+(-zos(j)*0.5_WP-0.5_WP) + fac) &
     !                       /(1.0_WP-(-zos(j)*0.5_WP-0.5_WP)) )     &
     !                       - log((1.0_WP+fac)/(1.0_WP)) )
        !yos_l(j)=-1.0_WP/(-fac*zos(j)+1.0_WP)**power+1.0_WP/(1.0_WP+fac)**power
     ! Gas
     yos_g(j)=stretch*(-zos(j)+1.0_WP)*0.5_WP
     !yos_g(j)=delta_g*6.0_WP*(-zos(j)+1.0_WP)*0.5_WP
     ! yos_g(j)=stretch*( log((1.0_WP+(-zos(j)*0.5_WP+0.5_WP))        &
     !                       /(1.0_WP-(-zos(j)*0.5_WP+0.5_WP) + fac)) &
     !                       - log((1.0_WP)/(1.0_WP+fac)) )
     !yos_g(j)=1.0_WP/(fac*zos(j)+1.0_WP)**power-1.0_WP/(1.0_WP+fac)**power
  end do

  ! Orr-Sommerfeld - lapack solver
  allocate(work(4*(nos-1)))
  allocate(rwork(16*(nos-1)))
  allocate(eigval_a(2*(nos-1)))
  allocate(eigval_b(2*(nos-1)))
  allocate(eigvec_l(1,2*(nos-1)))
  allocate(eigvec_r(2*(nos-1),2*(nos-1)))
  allocate(iwork(2*(nos-1)))

  ! Solution from Orr-Sommerfeld
  allocate(phi_l(nos))
  allocate(phi_g(nos))
  allocate(weight(nos))

  ! Setup eigenvalue problem
  allocate(Ain(2*(nos-1),2*(nos-1)))
  allocate(Bin(2*(nos-1),2*(nos-1)))
  allocate(A(2*nos,2*nos))
  allocate(B(2*nos,2*nos))
  allocate(C(2*nos,2*nos))

  ! Precompute the stencil for derivations
  allocate(Id(nos,nos))
  allocate(D1_g(nos,nos))
  allocate(D2_g(nos,nos))
  allocate(D3_g(nos,nos))
  allocate(D4_g(nos,nos))
  allocate(D1_l(nos,nos))
  allocate(D2_l(nos,nos))
  allocate(D3_l(nos,nos))
  allocate(D4_l(nos,nos))

  ! Chebyshev matrices
  allocate(T  (nos,nos))
  allocate(Gos(nos,nos))
  allocate(Tp (nos,nos))
  allocate(tt(2*nos,2*nos))

  Id = 0.0_WP
  D1_l=0.0_WP; D1_g = 0.0_WP
  D2_l=0.0_WP; D2_g = 0.0_WP
  D3_l=0.0_WP; D3_g = 0.0_WP
  D4_l=0.0_WP; D4_g = 0.0_WP

  do i=1,nos

     ! Identity matrix
     Id(i,i) = 1.0_WP

     do j=1,nos
        ! Chebyshev to real
        T(i,j) = cos(Pi*real((i-1)*(j-1),WP)/real(nos-1,WP))

        ! Real to Chebyshev
        Tp(i,j) = 2.0_WP * cos(Pi*real((i-1)*(j-1),WP)/real(nos-1,WP)) / real(nos-1,WP)
        if (i.eq.1 .or. i.eq.nos) Tp(i,j) = 0.5_WP * Tp(i,j)
        if (j.eq.1 .or. j.eq.nos) Tp(i,j) = 0.5_WP * Tp(i,j)

        ! Derivative in Chebyshev space (d*/dz)
        if (i.ge.j .or. mod(i+j,2).eq.0) then
           Gos(i,j) = 0.0_WP
        else
           if (i.eq.1 .or. i.eq.nos) then
              Gos(i,j) = real(j-1,WP)
           else
              Gos(i,j) = 2.0_WP*real(j-1,WP)
           end if
        end if

     end do
  end do

  ! Derivative in real space (d*/dy)
  call dgemm('N','N',nos,nos,nos,1.0_WP,T,nos,Gos,nos,0.0_WP,D1_g,nos)
  Gos = D1_g
  call dgemm('N','N',nos,nos,nos,1.0_WP,Gos,nos,Tp,nos,0.0_WP,D1_g,nos)
  D1_l=D1_g

  ! Account for the mapping
  do i=1,nos
     D1_l(i,:) = D1_l(i,:) / (-stretch*0.5_WP)
     D1_g(i,:) = D1_g(i,:) / (-stretch*0.5_WP)
     
     ! D1_l(i,:) = D1_l(i,:) / (-3.0_WP*delta_g)
     ! D1_g(i,:) = D1_g(i,:) / (-3.0_WP*delta_g)

     ! D1_l(i,:) = D1_l(i,:) * -1.0_WP*(6.0_WP*fac - 2.0_WP*zos(i) + 2.0_WP*fac*zos(i) - zos(i)**2 + 3.0_WP) &
     !    /(4.0_WP*stretch + 2.0_WP*fac*stretch)
     ! D1_g(i,:) = D1_g(i,:) * -1.0_WP*(6.0_WP*fac + 2.0_WP*zos(i) - 2.0_WP*fac*zos(i) - zos(i)**2 + 3.0_WP) &
     !    /(4.0_WP*stretch + 2.0_WP*fac*stretch)

     ! D1_l(i,:) = D1_l(i,:) * -(-1.0_WP/(yos_l(i) - 1.0_WP/(fac + 1.0_WP)**power))**(1.0_WP/power - 1.0_WP) &
     !      /(fac*power*(yos_l(i) - 1.0_WP/(fac + 1.0_WP)**power)**2)
     ! D1_g(i,:) = D1_g(i,:) * -(1.0_WP/(yos_g(i) + 1.0_WP/(fac + 1.0_WP)**power))**(1.0_WP/power - 1.0_WP) &
     !      /(fac*power*(yos_g(i) + 1.0_WP/(fac + 1.0_WP)**power)**2)
  end do

  ! Higher order derivatives
  call dgemm('N','N',nos,nos,nos,1.0_WP,D1_g,nos,D1_g,nos,0.0_WP,D2_g,nos)
  call dgemm('N','N',nos,nos,nos,1.0_WP,D1_l,nos,D1_l,nos,0.0_WP,D2_l,nos)
  call dgemm('N','N',nos,nos,nos,1.0_WP,D1_g,nos,D2_g,nos,0.0_WP,D3_g,nos)
  call dgemm('N','N',nos,nos,nos,1.0_WP,D1_l,nos,D2_l,nos,0.0_WP,D3_l,nos)
  call dgemm('N','N',nos,nos,nos,1.0_WP,D2_g,nos,D2_g,nos,0.0_WP,D4_g,nos)
  call dgemm('N','N',nos,nos,nos,1.0_WP,D2_l,nos,D2_l,nos,0.0_WP,D4_l,nos)

  ! Create base flow
  allocate(Ubase_l(nos))
  allocate(Up_l   (nos))
  allocate(Upp_l  (nos))
  allocate(Ubase_g(nos))
  allocate(Up_g   (nos))
  allocate(Upp_g  (nos))
  do j=1,nos
     Ubase_l(j) = U_l*erf(yos_l(j)/delta_l)
     Ubase_g(j) = U_g*erf(yos_g(j)/delta_g)
     Up_l   (j) = (2.0_WP*U_l)/(sqrt(pi)*delta_l*exp(yos_l(j)**2/delta_l**2))
     Up_g   (j) = (2.0_WP*U_g)/(sqrt(pi)*delta_g*exp(yos_g(j)**2/delta_g**2))
     Upp_l  (j) = -1.0_WP*(4.0_WP*U_l*yos_l(j))/(sqrt(pi)*delta_l**3*exp(yos_l(j)**2/delta_l**2))
     Upp_g  (j) = -1.0_WP*(4.0_WP*U_g*yos_g(j))/(sqrt(pi)*delta_g**3*exp(yos_g(j)**2/delta_g**2))
  end do

  ! Save the mean flow to file
  iunit = iopen()
  open(iunit,file='mean.txt')
  do j=1,nos
     write(iunit,'(i3,5e16.6E3)') j,yos_l(j),zos(j),Ubase_l(j),Up_l(j),Upp_l(j)
  end do
  do j=1,nos
     write(iunit,'(i3,5e16.6E3)') j,yos_g(j),zos(j),Ubase_g(j),Up_g(j),Upp_g(j)
  end do
  close(iclose(iunit))

  ! Test derivative opperators by differentiating 'test'
  allocate(test(2*nos))
  allocate(sol (2*nos))
  do j=1,nos
     test(j    )=Ubase_l(j) !tanh(yos_l(j)/delta_g)
     test(j+nos)=Ubase_g(j) !tanh(yos_g(j)/delta_g)
  end do
  iunit = iopen()
  open(iunit,file='test_function.txt')
  do j=1,nos
     write(iunit,'(7e16.6E3)') yos_l(j), zos(j), &
          sum(Id  (j,:)*test(1:nos)), &
          sum(D1_l(j,:)*test(1:nos)), &
          sum(D2_l(j,:)*test(1:nos)), &
          sum(D3_l(j,:)*test(1:nos)), &
          sum(D4_l(j,:)*test(1:nos))
  end do
  do j=1,nos
     write(iunit,'(7e16.6E3)') yos_g(j), zos(j), &
          sum(Id  (j,:)*test(nos+1:2*nos)), &
          sum(D1_g(j,:)*test(nos+1:2*nos)), &
          sum(D2_g(j,:)*test(nos+1:2*nos)), &
          sum(D3_g(j,:)*test(nos+1:2*nos)), &
          sum(D4_g(j,:)*test(nos+1:2*nos))
  end do
  close(iclose(iunit))

  A = zero
  B = zero

  iliq_min=1;     iliq_max=nos
  igas_min=nos+1; igas_max=2*nos

  ! Orr-Sommerfeld Equations =========================

  ! Interior points: Liquid
  do j=3,nos-2
     A(j,iliq_min:iliq_max) = &
           + D4_l(j,:)-2.0_WP*alpha**2*D2_l(j,:)+alpha**4*Id(j,:) &
           + ii*( alpha*Re_l*Upp_l(j)*Id(j,:) &
           - alpha*Re_l*Ubase_l(j)*(D2_l(j,:)-alpha**2*Id(j,:)))
     B(j,iliq_min:iliq_max) = ii*(-1.0_WP*alpha*Re_l*(D2_l(j,:)-alpha**2*Id(j,:)))
  end do

  ! Interior points: Gas
  do j=3,nos-2
     A(j+nos,igas_min:igas_max) = &
          + D4_g(j,:)-2.0_WP*alpha**2*D2_g(j,:)+alpha**4*Id(j,:) &
          + ii*( alpha*Re_l*(r/m)*Upp_g(j)*Id(j,:) &
          - alpha*Re_l*(r/m)*Ubase_g(j)*(D2_g(j,:)-alpha**2*Id(j,:)))
     B(j+nos,igas_min:igas_max) = ii*(-1.0_WP*alpha*Re_l*(r/m)*(D2_g(j,:)-alpha**2*Id(j,:)))
  end do

  ! Four Boundary Conditions =========================
  igas=nos; iliq=1

  ! phi=0 at top and bottom
  !A(1    ,iliq_min:iliq_max) = Id(iliq,:)
  !A(2*nos,igas_min:igas_max) = Id(igas,:)
  ! Applied by removing 1st and last column/row from A and B below

  ! phi'=0 at top and bottom
  A(2      ,iliq_min:iliq_max) = D1_l(iliq,:)
  A(2*nos-1,igas_min:igas_max) = D1_g(igas,:)

  ! Four Interface (match) Conditions =========================
  igas=1; iliq=nos

  !phi_l-phi_g=0
  A(nos-1,iliq_min:iliq_max) = + Id(iliq,:)
  A(nos-1,igas_min:igas_max) = - Id(igas,:)

  ! U_l' phi_l - U_g' phi_g = c(phi_g' - phi_l')
  A(nos  ,iliq_min:iliq_max) = + Up_l(iliq)*Id(iliq,:)
  A(nos  ,igas_min:igas_max) = - Up_g(igas)*Id(igas,:)
  B(nos  ,iliq_min:iliq_max) = - D1_l(iliq,:)
  B(nos  ,igas_min:igas_max) = + D1_g(igas,:) 

  ! (phi_l''+alpha**2*phi_l)-m*(phi_g''+alpha**2*phi_g) = 1/c*(m*U_g''*phi_g-U_l''*phi_l)
  ! A(nos+1,iliq_min:iliq_max) = +     ( Upp_l(iliq)*Id(iliq,:) )
  ! A(nos+1,igas_min:igas_max) = - m * ( Upp_g(igas)*Id(igas,:) )
  ! B(nos+1,iliq_min:iliq_max) = -     ( D2_l(iliq,:) + alpha**2*Id(iliq,:) )
  ! B(nos+1,igas_min:igas_max) = + m * ( D2_g(igas,:) + alpha**2*Id(igas,:) )
  A(nos+1,iliq_min:iliq_max) = +     ( D2_l(iliq,:) + alpha**2*Id(iliq,:) )
  A(nos+1,igas_min:igas_max) = - m * ( D2_g(igas,:) + alpha**2*Id(igas,:) )

  ! (phi_l'''-3*alpha**2*phi_l')-m*(phi_g'''-3*alpha**2*phi_g')
  !  + i*( alpha*Re_l*(c*phi_l'+U_l'*phi_l)-r*alpha*Re_l*(c*phi_g'+U_g'*phi_g) )
  !  - i*alpha*Re_l*(F+alpha**2*S)*(phi_l'-phi_g')/(U_l'-U_g') = 0
  A(nos+2,iliq_min:iliq_max) = &
       (D3_l(iliq,:)-3.0_WP*alpha**2*D1_l(iliq,:))+ii*  alpha*Re_l*Up_l(iliq)*Id(iliq,:) &
       +ii*alpha*Re_l*(F+alpha**2*S)*D1_l(iliq,:)/(Up_l(iliq)-Up_g(igas))
  A(nos+2,igas_min:igas_max) = -m * &
       (D3_g(igas,:)-3.0_WP*alpha**2*D1_g(igas,:))-ii*r*alpha*Re_l*Up_g(igas)*Id(igas,:) &
       -ii*alpha*Re_l*(F+alpha**2*S)*D1_g(igas,:)/(Up_l(iliq)-Up_g(igas))
  B(nos+2,iliq_min:iliq_max) = -ii*  alpha*Re_l*D1_l(iliq,:)
  B(nos+2,igas_min:igas_max) = +ii*r*alpha*Re_l*D1_g(igas,:)

  ! Scale equations so that largest value in row is 1.0
  do i=1,2*nos
     tmp=max(maxval(abs( real(A(i,:)))),maxval(abs( real(B(i,:)))), &
          maxval(abs(aimag(A(i,:)))),maxval(abs(aimag(B(i,:))))  )
     if (tmp.eq.0.0_WP) tmp=1.0_WP
     A(i,:)=A(i,:)/tmp
     B(i,:)=B(i,:)/tmp
  end do

  ! Get eigenvalues and eigenvectors
  lwork = 4*(nos-1)
  Ain=A(2:2*nos-1,2:2*nos-1) ! Save A and B, Ain and Bin are destroyed
  Bin=B(2:2*nos-1,2:2*nos-1)
  call ZGGEV('N','V',2*(nos-1),Ain,2*(nos-1),Bin,2*(nos-1),eigval_a,eigval_b, &
       eigvec_l,1,eigvec_r,2*(nos-1),work,lwork,rwork,ierr)
  if (ierr.ne.0) stop "ZGGEV: unable to compute the eigenvalues/vectors"

  ! Get the most unstable mode
  ! -> phase speed (real part) between min and max of Ubase
  ! -> negative imaginary part
  Umin = min(Ubase_l(1),Ubase_g(1))
  Umax = max(Ubase_l(1),Ubase_g(1))

  mode_index = -1
  omega = -ii*huge(1.0_WP)

  iunit = iopen()
  open(iunit,file='eigenvalues.txt')

  ! Detect the modes
  do jj=1,2*(nos-1)
     omega_ = eigval_a(jj)/(eigval_b(jj)+epsilon(abs(eigval_b(jj))))
     speed_ = real(omega_)/alpha

     ! Calculate residual
     sol = 0.0_WP
     sol(2:2*nos-1) = eigvec_r(1:2*(nos-1),jj) 
     residual_real=0.0_WP
     residual_imag=0.0_WP
     do j=1,2*nos
        residual=sum(A(j,:)*sol(:))-omega_*sum(B(j,:)*sol(:))
        residual_real=residual_real+ real(residual)**2
        residual_imag=residual_imag+aimag(residual)**2
     end do
     residual_real=sqrt(residual_real)
     residual_imag=sqrt(residual_imag)

     !if (sqrt(residual_real**2+residual_imag**2).gt.10e-10) cycle

     if (abs(eigval_b(jj)).ne.0.0_WP) then
        if (abs(omega_).gt.1e-10) then
           !if (speed_.ge.Umin .and. speed_.le.Umax) then
           !if (sqrt(residual_real**2+residual_imag**2).lt.10e-5) then
           if (aimag(omega_).gt.aimag(omega).and.aimag(omega_).lt.10.0_WP) then
              mode_index = jj
              omega = omega_
              speed = speed_
           end if
           !end if
           !end if
        end if
     end if

     ! Save eigenvalue to file
     if (sqrt(residual_real**2+residual_imag**2).lt.10.0_WP) then
        write(iunit,'(I4,3ES20.10)') jj, real(omega_), aimag(omega_),sqrt(residual_real**2+residual_imag**2)
     end if
     ! ! ====== Plot this eigenfunction (for debuggin)  ========== 
     ! ! Save eigen_function at yos points
     ! phi_l=0.0_WP; phi_g=0.0_WP;
     ! phi_l(2:nos)   = eigvec_r(1:nos-1      ,jj) 
     ! phi_g(1:nos-1) = eigvec_r(nos:2*(nos-1),jj)
     ! iunit2 = iopen()
     ! open(iunit2,file='phi.txt')
     ! do j=1,nos
     !    write(iunit2,'(i3,5ES20.10)') j,yos_l(j),phi_l(j),sum(D1_l(j,:)*phi_l(:))
     ! end do
     ! do j=1,nos
     !    write(iunit2,'(i3,5ES20.10)') j+nos, yos_g(j),phi_g(j),sum(D1_g(j,:)*phi_g(:))
     ! end do
     ! close(iclose(iunit2))
     ! if (sqrt(residual_real**2+residual_imag**2).lt.1.0e+1_WP) then

     !    print *, jj,'Growthrate=',alpha*delta_g*aimag(omega_)/U_g
     !    write(plot_string,'(A,I3,A,2ES20.10,A,ES20.10,A)') ' echo ''set title "',jj,', Residual=',residual_real,residual_imag,'Growthrate',alpha*delta_g*aimag(omega_)/U_g,'"; p "phi.txt" u 2:3 w lp t "Real Part", "phi.txt" u 2:4 w lp t "Imaginary Part" '' | gnuplot -persist' 
     !    call system(plot_string)
     ! end if
     ! ! ================

  end do

  close(iclose(iunit))


  if (mode_index.eq.-1) &
       stop "orr_sommerfeld: Could not find a correct mode"

  ! Print found eigenvalue
  print *, 'Most unstable eigenvalue=',omega

  ! Calculate growthrate
  growthrate = alpha*delta_g*aimag(omega)/U_g
  print *, 'Growthrate=',growthrate

  ! Calculate residual
  sol = 0.0_WP
  sol(2:2*nos-1) = eigvec_r(:,mode_index) 
  residual_real=0.0_WP
  residual_imag=0.0_WP
  do j=1,2*nos
     residual=sum(A(j,:)*sol(:))-omega*sum(B(j,:)*sol(:))
     residual_real=residual_real+ real(residual)**2
     residual_imag=residual_imag+aimag(residual)**2
  end do
  residual_real=sqrt(residual_real)
  residual_imag=sqrt(residual_imag)
  print *, 'Residual=',residual_real,residual_imag

  ! Get eigenfunction for found eigenvalue 
  phi_l=0.0_WP; phi_g=0.0_WP;
  phi_l(2:nos)   = eigvec_r(1:nos-1      ,mode_index) 
  phi_g(1:nos-1) = eigvec_r(nos:2*(nos-1),mode_index)

  ! Write phi_l, phi_g to file
  iunit = iopen()
  open(iunit,file='phi.txt')
  do j=1,nos
     write(iunit,'(i3,5ES20.10)') j,yos_l(j),phi_l(j),sum(D1_l(j,:)*phi_l(:))
  end do
  do j=1,nos
     write(iunit,'(i3,5ES20.10)') j+nos, yos_g(j),phi_g(j),sum(D1_g(j,:)*phi_g(:))
  end do
  close(iclose(iunit))

  if (simulation.eq.'poiseuille') then
     ! Write error of Eigenvalue
     iunit = iopen()
     open(iunit,file='eig_errors.txt',access='append')
     write(iunit,'(I4,2ES20.10)') nos, abs(real(omega)-0.237526488821), abs(aimag(omega)-0.003739670623)
     close(iclose(iunit))
  end if

  ! Write growthrate to file
  iunit = iopen()
  open(iunit,file='output.txt',access='append')
  write(iunit,'(I4,3ES20.10)') nos,stretch,alpha,growthrate
  close(iclose(iunit))
  
  ! Calculate Fluctuations ==========================
  allocate(wreal_l(nos))
  allocate(wimag_l(nos))
  allocate(wreal_g(nos))
  allocate(wimag_g(nos))
  ! U
  wreal_l=matmul(Tp, real(matmul(D1_l,phi_l)))
  wreal_g=matmul(Tp, real(matmul(D1_g,phi_g)))
  wimag_l=matmul(Tp,aimag(matmul(D1_l,phi_l)))
  wimag_g=matmul(Tp,aimag(matmul(D1_g,phi_g)))
  iunit=iopen()
  open(iunit,file='dphi_grid.txt')
  do j=1,ny
     if (ym(j).le.0.0_WP) then ! Liquid
        phi_r=chebev(0.0_WP,yos_l(1),wreal_l,nos,ym(j))
        phi_i=chebev(0.0_WP,yos_l(1),wimag_l,nos,ym(j))
     else                     ! Gas
        phi_r=chebev(yos_g(nos),0.0_WP,wreal_g,nos,ym(j))
        phi_i=chebev(yos_g(nos),0.0_WP,wimag_g,nos,ym(j))
     end if
     do i=1,nx     
        U_fluct(i,j)=epsi*(phi_r*cos(alpha*x(i))-phi_i*sin(alpha*x(i)))
     end do
     write(iunit,'(3ES20.10)') ym(j),phi_r,phi_i
  end do
  close(iclose(iunit))

  ! V
  iunit=iopen()
  open(iunit,file='phi_grid.txt')
  wreal_l=matmul(Tp, real(phi_l))
  wreal_g=matmul(Tp, real(phi_g))
  wimag_l=matmul(Tp,aimag(phi_l))
  wimag_g=matmul(Tp,aimag(phi_g))
  do j=1,ny
     if (y(j).le.0.0_WP) then ! Liquid
        phi_r=chebev(0.0_WP,yos_l(1),wreal_l,nos,y(j))
        phi_i=chebev(0.0_WP,yos_l(1),wimag_l,nos,y(j))
     else                     ! Gas
        phi_r=chebev(yos_g(nos),0.0_WP,wreal_g,nos,y(j))
        phi_i=chebev(yos_g(nos),0.0_WP,wimag_g,nos,y(j))
     end if
     do i=1,nx
        V_fluct(i,j)=epsi*alpha*(phi_i*cos(alpha*xm(i))+phi_r*sin(alpha*xm(i)))
     end do
     write(iunit,'(3ES20.10)') y(j),phi_r,phi_i
  end do
  close(iclose(iunit))

  ! W
  W_fluct=0.0_WP

  ! VOF
  phi_r=chebev(0.0_WP,y(1),wreal_l,nos,0.0_WP)
  phi_i=chebev(0.0_WP,y(1),wimag_l,nos,0.0_WP)
  do j=1,ny
     do i=1,nx
        ! Interface location at cell faces
        I1=interface(x(i  ))
        I2=interface(x(i+1))
        if      (I1.lt.y(j  )) then; case1=-1;
        else if (I1.lt.y(j+1)) then; case1= 0;
        else                       ; case1=+1;
        end if
        if      (I2.lt.y(j  )) then; case2=-1;
        else if (I2.lt.y(j+1)) then; case2= 0;
        else                       ; case2=+1;
        end if
        area=(x(i+1)-x(i))*(y(j+1)-y(j))
        ! Determine how cell is intersected and integrate
        if      (case1.eq.-1.and.case2.eq.-1) then
           VOF(i,j,:)=0.0_WP
        else if (case1.eq.-1.and.case2.eq. 0) then
           xl=intersect(y(j),x(i),x(i+1)); xr=x(i+1)
           VOF(i,j,:)=(integrate(xl,xr)-(xr-xl)*y(j))/area
        else if (case1.eq.-1.and.case2.eq.+1) then
           xl=intersect(y(j),x(i),x(i+1)); xr=intersect(y(j+1),x(i),x(i+1))
           VOF(i,j,:)=(integrate(xl,xr)-(xr-xl)*y(j) + (x(i+1)-xr)*(y(j+1)-y(j)))/area
        else if (case1.eq. 0.and.case2.eq.-1) then
           xl=x(i); xr=intersect(y(j),x(i),x(i+1))
           VOF(i,j,:)=(integrate(xl,xr)-(xr-xl)*y(j))/area
        else if (case1.eq. 0.and.case2.eq. 0) then
           xl=x(i); xr=x(i+1)
           VOF(i,j,:)=(integrate(xl,xr)-(xr-xl)*y(j))/area
        else if (case1.eq. 0.and.case2.eq.+1) then
           xl=x(i); xr=intersect(y(j+1),x(i),x(i+1))
           VOF(i,j,:)=(integrate(xl,xr)-(xr-xl)*y(j) + (x(i+1)-xr)*(y(j+1)-y(j)))/area
        else if (case1.eq.+1.and.case2.eq.-1) then
           xl=intersect(y(j+1),x(i),x(i+1)); xr=intersect(y(j),x(i),x(i+1))
           VOF(i,j,:)=(integrate(xl,xr)-(xr-xl)*y(j) + (xl-x(i))*(y(j+1)-y(j)))/area
        else if (case1.eq.+1.and.case2.eq. 0) then
           xl=intersect(y(j+1),x(i),x(i+1)); xr=x(i+1)
           VOF(i,j,:)=(integrate(xl,xr)-(xr-xl)*y(j) + (xl-x(i))*(y(j+1)-y(j)))/area
        else if (case1.eq.+1.and.case2.eq.+1) then
           VOF(i,j,:)=1.0_WP
        else
           stop "Case should have been caught"
        end if

        if (j.eq.16.or.j.eq.17) then 
           print *,i,j,I1,I2,y(j),y(j+1),VOF(i,j,1)
        end if
     end do
  end do
  return
contains
  ! Evaluate interface function
  function interface(x) 
    implicit none
    real(WP), intent(in) :: x
    real(WP) :: interface
    interface=epsi*alpha**2/abs(alpha*omega)**2* &
         (aimag(omega)*(phi_i*cos(alpha*x)+phi_r*sin(alpha*x)) &
         + real(omega)*(phi_r*cos(alpha*x)-phi_i*sin(alpha*x)) ) 
    return
  end function interface

  ! Find x location of intersection of interface function and y between x1 and x2
  function intersect(y,x1,x2)
    implicit none
    real(WP), intent(in) :: y,x1,x2
    real(WP) :: intersect
    real(WP) :: Il,Im,Ir,xl,xm,xr
    integer :: n
    ! Initialize
    xl=x1; Il=interface(xl)-y
    xr=x2; Ir=interface(xr)-y
    ! Find using bisection
    do n=1,100
       xm=0.5_WP*(xl+xr); Im=interface(xm)-y
       if (Il*Im.lt.0.0_WP) then 
          xr=xm; Ir=Im
       else
          xl=xm; Il=Im
       end if
    end do
    intersect=0.5_WP*(xl+xr)
    return
  end function intersect
   
  ! Integrate interface function from x1 to x2
  function integrate(x1,x2)
    implicit none
    real(WP), intent(in) :: x1,x2
    real(WP) :: integrate

    integrate=epsi*alpha**2/abs(alpha*omega)**2* &
         (aimag(omega)*(phi_i*sin(alpha*x2)-phi_r*cos(alpha*x2)) &
         + real(omega)*(phi_r*sin(alpha*x2)+phi_i*cos(alpha*x2)) & 
         -aimag(omega)*(phi_i*sin(alpha*x1)-phi_r*cos(alpha*x1)) &
         - real(omega)*(phi_r*sin(alpha*x1)+phi_i*cos(alpha*x1)) )/alpha
    return
  end function integrate

end subroutine orr_sommerfeld

