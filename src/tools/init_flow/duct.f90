module duct
  use string
  use precision
  use param
  implicit none
  
  ! Length and diameter of the domain
  real(WP) :: Lx,Ly,Lz
  real(WP) :: dx,dy,dz
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: RHO
  real(WP), dimension(:,:,:), pointer :: dRHO
  
  ! Initial velocity
  real(WP) :: Wmean
  
end module duct


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine duct_grid
  use duct
  use parser
  implicit none
  
  integer :: i,j,k
  
  ! Read in mesh size
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  
  ! Read in domain size
  call parser_read('Lz',Lz); dz=Lz/real(nz,WP)
  if (nx.eq.1) then
     Lx=dz; dx=dz
  else
     call parser_read('Lx',Lx); dx=Lx/real(nx-2,WP)
  end if
  if (ny.eq.1) then
     Ly=dz; dy=dz
  else
     call parser_read('Ly',Ly); dy=Ly/real(ny-2,WP)
  end if
  
  ! Set the periodicity (revert to channel if 2D)
  if (nx.eq.1) then
     xper=1
  else
     xper=0
  end if
  if (ny.eq.1) then
     yper=1
  else
     yper=0
  end if
  zper=1
  
  ! Cartesian
  icyl=0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*dx
  end do
  if (nx.eq.1) then
     x=x-0.5_WP*Lx
  else
     x=x-0.5_WP*Lx-dx
  end if
  do j=1,ny+1
     y(j) = real(j-1,WP)*dy
  end do
  if (ny.eq.1) then
     y=y-0.5_WP*Ly
  else
     y=y-0.5_WP*Ly-dy
  end if
  do k=1,nz+1
     z(k) = real(k-1,WP)*dz
  end do
  
  ! Create the mid points 
  do i=1,nx
     xm(i)= 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)= 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  if (nx.gt.1) then
     mask(1,:)  = 1
     mask(nx,:) = 1
  end if
  if (ny.gt.1) then
     mask(:, 1)=1
     mask(:,ny)=1
  end if
  
  return
end subroutine duct_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine duct_data
  use duct
  use parser
  implicit none
  
  integer :: i,j
  real(WP) :: rho0
  
  ! Allocate the array data
  nvar = 6
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U    => data(:,:,:,1); names(1) = 'U'
  V    => data(:,:,:,2); names(2) = 'V'
  W    => data(:,:,:,3); names(3) = 'W'
  P    => data(:,:,:,4); names(4) = 'P'
  RHO  => data(:,:,:,5); names(5) = 'RHO'
  dRHO => data(:,:,:,6); names(6) = 'dRHO'
  
  ! Create them
  call parser_read('Mean W Velocity',Wmean,0.0_WP)
  U    = 0.0_WP
  V    = 0.0_WP
  do j=1,ny
     do i=1,nx
        if (mask(i,j).eq.0) then
           W(i,j,:)=Wmean
        else
           W(i,j,:)=0.0_WP
        end if
     end do
  end do
  P    = 0.0_WP
  call parser_read('Density',rho0,1.0_WP)
  RHO  = rho0
  dRHO = 0.0_WP
  
  return
end subroutine duct_data


! ======================================== !
! Generate an initial droplet distribution !
! ======================================== !
subroutine duct_part
  use duct
  use math
  use lpt_mod
  use random
  use parser
  implicit none
  
  integer  :: iunit,ierr
  integer  :: np,i,ix,iy,iz
  integer  :: npartx,nparty,npartz
  real(WP) :: Hbed
  real(WP) :: VFavg,Volp,Volbed,sumVolp
  real(WP) :: rand,Lp,Lpx,Lpy,mean_d
  real(WP) :: d_mean,d_min,d_max,d_sd,d_shift
  real(WP) :: dt,time
  real(WP) :: uin,vin,win,amp
  character(len=str_medium) :: part_init
  character(len=str_medium) :: distro
  real(WP), dimension(:), pointer :: dtmp
  logical :: use_lpt

  ! Check whether lpt is used
  call parser_is_defined('Init part file',use_lpt)
  if (.not.use_lpt) return
  
  ! Initialize the random number generator
  call random_init
  
  ! Read average volume fraction and bed height
  call parser_read('Bed height',Hbed,Lz)
  call parser_read('Avg. volume fraction',VFavg)
  
  ! Read particle diameter
  call parser_read('Particle mean diameter',d_mean)
  call parser_read('Particle standard deviation',d_sd,0.0_WP)
  call parser_read('Particle min diameter',d_min,0.0_WP)
  call parser_read('Particle max diameter',d_max,0.0_WP)
  call parser_read('Particle diameter shift',d_shift,0.0_WP)
  
  ! Velocity parameters
  call parser_read('Particle u velocity',uin,0.0_WP)
  call parser_read('Particle v velocity',vin,0.0_WP)
  call parser_read('Particle w velocity',win,0.0_WP)
  call parser_read('Particle fluctuations',amp,0.0_WP)
  
  ! Particle distribution
  call parser_read('Particle distribution',distro)
  
  ! First get bed volume
  Volbed = Hbed*(Lx-2.0_WP*Lx/real(nx,WP))*(Ly-2.0_WP*Ly/real(ny,WP))
  
  ! Estimate number of particles
  Volp = Pi/6.0_WP*d_mean**3
  np = 5*int(VFavg*Volbed/Volp)
 
  ! Allocate temporary particle size array
  allocate(dtmp(np))

  ! Generate particles randomly until volume is sufficient
  np=0
  mean_d=0.0_WP
  sumVolp=0.0_WP
  do while (sumVolp.lt.VFavg*Volbed)
     
     ! Add a particle
     np=np+1
     
     ! Set particle size distribution (compact support Gaussian)
     !dtmp(np)=random_normal(m=d_mean,sd=d_sd)
     !if (d_sd.gt.0.0_WP) then
     !   do while (dtmp(np).gt.d_max.or.dtmp(np).lt.d_min)
     !      dtmp(np)=random_normal(m=d_mean,sd=d_sd)
     !   end do
     !else
     !   dtmp(np)d_mean
     !end if
     ! Set particle size distribution (compact support lognormal)
     dtmp(np)=random_lognormal(m=d_mean,sd=d_sd)+d_shift
     if (d_sd.gt.0.0_WP) then
        do while (dtmp(np).gt.d_max+epsilon(1.0_WP).or.dtmp(np).lt.d_min-epsilon(1.0_WP))
           dtmp(np)=random_lognormal(m=d_mean,sd=d_sd)+d_shift
        end do
     else
        dtmp(np)=d_mean
     end if
     
     ! Add up total particle volume
     if (trim(distro).eq.'uniform') then
        sumVolp=sumVolp+Pi/6.0_WP*d_mean**3.0_WP
     else
        sumVolp=sumVolp+Pi/6.0_WP*dtmp(np)**3.0_WP
     end if
     
     ! Compute mean particle diameter
     mean_d=mean_d+dtmp(np)
     
  end do
  
  ! Mean particle diameter
  mean_d = mean_d/np
  
  ! Mean particle volume
  Volp = sumVolp/np
  
  ! Mean interparticle distance
  select case (trim(distro))
  case ('random')
     Lp = (Volp/VFavg)**(1.0_WP/3.0_WP)
  case ('uniform')
     Lp = (Volp/VFavg)**(1.0_WP/3.0_WP)
     npartz = int(Lz/Lp)
     Lp = Hbed/real(npartz,WP)
     npartx = int((Lx-2.0_WP*Lx/real(nx,WP))/Lp)
     Lpx = Lx/real(npartx,WP)
     nparty = int((Ly-2.0_WP*Ly/real(ny,WP))/Lp)
     Lpy = Ly/real(nparty,WP)
     np = npartx*nparty*npartz
  case default
     stop 'Unknown particle distribution'
  end select
  
  ! Allocate particle array
  allocate(part(1:np))
  
  ! Add particles
  do i=1,np
     
     ! Set various parameters for the particle
     part(i)%dt  =0.0_WP
     part(i)%Acol=0.0_WP
     part(i)%Tcol=0.0_WP
     part(i)%wx  =0.0_WP
     part(i)%wy  =0.0_WP
     part(i)%wz  =0.0_WP
     part(i)%id  =int(i,kind=8)
     part(i)%stop=0
     
     ! Set particle size
     if (trim(distro).eq.'uniform') then
        part(i)%d = d_mean
     else
        part(i)%d = dtmp(i)
     end if
     
     ! Give it a velocity
     if (nx.gt.1) then
        part(i)%u = uin
        if (amp.gt.0.0_WP) then
           call random_number(rand)
           rand=2.0_WP*rand-1.0_WP
           part(i)%u = part(i)%u + amp*rand
        end if
     else
        part(i)%u = 0.0_WP
     end if
     if (ny.gt.1) then
        part(i)%v = vin
        if (amp.gt.0.0_WP) then
           call random_number(rand)
           rand=2.0_WP*rand-1.0_WP
           part(i)%v = part(i)%v + amp*rand
        end if
     else
        part(i)%v = 0.0_WP
     end if
     if (nz.gt.1) then
        part(i)%w = win
        if (amp.gt.0.0_WP) then
           call random_number(rand)
           rand=2.0_WP*rand-1.0_WP
           part(i)%w = part(i)%w + amp*rand
        end if
     else
        part(i)%w = 0.0_WP
     end if
     
  end do
  
  ! Effective volume fraction
  VFavg = sumVolp/Volbed
  
  ! Output
  print*,'Number of particles: ',np
  print*,'Interparticle spacing: ',Lp
  print*,'Effective mean diameter: ',mean_d
  print*,'Effective volume fraction: ',VFavg
  
  ! Distribute particles
  select case (trim(distro))
  case ('random')
     ! Random distribution
     do i=1,np
        call random_number(rand)
        part(i)%z = Hbed*rand
        if (ny.eq.1) then
           call random_number(rand)
           rand = rand-0.5_WP
           part(i)%x = Lx*rand
           part(i)%y = 0.0_WP
        else if (nx.eq.1) then
           call random_number(rand)
           rand = rand-0.5_WP
           part(i)%y = Ly*rand
           part(i)%x = 0.0_WP
        else
           call random_number(rand)
           rand = rand-0.5_WP
           part(i)%x = Lx*rand
           call random_number(rand)
           rand = rand-0.5_WP
           part(i)%y = Ly*rand
        end if
     end do
  case ('uniform')
     ! Uniform distribution
     do i=1,np
        iz = (i-1)/(npartx*nparty)
        ix = (i-1-npartx*nparty*iz)/nparty
        iy = i-1-npartx*nparty*iz-nparty*ix
        part(i)%z = z(1)+(real(iz,WP)+0.5_WP)*Lp
        part(i)%x = x(1)+(real(ix,WP)+0.5_WP)*Lpx+dx
        part(i)%y = y(1)+(real(iy,WP)+0.5_WP)*Lpy+dy
     end do 
  end select

  ! Generate the initial droplet file
  call parser_read('Init part file',part_init)
  call BINARY_FILE_OPEN(iunit,trim(part_init),"w",ierr)
  call BINARY_FILE_WRITE(iunit,np,1,kind(np),ierr)
  call BINARY_FILE_WRITE(iunit,part_kind,1,kind(part_kind),ierr)
  dt = 0.0_WP
  call BINARY_FILE_WRITE(iunit,dt,1,kind(dt),ierr)
  time = 0.0_WP
  call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr)
  do i=1,np
     call BINARY_FILE_WRITE(iunit,part(i),1,part_kind,ierr)
  end do
  call BINARY_FILE_CLOSE(iunit,ierr)
 
  return
end subroutine duct_part
