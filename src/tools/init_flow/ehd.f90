module ehd
  use string
  use precision
  use param
  implicit none
  
  ! Domain
  real(WP) :: Lx,Ly,Lz,dx
  character(len=str_medium) :: dim
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:),   pointer :: U
  real(WP), dimension(:,:,:),   pointer :: V
  real(WP), dimension(:,:,:),   pointer :: W
  real(WP), dimension(:,:,:,:), pointer :: VOF
  real(WP), dimension(:,:,:),   pointer :: L_Q  
end module ehd


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine ehd_grid
  use ehd
  use parser
  implicit none
  
  integer :: i,j,k
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  call parser_read('Lz',Lz)
  xper=1
  yper=0
  zper=1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*Lx/real(nx,WP)-0.5_WP*Lx
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*Ly/real(ny,WP)
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*Lz/real(nz,WP)-0.5_WP*Lz
  end do
  dx=Lx/real(nx,WP)
  
  ! Create the cell centered grid
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine ehd_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine ehd_data
  use ehd
  use parser
  use math
  implicit none
  
  real(WP) :: dradius,qo,vel
  real(WP), dimension(3) :: dcenter
  real(WP) :: myx,myy,myz,dist_drop,sigma,r2
  integer :: s,i,j,k,ii,jj,kk
  integer, parameter :: nF=20
  character(len=str_medium) :: sim_type
  
  ! Allocate the array data
  nvar = 12
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U    => data(:,:,:, 1); names( 1) = 'U'
  V    => data(:,:,:, 2); names( 2) = 'V'
  W    => data(:,:,:, 3); names( 3) = 'W'
  L_Q  => data(:,:,:, 4); names( 4) = 'L_Q'
  VOF  => data(:,:,:, 5:12);
  names( 5) = 'VOF1'
  names( 6) = 'VOF2'
  names( 7) = 'VOF3'
  names( 8) = 'VOF4'
  names( 9) = 'VOF5'
  names(10) = 'VOF6'
  names(11) = 'VOF7'
  names(12) = 'VOF8'
  
  ! Initialize
  U = 0.0_WP
  V = 0.0_WP
  W = 0.0_WP
  VOF = 0.0_WP
  L_Q = 0.0_WP

  ! Get simulation type
  call parser_read('Simulation type',sim_type)
  call parser_read('Charge density',qo)
  call parser_read('U velocity',vel,0.0_WP); U=vel
  call parser_read('V velocity',vel,0.0_WP); V=vel
  call parser_read('W velocity',vel,0.0_WP); W=vel

  select case(trim(sim_type))

  case ('droplet')
     call parser_read('Drop radius',dradius)
     call parser_read('Drop center',dcenter)
     call parser_read('Charge sigma',sigma)
     do k=1,nz
        do j=1,ny
           do i=1,nx
              dist_drop=dradius-sqrt((x(i)-dcenter(1))**2+(y(j)-dcenter(2))**2+(z(k)-dcenter(3))**2)
              if (abs(dist_drop).lt.2.0_WP*dx) then ! Close to interface, compute VOF on fine mesh
                 do s=1,8
                    VOF(i,j,k,s)=0.0_WP
                    do kk=1,nF
                       do jj=1,nF
                          do ii=1,nF
                             myx=xs1(s,i)+(real(ii,WP)-0.5_WP)/real(nf,WP)*(xs2(s,i)-xs1(s,i))
                             myy=ys1(s,j)+(real(jj,WP)-0.5_WP)/real(nf,WP)*(ys2(s,j)-ys1(s,j))
                             myz=zs1(s,k)+(real(kk,WP)-0.5_WP)/real(nf,WP)*(zs2(s,k)-zs1(s,k))
                             if (dradius.gt.sqrt((myx-dcenter(1))**2+(myy-dcenter(2))**2+(myz-dcenter(3))**2)) then
                                VOF(i,j,k,s)=VOF(i,j,k,s)+1.0_WP
                             end if
                          end do
                       end do
                    end do
                    VOF(i,j,k,s)=VOF(i,j,k,s)/real(nF,WP)**3
                 end do
              else ! Away from interface, VOF = 0 or 1
                 if (dist_drop.lt.0.0_WP) then
                    VOF(i,j,k,:)=0.0_WP
                 else
                    VOF(i,j,k,:)=1.0_WP
                 end if
              end if
              ! Electric charge
              r2=(xm(i)-dcenter(1))**2+(ym(j)-dcenter(2))**2+(zm(k)-dcenter(3))**2
              L_Q(i,j,k)=qo/(sigma*sqrt(2.0_WP*pi))*exp(-r2/(2.0_WP*sigma**2))
           end do
        end do
     end do
     
  case ('1D')
     ! 1D test
     do j=1,ny
        do s=1,8
           do jj=1,nF
              myy=ys1(s,j)+real(jj,WP)/real(nf+1,WP)*(ys2(s,j)-ys1(s,j))
              if (myy.gt.0.4083333333333334) VOF(:,j,:,s)=VOF(:,j,:,s)+1.0_WP/real(nF,WP)
           end do
        end do
     end do
     L_Q=qo*0.125_WP*sum(VOF(:,:,:,:),4)
  case default
     stop 'Unknown simulation type'
  end select

  return
end subroutine ehd_data
