module wave
  use string
  use precision
  use param
  implicit none
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:,:), pointer :: VOF
  
end module wave

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine wave_grid
  use wave
  use parser
  use math
  implicit none
  
  integer :: i,j,k
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  ny = nx
  nz = 1
  xper = 1
  yper = 0
  zper = 1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*twoPi/real(nx,WP)-Pi
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*twoPi/real(nx,WP)-Pi
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*twoPi/real(nx,WP)
  end do
  
  ! Create the cell centered grid
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine wave_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine wave_data
  use wave
  use parser
  use math
  implicit none
  real(WP) :: amp
  integer :: s,i,j,k
  logical :: int_left,int_right
  real(WP) :: xmin,xmax,ymin,ymax,x0
  logical :: top
  
  ! Allocate the array data
  nvar = 11
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U   => data(:,:,:,1); names(1) = 'U'
  V   => data(:,:,:,2); names(2) = 'V'
  W   => data(:,:,:,3); names(3) = 'W'
  VOF  => data(:,:,:, 4:11);
  names( 4) = 'VOF1'
  names( 5) = 'VOF2'
  names( 6) = 'VOF3'
  names( 7) = 'VOF4'
  names( 8) = 'VOF5'
  names( 9) = 'VOF6'
  names(10) = 'VOF7'
  names(11) = 'VOF8'
  
  ! Initialize
  U   = 0.0_WP
  V   = 0.0_WP
  W   = 0.0_WP
  VOF = 0.0_WP
  
  ! Setup the interface
  call parser_read('Initial amplitude',amp,0.01_WP*twoPi)
  print*,'amp=',amp
  ! Bottom of domain
  ! Loop over domain and subcells
  do s=1,8
     do k=1,nz
        do j=1,ny
           do i=1,nx
              ! Evaluate VOF using integral on subcell
              xmin=xs1(s,i); xmax=xs2(s,i)
              ymin=ys1(s,j); ymax=ys2(s,j)
              ! Check for intersections
              int_left=.false.; int_right=.false.
              if ((fun(xmin).ge.ymin).and.(fun(xmin).le.ymax)) int_left =.true.
              if ((fun(xmax).ge.ymin).and.(fun(xmax).le.ymax)) int_right=.true.
              if (int_left.and.int_right) then
                 ! Int bounds are xmin,xmax
              else if (int_left) then
                 ! Find right integration bound
                 call find_bound(xmin,xmax,ymin,ymax,'right',x0,top); xmax=x0
                 if (top) then
                    VOF(i,j,k,s)=(xs2(s,i)-xmax)/(xs2(s,i)-xs1(s,i))
                 end if
              else if (int_right) then
                 ! Find left integration bound
                 call find_bound(xmin,xmax,ymin,ymax,'left',x0,top); xmin=x0
                 if (top) then
                    VOF(i,j,k,s)=(xmin-xs1(s,i))/(xs2(s,i)-xs1(s,i))
                 end if
              else
                 ! Interface doesn't intersect cell
                 if (fun(0.5_WP*(xmin+xmax)).gt.ymax) then
                    ! Liquid cell
                    VOF(i,j,k,s)=1.0_WP
                 else
                    ! Gas cell
                    VOF(i,j,k,s)=0.0_WP
                 end if
                 cycle
              end if
              ! Perform integration
              VOF(i,j,k,s)=VOF(i,j,k,s)+compute_VOF(xmin,xmax,ymin,ymax)
           end do
        end do
     end do
  end do
  
  return

contains
  ! =========================== !
  ! Function defining interface !
  ! =========================== !
  function fun(x)
    real(WP), intent(in) :: x
    real(WP) :: fun
    fun=amp*cos(x);
    return
  end function fun
  ! =========================== !
  ! Integral of function -> VOF !
  ! =========================== !
  function compute_VOF(x1,x2,y1,y2) result(VOF)
    real(WP), intent(in) :: x1,x2,y1,y2
    real(WP) :: VOF
    VOF=(amp*(sin(x2)-sin(x1))-y1*(x2-x1))/((xs2(s,i)-xs1(s,i))*(ys2(s,j)-ys1(s,j)))
    return
  end function compute_VOF
  ! =================== !
  ! Find insection with !
  ! cell bottom/top     !
  !==================== !
  subroutine find_bound(x1,x2,y1,y2,face,x0,top)
    real(WP), intent(in) :: x1,x2,y1,y2
    character(len=*), intent(in) :: face
    real(WP), intent(out) :: x0
    logical, intent(out) :: top
    real(WP) :: funface
    ! Evaluate function on face
    select case(trim(face))
    case ('left')
       funface=fun(x1)
    case ('right')
       funface=fun(x2)
    case Default
       stop "Unknown face specified"
    end select
    ! Check if function exits top or bottom
    if (funface.le.y1) then
       ! Exits bottom
       x0=acos(y1/amp);
       if (x1.le.x0.and.x2.ge.x0) then
          ! Found bound
       elseif (x1.le.-x0.and.x2.ge.-x0) then
          x0=-x0
          ! Found bound
       else
          stop "x0 is not within cell! bottom"
       end if
       top=.false.
    elseif (funface.ge.y2) then
       ! Exits top
       x0=acos(y2/amp);
       if (x1.le.x0.and.x2.ge.x0) then
          ! Found bound
       elseif (x1.le.-x0.and.x2.ge.-x0) then
          x0=-x0
          ! Found bound
       else
          print *,x1,x2,y1,y2,face,x0
          stop "x0 is not within cell! top"
       end if
       top=.true.
    else
       stop "Exits boundary, shouldn't be here!"
    end if
    
    return
  end subroutine find_bound
  
end subroutine wave_data
