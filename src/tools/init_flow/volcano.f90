module volcano
  use precision
  use param
  implicit none
  
  ! Length and diameter of the domain
  real(WP) :: Lx,Ly,Lz,dx,dy,dz
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: RHO
  real(WP), dimension(:,:,:), pointer :: dRHO
  
end module volcano


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine volcano_grid
  use volcano
  use parser
  implicit none
  
  integer :: i,j,k
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  
  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  call parser_read('Lz',Lz)
  
  ! Set the periodicity
  xper=0
  yper=1
  zper=1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  dx=Lx/real(nx,WP)
  dy=Ly/real(ny,WP)
  dz=Lz/real(nz,WP)
  do i=1,nx+1
     x(i)=real(i-1,WP)*dx
  end do
  do j=1,ny+1
     y(j)=real(j-1,WP)*dy-0.5_WP*Ly
  end do
  do k=1,nz+1
     z(k)=real(k-1,WP)*dz-0.5_WP*Lz
  end do
  
  ! Create the mid points
  do i=1,nx
     xm(i)=0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)=0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)=0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask=0
    
  return
end subroutine volcano_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine volcano_data
  use volcano
  use parser
  implicit none
  
  ! Allocate the array data
  nvar = 6
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U    => data(:,:,:,1); names(1) = 'U'
  V    => data(:,:,:,2); names(2) = 'V'
  W    => data(:,:,:,3); names(3) = 'W'
  P    => data(:,:,:,4); names(4) = 'P'
  RHO  => data(:,:,:,5); names(5) = 'RHO'
  dRHO => data(:,:,:,6); names(6) = 'dRHO'
  
  ! Create them
  U    = 0.0_WP
  V    = 0.0_WP
  W    = 0.0_WP
  P    = 0.0_WP
  RHO  = 0.0_WP
  dRHO = 0.0_WP
  
  return
end subroutine volcano_data


! ========================================= !
! Generate an initial particle distribution !
! ========================================= !
subroutine volcano_part
  use volcano
  use parser
  use lpt_mod
  use random
  implicit none
  
  character(len=str_medium) :: distro
  character(len=str_medium) :: part_init
  logical :: use_lpt
  integer :: iunit,ierr
  integer :: i,np
  real(WP) :: Hbed,Dbed,VFavg
  real(WP) :: Lp,mean_d
  real(WP) :: dt,time,rand
  
  ! Check whether lpt is used
  call parser_is_defined('Init part file',use_lpt)
  if (.not.use_lpt) return
  
  ! Particle distribution
  call parser_read('Particle distribution',distro)

  ! Read average volume fraction and bed height
  call parser_read('Bed height',Hbed)
  call parser_read('Avg. volume fraction',VFavg)
  Dbed=Ly
  
  ! Distribute particles / assign diameter
  select case (trim(distro))
  case ('uniform')
     call volcano_uniform(Hbed,Dbed,VFavg,np,Lp,mean_d)
  case ('random')
     call volcano_random(Hbed,Dbed,VFavg,np,Lp,mean_d)
  case default
     stop 'Unknown particle distribution'
  end select

  ! Initialize the random number generator
  call random_init

  ! Set particle parameters
  do i=1,np
     
     ! Set various parameters for the particle
     part(i)%dt  =0.0_WP
     part(i)%Acol=0.0_WP
     part(i)%Tcol=0.0_WP
     part(i)%wx  =0.0_WP
     part(i)%wy  =0.0_WP
     part(i)%wz  =0.0_WP
     !if (part(i)%x.gt.Hbed*0.5_WP) then
        part(i)%id=i
     !else
     !   part(i)%id=1
     !end if
     
     part(i)%stop=0
     
     ! Give it a velocity
     part(i)%u = 0.0_WP
     part(i)%v = 0.0_WP
     part(i)%w = 0.0_WP
     
  end do
  
  ! Output
  print*,'Number of particles: ',np
  print*,'Interparticle spacing: ',Lp
  print*,'Effective mean diameter: ',mean_d
  print*,'Effective volume fraction: ',VFavg

  ! Generate the initial particle file
  call parser_read('Init part file',part_init)
  call BINARY_FILE_OPEN(iunit,trim(part_init),"w",ierr)
  call BINARY_FILE_WRITE(iunit,np,1,kind(np),ierr)
  call BINARY_FILE_WRITE(iunit,part_kind,1,kind(part_kind),ierr)
  dt = 0.0_WP
  call BINARY_FILE_WRITE(iunit,dt,1,kind(dt),ierr)
  time = 0.0_WP
  call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr)
  do i=1,np
     call BINARY_FILE_WRITE(iunit,part(i),1,part_kind,ierr)
  end do
  call BINARY_FILE_CLOSE(iunit,ierr)

  return
end subroutine volcano_part


! ==================== !
! Uniform distribution !
! ==================== !
subroutine volcano_uniform(Hbed,Dbed,VFavg,np,Lp,mean_d)
  use volcano
  use lpt_mod
  use parser
  use math
  use random
  implicit none
  
  integer :: i
  integer, intent(out) :: np
  integer  :: ix,iy,iz,pcount
  integer  :: npartx,nparty,npartz
  real(WP) :: Hbed,Dbed,VFavg
  real(WP), intent(out) :: Lp,mean_d
  real(WP) :: Volp,rp,Lpy,Lpz,rand
  real(WP), dimension(:), pointer :: xtmp,ytmp,ztmp
  
  ! Read particle diameter
  call parser_read('Particle mean diameter',mean_d)
  
  ! Particle volume
  Volp = Pi/6.0_WP*mean_d**3

  ! Mean interparticle distance
  !Lp = (Volp/VFavg)**(1.0_WP/3.0_WP)
  !npartx = int(Hbed/Lp)
  !Lp = Hbed/real(npartx,WP)
  !nparty = int(Dbed/Lp)
  !Lpy = Dbed/real(nparty,WP)
  !npartz = int(Lz/Lp)
  !Lpz = Lz/real(npartz,WP)
  call parser_read('npartx',npartx)
  call parser_read('nparty',nparty)
  call parser_read('npartz',npartz)
  np  = npartx*nparty*npartz
  Lp  = Hbed/real(npartx,WP)
  Lpy = Dbed/real(nparty,WP)
  Lpz = Lz  /real(npartz,WP)
  VFavg = Volp/(Lp*Lpy*Lpz)
  
  ! Allocate temporary particle arrays
  allocate(xtmp(1:np))
  allocate(ytmp(1:np))
  allocate(ztmp(1:np))
  
  ! Distribute particles across entire domain
  pcount=0
  do i=1,np
     ix = (i-1)/(nparty*npartz)
     iy = (i-1-nparty*npartz*ix)/npartz
     iz = i-1-nparty*npartz*ix-npartz*iy
     xtmp(i) = x(1)+(ix+0.5_WP)*Lp
     ytmp(i) = -0.5_WP*Dbed+(iy+0.5_WP)*Lpy
     ztmp(i) = z(1)+(iz+0.5_WP)*Lpz
     
     ! Detect particles outside diameter
     if (Dbed.gt.0.0_WP) then
        rp=sqrt(ytmp(i)**2+ztmp(i)**2)+0.5_WP*mean_d
        if (rp.ge.0.5_WP*Dbed) pcount=pcount+1
     end if
  end do
  
  ! Allocate particle array
  allocate(part(1:np-pcount))
  
  ! Distribute particles
  pcount=0
  do i=1,np
     ix = (i-1)/(nparty*npartz)
     iy = (i-1-nparty*npartz*ix)/npartz
     iz = i-1-nparty*npartz*ix-npartz*iy
     xtmp(i) = x(1)+(ix+0.5_WP)*Lp
     ytmp(i) = -0.5_WP*Dbed+(iy+0.5_WP)*Lpy
     ztmp(i) = z(1)+(iz+0.5_WP)*Lpz
     
     ! Detect particles outside diameter
     if (Dbed.gt.0.0_WP) then
        rp=sqrt(ytmp(i)**2+ztmp(i)**2)+0.5_WP*mean_d
        if (rp.lt.0.5_WP*Dbed) then
           pcount=pcount+1
           part(pcount)%d=mean_d
           part(pcount)%x=xtmp(i)
           part(pcount)%y=ytmp(i)
           part(pcount)%z=ztmp(i)
        end if
     else
        pcount=pcount+1
        part(pcount)%d=mean_d
        part(pcount)%x=xtmp(i)
        part(pcount)%y=ytmp(i)
        part(pcount)%z=ztmp(i)
     end if
  end do

  ! Update number of particles
  np=pcount
 
  return
end subroutine volcano_uniform


! =================== !
! Random distribution !
! =================== !
subroutine volcano_random(Hbed,Dbed,VFavg,np,Lp,mean_d)
  use volcano
  use lpt_mod
  use parser
  use math
  use random
  implicit none
  
  integer :: i
  integer, intent(out) :: np
  real(WP), intent(in) :: Hbed,Dbed
  real(WP), intent(out) :: Lp,mean_d
  real(WP), intent(inout) :: VFavg
  real(WP) :: Volp,Volbed,sumVolp,rp,rand
  real(WP) :: d_mean,d_min,d_max,d_sd,d_shift
  real(WP), dimension(:), pointer :: dtmp
  
  ! Read particle diameter
  call parser_read('Particle mean diameter',d_mean)
  call parser_read('Particle standard deviation',d_sd,0.0_WP)
  call parser_read('Particle min diameter',d_min,0.0_WP)
  call parser_read('Particle max diameter',d_max,0.0_WP)
  call parser_read('Particle diameter shift',d_shift,0.0_WP)

  ! Initialize the random number generator
  call random_init
  
  ! First get bed volume
  Volbed = Hbed*Dbed*Lz
  
  ! Estimate number of particles
  Volp = Pi/6.0_WP*d_mean**3
  np = 5*int(VFavg*Volbed/Volp)
  
  ! Allocate temporary particle size array
  allocate(dtmp(np))

  ! Generate particles randomly until volume is sufficient
  np=0
  mean_d=0.0_WP
  sumVolp=0.0_WP
  do while (sumVolp.lt.VFavg*Volbed)
     
     ! Add a particle
     np=np+1
     
     ! Set particle size distribution (compact support Gaussian)
     !dtmp(np)=random_normal(m=d_mean,sd=d_sd)
     !if (d_sd.gt.0.0_WP) then
     !   do while (dtmp(np).gt.d_max.or.dtmp(np).lt.d_min)
     !      dtmp(np)=random_normal(m=d_mean,sd=d_sd)
     !   end do
     !else
     !   dtmp(np)=d_mean
     !end if
     ! Set particle size distribution (compact support lognormal)
     dtmp(np)=random_lognormal(m=d_mean,sd=d_sd)+d_shift
     if (d_sd.gt.0.0_WP) then
        do while (dtmp(np).gt.d_max+epsilon(1.0_WP).or.dtmp(np).lt.d_min-epsilon(1.0_WP))
           dtmp(np)=random_lognormal(m=d_mean,sd=d_sd)+d_shift
        end do
     else
        dtmp(np)=d_mean
     end if
     
     ! Add up total particle volume
     sumVolp=sumVolp+Pi/6.0_WP*dtmp(np)**3.0_WP
     
     ! Compute mean particle diameter
     mean_d=mean_d+dtmp(np)
     
  end do

  ! Mean particle diameter
  mean_d = mean_d/np
  
  ! Mean particle volume
  Volp = sumVolp/np

  ! Mean interparticle distance
  Lp = (Volp/VFavg)**(1.0_WP/3.0_WP)

  ! Allocate particle array
  allocate(part(1:np))
  
  ! Effective volume fraction
  VFavg = sumVolp/Volbed
  
  ! Distribute particles
  do i=1,np
     ! Set particle size
     part(i)%d = dtmp(i)
     ! Place particle
     call random_number(rand)
     part(i)%x = Hbed*rand
     call random_number(rand)
     rand = rand-0.5_WP
     part(i)%y = Dbed*rand
     call random_number(rand)
     rand = rand-0.5_WP
     part(i)%z = Lz*rand
  end do
 
  return
end subroutine volcano_random


! =================== !
! Generate the inflow !
! =================== !
subroutine volcano_inflow
  use volcano
  use parser
  use math
  use fileio
  implicit none
  
  integer :: j
  real(WP) :: dhole,vhole
  
  ! Generate an inflow file
  nvar_inflow = 3
  ntime = 2
  
  ! Allocate some arrays
  allocate(inflow(ntime,ny,nz,nvar_inflow))
  allocate(names_inflow(nvar_inflow))
  
  ! Link the pointers
  U => inflow(:,:,:,1); names_inflow(1) = 'U'
  V => inflow(:,:,:,2); names_inflow(2) = 'V'
  W => inflow(:,:,:,3); names_inflow(3) = 'W'
  
  ! Compute time grid
  time_inflow = 0.0_WP
  dt_inflow   = 1.0_WP
  
  ! Read input
  call parser_read('Hole diameter',dhole)
  call parser_read('Hole velocity',vhole)
  
  ! Initialize velocity
  U = 0.0_WP
  V = 0.0_WP
  W = 0.0_WP
  
  ! Plug flow profile
  do j=1,ny
     if (abs(ym(j)).le.0.5_WP*dhole) then
        U(:,j,:)=vhole
     end if
  end do
  
  return
end subroutine volcano_inflow
