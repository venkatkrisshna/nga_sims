module particle
  use precision
  use param
  use fileio
  implicit none

  ! Box dimensions
  real(WP) :: Lx,Ly,Lz

  ! Number of particles per direction
  integer :: npartx,nparty,npartz

  ! Total number of particles
  integer :: np

  ! Particle diameter
  real(WP) :: diam
  
  ! Initial gas volume fraction
  real(WP) :: eps

  ! Initial particle velocity
  real(WP), dimension(3) :: vp
  
  ! Initial gas velocity
  real(WP), dimension(3) :: vg
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: G

end module particle


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine particle_grid
  use particle
  use parser
  use math
  implicit none
  
  integer :: i,j,k
  real(WP):: dx,dy,dz,Lp
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  
  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  call parser_read('Lz',Lz)
  
  call parser_read('xper',xper,1)
  call parser_read('yper',yper,1)
  zper=1
      
  ! Cartesian
  icyl = 0
  
  ! Read particle parameters
  call parser_read('Particle diameter',diam)
  call parser_read('Particle initial velocity',vp)
  call parser_read('Fluid initial velocity',vg)
  call parser_read('Init part vol frac',eps)
  
  ! Adjust domain dimension to get right volume fraction
  Lp = (Pi/(6.0_WP*eps))**(1.0_WP/3.0_WP)*diam
  npartx = max(nint(Lx/Lp),1)
  nparty = max(nint(Ly/Lp),1)
  npartz = max(nint(Lz/Lp),1)
    
  ! Number of particles
  np = npartx*nparty*npartz
  print*, 'Total number of particles',np
  eps= np*Pi/6.0_WP*diam**3/(Lx*Ly*Lz)
  print*, 'Effective volume fraction',eps
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))

  ! Mesh
  dx = Lx/real(nx,WP)
  dy = Ly/real(ny,WP)
  dz = Lz/real(nz,WP)

  do i=0,nx
    x(i+1)=dx*real(i,WP)
  end do
  do j=0,ny
    y(j+1)=dy*real(j,WP)
  end do
  do k=0,nz
    z(k+1)=dz*real(k,WP)
  end do

  ! Create the mid points 
  do i=1,nx
     xm(i)= 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)= 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do

  ! Create the masks
  mask = 0

  return
end subroutine particle_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine particle_data
  use particle
  use parser
  implicit none
  
  ! Allocate the array data
  nvar = 4
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U => data(:,:,:,1); names(1) = 'U'
  V => data(:,:,:,2); names(2) = 'V'
  W => data(:,:,:,3); names(3) = 'W'
  P => data(:,:,:,4); names(4) = 'P'
  
  ! Create them
  U = vg(1)
  V = vg(2)
  W = vg(3)
  P = 0.0_WP
   
  return
end subroutine particle_data


! ================ !
! Create particles !
! ================ !
subroutine particle_pos
  use particle
  use parser
  use math
  use rpt_mod
  implicit none
  
  real(WP) :: rand,p_dx,p_dy,p_dz,time
  integer :: i,j,k,n
  real(WP) :: fluct_amp
  integer :: ierr,file,part_size
  character(len=str_medium) :: filename
  
  ! Write empty initial particle file
  call parser_read('Init part file',filename)
  
  ! Do we use random numbers
  call parser_read('Particle fluctuations',fluct_amp)
  
  ! Open the file to write
  call BINARY_FILE_OPEN(file,trim(adjustl(filename)),"w",ierr)
  
  if (np.eq.0) then
     
     ! Write header corresponding to proc
     part_size = 0
     call BINARY_FILE_WRITE(file,np,1,kind(np),ierr)  
     call BINARY_FILE_WRITE(file,part_size,1,kind(part_size),ierr)
     time = 0.0_WP
     call BINARY_FILE_WRITE(file,time,1,kind(time),ierr) 
          
  else

     ! Allocate particles space
     allocate(part(np))
     
     ! inter-particle distance
     p_dx = Lx/npartx
     p_dy = Ly/nparty
     p_dz = Lz/npartz
     
     ! Populate particle array
     n = 0
     do i=1,npartx
        do j=1,nparty
           do k=1,npartz
              
              ! Increment counter
              n=n+1
              
              ! Particle velocity
              call random_number(rand)
              part(n)%u = 0.0_WP
              if (vp(1).gt.0.0_WP) then
                 part(n)%u = vp(1)+(2.0_WP*rand-1.0_WP)*fluct_amp
              end if
              call random_number(rand)
              part(n)%v = 0.0_WP
              if (vp(2).gt.0.0_WP) then
                 part(n)%v = vp(2)+(2.0_WP*rand-1.0_WP)*fluct_amp
              end if
              call random_number(rand)
              part(n)%w = 0.0_WP
              if (vp(3).gt.0.0_WP) then
                 part(n)%w = vp(3)+(2.0_WP*rand-1.0_WP)*fluct_amp
              end if
              
              ! Particle location
              part(n)%x = x(1)+real(i-0.5_WP,WP)*p_dx
              part(n)%y = y(1)+real(j-0.5_WP,WP)*p_dy
              part(n)%z = z(1)+real(k-0.5_WP,WP)*p_dz
              
              ! Particle diameter
              part(n)%d = diam
           
              ! Drag forces
              part(n)%fcol = 0.0_WP
              part(n)%drag_v = 0.0_WP
              part(n)%drag_p = 0.0_WP
              
           end do
        end do
     end do
     
     ! Write header corresponding to proc
     part_size = 0
     if (np.gt.0) part_size = sizeof(part(1))
     call BINARY_FILE_WRITE(file,np,1,kind(np),ierr)  
     call BINARY_FILE_WRITE(file,part_size,1,kind(part_size),ierr)
     time = 0.0_WP
     call BINARY_FILE_WRITE(file,time,1,kind(time),ierr) 
     
     ! Write local particles
     do n=1,np
        call BINARY_FILE_WRITE(file,part(n),1,part_size,ierr)
     end do
          
  end if
  
  ! Close the file 
  call BINARY_FILE_CLOSE(file,ierr)

  return
end subroutine particle_pos


