module rebound
  use precision
  use param
  implicit none

  ! Length and diameter of the domain
  real(WP) :: Lx,Ly,Lz
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: RHO
  real(WP), dimension(:,:,:), pointer :: dRHO
  
end module rebound


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine rebound_grid
  use rebound
  use parser
  implicit none
  
  integer :: i,j,k
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  
  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  call parser_read('Lz',Lz)

  ! Set the periodicity
  xper=1
  yper=1
  zper=1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*Lx/real(nx,WP)
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*Ly/real(ny,WP) - 0.5_WP*Ly
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*Lz/real(nz,WP) - 0.5_WP*Lz
  end do
  
  ! Create the mid points 
  do i=1,nx
     xm(i)= 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)= 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks - wall on left
  mask = 0
  mask(1,:) = 1
  
  return
end subroutine rebound_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine rebound_data
  use rebound
  use parser
  implicit none

  ! Allocate the array data
  nvar = 6
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U    => data(:,:,:,1); names(1) = 'U'
  V    => data(:,:,:,2); names(2) = 'V'
  W    => data(:,:,:,3); names(3) = 'W'
  P    => data(:,:,:,4); names(4) = 'P'
  RHO  => data(:,:,:,5); names(5) = 'RHO'
  dRHO => data(:,:,:,6); names(6) = 'dRHO'
  
  ! Create them
  U    = 0.0_WP
  V    = 0.0_WP
  W    = 0.0_WP
  P    = 0.0_WP
  RHO  = 0.0_WP
  dRHO = 0.0_WP
  
  return
end subroutine rebound_data


! ========================================= !
! Generate an initial particle distribution !
! ========================================= !
subroutine rebound_part
  use rebound
  use parser
  use lpt_mod
  use random
  implicit none

  character(len=str_medium) :: part_init
  logical :: use_lpt
  integer :: np,iunit,ierr
  real(WP) :: dt,time,rand
  real(WP) :: dpart,upart,vpart,xpart,ypart,zpart

  ! Check whether lpt is used
  call parser_is_defined('Init part file',use_lpt)
  if (.not.use_lpt) return

  ! Read particle parameters
  call parser_read('Particle diameter',dpart)
  call parser_read('Particle u velocity',upart)
  call parser_read('Particle v velocity',vpart)
  call parser_read('Particle x position',xpart)
  call parser_read('Particle y position',ypart)
  call parser_read('Particle z position',zpart)

  ! Allocation part array
  np=1
  allocate(part(np))

  ! Set particle parameters
  part(1)%id  =int8(1)
  part(1)%dt  =0.0_WP
  part(1)%Acol=0.0_WP
  part(1)%stop=0

  part(1)%d=dpart

  part(1)%x=xpart
  part(1)%y=ypart
  part(1)%z=zpart

  part(1)%u=upart
  part(1)%v=vpart
  part(1)%w=0.0_WP

  ! Generate the initial particle file
  call parser_read('Init part file',part_init)
  call BINARY_FILE_OPEN(iunit,trim(part_init),"w",ierr)
  call BINARY_FILE_WRITE(iunit,np,1,kind(np),ierr)
  call BINARY_FILE_WRITE(iunit,part_kind,1,kind(part_kind),ierr)
  dt = 0.0_WP
  call BINARY_FILE_WRITE(iunit,dt,1,kind(dt),ierr)
  time = 0.0_WP
  call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr)
  call BINARY_FILE_WRITE(iunit,part(1),1,part_kind,ierr)
  call BINARY_FILE_CLOSE(iunit,ierr)

  return
end subroutine rebound_part


! ====================== !
! Create the IB variable !
! ====================== !
subroutine rebound_optdata
  use rebound
  use parser
  implicit none
  
  real(WP) :: xwall
  integer :: i,j,k
  
  ! Get wall height
  xwall=x(1) + 2.0_WP*(x(2)-x(1))
  
  ! Gib opt data
  nod = 1
  allocate(OD(nx,ny,nz,nod))
  allocate(OD_names(nod))
  OD_names(1) = 'Gib'
  
  ! Create planar wall
  do i=1,nx
     OD(i,:,:,1) = xm(i) - xwall
  end do
  
  return
end subroutine rebound_optdata
