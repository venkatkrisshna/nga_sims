module conv_sinusoid
  use string
  use precision
  use param
  implicit none
  
  ! Size of the domain
  real(WP) :: Lx,Ly,Lz
  real(WP) :: dx,dy,dz
  real(WP) :: sx,sy
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:,:), pointer :: VOF
  
end module conv_sinusoid

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine conv_sinusoid_grid
  use conv_sinusoid
  use parser
  use math
  use random
  implicit none
  
  integer  :: i,j,k
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  
  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  call parser_read('Lz',Lz)
  
  ! Set the periodicity
  xper = 1
  yper = 1
  zper = 1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  dx = Lx/real(nx,WP)
  dy = Ly/real(ny,WP)
  dz = Lz/real(nz,WP)
  do i=1,nx+1
     x(i) = real(i-1,WP)*dx-0.5_WP*Lx
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*dy-0.5_WP*Ly
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*dz-0.5_WP*Lz
  end do

  do i=1,nx
     xm(i)= 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j)= 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k)= 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine conv_sinusoid_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine conv_sinusoid_data
  use conv_sinusoid
  use parser
  use math
  implicit none
  
  integer :: i,j,k
  character(len=str_short) :: direction
  logical :: use_multiphase

  call parser_read('Use multiphase',use_multiphase,.false.)

  if (use_multiphase) then
     ! Allocate the array data
     nvar = 11
     allocate(data(nx,ny,nz,nvar))
     allocate(names(nvar))

     ! Link the pointers
     U => data(:,:,:,1); names(1) = 'U'
     V => data(:,:,:,2); names(2) = 'V'
     W => data(:,:,:,3); names(3) = 'W'
     VOF  => data(:,:,:, 4:11);
     names( 4) = 'VOF1'
     names( 5) = 'VOF2'
     names( 6) = 'VOF3'
     names( 7) = 'VOF4'
     names( 8) = 'VOF5'
     names( 9) = 'VOF6'
     names(10) = 'VOF7'
     names(11) = 'VOF8'
     VOF=1.0_WP
  else
     ! Allocate the array data
     nvar = 4
     allocate(data(nx,ny,nz,nvar))
     allocate(names(nvar))

     ! Link the pointers
     U => data(:,:,:,1); names(1) = 'U'
     V => data(:,:,:,2); names(2) = 'V'
     W => data(:,:,:,3); names(3) = 'W'
     P => data(:,:,:,3); names(4) = 'P'
     P=0.0_WP
  end if
  
  ! Set be convective velocity
  call parser_read('Orientation',direction)
  select case (trim(direction))
  case('x')
     do k=1,nz
        do j=1,ny
           do i=1,nx
              V(i,j,k) = 1.0_WP-2.0_WP*cos(2.0_WP*pi*y (j))*sin(2.0_WP*pi*zm(k))
              W(i,j,k) = 1.0_WP+2.0_WP*sin(2.0_WP*pi*ym(j))*cos(2.0_WP*pi*z (k))
           end do
        end do
     end do
  case('y')
     do k=1,nz
        do j=1,ny
           do i=1,nx
              W(i,j,k) = 1.0_WP-2.0_WP*cos(2.0_WP*pi*z (k))*sin(2.0_WP*pi*xm(i))
              U(i,j,k) = 1.0_WP+2.0_WP*sin(2.0_WP*pi*zm(k))*cos(2.0_WP*pi*x (i))
           end do
        end do
     end do
  case('z')
     do k=1,nz
        do j=1,ny
           do i=1,nx
              U(i,j,k) = 1.0_WP-2.0_WP*cos(2.0_WP*pi*x (i))*sin(2.0_WP*pi*ym(j))
              V(i,j,k) = 1.0_WP+2.0_WP*sin(2.0_WP*pi*xm(i))*cos(2.0_WP*pi*y (j))
           end do
        end do
     end do
  end select
  
  return
end subroutine conv_sinusoid_data

