module tjunction
  use precision
  use param
  implicit none
  
  ! Domain size
  real(WP) :: Lx,Lz,Hc,Ht,Lt,Ly
  real(WP) :: dx,dy,dz
  
  ! Filter region
  real(WP) :: filter_thickness
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: RHO
  real(WP), dimension(:,:,:), pointer :: dRHO
  
  ! Pointer to variable in optdata
  real(WP), dimension(:,:,:), pointer :: iperm
  real(WP), dimension(:,:,:), pointer :: poros
  
end module tjunction

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine tjunction_grid
  use tjunction
  use parser
  implicit none
  
  integer :: i,j,k
  logical :: use_twall
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny); ny=ny+1
  call parser_read('nz',nz)
  call parser_read('Channel length',Lx)
  call parser_read('Channel width',Lz)
  call parser_read('Junction length',Lt)
  call parser_read('Channel height',Hc)
  call parser_read('Junction height',Ht)
  Ly=Hc+Ht
  call parser_read('Use top wall',use_twall)
  
  ! Set the periodicity
  xper = 0
  yper = 0
  zper = 1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid size
  dx=Lx/real(nx,WP)
  dy=Ly/real(ny-1,WP); Ly=Hc+Ht+dy
  if (nz.eq.1) Lz=min(dx,dy)
  dz=Lz/real(nz,WP)
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*dx-0.5_WP*Lx
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*dy-Ht
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*dz-0.5_WP*Lz
  end do
  
  ! Create the mid points
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask=0
  if (use_twall) mask(:,ny)=1
  do j=1,ny-1
     do i=1,nx
        if (abs(xm(i)).gt.0.5_WP*Lt.and.ym(j).lt.0.0_WP) then
           mask(i,j)=1
        end if
     end do
  end do
  
  return
end subroutine tjunction_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine tjunction_data
  use tjunction
  use parser
  implicit none
  
  integer :: i,j,k
  real(WP) :: rho0,permeability,porosity
  
  ! Allocate the array data
  nvar = 6
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U    => data(:,:,:,1); names(1) = 'U'
  V    => data(:,:,:,2); names(2) = 'V'
  W    => data(:,:,:,3); names(3) = 'W'
  P    => data(:,:,:,4); names(4) = 'P'
  RHO  => data(:,:,:,5); names(5) = 'RHO'
  dRHO => data(:,:,:,6); names(6) = 'dRHO'
  
  ! Create them
  U    = 0.0_WP
  V    = 0.0_WP
  W    = 0.0_WP
  P    = 0.0_WP
  call parser_read('Density',rho0,1.0_WP)
  RHO  = rho0
  RHO  = 1.0_WP
  dRHO = 0.0_WP
  
  ! Optional data file for porous coefficient
  nod = 2
  allocate(OD(nx,ny,nz,nod))
  allocate(OD_names(nod))
  
  ! Link the pointer
  iperm => OD(:,:,:,1); OD_names(1) = 'IPERM'
  poros => OD(:,:,:,2); OD_names(2) = 'POROS'
  
  ! Create filter region
  call parser_read('Filter thickness',filter_thickness)
  if (filter_thickness.gt.0.0_WP) then
     call parser_read('Permeability',permeability)
     call parser_read('Porosity',porosity)
     iperm=0.0_WP; poros=1.0_WP
     if (permeability.gt.0.0_WP) then
        do k=1,nz
           do j=1,ny
              do i=1,nx
                 if (abs(xm(i)).lt.0.5_WP*Lt.and.ym(j).lt.0.0_WP.and.ym(j).gt.-filter_thickness) then
                    iperm(i,j,k)=1.0_WP/permeability
                    poros(i,j,k)=porosity
                 end if
              end do
           end do
        end do
     end if
  end if
  
  return
end subroutine tjunction_data


! ========================================= !
! Generate an initial particle distribution !
! ========================================= !
subroutine tjunction_part
  use tjunction
  use parser
  use lpt_mod
  use random
  use math
  implicit none
  
  character(len=str_medium) :: part_init
  logical :: use_lpt
  integer :: iunit,ierr
  integer :: i,np
  real(WP) :: VFavg
  real(WP) :: dt,time,rand
  real(WP) :: fvol,pvol,fpd
  
  ! Check whether lpt is used
  call parser_is_defined('Init part file',use_lpt)
  if (.not.use_lpt) return
  
  ! Read average volume fraction and bed height
  call parser_read('Avg. volume fraction',VFavg)
  call parser_read('Filter part diameter',fpd)
  
  ! Initialize the random number generator
  call random_init
  
  ! Compute volume of filter region
  !fvol=filter_thickness*Lt*Lz
  fvol=2.0_WP*dy*Lt*Lz
  
  ! Estimate number of particles
  pvol = Pi/6.0_WP*fpd**3
  np = int(VFavg*fvol/pvol)
  
  ! Allocate particle size array
  allocate(part(np))
  
  ! Set particle parameters
  do i=1,np
     
     ! Set various parameters for the particle
     part(i)%dt  =0.0_WP
     part(i)%d   =fpd
     part(i)%u   =0.0_WP
     part(i)%v   =0.0_WP
     part(i)%w   =0.0_WP
     part(i)%Acol=0.0_WP
     part(i)%Tcol=0.0_WP
     part(i)%wx  =0.0_WP
     part(i)%wy  =0.0_WP
     part(i)%wz  =0.0_WP
     part(i)%id  =int(0,kind=8) ! Inactive particle
     part(i)%stop=0
     
     ! Distribute particles
     call random_number(rand)
     part(i)%x = +Lt*(rand-0.5_WP)
     call random_number(rand)
     !part(i)%y = -(filter_thickness-fpd)*rand-0.5_WP*fpd
     part(i)%y = -(2.0_WP*dy-fpd)*rand-0.5_WP*fpd
     call random_number(rand)
     part(i)%z = +Lz*(rand-0.5_WP)
     
  end do
  
  ! Output
  print*,'Number of particles: ',np
  print*,'Effective mean diameter: ',fpd
  print*,'Effective volume fraction: ',VFavg
  
  ! Generate the initial particle file
  call parser_read('Init part file',part_init)
  call BINARY_FILE_OPEN(iunit,trim(part_init),"w",ierr)
  call BINARY_FILE_WRITE(iunit,np,1,kind(np),ierr)
  call BINARY_FILE_WRITE(iunit,part_kind,1,kind(part_kind),ierr)
  dt = 0.0_WP
  call BINARY_FILE_WRITE(iunit,dt,1,kind(dt),ierr)
  time = 0.0_WP
  call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr)
  do i=1,np
     call BINARY_FILE_WRITE(iunit,part(i),1,part_kind,ierr)
  end do
  call BINARY_FILE_CLOSE(iunit,ierr)
  
  return
end subroutine tjunction_part

