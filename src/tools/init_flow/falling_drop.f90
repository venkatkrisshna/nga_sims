module falling_drop
  use string
  use precision
  use param
  implicit none
  
  ! Domain
  real(WP) :: Lx,Ly,Lz,dx,dy,dz
  character(len=str_medium) :: dim
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:),   pointer :: U
  real(WP), dimension(:,:,:),   pointer :: V
  real(WP), dimension(:,:,:),   pointer :: W
  real(WP), dimension(:,:,:,:), pointer :: VOF
  
end module falling_drop


! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine falling_drop_grid
  use falling_drop
  use parser
  implicit none
  
  integer :: i,j,k
  logical :: btm_wall
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)
  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  call parser_read('Lz',Lz)
  xper=1
  yper=1
  zper=1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*Lx/real(nx,WP)-0.5_WP*Lx
  end do
  do j=1,ny+1
     y(j) = real(j-2,WP)*Ly/real(ny,WP)
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*Lz/real(nz,WP)-0.5_WP*Lz
  end do
  dx=Lx/real(nx,WP)
  dy=Ly/real(ny,WP)
  dz=Lz/real(nz,WP)
  
  ! Create the cell centered grid
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  !mask(:,1) = 1
  
  return
end subroutine falling_drop_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine falling_drop_data
  use falling_drop
  use parser
  use math
  implicit none
  
  real(WP) :: dradius,pheight
  real(WP), dimension(3) :: dcenter,Vdrop
  real(WP) :: myx,myy,myz,dist_drop,dist_pool
  integer :: s,i,j,k,ii,jj,kk
  integer, parameter :: nF=20
  
  ! Allocate the array data
  nvar = 11
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U   => data(:,:,:,1); names(1) = 'U'
  V   => data(:,:,:,2); names(2) = 'V'
  W   => data(:,:,:,3); names(3) = 'W'
  VOF  => data(:,:,:, 4:11);
  names( 4) = 'VOF1'
  names( 5) = 'VOF2'
  names( 6) = 'VOF3'
  names( 7) = 'VOF4'
  names( 8) = 'VOF5'
  names( 9) = 'VOF6'
  names(10) = 'VOF7'
  names(11) = 'VOF8'
  
  ! Initialize
  U = 0.0_WP
  V = 0.0_WP
  W = 0.0_WP
  VOF=0.0_WP
  
  ! Set the initial distance field
  call parser_read('Drop radius',dradius,0.0_WP)
  call parser_read('Drop center',dcenter)
  call parser_read('Drop velocity',Vdrop)
  call parser_read('Pool height',pheight,0.0_WP)
  do k=1,nz
     do j=1,ny
        do i=1,nx

           ! Compute distance from pool and drop
           dist_pool = pheight - ym(j)
           dist_drop = sqrt((xm(i)-dcenter(1))**2+(ym(j)-dcenter(2))**2+(zm(k)-dcenter(3))**2) - dradius
           
           ! Compute VOF
           if (abs(dist_drop).le.abs(dist_pool)) then
              
              ! Droplet
              if (abs(dist_drop).lt.2.0_WP*max(dx,dy,dz)) then ! Close to interface, compute VOF on fine mesh
                 do s=1,8
                    do kk=1,nF
                       do jj=1,nF
                          do ii=1,nF
                             myx=xs1(s,i)+(real(ii,WP)-0.5_WP)/real(nf,WP)*(xs2(s,i)-xs1(s,i))
                             myy=ys1(s,j)+(real(jj,WP)-0.5_WP)/real(nf,WP)*(ys2(s,j)-ys1(s,j))
                             myz=zs1(s,k)+(real(kk,WP)-0.5_WP)/real(nf,WP)*(zs2(s,k)-zs1(s,k))
                             if (dradius.gt.sqrt((myx-dcenter(1))**2+(myy-dcenter(2))**2+(myz-dcenter(3))**2)) &
                                  VOF(i,j,k,s)=VOF(i,j,k,s)+1.0_WP
                          end do
                       end do
                    end do
                    VOF(i,j,k,s)=VOF(i,j,k,s)/real(nF,WP)**3
                 end do
              else ! Away from interface, VOF = 0 or 1
                 if (dist_drop.lt.0.0_WP) then
                    VOF(i,j,k,:)=1.0_WP
                 else
                    VOF(i,j,k,:)=0.0_WP
                 end if
              end if
              
 !          else
              
              ! Pool
!              if (dist_pool.lt.dx) then ! Close to interface, compute VOF on fine mesh
!                 VOF1(i,j,k)=0.0_WP
!                 VOF2(i,j,k)=0.0_WP
!                 VOF3(i,j,k)=0.0_WP
!                 VOF4(i,j,k)=0.0_WP
!                 VOF5(i,j,k)=0.0_WP
!                 VOF6(i,j,k)=0.0_WP
!                 VOF7(i,j,k)=0.0_WP
!                 VOF8(i,j,k)=0.0_WP
!                 do jj=1,nF
!                    myy=y(j)+real(jj,WP)/real(nf+1,WP)*(y(j+1)-ym(j))
!                    if (pheight-myy.gt.0.0_WP) VOF1(i,j,k)=VOF1(i,j,k)+1.0_WP
!                    myy=y(j)+real(jj,WP)/real(nf+1,WP)*(y(j+1)-ym(j))
!                    if (pheight-myy.gt.0.0_WP) VOF2(i,j,k)=VOF2(i,j,k)+1.0_WP
!                    myy=ym(j)+real(jj,WP)/real(nf+1,WP)*(y(j+1)-ym(j))
!                    if (pheight-myy.gt.0.0_WP) VOF3(i,j,k)=VOF3(i,j,k)+1.0_WP
!                    myy=ym(j)+real(jj,WP)/real(nf+1,WP)*(y(j+1)-ym(j))
!                    if (pheight-myy.gt.0.0_WP) VOF4(i,j,k)=VOF4(i,j,k)+1.0_WP
!                    myy=y(j)+real(jj,WP)/real(nf+1,WP)*(y(j+1)-ym(j))
!                    if (pheight-myy.gt.0.0_WP) VOF5(i,j,k)=VOF5(i,j,k)+1.0_WP
!                    myy=y(j)+real(jj,WP)/real(nf+1,WP)*(y(j+1)-ym(j))
!                    if (pheight-myy.gt.0.0_WP) VOF6(i,j,k)=VOF6(i,j,k)+1.0_WP
!                    myy=ym(j)+real(jj,WP)/real(nf+1,WP)*(y(j+1)-ym(j))
!                    if (pheight-myy.gt.0.0_WP) VOF7(i,j,k)=VOF7(i,j,k)+1.0_WP
!                    myy=ym(j)+real(jj,WP)/real(nf+1,WP)*(y(j+1)-ym(j))
!                    if (pheight-myy.gt.0.0_WP) VOF8(i,j,k)=VOF8(i,j,k)+1.0_WP
!
!                 end do
!                 VOF1(i,j,k)=VOF1(i,j,k)/real(nF,WP)
!                 VOF2(i,j,k)=VOF2(i,j,k)/real(nF,WP)
!                 VOF3(i,j,k)=VOF3(i,j,k)/real(nF,WP)
!                 VOF4(i,j,k)=VOF4(i,j,k)/real(nF,WP)
!                 VOF5(i,j,k)=VOF5(i,j,k)/real(nF,WP)
!                 VOF6(i,j,k)=VOF6(i,j,k)/real(nF,WP)
!                 VOF7(i,j,k)=VOF7(i,j,k)/real(nF,WP)
!                 VOF8(i,j,k)=VOF8(i,j,k)/real(nF,WP)
!              else ! Away from interface, VOF = 0 or 1
!                 if (dist_pool.lt.0.0_WP) then
!                    VOF1(i,j,k)=0.0_WP
!                    VOF2(i,j,k)=0.0_WP
!                    VOF3(i,j,k)=0.0_WP
!                    VOF4(i,j,k)=0.0_WP
!                    VOF5(i,j,k)=0.0_WP
!                    VOF6(i,j,k)=0.0_WP
!                    VOF7(i,j,k)=0.0_WP
!                    VOF8(i,j,k)=0.0_WP
!                 else
!                    VOF1(i,j,k)=1.0_WP
!                    VOF2(i,j,k)=1.0_WP
!                    VOF3(i,j,k)=1.0_WP
!                    VOF4(i,j,k)=1.0_WP
!                    VOF5(i,j,k)=1.0_WP
!                    VOF6(i,j,k)=1.0_WP
!                    VOF7(i,j,k)=1.0_WP
!                    VOF8(i,j,k)=1.0_WP
!                 end if
!              end if
           end if

        end do
     end do
  end do


  ! do k=1,nz
  !   do j=2,ny-1
  !     do i=2,nx-1

  !       ! Compute distance from drop and set velocity
  !       dist_drop = dradius - sqrt((xm(i)-dcenter(1))**2+(ym(j)-dcenter(2))**2+(zm(k)-dcenter(3))**2)
  !       VOFhere  = VOF1(i,j,k)+VOF2(i,j,k)+VOF3(i,j,k)+VOF4(i,j,k)+VOF5(i,j,k)+VOF6(i,j,k)+VOF7(i,j,k)+VOF8(i,j,k)
  !       VOFup    = VOF1(i,j+1,k)+VOF2(i,j+1,k)+VOF3(i,j+1,k)+VOF4(i,j+1,k)+VOF5(i,j+1,k)+VOF6(i,j+1,k)+VOF7(i,j+1,k)+VOF8(i,j+1,k)
  !       VOFdown  = VOF1(i,j-1,k)+VOF2(i,j-1,k)+VOF3(i,j-1,k)+VOF4(i,j-1,k)+VOF5(i,j-1,k)+VOF6(i,j-1,k)+VOF7(i,j-1,k)+VOF8(i,j-1,k)
  !       VOFleft  = VOF1(i-1,j,k)+VOF2(i-1,j,k)+VOF3(i-1,j,k)+VOF4(i-1,j,k)+VOF5(i-1,j,k)+VOF6(i-1,j,k)+VOF7(i-1,j,k)+VOF8(i-1,j,k)
  !       VOFright = VOF1(i+1,j,k)+VOF2(i+1,j,k)+VOF3(i+1,j,k)+VOF4(i+1,j,k)+VOF5(i+1,j,k)+VOF6(i+1,j,k)+VOF7(i+1,j,k)+VOF8(i+1,j,k)
  !       if (dist_drop.gt.-dx) then
  !           if(VOFhere.gt.0.0_WP .or. VOFup.gt.0.0_WP .or. VOFdown.gt.0.0_WP .or. VOFleft.gt.0.0_WP .or. VOFright.gt.0.0_WP) then
  !               V(i,j,k) = -Vdrop
  !           end if
  !       end if
  !     end do
  !   end do
  ! end do

  do k=1,nz
     do j=1,ny
        do i=1,nx
           U(i,j,k)=Vdrop(1)*0.125_WP*sum(VOF(i,j,k,:))
           V(i,j,k)=Vdrop(2)*0.125_WP*sum(VOF(i,j,k,:))
           W(i,j,k)=Vdrop(3)*0.125_WP*sum(VOF(i,j,k,:))
        end do
     end do
  end do

  return
end subroutine falling_drop_data
