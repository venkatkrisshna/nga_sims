module spurious_currents
  use string
  use precision
  use param
  implicit none
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:),   pointer :: U
  real(WP), dimension(:,:,:),   pointer :: V
  real(WP), dimension(:,:,:),   pointer :: W
  real(WP), dimension(:,:,:,:), pointer :: VOF
  
contains
  ! subroutine init_random_seed()
  !   implicit none
  !   integer, allocatable :: seed(:)
  !   integer :: i, n, un, istat, dt(8), pid
  !   integer :: t

  !   call random_seed(size = n)
  !   allocate(seed(n))
  !   ! First try if the OS provides a random number generator
  !   open(newunit=un, file="/dev/urandom", access="stream", &
  !        form="unformatted", action="read", status="old", iostat=istat)
  !   if (istat == 0) then
  !      read(un) seed
  !      close(un)
  !   else
  !      ! Fallback to XOR:ing the current time and pid. The PID is
  !      ! useful in case one launches multiple instances of the same
  !      ! program in parallel.
  !      call system_clock(t)
  !      if (t == 0) then
  !         call date_and_time(values=dt)
  !         t = (dt(1) - 1970) * 365 * 24 * 60 * 60 * 1000 &
  !              + dt(2) * 31 * 24 * 60 * 60 * 1000 &
  !              + dt(3) * 24 * 60 * 60 * 1000 &
  !              + dt(5) * 60 * 60 * 1000 &
  !              + dt(6) * 60 * 1000 + dt(7) * 1000 &
  !              + dt(8)
  !      end if
  !      pid = getpid()
  !      t = ieor(t, int(pid, kind(t)))
  !      do i = 1, n
  !         seed(i) = lcg(t)
  !      end do
  !   end if
  !   call random_seed(put=seed)
  ! contains
  !   ! This simple PRNG might not be good enough for real work, but is
  !   ! sufficient for seeding a better PRNG.
  !   function lcg(s)
  !     integer :: lcg
  !     integer :: s
  !     if (s == 0) then
  !        s = 104729
  !     else
  !        s = mod(s, 429496)
  !     end if
  !     s = mod(s * 2794, 429496)
  !     lcg = int(mod(s, int(huge(0))), kind(0))
  !   end function lcg
  ! end subroutine init_random_seed
  
end module spurious_currents

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine spurious_currents_grid
  use spurious_currents
  use parser
  use math
  implicit none
  
  integer :: i,j,k
  logical :: is3D
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('Three dimensional',is3D,.false.)
  ny = nx
  if (is3D) then
     nz=nx
  else ! 2D
     nz = 1
  end if
  xper = 1
  yper = 0
  zper = 1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)/real(nx,WP)-0.5_WP
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)/real(nx,WP)-0.5_WP
  end do
  if (is3D) then
     do k=1,nz+1
        z(k) = real(k-1,WP)/real(nx,WP)-0.5_WP
     end do
  else ! 2D
     do k=1,nz+1
        z(k) = real(k-1,WP)/real(nx,WP)-0.5_WP/real(nx,WP)
     end do
  end if
  
  ! Create the cell centered grid
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine spurious_currents_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine spurious_currents_data
  use spurious_currents
  use parser
  use math
  implicit none
  
  integer :: s,i,j,k,ii,jj,kk
  real(WP) :: myx,myy,myz
  integer, parameter :: nF=50
  real(WP) :: dx_,G
  real(WP), parameter :: rad=0.2_WP
  real(WP) :: x_1,x_2
  real(WP) :: x_min,x_max,y_min,y_max,yint
  integer :: xcycl,ycycl
  logical :: xdone,ydone
  real(WP) :: xo,yo,ran

  ! Random circle location
  ! if (irank.eq.iroot) then
  !    call init_random_seed()
  !    call random_number(ran)
  !    xo=(x(2)-x(1))*(ran-0.5_WP);
  !    call random_number(ran)
  !    yo=(x(2)-x(1))*(ran-0.5_WP);
  !    print *, '(xo,yo)=', xo,yo
  ! end if
  ! call parallel_bc(xo)
  ! call parallel_bc(yo)
  xo=0.0_WP
  yo=0.0_WP
  
  ! Allocate the array data
  nvar = 11
  allocate(data(imin_:imax_,jmin_:jmax_,kmin_:kmax_,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U    (imin_:imax_,jmin_:jmax_,kmin_:kmax_) => data(:,:,:, 1); names( 1) = 'U'
  V    (imin_:imax_,jmin_:jmax_,kmin_:kmax_) => data(:,:,:, 2); names( 2) = 'V'
  W    (imin_:imax_,jmin_:jmax_,kmin_:kmax_) => data(:,:,:, 3); names( 3) = 'W'
  VOF  (imin_:imax_,jmin_:jmax_,kmin_:kmax_,1:8) => data(:,:,:, 4:11);
  names( 4) = 'VOF1'
  names( 5) = 'VOF2'
  names( 6) = 'VOF3'
  names( 7) = 'VOF4'
  names( 8) = 'VOF5'
  names( 9) = 'VOF6'
  names(10) = 'VOF7'
  names(11) = 'VOF8'
  
  ! Initialize
  U   = 0.0_WP
  V   = 0.0_WP
  W   = 0.0_WP
  VOF = 0.0_WP

  ! Check if 2D or 3D
  if (nz.gt.1) then ! 3D
     ! Setup the interface 
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_

              dx_=max(x(i+1)-x(i),y(j+1)-y(j),z(k+1)-z(k))
              G = rad-sqrt(xm(i)**2+ym(j)**2+zm(k)**2)
              if      ( G.gt.+2.0_WP*dx_) then ! Liquid phase
                 VOF(i,j,k,:) = 1.0_WP
              else if ( G.lt.-2.0_WP*dx_) then ! Gas phase
                 VOF(i,j,k,:) = 0.0_WP
              else                                 ! Near interface
                 do s=1,8
                    do kk=1,nF
                       do jj=1,nF
                          do ii=1,nF
                             myx=xs1(s,i)+(real(ii,WP)-0.5_WP)/real(nf,WP)*(xs2(s,i)-xs1(s,i))
                             myy=ys1(s,j)+(real(jj,WP)-0.5_WP)/real(nf,WP)*(ys2(s,j)-ys1(s,j))
                             myz=zs1(s,k)+(real(kk,WP)-0.5_WP)/real(nf,WP)*(zs2(s,k)-zs1(s,k))
                             G = 0.2_WP-sqrt(myx**2+myy**2+myz**2)
                             if (G.gt.0.0_WP) VOF(i,j,k,s)=VOF(i,j,k,s)+1.0_WP
                          end do
                       end do
                    end do
                    VOF(i,j,k,s)=VOF(i,j,k,s)/real(nF,WP)**3
                 end do
              end if
           end do
        end do
     end do

  else ! 2D use exact integrations
     
     ! Setup the interface 
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_

              dx_=max(x(i+1)-x(i),y(j+1)-y(j),z(k+1)-z(k))
              G = rad-sqrt(xm(i)**2+ym(j)**2)
              if      ( G.gt.+2.0_WP*dx_) then ! Liquid phase
                 VOF(i,j,k,:) = 1.0_WP
              else if ( G.lt.-2.0_WP*dx_) then ! Gas phase
                 VOF(i,j,k,:) = 0.0_WP
              else                                 ! Near interface
                 do s=1,8
                    xdone=.false.
                    xcycl=1
                    do while (.not.xdone)
                       ! Split cell into parts located within the 1st quadrant
                       if ((xs1(s,i)-xo)*(xs2(s,i)-xo).lt.0.0_WP) then
                          ! Cell needs to be split into two
                          if (xcycl.eq.1) then
                             x_min=0.0_WP
                             x_max=abs(xs1(s,i)-xo)
                             xcycl=2
                          else
                             x_min=0.0_WP
                             x_max=abs(xs2(s,i)-xo)
                             xdone=.true.
                          end if
                       else
                          x_min=min(abs(xs1(s,i)-xo),abs(xs2(s,i)-xo))
                          x_max=max(abs(xs1(s,i)-xo),abs(xs2(s,i)-xo))
                          xdone=.true.
                       end if

                       ydone=.false.
                       ycycl=1
                       do while (.not.ydone)
                          if ((ys1(s,j)-yo)*(ys2(s,j)-yo).lt.0.0_WP) then
                             if (ycycl.eq.1) then
                                y_min=0.0_WP
                                y_max=abs(ys1(s,j)-yo)
                                ycycl=2
                             else
                                y_min=0.0_WP
                                y_max=abs(ys2(s,j)-yo)
                                ydone=.true.
                             end if
                          else
                             y_min=min(abs(ys1(s,j)-yo),abs(ys2(s,j)-yo))
                             y_max=max(abs(ys1(s,j)-yo),abs(ys2(s,j)-yo))
                             ydone=.true.
                          end if

                          ! Find Integration bounds
                          if (rad**2-x_min**2.gt.0.0_WP) then
                             yint=sqrt(rad**2-x_min**2)
                             if (yint.lt.y_min) then 
                                ! Cell in gas phase
                                VOF(i,j,k,s)=VOF(i,j,k,s)+0.0_WP
                                cycle
                             else if (yint.lt.y_max) then
                                ! Intersection on left face
                                x_1=x_min
                             else
                                ! Intersection on top face
                                x_1=sqrt(rad**2-y_max**2)
                             end if
                          else
                             ! Cell in gas phase
                             VOF(i,j,k,s)=VOF(i,j,k,s)+0.0_WP
                             cycle
                          end if

                          if (rad**2-x_max**2.gt.0.0_WP) then
                             yint=sqrt(rad**2-x_max**2)
                             if (yint.gt.y_max) then 
                                ! Cell in liquid phase
                                VOF(i,j,k,s)=VOF(i,j,k,s)+((x_max-x_min)*(y_max-y_min))/((xs2(s,i)-xs1(s,i))*(ys2(s,j)-ys1(s,j)))
                                cycle
                             else if (yint.gt.y_min) then
                                ! Intersection on right face
                                x_2=x_max
                             else
                                ! Intersection on bottom face
                                x_2=sqrt(rad**2-y_min**2)
                             end if
                          else
                             ! Intersection on bottom face
                             x_2=sqrt(rad**2-y_min**2)
                          end if

                          ! Integrate 
                          VOF(i,j,k,s)= VOF(i,j,k,s) + ( (x_1-x_min)*(y_max-y_min) + &
                               (rad**2*asin(x_2/rad))/2.0_WP - (rad**2*asin(x_1/rad))/2.0_WP &
                               - (x_1*(rad**2 - x_1**2)**(0.5_WP))/2.0_WP &
                               + (x_2*(rad**2 - x_2**2)**(0.5_WP))/2.0_WP &
                               - (y_min*(x_2-x_1)) &
                               ) &
                               / ((xs2(s,i)-xs1(s,i))*(ys2(s,j)-ys1(s,j)))
                          ! 2nd order VOF using trapizoidal rule
                          ! VOF(i,j,k,s) = VOF(i,j,k,s) + ( &
                          !      + (x_1-x_min)*(y_max-y_min) &
                          !      + (x_2-x_1)*(sqrt(rad**2-x_2**2)-y_min) &
                          !      + 0.5_WP*(x_2-x_1)*(sqrt(rad**2-x_1**2)-sqrt(rad**2-x_2**2)) &
                          !      ) / ((xs2(s,i)-xs1(s,i))*(ys2(s,j)-ys1(s,j)))

                       end do
                    end do
                 end do
              end if
           end do
        end do
     end do
  end if
        
  return
end subroutine spurious_currents_data
