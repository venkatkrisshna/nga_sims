program init_flow
  use string
  use parser
  use param
  implicit none

  character(len=str_medium) :: simulation
  character(len=str_medium) :: input_name
  logical                   :: isparallel

  ! Parse the command line
  call get_command_argument(1,input_name)
  
  ! Initialize the parser
  call parser_init()
  
  ! Read the input file
  call parser_parsefile(input_name)
  
  ! Detect the config
  call parser_readchar('Simulation',simulation)

  ! Check if using parallel init_flow
  call parser_read('Parallel init',isparallel,.false.)
  if (isparallel) then
     call parallel_init
     call parallel_init_topology(0,0,0)
     call parallel_init_io
  end if
  
  select case (trim(simulation))
  case ('Taylor vortex')
     call taylor_grid
     call taylor_data
  case ('Multiphase Taylor vortex')
     call taylor_multiphase_grid
     call taylor_multiphase_data
  case ('von Karman')
     call vonkarman_grid
     call vonkarman_data
  case ('vortex convection')
     call conv_vortex_grid
     call conv_vortex_data
  case ('sinusoid convection')
     call conv_sinusoid_grid
     call conv_sinusoid_data
  case ('scalar convection')
     call conv_scalar_grid
     call conv_scalar_data
  case('isotropic turbulence')
     call dns_box_grid
     call dns_box_data
     call dns_box_chemtable
     call dns_box_part
  case ('channel')
     call channel_grid
     call channel_data
  case ('pipe')
     call pipe_grid
     call pipe_data
  case ('cylindrical jet')
     call jetcyl_grid
     call jetcyl_data
  case ('cartesian jet')
     call jetcart_grid
     call jetcart_data
  case ('mixing layer')
     call mixing_grid
     call mixing_data
  case ('spatial mixing layer')
     call spatial_mixing_grid
     call spatial_mixing_inflow
     call spatial_mixing_data
  case ('laminar flame')
     call laminar_flame_grid
     call laminar_flame_data
  case ('Lamb vortex')
     call lamb_vortex_grid
     call lamb_vortex_data
  case ('density vortex')
     call density_vortex_grid
     call density_vortex_data
     call density_vortex_chemtable
  case ('spray')
     call spray_grid
     call spray_data
  case ('Zalesak')
     call zalesak_grid
     call zalesak_data
  case ('zy_Zalesak')
     call zy_zalesak_grid
     call zy_zalesak_data
  case ('zalesak cyl')
     call zalesak_cyl_grid
     call zalesak_cyl_data
  case ('evaporation DNS')
     call evap_DNS_grid
     call evap_DNS_data
  case ('boundary layer')
     call boundary_layer_grid
     call boundary_layer_data
  case ('spray combustion')
     call spray_combustion_grid
     call spray_combustion_data
  case ('gfm test')
     call gfm_test_grid
     call gfm_test_data
  case ('Marmottant')
     call marmottant_grid
     call marmottant_data
  case ('atomization')
     call atom_grid
     call parallel_geometry
     call atom_inflow
     call atom_data
  case ('atomization angled')
     call atom_angled_grid
     call parallel_geometry
     call atom_angled_inflow
     call atom_angled_data
  case ('levelset test')
     call levelset_test_grid
     call levelset_test_data
  case ('milk crown')
     call milk_crown_grid
     call milk_crown_data
  case ('deformation')
     call deformation_grid
     call deformation_data
  case ('3d deformation')
     call deformation_3d_grid
     call parallel_geometry
     call deformation_3d_data
  case ('Rayleigh-Taylor')
     call rayleigh_taylor_grid
     call rayleigh_taylor_data
     call rayleigh_taylor_chemtable
  case ('cartesian round jet')
     call round_cart_jet_grid
     call round_cart_jet_data
  case ('diesel DNS')
     call diesel_dns_grid
     call diesel_dns_data
  case ('energy conservation')
     call energy_cons_grid
     call energy_cons_data
  case ('levelset cyl test')
     call levelset_cyl_test_grid
     call levelset_cyl_test_data
  case ('vortex ring')
     call vortex_ring_grid
     call vortex_ring_data
  case ('multiphase vortex ring')
     call multiphase_vortex_ring_grid
     call multiphase_vortex_ring_data
  case ('multiphase normals')
     call multiphase_normals_grid
     call multiphase_normals_data
  case ('Rayleigh-Taylor vortex')
     call rt_vortex_grid
     call rt_vortex_data
  case ('Rayleigh instability')
     call rayleigh_grid
     call rayleigh_data
  case ('wave')
     call wave_grid
     call wave_data
  case ('spurious currents')
     call spurious_currents_grid
     call parallel_geometry
     call spurious_currents_data
  case ('oscillating droplet')
     call oscil_drop_grid
     call parallel_geometry
     call oscil_drop_data
  case ('curvature')
     call curvature_grid
     call curvature_data
  case ('detailed ignition chemistry')
     call ignition_grid
     call ignition_data
  case ('Helium Plume')
     call helium_plume_grid
     call helium_plume_data
     call helium_plume_optdata
  case ('blowing')
     call blowing_grid
     call blowing_data
  case ('air layer')
     call air_layer_grid
     call air_layer_data
  case ('electrospray')
     call electrospray_grid
     call parallel_geometry
     call electrospray_inflow
     call electrospray_data
  case ('grating')
     call grating_grid
     call grating_data
  case ('bubble')
     call bubble_grid
     call bubble_data
  case ('drop collision')
     call drop_coll_grid
     call drop_coll_data
  case ('glass')
     call glass_grid
     call glass_data
  case ('density ratio')
     call density_ratio_grid
     call density_ratio_data
  case ('multiphase channel')
     call multiphase_channel_grid
     call multiphase_channel_inflow
     call multiphase_channel_data
  case ('multiphase shear layer')
     call mp_shear_layer_grid
     call mp_shear_layer_data
  case ('rpt')
     call particle_grid
     call particle_data
     call particle_pos
  case ('fluidized bed')
     call fbmixing_grid
     call fbmixing_data
     call fbmixing_optdata
     call fbmixing_part
     call fbmixing_inflow
  case ('part rebound')
     call rebound_grid
     call rebound_data
     call rebound_part
     call rebound_optdata
  case ('IB pipe')
     call ib_pipe_grid
     call ib_pipe_data
     call ib_pipe_optdata
  case ('duct')
     call duct_grid
     call duct_data
     call duct_part
  case ('STL')
     call ib_stl_grid
     call parallel_geometry
     call ib_stl_data
     call ib_stl_inflow
  case ('IB duct')
     call ib_duct_grid
     call ib_duct_data
     call ib_duct_optdata
     call ib_duct_part
  case ('IB file')
     call ib_file_grid
     call ib_file_data
  case ('myjet')
     call myjet_grid
     call myjet_inflow
     call myjet_data
  case ('homogeneous ignition')
     call homogeneous_grid
     call homogeneous_data
  case('triple flame')
     call triple_flame_grid
     call triple_flame_data
     call triple_flame_inflow
  case ('filter')
     call filter_grid
     call filter_data
  case ('vortex particle')
     call vortex_particle_grid
     call vortex_particle_data
     call vortex_particle_part
  case ('splitter plate')
     call splitter_grid
     call splitter_data
     call splitter_inflow
     call splitter_optdata
  case ('freefalling drop')
     call freefalling_drop_grid
     call freefalling_drop_data
  case ('falling drop')
     call falling_drop_grid
     call falling_drop_data
 case ('ehd')
     call ehd_grid
     call ehd_data
  case ('Jet in crossflow')
     call jet_crossflow_grid
     call parallel_geometry
     call jet_crossflow_data
     call jet_crossflow_inflow
  case ('partfilter')
     call partfilter_grid
     call partfilter_data
     call partfilter_part
  case ('homogeneous riser')
     call riser_grid
     call parallel_geometry
     call riser_data
     call riser_part
  case ('kelvin helmholtz')
     call kelvin_helmholtz_grid
     call kelvin_helmholtz_data
  case ('hydrojump')
     call hydrojump_grid
     call hydrojump_data
     call hydrojump_inflow
  case ('random velocity')
     call random_vel_grid
     call random_vel_data
  case ('bedload')
     call bedload_grid
     call bedload_data
     call bedload_part
  case ('jetflame')
     call jetflame_grid
     call jetflame_data
  case ('Sandia flame')
     call sandiaflame_grid
     call sandiaflame_data
  case ('T junction')
     call tjunction_grid
     call tjunction_data
     call tjunction_part
  case ('volcano')
     call volcano_grid
     call volcano_data
     call volcano_part
     call volcano_inflow
  case ('square')
     call square_grid
     call square_data
  case ('Aussillous')
     call aussillous_grid
     call aussillous_data
     call aussillous_part
  case ('HIT interface')
     call hit_interface_grid
     call hit_interface_data
  case ('HITA')
     call HITA_grid
     call HITA_data
  case ('HITA_VOF')
     call HITA_VOF_grid
     call HITA_VOF_data
  case ('partacoustics')
     call partacoustics_grid
     call partacoustics_data
     call partacoustics_part
  case ('contact line')
     call contact_line_grid
     call parallel_geometry
     call contact_line_data
  case ('contact line dynamic')
     call contact_line_dyn_grid
     call parallel_geometry
     call contact_line_dyn_data
  case ('rain')
     call rain_grid
     call rain_data
  case default
     print*, "Unknown simulation", simulation
     print*, "Assuming a 2D Gambit mesh file is present"
     call gambit_grid
     call gambit_data
     call gambit_optdata
  end select
  
  ! Write the files
  if (isparallel) then
     call param_parwrite_config(simulation)
     call param_parwrite_data
     call param_parwrite_optdata
     call param_parwrite_inflow
     call param_parwrite_inflowy
     call param_parwrite_chemtable
     call parallel_final
  else
     call param_write_config(simulation)
     call param_write_data
     call param_write_optdata
     call param_write_inflow
     call param_write_inflowy
     call param_write_chemtable
  end if

contains
  ! ========================================== !
  ! Routine to initialize parallel environment !
  ! for init_flow routines                     !
  ! ========================================== !
  subroutine parallel_geometry
    use parallel
    implicit none

    integer, parameter :: nover=0 
    integer :: q,r
    
    if (isparallel) then
       ! Set bounds for i,j,k
       imin=1; imax=nx
       jmin=1; jmax=ny
       kmin=1; kmax=nz

       ! Set initial partitions along x
       if (npx.gt.nx) call parallel_kill('partition_init: nx has to be greater or equal to npx')
       q = nx/npx
       r = mod(nx,npx)
       if (iproc<=r) then
          nx_   = q+1
          imin_ = imin + (iproc-1)*(q+1)
       else
          nx_   = q
          imin_ = imin + r*(q+1) + (iproc-r-1)*q
       end if
       nxo_   = nx_ + 2*nover
       imax_  = imin_ + nx_ - 1
       imino_ = imin_ - nover
       imaxo_ = imax_ + nover

       ! Set initial partitions along y
       if (npy.gt.ny) call parallel_kill('partition_init: ny has to be greater or equal to npy')
       q = ny/npy
       r = mod(ny,npy)
       if (jproc<=r) then
          ny_   = q+1
          jmin_ = jmin + (jproc-1)*(q+1)
       else
          ny_   = q
          jmin_ = jmin + r*(q+1) + (jproc-r-1)*q
       end if
       nyo_   = ny_ + 2*nover
       jmax_  = jmin_ + ny_ - 1
       jmino_ = jmin_ - nover
       jmaxo_ = jmax_ + nover

       ! Set initial partitions along z
       if (npz.gt.nz) call parallel_kill('partition_init: nz has to be greater or equal to npz')
       q = nz/npz
       r = mod(nz,npz)
       if (kproc<=r) then
          nz_   = q+1
          kmin_ = kmin + (kproc-1)*(q+1)
       else
          nz_   = q
          kmin_ = kmin + r*(q+1) + (kproc-r-1)*q
       end if
       nzo_   = nz_ + 2*nover
       kmax_  = kmin_ + nz_ - 1
       kmino_ = kmin_ - nover
       kmaxo_ = kmax_ + nover
    else
       irank=1; iroot=1
       nx_ =nx; ny_ =ny; nz_ =nz
       nxo_=nx; nyo_=ny; nzo_=nz
       imin=1; imax=nx; imin_=1; imax_=nx; imino_=1; imaxo_=nx
       jmin=1; jmax=ny; jmin_=1; jmax_=ny; jmino_=1; jmaxo_=ny
       kmin=1; kmax=nz; kmin_=1; kmax_=nz; kmino_=1; kmaxo_=nz   
    end if
  
    return
  end subroutine parallel_geometry

end program

