module jet_crossflow
  use precision
  use param
  use parallel
  implicit none

  ! Domain dimensions
  real(WP) :: dx,dy,dz
  real(WP) :: Lx,Ly,Lz
  real(WP) :: x0,y0,z0

  ! Pointers to variable in data
  real(WP), dimension(:,:,:),   pointer :: U
  real(WP), dimension(:,:,:),   pointer :: V
  real(WP), dimension(:,:,:),   pointer :: W
  real(WP), dimension(:,:,:,:), pointer :: VOF

  ! Initial flow
  real(WP) :: Vjet,Djet ! y inflow
  real(WP) :: Umean     ! x inflow

end module jet_crossflow

! =============== !
! Create the mesh !
! =============== !
subroutine jet_crossflow_grid
  use jet_crossflow
  use parser
  implicit none
  integer :: i,j,k

  ! Periodicity
  xper=0
  yper=0
  zper=1

  ! Cartesian
  icyl=0

  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny)
  call parser_read('nz',nz)

  call parser_read('Lx',Lx)
  call parser_read('Ly',Ly)
  call parser_read('Lz',Lz)

  dx = Lx/real(nx,WP)
  dy = Ly/real(ny,WP)
  dz = Lz/real(nz,WP)

  call parser_read('x0',x0,0.5_WP*Lx)
  call parser_read('y0',y0,0.5_WP*Ly)
  call parser_read('z0',z0,0.5_WP*Lz)

  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))

  ! Create the grid
  do i=1,nx+1
     x(i) = real(i-1,WP)*dx-x0
  end do
  do j=1,ny+1
     y(j) = real(j-1,WP)*dy-y0
  end do
  do k=1,nz+1
     z(k) = real(k-1,WP)*dz-z0
  end do

  ! Create the cell centered grid
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do

  ! Create the masks
  mask = 0

  return
end subroutine jet_crossflow_grid

! ========================= !
! Create the variable array !
! ========================= !
subroutine jet_crossflow_data
  use jet_crossflow
  use parser
  implicit none

  integer :: s,i,j,k,ii,jj,kk
  real(WP) :: r
  integer, parameter :: nF=20
  real(WP) :: myx,myy,myz

  ! Read input file
  call parser_read('V jet' ,Vjet)
  call parser_read('D jet' ,Djet)
  call parser_read('U mean',Umean)

  ! Allocate the array data
  nvar = 11
  allocate(data(imin_:imax_,jmin_:jmax_,kmin_:kmax_,nvar))
  allocate(names(nvar))

  ! Link the pointers
  U    (imin_:imax_,jmin_:jmax_,kmin_:kmax_) => data(:,:,:, 1); names( 1) = 'U'
  V    (imin_:imax_,jmin_:jmax_,kmin_:kmax_) => data(:,:,:, 2); names( 2) = 'V'
  W    (imin_:imax_,jmin_:jmax_,kmin_:kmax_) => data(:,:,:, 3); names( 3) = 'W'
  VOF  (imin_:imax_,jmin_:jmax_,kmin_:kmax_,1:8) => data(:,:,:, 4:11);
  names( 4) = 'VOF1'
  names( 5) = 'VOF2'
  names( 6) = 'VOF3'
  names( 7) = 'VOF4'
  names( 8) = 'VOF5'
  names( 9) = 'VOF6'
  names(10) = 'VOF7'
  names(11) = 'VOF8'

  ! Create them
  U   = 0.0_WP
  V   = 0.0_WP
  W   = 0.0_WP
  VOF = 0.0_WP

  ! Initialize interface
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           
           ! Check if close to the interface
           if (abs(sqrt(xm(i)**2+ym(j)**2+zm(k)**2)-0.5_WP*Djet) &
                .le.2.0_WP*max(x(i+1)-x(i),y(j+1)-y(j),z(k+1)-z(k))) then
              ! Loop over subcells
              do s=1,8
                 ! Compute VOF on finer mesh
                 VOF(i,j,k,s)=0.0_WP
                 do kk=1,nF
                    do jj=1,nF
                       do ii=1,nF
                          ! x,y,z position
                          myx=xs1(s,i)+(real(ii,WP)-0.5_WP)/real(nf,WP)*(xs2(s,i)-xs1(s,i))
                          myy=ys1(s,j)+(real(jj,WP)-0.5_WP)/real(nf,WP)*(ys2(s,j)-ys1(s,j))
                          myz=zs1(s,k)+(real(kk,WP)-0.5_WP)/real(nf,WP)*(zs2(s,k)-zs1(s,k))
                          ! In jet?
                          r=sqrt(myx**2+myy**2+myz**2)
                          if (r.lt.0.5_WP*Djet) &
                               VOF(i,j,k,s)=VOF(i,j,k,s)+1.0_WP ! in jet
                       end do
                    end do
                 end do
                 VOF(i,j,k,s)=VOF(i,j,k,s)/real(nF,WP)**3
              end do
           else ! away from interface
              r=sqrt(xm(i)**2+ym(j)**2+zm(k)**2)
              if (r.lt.0.5_WP*Djet) then
                 VOF(i,j,k,:)=1.0_WP ! inside jet
              else
                 VOF(i,j,k,:)=0.0_WP ! outside jet
              end if
           end if
        end do
     end do
  end do
  
  return
end subroutine jet_crossflow_data

! =================== !
! Generate the inflow !
! =================== !
subroutine jet_crossflow_inflow
  use jet_crossflow
  use parser
  implicit none

  real(WP) :: r
  integer  :: s,i,j,k,n,ii,kk
  integer, parameter :: nF=20
  real(WP) :: myx,myz
  real(WP), dimension(22) :: Up,yp

  ! Inflow x : based on scaled profile
  ! ==================================
  nvar_inflow = 11
  ntime = 2

  ! Allocate some arrays
  allocate(inflow(ntime,jmin_:jmax_,kmin_:kmax_,nvar_inflow))
  allocate(names_inflow(nvar_inflow))

  ! Link the pointers
  U    (1:ntime,jmin_:jmax_,kmin_:kmax_) => inflow(:,:,:, 1); names_inflow( 1) = 'U'
  V    (1:ntime,jmin_:jmax_,kmin_:kmax_) => inflow(:,:,:, 2); names_inflow( 2) = 'V'
  W    (1:ntime,jmin_:jmax_,kmin_:kmax_) => inflow(:,:,:, 3); names_inflow( 3) = 'W'
  VOF  (1:ntime,jmin_:jmax_,kmin_:kmax_,1:8) => inflow(:,:,:, 4:11);
  names_inflow( 4) = 'VOF1'
  names_inflow( 5) = 'VOF2'
  names_inflow( 6) = 'VOF3'
  names_inflow( 7) = 'VOF4'
  names_inflow( 8) = 'VOF5'
  names_inflow( 9) = 'VOF6'
  names_inflow(10) = 'VOF7'
  names_inflow(11) = 'VOF8'

  ! Compute time grid
  time_inflow = 0.0_WP
  dt_inflow   = 1.0_WP

  ! Prepare the profiles
  U   = 0.0_WP
  V   = 0.0_WP
  W   = 0.0_WP
  VOF = 0.0_WP

  ! Populate profile using experimental data
  yp=(/ 0.0_WP,0.0375_WP,0.05_WP,0.0625_WP,0.075_WP, &
       0.1_WP,0.125_WP,0.175_WP,0.225_WP,0.275_WP,0.4_WP,0.525_WP, &
       0.775_WP,1.025_WP,1.525_WP,2.025_WP,3.025_WP,4.025_WP, &
       5.025_WP,6.025_WP,7.025_WP,huge(1.0_WP) /) ! mm
  yp=0.001_WP*yp ! mm -> m
  Up=(/ 0.0_WP,0.443822735_WP,0.474636321_WP,0.496435799_WP, &
       0.514219886_WP,0.539833541_WP,0.551526304_WP,0.583457978_WP, &
       0.604619363_WP,0.626263226_WP,0.678696104_WP,0.70845522_WP,  &
       0.787336096_WP,0.840191241_WP,0.909831152_WP,0.946344051_WP, &
       0.974873991_WP,0.988501126_WP,0.997831268_WP,1.002190372_WP, &
       1.0_WP, 1.0_WP /) ! m/s

  ! Define the velocity
  do j=jmin_,jmax_     
     profile:do n=1,21
        if (yp(n+1).ge.ym(j)) then
           U(:,j,:) = Umean*(Up(n)+(Up(n+1)-Up(n))* &
                (ym(j)-yp(n))/(yp(n+1)-yp(n)))
           exit profile
        end if
     end do profile
  end do

  ! Inflow y : bulk jet inflow
  ! ===============================
  nvar_inflowy = 11
  ntimey = 2

  ! Allocate some arrays
  allocate(inflowy(ntimey,imin_:imax_,kmin_:kmax_,nvar_inflowy))
  allocate(names_inflowy(nvar_inflowy))

  ! Link the pointers
  U    (1:ntime,jmin_:jmax_,kmin_:kmax_) => inflowy(:,:,:, 1); names_inflowy( 1) = 'U'
  V    (1:ntime,jmin_:jmax_,kmin_:kmax_) => inflowy(:,:,:, 2); names_inflowy( 2) = 'V'
  W    (1:ntime,jmin_:jmax_,kmin_:kmax_) => inflowy(:,:,:, 3); names_inflowy( 3) = 'W'
  VOF  (1:ntime,jmin_:jmax_,kmin_:kmax_,1:8) => inflowy(:,:,:, 4:11);
  names_inflowy( 4) = 'VOF1'
  names_inflowy( 5) = 'VOF2'
  names_inflowy( 6) = 'VOF3'
  names_inflowy( 7) = 'VOF4'
  names_inflowy( 8) = 'VOF5'
  names_inflowy( 9) = 'VOF6'
  names_inflowy(10) = 'VOF7'
  names_inflowy(11) = 'VOF8'

  ! Compute time grid
  time_inflowy = 0.0_WP
  dt_inflowy   = 1.0_WP

  ! Prepare the profiles
  U   = 0.0_WP
  V   = 0.0_WP
  W   = 0.0_WP
  VOF = 0.0_WP

  ! Initialize interface
  do k=kmin_,kmax_
     do i=imin_,imax_
        ! Check if close to the interface
        if (abs(sqrt(xm(i)**2+zm(k)**2)-0.5_WP*Djet) &
             .le.2.0_WP*max(x(i+1)-x(i),z(k+1)-z(k))) then
           ! Loop over subcells
           do s=1,8
              ! Compute VOF on finer mesh
              do kk=1,nF
                 do ii=1,nF
                    ! x,z position
                    myx=xs1(s,i)+(real(ii,WP)-0.5_WP)/real(nf,WP)*(xs2(s,i)-xs1(s,i))
                    myz=zs1(s,k)+(real(kk,WP)-0.5_WP)/real(nf,WP)*(zs2(s,k)-zs1(s,k))
                    ! In jet?
                    r=sqrt(myx**2+myz**2)
                    if (r.lt.0.5_WP*Djet) &
                         VOF(:,i,k,s)=VOF(:,i,k,s)+1.0_WP ! in jet
                 end do
              end do
              VOF(:,i,k,s)=VOF(:,i,k,s)/real(nF,WP)**2
           end do
        else ! away from interface
           r=sqrt(xm(i)**2+zm(k)**2)
           if (r.lt.0.5_WP*Djet) then
              VOF(:,i,k,:)=1.0_WP ! inside jet
           else
              VOF(:,i,k,:)=0.0_WP ! outside jet
           end if
        end if
     end do
  end do

  ! Initialize V velocity
  do k=kmin_,kmax_
     do i=imin_,imax_
        ! Loop over time
        do n=1,ntimey
           V(n,i,k)=Vjet*0.125_WP*sum(VOF(n,i,k,:))
        end do
     end do
  end do

  return
end subroutine jet_crossflow_inflow
