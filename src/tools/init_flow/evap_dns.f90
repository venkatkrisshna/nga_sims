module evap_dns
  use precision
  use param
  use string
  implicit none
  
  include 'fftw3.f'
  
  ! Spray file
  character(len=str_medium) :: part_init
  
  ! Length of the domain
  real(WP) :: L
  real(WP) :: dx
  integer :: dims
  character(len=str_medium) :: data_type
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:), pointer :: P
  real(WP), dimension(:,:,:), pointer :: ZMIX
  real(WP), dimension(:,:,:), pointer :: RHO
  real(WP), dimension(:,:,:), pointer :: dRHO
  real(WP), dimension(:,:,:), pointer :: T
  real(WP), dimension(:,:,:), pointer :: Yf
  real(WP), dimension(:,:,:), pointer :: Yo
  real(WP), dimension(:,:,:), pointer :: Yp
  
  ! Molecular weight
  real(WP), parameter :: W_C7H16  = 100.0e-3_WP   ! [kg/mol]
  real(WP), parameter :: W_O2     = 32.0e-3_WP    ! [kg/mol]
  real(WP), parameter :: W_N2     = 28.0e-3_WP    ! [kg/mol]
  real(WP), parameter :: W_CO2    = 44.0e-3_WP    ! [kg/mol]
  real(WP), parameter :: W_H2O    = 18.0e-3_WP    ! [kg/mol]
  real(WP), parameter :: W_air    = 0.21_WP*W_O2+0.79_WP*W_N2   ! [kg/mol]
  real(WP), parameter :: W_f  = 100.0e-3_WP    ! [kg/mol]
  real(WP), parameter :: W_o  =  32.0e-3_WP    ! [kg/mol]
  real(WP), parameter :: W_d  =  28.0e-3_WP    ! [kg/mol]
  real(WP), parameter :: W_p  = 452.0e-3_WP    ! [kg/mol]
  real(WP), parameter :: nu_f =  1.0_WP
  real(WP), parameter :: nu_o = 11.0_WP
  real(WP), parameter :: nu_p =  1.0_WP
  
  ! Ideal gas constant
  real(WP), parameter :: R_cst = 8.314472_WP      ! [J/(mol.K)]
  real(WP) :: rho_init
  
end module evap_dns

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine evap_dns_grid
  use evap_dns
  use parser
  implicit none
  
  integer :: i,j,k
  
  ! Read in the size of the domain
  call parser_read('Number of points',nx)
  call parser_read('Dimensionality',dims)
  call parser_read('Domain size',L)
  ny = nx
  if (dims.eq.3) then
     nz = nx
  else if (dims.eq.2) then
     nz = 1
  else
     stop 'Wrong dimensionality'
  end if
  
  ! Set the periodicity
  xper = 1
  yper = 1
  zper = 1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  dx = L/real(nx,WP)
  do i=1,nx+1
     x(i) = (i-1)*dx
  end do
  do j=1,ny+1
     y(j) = (j-1)*dx
  end do
  do k=1,nz+1
     z(k) = (k-1)*dx
  end do
  
  ! Create the mid points
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  
  return
end subroutine evap_dns_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine evap_dns_data
  use evap_dns
  use lpt_mod
  use random
  use parser
  use fileio
  use string
  implicit none
  
  ! Turbulent velocity
  real(WP) :: Ut
  
  ! Spectrum type
  character(len=str_short) :: spectrum
  real(WP) :: le,ld
  real(WP) :: epsilon
  
  ! Spectrum computation
  real(WP) :: psr,ps1,ps2
  real(WP) :: ke,kd,ks,dk,ksk0ratio,kc,kcksratio,kk,kx,ky,kz,kk2
  real(WP) :: alpha,spec_amp,eps,amp_disc
  integer  :: nk
  real(WP) :: e_total,energy_spec
  real(WP), dimension(:,:), pointer :: spect
  complex(WP), dimension(:,:,:), pointer :: ak,bk
  
  ! Other
  integer :: i,j,k,ik,iunit,dim
  complex(WP) :: ii=(0.0_WP,1.0_WP)
  real(WP) :: rand,pi
  real(WP) :: P_init,T_init
  
  ! Fourier coefficients
  integer(KIND=8) :: plan_r2c,plan_c2r
  complex(WP), dimension(:,:,:), pointer :: Uk,Vk,Wk
  complex(WP), dimension(:,:,:), pointer :: Cbuf
  real(WP), dimension(:,:,:), pointer :: Rbuf
  real(WP) :: f_phi
  
  ! Spray
  real(WP) :: T_part,rho_part,nparcel_part,d_part
  real(WP) :: dt,time,cdiam,local_spray_distro
  integer :: ierr,icount,ip,jp,kp
  character(len=str_medium) :: distro
  real(WP), dimension(:,:,:), pointer :: spray_distro
  
!!$  ! Initialize the random number generator
!!$  call random_init
!!$  
!!$  ! Decide how to generate the input file
!!$  call parser_read('Data type',data_type)
!!$  
!!$  ! Allocate the array data
!!$  select case (trim(data_type))
!!$  case ('passive mixing')
!!$     nvar = 4
!!$     allocate(data(nx,ny,nz,nvar))
!!$     allocate(names(nvar))
!!$     names = ''
!!$     data = 0.0_WP
!!$     U    => data(:,:,:,1);  names(1)  = 'U'
!!$     V    => data(:,:,:,2);  names(2)  = 'V'
!!$     W    => data(:,:,:,3);  names(3)  = 'W'
!!$     P    => data(:,:,:,4);  names(4)  = 'P'
!!$  case ('passive evaporation')
!!$     nvar = 5
!!$     allocate(data(nx,ny,nz,nvar))
!!$     allocate(names(nvar))
!!$     names = ''
!!$     data = 0.0_WP
!!$     U    => data(:,:,:,1);  names(1)  = 'U'
!!$     V    => data(:,:,:,2);  names(2)  = 'V'
!!$     W    => data(:,:,:,3);  names(3)  = 'W'
!!$     P    => data(:,:,:,4);  names(4)  = 'P'
!!$     ZMIX => data(:,:,:,5);  names(5)  = 'ZMIX'
!!$  case ('reacting')
!!$     nvar = 7
!!$     allocate(data(nx,ny,nz,nvar))
!!$     allocate(names(nvar))
!!$     names = ''
!!$     data = 0.0_WP
!!$     U    => data(:,:,:,1); names(1)= 'U'
!!$     V    => data(:,:,:,2); names(2)= 'V'
!!$     W    => data(:,:,:,3); names(3)= 'W'
!!$     P    => data(:,:,:,4); names(4)= 'P'
!!$     ZMIX => data(:,:,:,5); names(5)= 'ZMIX'
!!$     RHO  => data(:,:,:,6); names(6)= 'RHO'
!!$     dRHO => data(:,:,:,7); names(7)= 'dRHO'
!!$  case ('one-step')
!!$     nvar = 10
!!$     allocate(data(nx,ny,nz,nvar))
!!$     allocate(names(nvar))
!!$     names = ''
!!$     data = 0.0_WP
!!$     U    => data(:,:,:,1);  names(1) = 'U'
!!$     V    => data(:,:,:,2);  names(2) = 'V'
!!$     W    => data(:,:,:,3);  names(3) = 'W'
!!$     P    => data(:,:,:,4);  names(4) = 'P'
!!$     Yf   => data(:,:,:,5);  names(5) = 'Yf'
!!$     Yo   => data(:,:,:,6);  names(6) = 'Yo'
!!$     Yp   => data(:,:,:,7);  names(7) = 'Yp'
!!$     T    => data(:,:,:,8);  names(8) = 'T'
!!$     RHO  => data(:,:,:,9);  names(9) = 'RHO'
!!$     dRHO => data(:,:,:,10); names(10)= 'dRHO'
!!$  end select
!!$  
!!$  ! Read spectrum parameters
!!$  call parser_read('Fluctuations',Ut)
!!$  call parser_read('Spectrum form',spectrum)
!!$  call parser_read('Energetic scale',le)
!!$  if (trim(spectrum).eq.'VKP') then
!!$     call parser_read('Dissipative scale',ld)
!!$     call parser_read('Dissipation',epsilon)
!!$  end if
!!$  
!!$  ! Create pi
!!$  pi = acos(-1.0_WP)
!!$  
!!$  if (dims.eq.3) then
!!$  
!!$  ! =================
!!$  ! Velocity Spectrum
!!$  
!!$  ! Spectrum computation
!!$  ke = 2.0_WP*pi/le
!!$  if (trim(spectrum).eq.'VKP') kd = 2.0_WP*pi/ld
!!$  dk = 2.0_WP*pi/L
!!$  kc = real(nx/2,WP)*dk
!!$  eps=ke/1000000.0_WP
!!$  if (trim(spectrum).eq.'PP') then
!!$     spec_amp = 16.0_WP*sqrt(2.0_WP/pi)*Ut**2/ke
!!$  else if (trim(spectrum).eq.'VKP') then
!!$     alpha=1.5_WP
!!$     spec_amp = 1.5_WP*Ut**5/epsilon
!!$  end if
!!$  amp_disc = sqrt(dk)**3
!!$  
!!$  ! Compute spectrum
!!$  nk = nx/2+1
!!$  allocate(ak(nk,ny,nz),bk(nk,ny,nz))
!!$  do k=1,nz
!!$     do j=1,ny
!!$        do i=1,nk
!!$           ! Random numbers
!!$           call random_number(rand)
!!$           psr=2.0_WP*pi*(rand-0.5_WP)
!!$           call random_number(rand)
!!$           ps1=2.0_WP*pi*(rand-0.5_WP)
!!$           call random_number(rand)
!!$           ps2=2.0_WP*pi*(rand-0.5_WP)
!!$           ! Wavenumbers
!!$           kx=real(i-1,WP)*dk
!!$           ky=real(j-1,WP)*dk
!!$           if (j.gt.nk) ky=-real(nx+1-j,WP)*dk
!!$           kz=real(k-1,WP)*dk
!!$           if (k.gt.nk) kz=-real(nx+1-k,WP)*dk
!!$           kk=sqrt(kx**2+ky**2+kz**2)
!!$           ! Spectrums
!!$           if (trim(spectrum).eq.'PP') then
!!$              energy_spec=spec_amp*(kk/ke)**4*exp(-2.0_WP*(kk/ke)**2)
!!$           else if (trim(spectrum).eq.'VKP') then
!!$              energy_spec=spec_amp*(kk/ke)**4/(1.0_WP+(kk/ke)**2)**(17.0_WP/6.0_WP)*exp(-1.5_WP*alpha*(kk/kd)**(4.0_WP/3.0_WP))
!!$           end if
!!$           ! Coeff
!!$           ak(i,j,k)=0.0_WP
!!$           bk(i,j,k)=0.0_WP
!!$           if ((kk.gt.eps).and.(kk.le.kc)) then
!!$              ak(i,j,k)=amp_disc*sqrt(energy_spec/(2.0_WP*pi*kk**2))*exp(ii*ps1)*cos(psr)
!!$              bk(i,j,k)=amp_disc*sqrt(energy_spec/(2.0_WP*pi*kk**2))*exp(ii*ps2)*sin(psr)
!!$           end if
!!$        end do
!!$     end do
!!$  end do
!!$  
!!$  ! Output spectrum for comparison
!!$  allocate(spect(2,nx+1))
!!$  e_total=0.0_WP
!!$  do ik=1,nx+1
!!$     kk = dk*(ik-1)
!!$     spect(1,ik)=kk
!!$     if (trim(spectrum).eq.'PP') then
!!$        energy_spec=spec_amp*(kk/ke)**4*exp(-2.0_WP*(kk/ke)**2)
!!$     else if (trim(spectrum).eq.'VKP') then
!!$        energy_spec=spec_amp*(kk/ke)**4/(1.0_WP+(kk/ke)**2)**(17.0_WP/6.0_WP)*exp(-1.5_WP*alpha*(kk/kd)**(4.0_WP/3.0_WP))
!!$     end if
!!$     if ((kk.gt.eps).and.(kk.le.kc)) then
!!$        spect(2,ik) = energy_spec
!!$        e_total=e_total+dk*energy_spec
!!$     else
!!$        spect(2,ik) = 0.0_WP
!!$     end if
!!$  end do
!!$  iunit=iopen()
!!$  open(iunit,file='spectrum.analytic',form='formatted')
!!$  do i=1,nx+1
!!$     if ((spect(1,i).ne.0.0_WP).and.(spect(2,i).ne.0.0_WP)) then
!!$        write(11,*) spect(1,i),',',spect(2,i)  
!!$     end if
!!$  end do
!!$  close(iclose(iunit))
!!$  
!!$  ! Compute 3D velocity field
!!$  allocate(Uk(nk,ny,nz))
!!$  allocate(Vk(nk,ny,nz))
!!$  allocate(Wk(nk,ny,nz))
!!$  Uk=(0.0_WP,0.0_WP)
!!$  Vk=(0.0_WP,0.0_WP)
!!$  Wk=(0.0_WP,0.0_WP)
!!$  
!!$  do dim=1,3
!!$     
!!$     ! Compute the Fourier coefficients
!!$     do k=1,nz
!!$        do j=1,ny
!!$           do i=1,nk
!!$              
!!$              ! Wavenumbers
!!$              kx=real(i-1,WP)*dk
!!$              ky=real(j-1,WP)*dk
!!$              if (j.gt.nk) ky=-real(nx+1-j,WP)*dk
!!$              kz=real(k-1,WP)*dk
!!$              if (k.gt.nk) kz=-real(nx+1-k,WP)*dk
!!$              kk =sqrt(kx**2+ky**2+kz**2)
!!$              kk2=sqrt(kx**2+ky**2)
!!$              
!!$              ! Compute the Fourier coefficients
!!$              if ((kk.gt.eps).and.(kk.le.kc)) then
!!$                 if (dim.eq.1) then
!!$                    if (kk2.lt.eps) then
!!$                       Uk(i,j,k)=(ak(i,j,k)+bk(i,j,k))/sqrt(2.0_WP)
!!$                    else
!!$                       Uk(i,j,k)=(ak(i,j,k)*kk*ky+bk(i,j,k)*kx*kz)/(kk*kk2)
!!$                    end if
!!$                 end if
!!$                 if (dim.eq.2) then
!!$                    if (kk2.lt.eps) then
!!$                       Vk(i,j,k)=(bk(i,j,k)-ak(i,j,k))/sqrt(2.0_WP)
!!$                    else
!!$                       Vk(i,j,k)=(bk(i,j,k)*ky*kz-ak(i,j,k)*kk*kx)/(kk*kk2)
!!$                    end if
!!$                 end if
!!$                 if (dim.eq.3) Wk(i,j,k)=-bk(i,j,k)*kk2/kk
!!$              end if
!!$              
!!$           end do
!!$        end do
!!$     end do
!!$     
!!$  end do
!!$  
!!$  ! Oddball
!!$  do k=2,nx
!!$     do j=nk+1,nx
!!$        Uk(1,j,k)=conjg(Uk(1,ny+2-j,nz+2-k))
!!$        Vk(1,j,k)=conjg(Vk(1,ny+2-j,nz+2-k))
!!$        Wk(1,j,k)=conjg(Wk(1,ny+2-j,nz+2-k))
!!$     end do
!!$  end do
!!$  do k=nk+1,nx
!!$     Uk(1,1,k)=conjg(Uk(1,1,nz+2-k))
!!$     Vk(1,1,k)=conjg(Vk(1,1,nz+2-k))
!!$     Wk(1,1,k)=conjg(Wk(1,1,nz+2-k))
!!$  end do
!!$  
!!$  ! Inverse Fourier transform
!!$  allocate(Cbuf(nk,ny,nz))
!!$  allocate(Rbuf(nx,ny,nz))
!!$  call dfftw_plan_dft_c2r_3d(plan_c2r,nx,ny,nz,Cbuf,Rbuf,FFTW_ESTIMATE)
!!$  call dfftw_plan_dft_r2c_3d(plan_r2c,nx,ny,nz,Rbuf,Cbuf,FFTW_ESTIMATE)
!!$  
!!$  ! Execute the plans
!!$  do k=1,nz
!!$     do j=1,ny
!!$        do i=1,nk
!!$           Cbuf(i,j,k) = Uk(i,j,k)
!!$        end do
!!$     end do
!!$  end do
!!$  call dfftw_execute(plan_c2r)
!!$  do k=1,nz
!!$     do j=1,ny
!!$        do i=1,nx
!!$           U(i,j,k) = Rbuf(i,j,k)
!!$        end do
!!$     end do
!!$  end do
!!$  do k=1,nz
!!$     do j=1,ny
!!$        do i=1,nk
!!$           Cbuf(i,j,k) = Vk(i,j,k)
!!$        end do
!!$     end do
!!$  end do
!!$  call dfftw_execute(plan_c2r)
!!$  do k=1,nz
!!$     do j=1,ny
!!$        do i=1,nx
!!$           V(i,j,k) = Rbuf(i,j,k)
!!$        end do
!!$     end do
!!$  end do
!!$  do k=1,nz
!!$     do j=1,ny
!!$        do i=1,nk
!!$           Cbuf(i,j,k) = Wk(i,j,k)
!!$        end do
!!$     end do
!!$  end do
!!$  call dfftw_execute(plan_c2r)
!!$  do k=1,nz
!!$     do j=1,ny
!!$        do i=1,nz
!!$           W(i,j,k) = Rbuf(i,j,k)
!!$        end do
!!$     end do
!!$  end do
!!$  
!!$  ! Clean up
!!$  deallocate(Uk)
!!$  deallocate(Vk)
!!$  deallocate(Wk)
!!$  deallocate(ak)
!!$  deallocate(bk)
!!$  
!!$  else
!!$  
!!$  ! Random number initialization for 2D
!!$  do i=1,nx
!!$     do j=1,ny
!!$        call random_number(rand)
!!$        U(i,j,1) = 0.01_WP*(2.0_WP*rand-1.0_WP)
!!$        call random_number(rand)
!!$        V(i,j,1) = 0.01_WP*(2.0_WP*rand-1.0_WP)
!!$        W(i,j,1) = 0.0_WP
!!$     end do
!!$  end do
!!$  U=U-sum(U)/real(nx*ny*nz,WP)
!!$  V=V-sum(V)/real(nx*ny*nz,WP)
!!$  end if
!!$  
!!$  ! At this stage, we have U,V,W set-up correctly
!!$  ! Still to do for evaporation DNS:
!!$  !  - read in the density => create RHO
!!$  select case (trim(data_type))
!!$  case ('passive mixing')
!!$     rho_init = 1.0_WP
!!$  case ('passive evaporation')
!!$     rho_init = 1.0_WP
!!$     !  - ZMIX is initially 0 everywhere
!!$     ZMIX = 0.0_WP
!!$  case ('reacting')
!!$     call parser_read('Initial temperature',T_init)
!!$     call parser_read('Initial pressure',P_init)
!!$     rho_init = (P_init*W_air)/(R_cst*T_init)
!!$     print*,'Initial rho value =',rho_init
!!$     RHO  = rho_init
!!$     !  - P=0 is all right since we have to run a little bit to get physical turbulence
!!$     P = 0.0_WP
!!$     !  - ZMIX is initially 0 everywhere
!!$     ZMIX = 0.0_WP
!!$  case ('one-step')
!!$     call parser_read('Initial temperature',T_init)
!!$     T = T_init
!!$     call parser_read('Initial pressure',P_init)
!!$     Yf = 0.0_WP
!!$     Yo = 0.233_WP
!!$     Yp = 0.0_WP
!!$     rho_init = (P_init*W_air)/(R_cst*T_init)
!!$     RHO = rho_init
!!$     dRHO = 0.0_WP
!!$  end select
!!$  
!!$  !  - Need to initialize the spray
!!$  call parser_read('Initial distribution',distro,'none')
!!$  if (trim(distro).ne.'none') then
!!$     call parser_read('Number of drops',npart)
!!$     allocate(part(npart))
!!$     call parser_read('Drop density',rho_part)
!!$     call parser_read('Drop temperature',T_part)
!!$     call parser_read('Drop diameter',d_part)
!!$     call parser_read('Parcel size',nparcel_part)
!!$     select case (trim(distro))
!!$     case ('uniform')
!!$        do i=1,npart
!!$           call random_number(rand)
!!$           part(i)%x = real(nx,WP)*dx*rand
!!$           call random_number(rand)
!!$           part(i)%y = real(ny,WP)*dx*rand
!!$           call random_number(rand)
!!$           part(i)%z = real(nz,WP)*dx*rand
!!$        end do
!!$     case ('sphere')
!!$        call parser_read('Cluster diameter',cdiam,0.0_WP)
!!$        do i=1,npart
!!$           call random_number(rand)
!!$           part(i)%x = cdiam*(rand-0.5_WP)
!!$           call random_number(rand)
!!$           part(i)%y = cdiam*(rand-0.5_WP)
!!$           call random_number(rand)
!!$           part(i)%z = cdiam*(rand-0.5_WP)
!!$           do while (sqrt(part(i)%x**2+part(i)%y**2+part(i)%z**2).gt.0.5_WP*cdiam)
!!$              call random_number(rand)
!!$              part(i)%x = cdiam*(rand-0.5_WP)
!!$              call random_number(rand)
!!$              part(i)%y = cdiam*(rand-0.5_WP)
!!$              call random_number(rand)
!!$              part(i)%z = cdiam*(rand-0.5_WP)
!!$           end do
!!$           part(i)%x = part(i)%x + 0.5_WP*L
!!$           part(i)%y = part(i)%y + 0.5_WP*L
!!$           part(i)%z = part(i)%z + 0.5_WP*L
!!$        end do
!!$     case ('blobs')
!!$        
!!$        ! ===============
!!$        ! Scalar Spectrum
!!$        allocate(spray_distro(nx,nx,nx))
!!$        ! Initialize in similar manner to Eswaran and Pope 1988
!!$        call parser_read('ks/ko',ksk0ratio)
!!$        ks = ksk0ratio*dk
!!$        call parser_read('kc/ks',kcksratio)
!!$        kc = kcksratio*ks
!!$        
!!$        ! Compute the Fourier coefficients
!!$        do k=1,nz
!!$           do j=1,ny
!!$              do i=1,nk
!!$                 
!!$                 ! Wavenumbers
!!$                 kx=real(i-1,WP)*dk
!!$                 ky=real(j-1,WP)*dk
!!$                 if (j.gt.nk) ky=-real(nx+1-j,WP)*dk
!!$                 kz=real(k-1,WP)*dk
!!$                 if (k.gt.nk) kz=-real(nx+1-k,WP)*dk
!!$                 kk =sqrt(kx**2+ky**2+kz**2)
!!$                 kk2=sqrt(kx**2+ky**2)
!!$                 
!!$                 ! Compute the Fourier coefficients
!!$                 if ((ks-dk/2.0_WP.le.kk).and.(kk.le.ks+dk/2.0_WP)) then
!!$                    f_phi = 1.0_WP
!!$                 else
!!$                    f_phi = 0.0_WP
!!$                 end if
!!$                 call random_number(rand)
!!$                 if (kk.lt.1e-10) then
!!$                    Cbuf(i,j,k) = 0.0_WP
!!$                 else
!!$                    Cbuf(i,j,k) = sqrt(f_phi/(4.0_WP*pi*kk**2))*exp(ii*2.0_WP*pi*rand)
!!$                 end if
!!$                 
!!$              end do
!!$           end do
!!$        end do
!!$        
!!$        ! Oddball
!!$        do k=2,nx
!!$           do j=nk+1,nx
!!$              Cbuf(1,j,k)=conjg(Cbuf(1,ny+2-j,nz+2-k))
!!$           end do
!!$        end do
!!$        do k=nk+1,nx
!!$           Cbuf(1,1,k)=conjg(Cbuf(1,1,nz+2-k))
!!$        end do
!!$        
!!$        ! Inverse Fourier transform
!!$        call dfftw_execute(plan_c2r)
!!$        
!!$        ! Force 'double-delta' pdf on scalar field
!!$        do k=1,nz
!!$           do j=1,ny
!!$              do i=1,nx
!!$                 if (Rbuf(i,j,k).le.0.0_WP) then
!!$                    Rbuf(i,j,k) = 0.0_WP
!!$                 else
!!$                    Rbuf(i,j,k) = 1.0_WP
!!$                 end if
!!$              end do
!!$           end do
!!$        end do
!!$        
!!$        ! Fourier Transform and filter to smooth
!!$        call dfftw_execute(plan_r2c)
!!$        
!!$        do k=1,nz
!!$           do j=1,ny
!!$              do i=1,nk
!!$                 ! Wavenumbers
!!$                 kx=real(i-1,WP)*dk
!!$                 ky=real(j-1,WP)*dk
!!$                 if (j.gt.nk) ky=-real(nx+1-j,WP)*dk
!!$                 kz=real(k-1,WP)*dk
!!$                 if (k.gt.nk) kz=-real(nx+1-k,WP)*dk
!!$                 kk =sqrt(kx**2+ky**2+kz**2)
!!$                 kk2=sqrt(kx**2+ky**2)
!!$                 
!!$                 ! Filter to remove high wavenumber components
!!$                 if (kk.le.kc) then
!!$                    Cbuf(i,j,k) = Cbuf(i,j,k) * 1.0_WP
!!$                 else
!!$                    Cbuf(i,j,k) = Cbuf(i,j,k) * (kc/kk)**2
!!$                 end if
!!$                 
!!$              end do
!!$           end do
!!$        end do
!!$        
!!$        ! Oddball
!!$        do k=2,nx
!!$           do j=nk+1,nx
!!$              Cbuf(1,j,k)=conjg(Cbuf(1,ny+2-j,nz+2-k))
!!$           end do
!!$        end do
!!$        do k=nk+1,nx
!!$           Cbuf(1,1,k)=conjg(Cbuf(1,1,nz+2-k))
!!$        end do
!!$        
!!$        ! Fourier Transform back to real
!!$        call dfftw_execute(plan_c2r)
!!$        do k=1,nz
!!$           do j=1,ny
!!$              do i=1,nz
!!$                 spray_distro(i,j,k) = Rbuf(i,j,k)/real(nx*ny*nz,WP)
!!$              end do
!!$           end do
!!$        end do
!!$        
!!$        ! Generate the spray
!!$        icount = 0
!!$        do while (icount<npart)
!!$           i = icount + 1
!!$           ! Generate a random position
!!$           call random_number(rand)
!!$           part(i)%x = L*rand
!!$           call random_number(rand)
!!$           part(i)%y = L*rand
!!$           call random_number(rand)
!!$           part(i)%z = L*rand
!!$           ! Localize the drop in i,j,k
!!$           ip = part(i)%x/L*nx+1
!!$           jp = part(i)%y/L*nx+1
!!$           kp = part(i)%z/L*nx+1
!!$           ! Interpolate spray_distro at drop location
!!$           local_spray_distro = spray_distro(ip,jp,kp)
!!$           ! Test if we should keep the drop
!!$           call random_number(rand)
!!$           if (rand<local_spray_distro) icount = icount + 1
!!$        end do
!!$        
!!$     case default
!!$        stop 'Unknown droplets distribution'
!!$     end select
!!$     
!!$     ! Set other spray parameters
!!$     do i=1,npart
!!$        part(i)%d       = d_part
!!$        part(i)%rho     = rho_part
!!$        part(i)%m       = part(i)%rho*(PI/6.0_WP)*part(i)%d**3
!!$        part(i)%T       = T_part
!!$        part(i)%dmdt    = 0.0_WP
!!$        part(i)%beta    = 0.0_WP
!!$        part(i)%tbreak  = 0.0_WP
!!$        part(i)%nparcel = nparcel_part
!!$        part(i)%dt      = 0.0_WP
!!$        part(i)%i       = 0
!!$        part(i)%j       = 0
!!$        part(i)%k       = 0
!!$        part(i)%tag     = i
!!$        part(i)%stop    = 0
!!$        part(i)%u       = 0.0_WP
!!$        part(i)%v       = 0.0_WP
!!$        part(i)%w       = 0.0_WP
!!$     end do
!!$     
!!$     ! Generate the initial droplet file
!!$     call parser_read('Init part file',part_init)
!!$     call BINARY_FILE_OPEN(iunit,trim(part_init),"w",ierr)
!!$     call BINARY_FILE_WRITE(iunit,npart,1,kind(npart),ierr)
!!$     call BINARY_FILE_WRITE(iunit,part_kind,1,kind(part_kind),ierr)
!!$     dt = 0.0_WP
!!$     call BINARY_FILE_WRITE(iunit,dt,1,kind(dt),ierr)
!!$     time = 0.0_WP
!!$     call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr)
!!$     do i=1,npart
!!$        call BINARY_FILE_WRITE(iunit,part(i),1,part_kind,ierr)
!!$     end do
!!$     call BINARY_FILE_CLOSE(iunit,ierr)
!!$  end if
     

  if (dims.eq.3) then
     ! Destroy the plans
     call dfftw_destroy_plan(plan_c2r)
     call dfftw_destroy_plan(plan_r2c)
     
     ! Clean up
     deallocate(Cbuf)
     deallocate(Rbuf)
     
     ! Check the results
     call evap_dns_check(spectrum,Ut,ke)
  end if
  
  return
end subroutine evap_dns_data


subroutine evap_dns_check(spectrum,Ut,ke)
  use evap_dns
  use fileio
  use parser
  implicit none
  integer :: iunit,i
  real(WP) :: pi
  real(WP) :: Ut,ke,mu,nu
  character(len=*) :: spectrum
  real(WP) :: kcin,epsi,l11,lt,re_turb,tau_epsi,l_k,tau_k
  real(WP), dimension(:,:), pointer :: spect
  
  pi = acos(-1.0_WP)
  call parser_read('Viscosity',mu)
  nu = mu/rho_init
  kcin     = 3.0_WP*0.5_WP*Ut*Ut
  if (trim(spectrum).eq.'PP') then
     epsi  = 15.0_WP*Ut*Ut*ke*ke*nu/4.0_WP
     l11   = sqrt(2.0_WP*pi)/ke
  else if (trim(spectrum).eq.'VKP') then
     call parser_read('Dissipation',epsi)
  end if
  lt       = Ut**3.0_WP/epsi
  re_turb  = Ut**4.0_WP/nu/epsi
  tau_epsi = kcin/epsi
  l_k      = (nu**0.75_WP)/(epsi**0.25_WP)
  tau_k    = sqrt(nu/epsi)
  
  write(*,*)  
  write(*,*)' ======================================== '
  write(*,*)' Debugging turbulent values               '
  write(*,*)' ---------------------------------------- '
  if (trim(spectrum).eq.'PP') then
     write(*,*)' -Spectrum type ----------> Passot-Pouquet'
  else if (trim(spectrum).eq.'VKP') then
     write(*,*)' -Spectrum type ----------> Von Karman-Pao'
  end if
  write(*,*)' -Turbulent Reynolds '
  write(*,*)'   Re_t --------> ', re_turb
  write(*,*)' -Turbulent kinetic energy '
  write(*,*)'   k -----------> ', kcin
  write(*,*)' -Turbulent dissipation rate '
  write(*,*)'   epsilon -----> ', epsi
  write(*,*)' -Size of the biggest eddy '
  write(*,*)'   l_t ---------> ', lt
  write(*,*)'   C1 ----------> ', L/lt
  write(*,*)' -Eddy turn over time '
  write(*,*)'   tau ---------> ', tau_epsi
  write(*,*)' -Kolmogorov scale '
  write(*,*)'   l_k ---------> ', l_k
  write(*,*)'   C2 ----------> ', l_k/dx
  write(*,*)' -Kolmogorov time scale '
  write(*,*)'   tau_k -------> ', tau_k
  write(*,*)' -Reynolds lambda '
  write(*,*)'   Re_lambda ---> ', sqrt(re_turb*15.0_WP)
  if (trim(spectrum).eq.'PP') then
     write(*,*)' -Integral length scale '
     write(*,*)'   Lii --------> ', l11
     write(*,*)' -Turbulent Reynolds based on Lii '
     write(*,*)'   Re_Lii -----> ', Ut*l11/nu
  end if
  write(*,*)' ======================================== '
  write(*,*)
  write(*,*)
  
  ! Need to change that
  call evap_dns_stat
  
  ! Recompute the spectrum
  allocate(spect(2,nx+1))
  spect = 0.0_WP
  call get_spectrum(nx,L,U,spect)
  call get_spectrum(nx,L,V,spect)
  call get_spectrum(nx,L,W,spect)
  
  ! Output it
  iunit=iopen()
  open(iunit,file='spectrum.numeric',form='formatted')
  do i=1,nx+1
     if ((spect(1,i).ne.0.0_WP).and.(spect(2,i).ne.0.0_WP)) then
        write(11,*) spect(1,i),',',spect(2,i)
     end if
  end do
  close(iclose(iunit))
  
  return
end subroutine evap_dns_check


subroutine evap_dns_stat
  use evap_dns
  use parser
  use fileio
  implicit none
  integer :: i,j,k
  real(WP) :: upvp,upwp,vpwp,Ut_final,kcin_final,div,div_max,div_b
  real(WP), dimension(3) :: um,umax,umin,uminval
  
  ! Computing mean velocity
  um=0.0_WP
  umax=-1.0e+70_WP
  umin=99.0e+70_WP
  uminval=99.0e+70_WP   
  do k=1,nz
     do j=1,ny
        do i=1,nx
           um(1)=um(1)+U(i,j,k)
           umax(1)=max(umax(1),(U(i,j,k)))
           umin(1)=min(umin(1),(U(i,j,k)))
           uminval(1)=min(uminval(1),abs(U(i,j,k)))
           um(2)=um(2)+V(i,j,k)
           umax(2)=max(umax(2),(V(i,j,k)))
           umin(2)=min(umin(2),(V(i,j,k)))
           uminval(2)=min(uminval(2),abs(V(i,j,k)))
           um(3)=um(3)+W(i,j,k)
           umax(3)=max(umax(3),(W(i,j,k)))
           umin(3)=min(umin(3),(W(i,j,k)))
           uminval(3)=min(uminval(3),abs(W(i,j,k)))
        end do
     end do
  end do
  um(1)=um(1)/(nx*ny*nz)
  um(2)=um(2)/(nx*ny*nz)
  um(3)=um(3)/(nx*ny*nz)
  
  ! Computing cross correlations to confirm isotropy
  upvp=0.0_WP
  upwp=0.0_WP
  vpwp=0.0_WP
  do k=1,nz 
     do j=1,ny
        do i=1,nx
           upvp=upvp+(U(i,j,k)-um(1))*(V(i,j,k)-um(2))
           upwp=upwp+(U(i,j,k)-um(1))*(W(i,j,k)-um(3))
           vpwp=vpwp+(V(i,j,k)-um(2))*(W(i,j,k)-um(3))
        end do
     end do
  end do
  upvp = upvp / (nx*ny*nz)
  upwp = upwp / (nx*ny*nz)
  vpwp = vpwp / (nx*ny*nz)
  
  ! Computing the final Ut
  Ut_final = 0.0_WP
  do k=1,nz
     do j=1,ny
        do i=1,nx
           Ut_final = Ut_final + (U(i,j,k)-um(1))**2+(V(i,j,k)-um(2))**2+(W(i,j,k)-um(3))**2
        end do
     end do
  end do
  Ut_final = sqrt(Ut_final/nx/ny/nz/3.0_WP)
  kcin_final = 3.0_WP*0.5_WP*Ut_final*Ut_final
  
  ! Computing divergence values
  div_b=0.0_WP
  div_max=0.0_WP
  do k=2,nz-1
     do j=2,ny-1
        do i=2,nx-1
           div=abs(U(i+1,j,k)-U(i-1,j,k)+V(i,j+1,k)-V(i,j-1,k)+W(i,j,k+1)-W(i,j,k-1))
           div_max=max(div_max,div)
           div_b=div_b+div
        end do
     end do
  end do
  div_b=div_b/(nx-2)/(ny-2)/(nz-2)
  
  ! Output it ------------------------------------------
  write(*,*)  
  write(*,*)' ======================================== '
  write(*,*)' Debugging turbulent values given by      '
  write(*,*)' the generated field                      '
  write(*,*)' ---------------------------------------- '
  write(*,*)' -Turbulent velocity '
  write(*,*)'   up ---------> ', Ut_final
  write(*,*)' -Turbulent kinetic energy '
  write(*,*)'   k ----------> ', kcin_final
  write(*,*)' -Cross correlations'
  write(*,*)'   <upvp> -----> ', upvp
  write(*,*)'   <upwp> -----> ', upwp
  write(*,*)'   <vpwp> -----> ', vpwp
  write(*,*)' -Velocity min, max and mean'
  write(*,*)'   u_min ------> ', umin(1)
  write(*,*)'   u_mean -----> ', um(1)
  write(*,*)'   u_max ------> ', umax(1)
  write(*,*)'   |u|_min ----> ', uminval(1)
  write(*,*)'   v_min ------> ', umin(2)
  write(*,*)'   v_mean -----> ', um(2)
  write(*,*)'   v_max ------> ', umax(2)
  write(*,*)'   |v|_min ----> ', uminval(2)
  write(*,*)'   w_min ------> ', umin(3)
  write(*,*)'   w_mean -----> ', um(3)
  write(*,*)'   w_max ------> ', umax(3)
  write(*,*)'   |w|_min ----> ', uminval(3)
  write(*,*)' -Average divergence -1 order'
  write(*,*)'   Div_b ------> ', div_b
  write(*,*)' -Maximum divergence'
  write(*,*)'   Div_max ----> ', div_max
  write(*,*)' ======================================== '
  write(*,*)
  write(*,*)
  
  return
end subroutine evap_dns_stat
