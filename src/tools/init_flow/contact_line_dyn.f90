module contact_line_dyn
  use string
  use precision
  use param
  implicit none
  
  ! Pointers to variable in data
  real(WP), dimension(:,:,:), pointer :: U
  real(WP), dimension(:,:,:), pointer :: V
  real(WP), dimension(:,:,:), pointer :: W
  real(WP), dimension(:,:,:,:), pointer :: VOF
  
end module contact_line_dyn

! ==================== !
! Create the grid/mesh !
! ==================== !
subroutine contact_line_dyn_grid
  use contact_line_dyn
  use parser
  use math
  implicit none
  
  integer :: i,j,k
  real(WP) :: Lx,Ly,Lz
  
  ! Read in the size of the domain
  call parser_read('nx',nx)
  call parser_read('ny',ny,nx/2)
  call parser_read('Lx',Lx,2.0_WP)
  call parser_read('Ly',Ly,1.0_WP)
  nz=1
  Lz=Lx/real(nx,WP)
 
  xper = 0
  yper = 0
  zper = 1
  
  ! Cartesian
  icyl = 0
  
  ! Allocate the arrays
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx),ym(ny),zm(nz))
  allocate(mask(nx,ny))
  
  ! Create the grid
  do i=1,nx+1
     x(i) = Lx*(real(i-1,WP)/real(nx,WP))
  end do
  do j=1,ny+1
     y(j) = Ly*(real(j-1,WP)/real(ny,WP))
  end do
  do k=1,nz+1
     z(k) = Lz*(real(k-1,WP)/real(nz,WP)-0.5_WP)
  end do
  
  ! Create the cell centered grid
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! Create the masks
  mask = 0
  mask(:,1)=3
  
  return
end subroutine contact_line_dyn_grid


! ========================= !
! Create the variable array !
! ========================= !
subroutine contact_line_dyn_data
  use contact_line_dyn
  use parser
  use math
  implicit none
  
  integer :: i,j,di,dj
  real(WP) :: hgt
  
  ! Allocate the array data
  nvar = 11
  allocate(data(nx,ny,nz,nvar))
  allocate(names(nvar))
  
  ! Link the pointers
  U     => data(:,:,:,1); names(1) = 'U'
  V     => data(:,:,:,2); names(2) = 'V'
  W     => data(:,:,:,3); names(3) = 'W'
  VOF  => data(:,:,:, 4:11);
  names( 4) = 'VOF1'
  names( 5) = 'VOF2'
  names( 6) = 'VOF3'
  names( 7) = 'VOF4'
  names( 8) = 'VOF5'
  names( 9) = 'VOF6'
  names(10) = 'VOF7'
  names(11) = 'VOF8'
  
  ! Initialize
  U   = 0.0_WP
  V   = 0.0_WP
  W   = 0.0_WP
  VOF = 0.0_WP
  
  ! Setup the velocity 
  do j=jmin_,jmax_
     dj=j-jmin_+1
     U(2:nx-1,dj,:)=exp((y(1)-y(j)))
  end do

  ! Setup the interface
  call parser_read('Pool height',hgt,0.5_WP)
  do i=imin_,imax_
     ! Data array indices
     di=i-imin_+1
     if (x(i).lt.hgt) then
        VOF(di,:,:,:)=1.0_WP
     end if
  end do
  VOF(:,1,:,:)=0.0_WP
      
  return
end subroutine contact_line_dyn_data
