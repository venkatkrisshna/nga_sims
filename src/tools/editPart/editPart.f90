program editPart
  use precision
  use string
  use fileio
  implicit none

  ! Program to modify part file
  ! Requires knowing the structure of both part files

  ! Old part type (this is what is now in lpt_mod.f90)
  type part1_type
     ! Particle id
     integer(kind=8) :: id
     ! Position (Cartesian)
     real(WP) :: x
     real(WP) :: y
     real(WP) :: z
     ! Velocity (Cartesian)
     real(WP) :: u
     real(WP) :: v
     real(WP) :: w
     ! Angular velocity
     real(WP) :: wx
     real(WP) :: wy
     real(WP) :: wz
     ! Diameter
     real(WP) :: d
     ! Time step size
     real(WP) :: dt
     ! Collision force
     real(WP), dimension(3) :: Acol
     ! Collision torque
     real(WP), dimension(3) :: Tcol
     ! Position (mesh)
     integer :: i
     integer :: j
     integer :: k
     ! Control parameter
     integer :: stop
  end type part1_type

  ! New part type (copy and paste from lpt_mod.f90 as well)
  type part2_type
     ! Particle id
     integer(kind=8) :: id
     ! Position (Cartesian)
     real(WP) :: x
     real(WP) :: y
     real(WP) :: z
     ! Velocity (Cartesian)
     real(WP) :: u
     real(WP) :: v
     real(WP) :: w
     ! Angular velocity
     real(WP) :: wx
     real(WP) :: wy
     real(WP) :: wz
     ! Diameter
     real(WP) :: d
     ! Time step size
     real(WP) :: dt
     ! Collision force
     real(WP), dimension(3) :: Acol
     ! Collision torque
     real(WP), dimension(3) :: Tcol
     ! Position (mesh)
     integer :: i
     integer :: j
     integer :: k
     ! Control parameter
     integer :: stop
  end type part2_type
  
  ! Type size
  integer, parameter :: newpart_size=160
  
  ! Particle data
  integer :: i,ii,iunit1,iunit2,ierr,choice
  integer :: npart,part_size,newnpart
  character(len=str_medium) :: filename1,filename2
  real(WP) :: dt,time,value,maxy,miny
  type(part1_type), dimension(:), allocatable :: part1
  type(part2_type), dimension(:), allocatable :: part2
  real(WP) :: period
  integer :: nper,n
  
  ! Read file name from standard input
  print*,'======================'
  print*,'| ARTS - part Editor |'
  print*,'======================'
  print*
  print "(a28,$)", " part file before edition : "
  read "(a)", filename1
  print "(a27,$)", " part file after edition : "
  read "(a)", filename2

  ! READ ORIGINAL PARTICLE FILE
  ! ** Open the part file to read **
  call BINARY_FILE_OPEN(iunit1,trim(filename1),"r",ierr)
        
  ! Read local number of particles
  call binary_file_read(iunit1,npart,1,kind(npart),ierr)
  call binary_file_read(iunit1,part_size,1,kind(part_size),ierr)                                     
  call binary_file_read(iunit1,dt,1,kind(dt),ierr)                                         
  call binary_file_read(iunit1,time,1,kind(time),ierr)

  ! Allocate part
  allocate(part1(1:npart))
  
  ! Read particles
  call binary_file_read(iunit1,part1(1:npart),npart,part_size,ierr)
  
  ! Close the files
  call BINARY_FILE_CLOSE(iunit1,ierr)

  ! Write to monitor
  print *, 'Number of particles: ',npart
  
  ! Ask what action to take
  print*
  print*, "1. Change type (requires editing editPart.f90)"
  print*, "2. Zero out velocity"
  print*, "3. Safe copy in y"
  print*, "4. Periodic copy in z"
  print*, "5. Make bottom static"
  print "(a9,$)", "Choice : "
  read "(i1)", choice
  
  ! Case dependent operation
  select case(choice)
  case (1) ! Direct copy into new type
     print *, 'Part size old: ',part_size
     newnpart=npart
     allocate(part2(1:newnpart))
     do i=1,npart
        ! Old variables
        part2(i)%x=part1(i)%x
        part2(i)%y=part1(i)%y
        part2(i)%z=part1(i)%z
        part2(i)%u=part1(i)%u
        part2(i)%v=part1(i)%v
        part2(i)%w=part1(i)%w
        part2(i)%wx=part1(i)%wx
        part2(i)%wy=part1(i)%wy
        part2(i)%wz=part1(i)%wz
        part2(i)%d=part1(i)%d
        part2(i)%dt=part1(i)%dt
        part2(i)%Acol(1)=part1(i)%Acol(1)
        part2(i)%Acol(2)=part1(i)%Acol(2)
        part2(i)%Acol(3)=part1(i)%Acol(3)
        part2(i)%Tcol(1)=part1(i)%Tcol(1)
        part2(i)%Tcol(2)=part1(i)%Tcol(2)
        part2(i)%Tcol(3)=part1(i)%Tcol(3)
        part2(i)%i=part1(i)%i
        part2(i)%j=part1(i)%j
        part2(i)%k=part1(i)%k
        part2(i)%stop=part1(i)%stop
        part2(i)%id=part1(i)%id
        ! New variables
     end do
     print *, 'Part size new: ',part_size
  case (2) ! Zero out velocity
     newnpart=npart
     allocate(part2(1:newnpart))
     do i=1,npart
        part2(i)%x=part1(i)%x
        part2(i)%y=part1(i)%y
        part2(i)%z=part1(i)%z
        part2(i)%u=0.0_WP
        part2(i)%v=0.0_WP
        part2(i)%w=0.0_WP
        part2(i)%wx=0.0_WP
        part2(i)%wy=0.0_WP
        part2(i)%wz=0.0_WP
        part2(i)%d=part1(i)%d
        part2(i)%dt=part1(i)%dt
        part2(i)%Acol(1)=part1(i)%Acol(1)
        part2(i)%Acol(2)=part1(i)%Acol(2)
        part2(i)%Acol(3)=part1(i)%Acol(3)
        part2(i)%Tcol(1)=part1(i)%Tcol(1)
        part2(i)%Tcol(2)=part1(i)%Tcol(2)
        part2(i)%Tcol(3)=part1(i)%Tcol(3)
        part2(i)%i=part1(i)%i
        part2(i)%j=part1(i)%j
        part2(i)%k=part1(i)%k
        part2(i)%stop=part1(i)%stop
        part2(i)%id=part1(i)%id
     end do
  case (3) ! Copy particles in y - safe copy to guarranty no overlap
     newnpart=2*npart
     allocate(part2(1:newnpart))
     maxy=-huge(1.0_WP); miny=+huge(1.0_WP)
     do i=1,npart
        part2(i)%x=part1(i)%x
        part2(i)%y=part1(i)%y
        part2(i)%z=part1(i)%z
        part2(i)%u=part1(i)%u
        part2(i)%v=part1(i)%v
        part2(i)%w=part1(i)%w
        part2(i)%wx=part1(i)%wx
        part2(i)%wy=part1(i)%wy
        part2(i)%wz=part1(i)%wz
        part2(i)%d=part1(i)%d
        part2(i)%dt=part1(i)%dt
        part2(i)%Acol(1)=part1(i)%Acol(1)
        part2(i)%Acol(2)=part1(i)%Acol(2)
        part2(i)%Acol(3)=part1(i)%Acol(3)
        part2(i)%Tcol(1)=part1(i)%Tcol(1)
        part2(i)%Tcol(2)=part1(i)%Tcol(2)
        part2(i)%Tcol(3)=part1(i)%Tcol(3)
        part2(i)%i=part1(i)%i
        part2(i)%j=part1(i)%j
        part2(i)%k=part1(i)%k
        part2(i)%stop=part1(i)%stop
        part2(i)%id=part1(i)%id
        ! Find extent of first bed
        miny=min(miny,part1(i)%y-0.5_WP*part1(i)%d)
        maxy=max(maxy,part1(i)%y+0.5_WP*part1(i)%d)
     end do
     do i=1,npart
        ii=i+npart
        part2(ii)%x=part1(i)%x
        part2(ii)%y=part1(i)%y+maxy-miny
        part2(ii)%z=part1(i)%z
        part2(ii)%u=part1(i)%u
        part2(ii)%v=part1(i)%v
        part2(ii)%w=part1(i)%w
        part2(ii)%wx=part1(i)%wx
        part2(ii)%wy=part1(i)%wy
        part2(ii)%wz=part1(i)%wz
        part2(ii)%d=part1(i)%d
        part2(ii)%dt=part1(i)%dt
        part2(ii)%Acol(1)=part1(i)%Acol(1)
        part2(ii)%Acol(2)=part1(i)%Acol(2)
        part2(ii)%Acol(3)=part1(i)%Acol(3)
        part2(ii)%Tcol(1)=part1(i)%Tcol(1)
        part2(ii)%Tcol(2)=part1(i)%Tcol(2)
        part2(ii)%Tcol(3)=part1(i)%Tcol(3)
        part2(ii)%i=part1(i)%i
        part2(ii)%j=part1(i)%j
        part2(ii)%k=part1(i)%k
        part2(ii)%stop=part1(i)%stop
        part2(ii)%id=part1(i)%id
     end do
  case (4) ! Copy particles in z - periodic assumption
     print "(a19,$)", "Number of copies : "
     read(*,*) nper
     print "(a16,$)", "Period length : "
     read(*,*) period
     newnpart=nper*npart
     allocate(part2(1:newnpart))
     do n=1,nper
        do i=1,npart
           ii=i+npart*(n-1)
           part2(ii)%x=part1(i)%x
           part2(ii)%y=part1(i)%y
           part2(ii)%z=part1(i)%z+period*(n-1)
           part2(ii)%u=part1(i)%u
           part2(ii)%v=part1(i)%v
           part2(ii)%w=part1(i)%w
           part2(ii)%wx=part1(i)%wx
           part2(ii)%wy=part1(i)%wy
           part2(ii)%wz=part1(i)%wz
           part2(ii)%d=part1(i)%d
           part2(ii)%dt=part1(i)%dt
           part2(ii)%Acol(1)=part1(i)%Acol(1)
           part2(ii)%Acol(2)=part1(i)%Acol(2)
           part2(ii)%Acol(3)=part1(i)%Acol(3)
           part2(ii)%Tcol(1)=part1(i)%Tcol(1)
           part2(ii)%Tcol(2)=part1(i)%Tcol(2)
           part2(ii)%Tcol(3)=part1(i)%Tcol(3)
           part2(ii)%i=part1(i)%i
           part2(ii)%j=part1(i)%j
           part2(ii)%k=part1(i)%k
           part2(ii)%stop=part1(i)%stop
           part2(ii)%id=part1(i)%id
        end do
     end do
  case (5) ! Make bottom layer static
     newnpart=npart
     allocate(part2(1:newnpart))
     do i=1,npart
        if (part1(i)%y.lt.0.75_WP*part1(i)%d) then
           part2(i)%x=part1(i)%x
           part2(i)%y=part1(i)%y
           part2(i)%z=part1(i)%z
           part2(i)%u=0.0_WP
           part2(i)%v=0.0_WP
           part2(i)%w=0.0_WP
           part2(i)%wx=0.0_WP
           part2(i)%wy=0.0_WP
           part2(i)%wz=0.0_WP
           part2(i)%d=part1(i)%d
           part2(i)%dt=part1(i)%dt
           part2(i)%Acol(1)=0.0_WP
           part2(i)%Acol(2)=0.0_WP
           part2(i)%Acol(3)=0.0_WP
           part2(i)%Tcol(1)=0.0_WP
           part2(i)%Tcol(2)=0.0_WP
           part2(i)%Tcol(3)=0.0_WP
           part2(i)%i=part1(i)%i
           part2(i)%j=part1(i)%j
           part2(i)%k=part1(i)%k
           part2(i)%stop=part1(i)%stop
           part2(i)%id=-1
        else
           part2(i)%x=part1(i)%x
           part2(i)%y=part1(i)%y
           part2(i)%z=part1(i)%z
           part2(i)%u=part1(i)%u
           part2(i)%v=part1(i)%v
           part2(i)%w=part1(i)%w
           part2(i)%wx=part1(i)%wx
           part2(i)%wy=part1(i)%wy
           part2(i)%wz=part1(i)%wz
           part2(i)%d=part1(i)%d
           part2(i)%dt=part1(i)%dt
           part2(i)%Acol(1)=part1(i)%Acol(1)
           part2(i)%Acol(2)=part1(i)%Acol(2)
           part2(i)%Acol(3)=part1(i)%Acol(3)
           part2(i)%Tcol(1)=part1(i)%Tcol(1)
           part2(i)%Tcol(2)=part1(i)%Tcol(2)
           part2(i)%Tcol(3)=part1(i)%Tcol(3)
           part2(i)%i=part1(i)%i
           part2(i)%j=part1(i)%j
           part2(i)%k=part1(i)%k
           part2(i)%stop=part1(i)%stop
           part2(i)%id=part1(i)%id
        end if
     end do
  case default ! Erroneous input
     stop "Invalid choice"
  end select
  
  ! WRITE NEW PARTICLE FILE
  ! ** Open the part file to write **
  call BINARY_FILE_OPEN(iunit2,trim(filename2),"w",ierr)
  call BINARY_FILE_WRITE(iunit2,newnpart,1,kind(newnpart),ierr)
  call BINARY_FILE_WRITE(iunit2,newpart_size,1,kind(newpart_size),ierr)
  call BINARY_FILE_WRITE(iunit2,dt,1,kind(dt),ierr)
  call BINARY_FILE_WRITE(iunit2,time,1,kind(time),ierr)
  call BINARY_FILE_WRITE(iunit2,part2,newnpart,newpart_size,ierr)
  call BINARY_FILE_CLOSE(iunit2,ierr)
  
end program editPart
