program asciireader
  use precision
  use string
  use fileio
  implicit none
  
  ! --- ASCII reader ---
  character(len=str_long) :: asciifile
  character(len=str_long) :: buffer
  integer :: iunit,ierr
  
  ! --- CONFIG FILE ---
  ! Cylindrical or cartesian
  integer :: icyl
  ! Number of grid points
  integer :: nx,ny,nz
  integer(KIND=8) :: ncell
  ! Periodicity
  integer :: xper,yper,zper
  
  ! Grid/mesh variables for data file
  real(WP), dimension(:),   allocatable :: x,xm
  real(WP), dimension(:),   allocatable :: y,ym
  real(WP), dimension(:),   allocatable :: z,zm
  integer,  dimension(:,:), allocatable :: mask
  
  ! --- OPTDATA FILE ---
  ! Data array with all the optional variables
  integer :: nod
  character(len=str_short), dimension(:), pointer :: OD_names
  real(WP), dimension(:,:,:,:), pointer :: OD
  
  ! Other stuff
  integer :: n,i,j,k
  character(len=str_medium) :: sim_name
  
  ! Open file
  print "(a20,$)", "ASCII file to read: "
  read "(a)", asciifile
  open (newunit=iunit,file=asciifile,form='formatted',status='old',iostat=ierr)
  if (ierr.ne.0) stop "Could not open the ASCII file."
  
  ! Obtain mesh information
  !print "(a6,$)", "icyl: "
  !read(*,*) icyl
  icyl=0
  !print "(a6,$)", "xper: "
  !read(*,*) xper
  xper=0
  !print "(a6,$)", "yper: "
  !read(*,*) yper
  yper=0
  !print "(a6,$)", "zper: "
  !read(*,*) zper
  zper=0
  print "(a4,$)", "nx: "
  read(*,*) nx
  print "(a4,$)", "ny: "
  read(*,*) ny
  print "(a4,$)", "nz: "
  read(*,*) nz
  allocate(x (nx+1)); allocate(y (ny+1)); allocate(z (nz+1))
  allocate(xm(nx  )); allocate(ym(ny  )); allocate(zm(nz  ))
  allocate(mask(nx,ny))
  
  ! Obtain data information
  print "(a18,$)", "Number of fields: "
  read(*,*) nod
  allocate(OD_names(nod))
  allocate(OD(nx,ny,nz,nod))
  do n=1,nod
     print "(a14,i1,a2,$)", "Name of field ",n,": "
     read "(a)", OD_names(n)
  end do
  
  ! Read the data all at once
  if (nz.eq.1) then
     ! 2D reader
     k=1
     do j=1,ny
        do i=1,nx
           read(iunit,'(a)',iostat=ierr) buffer
           read(buffer,*) xm(i),ym(j),OD(i,j,k,:)
        end do
     end do
     zm(k)=0.0_WP
  else
     ! 3D reader
     do k=1,nz
        do j=1,ny
           do i=1,nx
              read(iunit,'(a)',iostat=ierr) buffer
              read(buffer,*) xm(i),ym(j),zm(k),OD(i,j,k,:)
           end do
        end do
     end do
  end if
  
  ! Rebuild the mesh
  do i=2,nx
     x(i)=(xm(i-1)+xm(i))/2.0_WP
  end do
  x( 1  )=xm( 1)-(x ( 2)-xm( 1))
  x(nx+1)=xm(nx)+(xm(nx)-x (nx))
  do j=2,ny
     y(j)=(ym(j-1)+ym(j))/2.0_WP
  end do
  y( 1  )=ym( 1)-(y ( 2)-ym( 1))
  y(ny+1)=ym(ny)+(ym(ny)-y (ny))
  if (nz.eq.1) then
     z(1)=-(x ( 2)-xm( 1))
     z(2)=+(xm(nx)-x (nx))
  else
     do k=2,nz
        z(k)=(zm(k-1)+zm(k))/2.0_WP
     end do
     z( 1  )=zm( 1)-(z ( 2)-zm( 1))
     z(nz+1)=zm(nz)+(zm(nz)-z (nz))
  end if
  mask=0
  
  ! Output the config file
  sim_name='from_asciireader'
  call param_write_config(sim_name)
  
  ! Output the optdata file
  call param_write_optdata
  
contains
  
  ! Write the mesh file to the disk
  subroutine param_write_config(simulation)
    use parser
    implicit none
    
    character(len=str_medium) :: simulation
    integer :: ierr,iunit
    character(len=str_medium) :: filename
    
    ! Open the mesh file
    print "(a22,$)", "Config file to write: "
    read "(a)", filename
    call BINARY_FILE_OPEN(iunit,trim(filename),"w",ierr)
    
    ! Write the mesh
    call BINARY_FILE_WRITE(iunit,simulation,str_medium,kind(simulation),ierr)
    call BINARY_FILE_WRITE(iunit,icyl,1,kind(icyl),ierr)
    call BINARY_FILE_WRITE(iunit,xper,1,kind(xper),ierr)
    call BINARY_FILE_WRITE(iunit,yper,1,kind(yper),ierr)
    call BINARY_FILE_WRITE(iunit,zper,1,kind(zper),ierr)
    call BINARY_FILE_WRITE(iunit,nx,1,kind(nx),ierr)
    call BINARY_FILE_WRITE(iunit,ny,1,kind(ny),ierr)
    call BINARY_FILE_WRITE(iunit,nz,1,kind(nz),ierr)
    call BINARY_FILE_WRITE(iunit,x,nx+1,kind(x),ierr)
    call BINARY_FILE_WRITE(iunit,y,ny+1,kind(y),ierr)
    call BINARY_FILE_WRITE(iunit,z,nz+1,kind(z),ierr)
    call BINARY_FILE_WRITE(iunit,mask,nx*ny,kind(mask),ierr)
    
    ! Close the file
    call BINARY_FILE_CLOSE(iunit,ierr)
    
    return
  end subroutine param_write_config
  
  ! Write the data file to the disk
  subroutine param_write_optdata
    use parser
    implicit none
    
    integer :: ierr,iunit
    integer :: var
    character(len=str_medium) :: filename
    real(WP) :: dt,time
    
    ! Create number of cells
    ncell=int(nx,8)*int(ny,8)*int(nz,8)
    
    ! Open the data file
    print "(a23,$)", "Optdata file to write: "
    read "(a)", filename
    call BINARY_FILE_OPEN(iunit,trim(filename),"w",ierr)
    
    ! Write sizes
    call BINARY_FILE_WRITE(iunit,nx,1,kind(nx),ierr)
    call BINARY_FILE_WRITE(iunit,ny,1,kind(ny),ierr)
    call BINARY_FILE_WRITE(iunit,nz,1,kind(nz),ierr)
    call BINARY_FILE_WRITE(iunit,nod,1,kind(nod),ierr)
    ! Write additional stuff
    dt = 0.0_WP
    time = 0.0_WP
    call BINARY_FILE_WRITE(iunit,dt,1,kind(dt),ierr)
    call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr)
    ! Write variable names
    do var=1,nod
       call BINARY_FILE_WRITE(iunit,OD_names(var),str_short,kind(OD_names),ierr)
    end do
    ! Write data field
    do var=1,nod
       call BINARY_BIGFILE_WRITE(iunit,OD(:,:,:,var),ncell,kind(OD),ierr)
    end do
    
    ! Close the file
    call BINARY_FILE_CLOSE(iunit,ierr)
    
    return
  end subroutine param_write_optdata
  
end program asciireader
