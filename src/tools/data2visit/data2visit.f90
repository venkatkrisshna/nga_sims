program data2visit
  use precision
  use string
  use fileio 
  implicit none

  include "silo_f9x.inc"

  ! Data array with all the variables (velocity,pressure,...)
  integer :: i,j,k
  integer :: nx,ny,nz,nvar
  character(len=str_short), dimension(:), allocatable :: names
  real(WP), dimension(:,:,:), allocatable :: data
  integer :: iunit,iunit2,ierr,ibuffer,var
  character(len=str_medium) :: filename1,filename2,directory
  real(WP) :: dt,time
  real(WP) :: Lx,Ly,Lz
  real(WP), dimension(:), allocatable :: x,y,z

  ! Silo
  integer :: dbfile, optlistid
  
  ! Read file name from standard input
  print*,'=================================='
  print*,'| ARTS - data to visit converter |'
  print*,'=================================='
  print*
  print *, " data file : "
  read "(a)", filename1
  print *, " visit file (e.g. data.silo) : "
  read "(a)", filename2
  print *, " Lx,Ly,Lz : "
  read (*,*) Lx,Ly,Lz

  ! Open the data file to read
  call BINARY_FILE_OPEN(iunit,trim(filename1),"r",ierr)
  
  ! Read sizes
  call BINARY_FILE_READ(iunit,nx,1,kind(nx),ierr)
  call BINARY_FILE_READ(iunit,ny,1,kind(ny),ierr)
  call BINARY_FILE_READ(iunit,nz,1,kind(nz),ierr)
  call BINARY_FILE_READ(iunit,nvar,1,kind(nvar),ierr)
  print*,'Grid :',nx,'x',ny,'x',nz
  
  ! Read additional stuff
  call BINARY_FILE_READ(iunit,dt,1,kind(dt),ierr)
  call BINARY_FILE_READ(iunit,time,1,kind(time),ierr)
  print*,'Data file at time :',time
  
  ! Read variable names
  allocate(names(nvar))
  do var=1,nvar
     call BINARY_FILE_READ(iunit,names(var),str_short,kind(names),ierr)
  end do
  print*,'Variables : ',names
  
  ! Allocate data buffer
  allocate(data(nx,ny,nz))

  ! Create silo database
  ierr=dbcreate(filename2,len_trim(filename2),DB_CLOBBER,DB_LOCAL, &
       "Silo database created with NGA",30,DB_HDF5,dbfile)
  if(dbfile.eq.-1) stop 'Could not create Silo file!'

  ! Create mesh
  allocate(x(nx+1));
  allocate(y(ny+1))
  allocate(z(nz+1))
  do i=1,nx+1; x(i)=real(i-1,WP)/real(nx,WP)*Lx; end do
  do j=1,ny+1; y(j)=real(j-1,WP)/real(ny,WP)*Ly; end do
  do k=1,nz+1; z(k)=real(k-1,WP)/real(nz,WP)*Lz; end do
     
  ! Write mesh and time info
  ierr = dbmkoptlist(2, optlistid)
  ierr = dbaddiopt(optlistid, DBOPT_CYCLE, 0)
  ierr = dbadddopt(optlistid, DBOPT_DTIME, time)
  ierr = dbputqm (dbfile,"Mesh",4,"xm",2,"ym",2,"zm",2,real(x,SP),real(y,SP),real(z,SP), &
       (/nx+1,ny+1,nz+1/),3,DB_FLOAT, DB_COLLINEAR, optlistid, ierr)
  ierr = dbfreeoptlist(optlistid)
   
  ! For each variable, read it then write the ensight data file
  do var=1,nvar
     ! Read the variable
     print*,'Doing ',names(var)
     ! Read variable from data file
     call BINARY_FILE_READ(iunit,data,nx*ny*nz,kind(data),ierr)
     ! Write to silo file
     ierr = dbputqv1(dbfile,trim(names(var)),len_trim(names(var)),"Mesh", 4, &
          real(data,SP),(/nx,ny,nz/),3,DB_F77NULL,0,DB_FLOAT,DB_ZONECENT,DB_F77NULL,ierr)
  end do

  ! Close data file
  call BINARY_FILE_CLOSE(iunit,ierr)

  ! Close silo database
  ierr = dbclose(dbfile)
    
end program data2visit
