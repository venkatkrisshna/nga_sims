program serialdata_merger
  use precision
  use string
  use fileio
  implicit none

  ! Data array with all the variables (velocity,pressure,...)
  integer :: nx,ny,nz,nvar
  integer :: nx_,ny_,nz_,datasize
  character(len=str_short), dimension(:), allocatable :: names
  integer :: iunit,ierr,unit
  character(len=str_medium) :: filename
  character(len=str_medium) :: dirname, buffer
  integer :: i,j,k
  integer :: ii,jj,kk
  integer :: nproc,proc
  real(WP), dimension(:,:,:), allocatable :: data
  real(WP), dimension(:,:,:,:), pointer :: data_tmp
  integer :: imin_, jmin_, kmin_
  integer :: var,var2
  integer, dimension(6) :: dims
  real(WP) :: dt
  real(WP) :: time

  ! Read file name from standard input
  print*,'======================'
  print*,'| Serial data merger |'
  print*,'======================'
  print*
  print "(a,$)", " serial data directory : "
  read "(a)", dirname
  
  ! First read header file ================================
  ! Set filename
  filename = trim(adjustl(dirname)) // '/data.header'
  ! Open file
  call BINARY_FILE_OPEN(iunit,trim(filename),"r",ierr)
  ! Read integer parameters
  call BINARY_FILE_READ(iunit,dims(1:5),5,kind(dims),ierr)
  nproc=dims(1)
  nx=dims(2)
  ny=dims(3)
  nz=dims(4)
  nvar=dims(5)
  ! Read time info
  call BINARY_FILE_READ(iunit,dt,1,kind(dt),ierr)
  call BINARY_FILE_READ(iunit,time,1,kind(time),ierr)
  ! Allocate and read variable names
  allocate(names(nvar))
  do var=1,nvar
     call BINARY_FILE_READ(iunit,names(var),str_short,kind(names),ierr)
  end do
  ! Close header file
  call BINARY_FILE_CLOSE(iunit,ierr)
  
  ! Allocate global data array ============================
  allocate(data(1:nx,1:ny,1:nz))
  
  ! Prepare file to write ====================================
  filename=dirname(1:len_trim(dirname)-7)
  call BINARY_FILE_OPEN(unit,trim(filename),"w",ierr)
  call BINARY_FILE_WRITE(unit,nx,1,kind(nx),ierr)
  call BINARY_FILE_WRITE(unit,ny,1,kind(ny),ierr)
  call BINARY_FILE_WRITE(unit,nz,1,kind(nz),ierr)
  call BINARY_FILE_WRITE(unit,nvar,1,kind(nvar),ierr)
  ! Write time info
  call BINARY_FILE_WRITE(unit,dt,1,kind(dt),ierr)
  call BINARY_FILE_WRITE(unit,time,1,kind(time),ierr)
  ! Write variable names
  do var=1,nvar
     call BINARY_FILE_WRITE(unit,names(var),str_short,kind(names),ierr)
  end do
  
  ! Loop over variables ===================================
  do var=1,nvar
     ! Loop over proc/file number
     print*,'Reading ',names(var)
     do proc=1,nproc
        ! Set filename
        write(buffer,'(i6.6)') proc
        filename = trim(adjustl(dirname)) // '/data.' // trim(adjustl(buffer))
        ! Open file
        call BINARY_FILE_OPEN(iunit,trim(filename),"r",ierr)
        ! Read local position and size
        call BINARY_FILE_READ(iunit,dims,6,kind(dims),ierr)
        imin_=dims(1)
        jmin_=dims(2)
        kmin_=dims(3)
        nx_=dims(4)
        ny_=dims(5)
        nz_=dims(6)
        datasize=nx_*ny_*nz_
        ! Allocate local data array
        if (associated(data_tmp)) deallocate(data_tmp) 
        allocate(data_tmp(1:nx_,1:ny_,1:nz_,nvar))
        ! Read the full data array
        do var2=1,nvar
           call BINARY_FILE_READ(iunit,data_tmp(:,:,:,var2),datasize,kind(data_tmp),ierr)
        end do
        ! Transfer it to the global array
        do k=1,nz_
           do j=1,ny_
              do i=1,nx_
                 ii=imin_+i-1
                 jj=jmin_+j-1
                 kk=kmin_+k-1
                 data(ii,jj,kk)=data_tmp(i,j,k,var)
              end do
           end do
        end do
        ! Close the file
        call BINARY_FILE_CLOSE(iunit,ierr)
     end do
     print*,'done'
     
     ! Write data field
     call BINARY_FILE_WRITE(unit,data,nx*ny*nz,kind(data),ierr)
     
  end do
  
  ! Close data file to write =================================
  call BINARY_FILE_CLOSE(unit,ierr)
  
end program serialdata_merger
