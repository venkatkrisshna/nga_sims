program stl2ensight
  use precision
  use string
  use fileio
  use parser
  use math
  implicit none
  
  ! General
  integer :: ierr,iunit,n
  
  ! STL file
  character(len=str_medium) :: stl_file
  character(len=80) :: cbuff
  character(len= 2) :: padding
  integer  :: nt
  real(SP) :: fltbuf,tmp
  type triangle_type
     real(WP), dimension(3) :: norm
     real(WP), dimension(3) :: v1
     real(WP), dimension(3) :: v2
     real(WP), dimension(3) :: v3
  end type triangle_type
  type(triangle_type), dimension(:), allocatable :: t
  real(WP) :: xmin,xmax,ymin,ymax,zmin,zmax
  real(WP), dimension(3) :: shift,scaling
  logical :: swap_xy,swap_yz,swap_zx
  logical :: swap_yx,swap_zy,swap_xz
  
  ! Ensight file
  character(len=str_medium) :: ensight_file
  character(len=str_medium) :: ensight_norm
  character(len=str_medium) :: case_file
  character(len=80) :: cbuffer
  character(len=80) :: str
  integer  :: ibuffer
  real(SP) :: rbuffer
  
  ! Input processing
  character(len=str_medium) :: input_name
  
  ! Parse the command line
  call get_command_argument(1,input_name)
  
  ! Initialize the parser
  call parser_init
  
  ! Read the input file
  call parser_parsefile(input_name)
  
  ! Read stl file
  call parser_readchar('STL file',stl_file)
  
  ! Open the STL file to read
  call BINARY_FILE_OPEN(iunit,trim(stl_file),"r",ierr)
  
  ! Read sizes
  call BINARY_FILE_READ(iunit,cbuff,80,kind(cbuff),ierr)
  call BINARY_FILE_READ(iunit,nt,1,kind(nt),ierr)
  print*,'STL name :',trim(cbuff)
  print*,'# of triangles :',nt
  
  ! Read triangles
  allocate(t(nt))
  do n=1,nt
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%norm(1)=real(fltbuf,WP)
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%norm(2)=real(fltbuf,WP)
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%norm(3)=real(fltbuf,WP)
     
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%v1(1)=real(fltbuf,WP)
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%v1(2)=real(fltbuf,WP)
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%v1(3)=real(fltbuf,WP)
     
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%v2(1)=real(fltbuf,WP)
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%v2(2)=real(fltbuf,WP)
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%v2(3)=real(fltbuf,WP)
     
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%v3(1)=real(fltbuf,WP)
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%v3(2)=real(fltbuf,WP)
     call BINARY_FILE_READ(iunit,fltbuf,1,kind(fltbuf),ierr); t(n)%v3(3)=real(fltbuf,WP)
          
     call BINARY_FILE_READ(iunit,padding,2,kind(padding),ierr)
  end do
  
  ! Output extents
  xmin=+huge(1.0_WP);ymin=+huge(1.0_WP);zmin=+huge(1.0_WP)
  xmax=-huge(1.0_WP);ymax=-huge(1.0_WP);zmax=-huge(1.0_WP)
  do n=1,nt
     if (t(n)%v1(1).lt.xmin) xmin=t(n)%v1(1)
     if (t(n)%v2(1).lt.xmin) xmin=t(n)%v2(1)
     if (t(n)%v3(1).lt.xmin) xmin=t(n)%v3(1)
     
     if (t(n)%v1(2).lt.ymin) ymin=t(n)%v1(2)
     if (t(n)%v2(2).lt.ymin) ymin=t(n)%v2(2)
     if (t(n)%v3(2).lt.ymin) ymin=t(n)%v3(2)
     
     if (t(n)%v1(3).lt.zmin) zmin=t(n)%v1(3)
     if (t(n)%v2(3).lt.zmin) zmin=t(n)%v2(3)
     if (t(n)%v3(3).lt.zmin) zmin=t(n)%v3(3)
     
     if (t(n)%v1(1).gt.xmax) xmax=t(n)%v1(1)
     if (t(n)%v2(1).gt.xmax) xmax=t(n)%v2(1)
     if (t(n)%v3(1).gt.xmax) xmax=t(n)%v3(1)
     
     if (t(n)%v1(2).gt.ymax) ymax=t(n)%v1(2)
     if (t(n)%v2(2).gt.ymax) ymax=t(n)%v2(2)
     if (t(n)%v3(2).gt.ymax) ymax=t(n)%v3(2)
     
     if (t(n)%v1(3).gt.zmax) zmax=t(n)%v1(3)
     if (t(n)%v2(3).gt.zmax) zmax=t(n)%v2(3)
     if (t(n)%v3(3).gt.zmax) zmax=t(n)%v3(3)
  end do
  print*,'Original extents - x :',xmin,'-->',xmax
  print*,'Original extents - y :',ymin,'-->',ymax
  print*,'Original extents - z :',zmin,'-->',zmax
  
  ! Read geometric transform info
  call parser_read('Rescale',scaling)
  call parser_read('Translate',shift)
  
  ! Transform the STL geometry
  print*,'Rescaling and translating...'
  do n=1,nt
     t(n)%v1=t(n)%v1*scaling+shift
     t(n)%v2=t(n)%v2*scaling+shift
     t(n)%v3=t(n)%v3*scaling+shift
     t(n)%norm=normalize(t(n)%norm*scaling)
  end do
     
  ! Direction swapping: x->y
  call parser_read('Swap x->y',swap_xy,.false.)
  call parser_read('Swap y->x',swap_yx,.false.)
  if (swap_xy.or.swap_yx) then
     print*,'Swapping x <=> y...'
     do n=1,nt
        tmp=t(n)%v1(1);t(n)%v1(1)=t(n)%v1(2);t(n)%v1(2)=tmp
        tmp=t(n)%v2(1);t(n)%v2(1)=t(n)%v2(2);t(n)%v2(2)=tmp
        tmp=t(n)%v3(1);t(n)%v3(1)=t(n)%v3(2);t(n)%v3(2)=tmp
        tmp=t(n)%norm(1);t(n)%norm(1)=t(n)%norm(2);t(n)%norm(2)=tmp
     end do
  end if
  
  ! Direction swapping: y->z
  call parser_read('Swap y->z',swap_yz,.false.)
  call parser_read('Swap z->y',swap_zy,.false.)
  if (swap_yz.or.swap_zy) then
     print*,'Swapping y <=> z...'
     do n=1,nt 
        tmp=t(n)%v1(2);t(n)%v1(2)=t(n)%v1(3);t(n)%v1(3)=tmp
        tmp=t(n)%v2(2);t(n)%v2(2)=t(n)%v2(3);t(n)%v2(3)=tmp
        tmp=t(n)%v3(2);t(n)%v3(2)=t(n)%v3(3);t(n)%v3(3)=tmp
        tmp=t(n)%norm(2);t(n)%norm(2)=t(n)%norm(3);t(n)%norm(3)=tmp
     end do
  end if
  
  ! Direction swapping: z->x
  call parser_read('Swap x->z',swap_xz,.false.)
  call parser_read('Swap z->x',swap_zx,.false.)
  if (swap_xz.or.swap_zx) then
     print*,'Swapping z <=> x...'
     do n=1,nt
        tmp=t(n)%v1(1);t(n)%v1(1)=t(n)%v1(3);t(n)%v1(3)=tmp
        tmp=t(n)%v2(1);t(n)%v2(1)=t(n)%v2(3);t(n)%v2(3)=tmp
        tmp=t(n)%v3(1);t(n)%v3(1)=t(n)%v3(3);t(n)%v3(3)=tmp
        tmp=t(n)%norm(1);t(n)%norm(1)=t(n)%norm(3);t(n)%norm(3)=tmp
     end do
  end if
  
  ! Output extents
  xmin=+huge(1.0_WP);ymin=+huge(1.0_WP);zmin=+huge(1.0_WP)
  xmax=-huge(1.0_WP);ymax=-huge(1.0_WP);zmax=-huge(1.0_WP)
  do n=1,nt
     if (t(n)%v1(1).lt.xmin) xmin=t(n)%v1(1)
     if (t(n)%v2(1).lt.xmin) xmin=t(n)%v2(1)
     if (t(n)%v3(1).lt.xmin) xmin=t(n)%v3(1)
     
     if (t(n)%v1(2).lt.ymin) ymin=t(n)%v1(2)
     if (t(n)%v2(2).lt.ymin) ymin=t(n)%v2(2)
     if (t(n)%v3(2).lt.ymin) ymin=t(n)%v3(2)
     
     if (t(n)%v1(3).lt.zmin) zmin=t(n)%v1(3)
     if (t(n)%v2(3).lt.zmin) zmin=t(n)%v2(3)
     if (t(n)%v3(3).lt.zmin) zmin=t(n)%v3(3)
     
     if (t(n)%v1(1).gt.xmax) xmax=t(n)%v1(1)
     if (t(n)%v2(1).gt.xmax) xmax=t(n)%v2(1)
     if (t(n)%v3(1).gt.xmax) xmax=t(n)%v3(1)
     
     if (t(n)%v1(2).gt.ymax) ymax=t(n)%v1(2)
     if (t(n)%v2(2).gt.ymax) ymax=t(n)%v2(2)
     if (t(n)%v3(2).gt.ymax) ymax=t(n)%v3(2)
     
     if (t(n)%v1(3).gt.zmax) zmax=t(n)%v1(3)
     if (t(n)%v2(3).gt.zmax) zmax=t(n)%v2(3)
     if (t(n)%v3(3).gt.zmax) zmax=t(n)%v3(3)
  end do
  print*,'New extents - x :',xmin,'-->',xmax
  print*,'New extents - y :',ymin,'-->',ymax
  print*,'New extents - z :',zmin,'-->',zmax
  
  ! Close the file
  call BINARY_FILE_CLOSE(iunit,ierr)
  ! ============================================================
  
  ! ============================================================
  ! Read ensight file
  call parser_readchar('Ensight file',ensight_file)
  ensight_norm=trim(ensight_file) // ".norm"
  
  ! Generate case file
  case_file=trim(adjustl(ensight_file)) // ".case"
  iunit = iopen()
  open(iunit,file=case_file,form="formatted",iostat=ierr,status="REPLACE")
  str='FORMAT'
  write(iunit,'(a80)') str
  str='type: ensight gold'
  write(iunit,'(a80)') str
  str='GEOMETRY'
  write(iunit,'(a80)') str
  str='model: ' // ensight_file
  write(iunit,'(a80)') str
  str='VARIABLE'
  write(iunit,'(a80)') str
  str='vector per element: normal ' // trim(ensight_norm)
  write(iunit,'(a80)') str
  close(iclose(iunit))
  
  ! Generate the geometry
  call BINARY_FILE_OPEN(iunit,trim(ensight_file),"w",ierr)
  
  ! Write header
  cbuffer = 'C Binary'
  call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
  cbuffer = 'Ensight Gold Geometry File'
  call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
  cbuffer = 'STL geometry from NGA'
  call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
  cbuffer = 'node id off'
  call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
  cbuffer = 'element id off'
  call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
  cbuffer = 'part'
  call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
  ibuffer = 1
  call BINARY_FILE_WRITE(iunit,ibuffer,1,kind(ibuffer),ierr)
  cbuffer = 'STL data from NGA'
  call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
  cbuffer = 'coordinates'
  call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
  ibuffer = 3*nt
  call BINARY_FILE_WRITE(iunit,ibuffer,1,kind(ibuffer),ierr)
  
  ! Write nodal position - x
  do n=1,nt
     rbuffer=t(n)%v1(1)
     call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
     rbuffer=t(n)%v2(1)
     call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
     rbuffer=t(n)%v3(1)
     call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
  end do
  
  ! Write nodal position - y
  do n=1,nt
     rbuffer=t(n)%v1(2)
     call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
     rbuffer=t(n)%v2(2)
     call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
     rbuffer=t(n)%v3(2)
     call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
  end do
  
  ! Write nodal position - z
  do n=1,nt
     rbuffer=t(n)%v1(3)
     call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
     rbuffer=t(n)%v2(3)
     call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
     rbuffer=t(n)%v3(3)
     call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
  end do
  
  ! Write elements
  cbuffer = 'tria3'
  call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
  ibuffer = nt
  call BINARY_FILE_WRITE(iunit,ibuffer,1,kind(ibuffer),ierr)
  
  ! Write connectivity
  do n=1,nt
     ibuffer=3*n-2
     call BINARY_FILE_WRITE(iunit,ibuffer,1,kind(ibuffer),ierr)
     ibuffer=3*n-1
     call BINARY_FILE_WRITE(iunit,ibuffer,1,kind(ibuffer),ierr)
     ibuffer=3*n-0
     call BINARY_FILE_WRITE(iunit,ibuffer,1,kind(ibuffer),ierr)
  end do
  
  ! Close the file
  call BINARY_FILE_CLOSE(iunit,ierr)
  
  ! Generate the normal vector file
  call BINARY_FILE_OPEN(iunit,trim(ensight_norm),"w",ierr)
  
  ! Write header
  cbuffer = 'STL normals from NGA'
  call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
  cbuffer = 'part'
  call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
  ibuffer = 1
  call BINARY_FILE_WRITE(iunit,ibuffer,1,kind(ibuffer),ierr)
  cbuffer = 'tria3'
  call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
  
  ! Write normal vector
  do n=1,nt
     rbuffer=t(n)%norm(1)
     call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
  end do
  do n=1,nt
     rbuffer=t(n)%norm(2)
     call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
  end do
  do n=1,nt
     rbuffer=t(n)%norm(3)
     call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
  end do
  
  ! Close the file
  call BINARY_FILE_CLOSE(iunit,ierr)
  
  ! ============================================================
  
end program stl2ensight
