program avgData
  use precision
  use string
  use fileio
  implicit none

  ! Data array with all the variables (velocity,pressure,...)
  integer :: nx,ny,nz,nvar
  character(len=str_short), dimension(:), allocatable :: names
  real(WP), dimension(:,:,:), allocatable :: mydata
  real(WP), dimension(:,:,:,:), allocatable :: adata
  integer :: iunit,iunit_list,ierr,var,nfiles
  character(len=str_medium) :: filename_list,filename_data,filename_avg
  real(WP) :: dt,time

  ! Read file name from standard input
  print*,'============================='
  print*,'| ARTS - average data files |'
  print*,'============================='
  print*
  print *, ' File containing list of data files to be averaged :'
  print *,'  (create file using e.g. "ls -l >> data_files")'
  read '(a)', filename_list
  print *, ' Data file to write with averages :'
  read '(a)', filename_avg

  ! Open the list of data files
  iunit_list=12
  open(iunit_list,file=filename_list,form='formatted',action='read')
     
  ! Loop over data files
  ierr=0
  nfiles=0
  loop: do while (ierr.eq.0) 

     ! Filename of this file
     read (iunit_list,'(a)',iostat=ierr) filename_data
     if (ierr.ne.0) exit loop
     
     ! ** Open the data file to read **
     call BINARY_FILE_OPEN(iunit,trim(filename_data),"r",ierr)
     if (ierr.ne.0) then
        print *,'Unable to open ',filename_data
        stop
     end if

     ! Update file counter
     nfiles=nfiles+1

     ! Read sizes
     call BINARY_FILE_READ(iunit,nx,1,kind(nx),ierr)
     call BINARY_FILE_READ(iunit,ny,1,kind(ny),ierr)
     call BINARY_FILE_READ(iunit,nz,1,kind(nz),ierr)
     call BINARY_FILE_READ(iunit,nvar,1,kind(nvar),ierr)

     ! Read additional stuff
     call BINARY_FILE_READ(iunit,dt,1,kind(dt),ierr)
     call BINARY_FILE_READ(iunit,time,1,kind(time),ierr)

     ! Allocate arrays
     if (nfiles.eq.1) then
        allocate(mydata(nx,ny,nz))
        allocate(adata(nx,ny,nz,nvar)); adata=0.0_WP
        allocate(names(nvar))
     end if


     ! Read variable names
     do var=1,nvar
        call BINARY_FILE_READ(iunit,names(var),str_short,kind(names),ierr)
     end do


     ! Read data from this data file
     do var=1,nvar
        call BINARY_FILE_READ(iunit,mydata,nx*ny*nz,kind(mydata),ierr)
        ! Update average
        adata(:,:,:,var)=adata(:,:,:,var)+mydata(:,:,:)
     end do

     ! Close file
     call BINARY_FILE_CLOSE(iunit,ierr)

  end do loop

  ! Close list of data files
  close(iunit_list)


  ! Finish computing average
  adata=adata/real(nfiles,WP)

  ! Write the averages to new data file
  call BINARY_FILE_OPEN(iunit,trim(filename_avg),"w",ierr)
  call BINARY_FILE_WRITE(iunit,nx,1,kind(nx),ierr)
  call BINARY_FILE_WRITE(iunit,ny,1,kind(ny),ierr)
  call BINARY_FILE_WRITE(iunit,nz,1,kind(nz),ierr)
  call BINARY_FILE_WRITE(iunit,nvar,1,kind(nvar),ierr)
  call BINARY_FILE_WRITE(iunit,dt,1,kind(dt),ierr)
  call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr)
  do var=1,nvar
     call BINARY_FILE_WRITE(iunit,names(var),str_short,kind(names),ierr)
  end do
  do var=1,nvar
     call BINARY_FILE_WRITE(iunit,adata(:,:,:,var),nx*ny*nz,kind(adata),ierr)
  end do
  call BINARY_FILE_CLOSE(iunit,ierr)

  
  
end program avgData

