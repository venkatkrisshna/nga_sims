program serialdata_splitter
  use precision
  use string
  use fileio
  implicit none
  
  ! Data array with all the variables (velocity,pressure,...)
  integer :: nx ,ny ,nz ,nvar
  integer :: nx_,ny_,nz_
  character(len=str_short), dimension(:), allocatable :: names
  integer :: iunit,ierr,unit
  character(len=str_medium) :: filename, dirname
  character(len=str_medium) :: dataname, buffer
  integer :: i,j,k
  integer :: nproc,proc,npx,npy,npz
  real(WP), dimension(:,:,:), allocatable :: data
  integer :: imin_,imax_
  integer :: jmin_,jmax_
  integer :: kmin_,kmax_
  integer :: var
  integer, dimension(6) :: dims
  integer, dimension(:), allocatable :: nxpp,nypp,nzpp
  integer, dimension(:,:,:), allocatable :: ind
  real(WP) :: dt
  real(WP) :: time
  
  ! Read file name from standard input
  print*,'========================'
  print*,'| Serial data splitter |'
  print*,'========================'
  print*
  print "(a,$)", " data to split : "
  read "(a)", dataname
  print "(a,$)", " Processors in x : "
  read "(i4)", npx
  print "(a,$)", " Processors in y : "
  read "(i4)", npy
  print "(a,$)", " Processors in z : "
  read "(i4)", npz
  nproc=npx*npy*npz
  
  ! Start reading data file ===============================
  ! Open file
  call BINARY_FILE_OPEN(iunit,trim(dataname),"r",ierr)
  ! Read parameters
  call BINARY_FILE_READ(iunit,nx,1,kind(nx),ierr)
  call BINARY_FILE_READ(iunit,ny,1,kind(ny),ierr)
  call BINARY_FILE_READ(iunit,nz,1,kind(nz),ierr)
  call BINARY_FILE_READ(iunit,nvar,1,kind(nvar),ierr)
  ! Read time info
  call BINARY_FILE_READ(iunit,dt,1,kind(dt),ierr)
  call BINARY_FILE_READ(iunit,time,1,kind(time),ierr)
  ! Read variable names
  allocate(names(1:nvar))
  do var=1,nvar
     call BINARY_FILE_READ(iunit,names(var),str_short,kind(names),ierr)
  end do
  ! Allocate data array
  allocate(data(nx,ny,nz))
  
  ! Prepare serial data directory and header===============
  ! Create directory name
  dirname = trim(adjustl(dataname)) // '_serial'
  call system("mkdir -p "//trim(dirname))
  ! Create header file
  filename = trim(dirname) // "/data.header"
  ! Open file
  call BINARY_FILE_OPEN(unit,trim(filename),"w",ierr)
  ! Write common integer info
  call BINARY_FILE_WRITE(unit,nproc,1,kind(nproc),ierr)
  call BINARY_FILE_WRITE(unit,nx,1,kind(nx),ierr)
  call BINARY_FILE_WRITE(unit,ny,1,kind(ny),ierr)
  call BINARY_FILE_WRITE(unit,nz,1,kind(nz),ierr)
  call BINARY_FILE_WRITE(unit,nvar,1,kind(nvar),ierr)
  ! Write time info
  call BINARY_FILE_WRITE(unit,dt,1,kind(dt),ierr)
  call BINARY_FILE_WRITE(unit,time,1,kind(time),ierr)
  ! Write variable names
  do var=1,nvar
     call BINARY_FILE_WRITE(unit,names(var),str_short,kind(names),ierr)
  end do
  ! Close the file
  call BINARY_FILE_CLOSE(unit,ierr)
  
  ! Prepare domain decomposition ==========================
  allocate(nxpp(1:npx));nxpp=nx/npx;nxpp(1:mod(nx,npx))=nxpp(1:mod(nx,npx))+1
  allocate(nypp(1:npy));nypp=ny/npy;nypp(1:mod(ny,npy))=nypp(1:mod(ny,npy))+1
  allocate(nzpp(1:npz));nzpp=nz/npz;nzpp(1:mod(nz,npz))=nzpp(1:mod(nz,npz))+1
  allocate(ind(1:nproc,1:3,1:2))
  proc=0
  do i=1,npx
     do j=1,npy
        do k=1,npz
           ! Processor counter
           proc=proc+1
           ! Set indices
           ind(proc,1,1)=1+sum(nxpp(1:i-1))
           ind(proc,1,2)=  sum(nxpp(1:i  ))
           ind(proc,2,1)=1+sum(nypp(1:j-1))
           ind(proc,2,2)=  sum(nypp(1:j  ))
           ind(proc,3,1)=1+sum(nzpp(1:k-1))
           ind(proc,3,2)=  sum(nzpp(1:k  ))
        end do
     end do
  end do
  ! Loop over processors
  do proc=1,nproc
     ! Set bounds
     imin_=ind(proc,1,1);imax_=ind(proc,1,2);nx_=imax_-imin_+1
     jmin_=ind(proc,2,1);jmax_=ind(proc,2,2);ny_=jmax_-jmin_+1
     kmin_=ind(proc,3,1);kmax_=ind(proc,3,2);nz_=kmax_-kmin_+1
     ! Set filename to write
     write(buffer,'(i6.6)') proc
     filename = trim(dirname) // "/data." // trim(adjustl(buffer))
     ! Open file
     call BINARY_FILE_OPEN(unit,trim(filename),"w",ierr)
     ! Write sizes
     dims(1)=imin_;dims(2)=jmin_;dims(3)=kmin_
     dims(4)=  nx_;dims(5)=  ny_;dims(6)=  nz_
     call BINARY_FILE_WRITE(unit,dims,6,kind(dims),ierr)
     ! Close file
     call BINARY_FILE_CLOSE(unit,ierr)
  end do
  
  ! Loop over each variable and split the data ============
  do var=1,nvar
     ! Print out variable name
     print*,'Splitting ',names(var)
     ! Read data field
     call BINARY_FILE_READ(iunit,data,nx*ny*nz,kind(data),ierr)
     ! Loop over processors
     do proc=1,nproc
        ! Set bounds
        imin_=ind(proc,1,1);imax_=ind(proc,1,2);nx_=imax_-imin_+1
        jmin_=ind(proc,2,1);jmax_=ind(proc,2,2);ny_=jmax_-jmin_+1
        kmin_=ind(proc,3,1);kmax_=ind(proc,3,2);nz_=kmax_-kmin_+1
        ! Set filename to write
        write(buffer,'(i6.6)') proc
        filename = trim(dirname) // "/data." // trim(adjustl(buffer))
        ! Open file
        call BINARY_FILE_OPEN(unit,trim(filename),"a",ierr)
        ! Write data chunck
        call BINARY_FILE_WRITE(unit,data(imin_:imax_,jmin_:jmax_,kmin_:kmax_),nx_*ny_*nz_,kind(data),ierr)
        ! Close file
        call BINARY_FILE_CLOSE(unit,ierr)
     end do
  end do
  
  ! Close data file to read ===============================
  call BINARY_FILE_CLOSE(iunit,ierr)
  
end program serialdata_splitter
