! ======================================= !
! Tool that performs PIV-type tracking    !
! based on time-dependent structure files !
! ======================================= !
module trackstruct
  use string
  use precision
  implicit none
  
  ! Structure data (19 columns + time)
  integer, parameter :: ncol=20
  integer, parameter :: ncalc=7
  
  ! Periodicity
  real(WP) :: xL  ,yL  ,zL
  logical  :: xper,yper,zper
  
  ! Tolerance
  real(WP) :: tolerance
  
  ! I/O data
  integer :: nfirst,nlast
  character(len=str_medium) :: prefix
  character(len=str_medium) :: direct
  
  ! Time information
  real(WP), dimension(:), allocatable :: time
  
  ! Temporary storage for new structures
  integer :: nnew
  real(WP), dimension(:,:), pointer :: new_structs
  
  ! ======================================== !
  ! Object-oriented handling of trajectories !
  ! ======================================== !
  ! Trajectory "node"
  type trajectory_type
     real(WP), dimension(:,:), pointer :: data
     real(WP), dimension(:,:), pointer :: calc
     type(trajectory_type), pointer :: next
     type(trajectory_type), dimension(:), pointer :: parents
     type(trajectory_type), dimension(:), pointer :: children
  end type trajectory_type
  ! Singly linked list of trajectories
  type trajectory_list
     type(trajectory_type), pointer :: first
  end type trajectory_list
  
  ! Lists
  type(trajectory_list) :: active,inactive
  
contains
  
  ! Create new list
  function trajectory_list_create() result(list)
    type(trajectory_list) :: list
    ! Create memory for the object
    allocate(list%first)
    ! Start from empty list
    nullify(list%first%next)
  end function trajectory_list_create
  
end module trackstruct


! =========== !
! Main driver !
! =========== !
program tracker
  call track_init
  call track_struct
  call track_dump
end program tracker


! ================================= !
! Initialize the tracking algorithm !
! ================================= !
subroutine track_init
  use trackstruct
  use parser
  use fileio
  implicit none
  
  character(len=str_medium) :: ftime
  integer :: ntimes
  real(WP), dimension(:), pointer :: times
  
  ! Input file processing
  character(len=str_medium) :: input_name
  call get_command_argument(1,input_name)
  call parser_init; call parser_parsefile(input_name)
  
  ! Handle periodicity
  call parser_read('xper',xper,.false.)
  call parser_read('yper',yper,.false.)
  call parser_read('zper',zper,.false.)
  if (xper) call parser_read('xL',xL)
  if (yper) call parser_read('yL',yL)
  if (zper) call parser_read('zL',zL)
  
  ! Tolerance for trajectory matching
  call parser_read('Tolerance',tolerance)
  
  ! Gather information on structure files
  call parser_readchar('Structure prefix',prefix)
  call parser_readchar('Output directory',direct)
  call parser_read('First file',nfirst,-1)
  call parser_read('Last file',nlast,-1)
  
  ! Read the timing info
  call parser_readchar('Time info',ftime)
  call parser_parsefile(trim(ftime))
  call parser_getsize('time values',ntimes)
  allocate(times(ntimes))
  call parser_read('time values',times)
  if (nfirst.eq.-1) then
     nfirst=1
     nlast=ntimes
  end if
  allocate(time(nfirst:nlast))
  time(nfirst:nlast)=times(nfirst:nlast)
  deallocate(times); nullify(times)
  
  ! Initialize two lists
  active  =trajectory_list_create()
  inactive=trajectory_list_create()
  
  return
end subroutine track_init


! ==================== !
! Perform the tracking !
! ==================== !
subroutine track_struct
  use trackstruct
  use fileio
  implicit none
  
  character(len=str_medium) :: filename,cbuf
  integer :: nf,iunit,code,tag,i
  
  ! Loop over files
  do nf=nfirst,nlast
     
     ! Read structure file in temporary structure array
     filename=trim(prefix)//'.'
     write(filename(len_trim(filename)+1:len_trim(filename)+6),'(i6.6)') nf
     iunit = iopen()
     open(iunit,file=filename,form="formatted",iostat=code,status='old',action='read')
     read(iunit,'(a)',iostat=code) cbuf
     nnew=0
     do while (code.eq.0)
        nnew=nnew+1
        read(iunit,'(a)',iostat=code) cbuf
     end do
     nnew=nnew-1
     if (associated(new_structs)) deallocate(new_structs)
     allocate(new_structs(ncol,nnew))
     rewind(iunit)
     read(iunit,'(a)',iostat=code) cbuf
     nnew=0
     do while (code.eq.0)
        nnew=nnew+1
        read(iunit,'(I20,10000ES20.12)',iostat=code) tag,new_structs(2:ncol,nnew)
     end do
     new_structs(1,:)=time(nf)
     nnew=nnew-1
     close(iclose(iunit))
     
     ! Match trajectories
     do i=1,nnew
        call match(new_structs(:,i))
     end do
     
     ! Transfer unmatched trajectories to other list
     call clean_inactive(time(nf))
     
     ! Print status
     print*,trim(filename),' processed.'
     
  end do
  
  return
end subroutine track_struct


! ============================================= !
! Match new structures to existing trajectories !
! ============================================= !
subroutine match(struct)
  use trackstruct
  implicit none
  
  real(WP), dimension(ncol), intent(in) :: struct
  type(trajectory_type), pointer :: previous,current,best
  real(WP) :: distance,best_dist,dt
  real(WP), dimension(3) :: Tpos,Spos,Vel,best_Spos
  real(WP), dimension(:,:), allocatable :: buf
  integer :: old_size,new_size
  
  ! Find closest active trajectory
  best_dist=huge(1.0_WP)
  previous=>active%first
  current=>previous%next
  do
     ! End of list
     if (.not.associated(current)) exit
     ! Cycle already matched trajectories
     if (current%data(1,size(current%data,2)).lt.struct(1)) then
        ! Measure distance
        Vel =(/current%calc(5,size(current%data,2)),&
               current%calc(6,size(current%data,2)),&
               current%calc(7,size(current%data,2))/)
        Tpos=(/current%calc(2,size(current%data,2)),&
               current%calc(3,size(current%data,2)),&
               current%calc(4,size(current%data,2))/)
        dt  =struct(1)-current%data(1,size(current%data,2))
        Tpos=Tpos+dt*Vel
        Spos=(/struct(3),struct(4),struct(5)/)
        if (xper) then ! X periodicity
           do while (abs(Tpos(1)-(Spos(1)+xL)).lt.abs(Tpos(1)-Spos(1)))
              Spos(1)=Spos(1)+xL
           end do
           do while (abs(Tpos(1)-(Spos(1)-xL)).lt.abs(Tpos(1)-Spos(1)))
              Spos(1)=Spos(1)-xL
           end do
        end if
        if (yper) then ! Y periodicity
           do while (abs(Tpos(2)-(Spos(2)+yL)).lt.abs(Tpos(2)-Spos(2)))
              Spos(2)=Spos(2)+yL
           end do
           do while (abs(Tpos(2)-(Spos(2)-yL)).lt.abs(Tpos(2)-Spos(2)))
              Spos(2)=Spos(2)-yL
           end do
        end if
        if (zper) then ! Z periodicity
           do while (abs(Tpos(3)-(Spos(3)+zL)).lt.abs(Tpos(3)-Spos(3)))
              Spos(3)=Spos(3)+zL
           end do
           do while (abs(Tpos(3)-(Spos(3)-zL)).lt.abs(Tpos(3)-Spos(3)))
              Spos(3)=Spos(3)-zL
           end do
        end if
        distance=sqrt(dot_product(Tpos-Spos,Tpos-Spos))
        ! Pick best trajectory
        if (distance.lt.best_dist) then
           best_dist=distance
           best=>current
           best_Spos=Spos
        end if
     end if
     ! Go to next trajectory
     previous=>previous%next
     current=>current%next
  end do
  
  ! Decide what to do based on tolerance
  if (best_dist.lt.tolerance) then
     ! Add new entry to best trajectory
     old_size=size(best%data,2)
     new_size=old_size+1
     allocate(buf(ncol,new_size))
     buf(:,1:old_size)=best%data
     buf(:,new_size)=struct
     deallocate(best%data)
     allocate(best%data(ncol,new_size))
     best%data=buf
     deallocate(buf)
     allocate(buf(ncalc,new_size))
     buf(:,1:old_size)=best%calc
     ! Calc new data
     buf(1,new_size)=struct(1)-best%data(1,1) ! Shifted time
     buf(2:4,new_size)=best_Spos(1:3) ! Shifted position
     buf(5,new_size)=(buf(2,new_size)-buf(2,new_size-1))/(buf(1,new_size)-buf(1,new_size-1)) ! x velocity
     buf(6,new_size)=(buf(3,new_size)-buf(3,new_size-1))/(buf(1,new_size)-buf(1,new_size-1)) ! y velocity
     buf(7,new_size)=(buf(4,new_size)-buf(4,new_size-1))/(buf(1,new_size)-buf(1,new_size-1)) ! z velocity
     deallocate(best%calc)
     allocate(best%calc(ncalc,new_size))
     best%calc=buf
     deallocate(buf)
  else
     ! Create new trajectory after previous
     allocate(previous%next)
     nullify(previous%next%next)
     allocate(previous%next%data(ncol,1))
     previous%next%data(:,1)=struct
     allocate(previous%next%calc(ncalc,1))
     previous%next%calc(1,1)=0.0_WP ! Shifted time
     previous%next%calc(2:4,1)=previous%next%data(3:5,1) ! Shifted position
     previous%next%calc(5:7,1)=0.0_WP ! Velocity
  end if
  
  return
end subroutine match


! ============================================== !
! Move inactive trajectories at t to "done" list !
! ============================================== !
subroutine clean_inactive(t)
  use trackstruct
  implicit none
  
  real(WP), intent(in) :: t
  type(trajectory_type), pointer :: previous
  type(trajectory_type), pointer :: current
  type(trajectory_type), pointer :: last
  
  ! First locate the end of the inactive list
  last=>inactive%first
  do
     if (.not.associated(last%next)) exit
     last=>last%next
  end do
  
  ! Traverse the active list
  previous=>active%first
  current=>previous%next
  do
     ! End of list
     if (.not.associated(current)) return
     if (current%data(1,size(current%data,2)).eq.t) then
        ! Trajectory was just updated, stay active
        previous=>previous%next
        current=>current%next
     else
        ! Trajectory was not updated, become inactive
        previous%next=>current%next
        ! Add current to inactive list
        nullify(current%next)
        last%next=>current
        ! Update last term in inactive list
        last=>last%next
        ! Reset current allocatable to continue
        current=>previous%next
     end if
  end do
  
  return
end subroutine clean_inactive


! =============================== !
! Dump generated data to the disk !
! =============================== !
subroutine track_dump
  use trackstruct
  use fileio
  implicit none
  
  character(len=str_medium) :: filename
  type(trajectory_type), pointer :: current
  integer :: count,iunit,i
  
  ! Count trajectories
  count=0
  current=>active%first%next
  do
     if (.not.associated(current)) exit
     if (size(current%calc,2).gt.100) then
        count=count+1
     end if
     current=>current%next
  end do
  current=>inactive%first%next
  do
     if (.not.associated(current)) exit
     if (size(current%calc,2).gt.100) then
        count=count+1
     end if
     current=>current%next
  end do
  print*,'Tracked',count,'trajectories'
    
  ! Transfer data
  count=0
  current=>active%first%next
  do
     if (.not.associated(current)) exit
     if (size(current%calc,2).gt.100) then
        count=count+1
        filename=trim(direct)//'/trajectory.'
        write(filename(len_trim(filename)+1:len_trim(filename)+6),'(i6.6)') count
        iunit=iopen()
        open(iunit,file=filename,form="formatted",status='replace',action='write')
        do i=1,size(current%calc,2)
           !write(iunit,'(10000ES20.12)') current%calc(:,i),current%data(:,i)
           write(iunit,'(10000ES20.12)') current%data(1,i),current%calc(4,i),current%calc(7,i)
        end do
        close(iclose(iunit))
     end if
     current=>current%next
  end do
  current=>inactive%first%next
  do
     if (.not.associated(current)) exit
     if (size(current%calc,2).gt.100) then
        count=count+1
        filename=trim(direct)//'/trajectory.'
        write(filename(len_trim(filename)+1:len_trim(filename)+6),'(i6.6)') count
        iunit=iopen()
        open(iunit,file=filename,form="formatted",status='replace',action='write')
        do i=1,size(current%calc,2)
           !write(iunit,'(10000ES20.12)') current%calc(:,i),current%data(:,i)
           write(iunit,'(10000ES20.12)') current%data(1,i),current%calc(4,i),current%calc(7,i)
        end do
        close(iclose(iunit))
     end if
     current=>current%next
  end do
    
  return
end subroutine track_dump
