program config2ensight
  use precision
  use string
  use fileio
  implicit none

  ! Data array with all the variables (velocity,pressure,...)
  integer :: nx,ny,nz
  integer :: xper,yper,zper
  integer :: icyl
  real(WP), dimension(:), allocatable :: xWP,yWP,zWP
  integer, dimension(:,:), allocatable :: mask
  integer :: ierr,iunit
  character(len=str_medium) :: filename1,filename2,directory
  character(len=str_medium) :: config
  integer :: i,j,k,i1,i2,j1,j2
  integer :: usemask,is_uniform

  integer, dimension(:,:,:), allocatable :: iblank
  character(len=80) :: buffer
  real(SP), dimension(:), allocatable :: x,y,z
  real(SP), dimension(:), allocatable :: xm,ym,zm
  real(SP) :: max_x,max_y,max_z
  real(SP) :: min_x,min_y,min_z
  real(SP) :: x_delta,y_delta,z_delta
  integer :: ipart

  ! Read file name from standard input
  print*,'==========================================='
  print*,'| ARTS - config to ENSIGHT GOLD converter |'
  print*,'==========================================='
  print*
  print "(a15,$)", " config file : "
  read "(a)", filename1
  print "(a21,$)", " ensight directory : "
  read "(a)", directory
  print "(a29,$)", " uniform grid (yes=1/no=0) : "
  read "(i5)", is_uniform
  print "(a25,$)", " use mask (yes=1/no=0) : "
  read "(i5)", usemask
  
  !call CREATE_FOLDER(trim(directory))
  call system("mkdir -p "//trim(directory))
  filename2 = trim(directory) // '/geometry'

  ! ** Open the config file to read **
  call BINARY_FILE_OPEN(iunit,trim(filename1),"r",ierr)
  
  ! Read sizes
  call BINARY_FILE_READ(iunit,config,str_medium,kind(config),ierr)
  call BINARY_FILE_READ(iunit,icyl,1,kind(icyl),ierr)
  call BINARY_FILE_READ(iunit,xper,1,kind(xper),ierr)
  call BINARY_FILE_READ(iunit,yper,1,kind(yper),ierr)
  call BINARY_FILE_READ(iunit,zper,1,kind(zper),ierr)
  call BINARY_FILE_READ(iunit,nx,1,kind(nx),ierr)
  call BINARY_FILE_READ(iunit,ny,1,kind(ny),ierr)
  call BINARY_FILE_READ(iunit,nz,1,kind(nz),ierr)
  print*,'Config : ',config
  print*,'Grid :',nx,'x',ny,'x',nz
  
  ! Read grid field
  allocate(xWP(nx+1),yWP(ny+1),zWP(nz+1))
  if (usemask.eq.1) allocate(mask(nx,ny))
  call BINARY_FILE_READ(iunit,xWP,nx+1,kind(xWP),ierr)
  call BINARY_FILE_READ(iunit,yWP,ny+1,kind(yWP),ierr)
  call BINARY_FILE_READ(iunit,zWP,nz+1,kind(zWP),ierr)
  if (usemask.eq.1) call BINARY_FILE_READ(iunit,mask,nx*ny,kind(mask),ierr)
  
  ! Close the file
  call BINARY_FILE_CLOSE(iunit,ierr)

  ! Create arrays necessary for Ensight format
  allocate(x(1:nx+1),y(1:ny+1),z(1:nz+1))
  allocate(xm(1:nx),ym(1:ny),zm(1:nz))
  if (usemask.eq.1) then
     allocate(iblank(nx+1,ny+1,nz+1)); iblank=1
     do k=1,nz+1
        do j=1,ny+1
           do i=1,nx+1
              i1=max(i-1,1); i2=min(i,nx)
              j1=max(j-1,1); j2=min(j,ny)
              if ( (mask(i1,j1).eq.1 .or. mask(i1,j1).eq.3) .and. &
                   (mask(i2,j1).eq.1 .or. mask(i2,j1).eq.3) .and. &
                   (mask(i1,j2).eq.1 .or. mask(i1,j2).eq.3) .and. &
                   (mask(i2,j2).eq.1 .or. mask(i2,j2).eq.3)) then
                 iblank(i,j,k)=0
              end if
           end do
        end do
     end do
  end if

  x = real(xWP,SP)
  y = real(yWP,SP)
  z = real(zWP,SP)
  do i=1,nx
     xm(i) = 0.5_SP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_SP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_SP*(z(k)+z(k+1))
  end do
  
  max_x = x(nx+1)
  max_y = y(ny+1)
  max_z = z(nz+1)
  min_x = x(1)
  min_y = y(1)
  min_z = z(1)
  
  x_delta=(max_x-min_x)/real(nx,SP)
  y_delta=(max_y-min_y)/real(ny,SP)
  z_delta=(max_z-min_z)/real(nz,SP)
  
  ! ** Open the grid file to write **
  call BINARY_FILE_OPEN(iunit,trim(filename2),"w",ierr)

  ! Write the geometry
  buffer = 'C Binary'
  call BINARY_FILE_WRITE(iunit,buffer,80,kind(buffer),ierr)
  buffer = 'Ensight Gold Geometry File'
  call BINARY_FILE_WRITE(iunit,buffer,80,kind(buffer),ierr)
  buffer = 'Structured Geometry from ARTS'
  call BINARY_FILE_WRITE(iunit,buffer,80,kind(buffer),ierr)
  buffer = 'node id off'
  call BINARY_FILE_WRITE(iunit,buffer,80,kind(buffer),ierr)
  buffer = 'element id off'
  call BINARY_FILE_WRITE(iunit,buffer,80,kind(buffer),ierr)

  buffer = 'extents'
  call BINARY_FILE_WRITE(iunit,buffer,80,kind(buffer),ierr)
  call BINARY_FILE_WRITE(iunit,min_x,1,kind(min_x),ierr)
  call BINARY_FILE_WRITE(iunit,max_x,1,kind(max_x),ierr)
  call BINARY_FILE_WRITE(iunit,min_y,1,kind(min_y),ierr)
  call BINARY_FILE_WRITE(iunit,max_y,1,kind(max_y),ierr)
  call BINARY_FILE_WRITE(iunit,min_z,1,kind(min_z),ierr)
  call BINARY_FILE_WRITE(iunit,max_z,1,kind(max_z),ierr)

  ! Cell centers
  buffer = 'part'
  call BINARY_FILE_WRITE(iunit,buffer,80,kind(buffer),ierr)
  ipart = 1
  call BINARY_FILE_WRITE(iunit,ipart,1,kind(ipart),ierr)

  buffer = 'Complete geometry'
  call BINARY_FILE_WRITE(iunit,buffer,80,kind(buffer),ierr)

  if (is_uniform.eq.1) then
     
     if (usemask.eq.1) then
        buffer = 'block uniform iblanked'
     else
        buffer = 'block uniform'
     end if
     call BINARY_FILE_WRITE(iunit,buffer,80,kind(buffer),ierr)

     call BINARY_FILE_WRITE(iunit,nx+1,1,kind(nx),ierr)
     call BINARY_FILE_WRITE(iunit,ny+1,1,kind(ny),ierr)
     call BINARY_FILE_WRITE(iunit,nz+1,1,kind(nz),ierr)

     call BINARY_FILE_WRITE(iunit,min_x,1,kind(min_x),ierr)
     call BINARY_FILE_WRITE(iunit,min_y,1,kind(min_y),ierr)
     call BINARY_FILE_WRITE(iunit,min_z,1,kind(min_z),ierr)

     call BINARY_FILE_WRITE(iunit,x_delta,1,kind(x_delta),ierr)
     call BINARY_FILE_WRITE(iunit,y_delta,1,kind(y_delta),ierr)
     call BINARY_FILE_WRITE(iunit,z_delta,1,kind(z_delta),ierr)
     
     if (usemask.eq.1) call BINARY_FILE_WRITE(iunit,iblank,(nx+1)*(ny+1)*(nz+1),kind(iblank),ierr)
     
  else
     
     if (usemask.eq.1) then
        buffer = 'block rectilinear iblanked'
     else
        buffer = 'block rectilinear'
     end if
     call BINARY_FILE_WRITE(iunit,buffer,80,kind(buffer),ierr)
     
     call BINARY_FILE_WRITE(iunit,nx,1,kind(nx),ierr)
     call BINARY_FILE_WRITE(iunit,ny,1,kind(ny),ierr)
     call BINARY_FILE_WRITE(iunit,nz,1,kind(nz),ierr)
  
     call BINARY_FILE_WRITE(iunit,xm,nx,kind(xm),ierr)
     call BINARY_FILE_WRITE(iunit,ym,ny,kind(ym),ierr)
     call BINARY_FILE_WRITE(iunit,zm,nz,kind(zm),ierr)
     
     if (usemask.eq.1) call BINARY_FILE_WRITE(iunit,iblank,(nx+1)*(ny+1)*(nz+1),kind(iblank),ierr)
     
  end if
  
  ! Close the file
  call BINARY_FILE_CLOSE(iunit,ierr)
  
end program config2ensight
