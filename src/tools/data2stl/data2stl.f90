program data2stl
  use precision
  use string
  use fileio
  use parser
  use cli_reader
  use compgeom
  implicit none

  ! STL file
  integer :: my_ntri
  real(WP), dimension(:,:,:), allocatable :: tri
  real(WP) :: iso
  real(WP), dimension(3,8) :: cpos
  real(WP), dimension(  8) :: cval
  real(WP), dimension(  3) :: vec1,vec2,norm1,norm2,tmpV
  character(len=80) :: file_header
  integer*4 :: n_tri
  real(WP) :: norm,normx,normy,normz
  character(len=2)  :: buffer
  
  character(len=str_medium) :: filename1,filename2,filename3,cbuffer
  integer  :: iunit1,iunit2,iunit3,ierr
  integer  :: nx,ny,nz,nvar
  real(WP) :: dt,time
  integer  :: var,i,j,k,n,nn,si,sj,sk
  real(WP), dimension(:),     allocatable :: x ,y ,z
  real(WP), dimension(:),     allocatable :: xm,ym,zm
  real(WP), dimension(:,:,:), allocatable :: data
  character(len=str_short), dimension(:), allocatable :: names
  character(len=str_short) :: varname
  integer :: nArgs
  
  ! Read file name from standard input
  print*,'================================='
  print*,'| ARTS - Gdata to stl converter |'
  print*,'================================='
  print*
  nArgs=iargc()
  if (nArgs.eq.5) then
     call GETARG(1 , filename1)
     call GETARG(2 , filename2)
     call GETARG(3 , filename3)
     call GETARG(4 , varname)
     call GETARG(5 , cbuffer)
     read(cbuffer,*) iso
  else
     print "(a16,$)", "> config file : "
     read "(a)", filename1
     print "(a16,$)", ">   data file : "
     read "(a)", filename2
     print "(a16,$)", ">    stl file : "
     read "(a)", filename3
  end if

  print *,'=================='
  print *,'config: ',filename1
  print *,'data:   ',filename2
  print *,'stl:    ',filename3
  
  ! ****** Open the config file to read *****
  call BINARY_FILE_OPEN(iunit1,trim(filename1),"r",ierr)

  ! Read the header
  call BINARY_FILE_READ(iunit1,nx,1,kind(nx),ierr)
  call BINARY_FILE_READ(iunit1,ny,1,kind(ny),ierr)
  call BINARY_FILE_READ(iunit1,nz,1,kind(nz),ierr)
  print*,'Config :',nx,'x',ny,'x',nz
  
  ! Read the grid
  allocate(x(nx+1),y(ny+1),z(nz+1))
  allocate(xm(nx), ym(ny), zm(nz))
  call BINARY_FILE_READ(iunit1,x,nx+1,kind(x),ierr)
  call BINARY_FILE_READ(iunit1,y,ny+1,kind(y),ierr)
  call BINARY_FILE_READ(iunit1,z,nz+1,kind(z),ierr)
  
  ! Close the config file
  call BINARY_FILE_CLOSE(iunit1,ierr)
  
  ! Create the cell centered grid
  do i=1,nx
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=1,ny
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do
  do k=1,nz
     zm(k) = 0.5_WP*(z(k)+z(k+1))
  end do
  
  ! ***** Open the data file to read *****
  call BINARY_FILE_OPEN(iunit2,trim(filename2),"r",ierr)
  
  ! Read the header
  call BINARY_FILE_READ(iunit2,nx,1,kind(nx),ierr)
  call BINARY_FILE_READ(iunit2,ny,1,kind(ny),ierr)
  call BINARY_FILE_READ(iunit2,nz,1,kind(nz),ierr)
  call BINARY_FILE_READ(iunit2,nvar,1,kind(nvar),ierr)
  call BINARY_FILE_READ(iunit2,dt,1,kind(dt),ierr)
  call BINARY_FILE_READ(iunit2,time,1,kind(time),ierr)
  print*,'Data :  ',nx,'x',ny,'x',nz
  
  ! Read variable names
  allocate(names(nvar))
  do var=1,nvar
     call BINARY_FILE_READ(iunit2,names(var),str_short,kind(names),ierr)
  end do
  print*,'Variables : ',names
  
  ! Variable to read
  print "(a16,$)", ">    variable : "
  read "(a)", varname
  print "(a16,$)", ">    isovalue : "
  read(*,*) iso
  
  ! Allocate arrays
  allocate(data(nx,ny,nz))
  allocate(tri(3,4,12))
  
  do var=1,nvar
     ! Read data field
     call BINARY_FILE_READ (iunit2,data(:,:,:),nx*ny*nz,kind(data),ierr)
     if (names(var).ne.varname) cycle
     
     ! ***** Open the stl file to write *****
     call BINARY_FILE_OPEN(iunit3,trim(filename3),"w",ierr)
     
     ! Write stl header
     file_header='file-header'
     n_tri=0
     call BINARY_FILE_WRITE(iunit3,file_header,80,kind(file_header),ierr)
     call BINARY_FILE_WRITE(iunit3,n_tri,1,kind(n_tri),ierr)
     
     print *,'maxval=',maxval(data)
     print *,'minval=',minval(data)
     print *,'isoval=',iso
     
     ! Make stl triangles
     do k=1,nz-1
        do j=1,ny-1
           do i=1,nx-1
              
              ! Form 3d sub-cell
              n=0
              do sk=0,1
                 do sj=0,1
                    do si=0,1
                       n=n+1
                       cpos(:,n)=(/ xm(i+si),ym(j+sj),zm(k+sk) /)
                       cval(  n)=data(i+si,j+sj,k+sk)
                    end do
                 end do
              end do
              
              ! Estimate normal
              normx=-(data(i+1,j,k)-data(i,j,k))/(xm(i+1)-xm(i))
              normy=-(data(i,j+1,k)-data(i,j,k))/(ym(j+1)-ym(j))
              normz=-(data(i,j,k+1)-data(i,j,k))/(zm(k+1)-zm(k))
              norm=sqrt(normx**2+normy**2+normz**2)+epsilon(1.0_WP)
              normx=normx/norm
              normy=normy/norm
              normz=normz/norm
              
              ! Calculate triangles
              do nn=1,12
                 tri(:,1,nn)=(/normx,normy,normz/)
              end do
              call triangulate_3cell(iso,cpos,cval,my_ntri,tri(:,2:4,1:12))
              
              ! Flip triangles to follow right hand rule
              do nn=1,my_ntri
                 vec1=tri(:,3,nn)-tri(:,2,nn)
                 vec2=tri(:,4,nn)-tri(:,2,nn)
                 norm1=normalize(cross_product(vec1,vec2))
                 norm2=(/normx,normy,normz/)
                 if (dot_product(norm1,norm2).lt.0.0_WP) then ! Flip triangle
                    tmpV=tri(:,4,nn)
                    tri(:,4,nn)=tri(:,3,nn)
                    tri(:,3,nn)=tmpV
                 end if
              end do

              ! Update triangle counter
              n_tri=n_tri+my_ntri
              
              ! Write stl file
              do nn=1,my_ntri
                 call BINARY_FILE_WRITE(iunit3,real(tri(:,1,nn),SP),3,kind(real(tri,SP)),ierr)
                 call BINARY_FILE_WRITE(iunit3,real(tri(:,2,nn),SP),3,kind(real(tri,SP)),ierr)
                 call BINARY_FILE_WRITE(iunit3,real(tri(:,3,nn),SP),3,kind(real(tri,SP)),ierr)
                 call BINARY_FILE_WRITE(iunit3,real(tri(:,4,nn),SP),3,kind(real(tri,SP)),ierr)
                 call BINARY_FILE_WRITE(iunit3,buffer,2,kind(buffer),ierr)
              end do
           end do
        end do
     end do
  end do 
  
  print *,'Wrote ',n_tri,' triangles'

  call BINARY_FILE_REWIND(iunit3,ierr)
  call BINARY_FILE_WRITE(iunit3,file_header,80,kind(file_header),ierr)
  call BINARY_FILE_WRITE(iunit3,n_tri,1,kind(n_tri),ierr)

  ! Close the files
  call BINARY_FILE_CLOSE(iunit2,ierr)
  call BINARY_FILE_CLOSE(iunit3,ierr)

end program data2stl
