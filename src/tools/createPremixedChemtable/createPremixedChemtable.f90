program createPremixedChemtable
  use premixed_table
  use parser
  use precision
  use string
  implicit none

  integer :: n
  
  ! Filename of input file
  character(len=str_medium) :: name

  ! -------------------------------------------------------

  ! Parse the input file
  call get_command_argument(1,name)
  call parser_init
  call parser_parsefile(name)
  
  ! Read the list of files
  call parser_getsize("List of Flamelets", nfiles)
  allocate(files(nfiles))
  call parser_read("List of Flamelets", files)
  
  ! Initialize the modules
  call premixed_flamelet_init
  call premixed_table_init

  ! -------------------------------------------------------

  print*,''
  print*,'** Files in the table **'
  do n=1,nfiles
     write(*,'(a)') trim(files(n))
     ! Read the file
     call premixed_flamelet_readfile(files(n))
     ! Convolute with PDF
     call premixed_table_convolute(n)
     ! Deallocate the data array
     call premixed_flamelet_cleanup
  end do

  ! Change the variables names
  call premixed_table_convert_names
  ! Compute the table
  call premixed_table_setup
  ! Consider the lean and rich flammability limits
  call premixed_table_quenching
  ! Print some statistics
  call premixed_table_stats
  ! Write the table
  call premixed_table_write
  
end program createPremixedChemtable

