module premixed_table
  use premixed_flamelet
  use precision
  use string
  implicit none
  
  ! Table parameters
  integer :: nCMean, nCVar, nZMean
  real(WP), dimension(:), allocatable :: CMean, CVar, ZMean
  
  ! Beta pdf & normalized progress variable
  real(WP), dimension(:), pointer :: pdf
  real(WP), dimension(:), pointer :: C
  
  ! List of Flamelet files
  integer :: nfiles
  character(len=str_long), dimension(:), allocatable :: files
  
  ! Variables to be wrtitten into the table
  integer :: nvar_out
  character(len=str_medium), dimension(:), allocatable :: output_name
  real(WP), dimension(:,:,:,:), allocatable :: output_data
  integer,  dimension(:,:,:),   allocatable :: mask
  
  ! Variable after convolution
  real(WP), dimension(:,:,:,:), pointer :: postconv
  
contains
  
  ! Linear mesh between min & max
  ! -----------------------------
  subroutine create_cmean
    use parser
    implicit none
    integer  :: icm
    real(WP) :: minC,maxC
    integer  :: nlog
    
    ! Get min/max of C
    call parser_read('Estimated min C',minC,0.0_WP)
    call parser_read('Estimated max C',maxC,0.4_WP)
    
    ! Get number of log points
    call parser_read('Number of log points',nlog,1)
    
    ! Log & Linear progression
    CMean(1) = minC
    do icm=2,nlog-1
       CMean(icm) = minC + (maxC-minC)/real(nCMean-nlog+1,WP)*0.5_WP**(nlog-icm)
    end do
    do icm=nlog,nCMean
       CMean(icm) = minC + real(icm-nlog+1,WP)*(maxC-minC)/real(nCMean-nlog+1,WP)
    end do
    
    return
  end subroutine create_cmean
  
  
  ! Quadratic mesh between 0 and 0.25
  ! ---------------------------------
  subroutine create_cvar
    implicit none
    integer :: icv
    real(WP) :: minC,maxC
    
    ! Get min/max of C
    minC = minval(CMean)
    maxC = maxval(CMean)

    do icv=1,nCVar
       CVar(icv) = 0.25_WP * (maxC-minC)**2 * (real(icv-1,WP) / real(nCVar-1,WP))**2
    end do

    return
  end subroutine create_cvar

  
  ! One point for each boundary: 0 and 1
  ! One point for each flammability limit
  ! n-4 points between min/max of phi 
  ! -------------------------------------
  subroutine create_zmean
    use parser
    implicit none
    
    integer  :: zm
    real(WP) :: Zst,phi_LFL,phi_RFL
    real(WP) :: Zmin, Zmax
    
    ! Get flammability limits
    call parser_read('Stoichiometric mixture fraction',Zst)
    call parser_read('Lean flammability limit',phi_LFL)
    call parser_read('Rich flammability limit',phi_RFL)
    
    ! Force the bounds to be 0 and 1
    ZMean(1)      = 0.0_WP
    ZMean(nZMean) = 1.0_WP
    
    ! Force the flammability limits
    !ZMean(2)        = phi_LFL / (phi_LFL + (1.0_WP-Zst)/Zst)
    !ZMean(nZMean-1) = phi_RFL / (phi_RFL + (1.0_WP-Zst)/Zst)
    ZMean(1)      = phi_LFL / (phi_LFL + (1.0_WP-Zst)/Zst)
    ZMean(nZMean) = phi_RFL / (phi_RFL + (1.0_WP-Zst)/Zst)
    
    ! Get min/max Z from flamelets
    Zmin = minval(postconv(nvar_in-2,:,:,:))
    Zmax = maxval(postconv(nvar_in-2,:,:,:))
    
    ! Linear distribution of points between Zmin and Zmax
    !do zm=1,nZMean-4
    !   ZMean(zm+2) = Zmin + (Zmax-Zmin)*real(zm-1,WP)/real(nZMean-5,WP)
    !end do
    do zm=1,nZMean-2
       ZMean(zm+1) = Zmin + (Zmax-Zmin)*real(zm-1,WP)/real(nZMean-3,WP)
    end do
    
    return
  end subroutine create_zmean
  
  
  ! Precompute the PDF for a beta distribution with
  ! -> mean progress variable : cm
  ! -> variance of progress variable : cv
  ! All normalized quantities, 0 < cm < 1
  ! -----------------------------------------------
  subroutine create_beta_pdf(cm,cv)
    use math
    implicit none
    
    real(WP), intent(in)  :: cm,cv
    real(WP) :: a,b,factor,tmp,dc
    integer  :: index1,n
    logical  :: bad_pdf
    
    pdf = 0.0_WP
    bad_pdf = .false.
    
    ! Zero mean : delta at C=0
    if (cm.le.1.0E-10_WP) then
       pdf(1) = 1.0_WP
       return
    end if
    
    ! Max mean : delta at C=1
    if (cm.ge.1.0_WP-1.0E-10_WP) then
       pdf(nPoints) = 1.0_WP
       return
    end if
    
    ! Zero variance : delta at C=cm
    if (cv.le.1.0E-10_WP) then
       index1 = 1
       do while (C(index1).lt.cm) 
          index1 = index1+1
       end do
       pdf(index1-1) = (C(index1)-cm)  /(C(index1)-C(index1-1))
       pdf(index1)   = (cm-C(index1-1))/(C(index1)-C(index1-1))
       return
    end if
    
    ! Impossible cases => two delta at 0 and 1
    if (cv.ge.cm*(1.0_WP-cm)) then
       pdf(1) = 1.0_WP-cm
       pdf(nPoints) = cm
       return
    end if
    
    a = cm*(cm*(1.0_WP-cm)/cv - 1.0_WP)
    b = a/cm - a
    factor = gammaln(a+b) - gammaln(a) - gammaln(b)
    
    ! Left BC : explicit integration
    dc = 0.5_WP*(C(2)-C(1))
    if (dc.gt.1.0e-10_WP) then
       tmp = a*log(dc) + factor
       pdf(1) = exp(tmp) / a
    end if
    ! Right BC : explicit integration
    dc = 0.5_WP*(C(nPoints)-C(nPoints-1))
    if (dc.gt.1.0e-10_WP) then
       tmp = b*log(dc) + factor
       pdf(nPoints) = exp(tmp) / b
    end if
    ! Other Points
    do n=2,nPoints-1
       dc = 0.5_WP*(C(n+1)-C(n-1))
       if (dc.gt.1.0e-10_WP) then
          tmp = (a-1.0_WP)*log(C(n)) + (b-1.0_WP)*log(1.0_WP-C(n))
          tmp = tmp + factor
          if (tmp.lt.-100.0_WP) then
             pdf(n) = 0.0_WP
          elseif (tmp.gt.-1.0_WP*log(dc)) then
             bad_pdf = .true.
          else
             pdf(n) = exp(tmp) * dc
          end if
       end if
    end do
    
    ! If bad pdf, then redo it assuming zero variance
    if (bad_pdf) then
       pdf = 0.0_WP
       index1 = 1
       do while (C(index1).lt.cm) 
          index1 = index1+1
       end do
       pdf(index1-1) = (C(index1)-cm)  /(C(index1)-C(index1-1))
       pdf(index1)   = (cm-C(index1-1))/(C(index1)-C(index1-1))
       return
    end if
    
    ! Normalize the pdf
    pdf = pdf / sum(pdf)
    
    return
  end subroutine create_beta_pdf
  
end module premixed_table


! ============================= !
! Initialize the table creation !
! ============================= !
subroutine premixed_table_init
  use premixed_table
  use parser
  implicit none
  
  ! Read the dimension of the final table
  call parser_read('Number of points for mean C', nCMean)
  call parser_read('Number of points for variance of C', nCVar)
  call parser_read('Number of points for mean Z', nZMean)
  allocate(CMean(nCMean))
  allocate(CVar (nCVar))
  allocate(ZMean(nZMean))
  
  ! Create the first two directions of the table
  call create_cmean
  call create_cvar
  
  ! Allocate arrays
  allocate(postconv(nvar_in,nCMean,nCVar,nfiles))
  
  return
end subroutine premixed_table_init
  

! ======================================================== !
! Convolute the data with a delta PDF of progress variable !
! ======================================================== !
subroutine premixed_table_convolute(file)
  use premixed_table
  implicit none
  
  integer, intent(in) :: file
  integer  :: icm, icv, k, var
  real(WP) :: minC,maxC
  real(WP) :: cm,cv,meanVal
    
  ! Get min/max of PROG from flamelet
  minC = minval(PROG)
  maxC = maxval(PROG)
  
  ! Test if prog max correctly estimated
  if (maxC.gt.CMean(nCMean)) then
     print*,"WARNING: Estimated max C too small! -> ", maxC
  end if
  
  ! Prepare the convolution
  allocate(pdf(nPoints))
  allocate(C  (nPoints))
  
  ! Create normalized progress variable
  C = (PROG-minC)/(maxC-minC)
  
  do icv=1,nCVar
     do icm=1,nCMean
        
        ! Create normalized progress variable
        cm = (CMean(icm)-minC)/(maxC-minC)
        cv = CVar(icv)/(maxC-minC)**2
        
        ! Create pdf
        call create_beta_pdf(cm,cv)
        
        ! Convolute
        do var=1,nvar_in
           meanVal  = 0.0_WP
           
           if (trim(input_name(var)).eq.'density') then
              do k=1,nPoints
                 meanVal = meanVal + pdf(k)/input_data(k,var)
              end do
              postconv(var,icm,icv,file)  = 1.0_WP / meanVal
           else
              do k=1,nPoints
                 meanVal = meanVal + pdf(k)*input_data(k,var)
              end do
              postconv(var,icm,icv,file)  = meanVal
           end if
        end do
     end do
  end do
  
  ! Finish the convolution
  deallocate(pdf)
  deallocate(C)
  nullify(pdf)
  nullify(C)
  
  return
end subroutine premixed_table_convolute


! ========================================================== !
! Convert the names from FlameMaster to user specified names !
! ========================================================== !
subroutine premixed_table_convert_names
  use premixed_table
  use parser
  implicit none
  
  character(len=str_medium), dimension(:), allocatable :: conversion
  character(len=str_medium) :: varname
  integer :: i,n,var

  ! Default name as in FlameMaster
  nvar_out = nvar_in
  allocate(output_name(nvar_out))
  output_name = input_name
  
  ! Get the number of name conversions
  call parser_getsize('Name conversion',n)
  if (mod(n,3).ne.0) stop "premixed_table_convert_names: Problem in the definition of conversion names"
  
  ! Allocate array and read
  allocate(conversion(n))
  call parser_read('Name conversion',conversion)
  
  ! Convert the names
  n = n / 3
  do i=1,n
     varname = trim(conversion((i-1)*3+3))
     loop1:do var=1,nvar_in
        if (trim(input_name(var)).eq.trim(varname)) exit loop1
     end do loop1
     if (var.eq.nvar_in+1) then
        print*, "table_convert_names: Unknown variable name : " // varname
        stop
     end if
     output_name(var) = trim(conversion((i-1)*3+1))
  end do
  
  return
end subroutine premixed_table_convert_names


! ============================================== !
! Setup the table by mapping the first direction !
! ============================================== !
subroutine premixed_table_setup
  use premixed_table
  use parser
  implicit none
  
  integer  :: icm,icv,izm,var
  integer  :: file, file_up, file_down
  real(WP) :: err, err_up, err_down
  real(WP) :: alpha_up,alpha_down
  
  real(WP), dimension(:), pointer :: tmp
  
  ! Allocate final table
  allocate(output_data(nCMean,nCVar,nZMean,nvar_out))
  allocate(mask(nCMean,nCVar,nZMean))
  mask=0
  
  ! Create mesh in mean mixture fraction
  call create_zmean
  
  ! Loop over the three mapping directions
  do icm=1,nCMean
     do icv=1,nCVar
        do izm=1,nZMean
           
           tmp => postconv(nvar_in-2,icm,icv,:)
           
           ! Find the two files right above and right below
           err_up   = +huge(1.0_WP)
           err_down = -huge(1.0_WP)
           
           file_up   = 0
           file_down = 0
           
           do file=1,nfiles
              err = tmp(file) - ZMean(izm)
              
              if ((err.ge.0.0_WP) .and. (err.le.err_up)) then
                 file_up = file
                 err_up = err
              end if
              if ((err.le.0.0_WP) .and. (err.ge.err_down)) then
                 file_down = file
                 err_down = err
              end if
           end do
           
           ! Interpolate
           if (file_up.eq.0 .or. file_down.eq.0) then
              if (file_up.eq.0) then
                 alpha_up   = 0.0_WP
                 alpha_down = 1.0_WP
                 file_up = 1
              end if
              if (file_down.eq.0) then
                 alpha_up   = 1.0_WP
                 alpha_down = 0.0_WP
                 file_down = 1
              end if
              ! Mask it
              mask(icm,icv,izm) = 1
           else
              if (file_up.eq.file_down) then
                 alpha_up   = 1.0_WP
                 alpha_down = 0.0_WP
              else
                 alpha_up   = (Zmean(izm)-tmp(file_down)) / (tmp(file_up)-tmp(file_down))
                 alpha_down = (tmp(file_up)-Zmean(izm))   / (tmp(file_up)-tmp(file_down))
              end if
           end if
           
           do var=1,nvar_out
              if (trim(input_name(var)).eq.'density') then
                 output_data(icm,icv,izm,var) =  1.0_WP/( &
                      alpha_up  /postconv(var,icm,icv,file_up) + &
                      alpha_down/postconv(var,icm,icv,file_down) )
              else
                 output_data(icm,icv,izm,var) =  &
                      alpha_up  *postconv(var,icm,icv,file_up) + &
                      alpha_down*postconv(var,icm,icv,file_down)
              end if
           end do
           
        end do
     end do
  end do
  
  return
end subroutine premixed_table_setup


! ============================================================= !
! Extent the value of the chemtable outside the physical bounds !
! ============================================================= !
subroutine premixed_table_quenching
  use premixed_table
  use parser
  implicit none
  
!!$  ! By default, just copy the values
!!$  output_data(:,:,1,       :) = output_data(:,:,3,       :)
!!$  output_data(:,:,2,       :) = output_data(:,:,3,       :)
!!$  output_data(:,:,nZMean-1,:) = output_data(:,:,nZMean-2,:)
!!$  output_data(:,:,nZMean,  :) = output_data(:,:,nZMean-2,:)
!!$  
!!$  ! Outside the flammability limits
!!$  ! -> Set sL to zero
!!$  output_data(:,:,1,       nvar_out-4) = 0.0_WP
!!$  output_data(:,:,2,       nvar_out-4) = 0.0_WP
!!$  output_data(:,:,nZMean-1,nvar_out-4) = 0.0_WP
!!$  output_data(:,:,nZMean,  nvar_out-4) = 0.0_WP
!!$  ! -> Set SRC_PROG to zero
!!$  output_data(:,:,1,       nvar_out-1) = 0.0_WP
!!$  output_data(:,:,2,       nvar_out-1) = 0.0_WP
!!$  output_data(:,:,nZMean-1,nvar_out-1) = 0.0_WP
!!$  output_data(:,:,nZMean,  nvar_out-1) = 0.0_WP
!!$  ! -> Set PROG to zero
!!$  output_data(:,:,1,       nvar_out) = 0.0_WP
!!$  output_data(:,:,2,       nvar_out) = 0.0_WP
!!$  output_data(:,:,nZMean-1,nvar_out) = 0.0_WP
!!$  output_data(:,:,nZMean,  nvar_out) = 0.0_WP
  
  return
end subroutine premixed_table_quenching


! ===================== !
! Print some statistics !
! ===================== !
subroutine premixed_table_stats
  use premixed_table
  implicit none
  
  integer :: var
  
  print*,''
  
  ! Min and Max of the coordinates
  print*, '** Coordinates of the table **'
  write(*,10) 'Variable    ','Min    ','Max    '
  write(*,10) '------------','------------','------------'
  write(*,11) 'CMEAN       ', minval(CMean), maxval(CMean)
  write(*,11) 'CVAR        ', minval(CVar),  maxval(CVar)
  write(*,11) 'ZMEAN       ', minval(ZMean), maxval(ZMean)
  print*,''
  
  ! Min and Max of all the mapped quantities
  print*, '** Mapped quantities **'
  write(*,10) 'Variable    ','Min    ','Max    '
  write(*,10) '------------','------------','------------'
  do var=1,nvar_out
     write(*,11) output_name(var),minval(output_data(:,:,:,var)),maxval(output_data(:,:,:,var))
  end do
  print*,''

10 format (A12,'  ',A12,'  ',A12)
11 format (A12,'  ',ES12.4,'  ',ES12.4)

  return
end subroutine premixed_table_stats


! ===================================== !
! Write the table back to a binary file !
! ===================================== !
subroutine premixed_table_write
  use premixed_table
  use parser
  implicit none
  
  integer :: ierr,var,iunit
  character(len=str_medium) :: filename
  character(len=str_medium), parameter :: combModel = 'FPVA'
  
  ! Open the data file
  call parser_read('Table filename', filename)
  call BINARY_FILE_OPEN(iunit,trim(filename),"w",ierr)
  
  ! Write sizes
  call BINARY_FILE_WRITE(iunit,nCMean,1,kind(nCMean),ierr)
  call BINARY_FILE_WRITE(iunit,nCVar, 1,kind(nCVar), ierr)
  call BINARY_FILE_WRITE(iunit,nZMean,1,kind(nZMean),ierr)
  call BINARY_FILE_WRITE(iunit,nvar_out,1,kind(nvar_out),ierr)
  ! Write the axis coordinates
  call BINARY_FILE_WRITE(iunit,CMean,nCMean,kind(CMean),ierr)
  call BINARY_FILE_WRITE(iunit,CVar, nCVar, kind(CVar), ierr)
  call BINARY_FILE_WRITE(iunit,ZMean,nZMean,kind(ZMean),ierr)
  ! Masks
  call BINARY_FILE_WRITE(iunit,mask,nCMean*nCVar*nZMean,kind(mask),ierr)
  ! Write additional stuff
  call BINARY_FILE_WRITE(iunit,combModel,str_medium,kind(combModel),ierr)
  ! Write variable names
  do var=1,nvar_out
     call BINARY_FILE_WRITE(iunit,output_name(var),str_medium,kind(output_name),ierr)
  end do
  ! Write data field
  do var=1,nvar_out
     call BINARY_FILE_WRITE(iunit,output_data(:,:,:,var),nCMean*nCVar*nZMean,kind(output_data),ierr)
  end do
  
  call BINARY_FILE_CLOSE(iunit,ierr)
  
  return
end subroutine premixed_table_write

