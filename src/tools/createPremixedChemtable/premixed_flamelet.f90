module premixed_flamelet
  use precision
  use string
  implicit none
  
  ! Number of points/species in the flamelet
  integer :: nPoints
  
  ! List of names/variables to get from FlameMaster files
  integer :: nvar_in
  character(len=str_medium), dimension(:), allocatable :: input_name
  real(WP), dimension(:,:), pointer :: input_data
  logical, dimension(:), allocatable :: found
  
  ! FPVA constituant variables
  integer :: nFPVA
  character(len=str_short), dimension(:), allocatable :: FPVA_name
  
  ! Particular arrays for steady flamelets / FPVA
  real(WP), dimension(:), pointer :: DIFF
  real(WP), dimension(:), pointer :: PROG
  real(WP), dimension(:), pointer :: SRC_PROG
  
  ! Additional temporary variables to read - Le<>1
  real(WP), dimension(:), pointer :: ZMIX
  
  real(WP) :: flame_thickness
  real(WP) :: burning_velocity
  real(WP) :: equivalence_ratio

  real(WP) :: Zst
  
end module premixed_flamelet


! ============================================== !
! FLAMELET INITIALIZATION                        !
! Convert the names to be those from FlameMaster !
! ============================================== !
subroutine premixed_flamelet_init
  use premixed_flamelet
  use parser
  implicit none
  
  ! Get number of additional variables to store in the table
  call parser_getsize("FlameMaster variables", nvar_in)

  ! Get the stoichiometric mixture fraction 
  call parser_read('Stoichiometric mixture fraction',Zst)
  
  ! Treat the case the model is FPVA
  allocate(input_name(nvar_in+6))
  call parser_read("FlameMaster variables", input_name(1:nvar_in))
  
  nvar_in = nvar_in + 6
  input_name(nvar_in-5) = 'RHOu'
  input_name(nvar_in-4) = 'lF'
  input_name(nvar_in-3) = 'SL'
  input_name(nvar_in-2) = 'ZMIX'
  input_name(nvar_in-1) = 'SRC_PROG'
  input_name(nvar_in)   = 'PROG'
  
  call parser_getsize('FPVA variables',nFPVA)
  allocate(FPVA_name(nFPVA))
  call parser_read('FPVA variables',FPVA_name)
  
  ! Allocate array to specify wether the variables have been found
  allocate(found(nvar_in))
  
  return
end subroutine premixed_flamelet_init



! ================================================ !
! Read a flamelet file and store its value in data !
! ================================================ !
subroutine premixed_flamelet_readfile(filename)
  use premixed_flamelet
  use fileio
  implicit none
  
  character(len=*), intent(in) :: filename
  integer :: iunit, ierr, var, n, nlines, index1
  character(len=str_long) :: buffer
  character(len=str_long) :: line
  character(len=str_medium) :: varname, tmpname
  real(WP), dimension(:), pointer :: tmp
    
  ! Open the file
  iunit = iopen()
  open(iunit,file=trim(filename),form='formatted',status='old',iostat=ierr)
  if (ierr.ne.0) then
     print*,"premixed_flamelet_readfile: Error opening the file : " // filename
     stop
  end if
  
  found = .false.
  ierr = 0
  buffer = ''
  do while(index(buffer,'body').eq.0)
     
     ! First get some parameters
     read(iunit,'(a)',iostat=ierr) buffer
     
     ! Get nPoints and allocate arrays
     if (index(buffer,'gridPoints').ne.0) then
        read(buffer(index(buffer,'=')+1:),*) nPoints
        nlines = ceiling(nPoints / 5.0_WP)
        
        allocate(input_data(nPoints,nvar_in))
        allocate(tmp(nPoints))
        
     ! Get laminar flame speed in m/s
     elseif (index(buffer,'burningVelocity').ne.0) then
        read(buffer(index(buffer,'=')+1:index(buffer,'=')+7),*) burning_velocity
        burning_velocity = burning_velocity / 100.0_WP
        
     ! Get flame sthickness
     elseif (index(buffer,'FlameThickness').ne.0) then
        read(buffer(index(buffer,'=')+1:index(buffer,'=')+11),*) flame_thickness
     
     ! Get equivalence ratio
     elseif (index(buffer,'fuel-air-equivalence-ratio').ne.0) then
        read(buffer(index(buffer,'=')+1:),*) equivalence_ratio
     end if

  end do

  ! Preset diffusivity to 1
  loop0:do var=1,nvar_in
     if (trim(input_name(var)).eq.'diffusivity') exit loop0
  end do loop0
  if (var.le.nvar_in) then
     DIFF => input_data(:,var)
     DIFF = 1.0_WP
  end if
  
  ! Set burning velocity and flame thickness
  input_data(:,nvar_in-4) = flame_thickness
  input_data(:,nvar_in-3) = burning_velocity
  found(nvar_in-4:nvar_in-3) = .true.
  
  ! Preset arrays
  SRC_PROG   => input_data(:,nvar_in-1); SRC_PROG   = 0.0_WP
  ZMIX       => input_data(:,nvar_in-2); ZMIX       = Zst
  PROG       => input_data(:,nvar_in);   PROG       = 0.0_WP
  
  loop1:do while (ierr .eq. 0)
     
     ! Read name of variable
     read(iunit,'(a)',iostat=ierr) buffer
     if (trim(buffer).eq.'trailer') exit loop1
     
     ! Read name of variable
     read(buffer,'(a)',iostat=ierr) varname
     index1 = index(varname,' ')
     if (index1.ne.0) varname(index1:) = ''
     
     ! Read the array
     line = ''
     do n=1,nlines
        read(iunit,'(a)',iostat=ierr) buffer
        line = trim(line) // adjustl(trim(buffer))
     end do
     
     ! Is it part of the diffusivity?
     if (trim(varname).eq.'lambda') then
        read(line,*) tmp
        loop2:do var=1,nvar_in
           if (trim(input_name(var)).eq.'diffusivity') exit loop2
        end do loop2
        if (var.le.nvar_in) then
           DIFF = DIFF * tmp
           found(var) = .true.
        end if
     end if
     if (trim(varname).eq.'cp') then
        read(line,*) tmp
        loop3:do var=1,nvar_in
           if (trim(input_name(var)).eq.'diffusivity') exit loop3
        end do loop3
        if (var.le.nvar_in) then
           DIFF = DIFF / tmp
           found(var) = .true.
        end if
     end if
     
     ! Is it part of the Mixture fraction?
     if (trim(varname).eq.'massfraction-H2') then
        read(line,*) tmp
        ZMIX = ZMIX + tmp*(1.0_WP-Zst)/1.0_WP
        found(nvar_in-2) = .true.
     end if
     if (trim(varname).eq.'massfraction-O2') then
        read(line,*) tmp
        ZMIX = ZMIX - tmp*Zst/0.232_WP
        found(nvar_in-2) = .true.
     end if
     
     ! Is it a variable for premixed FPVA?
     loop4:do var=1,nFPVA
        tmpname = 'massfraction-' // trim(FPVA_name(var))
        if (trim(varname).eq.trim(tmpname)) then
           read(line,*) tmp
           PROG = PROG + tmp
           found(nvar_in) = .true.
           exit loop4
        end if
        tmpname = 'ProdRate' // trim(FPVA_name(var))
        if (trim(varname).eq.trim(tmpname)) then
           read(line,*) tmp
           SRC_PROG = SRC_PROG + tmp
           found(nvar_in-1) = .true.
           exit loop4
        end if
     end do loop4
     
     ! Do we want that variable?
     loop5:do var=1,nvar_in
        if (trim(input_name(var)).eq.varname) then
           read(line,*) input_data(:,var)
           found(var) = .true.
           exit loop5
        end if
     end do loop5
     
  end do loop1
  
  ! For FPVA, divide source term by density (Reynolds average)
  loop7:do var=1,nvar_in
     if (trim(input_name(var)).eq.'density') exit loop7
  end do loop7
  if (var.le.nvar_in) then
     ! Compute source term for PROG not RHO*PROG
     SRC_PROG = SRC_PROG / input_data(:,var)
     ! Compute the unburned density
     input_data(:,nvar_in-5) = input_data(1,var)
     found(nvar_in-5) = .true.
  end if
  
  ! Do we have all the variables?
  do var=1,nvar_in
     if (.not.found(var)) then
        print*,"Variable ",trim(input_name(var))," not found in flamelet file"
        stop
     end if
  end do
  
  ! Deallocate
  deallocate(tmp); nullify(tmp)
  close(iclose(iunit))
  
  return
end subroutine premixed_flamelet_readfile


! ========================================= !
! Deallocate the data array for new reading !
! ========================================= !
subroutine premixed_flamelet_cleanup
  use premixed_flamelet
  implicit none
  
  deallocate(input_data)
  nullify(input_data)
  
  return
end subroutine premixed_flamelet_cleanup
