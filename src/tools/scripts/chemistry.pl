#!/usr/bin/perl -w
# Division by 0: BUGGGG

use Carp;
use PDL;
use PDL::Fit::Polynomial;
use PDL::Fit::Linfit;

# ======================================================================================= #
# Input arguments 
# ======================================================================================= #

$printTHERMOFM = 0;

my $input = $ARGV[0];
{
  open IN, "<$input" or die "Could not find input file $input\n";
  my @in = <IN>;
  @in = &trim(@in);
  my $found_transport = 0;
  my $found_mechanism = 0;
  my $found_thermo = 0;
  my $found_fit = 0;
  foreach (@in){
    if ($_=~/^(!|\#)/) {next}
    if ($_=~/Transport file([\s]+)?:([\s]+)?([^\s]+)/){
      $transfile = $3;
      $found_transport = 1;
    }
    if ($_=~/Mechanism file([\s]+)?:([\s]+)?([^\s]+)/){
      $mechfile = $3;
      $found_mechanism = 1;
    }
    if ($_=~/Thermo file([\s]+)?:([\s]+)?([^\s]+)/){
      $thermofile = $3;
      $found_thermo = 1;
    }
    if ($_=~/Fitted transport([\s]+)?:([\s]+)?([^\s]+)/){
      $use_fit = $3;
      $found_fit = 1;
    }
  } 
  if ($found_transport==0){die "\#Transport file\# not found in input\n"}
  if ($found_thermo==0){die "\#Thermo file\# not found in input\n"}
  if ($found_mechanism==0){die "\#Mechanism file\# not found in input\n"}
  if ($found_fit==0){$use_fit=0}
}


# ======================================================================================= #
# Parameters 
# ======================================================================================= #

# Ideal gas constant
$Rcst = 8.314;

# Atmospheric pressure [Pa]
$Patm = 1.0133e5;#1e5;#101300;

# Maths
$Pi = 3.14159265;
$calfact = 4.184; # Calorie-Joule conversion
$angstrom_fac = 1e-10;

# Avogadro number
$Av = 6.0221412927e+23; #[/mol]

# Boltzmann constant
$kb = 1.3806503e-23; #[J/K]=[kg.m2/s2.K]

# Additional species #
@species = qw/N2/;
@speciesCHECK = @species;

# Possible atoms
%atoms = ( "TI",0,"CL",1,"C",2,"H",3,"O",4,"N",5,"AR",6,"HE",7);
#@atom_name = qw/TI CL C H O N AR HE/;
@atom_mass = qw/0.0478671 0.0354532 0.01201 0.001008 0.016 0.01401 0.039948 0.004/;

# Open fortran mechanism file
open M, ">mechanism.f90" or die "Cannot open mechanism.f90 file: $!";


# ======================================================================================= #
# Processing of thermodata file 
# ======================================================================================= #

# -------------------------------------------------- #
# Arrays and hashes
# -------------------------------------------------- #
# %speciesTH : species names in thermo file 
#              keys: name of species as it appears in thermo file
#              value: index in thermo file
#
# %WTH : molar mass from thermo file
#        keys: name in thermofile
#        value: molar mass in kg/mol
#
# @Ahi,@alo : polynomial fit coefficients of heat capacity, enthalpy and entropy.
#             $Ahi/alo[index of species in thermofile][1..7]
#
# @compo : $compo[index of species in thermofile][1..natom] follows atom ordering defined in %atoms
#
# -------------------------------------------------- #


# Open thermo file
open TH, "<$thermofile" or die "Error opening thermo file $thermofile: $!";
@thermo = <TH>;
close(TH);

# Process header
while ($thermo[0]=~/^\s*!|^\s*\#/i){shift @thermo}
if ($thermo[0]!~/THERMO/){die "Check start of thermo file. Should read THERMO (comments allowed)"}
shift @thermo;
while ($thermo[0]=~/^\s*!|^\s*\#/i){shift @thermo}
if ($thermo[0]!~/[\d\.\s]+/){die "Check default T ranges (comments allowed)"}
{
  my $tmp = shift @thermo;
  my @tmp = split /\s+/, &trim($tmp);
  @tmp = &trim(@tmp);
  #$TminD = $tmp[0];
  $TmedD = $tmp[1];
  #$TmaxD = $tmp[2];
}

# Process thermo data
my $nspecTH = 0;
$ith = 0;
while ($ith<=$#thermo){

  # Check if end of file
  if ($thermo[$ith]=~/END/i){last}

  # Valid first line for species should have "1" at position 80
  if (length($thermo[$ith])<80){next}
  my $check = substr($thermo[$ith], 79, 1);
  if ($check!=1){$ith++;next}
  
  # Convert to all uppercase
  $thermo[$ith] =~ tr/a-z/A-Z/;
  
  # Store first line
  my $l1 = $thermo[$ith];

  # Find next 3 lines: should have 2,3, and 4 at position 80
  my ($l2,$l3,$l4);
  my $done = 0;
  while ($done!=1){
    $ith++;
    if (length($thermo[$ith])<80){next}
    my $check = substr($thermo[$ith], 79, 1);
    if ($check==2){$l2 = $thermo[$ith]}
    if ($check==3){$l3 = $thermo[$ith]}
    if ($check==4){$l4 = $thermo[$ith];$done=1}
    if ($check==1){die "Problem finding all lines corresponding to:\n  $l1\n"}
  }
  $ith++;
 
  # Species name
  my @il1 = split /\s+/,$l1;
  @il1 = &trim(@il1);
  $speciesTH{$il1[0]} = $nspecTH;
  
  # Composition
  my (@at,@nt);
  $at[0] = substr($l1,24,2);
  $nt[0] = substr($l1,26,3);
  $at[1] = substr($l1,29,2);
  $nt[1] = substr($l1,31,3);
  $at[2] = substr($l1,34,2);
  $nt[2] = substr($l1,36,3);
  $at[3] = substr($l1,39,2);
  $nt[3] = substr($l1,41,3);
  @at = &trim(@at);
  @nt = &trim(@nt);
  my @composition;
  for (my $j=0;$j<=$#atom_mass;$j++){$composition[$j] = 0.0}
  for (my $j=0;$j<=3;$j++){
    if ($at[$j]!~/[A-Z]/){next}
    if (!exists $atoms{$at[$j]}){print "Warning: atom $at[$j] for species $il1[0] is unknown\n"}
    else{$composition[$atoms{$at[$j]}] = $nt[$j]}
  }
  
  # Specific temperature ranges
  my $Tstring = substr($l1,45,30);
  my @Tr = split /\s+/,&trim($Tstring);
  $Tmed{$il1[0]} = $Tr[2]; # Only Tmed is used
  
  # Molar mass
  $WTH{$il1[0]} = 0.0;
  for (my $j=0;$j<=$#atom_mass;$j++){
    $WTH{$il1[0]}+=$atom_mass[$j]*$composition[$j];
  }
  
  # Thermo coefficients
  my $index = $nspecTH;
  $Ahi[$index][1]=sprintf("%.8e",substr($l2,0,15));
  $Ahi[$index][2]=sprintf("%.8e",substr($l2,15,15));
  $Ahi[$index][3]=sprintf("%.8e",substr($l2,30,15));
  $Ahi[$index][4]=sprintf("%.8e",substr($l2,45,15));
  $Ahi[$index][5]=sprintf("%.8e",substr($l2,60,15));
  $Ahi[$index][6]=sprintf("%.8e",substr($l3,0,15));
  $Ahi[$index][7]=sprintf("%.8e",substr($l3,15,15));
  $alo[$index][1]=sprintf("%.8e",substr($l3,30,15));
  $alo[$index][2]=sprintf("%.8e",substr($l3,45,15));
  $alo[$index][3]=sprintf("%.8e",substr($l3,60,15));
  $alo[$index][4]=sprintf("%.8e",substr($l4,0,15));
  $alo[$index][5]=sprintf("%.8e",substr($l4,15,15));
  $alo[$index][6]=sprintf("%.8e",substr($l4,30,15));
  $alo[$index][7]=sprintf("%.8e",substr($l4,45,15));
  # Store composition
  #@{$compo[$index]} = @composition;
  
  # Increment species counter
  $nspecTH++;  
}
print "Done with thermo file\n";

# ======================================================================================= #
# Processing of transport property file 
# ======================================================================================= #

# -------------------------------------------------- #
# Arrays and hashes
# -------------------------------------------------- #
#
# %KoEps : Boltzmann constant divided by Lennard-Jones potential well depth 
#          UNIT: [1/K]
#          keys: species name (should match thermodata file names
#          value: k/eps
#
# %sigma : Lennard-Jones diameter
#          UNIT: [A]
#          keys: species name (should match thermodata file names
#          value: sigma
#
# -------------------------------------------------- #


# Assumes ALL species have some kind of data in the transport file
open TR, "<$transfile" or die "Error opening mechanism file $transfile: $!";
@trans = <TR>;
@trans = &trim(@trans);
close(TR);

for (my$i=0;$i<=$#trans;$i++){
  # Skip comments
  if ($trans[$i]=~/^\s*!|^\s*\#/i){next}
  # Skip blank lines
  if ($trans[$i]!~/[\d\D]+/){next}
  # Get coefficients
  @list = split /\s+/, $trans[$i];
  @list = &trim(@list);
  $linearity{$list[0]} = $list[1];
  $KoEps{$list[0]} = 1/$list[2];
  $sigma{$list[0]} = $angstrom_fac*$list[3];
  $Zrot{$list[0]} = $list[6];
}


# ======================================================================================= #
# Processing of mechanism file 
# ======================================================================================= #

# -------------------------------------------------- #
# Notes on Chemkin format
# -------------------------------------------------- #
# 
# +M   : Third body is added to k, no pressure dependent coefficient
# (+M) : Pressure dependent coefficients (2 sets of A,n,E). 
#      => TROE line: 6 coeffs, fca,fcta,fcb,fctb,fcc,fctc
#      => no TROE line: fcc = 1, fctc and others = 0
#
# -------------------------------------------------- #

# Open mechanism file and load data
open ME, "<$mechfile" or die "Error opening mechanism file $mechfile: $!";
@mech = <ME>;
@mech = &trim(@mech);
close(ME);

# ------------------ Species list ------------------ #
{
  # Read list from mechanism file
  my $in = 0;
  while ($mech[$in]!~/SPECIES/i){$in++}
  $in++;
  while ($mech[$in]!~/END/i){
    my @tmp = split /\s+/, $mech[$in];
    @tmp = &trim(@tmp);
    push @species, @tmp;
    $in++;
  }
  # Get number of species
  $nspec = $#species;
  # Check that there exists thermo and trans data for each species in mechanism
  foreach (@species){
    if (!exists $linearity{$_}){print "Species $_ not in transport properties\n";die;}
    if (!exists $speciesTH{$_}){print "Species $_ not in thermo properties\n";die;}
  }
}

# ------------------ Reaction list ------------------ #
{
  # Find start of reaction list
  while ($mech[0]!~/REACTIONS/i){shift @mech}
  shift @mech;
  my $ilabel = 0;
  $nreac = -1;
  my $iM = 0;
  my $lastrf;
 
  # Go through reactions 
  for (my $in=0;$in<=$#mech;$in++) {
    my $m = &trim($mech[$in]);
    
    # Skip comment and blank lines
    if ($m!~/[^\s]/ || $m=~/^[\s]?(\#|!)/) {next}
    
    # --- Identify line type (reaction, info, reverse, troe etc. --- #
    if ($m=~/END/i) {
      # ---------------------- Last line of the file ---------------------- #
      last;

    }elsif ($m=~/DUPLICATE/i) {
      # -------- Duplicate reaction: nothing to do -------- #
      next;

    }elsif ($m=~/REV/i) {
      # ---------------------- Reversed rates ---------------------- #
      # Increment number of reactions
      $nreac++;
      # Don't increment label
      $reaction[$nreac][0] = $ilabel."b"; 

      # Save rates, 
      if ($m=~/REV\s*\/\s*([^\s]+)\s+([^\s]+)\s+([^\s]+)\s*\//){
	#my @tmp = split /\s+/,$m;
	#@tmp = &trim(@tmp);
	#print "@tmp\n";
	#$reaction[$nreac][3] = sprintf("%.8e",$tmp[1] * (1e-06)**(&mysum(@{$reaction[$lastrf][17]})-1));
	#$reaction[$nreac][4] = $tmp[2];
	#$reaction[$nreac][5] = sprintf("%.3e",$tmp[3] * $calfact);
	$reaction[$nreac][3] = sprintf("%.8e",$1 * (1e-06)**(&mysum(@{$reaction[$lastrf][17]})-1));
	$reaction[$nreac][4] = $2;
	$reaction[$nreac][5] = sprintf("%.3e",$3 * $calfact);
      }

      # Update pressure dependency (set to 0, unless it has been set to 1 already)
      if (!defined $reaction[$nreac][6]){ $reaction[$nreac][6] = 0}
      
      # Update reaction expression
      $reaction[$nreac][18] = "REV - ".$reaction[$lastrf][18];
      @{$reaction[$nreac][1]} = @{$reaction[$lastrf][2]};
      @{$reaction[$nreac][2]} = @{$reaction[$lastrf][1]};
      @{$reaction[$nreac][16]} = @{$reaction[$lastrf][17]};
      @{$reaction[$nreac][17]} = @{$reaction[$lastrf][16]};

      # Explicit backward rates are not allowed for TROE reactions.
	
    }elsif ($m=~/LOW/i) {
      # -------- Pressure dependent rate coefficients - simple form -------- # 
      # Applies to last forward reaction detected
      
      # Move current main coefficient to pressure section
      $reaction[$lastrf][6] = 1;
      $reaction[$lastrf][7] = sprintf("%.8e",$reaction[$lastrf][3] * (1e+06));
      $reaction[$lastrf][8] = $reaction[$lastrf][4];
      $reaction[$lastrf][9] = $reaction[$lastrf][5];
      
      # Extract and store coefficients
      if ($m=~/LOW\s*\/\s*([^\s]+)\s+([^\s]+)\s+([^\s]+)\s*\//){
	$reaction[$lastrf][3] = sprintf("%.8e",$1 * (1e-06)**(&mysum(@{$reaction[$lastrf][16]})-1));
	$reaction[$lastrf][4] = $2;
	$reaction[$lastrf][5] = sprintf("%.3e",$3 * $calfact);
      }else{ die "Could not understand LOW coefficients for reaction $reaction[$lastrf][18]\n"}
      
    
    }elsif ($m=~/TROE/i) {
      # -------- Pressure dependent rate coefficients - troe form --------#
      # Applies to last reaction detected

      # Extract and store coefficients
      if ($m=~/TROE\s*\/\s*([^\s]+)\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)\s*\//){
	$reaction[$lastrf][10] = 1-$1; # fca
	$reaction[$lastrf][11] = $2;   # fcta
	$reaction[$lastrf][12] = $1;   # fcb
	$reaction[$lastrf][13] = $3;   # fctb
	$reaction[$lastrf][14] = 1;    # fcc
	$reaction[$lastrf][15] = $4;   # fctc
      }else{ die "Could not understand TROE coefficients for reaction $reaction[$lastrf][18]\n"}


    }elsif ($m=~/\//i) {
      # ---------------------- Third-body coefficients ---------------------- #
      
      # Decompose line into species and coeff elements
      my @tmp = split /\//,$m;
      @tmp = &trim(@tmp);
      
      # Store coefficients 
      for (my $i=0;$i<$#tmp;$i=$i+2) {
	# Check that species exists first
	if (&is_in_list($tmp[$i],\@species)!=1) {die "Unknown species \#$tmp[0]\# on third body line \#$m\#\n"}
	push @{$M{"M".$iM}[0]}, $tmp[$i];	 # name of species
	push @{$M{"M".$iM}[1]}, $tmp[$i+1];	 # coefficient
      }


    }elsif ($m=~/=/i) {
      # ---------------------- Reaction line ---------------------- #
      my (@reactants,@products,@reaccoeff,@prodcoeff);
      my @tmp = split /\s+/,$m;
      @tmp = &trim(@tmp);
      $nreac++;

      # Increment label and reaction numbers
      $ilabel++;
      $reaction[$nreac][0] = $ilabel."f"; # Assume reversible reaction

      # Reactants and products
      my $r = $tmp[0];
      # Record potential irreversibility
      if ( $r=~/>/ && $r!~/</){$reaction[$nreac][0]=~s/f$//}
      
      # Treat reactants and products separately
      if ($r=~/([^\s]+)(<?=>?)([^\s]+)/){
	my $part1 = $1; # reactants
	my $part2 = $3; # products
	$part1=~s/<|>//;
	$part2=~s/<|>//;
	@reactants = split /\+/, $part1;
	@products = split /\+/, $part2;
	# Handle case of (+M)
	foreach (@reactants,@products) {s/\($//}

	# Separate stoichiometric coefficients, species names, and thirdbody
	# Reactants
	for (my $i=0;$i<=$#reactants;$i++){
	  # There is a thirdbody
	  if ($reactants[$i]=~/^M/){
	    # Increment counter of thirdbodies
	    $iM++;
	    # Initialize it to 1.0 [OTHER] for now
	    push @{$M{"M".$iM}[0]}, "OTHER";	 # name of species
	    push @{$M{"M".$iM}[1]}, 1.0;	 # coefficient
	    # Change its name to correct index
	    $reactants[$i] = "M".$iM;
	    $reaccoeff[$i] = 1.0;
	  }

	  # Directly corresponds to species: stoic coeff is unity
	  elsif (&is_in_list($reactants[$i],\@species)){$reaccoeff[$i]=1.0}

	  # Not in species list: should start with a number (stoic coeff)
	  elsif ($reactants[$i]=~/^([\d]+)/){
	    my $coeff = $1;
	    $reactants[$i] =~ s/^$coeff//;
	    $reaccoeff[$i] = $coeff;
	    print "Assuming stoichiometric coefficient of $reaccoeff[$i] for species $reactants[$i] in reaction $r\n";
	    if (&is_in_list($reactants[$i],\@species)!=1){die "Unknown species $reactants[$i] in reaction $r\n"}
	  }
	  
	  # default
	  else {
	    die "Missing something in reactants of reaction $r!\n";
	  }
	}
	# Products
	for (my $i=0;$i<=$#products;$i++){
	  
	  # There is a thirdbody
	  if ($products[$i]=~/^M/){
	    # Change its name to correct index
	    $products[$i] = "M".$iM;
	    $prodcoeff[$i] = 1.0;
	  }

	  # Directly corresponds to species: stoic coeff is unity
	  elsif (&is_in_list($products[$i],\@species)){$prodcoeff[$i]=1.0}

	  # Not in species list: should start with a number (stoic coeff)
	  elsif ($products[$i]=~/^([\d]+)/){
	    my $coeff = $1;
	    $products[$i] =~ s/^$coeff//;
	    $prodcoeff[$i] = $coeff;
	    print "Assuming stoichiometric coefficient of $prodcoeff[$i] for species $products[$i] in reaction $r\n";
	    if (&is_in_list($products[$i],\@species)!=1){die "Unknown species $products[$i] in reaction $r\n"}
	  }

	  # default
	  else {die "Missing something in products of reaction $r!\n"}
	}
      }
      
      # Consolidate reactants and products lists (H+H=2H)
      {
	my %tmp;
	for (my $i=0;$i<=$#reactants;$i++) {$tmp{$reactants[$i]}+=$reaccoeff[$i]}
	@reactants = keys %tmp;
	@reaccoeff = ();
	for (my $i=0;$i<=$#reactants;$i++) {$reaccoeff[$i] = $tmp{$reactants[$i]}}
      }{
	my %tmp;
	for (my $i=0;$i<=$#products;$i++) {$tmp{$products[$i]}+=$prodcoeff[$i]}
	@products = keys %tmp;
	@prodcoeff = ();
	for (my $i=0;$i<=$#products;$i++) {$prodcoeff[$i] = $tmp{$products[$i]}}
      }

      # Store species data for reaction
      @{$reaction[$nreac][1]} = @reactants;
      @{$reaction[$nreac][2]} = @products;      
      @{$reaction[$nreac][16]} = @reaccoeff;
      @{$reaction[$nreac][17]} = @prodcoeff;
      $reaction[$nreac][18] = $r;

      # Rate coefficients
      $reaction[$nreac][3] = sprintf("%.8e",$tmp[1] * (1e-06)**(&mysum(@reaccoeff)-1));
      $reaction[$nreac][4] = $tmp[2];
      $reaction[$nreac][5] = sprintf("%.4e",$tmp[3] * $calfact);

      # Initialize remaining of arrays
      # Pressure dependent coefficients
      if ($r=~/\(\+M\)/){
	$reaction[$nreac][6] = 1;
	$reaction[$nreac][7] = 0;
	$reaction[$nreac][8] = 0;
	$reaction[$nreac][9] = 0;
	$reaction[$nreac][10] = 0;
	$reaction[$nreac][11] = 0;
	$reaction[$nreac][12] = 0;
	$reaction[$nreac][13] = 0;
	$reaction[$nreac][14] = 1.0;
	$reaction[$nreac][15] = 0;
      }else{
	$reaction[$nreac][6] = 0;
	$reaction[$nreac][7] = 0;
	$reaction[$nreac][8] = 0;
	$reaction[$nreac][9] = 0;
	$reaction[$nreac][10] = 0;
	$reaction[$nreac][11] = 0;
	$reaction[$nreac][12] = 0;
	$reaction[$nreac][13] = 0;
	$reaction[$nreac][14] = 0;
	$reaction[$nreac][15] = 0;
      }
      	
      # Set last forward reaction index
      $lastrf = $nreac;

      # Update list of used species
      push @speciesCHECK,@reactants,@products;
      
    }else{
      die "line \#$m\# is unclassified\n";
    }
  }
}

# Remove unused species from species list
# Unique species
@speciesCHECK = &rm_duplicates(\@speciesCHECK);
@speciesCHECK = &rm_M(\@speciesCHECK);
@species = @speciesCHECK;
$nspec = $#species;

# Link name/index
for (my$i=0;$i<=$#species;$i++) {
  $link{$species[$i]} = $i;
}

# Remove dashes for fortran code
@speciesX = @species;
# Remove unconventional symbols
@speciesX = &repsymb(@speciesX);
# Modify species names starting with number
@speciesX = &firstchar(@speciesX);

# $nreac++;
# $reaction[$nreac][0] = $label;
# @{$reaction[$nreac][1]} = @reactants;
# @{$reaction[$nreac][2]} = @products;
# $reaction[$nreac][3] = sprintf("%.8e",$a * (1e-06)**(&mysum(@reaccoeff)-1));
# $reaction[$nreac][4] = $n;
# $reaction[$nreac][5] = sprintf("%.3e",$E * (1e+03));
# $reaction[$nreac][6] = $pressure;
# $reaction[$nreac][7] = sprintf("%.8e",$ai * (1e-06)**(&mysum(@reaccoeff)-2));
# $reaction[$nreac][8] = $ni;
# $reaction[$nreac][9] = sprintf("%.3e",$Ei * (1e+03));
# $reaction[$nreac][10] = $fca;
# $reaction[$nreac][11] = $fcta;
# $reaction[$nreac][12] = $fcb;
# $reaction[$nreac][13] = $fctb;
# $reaction[$nreac][14] = $fcc;
# $reaction[$nreac][15] = $fctc;
# @{$reaction[$nreac][16]} = @reaccoeff;
# @{$reaction[$nreac][17]} = @prodcoeff;
# $reaction[$nreac][18] = $reac;

# ======================================================================================= #
# Update or create backward reactions if not already present 
# ======================================================================================= #

# ---------------------------------------------------------------------------- #
# Make sure no TROE reaction has reverse rates explicitely defined
# ---------------------------------------------------------------------------- #
for (my $i=0;$i<=$nreac;$i++) {
  if ($reaction[$i][0]=~/b/) {
    (my $tmplab=$reaction[$i][0]) =~ s/b/f/;
    my $nback = $i;
    my $nforw = -1;
    for (my $j=0;$j<=$nreac;$j++) {
      if ($reaction[$j][0] eq $tmplab) {
	$nforw = $j;
      }
    }
    if ($nforw < 0){die "Could not find forward reaction for reaction \#$reaction[$i][18]\#"}
    if ($reaction[$nforw][6]==1) {
      die "Reaction \#$reaction[$nforw][18]\# has both TROE and explicit reverse rates: not handled here";
    }  
  }
}  

# ---------------------------------------------------------------------------- #
# Go through reaction list and identify reactions needing backward reactions. 
# Backward rates are fitted using a 3-parameter Arrhenius format
# ---------------------------------------------------------------------------- #

for (my $i=0;$i<=$nreac;$i++) {
  if ($reaction[$i][0]=~/f/) {
    (my $tmplab=$reaction[$i][0]) =~ s/f/b/;
    my $nforw = $i;
    my $foundbackw = 0;
    for (my $j=0;$j<=$nreac;$j++) {
      if ($reaction[$j][0] eq $tmplab) {
	$foundbackw = 1;
      }
    }
    if ($foundbackw == 0) {
      $nreac++;
      
      # Label
      $reaction[$nreac][0] = $tmplab;
      
      # Reactants and products
      @{$reaction[$nreac][1]} = @{$reaction[$nforw][2]};
      @{$reaction[$nreac][2]} = @{$reaction[$nforw][1]};
      
      # Stoic. coefficients
      @{$reaction[$nreac][16]} = @{$reaction[$nforw][17]};
      @{$reaction[$nreac][17]} = @{$reaction[$nforw][16]};
      
      # Pressure dependent
      $reaction[$nreac][6] = $reaction[$nforw][6];
      
      # Lindemann and Troe coefficients
      $reaction[$nreac][10] = $reaction[$nforw][10];
      $reaction[$nreac][11] = $reaction[$nforw][11];
      $reaction[$nreac][12] = $reaction[$nforw][12];
      $reaction[$nreac][13] = $reaction[$nforw][13];
      $reaction[$nreac][14] = $reaction[$nforw][14];
      $reaction[$nreac][15] = $reaction[$nforw][15];

      # Expression
      $reaction[$nreac][18] = "REV - ".$reaction[$nforw][18];
      
      # Temperature array (equidistant in 1/T)
      my @T;
      my $Tend = 2000;
      my $Tstart = 500;
      my $npoints = 10;
      my $dT = ( 1.0 / $Tend - 1.0 / $Tstart ) / ( $npoints - 1.0 );
      for (my $ii=0;$ii<$npoints;$ii++) {
	$T[$ii] = 1.0 / ( 1.0 / $Tstart + $dT * $ii);
      }
      
      # Functions
      my (@f1,@f2,@f3,$f3,$f1,$f2,$funcs,$data,$yfit,$coeffs);
      for (my $ii=0;$ii<$npoints;$ii++) {
	$f1[$ii] = 1;              # constant function
	$f2[$ii] = log(1/$T[$ii]); # log function
	$f3[$ii] = 1/$T[$ii];      # 1/x for activation energy
      }
      $f1 = pdl(@f1);
      $f2 = pdl(@f2);
      $f3 = pdl(@f3);
      $funcs = cat $f1,$f2,$f3;
      
      # Data to fit from equilibrium constant - a,n,E
      for (my $ii=0;$ii<$npoints;$ii++) {
	
	# Compute ln k_b - n * ln( T ) = ln ( k_f / K_C ) - nf * ln( T ) */
	my $p0 = 1.0133e5;
	my $sumNu = 0.0;
	my $sumNuMu = 0.0;
	my $lnKC;
	my $RT = $Rcst * $T[$ii];
	my $lnT = log( $T[$ii] );
	my $lnKfMinNLnT;
	
	# Compute lnKf 
	$lnKf = log( $reaction[$nforw][3] ); 
	$lnKf += $reaction[$nforw][4] * $lnT; 
	$lnKf -= $reaction[$nforw][5] / $RT;
	
	# Compute lnKc
	# Reactants
	for (my $k=0;$k<=$#{$reaction[$nforw][1]};$k++) {
	  if ($reaction[$nforw][1][$k]=~/M/) {
	    next;
	  }
	  $sumNu -= $reaction[$nforw][16][$k];
	  my $is = $speciesTH{$reaction[$nforw][1][$k]};
	  if ($T[$ii]>=$Tmed{$reaction[$nforw][1][$k]}) {
	    $sumNuMu -= $reaction[$nforw][16][$k]*&free_enthalpy($T[$ii],\@{$Ahi[$is]});
	  } else {
	    $sumNuMu -= $reaction[$nforw][16][$k]*&free_enthalpy($T[$ii],\@{$alo[$is]});
	  }
	}
	# Products
	for (my $k=0;$k<=$#{$reaction[$nforw][2]};$k++) {
	  if ($reaction[$nforw][2][$k]=~/M/) {
	    next;
	  }
	  $sumNu += $reaction[$nforw][17][$k];
	  my $is = $speciesTH{$reaction[$nforw][2][$k]};   
	  if ($T[$ii]>=$Tmed{$reaction[$nforw][2][$k]}) {
	    $sumNuMu += $reaction[$nforw][17][$k]*&free_enthalpy($T[$ii],\@{$Ahi[$is]});
	  } else {
	    $sumNuMu += $reaction[$nforw][17][$k]*&free_enthalpy($T[$ii],\@{$alo[$is]});
	  }
	}
	
	$lnKC = -$sumNu * log( $RT / $p0 ) - $sumNuMu / $RT;
	$rates[$ii] = $lnKf - $lnKC;
      }
      
      $data = pdl(@rates);
      
      # Fit data
      ($yfit, $coeffs)  = linfit1d($data, $funcs);
      $reaction[$nreac][3] = sprintf("%.8e",exp(at($coeffs,0)));   
      $reaction[$nreac][4] = sprintf("%.4f",-at($coeffs,1));
      $reaction[$nreac][5] = sprintf("%.4e",-(at($coeffs,2)*$Rcst));
      
      #$tmp1 = sprintf("%.8e",$reaction[$nreac][3]/(1e-06)**(&mysum(@{$reaction[$nreac][16]})-1));  
      #$tmp2 = sprintf("%.1f",$reaction[$nreac][5]*1e-3);
      #print "$reaction[$nreac][0]: $tmp1, $reaction[$nreac][4], $tmp2\n";
      
      
      # Data to fit from equilibrium constant - ai,ni,Ei if present
      if ($reaction[$nreac][6]==1) {
	for (my $ii=0;$ii<$npoints;$ii++) {
	  
	  # Compute ln k_b - n * ln( T ) = ln ( k_f / K_C ) - nf * ln( T ) */
	  my $p0 = 1.0133e5;
	  my $sumNu = 0.0;
	  my $sumNuMu = 0.0;
	  my $lnKC;
	  my $RT = $Rcst * $T[$ii];
	  my $lnT = log( $T[$ii] );
	  my $lnKfMinNLnT;
	  
	  # Compute lnKf 
	  $lnKf = log( $reaction[$nforw][7] ); 
	  $lnKf += $reaction[$nforw][8] * $lnT; 
	  $lnKf -= $reaction[$nforw][9] / $RT;
	  
	  # Compute lnKc
	  # Reactants
	  for (my $k=0;$k<=$#{$reaction[$nforw][1]};$k++) {
	    if ($reaction[$nforw][1][$k]=~/M/) {
	      next;
	    }
	    $sumNu -= $reaction[$nforw][16][$k];
	    my $is = $speciesTH{$reaction[$nforw][1][$k]};
	    if ($T[$ii]>=$Tmed{$reaction[$nforw][1][$k]}) {
	      $sumNuMu -= $reaction[$nforw][16][$k]*&free_enthalpy($T[$ii],\@{$Ahi[$is]});
	    } else {
	      $sumNuMu -= $reaction[$nforw][16][$k]*&free_enthalpy($T[$ii],\@{$alo[$is]});
	    }
	  }
	  # Products
	  for (my $k=0;$k<=$#{$reaction[$nforw][2]};$k++) {
	    if ($reaction[$nforw][2][$k]=~/M/) {
	      next;
	    }
	    $sumNu += $reaction[$nforw][17][$k];
	    my $is = $speciesTH{$reaction[$nforw][2][$k]};
	    if ($T[$ii]>=$Tmed{$reaction[$nforw][2][$k]}) {
	      $sumNuMu += $reaction[$nforw][17][$k]*&free_enthalpy($T[$ii],\@{$Ahi[$is]});
	    } else {
	      $sumNuMu += $reaction[$nforw][17][$k]*&free_enthalpy($T[$ii],\@{$alo[$is]});
	    }
	  }
	  
	  $lnKC = -$sumNu * log( $RT / $p0 ) - $sumNuMu / $RT;
	  $rates[$ii] = $lnKf - $lnKC;
	}
	
	$data = pdl(@rates);
	
	# Fit data
	($yfit, $coeffs)  = linfit1d($data, $funcs);
	$reaction[$nreac][7] = sprintf("%.8e",exp(at($coeffs,0)));   
	$reaction[$nreac][8] = sprintf("%.2f",-at($coeffs,1));
	$reaction[$nreac][9] = sprintf("%.4e",-(at($coeffs,2)*$Rcst));
	
      }		
    }
  }
}


# ---------------------------------------------------------------------------- #
# Link backward and forward reactions #
# ---------------------------------------------------------------------------- #

# For each backward reaction, get index of corresponding forward reaction
for (my $i=0;$i<=$nreac;$i++) {
  $reaction[$i][19] = 0;
  if ($reaction[$i][0]=~/b/) {
    $reaction[$i][19] = 1;
    (my $tmplab=$reaction[$i][0]) =~ s/b/f/;
    my $foundf = 0;

    # Look for forward reaction
  inner: for (my $j=0;$j<=$nreac;$j++){
      if ($reaction[$j][0] eq $tmplab) {
	$foundf = 1;
	$reaction[$i][20] = $j+1;
	last inner;
      }
    }
    # Return if corresponding forward reaction was not found
    if ($foundf==0) {
      die("Could not find corresponding forward for reaction: $reaction[$i][0]");
    }
  }
}


# ======================================================================================= #
# Create subroutines for fortran code 
# ======================================================================================= #

# ------------------------------------------------ #
# Rate coefficients declaration
# ------------------------------------------------ #
for (my$i=0;$i<=$nreac;$i++) {
  $kdef[$i] = "";
  # if not a standard reaction
  if ($reaction[$i][6]==1) {
    $kdef[$i] .= "  real(WP) :: k".$reaction[$i][0]."_0, k".$reaction[$i][0]."_inf";
    $kdef[$i] .= ", FC".$reaction[$i][0];
    $kdef[$i] .= "\n";
  }
}

# ------------------------------------------------ #
# Rate coefficients
# ------------------------------------------------ #
for (my$i=0;$i<=$nreac;$i++) {
  if ($reaction[$i][6]==0) {
    $kline[$i] = "  k(r".$reaction[$i][0].") = ($reaction[$i][3]_WP)";
    if ($reaction[$i][4]==0 && $reaction[$i][5] != 0) { # No temperature exponent, but some activation energy
      $kline[$i] .= "*exp(-($reaction[$i][5]_WP)/RT)"; 
    }elsif ($reaction[$i][4]!=0 && $reaction[$i][5] == 0) { # Some temperature exponent, but no activation energy
      $kline[$i] .= "*Tloc**($reaction[$i][4]_WP)";
    }elsif ($reaction[$i][4]!=0 && $reaction[$i][5] != 0) { # both temperature exponent and activation energy
      $kline[$i] .= "*exp(-($reaction[$i][5]_WP)/RT+($reaction[$i][4]_WP)*lnT)";
    }
    #$kline[$i] = "  k(r".$reaction[$i][0].") = ($reaction[$i][3]_WP)*Tloc**($reaction[$i][4]_WP)*exp(-($reaction[$i][5]_WP)/(8.314_WP*Tloc))";
  } else {
    $kline[$i] = "";
    # K0
    $kline[$i] .= "  k".$reaction[$i][0]."_0 = ($reaction[$i][3]_WP)*exp(-($reaction[$i][5]_WP)/RT+($reaction[$i][4]_WP)*lnT)";
    # K inf
    $kline[$i] .= "\n  k".$reaction[$i][0]."_inf = ($reaction[$i][7]_WP)*exp(-($reaction[$i][9]_WP)/RT+($reaction[$i][8]_WP)*lnT)";
    # FC
    $kline[$i] .= "\n  FC".$reaction[$i][0]." = ";
    if ($reaction[$i][11]!=0.0) {
      $kline[$i] .= "($reaction[$i][10]_WP)*exp(-Tloc/($reaction[$i][11]_WP))";
    }
    if ($reaction[$i][13]!=0.0) {
      $kline[$i] .= " + ($reaction[$i][12]_WP)*exp(-Tloc/($reaction[$i][13]_WP))";
    }
    $kline[$i] .= " + ($reaction[$i][14]_WP)*exp(-($reaction[$i][15]_WP)/Tloc)";
    # K
    # Find correct M
    my $mloc = "";
    foreach (@{$reaction[$i][1]}) {
      if (exists $M{$_}) {
	$mloc = $_;
      }
    }
    if ($mloc!~/[^\s]/) {
      die "Could not find M species for reaction $reaction[$i][0]";
    }
    $kline[$i] .= "\n  k(r".$reaction[$i][0].") = getlindratecoeff(Tloc, pressure, k".$reaction[$i][0]."_0, k".$reaction[$i][0]."_inf, FC".$reaction[$i][0].", M(m$mloc) )";
  }
  $kline[$i] .= "\n";
}

# ------------------------------------------------ #
# Create reaction rates
# ------------------------------------------------ #
for (my$i=0;$i<=$nreac;$i++) {
  $w[$i] = "  w(r".$reaction[$i][0].") = k(r".$reaction[$i][0].") ";
  for (my$j=0;$j<=$#{$reaction[$i][1]};$j++) {
    if (exists $M{$reaction[$i][1][$j]}) {
      if ($reaction[$i][6]==0) {
	$w[$i] .= " * m(m".&repsymb($reaction[$i][1][$j]).")";
      }
    } else {
      $w[$i] .= " * c(g".&repsymb($reaction[$i][1][$j]).")";
      if ($reaction[$i][16][$j]!=1) {
	$w[$i] .= "**$reaction[$i][16][$j]_WP ";
      }
    }
  }
  $w[$i] .= "\n";
}

# ------------------------------------------------ #
# Create source terms foreach species
# ------------------------------------------------ #
for (my $i=0;$i<=$nspec;$i++) {
  $ncdot[$i] = 0;
}
for (my $i=0;$i<=$nspec;$i++) {
  $cdot[$i] = "  cdot(g".$speciesX[$i].") = 0.0_WP ";
}
for (my $i=0;$i<=$nreac;$i++) {

  # Reactants
  for (my $j=0;$j<=$#{$reaction[$i][1]};$j++) {
    if ($reaction[$i][1][$j]=~/^M/) {
      next;
    }
    $cdot[$link{$reaction[$i][1][$j]}] .= "- ";
    if ($reaction[$i][16][$j]!=1) {
      $cdot[$link{$reaction[$i][1][$j]}] .= $reaction[$i][16][$j]."_WP * ";
    }
    $cdot[$link{$reaction[$i][1][$j]}] .= "w(r".$reaction[$i][0].") ";

    $ncdot[$link{$reaction[$i][1][$j]}]++;
    if ($ncdot[$link{$reaction[$i][1][$j]}]>=50) {
      $cdot[$link{$reaction[$i][1][$j]}].="\n  cdot(g".$speciesX[$link{$reaction[$i][1][$j]}].") = cdot(g".$speciesX[$link{$reaction[$i][1][$j]}].") ";
      $ncdot[$link{$reaction[$i][1][$j]}]=0
    }

  }
  # Products
  for (my $j=0;$j<=$#{$reaction[$i][2]};$j++) {
    if ($reaction[$i][2][$j]=~/^M/) {
      next;
    }
    $cdot[$link{$reaction[$i][2][$j]}] .= "+ ";
    if ($reaction[$i][17][$j]!=1) {
      $cdot[$link{$reaction[$i][2][$j]}] .= $reaction[$i][17][$j]."_WP * ";
    }
    $cdot[$link{$reaction[$i][2][$j]}] .= "w(r".$reaction[$i][0].") ";

    $ncdot[$link{$reaction[$i][2][$j]}]++;
    if ($ncdot[$link{$reaction[$i][2][$j]}]>=50) {
      $cdot[$link{$reaction[$i][2][$j]}].="\n  cdot(g".$speciesX[$link{$reaction[$i][2][$j]}].") = cdot(g".$speciesX[$link{$reaction[$i][2][$j]}].") ";
      $ncdot[$link{$reaction[$i][2][$j]}]=0
    }
  }
}
for (my $i=0;$i<=$nspec;$i++) {
  $cdot[$i] .= "\n\n";
}

# ---------------------------------------------------------------- #
# Create production rates per reaction for each species 
# ---------------------------------------------------------------- #
for (my $i=0;$i<=$nreac;$i++) {
  for (my $j=0;$j<=$nspec;$j++) {
    $detpr[$j][$i] = "  detPR(g".$speciesX[$j].",r".$reaction[$i][0].") = ";
    $idetpr[$j][$i] = -1;
  }
}

for (my $i=0;$i<=$nreac;$i++) {
  # Reactants
  for (my $j=0;$j<=$#{$reaction[$i][1]};$j++) {
    if ($reaction[$i][1][$j]=~/^M/) {
      next;
    }
    $detpr[$link{$reaction[$i][1][$j]}][$i] .= "- ";
    if ($reaction[$i][16][$j]!=1) {
      $detpr[$link{$reaction[$i][1][$j]}][$i] .= $reaction[$i][16][$j]."_WP * ";
    }
    $detpr[$link{$reaction[$i][1][$j]}][$i] .= "w(r".$reaction[$i][0].") ";
    $idetpr[$link{$reaction[$i][1][$j]}][$i] = 1;
  }
  # Products
  for (my $j=0;$j<=$#{$reaction[$i][2]};$j++) {
    if ($reaction[$i][2][$j]=~/^M/) {
      next;
    }
    $detpr[$link{$reaction[$i][2][$j]}][$i] .= "+ ";
    if ($reaction[$i][17][$j]!=1) {
      $detpr[$link{$reaction[$i][2][$j]}][$i] .= $reaction[$i][17][$j]."_WP * ";
    }
    $detpr[$link{$reaction[$i][2][$j]}][$i] .= "w(r".$reaction[$i][0].") ";
    $idetpr[$link{$reaction[$i][2][$j]}][$i] = 1;
  }
}


# ======================================================================================= #
# Transport properties: use exact equations
# ======================================================================================= #
{
      
  # Loop through species
  for (my $is=0;$is<=$nspec;$is++) {

    # -------------------------------------------------- #
    # Viscosity
    # -------------------------------------------------- #
    
    # Coefficient independent of temperature
    $mucoeff[$is] = (5*sqrt($kb))/(16*sqrt($Pi*$Av))*sqrt( $WTH{$species[$is]} ) / ($sigma{$species[$is]}**2);

    # -------------------------------------------------- #
    # Diffusivity
    # -------------------------------------------------- #

    # Loop through species again for binary coefficients
    for (my $iz=0;$iz<=$nspec;$iz++) {
      my $sigmajk = 0.5*($sigma{$species[$is]}+$sigma{$species[$iz]});
      my $mjk = (1/$Av)*($WTH{$species[$is]}*$WTH{$species[$iz]})/($WTH{$species[$is]}+$WTH{$species[$iz]});
      $Dcoeff[$is][$iz] = 3/16*sqrt(2/$Pi)*sqrt($kb**3)/(sqrt($mjk)*$sigmajk**2);
    }

  }

}

# ======================================================================================================= #
# Transport properties: compute fitting coefficients for viscosity, binary coefficients, and conductivity
# ======================================================================================================= #
{

  # ------------------------------------------------------------------ #
  # Fitting parameters (default polynomial order = 4 ie 5 parameters)
  # ------------------------------------------------------------------ #

  # Number of coefficients in polynomial fit 
  $nfit = 5;

  # Temperature array
  my @lnTfit;
  my @Tfit;
  my $lnTstart = log(298);
  my $lnTend = log(3500);
  my $npoints = 500;
  my $dlnT = ( $lnTend - $lnTstart ) / ( $npoints - 1.0 );
  for (my $ii=0;$ii<$npoints;$ii++) { 
    $lnTfit[$ii] = $lnTstart + $dlnT * $ii;
    $Tfit[$ii] = exp($lnTfit[$ii]);
  }

  # Fitting functions
  my (@f1,@f2,@f3,@f4,@f5,$f1,$f2,$f3,$f4,$f5,$funcs,$data,$yfit,$coeffs);
  for (my $ii=0;$ii<$npoints;$ii++) {
    $f1[$ii] = 1;		# constant function
    $f2[$ii] = $lnTfit[$ii];	# linear term
    $f3[$ii] = $lnTfit[$ii]**2;	# quadratic
    $f4[$ii] = $lnTfit[$ii]**3;	# cubic
    $f5[$ii] = $lnTfit[$ii]**4;	# cubic
  }
  $f1 = pdl(@f1);
  $f2 = pdl(@f2);
  $f3 = pdl(@f3);
  $f4 = pdl(@f4);
  $f5 = pdl(@f5);
  $funcs = cat $f1,$f2,$f3,$f4,$f5;
  $xdata = pdl(@lnTfit);

  my $err_mu_abs = 0;
  my $err_mu_rel = 0;
  my $err_D_abs = 0;
  my $err_D_rel = 0;
  my $err_lambda_abs = 0;
  my $err_lambda_rel = 0;
  
  # Loop through species
  for (my $is=0;$is<=$nspec;$is++) {

    # -------------------------------------------------- #
    # Viscosity
    # -------------------------------------------------- #
    
    # Viscosity vector
    my (@lnmu,@weightlnmu);
    
    # Temperature independent part of viscosity expression
    my $mucoeff = (5*sqrt(($WTH{$species[$is]}/$Av)*$kb))/(16*sqrt($Pi)*($sigma{$species[$is]})**2);

    # Compute viscosity for each species at Tfit points
    for (my $iT=0;$iT<$npoints;$iT++) {
      $lnmu[$iT] = log($mucoeff*sqrt($Tfit[$iT])/&omega_mu($Tfit[$iT]*$KoEps{$species[$is]}));
      $weightlnmu[$iT] = 1/($lnmu[$iT]**2);
    }

    # Fit polynomial on @lnmu data;
    $data = pdl(@lnmu);
    $weights = pdl(@weightlnmu);
    ($yfit, $coeffs)  = fitpoly1d($xdata, $data, 5, {Weights => $weights} );
    $lnmufit[$is][0] = sprintf("%.8e",at($coeffs,0)); 
    $lnmufit[$is][1] = sprintf("%.8e",at($coeffs,1)); 
    $lnmufit[$is][2] = sprintf("%.8e",at($coeffs,2)); 
    $lnmufit[$is][3] = sprintf("%.8e",at($coeffs,3)); 
    $lnmufit[$is][4] = sprintf("%.8e",at($coeffs,4)); 

    # Error control
    my $err_mu_abs_max = 0;
    my $err_mu_rel_max = 0;
    for (my $iT=0;$iT<$npoints;$iT++) {
      my $fit = exp($lnmufit[$is][0]+$lnTfit[$iT]*($lnmufit[$is][1]+$lnTfit[$iT]*($lnmufit[$is][2]+$lnTfit[$iT]*($lnmufit[$is][3]+$lnTfit[$iT]*($lnmufit[$is][4])))));
      # Max absolute error
      $err_mu_abs_max = &maxval($err_mu_abs_max,abs(exp($lnmu[$iT])-$fit));
      $err_mu_abs = &maxval($err_mu_abs,$err_mu_abs_max);
      # Max relative error 
      $err_mu_rel_max = &maxval($err_mu_rel_max,abs( exp($lnmu[$iT])-$fit) / exp($lnmu[$iT]) );
      $err_mu_rel = &maxval($err_mu_rel,$err_mu_rel_max);
    }
    #print "Visc $species[$is] error - absolute: $err_mu_abs_max, relative: $err_mu_rel_max\n";


    # -------------------------------------------------- #
    # Diffusivity
    # -------------------------------------------------- #

    # Loop over all other species
    for (my $iz=0;$iz<=$nspec;$iz++) {
      
      # Diffusivity vector
      my (@lnD,@weightlnD);

      my $sigmajk = 0.5*($sigma{$species[$is]}+$sigma{$species[$iz]});
      my $mjk = (1/$Av)*($WTH{$species[$is]}*$WTH{$species[$iz]})/($WTH{$species[$is]}+$WTH{$species[$iz]});
      my $koepsjk = sqrt($KoEps{$species[$is]}*$KoEps{$species[$iz]});
      my $dcoeff = 3/16*sqrt(2/$Pi)*sqrt($kb**3)/(sqrt($mjk)*$sigmajk**2);
            
      for (my $iT=0;$iT<$npoints;$iT++) {
     	$T3invP[$iT] = sqrt($Tfit[$iT]**3)/$Patm;
     	$lnD[$iT] = log($dcoeff*$T3invP[$iT]/&omega_D($Tfit[$iT]*$koepsjk));
	$weightlnD[$iT] = 1;#/($lnD[$iT]**2);
      }

      # Fit polynomial on @lnD data;
      $data = pdl(@lnD);
      $weights = pdl(@weightlnD);
      ($yfit, $coeffs)  = fitpoly1d($xdata, $data, 5, {Weights => $weights} );
      $lnDfit[$is][$iz][0] = sprintf("%.8e",at($coeffs,0)); 
      $lnDfit[$is][$iz][1] = sprintf("%.8e",at($coeffs,1)); 
      $lnDfit[$is][$iz][2] = sprintf("%.8e",at($coeffs,2)); 
      $lnDfit[$is][$iz][3] = sprintf("%.8e",at($coeffs,3)); 
      $lnDfit[$is][$iz][4] = sprintf("%.8e",at($coeffs,4)); 
      
      # Error control
      my $err_D_abs_max = 0;
      my $err_D_rel_max = 0;
      for (my $iT=0;$iT<$npoints;$iT++) {
     	my $fit = exp($lnDfit[$is][$iz][0]+$lnTfit[$iT]*($lnDfit[$is][$iz][1]+$lnTfit[$iT]*($lnDfit[$is][$iz][2]+$lnTfit[$iT]*($lnDfit[$is][$iz][3]+$lnTfit[$iT]*($lnDfit[$is][$iz][4])))));
     	# Max absolute error
     	$err_D_abs_max = &maxval($err_D_abs_max,abs(exp($lnD[$iT])-$fit));
	$err_D_abs = &maxval($err_D_abs,$err_D_abs_max);
     	# Max relative error 
     	$err_D_rel_max = &maxval($err_D_rel_max,abs( (exp($lnD[$iT])-$fit) / exp($lnD[$iT]) ));
	$err_D_rel = &maxval($err_D_rel,$err_D_rel_max);
     	# Store data for plotting
     	$D[$iT][$is][$iz][0] = exp($lnD[$iT]);
     	$D[$iT][$is][$iz][1] = $fit;
      }
      #print "Diff[$species[$is],$species[$iz]] error - absolute: $err_D_abs_max, relative: $err_D_rel_max\n";

    } # End secondary species loop

    # -------------------------------------------------- #
    # Conductivity
    # -------------------------------------------------- #

    # Conductivity vector
    my (@lnlambda,@weightlnlambda);

    # Constant part
    my $myZrot_298 = $Zrot{$species[$is]};
    my $mylin = $linearity{$species[$is]};
    
    # Compute conductivity for each species at Tfit points
    for (my $iT=0;$iT<$npoints;$iT++) {
      
      # Zrot at T
      my $epsokoT = 1/($Tfit[$iT]*$KoEps{$species[$is]});
      my $F = 1+$Pi**(3/2)/2*($epsokoT)+($Pi**2/4+2)*($epsokoT)+($Pi**(3/2))*($epsokoT)**(3/2);
      $myZrot = $myZrot_298*$F;
      
      # Heat capacity at constant volume
      my $isTH = $speciesTH{$species[$is]};
      if ($Tfit[$iT]<=$Tmed{$species[$is]}) {
     	$Cv = $Rcst*($alo[$isTH][1]+$alo[$isTH][2]*$Tfit[$iT]+$alo[$isTH][3]*$Tfit[$iT]**2+$alo[$isTH][4]*$Tfit[$iT]**3+$alo[$isTH][5]*$Tfit[$iT]**4 - 1);
     	$Cp = $Cv+$Rcst;
      } else {
     	$Cv = $Rcst*($Ahi[$isTH][1]+$Ahi[$isTH][2]*$Tfit[$iT]+$Ahi[$isTH][3]*$Tfit[$iT]**2+$Ahi[$isTH][4]*$Tfit[$iT]**3+$Ahi[$isTH][5]*$Tfit[$iT]**4 - 1);
     	$Cp = $Cv+$Rcst;
      }
     
      # Cvs
      my ($Cv_trans,$Cv_rot,$Cv_vib);
      if ($mylin==0) {		# Atomic molecule
     	$Cv_trans = 3/2*$Rcst;
     	$Cv_rot = 0;
     	$Cv_vib = 0;

      } elsif ($mylin==1) {	# Linear molecule
     	$Cv_trans = 3/2*$Rcst;
     	$Cv_rot = $Rcst;
     	$Cv_vib = $Cv-5/2*$Rcst;
	
      } elsif ($mylin==2) {	# Non-linear molecule
     	$Cv_trans = 3/2*$Rcst;
     	$Cv_rot = 3/2*$Rcst;
     	$Cv_vib = $Cv-3*$Rcst;

      } else {
     	die "Undefined molecule linarity for species $species[$is]\n";
      }

      # Various parts of conductivity expression
      my $rhoDkk = 3/16*sqrt(2/$Pi)*$WTH{$species[$is]}/$Rcst*sqrt($Av/$WTH{$species[$is]})*(1/($sigma{$species[$is]})**2)*sqrt($kb**3)*sqrt($Tfit[$iT])*(1/&omega_D($Tfit[$iT]*$KoEps{$species[$is]}));
      my $A = 5/2-$rhoDkk/exp($lnmu[$iT]);
      my $B = $myZrot+2/$Pi*(5/3*($Cv_rot/$Rcst)+($rhoDkk/exp($lnmu[$iT])));
      my $ftrans = 5/2*(1-2/$Pi*($Cv_rot/$Cv_trans)*($A/$B));
      my $frot = $rhoDkk/exp($lnmu[$iT])*(1+2/$Pi*($A/$B));
      my $fvib = $rhoDkk/exp($lnmu[$iT]);
      
      # Final expressions
      $lnlambda[$iT] = (exp($lnmu[$iT])/$WTH{$species[$is]})*($ftrans*$Cv_trans + $frot*$Cv_rot + $fvib*$Cv_vib);
      $lnlambda[$iT] = log($lnlambda[$iT]/sqrt($Tfit[$iT])); 
      #$lambdaFM[$iT] = exp($lnmu[$iT])*($Cp/$WTH{$species[$is]}+1.2*$Rcst/$WTH{$species[$is]});
      #if ($iT==0){print "$species[$is]\t$Tfit[$iT]\t".(sqrt($Tfit[$iT])*exp($lnlambda[$iT]))."\t$lambdaFM[$iT]\t".($Cp/$WTH{$species[$is]})."\t".(exp($lnmu[$iT]))."\n"}
      $weightlnlambda[$iT] = 1/($lnlambda[$iT]**2);
    }

    # Fit polynomial on @lnlambda data;
    $data = pdl(@lnlambda);
    $weights = pdl(@weightlnlambda);
    ($yfit, $coeffs)  = fitpoly1d($xdata, $data, 5, {Weights => $weights} );
    $lnlambdafit[$is][0] = sprintf("%.8e",at($coeffs,0)); 
    $lnlambdafit[$is][1] = sprintf("%.8e",at($coeffs,1)); 
    $lnlambdafit[$is][2] = sprintf("%.8e",at($coeffs,2)); 
    $lnlambdafit[$is][3] = sprintf("%.8e",at($coeffs,3)); 
    $lnlambdafit[$is][4] = sprintf("%.8e",at($coeffs,4)); 
    
    # Error control
    my $err_lambda_abs_max = 0;
    my $err_lambda_rel_max = 0;
    for (my $iT=0;$iT<$npoints;$iT++) {
      my $fit = sqrt($Tfit[$iT])*exp($lnlambdafit[$is][0]+$lnTfit[$iT]*($lnlambdafit[$is][1]+$lnTfit[$iT]*($lnlambdafit[$is][2]+$lnTfit[$iT]*($lnlambdafit[$is][3]+$lnTfit[$iT]*($lnlambdafit[$is][4])))));
      # Max absolute error
      $err_lambda_abs_max = &maxval($err_lambda_abs_max,abs(sqrt($Tfit[$iT])*exp($lnlambda[$iT])-$fit));
      $err_lambda_abs = &maxval($err_lambda_abs,$err_lambda_abs_max);
      #print "$species[$is]: $Tfit[$iT]\t".sqrt($Tfit[$iT])*exp($lnlambda[$iT])."\t$fit\t$lambdaFM[$iT]\n";
      # Max relative error 
      $err_lambda_rel_max = &maxval($err_lambda_rel_max,abs( (sqrt($Tfit[$iT])*exp($lnlambda[$iT])-$fit) / (sqrt($Tfit[$iT])*exp($lnlambda[$iT])) ));
      $err_lambda_rel = &maxval($err_lambda_rel,$err_lambda_rel_max);
    }
    #print "Conductivity $species[$is] error - absolute: $err_lambda_abs_max, relative: $err_lambda_rel_max\n";

  } # end species loop 

    print "Maximum error for viscosity: absolute: $err_mu_abs, relative: $err_mu_rel\n";
    print "Maximum error for diffusion coefficients: absolute: $err_D_abs, relative: $err_D_rel\n";
    print "Maximum error for conductivity: absolute: $err_lambda_abs, relative: $err_lambda_rel\n";


}


# ======================================================================================= #
# Print mechanism.f90 file 
# ======================================================================================= #

# ------------------------------------------------ #
# Initial comments in sources
# ------------------------------------------------ #
my $length = length($mechfile);
print M "! ";
for (my $i=0;$i<$length;$i++) {
  print M '-';
}
print M " !\n";
print M "! $mechfile !\n";
print M "! ";
for (my $i=0;$i<$length;$i++) {
  print M '-';
}
print M " !\n";

# ------------------------------------------------ #
# Module "mechanism"
# ------------------------------------------------ #
print M "module mechanism\n";
print M "  use precision\n";
print M "  use string\n";
print M "  implicit none\n";
print M "\n";
print M "  integer, parameter :: nspec = ".($nspec+1)."\n";
print M "  integer, parameter :: nreac = ".($nreac+1)."\n";
print M "\n";
print M "  ! Gas phase species\n";
for (my$i=0;$i<=$nspec;$i++) {
  print M "  integer, parameter :: g".$speciesX[$i]." = ".($i+1)."\n";
}
print M "\n";
print M "  ! Index of reactions\n";
for (my$i=0;$i<=$nreac;$i++) {
  print M "  integer, parameter :: r".$reaction[$i][0]." = ".($i+1)."\n";
}
print M "\n";
print M "  ! Index of third bodies\n";
my $mcount = 1;
foreach (keys %M) {
  print M "  integer, parameter :: m".$_." = ".($mcount)."\n";
  $mcount++
}
print M "\n";
print M "  ! Transport parameters and function\n";
print M "  real(WP), dimension(nspec) :: koveps,mucoeff\n";
print M "  real(WP), dimension(nspec,nspec) :: Dcoeffs,Ocoeffs\n";
print M "\n";
if ($use_fit == 1){
  print M "  ! Fitting coefficients for transport properties\n";
  print M "  real(WP), dimension(nspec,$nfit) :: mufc,lambdafc\n";
  print M "  real(WP), dimension(nspec,nspec,$nfit) :: dfc\n";
  print M "\n";
} # end fit

# --- Declarations --- #
print M "  ! --- Declarations - Rate coefficients --- !\n";
foreach (@kdef) {
  if ($_=~/[^\s]/) {
    print M "$_";
  }
}
print M "\n";
print M "contains\n";
print M "\n";

# Function for pressure dependent coefficients
print M "  ! ============================================ !\n";
print M "  ! Function for pressure dependent coefficients !\n";
print M "  ! ============================================ !\n";
print M "  real(WP) function getlindratecoeff (Tloc,pressure,k0,kinf,fc,concin)\n";
print M "      implicit none\n\n";
    
print M "    real(WP) ::  Tloc,pressure,k0,kinf,fc\n";
print M "    real(WP), parameter :: R = 8.31434\n";
print M "    real(WP) :: ntmp,ccoeff,dcoeff,lgknull\n";
print M "    real(WP) :: f\n";
print M "    real(WP) :: conc, concin\n\n";
    
print M "    if (concin.gt.0.0_WP) then\n";
print M "      conc = concin\n";
print M "    else\n";
print M "      conc = pressure / ( R * Tloc )\n";
print M "    end if\n";
print M "    ntmp = 0.75 - 1.27 * dlog10( fc )\n\n";
    
print M "    ccoeff = - 0.4 - 0.67 * dlog10( fc )\n";
print M "    dcoeff = 0.14\n";
print M "    k0 = k0 * conc / max(kinf, 1.0e-60_WP)\n";
print M "    lgknull = dlog10(k0)\n";
print M "    f = (lgknull+ccoeff)/(ntmp-dcoeff*(lgknull+ccoeff))\n";
print M "    f = fc**(1.0_WP / ( f * f + 1.0_WP ))\n";
print M "    getlindratecoeff = kinf * f * k0 / ( 1.0 + k0 )\n";
print M "\n";
print M "  end function getlindratecoeff\n";
print M "\n";

if ($use_fit!=1){
  # Collision integral omega(2,2,*)
  print M "  ! ================================================ !\n";
  print M "  ! Compute omegamu needed for transport calculation !\n";
  print M "  ! ================================================ !\n";
  print M "  real(WP) function omegamu(T_)\n";
  print M "    implicit none\n";
  print M "    real(WP), intent(in) :: T_\n";
  print M "    real(WP), parameter :: m1=3.3530622607_WP\n";
  print M "    real(WP), parameter :: m2=2.53272006_WP\n";
  print M "    real(WP), parameter :: m3=2.9024238575_WP\n";
  print M "    real(WP), parameter :: m4=0.11186138893_WP\n";
  print M "    real(WP), parameter :: m5=0.8662326188_WP  ! = -0.1337673812 + 1.0\n";
  print M "    real(WP), parameter :: m6=1.3913958626_WP\n";
  print M "    real(WP), parameter :: m7=3.158490576_WP\n";
  print M "    real(WP), parameter :: m8=0.18973411754_WP\n";
  print M "    real(WP), parameter :: m9=0.00018682962894_WP\n";
  print M "    omegamu=(m1+T_*(m2+T_*(m3+T_*m4)))/(m5+T_*(m6+T_*(m7+T_*(m8+T_*m9))))\n";
  print M "  end function omegamu\n";
  print M "\n";
  # Collision integral omega(1,1,*)
  print M "  ! ================================================ !\n";
  print M "  ! Compute omegamu needed for transport calculation !\n";
  print M "  ! ================================================ !\n";
  print M "  real(WP) function omegaD(T_)\n";
  print M "    implicit none\n";
  print M "    real(WP), intent(in) :: T_\n";
  print M "    real(WP), parameter :: m1=6.8728271691_WP\n";
  print M "    real(WP), parameter :: m2 = 9.4122316321_WP\n";
  print M "    real(WP), parameter :: m3 = 7.7442359037_WP\n";
  print M "    real(WP), parameter :: m4 = 0.23424661229_WP\n";
  print M "    real(WP), parameter :: m5 = 1.45337701568_WP	! = 1.0 + 0.45337701568 \n";
  print M "    real(WP), parameter :: m6 = 5.2269794238_WP\n";
  print M "    real(WP), parameter :: m7 = 9.7108519575_WP\n";
  print M "    real(WP), parameter :: m8 = 0.46539437353_WP\n";
  print M "    real(WP), parameter :: m9 = 0.00041908394781_WP\n";
  print M "    omegaD=(m1+T_*(m2+T_*(m3+T_*m4)))/(m5+T_*(m6+T_*(m7+T_*(m8+T_*m9))));\n";
  print M "  end function omegaD\n";
  print M "\n";
} # end fit

print M "end module mechanism\n";
print M "\n";


# ------------------------------------------------ #
# Third bodies
# ------------------------------------------------ #
print M "! ================ !\n";
print M "! Third bodies !\n";
print M "! ================ !\n";
print M "subroutine thirdbodies ( c, M )\n";
print M "  use mechanism\n";
print M "  implicit none\n";
print M "\n";
print M "  real(WP), dimension(nspec) :: c\n";
print M "  real(WP), dimension(".((keys%M)+1).") :: M\n";
print M "\n";
print M "  ! Get Third body concentrations \n";
foreach (keys %M) {
  my $line = "  M(m".$_.") = ";
  # Check if OTHER is there
  my $offset = 0.0;
  for (my $i=0;$i<=$#{$M{$_}[0]};$i++) {
    if ($M{$_}[0][$i]=~/OTHER/) {
      $offset = 1.0;
    }
  }
  # Create line to write in mech file
  for (my $i=0;$i<=$#{$M{$_}[0]};$i++) {
    if ($M{$_}[0][$i]=~/OTHER/) {
      if ($i!=0) {
	$line .= "+ ";
      }
      $line .= "sum(c) ";
    } else {
      if (exists $link{$M{$_}[0][$i]}) {
	if ($i!=0) {
	  $line .= "+ ";
	}
	$line .= "(".($M{$_}[1][$i]-$offset)."_WP)*c(g".$M{$_}[0][$i].") ";
      }
    }
  }
  print M $line."\n";
}	
print M "\n";
print M "  return\n";
print M "end subroutine thirdbodies\n";
print M "\n";

# ------------------------------------------------ #
# Rate coefficients
# ------------------------------------------------ #
print M "! ================= !\n";
print M "! Rate coefficients !\n";
print M "! ================= !\n";
print M "subroutine ratecoefficients ( k, Tloc, pressure, M )\n";
print M "  use mechanism\n";
print M "  implicit none\n";
print M "\n";
print M "  real(WP), dimension(nreac) :: k\n";
print M "  real(WP), dimension(".((keys%M)+1).") :: M\n";
print M "  real(WP) :: Tloc,lnT,RT,pressure\n";
print M "\n";
print M "  ! Logarithm of temperature, R*T\n";
print M "  lnT = log(Tloc)\n";
print M "  RT = 8.314_WP*Tloc\n";
print M "\n";
print M "  ! Get rate coefficients \n";
foreach (@kline) {
  print M "$_";
}
print M "\n";
print M "  return\n";
print M "end subroutine ratecoefficients\n";
print M "\n";

# ------------------------------------------------ #
# Reaction rates
# ------------------------------------------------ #
print M "! ============== !\n";
print M "! Reaction rates !\n";
print M "! ============== !\n";
print M "subroutine reactionrates ( w, k, c, M )\n";
print M "  use mechanism\n";
print M "  implicit none\n";
print M "\n";
print M "  real(WP), dimension(nreac) :: k, w\n";
print M "  real(WP), dimension(nspec) :: c\n";
print M "  real(WP), dimension(".((keys%M)+1).") :: M\n";
print M "\n";
print M "  ! Get reaction rates \n";
foreach (@w) {
  print M "$_";
}
print M "\n";
print M "  return\n";
print M "end subroutine reactionrates\n";
print M "\n";


# ------------------------------------------------ #
# Production rates
# ------------------------------------------------ #
print M "! ================ !\n";
print M "! Production rates !\n";
print M "! ================ !\n";
print M "subroutine prodrates ( cdot, w)\n";
print M "  use mechanism\n";
print M "  implicit none\n";
print M "\n";
print M "  ! --- Declarations - Miscellaneous --- !\n";
print M "  real(WP), dimension(nspec) :: cdot\n";
print M "  real(WP), dimension(nreac) :: w\n";
print M "\n";

# --- Expressions for production rates --- !
print M "  ! --- Production rates --- !\n";
for (my $i=0;$i<=$nspec;$i++) {
  print M "$cdot[$i]";
}
print M "\n";
print M "  return\n";
print M "end subroutine prodrates\n";
print M "\n";


# ------------------------------------------------ #
# Parameters initialization
# ------------------------------------------------ #
print M "! =========================== !\n";
print M "!  Parameters initialization  !\n";
print M "! =========================== !\n";
print M "subroutine mechanism_init \n";
print M "  use mechanism\n";
print M "  implicit none\n";
print M "\n";
if ($use_fit==0){
  print M "  call get_kovereps\n";
  print M "  call get_mucoeff\n";
  print M "  call get_Dcoeff\n";
  print M "  call get_Ocoeff\n";
  print M "\n";
}else{
  print M "  call transport_fits\n";
  print M "\n";
}
print M "  return\n";
print M "end subroutine mechanism_init\n\n";


# ------------------------------------------------ #
# Molar mass
# ------------------------------------------------ #
print M "! ============ !\n";
print M "!  Molar mass  !\n";
print M "! ============ !\n";
print M "subroutine getmolarmass( mm )\n";
print M "  use mechanism\n";
print M "  implicit none\n";
print M "\n";
print M "  real(WP), dimension(nspec) :: mm\n";
print M "\n";
for (my$i=0;$i<=$nspec;$i++) {
  print M "  mm(g".$speciesX[$i].") = ".$WTH{$species[$i]}."_WP\n";
}
print M "\n";
print M "  return\n";
print M "end subroutine getmolarmass\n\n";


# ------------------------------------------------ #
# Species names
# ------------------------------------------------ #
print M "! =============== !\n";
print M "!  Species names  !\n";
print M "! =============== !\n";
print M "subroutine getspeciesnames( names )\n";
print M "  use mechanism\n";
print M "  implicit none\n";
print M "\n";
print M "  character(len=str_medium), dimension(nspec) :: names\n";
print M "\n";
for (my$i=0;$i<=$nspec;$i++) {
  print M "  names(g".$speciesX[$i].") = '$species[$i]'\n";
}
print M "\n";
print M "  return\n";
print M "end subroutine getspeciesnames\n";
print M "\n";

# ------------------------------------------------ #
# Number of species - ALL
# ------------------------------------------------ #
print M "! ================= !\n";
print M "! Number of species !\n";
print M "! ================= !\n";
print M "subroutine getnspecies_all( nspecies )\n";
print M  "  use mechanism\n";
print M  "  implicit none\n";
print M  "\n";
print M  "  integer :: nspecies\n";
print M  "\n";
print M  "  nspecies = nspec\n";
print M  "\n";
print M  "  return\n";
print M  "end subroutine getnspecies_all\n";
print M "\n";

# ------------------------------------------------ #
# Number of reactions
# ------------------------------------------------ #
print M "! =================== !\n";
print M "! Number of reactions !\n";
print M "! =================== !\n";
print M "subroutine getnreactions( nreactions )\n";
print M  "  use mechanism\n";
print M  "  implicit none\n";
print M  "\n";
print M  "  integer ::  nreactions\n";
print M  "\n";
print M  "  nreactions = nreac\n";
print M  "\n";
print M  "  return\n";
print M  "end subroutine getnreactions\n";

# ------------------------------------------------ #
# Number of species - resolved
# ------------------------------------------------ #
print M "! ================================== !\n";
print M "! Number of non-steady-state species !\n";
print M "! ================================== !\n";
print M "subroutine getnspecies(nspecies_nons)\n";
print M "  use mechanism\n";
print M "  implicit none\n";
print M  "\n";
print M "  integer :: nspecies_nons\n";
print M "  nspecies_NONS = nspec\n";
print M  "\n"; 
print M "  return\n";
print M "end subroutine getnspecies\n";
print M  "\n\n";

# ------------------------------------------------ #
# Heat capacity and enthalpy of formation
# ------------------------------------------------ #
print M "! ===================== !\n";
print M "! Cp and H computations !\n";
print M "! ===================== !\n";
print M "subroutine compthermodata(H,Cp,T)\n";
print M "  use mechanism\n\n";
print M "  implicit none\n\n";
print M "  real(WP), dimension(nspec) :: H,Cp\n";
print M "  real(WP) :: T\n\n";

print M "  ! All species with default medium temperature of 1000K\n";
print M "  if (T.gt.$TmedD) then\n\n";
for (my $i=0;$i<=$nspec;$i++) {
  my $sX = $speciesX[$i];
  my $is = $speciesTH{$species[$i]};
  my $myw = $WTH{$species[$i]};
  if ($Tmed{$species[$i]}==$TmedD){
    print M "    H(g".$sX.") = ".($Rcst/$myw)." * ( T * (($Ahi[$is][1]_WP) + T * ((".($Ahi[$is][2]/2.0)."_WP) + T * ((".($Ahi[$is][3]/3.0)."_WP) + T * ((".($Ahi[$is][4]/4.0)."_WP) + T * ((".($Ahi[$is][5]/5.0)."_WP)))))) + ($Ahi[$is][6]_WP))\n";
    print M "    Cp(g".$sX.") = ".($Rcst/$myw)." * (($Ahi[$is][1]_WP) + T * (($Ahi[$is][2]_WP) + T * (($Ahi[$is][3]_WP) + T * (($Ahi[$is][4]_WP) + T * ($Ahi[$is][5]_WP)))))\n\n";
  }
}
print M "\n  else \n\n";
for (my $i=0;$i<=$nspec;$i++) {
  my $sX = $speciesX[$i];
  my $is = $speciesTH{$species[$i]};
  my $myw = $WTH{$species[$i]};
  if ($Tmed{$species[$i]}==$TmedD){
    print M "    H(g".$sX.") = ".($Rcst/$myw)." * ( T * (($alo[$is][1]_WP) + T * ((".($alo[$is][2]/2.0)."_WP) + T * ((".($alo[$is][3]/3.0)."_WP) + T * ((".($alo[$is][4]/4.0)."_WP) + T * ((".($alo[$is][5]/5.0)."_WP)))))) + ($alo[$is][6]_WP))\n";
    print M "    Cp(g".$sX.") = ".($Rcst/$myw)." * (($alo[$is][1]_WP) + T * (($alo[$is][2]_WP) + T * (($alo[$is][3]_WP) + T * (($alo[$is][4]_WP) + T * ($alo[$is][5]_WP)))))\n\n";
  }
}
print M "  end if\n\n";
print M "  ! Species with specific medium temperature\n";
for (my $i=0;$i<=$nspec;$i++) {
  my $sX = $speciesX[$i];
  my $is = $speciesTH{$species[$i]};
  my $myw = $WTH{$species[$i]};
  if ($Tmed{$species[$i]}!=$TmedD){
    print M "  if (T.gt.$Tmed{$species[$i]}) then\n";
    print M "    H(g".$sX.") = ".($Rcst/$myw)."_WP * ( T * (($Ahi[$is][1]_WP) + T * ((".($Ahi[$is][2]/2.0)."_WP) + T * ((".($Ahi[$is][3]/3.0)."_WP) + T * ((".($Ahi[$is][4]/4.0)."_WP) + T * ((".($Ahi[$is][5]/5.0)."_WP)))))) + ($Ahi[$is][6]_WP))\n";
    print M "    Cp(g".$sX.") = ".($Rcst/$myw)." * (($Ahi[$is][1]_WP) + T * (($Ahi[$is][2]_WP) + T * (($Ahi[$is][3]_WP) + T * (($Ahi[$is][4]_WP) + T * ($Ahi[$is][5]_WP)))))\n";
    print M "  else \n";
    print M "    H(g".$sX.") = ".($Rcst/$myw)."_WP * ( T * (($alo[$is][1]_WP) + T * ((".($alo[$is][2]/2.0)."_WP) + T * ((".($alo[$is][3]/3.0)."_WP) + T * ((".($alo[$is][4]/4.0)."_WP) + T * ((".($alo[$is][5]/5.0)."_WP)))))) + ($alo[$is][6]_WP))\n";
    print M "    Cp(g".$sX.") = ".($Rcst/$myw)." * (($alo[$is][1]_WP) + T * (($alo[$is][2]_WP) + T * (($alo[$is][3]_WP) + T * (($alo[$is][4]_WP) + T * ($alo[$is][5]_WP)))))\n";
    print M "  end if\n\n";
  }
}

print M "\n  return\n";
print M "end subroutine compthermodata\n\n";

# ----------------------------------------------------------------------------------- #
# Choose wether we want to use fits for the transport properties or exact expressions #
# ----------------------------------------------------------------------------------- #
if ($use_fit==1){
# ------------------------------------------------ #
# Pure compounds fitting coefficients 
# ------------------------------------------------ #
  print M "subroutine transport_fits\n";
  print M "  use mechanism\n";
  print M "  implicit none\n";
  print M "  \n";
  print M "  \n";
  # ---- Viscosity fitting coefficients ---- #
  print M "  ! Pure compounds viscosity fitting coefficients\n";
  for (my$i=0;$i<=$nspec;$i++) {
    print M "  ! ".$species[$i]."\n";
    for (my$k=0;$k<$nfit;$k++) {
      print M "  mufc(".($i+1).",".($k+1).") = $lnmufit[$i][$k]\n";
    }
    print M "\n";
  }
  print M "\n";
  # ---- Conductivity fitting coefficients ---- #
  print M "  ! Pure compounds conductivity fitting coefficients\n";
  for (my$i=0;$i<=$nspec;$i++) {
    print M "  ! ".$species[$i]."\n";
    for (my$k=0;$k<$nfit;$k++) {
      print M "  lambdafc(".($i+1).",".($k+1).") = $lnlambdafit[$i][$k]\n";
    }
    print M "\n";
  }
  print M "\n";
  # ---- Diffusivity fitting coefficients ---- #
  print M "  ! Pure compounds diffusivity fitting coefficients\n";
  for (my$i=0;$i<=$nspec;$i++) {
    for (my$j=0;$j<=$nspec;$j++) {
      print M "  ! Binary coefficient D($species[$i],$species[$j])\n";
      for (my$k=0;$k<$nfit;$k++) {
	print M "  dfc(".($i+1).",".($j+1).",".($k+1).") = $lnDfit[$i][$j][$k]\n";
      }
    }
    print M "\n";
  }
  print M "\n";

  print M "  return\n";
  print M "end subroutine transport_fits\n";
  print M "\n";
  print M "\n";

  # ------------------------------------------------------ #
  print M "! ===================================================== !\n";
  print M "! Polynomial approximations of pur compounds viscosity !\n";
  print M "! ===================================================== !\n";
  print M "subroutine get_viscosity( mu, T )\n";
  print M "  use mechanism\n";
  print M "  implicit none\n";
  print M "\n";
  print M "  ! Viscosity array   \n  real(WP), dimension(nspec) :: mu\n";
  print M "  ! Temperature\n  real(WP) :: T\n";
  print M "  ! Misc.\n  integer :: i,k\n  real(WP) :: lnT\n";
  print M "\n";

  # Log of temperature
  print M "  ! Logarithm of temperature\n";
  print M "  lnT = log(T)\n";
  print M "\n";

  # Viscosity - fit
  print M "  ! Viscosity - 4rd order polynomial fit\n";
  print M "  do i=1,nspec\n";
  print M "    mu(i) = mufc(i,1)+lnT*(mufc(i,2)+lnT*(mufc(i,3)+lnT*(mufc(i,4)+lnT*mufc(i,5))))\n";
  print M "    mu(i) = exp(mu(i))\n";
  print M "  end do\n";
  print M "\n";

  print M "  return\n";
  print M "end subroutine get_viscosity\n\n";

  # ------------------------------------------------------ #
  print M "! ======================================================= !\n";
  print M "! Polynomial approximations of pur compounds conductivity !\n";
  print M "! ======================================================= !\n";
  print M "subroutine get_conductivity( lambda, T, mu, Cp, W )\n";
  print M "  use mechanism\n";
  print M "  implicit none\n";
  print M "\n";
  print M "  ! Conductivity array\n  real(WP), dimension(nspec) :: lambda\n";
  print M "  ! Temperature\n  real(WP) :: T\n";
  print M "  ! Viscosity and co.\n  real(WP), dimension(nspec) :: mu, Cp, W\n";
  print M "  ! Misc.\n  integer :: i,k\n  real(WP) :: lnT\n";
  print M "\n";

  # Log of temperature
  print M "  ! Logarithm of temperature\n";
  print M "  lnT = log(T)\n";
  print M "\n";
  
  # Conductivity
  print M "  ! Conductivity - 4rd order polynomial fit\n";
  print M "  do i=1,nspec\n";
  print M "    lambda(i) = lambdafc(i,1)+lnT*(lambdafc(i,2)+lnT*(lambdafc(i,3)+lnT*(lambdafc(i,4)+lnT*lambdafc(i,5))))\n";
  print M "    lambda(i) = exp(lambda(i))\n";
  print M "    lambda(i) = sqrt(T)*lambda(i)\n";
  print M "  end do\n";
  print M "\n";
  
  print M "  return\n";
  print M "end subroutine get_conductivity\n\n";

  # ------------------------------------------------------ #
  print M "! ====================================================== !\n";
  print M "! Polynomial approximations of pur compounds diffusivity !\n";
  print M "! ====================================================== !\n";
  print M "subroutine get_invDij( invdiff, T, P )\n";
  print M "  use mechanism\n";
  print M "  implicit none\n";
  print M "\n";
  print M "  ! Diffusivity array \n  real(WP), dimension(nspec,nspec) :: invdiff\n";
  print M "  ! Temperature\n  real(WP) :: T\n";
  print M "  ! Pressure (to rescale)\n  real(WP) :: P\n";
  print M "  ! Misc.\n  integer :: i,j,k\n  real(WP) :: lnT\n";
  print M "\n";

  # Log of temperature
  print M "  ! Logarithm of temperature\n";
  print M "  lnT = log(T)\n";
  print M "\n";

  # Diffusivity
  print M "  ! Diffusivity - 4rd order polynomial fit\n";
  print M "  do i=1,nspec\n";
  print M "    do j=1,nspec\n";
  print M "       invdiff(i,j) = dfc(i,j,1)+lnT*(dfc(i,j,2)+lnT*(dfc(i,j,3)+lnT*(dfc(i,j,4)+lnT*dfc(i,j,5))))\n";
  print M "       invdiff(i,j) = 1.0_WP/(P/".($Patm)."_WP*exp(invdiff(i,j)))\n";
  print M "    end do\n";
  print M "  invdiff(i,i) = 0.0_WP\n";
  print M "  end do\n";
  print M "\n";
  print M "  return\n";
  print M "end subroutine get_invDij\n\n";

}else{

  # --------------------------  Use exact expression  ---------------------------- #
  print M "! ================================== !\n";
  print M "! k over epsilon from transport file !\n";
  print M "! ================================== !\n";
  print M "subroutine get_kovereps\n";
  print M "  use mechanism\n";
  print M "  implicit none\n";
  print M "\n";
  print M "  ! k over epsilon from transport\n";
  print M "\n";
  for (my$i=0;$i<=$nspec;$i++) {
    print M "  koveps(g".$speciesX[$i].") = ".($KoEps{$species[$i]})."_WP\n";
  }
  print M "\n";
  print M "  print*, 'WARNING: approximate expression for lambda used'\n";
  print M "  return\n";
  print M "end subroutine get_kovereps\n\n";


  print M "! ================================ !\n";
  print M "! Omega_coeffs (k over epsilon^2)  !\n";
  print M "! ================================ !\n";
  print M "subroutine get_Ocoeff\n";
  print M "  use mechanism\n";
  print M "  implicit none\n";
  print M "\n";
  print M "  ! constant part of omega expression from transport\n";
  print M "\n";
  for (my$i=0;$i<=$nspec;$i++) {
    for (my$j=0;$j<=$nspec;$j++) {
      print M "  Ocoeffs(g".$speciesX[$i].",g".$speciesX[$j].") = ".(sqrt($KoEps{$species[$i]}*$KoEps{$species[$j]}))."_WP\n";
    }
    print M "\n";
  }
  print M "\n";
  print M "  return\n";
  print M "end subroutine get_Ocoeff\n\n";

  
  print M "! ========================== !\n";
  print M "! Constant part of viscosity !\n";
  print M "! ========================== !\n";
  print M "subroutine get_mucoeff\n";
  print M "  use mechanism\n";
  print M "  implicit none\n";
  print M "\n";
  print M "  ! Constant part in viscosity expression \n";
  print M "\n";
  for (my$i=0;$i<=$nspec;$i++) {
    print M "  mucoeff(g".$speciesX[$i].") = ".($mucoeff[$i])."_WP\n";
  }
  print M "\n";
  print M "  return\n";
  print M "end subroutine get_mucoeff\n\n";
  
  print M "! ======================== !\n";
  print M "! Pure component viscosity !\n";
  print M "! ======================== !\n";
  print M "subroutine get_viscosity( mu, T )\n";
  print M "  use mechanism\n";
  print M "  implicit none\n";
  print M "\n";
  print M "  ! Viscosity array \n  real(WP), dimension(nspec) :: mu\n";
  print M "  ! Temperature \n  real(WP) :: T\n";
  print M "  integer :: i\n";
  print M "\n";
  print M "  do i=1,nspec\n";
  print M "    mu(i) = mucoeff(i)*sqrt(T)/omegamu(T*koveps(i))\n";
  print M "  end do\n";
  print M "\n";
  print M "  return\n";
  print M "end subroutine get_viscosity\n\n";

  print M "! =========================== !\n";
  print M "! Pure component conductivity !\n";
  print M "! =========================== !\n";
  print M "subroutine get_conductivity( lambda, T, mu, Cp, W )\n";
  print M "  use mechanism\n";
  print M "  implicit none\n";
  print M "\n";
  print M "  ! Conductivity array\n  real(WP), dimension(nspec) :: lambda\n";
  print M "  ! Temperature\n  real(WP) :: T\n";
  print M "  ! Viscosity and co.\n  real(WP), dimension(nspec) :: mu, Cp, W\n";
  print M "\n";
  print M "  lambda = mu*(Cp+1.2_WP*".$Rcst."_WP/W)\n";
  print M "\n";
  print M "  return\n";
  print M "end subroutine get_conductivity\n\n";

  
  print M "! ============================================== !\n";
  print M "! Constant part of binary diffusion coefficients !\n";
  print M "! ============================================== !\n";
  print M "subroutine get_Dcoeff\n";
  print M "  use mechanism\n";
  print M "  implicit none\n";
  print M "\n";
  for (my$i=0;$i<=$nspec;$i++) {
    for (my$j=0;$j<=$nspec;$j++) {
      print M "  Dcoeffs(g".$speciesX[$i].",g".$speciesX[$j].") = ".($Dcoeff[$i][$j])."_WP\n";
    }
    print M "\n";
  }
  print M "\n";
  print M "  return\n";
  print M "end subroutine get_Dcoeff\n\n";
  
  print M "! ======================================== !\n";
  print M "! Inverse of binary diffusion coefficients !\n";
  print M "! ======================================== !\n";
  print M "subroutine get_invDij( invDij, T, P )\n";
  print M "  use mechanism\n";
  print M "  implicit none\n";
  print M "\n";
  print M "  ! Diffusion coefficients array \n  real(WP), dimension(nspec,nspec) :: invDij\n";
  print M "  ! Temperature \n  real(WP) :: T\n";
  print M "  ! Pressure \n  real(WP) :: P\n";
  print M "  ! Mix \n  real(WP) :: TPterm\n";
  print M "  integer :: i,j\n";
  print M "\n";
  print M "  ! Pressure and temperature dependent term\n";
  print M "  TPterm = P/(T*sqrt(T))\n";
  print M "  do i=1,nspec\n";
  print M "    do j=1,i-1\n";
  print M "      invDij(i,j) = TPterm*omegaD(T*Ocoeffs(i,j))/Dcoeffs(i,j)\n";
  print M "      invDij(j,i) = invDij(i,j)\n"; 
  print M "    end do\n";
  print M "    invDij(i,i) = 0.0_WP\n";
  print M "  end do\n";
  print M "\n";
  print M "  return\n";
  print M "end subroutine get_invDij\n\n";
}


# ============================================================================== #
# Analytical jacobian 
# ============================================================================== #

print M "! ---------------------------------------------------------------- !\n";
print M "! =================== !\n";
print M "! Analytical jacobian !\n";
print M "! =================== !\n";
print M "subroutine get_jacobian( c, Tloc, pr, w, k, M, rho, cpmix, h, cp, mm, pressure, jac)\n";
print M "  use mechanism\n";
print M "  implicit none\n";
print M "\n";
print M "  real(WP), dimension(nspec) :: c,pr,h,mm,cp\n";
print M "  real(WP), dimension(nreac) :: k,wc,w\n";
print M "  real(WP), dimension(nspec+1,nspec+1) :: jac\n";
print M "  real(WP), dimension(".((keys%M)+1).") :: M\n";
print M "  real(WP) :: Tloc,cpmix,dtmp,rho,pressure\n";
print M "  integer :: i\n";
print M "\n";

# --- Initialize --- !
print M "  ! --- Initialize jacobian --- !\n";
print M "  jac = 0.0_WP\n\n";

# --------------------------------------- #
# d(omega)/d(Y): nspec*nspec matrix
print M "  ! --- Jacobian: d(omega)/d(Y) --- !\n";
for (my$i=0;$i<=$nspec;$i++) {
  my $flagp = 0;
  for (my$j=0;$j<=$nspec;$j++) {

     # --- d(omega_i)/d(Y_j) --- #
    my $line = "  jac(".($i+1).",".($j+1).") = ";
    $flag[$i][$j] = 0;
    for (my$k=0;$k<=$nreac;$k++) {
      my $sign = 0;
      
      # Check if/where species i appears in reaction k
      if (&is_in_list($species[$i],\@{$reaction[$k][1]})) {$sign -= 1}
      if (&is_in_list($species[$i],\@{$reaction[$k][2]})) {$sign += 1}
      
      # If species i is not in reaction k, or appears as third body, skip to next reaction
      if ($sign == 0) {next}

      # Check if species j appears in the reactants of reaction k
      if (&is_in_list($species[$j],\@{$reaction[$k][1]})) {
	# Check if species acts as thirdbody
	if (&is_in_list($species[$j],\@{$reaction[$k][2]})){next}
	# Sign of contribution
	if ($flag[$i][$j]>0 && $sign>0) {$line .= " + "}
	elsif ($sign<0) {$line .= " - "}
	# rate coefficient with appropriate stoichiometric coefficient
	my $myi;
	if ($sign<0) {
	  # Species i is a reactant
	  $myi = &find_in_list($species[$i],\@{$reaction[$k][1]});
	  if ($reaction[$k][16][$myi]==1) {
	    $line .= "k(r".$reaction[$k][0].")";
	  }else{
	    $line .= "$reaction[$k][16][$myi]_WP * k(r".$reaction[$k][0].")";
	  }
	}else{
	  # Species i is a product
	  $myi = &find_in_list($species[$i],\@{$reaction[$k][2]});
	  if ($reaction[$k][17][$myi]==1) {
	    $line .= "k(r".$reaction[$k][0].")";
	  }else{
	    $line .= "$reaction[$k][17][$myi]_WP * k(r".$reaction[$k][0].")";
	  }
	}
	for (my$n=0;$n<=$#{$reaction[$k][1]};$n++){
	  # Species j
	  if ($species[$j] eq $reaction[$k][1][$n]) {
	    # Species j appears with nu=1
	    if ($reaction[$k][16][$n]==1){next}
	    # Species j appears with nu=2
	    elsif ($reaction[$k][16][$n]==2){$line .= " * 2.0_WP * c(g".&repsymb($reaction[$k][1][$n]).")"}
	    # Species j appears with nu!=1or2
	    else {$line .= " * $reaction[$k][16][$n]_WP * c(g".&repsymb($reaction[$k][1][$n]).")**$reaction[$k][16][$n]_WP"}
	  }else{
	    # Other species are treated as usual
	    if (exists $M{$reaction[$k][1][$n]}) {
	      if ($reaction[$k][6]==0) {
		$line .= " * m(m".&repsymb($reaction[$k][1][$n]).")";
	      }
	    } else {
	      $line .= " * c(g".&repsymb($reaction[$k][1][$n]).")";
	      if ($reaction[$k][16][$n]!=1) {
		$line .= "**$reaction[$k][16][$n]_WP ";
	      }
	    }
	  }
	}
	$flag[$i][$j] += 1;
	$flagp = 1;
	if ($flag[$i][$j]>50){$line .= "\n  jac(".($i+1).",".($j+1).") = jac(".($i+1).",".($j+1).")";$flag[$i][$j] = 1}
      }
    }
    if ($flag[$i][$j]>0){
      print M "$line \n";
      print M "  jac(".($i+1).",".($j+1).") = mm(g".$speciesX[$i].")/mm(g".$speciesX[$j].")*jac(".($i+1).",".($j+1).")\n\n";
    }
  }
}

# --------------------------------------- #
# d(omegaT)/d(Y_j)
print M "  ! --- Jacobian: d(omegaT)/d(Y) --- !\n";
print M "  do i=1,nspec\n";
print M "    jac(".($nspec+2).",i) = -1.0_WP/cpmix*sum(h*jac(1:nspec,i))\n";
print M "  end do\n";
print M "  \n";



# --------------------------------------- #
# Rate coefficient derivatives
print M "  ! ---  Rate coefficient derivatives --- !\n";
print M "  wc = 0.0_WP\n  dtmp = 8.314_WP*Tloc**2.0_WP\n";
# Loop through all reactions
for (my$k=0;$k<=$nreac;$k++) {

  # Compute derivative of reaction coefficient
  my $ka = "$reaction[$k][3]";
  my $kn = "$reaction[$k][4]";
  my $kE = "$reaction[$k][5]";
  $dkdTcoeff = "(";
  if ($kn!=0.0) {$dkdTcoeff .=  "(".$kn."_WP)/Tloc"}
  if ($kE!=0.0) {
    if ($kn!=0.0) {$dkdTcoeff .= " +"}
    $dkdTcoeff .= " (".$kE."_WP)/dtmp";
  }
  $dkdTcoeff .= ")";
  if ($kn!=0.0 || $kE!=0.0) {print M "  wc(r".$reaction[$k][0].") = $dkdTcoeff * w(r".$reaction[$k][0].") \n"}

}
print M "\n";

# --------------------------------------- #
# d(omegaY)/d(T)
print M "  ! --- Jacobian: d(omegaY)/d(T) --- !\n";
@cdot = ();
@ncdot = ();
for (my $i=0;$i<=$nspec;$i++) {
  $ncdot[$i] = 0;
}
for (my $i=0;$i<=$nspec;$i++) {
  $cdot[$i] = "  jac(g".$speciesX[$i].",".($nspec+2).") = 0.0_WP ";
}
for (my $i=0;$i<=$nreac;$i++) {

  # Reactants
  for (my $j=0;$j<=$#{$reaction[$i][1]};$j++) {
    if ($reaction[$i][1][$j]=~/^M/) {
      next;
    }
    $cdot[$link{$reaction[$i][1][$j]}] .= "-";
    if ($reaction[$i][16][$j]!=1) {
      $cdot[$link{$reaction[$i][1][$j]}] .= $reaction[$i][16][$j]."_WP*";
    }
    $cdot[$link{$reaction[$i][1][$j]}] .= "wc(r".$reaction[$i][0].")";

    $ncdot[$link{$reaction[$i][1][$j]}]++;
    if ($ncdot[$link{$reaction[$i][1][$j]}]>=50) {
      $cdot[$link{$reaction[$i][1][$j]}].="\n  jac(g".$speciesX[$link{$reaction[$i][1][$j]}].",".($nspec+2).") = jac(g".$speciesX[$link{$reaction[$i][1][$j]}].",".($nspec+2).") ";
      $ncdot[$link{$reaction[$i][1][$j]}]=0
    }

  }
  # Products
  for (my $j=0;$j<=$#{$reaction[$i][2]};$j++) {
    if ($reaction[$i][2][$j]=~/^M/) {
      next;
    }
    $cdot[$link{$reaction[$i][2][$j]}] .= "+";
    if ($reaction[$i][17][$j]!=1) {
      $cdot[$link{$reaction[$i][2][$j]}] .= $reaction[$i][17][$j]."_WP*";
    }
    $cdot[$link{$reaction[$i][2][$j]}] .= "wc(r".$reaction[$i][0].")";

    $ncdot[$link{$reaction[$i][2][$j]}]++;
    if ($ncdot[$link{$reaction[$i][2][$j]}]>=50) {
      $cdot[$link{$reaction[$i][2][$j]}].="\n  jac(g".$speciesX[$link{$reaction[$i][2][$j]}].",".($nspec+2).") = jac(g".$speciesX[$link{$reaction[$i][2][$j]}].",".($nspec+2).") ";
      $ncdot[$link{$reaction[$i][2][$j]}]=0
    }
  }
}
for (my $i=0;$i<=$nspec;$i++) {
  $cdot[$i] .= "\n\n";
}

# --- Expressions for production rates --- !
for (my $i=0;$i<=$nspec;$i++) {
  print M "$cdot[$i]";
}
print M "  ! Go back to mass basis\n";
print M "  jac(1:nspec,".($nspec+2).") = jac(1:nspec,".($nspec+2).") * mm/rho\n\n";

# --------------------------------------- #
# d(omegaY)/d(T): 
print M "  ! --- Jacobian: d(omegaT)/d(T) --- !\n";
print M "  jac(".($nspec+2).",".($nspec+2).") = -1.0_WP/cpmix * sum(cp*pr(1:nspec)) + sum(h*jac(1:nspec,".($nspec+2)."))\n";
print M "\n";
print M "  return\n";
print M "end subroutine get_jacobian\n";

close (M);


if ($printTHERMOFM==1) {
  # ======================================================================================= #
  # Thermo and trans data files cleaned up for FlamMaster
  # ======================================================================================= #
  # Re-open thermo file
  open TH, "<$thermofile" or die "Error opening thermo file $thermofile: $!";
  @thermo = <TH>;
  close(TH);
  my $outfile = "NEW".$thermofile;
  open OUT,">$outfile";

  # Process header
  while ($thermo[0]=~/^\s*!|^\s*\#/i){shift @thermo}
  if ($thermo[0]!~/THERMO/) {
    die "Check start of thermo file. Should read THERMO (comments allowed)";
  }
  my $h = shift @thermo;
  print OUT $h;
  while ($thermo[0]=~/^\s*!|^\s*\#/i) {
    shift @thermo;
  }
  if ($thermo[0]!~/[\d\.\s]+/) {
    die "Check default T ranges (comments allowed)";
  }
  {
    my $tmp = shift @thermo;
    print OUT "$tmp";
    my @tmp = split /\s+/, &trim($tmp);
    @tmp = &trim(@tmp);
    #$TminD = $tmp[0];
    $TmedD = $tmp[1];
    #$TmaxD = $tmp[2];
  }

  # Process thermo data
  my $nspecTH = 0;
  $ith = 0;
  while ($ith<=$#thermo) {

    # Check if end of file
    if ($thermo[$ith]=~/END/i) {
      last;
    }

    # Valid first line for species should have "1" at position 80
    if (length($thermo[$ith])<80) {
      next;
    }
    my $check = substr($thermo[$ith], 79, 1);
    if ($check!=1) {
      $ith++;next;
    }
  
    # Convert to all uppercase
    $thermo[$ith] =~ tr/a-z/A-Z/;
  
    # Store first line
    my $l1 = $thermo[$ith];

    # Find next 3 lines: should have 2,3, and 4 at position 80
    my ($l2,$l3,$l4);
    my $done = 0;
    while ($done!=1) {
      $ith++;
      if (length($thermo[$ith])<80) {
	next;
      }
      my $check = substr($thermo[$ith], 79, 1);
      if ($check==2) {
	$l2 = $thermo[$ith];
      }
      if ($check==3) {
	$l3 = $thermo[$ith];
      }
      if ($check==4) {
	$l4 = $thermo[$ith];$done=1;
      }
      if ($check==1) {
	die "Problem finding all lines corresponding to:\n  $l1\n";
      }
    }
    $ith++;
 
    # Species name
    my @il1 = split /\s+/,$l1;
    @il1 = &trim(@il1);
    $speciesTH{$il1[0]} = $nspecTH;
  
    # Composition
    my (@at,@nt);
    $at[0] = substr($l1,24,2);
    $nt[0] = substr($l1,26,3);
    $at[1] = substr($l1,29,2);
    $nt[1] = substr($l1,31,3);
    $at[2] = substr($l1,34,2);
    $nt[2] = substr($l1,36,3);
    $at[3] = substr($l1,39,2);
    $nt[3] = substr($l1,41,3);
    @at = &trim(@at);
    @nt = &trim(@nt);
    my @composition;
    for (my $j=0;$j<=$#atom_mass;$j++) {
      $composition[$j] = 0.0;
    }
    for (my $j=0;$j<=3;$j++) {
      if ($at[$j]!~/[A-Z]/) {
	next;
      }
      if (!exists $atoms{$at[$j]}) {
	print "Warning: atom $at[$j] for species $il1[0] is unknown\n";
      } else {
	$composition[$atoms{$at[$j]}] = $nt[$j];
      }
    }
  
    # Specific temperature ranges
    my $Tstring = substr($l1,45,30);
    my @Tr = split /\s+/,&trim($Tstring);
    $Tmed{$il1[0]} = $Tr[2];	# Only Tmed is used
  
    # Molar mass
    $WTH{$il1[0]} = 0.0;
    for (my $j=0;$j<=$#atom_mass;$j++) {
      $WTH{$il1[0]}+=$atom_mass[$j]*$composition[$j];
    }
  
    # Thermo coefficients
    my $index = $nspecTH;
    $Ahi[$index][1]=sprintf("%.8e",substr($l2,0,15));
    $Ahi[$index][2]=sprintf("%.8e",substr($l2,15,15));
    $Ahi[$index][3]=sprintf("%.8e",substr($l2,30,15));
    $Ahi[$index][4]=sprintf("%.8e",substr($l2,45,15));
    $Ahi[$index][5]=sprintf("%.8e",substr($l2,60,15));
    $Ahi[$index][6]=sprintf("%.8e",substr($l3,0,15));
    $Ahi[$index][7]=sprintf("%.8e",substr($l3,15,15));
    $alo[$index][1]=sprintf("%.8e",substr($l3,30,15));
    $alo[$index][2]=sprintf("%.8e",substr($l3,45,15));
    $alo[$index][3]=sprintf("%.8e",substr($l3,60,15));
    $alo[$index][4]=sprintf("%.8e",substr($l4,0,15));
    $alo[$index][5]=sprintf("%.8e",substr($l4,15,15));
    $alo[$index][6]=sprintf("%.8e",substr($l4,30,15));
    $alo[$index][7]=sprintf("%.8e",substr($l4,45,15));
    # Store composition
    #@{$compo[$index]} = @composition;
  
    # Increment species counter
    $nspecTH++;  

    # Print in new file if species is in mechanism
    if (&is_in_list($il1[0],\@species)) {
      print OUT $l1;
      print OUT $l2;
      print OUT $l3;
      print OUT $l4;
    }
  }
  print OUT "END\n";
}



# ======================================================================================= #
# Subroutines
# ======================================================================================= #

# -------------------------------------------- #
# Collision integral omega_2,2* (omega_mu)
# -------------------------------------------- #
sub omega_mu {

  # Reduced temperature T*=T*K/eps
  my $Ts = $_[0];
  my $m1=3.3530622607;
  my $m2=2.53272006;
  my $m3=2.9024238575;
  my $m4=0.11186138893;
  my $m5=0.8662326188;		# = -0.1337673812 + 1.0
  my $m6=1.3913958626;
  my $m7=3.158490576;
  my $m8=0.18973411754;
  my $m9=0.00018682962894;
  my $omu=($m1+$Ts*($m2+$Ts*($m3+$Ts*$m4)))/($m5+$Ts*($m6+$Ts*($m7+$Ts*($m8+$Ts*$m9))));

  return $omu;
}

# -------------------------------------------- #
# Collision integral omega_1,1* (omega_D)
# -------------------------------------------- #
sub omega_D {

  # Reduced temperature T*=T*K/eps
  my $Ts = $_[0];
  my $m1 = 6.8728271691;
  my $m2 = 9.4122316321;
  my $m3 = 7.7442359037;
  my $m4 = 0.23424661229;
  my $m5 = 1.45337701568;	# = 1.0 + 0.45337701568 
  my $m6 = 5.2269794238;
  my $m7 = 9.7108519575;
  my $m8 = 0.46539437353;
  my $m9 = 0.00041908394781;
  my $oD = ($m1+$Ts*($m2+$Ts*($m3+$Ts*$m4)))/($m5+$Ts*($m6+$Ts*($m7+$Ts*($m8+$Ts*$m9))));

  return $oD;
}
# -------------------------------------------- #
# Remove one element in a list
# -------------------------------------------- #
sub rm_elem {
    
  my $elem = $_[0];
  my @list = @{$_[1]};
  my @newList;

  foreach (@list) {
    if ($_ ne $elem) {
      push @newList,$_;
    }
  }
    
  return @newList;
}

# -------------------------------------------- #
# Remove all duplicates from a list
# -------------------------------------------- #
sub rm_duplicates {
    
  my @list = @{$_[0]};
  my %seen = ();
  my @uniq;

  foreach my $item (@list) {
    push (@uniq, $item) unless $seen{$item}++;
  }
  return @uniq;
}

# --------------------------------------------------------- #
# Remove all species starting by M (thirdbody) in a list
# --------------------------------------------------------- #
sub rm_M {
    
  my @list = @{$_[0]};
  my @uniq;

  foreach my $item (@list) {
    push (@uniq, $item) unless ($item=~/^M/);
  }
  return @uniq;
}

# --------------------------------------------------------- #
# Removes spaces before and after each element of the list 
# --------------------------------------------------------- #
sub trim {

  my @out = @_;
  for (@out) {
    s/^\s+//;
    s/\s+$//;
  }
  return wantarray ? @out : $out[0];
}

# -------------------------------------------- #
# Returns maximum value found in a list
# -------------------------------------------- #
sub maxval {

  my ($max_so_far) = shift @_;
  eval{
    foreach (@_) {
      if ($_ > $max_so_far) {
	$max_so_far = $_;
      }
    }
  };
  if ($@) {
    confess "$@";
  }
  return $max_so_far;
}

# ------------------------------------------------------------ #
# Replaces * by D, - by X for fortran variable names
# ------------------------------------------------------------ #
sub repsymb { 
  my @out = @_;
  for (@out) {
    s/\-/X/g;
    s/\*/D/g;
    s/\(|\)/J/g;
  }
  return wantarray ? @out : $out[0];
}

# ------------------------------------------------------------ #
# Add X to species whose name starts with number
# ------------------------------------------------------------ #
sub firstchar { 
  my @out = @_;
  for (@out) {
    if ($_=~/^\d/){$_="X".$_}
  }
  return wantarray ? @out : $out[0];
}

# -------------------------------------------- #
# Returns sum of elements in a list
# -------------------------------------------- #
sub mysum {

  my @list = @_;
  my $sum = 0;
  foreach (@list) {
    $sum+=$_;
  }
  return $sum;
}

# -------------------------------------------- #
# Compute free enthalpy of a species
# -------------------------------------------- #
sub free_enthalpy {

  my $T = $_[0];
  my @a = @{$_[1]};

  my $mu = 0.0;
  $mu = $a[1] * ( 1.0 - log( $T ) ) + $a[6] / $T - $a[7];
  $mu -= 0.5 * $T * ( $a[2] + $T * ( $a[3] / 3.0 + $T * ( $a[4] / 6.0 + 0.1 * $T * $a[5] ) ) );
  $mu *= $Rcst * $T;
    
  return $mu;
}

# -------------------------------------------- #
# Returns 1 if element is in list
# -------------------------------------------- #
sub is_in_list{

    my $element = $_[0];
    my @list = @{$_[1]};
    my %list;
    
    foreach (@list) {$list{$_}++};
    return exists $list{$element};
}

sub find_in_list{

    ############################################################
    # Take : $element @list                                    #
    #        return location of $element in the list, die else #
    ############################################################

    my $element = $_[0];
    my @list = @{$_[1]};

    for (my $i=0;$i<=$#list;$i++) {
	if ($list[$i] eq $element){
	    return $i;
	}
    }
    confess "Couldn't find element \#$element\# in list:\n@list\n";
}





