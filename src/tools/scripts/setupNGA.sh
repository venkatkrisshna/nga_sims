#!/bin/bash

# Path to NGA
NGA_DIR=~/NGA

# Install path for prerequisites
PRE_DIR=~/opt

# URL to download prerequisites (Use the .tar.gz or .tgz file types)
LAPACK='http://www.netlib.org/lapack/lapack-3.5.0.tgz'
FFTW='http://www.fftw.org/fftw-3.3.3.tar.gz'
HDF5='http://www.hdfgroup.org/ftp/HDF5/current/src/hdf5-1.8.12.tar.gz'
SZIP='http://www.hdfgroup.org/ftp/lib-external/szip/2.1/src/szip-2.1.tar.gz'
SILO='https://wci.llnl.gov/codes/silo/silo-4.9.1/silo-4.9.1.tar.gz'
OPENMPI='http://www.open-mpi.org/software/ompi/v1.4/downloads/openmpi-1.4.5.tar.gz'
HYPRE='http://computation.llnl.gov/casc/hypre/download/hypre-2.7.0b.tar.gz'
