#!/bin/sh

# This is a naive first version of an automatic script
# for installing all libraries required by NGA.
# For now, the integration will openmpi enabled compilers
# is essentially wrong...

# Define build directory
DIR="/ccs/home/desjardi1/Builds"

# Define compilers
export CC="cc"
export CPP="cc -E"
export CXX="CC"
export FC="ftn"
export F77="ftn"
export FFLAGS="-O3"
export FCFLAGS="-O3"

# Define library names - needs to be updated regularly!
OPENMPI=openmpi-1.6.5
FFTW=fftw-3.3.3
HYPRE=hypre-2.9.0b
LAPACK=lapack-3.4.2
HDF5=hdf5-1.8.11
SZIP=szip-2.1
SILO=silo-4.9.1

# Download all needed libraries - needs to be updated regularly!
wget http://www.open-mpi.org/software/ompi/v1.6/downloads/$OPENMPI.tar.gz
wget http://www.fftw.org/$FFTW.tar.gz
wget http://computation.llnl.gov/casc/hypre/download/$HYPRE.tar.gz
wget http://www.netlib.org/lapack/$LAPACK.tgz
wget http://www.hdfgroup.org/ftp/HDF5/current/src/$HDF5.tar.gz
wget http://www.hdfgroup.org/ftp/lib-external/szip/2.1/src/$SZIP.tar.gz
wget https://wci.llnl.gov/codes/silo/silo-4.9.1/$SILO.tar.gz

# Untar all libraries
tar zxvf $OPENMPI.tar.gz
tar zxvf $FFTW.tar.gz
tar zxvf $HYPRE.tar.gz
tar zxvf $LAPACK.tgz
tar zxvf $HDF5.tar.gz
tar zxvf $SZIP.tar.gz
tar zxvf $SILO.tar.gz

# Create directories
mkdir openmpi
mkdir fftw
mkdir hypre
mkdir lapack
mkdir hdf5
mkdir szip
mkdir silo

# Compile OPENMPI
cd $DIR/$OPENMPI
./configure --prefix=$DIR/openmpi --enable-mpi-f90 | tee myconf.log
make all
make install

# Compile FFTW
cd $DIR/$FFTW
./configure --enable-mpi --prefix=$DIR/fftw | tee myconf.log
make all
make install

# Compile HYPRE
cd $DIR/$HYPRE/src
./configure --prefix=$DIR/hypre | tee myconf.log
make all
make install

# Compile HDF5
cd $DIR/$HDF5
./configure --prefix=$DIR/hdf5 | tee myconf.log
make all
make install

# Compile SZIP
cd $DIR/$SZIP
./configure --prefix=$DIR/szip | tee myconf.log
make all
make install

# Compile SILO
cd $DIR/$SILO
./configure --enable-fortran --with-hdf5=$DIR/hdf5/include,$DIR/hdf5/lib --with-szlib=$DIR/szip --prefix=$DIR/silo --disable-silex | tee myconf.log
make all
make install 

# Compile LAPACK
echo "DO LAPACK YOURSELF!"
