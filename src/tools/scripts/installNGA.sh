#!/bin/bash

# Get the setup information
source setupNGA.sh
if [ $? -ne 0 ]; then
  echo 'Put "installNGA.sh" in the same directory as "setupNGA.sh"'
  exit 1
fi

# Download all needed libraries
if [ -d "$PRE_DIR" ]; then
  cd $PRE_DIR
else
  mkdir $PRE_DIR
  cd $PRE_DIR
fi
mkdir src
mkdir tarballs
cd tarballs
# Use 'curl' if there is no 'wget' (for MACs).
if [ -z $(which wget) ]; then
  wget () { curl -sO "$@" ; }
fi

# Dowload the source
echo 'Dowloading prerequisites.'
wget $LAPACK
wget $FFTW
wget $HDF5
wget $SZIP
wget $SILO
wget $OPENMPI
wget $HYPRE

# Untar all libraries
echo 'Extracting the source.'
tar -zxC $PRE_DIR/src/ -f *lapack*
tar -zxC $PRE_DIR/src/ -f *fftw*
tar -zxC $PRE_DIR/src/ -f *hdf5*
tar -zxC $PRE_DIR/src/ -f *szip*
tar -zxC $PRE_DIR/src/ -f *silo*
tar -zxC $PRE_DIR/src/ -f *openmpi*
tar -zxC $PRE_DIR/src/ -f *hypre*

# Create directories for the library, bin, etc... files
mkdir $PRE_DIR/lapack
mkdir $PRE_DIR/fftw
mkdir $PRE_DIR/hdf5
mkdir $PRE_DIR/szip
mkdir $PRE_DIR/silo
mkdir $PRE_DIR/openmpi
mkdir $PRE_DIR/hypre

# Define the compilers
export CC=gcc
export CXX=g++
export F77=gfortran
export CFLAGS="-O3 -ftree-vectorize"
export CXXFLAGS="-O3 -ftree-vectorize"
export FFLAGS="-O3 -ftree-vectorize"

# Define a shortcut with 'sed'. Equivalent to 'sed -i' on non-MAC systems.
sedi () { sed "$@" > foo.tmp; mv foo.tmp $2; }

# Compile Lapack
cd $PRE_DIR/src/*lapack*
echo 'Compiling LAPACK/BLAS.'
cp make.inc.example make.inc
sedi 's/FORTRAN.*=.*/FORTRAN = gfortran/g' make.inc
sedi 's/OPTS.*=.*/OPTS = -O3 -ftree-vectorize/g' make.inc
sedi 's/DRVOPTS.*=.*/DRVOPTS = $(OPTS)/g' make.inc
sedi 's/NOOPT.*=.*/NOOPT = /g' make.inc
sedi 's/LOADER.*=.*/LOADER = gfortran/g' make.inc
sedi 's/LOADOPTS.*=.*/LOADOPTS = -O3 -ftree-vectorize/g' make.inc
sedi 's/BLASLIB.*=.*/BLASLIB = \.\.\/\.\.\/libblas\.a/g' make.inc
sedi 's/LAPACKLIB.*=.*/LAPACKLIB = liblapack.a/g' make.inc
make blaslib
if [ $? -ne 0 ]; then
  echo 'Failed to compile BLAS.'
  exit 1
fi
make lapacklib
if [ $? -ne 0 ]; then
  echo 'Failed to compile LAPACK.'
  exit 1
fi
cp libblas.a $PRE_DIR/lapack
cp liblapack.a $PRE_DIR/lapack

# Compile and install FFTW
cd $PRE_DIR/src/*fftw*
echo 'Compiling FFTW'
./configure CC=gcc CXX=g++ F77=gfortran FC=gfortran --prefix=$PRE_DIR/fftw/
if [ $? -ne 0 ]; then
  echo 'Failed to configure FFTW.'
  exit 1
fi
make
if [ $? -ne 0 ]; then
  echo 'Failed to compile FFTW.'
  exit 1
fi
make install

# Compile and install HDF5
echo 'Compiling HDF5'
cd $PRE_DIR/src/*hdf5*
./configure --prefix=$PRE_DIR/hdf5
if [ $? -ne 0 ]; then
  echo 'Failed to configure HDF5.'
  exit 1
fi
make
if [ $? -ne 0 ]; then
  echo 'Failed to compile HDF5.'
  exit 1
fi
make install

# Compile and install SZIP
cd $PRE_DIR/src/*szip*
echo 'Compiling SZIP'
./configure --prefix=$PRE_DIR/szip
if [ $? -ne 0 ]; then
  echo 'Failed to configure SZIP.'
  exit 1
fi
make
if [ $? -ne 0 ]; then
  echo 'Failed to compile SZIP.'
  exit 1
fi
make install

# Compile and install SILO
cd $PRE_DIR/src/*silo*
echo 'Compiling SILO'
./configure --enable-fortran --without-zlib --with-hdf5=$PRE_DIR/hdf5/include/,$PRE_DIR/hdf5/lib --with-szlib=$PRE_DIR/szip/ --prefix=$PRE_DIR/silo/ --disable-silex
if [ $? -ne 0 ]; then
  echo 'Failed to configure SILO.'
  exit 1
fi
make
if [ $? -ne 0 ]; then
  echo 'Failed to compile SILO.'
  exit 1
fi
make install

# Compile and install OPENMPI
cd $PRE_DIR/src/*open*
echo 'Compiling Open MPI'
./configure CC=gcc CXX=g++ F77=gfortran FC=gfortran --prefix=$PRE_DIR/openmpi/
if [ $? -ne 0 ]; then
  echo 'Failed to configure OpenMPI.'
  exit 1
fi
make
if [ $? -ne 0 ]; then
  echo 'Failed to compile OpenMPI.'
  exit 1
fi
make install

# Compile and install Hypre
export CC=$PRE_DIR/openmpi/bin/mpicc
export CXX=$PRE_DIR/openmpi/bin/mpicxx
export F77=$PRE_DIR/openmpi/bin/mpif90
export CFLAGS="-O3 -ftree-vectorize"
export CXXFLAGS="-O3 -ftree-vectorize"
export FFLAGS="-O3 -ftree-vectorize"

cd $PRE_DIR/src/*hypre*/src
echo 'Compiling HYPRE'
#Add OpenMPI to path
export LD_LIBRARY_PATH=$PRE_DIR/openmpi/lib/:$LD_LIBRARY_PATH
export DYLD_LIBRARY_PATH=$PRE_DIR/openmpi/lib/:$DYLD_LIBRARY_PATH #for MACs
#./configure CC=$PRE_DIR/openmpi/bin/mpicc CXX=$PRE_DIR/openmpi/bin/mpicxx F77=$PRE_DIR/openmpi/bin/mpif90 FC=$PRE_DIR/openmpi/bin/mpif90 --prefix=$PRE_DIR/hypre
./configure CC=$PRE_DIR/openmpi/bin/mpicc CXX=$PRE_DIR/openmpi/bin/mpicxx F77=$PRE_DIR/openmpi/bin/mpif90 FC=$PRE_DIR/openmpi/bin/mpif90 --enable-fortran --prefix=$PRE_DIR/hypre
if [ $? -ne 0 ]; then
  echo 'Failed to configure Hypre.'
  exit 1
fi
make
if [ $? -ne 0 ]; then
  echo 'Failed to compile Hypre.'
  exit 1
fi
make install
echo 'Prerequisites are installed.'

# Compile NGA
cd $NGA_DIR
cd src
echo "Compiling NGA."
# Add the Prerequisites to the path
export LD_LIBRARY_PATH=$PRE_DIR/hypre/lib:$PRE_DIR/openmpi/lib/:$PRE_DIR/silo/lib:$PRE_DIR/szip/lib:$PRE_DIR/hdf5:$PRE_DIR/fftw/lib:$PRE_DIR/lapack:$LD_LIBRARY_PATH
export DYLD_LIBRARY_PATH=$PRE_DIR/hypre/lib:$PRE_DIR/openmpi/lib/:$PRE_DIR/silo/lib:$PRE_DIR/szip/lib:$PRE_DIR/hdf5:$PRE_DIR/fftw/lib:$PRE_DIR/lapack:$DYLD_LIBRARY_PATH #for MACs
# Update Makefile.in
echo "# Directories
HOMEDIR = $NGA_DIR
LIBDIR  = \$(HOMEDIR)/lib
MODDIR  = \$(HOMEDIR)/mod
OBJDIR  = \$(HOMEDIR)/obj
BINDIR  = \$(HOMEDIR)/bin
VPATH   = \$(LIBDIR) \$(BINDIR) \$(OBJDIR)

# Compiler and archiver
CC  = $PRE_DIR/openmpi/bin/mpicc
CXX = $PRE_DIR/openmpi/bin/mpicxx
F90 = $PRE_DIR/openmpi/bin/mpif90
F77 = $PRE_DIR/openmpi/bin/mpif90
LD  = $PRE_DIR/openmpi/bin/mpif90
AR  = ar rcv
RL  = ranlib

# Compiler flags
CFLAGS   =
F90FLAGS = -cpp -ffree-form -std=gnu -ffree-line-length-none
F77FLAGS = -cpp -std=legacy
LDFLAGS  =
INCFLAGS = -I \$(MODDIR)
MODFLAGS = -J \$(MODDIR)
DBGFLAGS = -g -fbacktrace -fcheck=all -pedantic -Wall -Wextra -W -Wno-unused-function -fopenmp  
OPTFLAGS = -O3 -ftree-vectorize -ffast-math -funroll-loops -fomit-frame-pointer -pipe -fopenmp

# External libraries
BLAS_DIR = $PRE_DIR/lapack
BLAS_LIB = -L\$(BLAS_DIR) -lblas
LAPACK_DIR = $PRE_DIR/lapack
LAPACK_LIB = -L\$(LAPACK_DIR) -llapack

HYPRE_DIR = $PRE_DIR/hypre
HYPRE_INC = -I\$(HYPRE_DIR)/include
HYPRE_LIB = -L\$(HYPRE_DIR)/lib -lHYPRE

FFTW_DIR = $PRE_DIR/fftw
FFTW_INC = -I\$(FFTW_DIR)/include
FFTW_LIB = -L\$(FFTW_DIR)/lib -lfftw3

HDF5_DIR = $PRE_DIR/hdf5
SZIP_DIR = $PRE_DIR/szip
SILO_DIR = $PRE_DIR/silo
SILO_INC = -I\$(SILO_DIR)/include
SILO_LIB = -L\$(SILO_DIR)/lib -lsiloh5 -L\$(HDF5_DIR)/lib -lhdf5 -L\$(SZIP_DIR)/lib -lsz -L/usr/local/lib -lstdc++ #-lz

# Installation script
INSTDIR = \$(HOME)/bin)
INSTSCPT = cp \$(BINDIR)/* \$(INSTDIR)/.
" > Makefile.in
make debug
if [ $? -ne 0 ]; then
  echo 'Failed to compile NGA.'
  exit 1
fi

# Modify .profile file so that Prerequisites will always be in path
cd
echo "export LD_LIBRARY_PATH=$PRE_DIR/hypre/lib:$PRE_DIR/openmpi/lib/:$PRE_DIR/silo/lib:$PRE_DIR/szip/lib:$PRE_DIR/hdf5:$PRE_DIR/fftw/lib:$PRE_DIR/lapack:\$LD_LIBRARY_PATH" >> .profile
echo "export DYLD_LIBRARY_PATH=$PRE_DIR/hypre/lib:$PRE_DIR/openmpi/lib/:$PRE_DIR/silo/lib:$PRE_DIR/szip/lib:$PRE_DIR/hdf5:$PRE_DIR/fftw/lib:$PRE_DIR/lapack:\$DYLD_LIBRARY_PATH" >> .profile # For MACs
cd -
echo '




      NGA is installed! Laissez les bons temps rouler!

'
