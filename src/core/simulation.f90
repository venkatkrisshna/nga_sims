module simulation
  use time_info
  implicit none

  logical :: solveNS
  
end module simulation

! ================================ !
! Initialization of the simulation !
! ================================ !
subroutine simulation_init
  use simulation
  use parser
  use partition
  implicit none

  ! Check if solving Navier-Stokes
  call parser_read('Solve NS',solveNS,.true.)
  
  ! Initialize the monitoring system
  call monitor_pre_init
  
  ! Initialize the time parameters
  call time_info_init
  
  ! Initialize IB module
  call ib_init
  
  ! Initialize LPT module
  call lpt_init
  
  ! Initialize porous model
  call bodyforce_porous_init
  
  ! Initialize PPT module
  call ppt_init
  
  ! Initialize the modules with the previous time step
  call multiphase_pre_init
  call boundary_init
  call scalar_init
  call soot_init
  call combustion_init
  call velocity_init
  call multiphase_init
    
  ! initialize the IB-lagrange module
  call ib_lagrange_init
  call ib_velocity_init
  
  ! Pre-interpolate the velocities
  call interpolate_init
  call interpolate_velocities
  call strainrate_init
  
  ! Additional modules
  call bodyforce_init
  call levelset_init
  
  ! Initialize the pressure
  call pressure_init

  ! Continuity equation
  if (solveNS) call velocity_pressure
  
  ! Initialize SGS models
  call sgsmodel_init
  
  ! Output stuff
  call stat_init
  call dump_init
  call inflow_generation_init
  
  ! Monitor the initial data
  call monitor_post_init
  call monitor_timestep
  
  ! Log
  call monitor_log("SIMULATION INITIALIZED")
  
  return
end subroutine simulation_init


! =========================== !
! Simulation time advancement !
! =========================== !
subroutine simulation_run
  use simulation
  implicit none
  
  do while (.not. done())
  
     ! Increment time information
     call predict_timestepsize
     ntime = ntime + 1
     time  = time  + dt
     
     ! Pre-step routines 0 - geometry
     call ib_update
     
     ! Pre-step routines 1 - physical properties
     call combustion_prestep
     call soot_prestep
     call sgsmodel_eddyVISC
     call sgsmodel_eddyDIFF
     call velocity_CFL_centerline
     
     ! Pre-step routines 2 - prepare the subiterations
     call levelset_prestep
     call scalar_prestep
     call velocity_prestep
     call velocity_predict_density
     call lpt_step
     call ppt_step
     call multiphase_prestep
     
     do niter=1,max_niter

        ! Interface equations
        call multiphase_step        

        ! Level set transport equations
        call levelset_step

        ! Scalar transport equations
        call scalar_step
        
        ! Equation of state
        call combustion_step
        
        ! Momentum equations
        if (solveNS) call velocity_step
        
        ! Continuity equation
        if (solveNS) call velocity_pressure
        
        ! Monitoring - per sub-iterations
        call monitor_iteration
        
     end do
     
     ! Additonal transport for EHD scalars
     call multiphase_ehd_scalar_step
     
     ! Advance soot here
     call soot_step
     
     ! Level set for combustion
     call levelset_poststep
     
     ! Monitoring - per iterations
     call monitor_timestep
     
     ! Write out data and results
     call interpolate_velocities
     call dump_statistics
     call simulation_write(.false.)
     call dump_result
     call inflow_generation_save
     
  end do
  
  return
end subroutine simulation_run


! ==================== !
! Write solution files !
! ==================== !
subroutine simulation_write(flag)
  use simulation
  implicit none
  logical, intent(in) :: flag
  
  call lpt_write(flag)
  call ppt_write(flag)
  call data_write(flag)
  call optdata_write(flag)
  
  return
end subroutine simulation_write


! ================== !
! End the simulation !
! ================== !
subroutine simulation_finalize
  use simulation
  implicit none
  
  ! Write necessary data to files
  call rpt_finalize
  call lpt_finalize
  call data_finalize
  call optdata_finalize
  
  ! Log
  call monitor_log("END OF SIMULATION")
  
  ! Finalize the monitor module
  call monitor_finalize
  
  return
end subroutine simulation_finalize
