! ========================== !
! Eulerian particle tracking !
! ========================== !

module ept
  use precision
  use partition
  use geometry
  use time_info
  use data
  use string
  implicit none

end module ept

! ============================== !
! Initialization of the Eulerian !
! particle routines              !
! ============================== !
subroutine ept_init
  use ept
  implicit none
  
  return
end subroutine ept_init

