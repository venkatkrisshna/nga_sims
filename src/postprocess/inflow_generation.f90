module inflow_generation
  use precision
  use geometry
  use partition
  use string
  implicit none
  
  ! Flag to indicate if we generate inflow profiles
  integer :: iinflow

  ! Inflow communicator
  integer :: has_inflow
  integer :: inflow_comm,inflow_irank
  
  ! Name of output and file id
  character(len=str_medium) :: filename
  integer :: ifile
  
  ! Frequency of output
  real(WP) :: inflow_freq
  
  ! Location of inflow generation
  real(WP) :: inflow_loc
  integer  :: iloc_inf
  
  ! Number of time steps and last save
  integer  :: inflow_ntime
  real(WP) :: inflow_save

  ! Velocity values at the last plane
  integer :: nvar
  character(len=str_short), dimension(:), allocatable :: names
  real(WP), dimension(:,:,:), allocatable :: buf
  
  ! View in the file
  integer :: default_view
  integer :: data_size
  integer, dimension(:), allocatable :: fileview
  
end module inflow_generation


! =================================== !
! Initialize Inflow Generation module !
! =================================== !
subroutine inflow_generation_init
  use inflow_generation
  use parallel
  use parser
  use time_info
  implicit none
  
  logical :: file_is_there,isdefined
  integer, dimension(3) :: gsizes, lsizes, start
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer, dimension(4) :: dims
  integer :: ierr,var

  ! Do we output an inflow profile?
  call parser_is_defined('Inflow file to write',isdefined)
  
  ! If not return
  iinflow = 0
  if (isdefined) iinflow = 1
  if (iinflow.eq.0) return
  
  ! Read filename and frequency
  call parser_read('Inflow file to write',filename)
  inquire(file=filename,exist=file_is_there)
  filename = trim(mpiiofs) // trim(filename)
  call parser_read('Inflow frequency',inflow_freq)
  call parser_read('Inflow location',inflow_loc,0.5_WP*xL)
  
  ! Handle inflow generation location
  if (xper.eq.1) then
     ! Generate the inflow at the end of the domain
     iloc_inf=imax
  else
     ! Generate the inflow elsewhere because of convective outflow
     if (inflow_loc.gt.xm(imax)) then
        iloc_inf=imax
     else
        iloc_inf=imin
        do while (xm(iloc_inf).lt.inflow_loc)
           iloc_inf=iloc_inf+1
        end do
     end if
     ! Do not interpolate, just take the closest point => minimize errors
  end if

  ! Split the communicator
  if (imin_.le.iloc_inf .and. imax_.ge.iloc_inf) then
     has_inflow= 1
  else 
     has_inflow= 0
  end if
  
  call MPI_COMM_SPLIT(comm,has_inflow,MPI_UNDEFINED,inflow_comm,ierr)
  call MPI_COMM_RANK(inflow_comm,inflow_irank,ierr)
  inflow_irank = inflow_irank + 1
 
  ! Only continue if proc has inflow
  if (has_inflow .ne. 1) return
  
  ! Setup the names
  nvar = 3
  allocate(names(nvar))
  names(1) = 'U'
  names(2) = 'V'
  names(3) = 'W'
  
  ! Allocate arrays
  allocate(buf(jmin_:jmax_,kmin_:kmax_,nvar))
  allocate(fileview(nvar))
  
  ! Open the file to write or create new one if none exists
      if (file_is_there) call inflow_generation_read

    if(.not.file_is_there) then
      call MPI_FILE_OPEN(inflow_comm,filename,IOR(MPI_MODE_WRONLY,MPI_MODE_CREATE), &
          mpi_info,ifile,ierr)
      ! Set headers
      inflow_save = time
      inflow_ntime = 0
      if (inflow_irank.eq.1) then
        ! Write dimensions
        dims(1) = inflow_ntime
        dims(2) = ny
        dims(3) = nz
        dims(4) = nvar
        call MPI_FILE_WRITE(ifile,dims,4,MPI_INTEGER,status,ierr)
        call MPI_FILE_WRITE(ifile,inflow_freq,1,MPI_REAL_WP,status,ierr)
        call MPI_FILE_WRITE(ifile,time,1,MPI_REAL_WP,status,ierr)
        ! Write variable names
        do var=1,nvar
            call MPI_FILE_WRITE(ifile,names(var),str_short,MPI_CHARACTER,status,ierr)
        end do
        ! Write the grid
        call MPI_FILE_WRITE(ifile,icyl,1,MPI_INTEGER,status,ierr)
        call MPI_FILE_WRITE(ifile,y(jmin:jmax+1),ny+1,MPI_REAL_WP,status,ierr)
        call MPI_FILE_WRITE(ifile,z(kmin:kmax+1),nz+1,MPI_REAL_WP,status,ierr)
      end if
    end if

    ! Global sizes
    gsizes(1) = ny
    gsizes(2) = nz
    gsizes(3) = nvar
    ! Local sizes
    lsizes(1) = ny_
    lsizes(2) = nz_
    lsizes(3) = 1
    ! Starting index
    start(1) = jmin_-jmin
    start(2) = kmin_-kmin
    ! Define the size
    data_size = lsizes(1)*lsizes(2)*lsizes(3)
    ! Predefine the view  
    do var=1,nvar
       start(3) = var - 1
       call MPI_TYPE_CREATE_SUBARRAY(3,gsizes,lsizes,start,&
            MPI_ORDER_FORTRAN,MPI_REAL_WP,fileview(var),ierr)
       call MPI_TYPE_COMMIT(fileview(var),ierr)
    end do

  return
end subroutine inflow_generation_init

! ================================= !
! Read inflow profile if it exists  !
! Allows for restarts               !
! ================================= !
subroutine inflow_generation_read
  use inflow_generation
  use parallel
  use metric_generic
  use data
  use time_info
  implicit none

  integer, dimension(4) :: dims
  integer :: ierr
  integer, dimension(MPI_STATUS_SIZE) :: status

  ! Open the file to read (needs to have same name as one being written)
  call MPI_FILE_OPEN(inflow_comm,filename,MPI_MODE_RDWR,mpi_info,ifile,ierr)
  ! Read dimensions from header
  call MPI_FILE_READ_ALL(ifile, dims, 4, MPI_INTEGER, status, ierr)

  if((dims(2) .ne. ny) .or. (dims(3) .ne. nz)) then
    print*, 'expected = ', nx, ny, nz
    print*, 'inflow generated file = ', nx, dims(2), dims(3)
    call die('inflow_generation_read: The dimensions of ny and nz do not match current simulation')
  end if

  ! Set restarting value of inflow_ntime
  inflow_ntime = dims(1)

  ! Set inflow_save to last time inflow slice was added
  inflow_save = int(inflow_ntime*inflow_freq)

  call monitor_log("GENERATING INFLOW FILE FROM EXISTING FILE")

  return
end subroutine inflow_generation_read



! ================================ !
! Dump inflow profile if necessary !
! ================================ !
subroutine inflow_generation_save
  use inflow_generation
  use parallel
  use metric_generic
  use data
  use time_info
  implicit none
  
  integer :: var,ierr
  integer(kind=MPI_OFFSET_KIND) :: disp
  integer(kind=MPI_OFFSET_KIND) :: ny_MOK, nz_MOK, WP_MOK, str_MOK, nvar_MOK, inflow_ntime_MOK
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer :: j,k

  ! If not return
  if (iinflow.eq.0) return

  ! If proc does not contain inflow return
  if (has_inflow .ne. 1) return

  ! New time step to store
  if (int(time/inflow_freq).eq.inflow_save) return
  inflow_save = int(time/inflow_freq)
  inflow_ntime = inflow_ntime + 1
  
  ! Update the number of time steps stored
  disp = 0
  call MPI_FILE_SET_VIEW(ifile,disp,MPI_INTEGER,MPI_INTEGER, &
       "native",mpi_info,ierr)
  if (inflow_irank.eq.1) then
     call MPI_FILE_WRITE(ifile,inflow_ntime,1,MPI_INTEGER,status,ierr)
  end if

  ! Compute all the quantities...
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        buf(j,k,1) = sum(interp_u_xm(iloc_inf,j,:)*U(iloc_inf-st1:iloc_inf+st2,j,k))
        buf(j,k,2) = V(iloc_inf,j,k)
        buf(j,k,3) = W(iloc_inf,j,k)
     end do
  end do

! Resize integers in display so NGA can write bigger files
  ny_MOK =              int(ny,           MPI_Offset_kind)
  nz_MOK =              int(nz,           MPI_Offset_kind)
  WP_MOK =              int(WP,           MPI_Offset_kind)
  str_MOK =             int(str_short,    MPI_Offset_kind)
  nvar_MOK =            int(nvar,         MPI_Offset_kind)
  inflow_ntime_MOK =    int(inflow_ntime, MPI_Offset_kind)

  ! Displacement
  disp = 4*4 + 2*WP_MOK + nvar_MOK*str_MOK + 4 + (ny_MOK+nz_MOK+2)*WP_MOK + (inflow_ntime_MOK-1)*nvar_MOK*ny_MOK*nz_MOK*WP_MOK

  ! Write the data
  do var=1,nvar
     call MPI_FILE_SET_VIEW(ifile,disp,MPI_REAL_WP,fileview(var), &
          "native",mpi_info,ierr)
     call MPI_FILE_WRITE_ALL(ifile,buf(:,:,var),data_size, &
          MPI_REAL_WP,status,ierr)
  end do
  
  ! Log
  call monitor_log("INFLOW PROFILE FILE WRITTEN")
  
  return
end subroutine inflow_generation_save


! ============== !
! Close the file !
! ============== !
subroutine inflow_generation_close
  use inflow_generation
  use parallel
  implicit none
  integer :: ierr
  
  ! Close the file
  call MPI_FILE_CLOSE(ifile,ierr)
  
  return
end subroutine inflow_generation_close
