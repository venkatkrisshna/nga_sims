module stat_1d
  use stat
  implicit none
  
  ! Whether Favre averages are used
  logical :: use_rho
  
  ! Sampled variables
  integer :: stat_nvar
  character(len=str_medium), dimension(:), pointer :: stat_name
  
  ! ----------------------------------------------------
  ! 1D-Y STATISTICS 
  !  -> as a function of y
  !  -> at a given station in x
  !
  ! nxloc = -1  for no station (x periodic)
  ! xloc belongs to [ x(iloc)  ,x(iloc+1)   [
  ! xloc belongs to [ xm(imloc),xm(imloc+1) [
  ! xloc = cx(iloc) *x(iloc)  + (1-cx(iloc)) *x(iloc+1)
  ! xmloc = cxm(iloc)*xm(iloc) + (1-cxm)iloc))*xm(iloc+1)
  ! -----------------------------------------------------
  logical :: allx
  integer :: nxloc
  real(WP), dimension(:),     allocatable :: xloc
  integer,  dimension(:),     allocatable :: iloc
  integer,  dimension(:),     allocatable :: iloc_ifwrite
  integer,  dimension(:),     allocatable :: imloc
  real(WP), dimension(:,:),   allocatable :: cxloc
  real(WP), dimension(:,:),   allocatable :: cxmloc
  real(WP), dimension(:,:,:), allocatable :: stat_y
  real(WP), dimension(:,:,:), allocatable :: buf_y
  real(WP) :: Delta_ty
  
  ! Spray
  real(WP), dimension(:,:,:), allocatable :: stat_y_spray
  real(WP), dimension(:,:,:), allocatable :: buf_y_spray
  real(WP) :: Delta_ty_spray
  
  ! -----------------------------------------------------
  ! 1D-X STATISTICS 
  !  -> as a function of x
  !  -> at a given station in y
  !
  ! nyloc = -1  for no station (y periodic)
  ! yloc belongs to [ y(jloc)  ,y(jloc+1)   [
  ! yloc belongs to [ ym(jmloc),ym(jmloc+1) [
  ! yloc = cy(jloc) *y(jloc)  + (1-cy(jloc)) *y(jloc+1)
  ! ymloc = cym(jloc)*ym(jloc) + (1-cym(jloc))*ym(jloc+1)
  ! -----------------------------------------------------
  integer :: nyloc
  real(WP), dimension(:),     allocatable :: yloc
  integer,  dimension(:),     allocatable :: jloc
  integer,  dimension(:),     allocatable :: jloc_ifwrite
  integer,  dimension(:),     allocatable :: jmloc
  real(WP), dimension(:,:),   allocatable :: cyloc
  real(WP), dimension(:,:),   allocatable :: cymloc
  real(WP), dimension(:,:,:), allocatable :: stat_x
  real(WP), dimension(:,:,:), allocatable :: buf_x
  real(WP) :: Delta_tx
  
contains
  
  ! ================================= !
  ! General initialization of STAT-1D !
  ! ================================= !
  subroutine stat_1d_init
    use data
    use combustion
    implicit none
    
    integer :: isc,ns
    
    ! Return if init already done
    if (associated(stat_name)) return
    
    ! Create a timer
    call timing_create('stat_1d')
    
    ! Check whether rho-stats are needed
    use_rho=.false.
    if (trim(chemistry).ne.'none') use_rho=.true.
    if (use_lpt) use_rho=.true.
    
    stat_nvar = 0
    ! Density
    if (use_rho) stat_nvar = stat_nvar+2
    ! Velocity
    stat_nvar = stat_nvar+9
    if (use_rho) stat_nvar = stat_nvar+9
    ! Scalars
    do isc=1,nscalar
       stat_nvar = stat_nvar+2
       if (use_rho) stat_nvar = stat_nvar+2
    end do
    ! Combustion
    if (trim(chemistry).eq.'diffusion') then
       stat_nvar = stat_nvar+6
    elseif (trim(chemistry).eq.'premixed') then
       stat_nvar = stat_nvar+6
    end if
    ! LPT
    if (use_lpt) stat_nvar = stat_nvar+11
    
    ! Allocate
    allocate(stat_name(stat_nvar))
    
    ns = 0
    
    ! Density statistics
    if (use_rho) then
       stat_name(ns+1) = 'RHO'
       stat_name(ns+2) = 'RHO^2'
       ns = ns+2
    end if
    
    ! Velocity statistics
    stat_name(ns+1) = 'U'
    stat_name(ns+2) = 'V'
    stat_name(ns+3) = 'W'
    stat_name(ns+4) = 'UU'
    stat_name(ns+5) = 'VV'
    stat_name(ns+6) = 'WW'
    stat_name(ns+7) = 'UV'
    stat_name(ns+8) = 'VW'
    stat_name(ns+9) = 'WU'
    ns = ns+9
    if (use_rho) then
       stat_name(ns+1) = 'rhoU'
       stat_name(ns+2) = 'rhoV'
       stat_name(ns+3) = 'rhoW'
       stat_name(ns+4) = 'rhoUU'
       stat_name(ns+5) = 'rhoVV'
       stat_name(ns+6) = 'rhoWW'
       stat_name(ns+7) = 'rhoUV'
       stat_name(ns+8) = 'rhoVW'
       stat_name(ns+9) = 'rhoWU'
       ns = ns+9
    end if
    
    ! Scalars
    do isc=1,nscalar
       stat_name(ns+1) = 'SC-'  // trim(adjustl(sc_name(isc)))
       stat_name(ns+2) = 'SC^2-'// trim(adjustl(sc_name(isc)))
       ns = ns+2
       if (use_rho) then
          stat_name(ns+1) = 'rhoS-'  // trim(adjustl(sc_name(isc)))
          stat_name(ns+2) = 'rhoS^2-'// trim(adjustl(sc_name(isc)))
          ns = ns+2
       end if
    end do
    
    ! LPT-related stats
    if (use_lpt) then
       ! Volume fraction statistics
       stat_name(ns+1) = 'eps'
       stat_name(ns+2) = 'eps^2'
       ns = ns+2
       ! Velocity statistics
       stat_name(ns+1) = 'Up'
       stat_name(ns+2) = 'Vp'
       stat_name(ns+3) = 'Wp'
       stat_name(ns+4) = 'UpUp'
       stat_name(ns+5) = 'VpVp'
       stat_name(ns+6) = 'WpWp'
       stat_name(ns+7) = 'UpVp'
       stat_name(ns+8) = 'VpWp'
       stat_name(ns+9) = 'WpUp'
       ns = ns+9
    end if
    
    return
  end subroutine stat_1d_init
  
  ! ============================== !
  ! Setup the 1D metric            !
  !  -> indices                    !
  !  -> interpolation coefficients !
  ! ============================== !
  subroutine stat_1dx_metric
    implicit none
    integer :: i,loc
    
    ! Allocate the arrays
    allocate(iloc(nxloc))
    allocate(iloc_ifwrite(nxloc))
    allocate(imloc(nxloc))
    allocate(cxloc(nxloc,2))
    allocate(cxmloc(nxloc,2))
    
    ! Locate the first i index to the left
    do loc=1,nxloc
       if (x(imin_).gt.xloc(loc) .or. x(imax_+1).le.xloc(loc)) then
          iloc(loc) = imin_
          imloc(loc) = imin_
          cxloc(loc,:) = 0.0_WP
          cxmloc(loc,:) = 0.0_WP
          iloc_ifwrite(loc) = 0
       else
          i = imin_
          do while(i.le.imax_ .and. x(i+1).le.xloc(loc))
             i = i+1
          end do
          iloc(loc) = i
          cxloc(loc,1) = (x(iloc(loc)+1)-xloc(loc))/(x(iloc(loc)+1)-x(iloc(loc)))
          cxloc(loc,2) = 1.0_WP-cxloc(loc,1)
          
          if (xloc(loc).ge.xm(i)) then
             imloc(loc) = i
          else
             imloc(loc) = i-1
          end if
          cxmloc(loc,1) = (xm(imloc(loc)+1)-xloc(loc))/(xm(imloc(loc)+1)-xm(imloc(loc)))
          cxmloc(loc,2) = 1.0_WP-cxmloc(loc,1)
          iloc_ifwrite(loc) = 1
       end if
    end do
    
    return
  end subroutine stat_1dx_metric
  
  subroutine stat_1dy_metric
    implicit none
    integer :: j,loc
    
    ! Allocate the arrays
    allocate(jloc(nyloc))
    allocate(jloc_ifwrite(nyloc))
    allocate(jmloc(nyloc))
    allocate(cyloc(nyloc,2))
    allocate(cymloc(nyloc,2))
    
    ! Locate the first j index to the bottom - cell face
    do loc=1,nyloc
       if (y(jmin_).gt.yloc(loc) .or. y(jmax_+1).le.yloc(loc)) then
          jloc(loc) = jmin_
          jmloc(loc) = jmin_
          cyloc(loc,:) = 0.0_WP
          cymloc(loc,:) = 0.0_WP
          jloc_ifwrite(loc) = 0
       else
          j = jmin_
          do while(j.le.jmax_ .and. y(j+1).le.yloc(loc))
             j = j+1
          end do
          jloc(loc) = j
          cyloc(loc,1) = (y(jloc(loc)+1)-yloc(loc))/(y(jloc(loc)+1)-y(jloc(loc)))
          cyloc(loc,2) = 1.0_WP-cyloc(loc,1)
          
          if (yloc(loc).ge.ym(j)) then
             jmloc(loc) = j
          else
             jmloc(loc) = j-1
          end if
          cymloc(loc,1) = (ym(jmloc(loc)+1)-yloc(loc))/(ym(jmloc(loc)+1)-ym(jmloc(loc)))
          cymloc(loc,2) = 1.0_WP-cymloc(loc,1)
          jloc_ifwrite(loc) = 1
       end if
    end do
    
    return
  end subroutine stat_1dy_metric
  
end module stat_1d


! ================================== !
! Initialize the 1D statistic module !
!   -> at a given x                  !
!   -> as a function of y            !
! ================================== !
subroutine stat_1dy_init
  use stat_1d
  use parallel
  use parser
  implicit none
  logical :: isdef,file_is_there
  character(len=str_medium) :: text
  
  ! General 1D non specific initialization
  call stat_1d_init
  
  ! Start the timer
  call timing_start('stat_1d')
  
  ! No stat at a given x (as a function of y) if y periodic
  if (yper.eq.1) call die('stat_1dy_init: 1D-y statistics impossible (y is periodic)')
  
  ! Any x locations ?
  call parser_is_defined('Statistics locations x',isdef)
  if (isdef) then
     call parser_read('Statistics locations x',text)
     if (trim(text).eq.'all') then
        if (xper.ne.1) call die('stat_1dy_init: all incompatible with xper')
        ! Create all locations
        allx = .true.
        nxloc = nx
        allocate(xloc(nxloc))
        xloc = xm(imin:imax)
     else
        ! Read the locations
        allx = .false.
        call parser_getsize('Statistics locations x',nxloc)
        allocate(xloc(nxloc))
        call parser_read('Statistics locations x',xloc)
     end if
  else
     call die('stat_1dy_init: Statistics locations x not specified')
  end if
  
  ! Create metric
  call stat_1dx_metric
  
  ! Allocate stat arrays
  allocate(stat_y(nxloc,jmin:jmax,stat_nvar))
  allocate(buf_y (nxloc,jmin:jmax,stat_nvar))
  stat_y = 0.0_WP
  
  ! Open file if it exists
  inquire(file='stat/stat-1Dy',exist=file_is_there)
  if (file_is_there) then
     call stat_1dy_read
  else
     Delta_ty = 0.0_WP
  end if
  
  ! Stop the timer
  call timing_stop('stat_1d')
  
  return
end subroutine stat_1dy_init


! ================================== !
! Initialize the 1D statistic module !
!   -> at a given y                  !
!   -> as a function of x            !
! ================================== !
subroutine stat_1dx_init
  use stat_1d
  use parallel
  use parser
  implicit none
  logical :: isdef,file_is_there
  
  ! General 1D non specific initialization
  call stat_1d_init
  
  ! Start the timer
  call timing_start('stat_1d')
  
  ! No stat at a given y (as a function of x) if x periodic
  if (xper.eq.1) call die('stat_1dx_init: 1D-x statistics impossible (x is periodic)')
  
  ! Any y locations ?
  call parser_is_defined('Statistics locations y',isdef)
  if (isdef) then
     call parser_getsize('Statistics locations y',nyloc)
     allocate(yloc(nyloc))
     call parser_read('Statistics locations y',yloc)
  else 
     call die('stat_1dy_init: Statistics locations y not specified')
  end if
  
  ! Create metric
  call stat_1dy_metric
  
  ! Allocate stat arrays
  allocate(stat_x(nyloc,imin:imax,stat_nvar))
  allocate(buf_x(nyloc,imin:imax,stat_nvar))
  stat_x = 0.0_WP
  
  ! Open file if it exists
  inquire(file='stat/stat-1Dx',exist=file_is_there)
  if (file_is_there) then
     call stat_1dx_read
  else
     Delta_tx = 0.0_WP
  end if
    
  ! Stop the timer
  call timing_stop('stat_1d')
  
  return
end subroutine stat_1dx_init


! ======================= !
! Sample the statistics   !
! at the center: xm or ym !
! ======================= !
subroutine stat_1dy_sample
  use stat_1d
  use data
  use combustion
  use parallel
  use memory
  use time_info
  use lpt
  implicit none
  
  integer  :: i,im,j,k,isc,loc,ns,n
  logical  :: found_loc
  real(WP) :: xcyl,ycyl,zcyl,ucyl,vcyl,wcyl
  
  ! Start the timer
  call timing_start('stat_1d')
  
  ! Prepare the chemical variable if necessary
  if     (trim(chemistry).eq.'diffusion') then
     !call diffusion_lookup('Y_F',  tmp1)
     !call diffusion_lookup('Y_O2' ,tmp2)
     !call diffusion_lookup('Y_CO2',tmp3)
     !call diffusion_lookup('Y_H2O',tmp4)
     !call diffusion_lookup('Y_CO' ,tmp5)
  elseif (trim(chemistry).eq.'premixed') then
     !call premixed_lookup('Y_H2O',tmp1)
     !call premixed_lookup('Y_CO' ,tmp2)
     !call premixed_lookup('Y_CO2',tmp3)
     !call premixed_lookup('Y_O2' ,tmp4)
     !call permixed_lookup('Y_OH' ,tmp5)
  end if
  
  ! Prepare LPT variables
  if (use_lpt) call lpt_stat_prepare
  
  ! Gather the stats
  do loc=1,nxloc
     i  = iloc (loc)
     im = imloc(loc)
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           
           ns = 0
           
           ! Density
           if (use_rho) then
              stat_y(loc,j,ns+1) = stat_y(loc,j,ns+1) + dt*sum(cxmloc(loc,:)*RHO(im:im+1,j,k))
              stat_y(loc,j,ns+2) = stat_y(loc,j,ns+2) + dt*sum(cxmloc(loc,:)*RHO(im:im+1,j,k)**2)
              ns = ns+2
           end if
           
           ! Velocity
           stat_y(loc,j,ns+1) = stat_y(loc,j,ns+1) + dt*sum(cxloc (loc,:)*U(i:i+1  ,j,k))
           stat_y(loc,j,ns+2) = stat_y(loc,j,ns+2) + dt*sum(cxmloc(loc,:)*V(im:im+1,j,k))
           stat_y(loc,j,ns+3) = stat_y(loc,j,ns+3) + dt*sum(cxmloc(loc,:)*W(im:im+1,j,k))
           stat_y(loc,j,ns+4) = stat_y(loc,j,ns+4) + dt*sum(cxloc (loc,:)*U(i:i+1  ,j,k)**2)
           stat_y(loc,j,ns+5) = stat_y(loc,j,ns+5) + dt*sum(cxmloc(loc,:)*V(im:im+1,j,k)**2)
           stat_y(loc,j,ns+6) = stat_y(loc,j,ns+6) + dt*sum(cxmloc(loc,:)*W(im:im+1,j,k)**2)
           stat_y(loc,j,ns+7) = stat_y(loc,j,ns+7) + dt*sum(cxloc (loc,:)*U(i:i+1  ,j,k))*sum(cxmloc(loc,:)*V(im:im+1,j,k))
           stat_y(loc,j,ns+8) = stat_y(loc,j,ns+8) + dt*sum(cxmloc(loc,:)*V(im:im+1,j,k))*sum(cxmloc(loc,:)*W(im:im+1,j,k))
           stat_y(loc,j,ns+9) = stat_y(loc,j,ns+9) + dt*sum(cxmloc(loc,:)*W(im:im+1,j,k))*sum(cxloc (loc,:)*U(i:i+1  ,j,k))
           ns = ns+9
           
           if (use_rho) then
              stat_y(loc,j,ns+1) = stat_y(loc,j,ns+1) + dt*sum(cxloc (loc,:)*rhoU(i:i+1  ,j,k))
              stat_y(loc,j,ns+2) = stat_y(loc,j,ns+2) + dt*sum(cxmloc(loc,:)*rhoV(im:im+1,j,k))
              stat_y(loc,j,ns+3) = stat_y(loc,j,ns+3) + dt*sum(cxmloc(loc,:)*rhoW(im:im+1,j,k))
              stat_y(loc,j,ns+4) = stat_y(loc,j,ns+4) + dt*sum(cxloc (loc,:)*rhoU(i:i+1  ,j,k)*U(i:i+1  ,j,k))
              stat_y(loc,j,ns+5) = stat_y(loc,j,ns+5) + dt*sum(cxmloc(loc,:)*rhoV(im:im+1,j,k)*V(im:im+1,j,k))
              stat_y(loc,j,ns+6) = stat_y(loc,j,ns+6) + dt*sum(cxmloc(loc,:)*rhoW(im:im+1,j,k)*W(im:im+1,j,k))
              stat_y(loc,j,ns+7) = stat_y(loc,j,ns+7) + dt*sum(cxloc (loc,:)*rhoU(i:i+1  ,j,k))*sum(cxmloc(loc,:)*V(im:im+1,j,k))
              stat_y(loc,j,ns+8) = stat_y(loc,j,ns+8) + dt*sum(cxmloc(loc,:)*rhoV(im:im+1,j,k))*sum(cxmloc(loc,:)*W(im:im+1,j,k))
              stat_y(loc,j,ns+9) = stat_y(loc,j,ns+9) + dt*sum(cxmloc(loc,:)*rhoW(im:im+1,j,k))*sum(cxloc (loc,:)*U(i:i+1  ,j,k))
              ns = ns+9
           end if
           
           ! Scalars
           do isc=1,nscalar
              stat_y(loc,j,ns+1) = stat_y(loc,j,ns+1) + dt*sum(cxmloc(loc,:)*SC(im:im+1,j,k,isc))
              stat_y(loc,j,ns+2) = stat_y(loc,j,ns+2) + dt*sum(cxmloc(loc,:)*SC(im:im+1,j,k,isc)**2)
              ns = ns+2
              if (use_rho) then
                 stat_y(loc,j,ns+1) = stat_y(loc,j,ns+1) + dt*sum(cxmloc(loc,:)*SC(im:im+1,j,k,isc)*RHO(im:im+1,j,k))
                 stat_y(loc,j,ns+2) = stat_y(loc,j,ns+2) + dt*sum(cxmloc(loc,:)*SC(im:im+1,j,k,isc)**2*RHO(im:im+1,j,k))
                 ns = ns+2
              end if
           end do
           
           ! LPT
           if (use_lpt) then
              stat_y(loc,j,ns+1) = stat_y(loc,j,ns+1) + dt*sum(cxmloc(loc,:)*tmp6 (im:im+1,j,k))
              stat_y(loc,j,ns+2) = stat_y(loc,j,ns+2) + dt*sum(cxmloc(loc,:)*tmp6 (im:im+1,j,k)**2)
              ns = ns+2
              stat_y(loc,j,ns+1) = stat_y(loc,j,ns+1) + dt*sum(cxmloc(loc,:)*tmp7 (im:im+1,j,k))
              stat_y(loc,j,ns+2) = stat_y(loc,j,ns+2) + dt*sum(cxmloc(loc,:)*tmp8 (im:im+1,j,k))
              stat_y(loc,j,ns+3) = stat_y(loc,j,ns+3) + dt*sum(cxmloc(loc,:)*tmp9 (im:im+1,j,k))
              stat_y(loc,j,ns+4) = stat_y(loc,j,ns+4) + dt*sum(cxmloc(loc,:)*tmp10(im:im+1,j,k))
              stat_y(loc,j,ns+5) = stat_y(loc,j,ns+5) + dt*sum(cxmloc(loc,:)*tmp11(im:im+1,j,k))
              stat_y(loc,j,ns+6) = stat_y(loc,j,ns+6) + dt*sum(cxmloc(loc,:)*tmp12(im:im+1,j,k))
              stat_y(loc,j,ns+7) = stat_y(loc,j,ns+7) + dt*sum(cxmloc(loc,:)*tmp13(im:im+1,j,k))
              stat_y(loc,j,ns+8) = stat_y(loc,j,ns+8) + dt*sum(cxmloc(loc,:)*tmp14(im:im+1,j,k))
              stat_y(loc,j,ns+9) = stat_y(loc,j,ns+9) + dt*sum(cxmloc(loc,:)*tmp15(im:im+1,j,k))
              ns = ns+9
           end if
           
        end do
     end do
  end do
  
  Delta_ty = Delta_ty + dt
    
  ! Stop the timer
  call timing_stop('stat_1d')
  
  return
end subroutine stat_1dy_sample

subroutine stat_1dx_sample
  use stat_1d
  use data
  use combustion
  use parallel
  use memory
  use time_info
  implicit none
  
  integer  :: i,j,jm,k,isc,loc,ns,n
  logical  :: found_loc
  real(WP) :: xcyl,ycyl,zcyl,ucyl,vcyl,wcyl
  
  ! Start the timer
  call timing_start('stat_1d')
  
  ! Prepare the chemical variable if necessary
  if (trim(chemistry).eq.'diffusion') then
     !call diffusion_lookup('Y_F',  tmp1)
     !call diffusion_lookup('Y_O2' ,tmp2)
     !call diffusion_lookup('Y_CO2',tmp3)
     !call diffusion_lookup('Y_H2O',tmp4)
     !call diffusion_lookup('Y_CO' ,tmp5)
  end if
  
  ! Gather the stats
  do loc=1,nyloc
     j  = jloc (loc)
     jm = jmloc(loc)
     do k=kmin_,kmax_
        do i=imin_,imax_
           
           ns = 0
           
           ! Density
           if (use_rho) then
              stat_x(loc,i,ns+1) = stat_x(loc,i,ns+1) + dt*sum(cymloc(loc,:)*RHO(i,jm:jm+1,k))
              stat_x(loc,i,ns+2) = stat_x(loc,i,ns+2) + dt*sum(cymloc(loc,:)*RHO(i,jm:jm+1,k)**2)
              ns = ns+2
           end if

           ! Velocity statistics
           stat_x(loc,i,ns+1) = stat_x(loc,i,ns+1) + dt*sum(cymloc(loc,:)*U(i,jm:jm+1,k))
           stat_x(loc,i,ns+2) = stat_x(loc,i,ns+2) + dt*sum(cyloc (loc,:)*V(i,j:j+1  ,k))
           stat_x(loc,i,ns+3) = stat_x(loc,i,ns+3) + dt*sum(cymloc(loc,:)*W(i,jm:jm+1,k))
           stat_x(loc,i,ns+4) = stat_x(loc,i,ns+4) + dt*sum(cymloc(loc,:)*U(i,jm:jm+1,k)**2)
           stat_x(loc,i,ns+5) = stat_x(loc,i,ns+5) + dt*sum(cyloc (loc,:)*V(i,j:j+1  ,k)**2)
           stat_x(loc,i,ns+6) = stat_x(loc,i,ns+6) + dt*sum(cymloc(loc,:)*W(i,jm:jm+1,k)**2)
           stat_x(loc,i,ns+7) = stat_x(loc,i,ns+7) + dt*sum(cymloc(loc,:)*U(i,jm:jm+1,k))*sum(cyloc (loc,:)*V(i,j:j+1  ,k))
           stat_x(loc,i,ns+8) = stat_x(loc,i,ns+8) + dt*sum(cyloc (loc,:)*V(i,j:j+1  ,k))*sum(cymloc(loc,:)*W(i,jm:jm+1,k))
           stat_x(loc,i,ns+9) = stat_x(loc,i,ns+9) + dt*sum(cymloc(loc,:)*W(i,jm:jm+1,k))*sum(cymloc(loc,:)*U(i,jm:jm+1,k))
           ns = ns+9
           
           if (use_rho) then
              stat_x(loc,i,ns+1) = stat_x(loc,i,ns+1) + dt*sum(cymloc(loc,:)*rhoU(i,jm:jm+1,k))
              stat_x(loc,i,ns+2) = stat_x(loc,i,ns+2) + dt*sum(cyloc (loc,:)*rhoV(i,j:j+1  ,k))
              stat_x(loc,i,ns+3) = stat_x(loc,i,ns+3) + dt*sum(cymloc(loc,:)*rhoW(i,jm:jm+1,k))
              stat_x(loc,i,ns+4) = stat_x(loc,i,ns+4) + dt*sum(cymloc(loc,:)*rhoU(i,jm:jm+1,k)*U(i,jm:jm+1,k))
              stat_x(loc,i,ns+5) = stat_x(loc,i,ns+5) + dt*sum(cyloc (loc,:)*rhoV(i,j:j+1  ,k)*V(i,j:j+1  ,k))
              stat_x(loc,i,ns+6) = stat_x(loc,i,ns+6) + dt*sum(cymloc(loc,:)*rhoW(i,jm:jm+1,k)*W(i,jm:jm+1,k))
              stat_x(loc,i,ns+7) = stat_x(loc,i,ns+7) + dt*sum(cymloc(loc,:)*rhoU(i,jm:jm+1,k))*sum(cyloc (loc,:)*V(i,j:j+1  ,k))
              stat_x(loc,i,ns+8) = stat_x(loc,i,ns+8) + dt*sum(cyloc (loc,:)*rhoV(i,j:j+1  ,k))*sum(cymloc(loc,:)*W(i,jm:jm+1,k))
              stat_x(loc,i,ns+9) = stat_x(loc,i,ns+9) + dt*sum(cymloc(loc,:)*rhoW(i,jm:jm+1,k))*sum(cymloc(loc,:)*U(i,jm:jm+1,k))
              ns = ns+9
           end if
           
           ! Scalars
           do isc=1,nscalar
              stat_x(loc,i,ns+1) = stat_x(loc,i,ns+1) + dt*sum(cymloc(loc,:)*SC(i,jm:jm+1,k,isc))
              stat_x(loc,i,ns+2) = stat_x(loc,i,ns+2) + dt*sum(cymloc(loc,:)*SC(i,jm:jm+1,k,isc)**2)
              ns = ns+2
              if (use_rho) then
                 stat_x(loc,i,ns+1) = stat_x(loc,i,ns+1) + dt*sum(cymloc(loc,:)*SC(i,jm:jm+1,k,isc)*RHO(i,jm:jm+1,k))
                 stat_x(loc,i,ns+2) = stat_x(loc,i,ns+2) + dt*sum(cymloc(loc,:)*SC(i,jm:jm+1,k,isc)**2*RHO(i,jm:jm+1,k))
                 ns = ns+2
              end if
           end do
           
        end do
     end do
  end do
  
  Delta_tx = Delta_tx + dt
  
  ! Stop the timer
  call timing_stop('stat_1d')
  
  return
end subroutine stat_1dx_sample


! ================================= !
! Read the statistics from the disk !
! ================================= !
subroutine stat_1dy_read
  use stat_1d
  use parallel
  implicit none
  
  real(WP) :: time
  integer, dimension(3) :: dims
  integer, dimension(MPI_STATUS_SIZE) :: status
  character(len=str_medium)  :: name
  character(len=str_medium) :: filename
  integer :: var,ierr,ifile,loc
  
  ! Open the file to write
  filename = trim(mpiiofs) // "stat/stat-1Dy"
  call MPI_FILE_OPEN(comm,filename,MPI_MODE_RDONLY,mpi_info,ifile,ierr)
  
  ! Read dimensions from header
  call MPI_FILE_READ_ALL(ifile,dims,3,MPI_INTEGER,status,ierr)
  if (dims(1).ne.nxloc .or. dims(2).ne.ny) then
     print*, 'expected = ',nxloc,ny
     print*, 'stat = ',dims(1),dims(2)
     call die('stat_1dy_read: The size of the stat file is incorrect')
  end if
  if (dims(3).ne.stat_nvar) call die('stat_1dy_read: Wrong number of variables in stat file')
  
  ! Read some headers
  call MPI_FILE_READ_ALL(ifile,Delta_ty,1,MPI_REAL_WP,status,ierr)
  call MPI_FILE_READ_ALL(ifile,time,1,MPI_REAL_WP,status,ierr)
  
  ! Read variable names
  do var=1,stat_nvar
     call MPI_FILE_READ_ALL(ifile,name,str_medium,MPI_CHARACTER,status,ierr)
     if (name.ne.stat_name(var)) then
        print*,irank,name,stat_name(var)
        call die('stat_1dy_read: Variables names in stat and data files do not correspond')
     end if
  end do
  
  ! Read
  call MPI_FILE_READ_ALL(ifile,buf_y,nxloc*ny*stat_nvar,MPI_REAL_WP,status,ierr)
  
  ! Close the file
  call MPI_FILE_CLOSE(ifile,ierr)
  
  ! Restart the stats
  do loc=1,nxloc
     if (iloc_ifwrite(loc).eq.1) stat_y(loc,jmin_:jmax_,:) = buf_y(loc,jmin_:jmax_,:) * Delta_ty*real(nz_,WP)
  end do
  
  return
end subroutine stat_1dy_read

subroutine stat_1dx_read
  use stat_1d
  use parallel
  implicit none
  
  real(WP) :: time
  integer, dimension(3) :: dims
  integer, dimension(MPI_STATUS_SIZE) :: status
  character(len=str_medium)  :: name
  character(len=str_medium) :: filename
  integer :: var,ierr,ifile,loc
  
  ! Open the file to write
  filename = trim(mpiiofs) // "stat/stat-1Dx"
  call MPI_FILE_OPEN(comm,filename,MPI_MODE_RDONLY,mpi_info,ifile,ierr)
  
  ! Read dimensions from header
  call MPI_FILE_READ_ALL(ifile,dims,3,MPI_INTEGER,status,ierr)
  if ((dims(1).ne.nyloc) .or. (dims(2).ne.nx)) then
     print*, 'expected = ',nyloc,nx
     print*, 'stat = ',dims(1),dims(2)
     call die('stat_1dx_read: The size of the stat file is incorrect')
  end if
  if (dims(3).ne.stat_nvar) call die('stat_1dx_read: Wrong number of variables in stat file')
  
  ! Read some headers
  call MPI_FILE_READ_ALL(ifile,Delta_tx,1,MPI_REAL_WP,status,ierr)
  call MPI_FILE_READ_ALL(ifile,time,1,MPI_REAL_WP,status,ierr)
  
  ! Read variable names
  do var=1,stat_nvar
     call MPI_FILE_READ_ALL(ifile,name,str_medium,MPI_CHARACTER,status,ierr)
     if (name.ne.stat_name(var)) then
        call die('stat_1dx_read: Variables names in stat and data files do not correspond')
     end if
  end do
  
  ! Read
  call MPI_FILE_READ_ALL(ifile,buf_x,nyloc*nx*stat_nvar,MPI_REAL_WP,status,ierr)

  ! Close the file
  call MPI_FILE_CLOSE(ifile,ierr)
  
  ! Restart the stats
  do loc=1,nyloc
     if (jloc_ifwrite(loc).eq.1) stat_x(loc,imin_:imax_,:) = buf_x(loc,imin_:imax_,:) * Delta_tx*real(nz_,WP)
  end do
  
  return
end subroutine stat_1dx_read


! ================================ !
! Write the statistics to the disk !
! ================================ !
subroutine stat_1dy_write
  use stat_1d
  use parallel
  use time_info
  use fileio
  implicit none
  
  character(len=str_medium) :: filename
  integer  :: j,loc,iunit,ierr,var

  ! Start the timer
  call timing_start('stat_1d')
  
  ! Nothing to write
  if (Delta_ty.eq.0.0_WP) return

  ! Gather the data
  call parallel_sum(stat_y,buf_y)
  buf_y = buf_y / (Delta_ty*real(nz,WP))

  ! Return if not the root process
  if (irank.ne.iroot) then
     call timing_stop('stat_1d')  
     return
  end if

  ! ------- WRITE THE BINARY FILE ------
  ! Open the file
  filename = "stat/stat-1Dy"
  call BINARY_FILE_OPEN(iunit,trim(filename),"w",ierr)

  ! Write dimensions
  call BINARY_FILE_WRITE(iunit,nxloc,1,kind(nxloc),ierr)
  call BINARY_FILE_WRITE(iunit,ny,1,kind(ny),ierr)
  call BINARY_FILE_WRITE(iunit,stat_nvar,1,kind(stat_nvar),ierr)
  call BINARY_FILE_WRITE(iunit,Delta_ty,1,kind(Delta_ty),ierr)
  call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr)

  ! Write variable names
  do var=1,stat_nvar
     call BINARY_FILE_WRITE(iunit,stat_name(var),str_medium,kind(stat_name),ierr)
  end do
  
  ! Write the stats
  call BINARY_FILE_WRITE(iunit,buf_y,ny*stat_nvar*nxloc,kind(buf_y),ierr)
  
  ! Close the file
  call BINARY_FILE_CLOSE(iunit,ierr)
  
  ! ------- WRITE THE ASCI FILE ------
  if (allx) then
     iunit = iopen()
     write(filename,'(a17)') "stat/stat-1Dy.txt"
     open (iunit, file=adjustl(trim(filename)), form="formatted", iostat=ierr)
     write(iunit,'(10000a20)') 'y', 'ym', (trim(adjustl(stat_name(var))),var=1,stat_nvar)
     do j=jmin,jmax
        write(iunit,'(10000ES20.12)') y(j)+epsilon(y(j)),ym(j)+epsilon(ym(j)),sum(buf_y(:,j,:),dim=1)/real(nx,WP)
     end do
     flush(iunit)
     close(iclose(iunit))
  else
     do loc=1,nxloc
        iunit = iopen()
        write(filename,'(a14,e12.6e2,a4)') "stat/stat-1Dy-",xloc(loc),".txt"
        open (iunit, file=adjustl(trim(filename)), form="formatted", iostat=ierr)
        write(iunit,'(10000a20)') 'y', 'ym', (trim(adjustl(stat_name(var))),var=1,stat_nvar)
        do j=jmin,jmax
           write(iunit,'(10000ES20.12)') y(j)+epsilon(y(j)),ym(j)+epsilon(ym(j)),buf_y(loc,j,:)
        end do
        close(iclose(iunit))
     end do
  end if
  
  ! Log
  call monitor_log("1D-Y STATISTICS FILE WRITTEN")
    
  ! Stop the timer
  call timing_stop('stat_1d')
  
  return
end subroutine stat_1dy_write


subroutine stat_1dx_write
  use stat_1d
  use parallel
  use time_info
  use fileio
  implicit none
  
  character(len=str_medium) :: filename
  integer  :: i,loc,iunit,ierr,var

  ! Start the timer
  call timing_start('stat_1d')
  
  ! Nothing to write
  if (Delta_tx.eq.0.0_WP) return

  ! Gather the data
  call parallel_sum(stat_x,buf_x)
  buf_x = buf_x / (Delta_tx*real(nz,WP))

  ! Return if not the root process
  if (irank.ne.iroot) then
     call timing_stop('stat_1d')  
     return
  end if

  ! ------- WRITE THE BINARY FILE ------
  ! Open the file
  filename = "stat/stat-1Dx"
  call BINARY_FILE_OPEN(iunit,trim(filename),"w",ierr)

  ! Write dimensions
  call BINARY_FILE_WRITE(iunit,nyloc,1,kind(nyloc),ierr)
  call BINARY_FILE_WRITE(iunit,nx,1,kind(nx),ierr)
  call BINARY_FILE_WRITE(iunit,stat_nvar,1,kind(stat_nvar),ierr)
  call BINARY_FILE_WRITE(iunit,Delta_tx,1,kind(Delta_tx),ierr)
  call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr)

  ! Write variable names
  do var=1,stat_nvar
     call BINARY_FILE_WRITE(iunit,stat_name(var),str_medium,kind(stat_name),ierr)
  end do
  
  ! Write the stats
  call BINARY_FILE_WRITE(iunit,buf_x,nx*stat_nvar*nyloc,kind(buf_x),ierr)
  
  ! Close the file
  call BINARY_FILE_CLOSE(iunit,ierr)
  
  ! ------- WRITE THE ASCI FILE ------
  do loc=1,nyloc
     iunit = iopen()
     write(filename,'(a14,e12.6e2,a4)') "stat/stat-1Dx-",yloc(loc),".txt"
     open (iunit, file=adjustl(trim(filename)), form="formatted", iostat=ierr)
     write(iunit,'(10000a20)') 'x', 'xm', (trim(adjustl(stat_name(var))),var=1,stat_nvar)
     do i=imin,imax
        write(iunit,'(10000ES20.12)') x(i)+epsilon(x(i)),xm(i)+epsilon(xm(i)),buf_x(loc,i,:)
     end do
     close(iclose(iunit))
  end do
  
  ! Log
  call monitor_log("1D-X STATISTICS FILE WRITTEN")
  
  ! Stop the timer
  call timing_stop('stat_1d')
  
  return
end subroutine stat_1dx_write
