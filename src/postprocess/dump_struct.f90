! =========================================== !
! Struct identification routine to id, build, !
! synch across procs and compute stats        !
! =========================================== !
module dump_struct
  use precision
  use geometry
  use partition
  use math
  use string
  implicit none

  ! Time info
  integer :: nout_time
  real(WP), dimension(:), pointer :: out_time => null()

  ! define data type for local structure
  type struct_type
     ! Location WRT mesh
     integer, dimension(:,:), pointer :: node => null()
     integer :: nnode
     integer :: id
     integer, dimension(3) :: per
     real(WP) :: vol
     real(WP) :: x
     real(WP) :: y
     real(WP) :: z
     real(WP) :: u
     real(WP) :: v
     real(WP) :: w
     type(struct_type), pointer :: next => null()
  end type struct_type

  ! define data type for meta_structures
  type meta_struct_type 
     integer :: id     
     real(WP) :: vol
     real(WP) :: x
     real(WP) :: y
     real(WP) :: z
     real(WP) :: u
     real(WP) :: v
     real(WP) :: w
     real(WP), dimension(3) :: lengths
     real(WP), dimension(3,3) :: axes
  end type meta_struct_type

  ! Struct array and sizes
  type(struct_type), pointer :: first_struct => null()  ! first element
  type(struct_type), pointer :: my_struct => null()     ! subsequent struct in linked list
  integer :: n_struct, n_struct_max, n_meta_struct

  ! list of meta-structures' ID tags
  integer,dimension(:), pointer :: meta_structures => null()

  ! List of meta-structures
  type(meta_struct_type), dimension(:), pointer :: meta_structures_list => null()

  ! ID tags available globally
  integer, dimension (:,:,:), pointer :: id => null()    

  ! Periodicity tag available globally
  integer, dimension (:,:,:,:), pointer :: idp => null()

  ! List of output variables names
  integer :: meta_structures_nname
  character(len=str_medium), dimension(:), pointer :: meta_structures_name => null()

  ! Cut-off defining structure
  real(WP) :: cutoff

end module dump_struct

! ---------------------------------- !
! Initialization for struct routines !
! ---------------------------------- !
subroutine dump_struct_init  
  use dump_struct
  use parser
  use time_info
  use parallel
  implicit none

  logical :: file_is_there
  integer :: i,ierr

  ! Create & Start the timer
  call timing_create('structures')
  call timing_start ('structures')

  ! Structure cutoff
  call parser_read('Structure cutoff',cutoff)

  ! Allocate id array to assist in struct_synch across procs
  allocate(id(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(idp(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3))

  ! Variable names
  meta_structures_nname = 20
  allocate(meta_structures_name(meta_structures_nname))
  meta_structures_name(1) = 'Tag'
  meta_structures_name(2) = 'Volume'
  meta_structures_name(3) = 'X'
  meta_structures_name(4) = 'Y'
  meta_structures_name(5) = 'Z'
  meta_structures_name(6) = 'U'
  meta_structures_name(7) = 'V'
  meta_structures_name(8) = 'W'
  meta_structures_name(9) = 'L1'
  meta_structures_name(10) = 'L2'
  meta_structures_name(11) = 'L3'
  meta_structures_name(12) = 'V11'
  meta_structures_name(13) = 'V12'
  meta_structures_name(14) = 'V13'
  meta_structures_name(15) = 'V21'
  meta_structures_name(16) = 'V22'
  meta_structures_name(17) = 'V23'
  meta_structures_name(18) = 'V31'
  meta_structures_name(19) = 'V32'
  meta_structures_name(20) = 'V33'

  ! Open the file
  inquire(file='structs/times',exist=file_is_there)
  call MPI_BARRIER(comm,ierr)

  if (file_is_there) then
     ! Read the file
     call parser_parsefile('structs/times')
     ! Get the time
     call parser_getsize('time values',nout_time)
     allocate(out_time(nout_time))
     call parser_read('time values',out_time)
     ! Remove future time
     future: do i=1,size(out_time)
        if (out_time(i).GE.time*0.99999_WP) then
           nout_time = i-1
           exit future
        end if
     end do future
  else
     ! Create directory
     if (irank.eq.iroot) call CREATE_FOLDER("structs")
     ! Set the time
     nout_time = 0
     allocate(out_time(1))
  end if

  ! Stop the timer
  call timing_stop ('structures')

  return
end subroutine dump_struct_init

! --------------------------------- !
! Start building, synching struct   !
! Call routines from here           !
! --------------------------------- !  
subroutine dump_structures
  use dump_struct
  use parallel 
  use time_info
  use parser
  implicit none

  ! Start the timer
  call timing_start('structures')

  ! Update the times file
  call dump_struct_times

  ! Start building the struct
  call struct_build

  ! Synchronize struct tags across procs
  call struct_synch

  ! Synchronize struct periodicity across procs
  call struct_synch_per

  ! Update global tag array (do we need this?)
  call struct_tag_update

  ! Build array of structures, sort, reduce, compute final stats
  call struct_final
  
  ! Processes splits/merges
  call dump_structID_update

  ! De-allocate local structures
  call kill_struct

  ! De-allocate list of meat_structures
  deallocate(meta_structures)
  deallocate(meta_structures_list)
  nullify(meta_structures_list)
  nullify(meta_structures)

  ! Stop the timer
  call timing_stop('structures')

  return
end subroutine dump_structures

! -------------------------------------------------------------------------- !
! Update file containing  the times at which the structures have been dumped !
! -------------------------------------------------------------------------- !
subroutine dump_struct_times
  use dump_struct
  use time_info
  use parallel
  use fileio
  implicit none

  integer :: iunit,ierr
  real(WP), dimension(:), allocatable :: buffer
  character(len=80) :: str

  ! Update the time info
  allocate(buffer(nout_time))
  buffer(1:nout_time) = out_time(1:nout_time)
  deallocate(out_time)
  nout_time = nout_time + 1
  allocate(out_time(nout_time))
  out_time(1:nout_time-1) = buffer(1:nout_time-1)
  out_time(nout_time) = time

  ! Write - Single proc & parallel => only root writes (in ASCII)
  if (irank==iroot) then

     ! Open the file
     iunit = iopen()
     open(iunit,file="structs/times",form="formatted",iostat=ierr,status="REPLACE")

     ! Time section
     str='TIME'
     write(iunit,'(a80)') str
     str='number of steps:'
     write(iunit,'(a16,x,i12)') str,nout_time
     str='time values:'
     write(iunit,'(a12,x,10000000(3(ES12.5,x),/))') str,out_time

     ! Close the file
     close(iclose(iunit))
  end if

  return
end subroutine dump_struct_times

! --------------------------- !
! Deallocate local structures !
! --------------------------- !
subroutine kill_struct
  use dump_struct
  use parallel
  implicit none

  type(struct_type), pointer :: current => null()
  type(struct_type), pointer :: next => null()

  current => first_struct
  do while (associated(current))
     next => current%next
     if (associated(current%node)) then
        deallocate(current%node)
        nullify(current%node)
     end if
     deallocate(current)
     nullify(current)
     current => next
  end do

  ! Finish clean-up
  nullify(first_struct)
  nullify(my_struct)

  return
end subroutine kill_struct

! --------------------------------- !
! Start building contiguous structs !
! --------------------------------- !
subroutine struct_build
  use dump_struct 
  use data
  use combustion
  use parallel
  use multiphase

  implicit none

  integer :: i,j,k,n,ii,jj,kk,id_offset,idd,npp,nnode,ierr
  integer :: dim,dir,i_struct,j_struct,k_struct,s_start,s_end, i_s
  integer, dimension(3) :: pos

  ! Initialize id (for tagging)
  id = 0
  idp = 0
  n_struct = 0 ! counter for local number of structs

  ! Compute some useful values
  npp = int((nx*ny*nz)/(npx*npy*npz)) ! number points per processor
  id_offset = npp*(irank - 1)         ! global tag offset; unique for each proc

  ! initialize the global tag to id_offset
  idd  = id_offset

  ! outer loop over domain to find "corner" of structure
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_

           ! Find untagged point in gas phase
           if (VOFavg(i,j,k).ge.cutoff .and. id(i,j,k).eq.0) then

              ! Allocate next structure
              if(idd.eq.id_offset) then
                 allocate(first_struct)
                 my_struct => first_struct
              else
                 allocate(my_struct%next)
                 my_struct => my_struct%next
              end if

              ! Initialize first elements
              my_struct%nnode = 0
              my_struct%id = 0
              nullify(my_struct%next)

              ! Initialize periodicity stuff
              my_struct%per = 0

              ! Update local and global tag values
              idd  = idd  + 1

              ! Set counter for nnode in current structure
              nnode = 1
              n_struct = n_struct + 1

              ! allocate node array component of my_struct
              allocate(my_struct%node(nnode,5))  

              ! store first point
              my_struct%id        = idd
              my_struct%node(1,1) = i
              my_struct%node(1,2) = j
              my_struct%node(1,3) = k
              my_struct%node(1,4) = 1           ! S node column
              my_struct%node(1,5) = 0           ! C node column

              ! Periodicity
              if (xper.eq.1 .and. i.eq.imax) my_struct%per(1) = 1
              if (yper.eq.1 .and. j.eq.jmax) my_struct%per(2) = 1
              if (zper.eq.1 .and. k.eq.kmax) my_struct%per(3) = 1 

              ! store global ID tag
              id(i,j,k) = idd 

              ! set location of S nodes
              s_start = 1
              s_end = 1

              ! start building structure in layers of S and C
              do while (maxval(my_struct%node(:,4)).gt.0) ! look at S column to find a non-zero value
                 do i_s = s_start,s_end                   ! loop over the size of the S-marked node list

                    ! Get indices
                    i_struct = my_struct%node(i_s,1)
                    j_struct = my_struct%node(i_s,2)
                    k_struct = my_struct%node(i_s,3)

                    ! Loop over dimension
                    do dim=1,3
                       ! Loop over direction
                       do dir=-1,+1,2

                          ! Find neighboring point
                          pos      = 0
                          pos(dim) = dir
                          ii = i_struct+pos(1)
                          jj = j_struct+pos(2)
                          kk = k_struct+pos(3)

                          ! Ensure we don't grow into ghost cells
                          ii = max(ii,imin_)    ! no less than imin_
                          ii = min(ii,imax_)    ! no more than imax_
                          jj = max(jj,jmin_)
                          jj = min(jj,jmax_)
                          kk = max(kk,kmin_)
                          kk = min(kk,kmax_)

                          if (VOFavg(ii,jj,kk).ge.cutoff .and. id(ii,jj,kk).eq.0) then

                             ! store ID tags for new nodes
                             id(ii,jj,kk) = idd
                             ! increment nnode
                             nnode = nnode + 1
                             ! resize the array if necessary ==> current implementation resizes for each new node
                             if (nnode.gt.size(my_struct%node(:,1))) call struct_resize(nnode-1)
                             ! add new node to my_struct
                             my_struct%node(nnode,1) = ii
                             my_struct%node(nnode,2) = jj
                             my_struct%node(nnode,3) = kk
                             my_struct%node(nnode,5) = 1 ! mark with C
                             my_struct%node(nnode,4) = 0 ! place something in S
                             ! Periodicity
                             if (xper.eq.1 .and. ii.eq.imax) my_struct%per(1) = 1
                             if (yper.eq.1 .and. jj.eq.jmax) my_struct%per(2) = 1
                             if (zper.eq.1 .and. kk.eq.kmax) my_struct%per(3) = 1 

                          end if

                       end do ! dir
                    end do ! dim
                 end do ! S-marked nodes

                 ! Update limits for S nodes
                 s_start = s_end +1
                 s_end   = nnode
                 my_struct%node(:,4) = 0                   ! zero S column
                 my_struct%node(:,4) = my_struct%node(:,5) ! set S column equal to C column
                 my_struct%node(:,5) = 0                   ! zero C column

              end do ! end of building contiguous structure

              ! store final count of nodes
              my_struct%nnode = nnode

              ! Update periodicity array idp
              do n=1,nnode
                 ii= my_struct%node(n,1)
                 jj= my_struct%node(n,2)
                 kk= my_struct%node(n,3)
                 idp(ii,jj,kk,:) = max(0,my_struct%per)
              end do

           end if

        end do ! k
     end do ! j
  end do ! i

  ! total structures or parts of structures across all procs
  call MPI_ALLREDUCE(n_struct,n_struct_max,1,MPI_INTEGER,MPI_MAX,comm,ierr)

  return
end subroutine struct_build

! ---------------- !
! Resize my_struct !
! ---------------- !
subroutine struct_resize(n)
  use dump_struct
  implicit none

  integer, intent(in) :: n
  integer, dimension(:,:), pointer :: node_temp => null()
  integer :: n_new,i

  ! add one row
  n_new = n + 1
  allocate(node_temp(1:n_new,1:5))
  do i=1,n
     node_temp(i,1) = my_struct%node(i,1)
     node_temp(i,2) = my_struct%node(i,2)
     node_temp(i,3) = my_struct%node(i,3)
     node_temp(i,4) = my_struct%node(i,4)
     node_temp(i,5) = my_struct%node(i,5)
  end do
  deallocate(my_struct%node)
  nullify(my_struct%node)
  my_struct%node => node_temp

  return
end subroutine struct_resize

! ----------------------------------------- !
! Synchronize struct tags across all procs  !
! ----------------------------------------- !
subroutine struct_synch
  use dump_struct
  use boundary
  use parallel
  implicit none

  integer :: i,j,k,n,ii,jj,kk,stop_,stop_global,counter,ierr

  ! initialize global stop criterion
  stop_global = 1

  ! initialize a counter
  counter = 0

  ! march along interior border
  do while (stop_global.ne.0)

     ! Initialize local stop flag
     stop_ = 0

     ! update ghost cells
     call boundary_update_border_int(id,'+','ym')      

     ! imin_
     do j= jmin_,jmax_
        do k= kmin_,kmax_

           ! check if: (a) ID_ghost > 0; (b) ID_border > ID_ghost
           if (id(imin_-1,j,k).gt.0.and.id(imin_,j,k).gt.id(imin_-1,j,k)) then

              ! Initialize stop flag
              stop_ = 0

              ! start marching thru list, starting at first_struct
              my_struct => first_struct

              do while(associated(my_struct).and.stop_.eq.0)
                 if (my_struct%id.eq.id(imin_,j,k)) then

                    ! Update structure id with ghost cell value
                    my_struct%id  = id(imin_-1,j,k)

                    ! Update global tag arrays id and idp
                    do n=1,my_struct%nnode
                       ii = my_struct%node(n,1)
                       jj = my_struct%node(n,2)
                       kk = my_struct%node(n,3)
                       id(ii,jj,kk) = id(imin_-1,j,k)
                    end do

                    ! Exit loop over structures
                    stop_ = 1

                 end if

                 ! Go to next structure
                 my_struct => my_struct%next

              end do
           end if
        end do
     end do

     ! imax_
     do j= jmin_,jmax_
        do k= kmin_,kmax_

           ! check if: (a) ID_ghost > 0; (b) ID_border > ID_ghost
           if (id(imax_+1,j,k).gt.0.and.id(imax_,j,k).gt.id(imax_+1,j,k)) then

              ! Initialize stop flag
              stop_ = 0

              ! start marching thru list, starting at first_struct
              my_struct => first_struct

              do while(associated(my_struct).and.stop_.eq.0)
                 if (my_struct%id.eq.id(imax_,j,k)) then

                    ! Update structure id with ghost cell value
                    my_struct%id = id(imax_+1,j,k)

                    ! Update global tag arrays id and idp
                    do n=1,my_struct%nnode
                       ii = my_struct%node(n,1)
                       jj = my_struct%node(n,2)
                       kk = my_struct%node(n,3)
                       id(ii,jj,kk) = id(imax_+1,j,k)
                    end do

                    ! Exit loop over structures
                    stop_ = 1

                 end if

                 ! Go to next structure
                 my_struct => my_struct%next

              end do
           end if
        end do
     end do

     ! jmin_
     do i= imin_,imax_
        do k= kmin_,kmax_

           if (id(i,jmin_-1,k).gt.0.and.id(i,jmin_,k).gt.id(i,jmin_-1,k)) then

              ! Initialize stop flag
              stop_ = 0

              ! start marching thru list, starting at first_struct
              my_struct => first_struct

              do while(associated(my_struct).and.stop_.eq.0)
                 if (my_struct%id.eq.id(i,jmin_,k)) then

                    ! Update structure id with ghost cell value
                    my_struct%id = id(i,jmin_-1,k)

                    ! Update global tag arrays id and idp
                    do n=1,my_struct%nnode
                       ii = my_struct%node(n,1)
                       jj = my_struct%node(n,2)
                       kk = my_struct%node(n,3)
                       id(ii,jj,kk) = id(i,jmin_-1,k)
                    end do

                    ! Exit loop over structures
                    stop_ = 1

                 end if

                 ! Go to next structure
                 my_struct => my_struct%next
              end do
           end if
        end do
     end do

     ! jmax_
     do i= imin_,imax_
        do k= kmin_,kmax_

           if (id(i,jmax_+1,k).gt.0.and.id(i,jmax_,k).gt.id(i,jmax_+1,k)) then

              ! Initialize stop flag
              stop_ = 0

              ! start marching thru list, starting at first_struct
              my_struct => first_struct

              do while(associated(my_struct).and.stop_.eq.0)
                 if (my_struct%id.eq.id(i,jmax_,k)) then

                    ! Update structure id with ghost cell value
                    my_struct%id = id(i,jmax_+1,k)

                    ! Update global tag arrays id and idp
                    do n=1,my_struct%nnode
                       ii = my_struct%node(n,1)
                       jj = my_struct%node(n,2)
                       kk = my_struct%node(n,3)
                       id(ii,jj,kk) = id(i,jmax_+1,k)
                    end do

                    ! Exit loop over structures
                    stop_ = 1
                 end if

                 ! Exit loop over structures
                 my_struct => my_struct%next
              end do
           end if
        end do
     end do

     ! kmin_
     do i= imin_,imax_
        do j= jmin_,jmax_

           if (id(i,j,kmin_-1).gt.0.and.id(i,j,kmin_).gt.id(i,j,kmin_-1)) then

              ! Initialize stop flag
              stop_ = 0

              ! start marching thru list, starting at first_struct
              my_struct => first_struct

              do while(associated(my_struct).and.stop_.eq.0)
                 if (my_struct%id.eq.id(i,j,kmin_)) then

                    ! Update structure id with ghost cell value
                    my_struct%id = id(i,j,kmin_-1)

                    ! Update global tag arrays id and idp
                    do n=1,my_struct%nnode
                       ii = my_struct%node(n,1)
                       jj = my_struct%node(n,2)
                       kk = my_struct%node(n,3)
                       id(ii,jj,kk) = id(i,j,kmin_-1)
                    end do

                    ! Exit loop over structures
                    stop_ = 1

                 end if

                 ! Go to next structure
                 my_struct => my_struct%next

              end do
           end if
        end do
     end do

     ! kmax_
     do i= imin_,imax_
        do j= jmin_,jmax_

           if (id(i,j,kmax_+1).gt.0.and.id(i,j,kmax_).gt.id(i,j,kmax_+1)) then

              ! Initialize stop flag
              stop_ = 0

              ! start marching thru list, starting at first_struct
              my_struct => first_struct

              do while(associated(my_struct).and.stop_.eq.0)
                 if (my_struct%id.eq.id(i,j,kmax_)) then

                    ! Update structure id with ghost cell value
                    my_struct%id = id(i,j,kmax_+1)

                    ! Update global tag arrays id and idp
                    do n=1,my_struct%nnode
                       ii = my_struct%node(n,1)
                       jj = my_struct%node(n,2)
                       kk = my_struct%node(n,3)
                       id(ii,jj,kk) = id(i,j,kmax_+1)
                    end do

                    ! Exit loop over structures
                    stop_ = 1

                 end if

                 ! Go to next structure
                 my_struct => my_struct%next

              end do
           end if
        end do
     end do

     ! Check if we did some changes
     call MPI_ALLREDUCE(stop_,stop_global,1,MPI_INTEGER,MPI_MAX,comm,ierr)

     ! Count how many global updates we do
     counter = counter + 1

  end do

  ! update ghost cells    
  call boundary_update_border_int(id,'+','ym')

  return

end subroutine struct_synch

! ------------------------------------------------ !
! Synchronize struct periodicity across all procs  !
! ------------------------------------------------ !
subroutine struct_synch_per
  use dump_struct
  use boundary
  use parallel
  implicit none

  integer :: i,j,k,n,ii,jj,kk,stop_,stop_global,counter,ierr,flag

  ! initialize global stop criterion
  stop_global = 1

  ! initialize a counter
  counter = 0

  ! march along interior border
  do while (stop_global.ne.0)

     ! Initialize local stop flag
     stop_ = 0

     ! update ghost cells
     call boundary_update_border_int(idp(:,:,:,1),'+','ym')      
     call boundary_update_border_int(idp(:,:,:,2),'+','ym')      
     call boundary_update_border_int(idp(:,:,:,3),'+','ym')      

     ! imin_
     do j= jmin_,jmax_
        do k= kmin_,kmax_

           ! check if: (a) not domain boundary; (b) structure there (c) ghost cell has periodicity flag on
           if ( id(imin_,j,k).gt.0 .and. &
                ( (imin_.ne.imin .and. idp(imin_,j,k,1).lt.idp(imin_-1,j,k,1)) .or. &
                idp(imin_,j,k,2).lt.idp(imin_-1,j,k,2) .or. &
                idp(imin_,j,k,3).lt.idp(imin_-1,j,k,3)) ) then

              ! Initialize stop flag
              flag = 0

              ! start marching thru list, starting at first_struct
              my_struct => first_struct

              do while(associated(my_struct).and.flag.eq.0)
                 if (my_struct%id.eq.id(imin_,j,k)) then

                    ! Make sure it is the structure that we want (may be several with same tag)
                    do n=1,my_struct%nnode
                       if (my_struct%node(n,1).eq.imin_.and.my_struct%node(n,2).eq.j.and.my_struct%node(n,3).eq.k) flag = 1
                    end do

                    if (flag .eq.1) then 
                       ! Update global per array idp
                       do n=1,my_struct%nnode
                          ii = my_struct%node(n,1)
                          jj = my_struct%node(n,2)
                          kk = my_struct%node(n,3)
                          ! i
                          if (imin_.ne.imin) then 
                             stop_ = max(stop_,-(idp(ii,jj,kk,1)-idp(imin_-1,j,k,1)))
                             idp(ii,jj,kk,1) = max(idp(ii,jj,kk,1),idp(imin_-1,j,k,1))
                          end if
                          ! j 
                          stop_= max(stop_,-(idp(ii,jj,kk,2)-idp(imin_-1,j,k,2)))
                          idp(ii,jj,kk,2) = max(idp(ii,jj,kk,2),idp(imin_-1,j,k,2))
                          ! k
                          stop_= max(stop_,-(idp(ii,jj,kk,3)-idp(imin_-1,j,k,3)))                          
                          idp(ii,jj,kk,3) = max(idp(ii,jj,kk,3),idp(imin_-1,j,k,3))                
                       end do
                    end if
                 end if

                 ! Go to next structure
                 my_struct => my_struct%next

              end do
           end if
        end do
     end do

     ! imax_
     do j= jmin_,jmax_
        do k= kmin_,kmax_

           if ( id(imax_,j,k).gt.0 .and. &
                ( (imax_.eq.imax .and. idp(imax_,j,k,1).lt.idp(imax_+1,j,k,1)) .or. &
                idp(imax_,j,k,2).lt.idp(imax_+1,j,k,2) .or. &
                idp(imax_,j,k,3).lt.idp(imax_+1,j,k,3)) ) then

              ! Initialize stop flag
              flag = 0

              ! start marching thru list, starting at first_struct
              my_struct => first_struct

              do while(associated(my_struct).and.flag.eq.0)
                 if (my_struct%id.eq.id(imax_,j,k)) then

                    ! Make sure it is the structure that we want (may be several with same tag)
                    do n=1,my_struct%nnode
                       if (my_struct%node(n,1).eq.imax_.and.my_struct%node(n,2).eq.j.and.my_struct%node(n,3).eq.k) flag = 1
                    end do

                    if (flag.eq.1) then
                       ! Update global per array idp
                       do n=1,my_struct%nnode
                          ii = my_struct%node(n,1)
                          jj = my_struct%node(n,2)
                          kk = my_struct%node(n,3)
                          ! i
                          if (imax_.ne.imax) then
                             stop_ = max(stop_,-(idp(ii,jj,kk,1)-idp(imax_+1,j,k,1)))                       
                             idp(ii,jj,kk,1) = max(idp(ii,jj,kk,1),idp(imax_+1,j,k,1))
                          end if
                          ! j
                          stop_ = max(stop_,-(idp(ii,jj,kk,2)-idp(imax_+1,j,k,2)))                          
                          idp(ii,jj,kk,2) = max(idp(ii,jj,kk,2),idp(imax_+1,j,k,2))
                          ! k
                          stop_ = max(stop_,-(idp(ii,jj,kk,3)-idp(imax_+1,j,k,3)))                          
                          idp(ii,jj,kk,3) = max(idp(ii,jj,kk,3),idp(imax_+1,j,k,3))
                       end do
                    end if
                 end if

                 ! Go to next structure
                 my_struct => my_struct%next

              end do
           end if
        end do
     end do

     ! jmin_
     do i= imin_,imax_
        do k= kmin_,kmax_

           if ( id(i,jmin_,k).gt.0 .and. &
                ( idp(i,jmin_,k,1).lt.idp(i,jmin_-1,k,1) .or. &
                (jmin_.ne.jmin .and. idp(i,jmin_,k,2).lt.idp(i,jmin_-1,k,2)) .or. &
                idp(i,jmin_,k,3).lt.idp(i,jmin_-1,k,3)) ) then

              ! Initialize stop flag
              flag = 0

              ! start marching thru list, starting at first_struct
              my_struct => first_struct

              do while(associated(my_struct).and.flag.eq.0)
                 if (my_struct%id.eq.id(i,jmin_,k)) then

                    ! Make sure it is the structure that we want (may be several with same tag)
                    do n=1,my_struct%nnode
                       if (my_struct%node(n,1).eq.i.and.my_struct%node(n,2).eq.jmin_.and.my_struct%node(n,3).eq.k) flag = 1
                    end do

                    if (flag.eq.1) then
                       ! Update global per array idp
                       do n=1,my_struct%nnode
                          ii = my_struct%node(n,1)
                          jj = my_struct%node(n,2)
                          kk = my_struct%node(n,3)
                          ! i
                          stop_ = max(stop_,-(idp(ii,jj,kk,1)-idp(i,jmin_-1,k,1)))                       
                          idp(ii,jj,kk,1) = max(idp(ii,jj,kk,1),idp(i,jmin_-1,k,1))
                          ! j
                          if (jmin_.ne.jmin) then
                             stop_ = max(stop_,-(idp(ii,jj,kk,2)-idp(i,jmin_-1,k,2)))                          
                             idp(ii,jj,kk,2) = max(idp(ii,jj,kk,2),idp(i,jmin_-1,k,2))
                          end if
                          ! k
                          stop_ = max(stop_,-(idp(ii,jj,kk,3)-idp(i,jmin_-1,k,3)))                          
                          idp(ii,jj,kk,3) = max(idp(ii,jj,kk,3),idp(i,jmin_-1,k,3))
                       end do
                    end if
                 end if

                 ! Go to next structure
                 my_struct => my_struct%next

              end do
           end if
        end do
     end do

     ! jmax_
     do i= imin_,imax_
        do k= kmin_,kmax_

           if ( id(i,jmax_,k).gt.0 .and. &
                ( idp(i,jmax_,k,1).lt.idp(i,jmax_+1,k,1) .or. &
                (jmax_.ne.jmax.and.idp(i,jmax_,k,2).lt.idp(i,jmax_+1,k,2)) .or. &
                idp(i,jmax_,k,3).lt.idp(i,jmax_+1,k,3)) ) then

              ! Initialize stop flag
              flag = 0

              ! Start marching thru list, starting at first_struct
              my_struct => first_struct

              do while(associated(my_struct).and.flag.eq.0)
                 if (my_struct%id.eq.id(i,jmax_,k)) then

                    ! Make sure it is the structure that we want (may be several with same tag)
                    do n=1,my_struct%nnode
                       if (my_struct%node(n,1).eq.i.and.my_struct%node(n,2).eq.jmax_.and.my_struct%node(n,3).eq.k) flag = 1
                    end do

                    if (flag.eq.1) then
                       ! Update global per array idp
                       do n=1,my_struct%nnode
                          ii = my_struct%node(n,1)
                          jj = my_struct%node(n,2)
                          kk = my_struct%node(n,3)
                          ! i
                          stop_ = max(stop_,-(idp(ii,jj,kk,1)-idp(i,jmax_+1,k,1)))
                          idp(ii,jj,kk,1) = max(idp(ii,jj,kk,1),idp(i,jmax_+1,k,1))
                          ! j
                          if (jmax_.ne.jmax) then
                             stop_ = max(stop_,-(idp(ii,jj,kk,2)-idp(i,jmax_+1,k,2)))                          
                             idp(ii,jj,kk,2) = max(idp(ii,jj,kk,2),idp(i,jmax_+1,k,2))
                          end if
                          ! k
                          stop_ = max(stop_,-(idp(ii,jj,kk,3)-idp(i,jmax_+1,k,3)))                          
                          idp(ii,jj,kk,3) = max(idp(ii,jj,kk,3),idp(i,jmax_+1,k,3))
                       end do
                    end if
                 end if

                 ! Go to next structure
                 my_struct => my_struct%next

              end do
           end if
        end do
     end do

     ! kmin_
     do i= imin_,imax_
        do j= jmin_,jmax_

           if ( id(i,j,kmin_).gt.0 .and.&
                ( idp(i,j,kmin_,1).lt.idp(i,j,kmin_-1,1) .or. &
                idp(i,j,kmin_,2).lt.idp(i,j,kmin_-1,2) .or. &
                (kmin_.ne.kmin .and. idp(i,j,kmin_,3).lt.idp(i,j,kmin_-1,3))) ) then

              ! Initialize stop flag
              flag = 0

              ! start marching thru list, starting at first_struct
              my_struct => first_struct

              do while(associated(my_struct).and.flag.eq.0)
                 if (my_struct%id.eq.id(i,j,kmin_)) then

                    ! make sure it is the structure that we want (may be several with same tag)
                    do n=1,my_struct%nnode
                       if (my_struct%node(n,1).eq.i.and.my_struct%node(n,2).eq.j.and.my_struct%node(n,3).eq.kmin_) flag = 1
                    end do

                    ! Update global tag arrays id and idp
                    do n=1,my_struct%nnode
                       ii = my_struct%node(n,1)
                       jj = my_struct%node(n,2)
                       kk = my_struct%node(n,3)
                       ! i
                       stop_ = max(stop_,-(idp(ii,jj,kk,1)-idp(i,j,kmin_-1,1)))                       
                       idp(ii,jj,kk,1) = max(idp(ii,jj,kk,1),idp(i,j,kmin_-1,1))
                       ! j
                       stop_ = max(stop_,-(idp(ii,jj,kk,2)-idp(i,j,kmin_-1,2)))                       
                       idp(ii,jj,kk,2) = max(idp(ii,jj,kk,2),idp(i,j,kmin_-1,2))
                       ! k
                       if (kmin_.ne.kmin) then 
                          stop_ = max(stop_,-(idp(ii,jj,kk,3)-idp(i,j,kmin_-1,3)))                       
                          idp(ii,jj,kk,3) = max(idp(ii,jj,kk,3),idp(i,j,kmin_-1,3))
                       end if
                    end do
                 end if

                 ! Go to next structure
                 my_struct => my_struct%next

              end do
           end if
        end do
     end do

     ! kmax_
     do i= imin_,imax_
        do j= jmin_,jmax_

           if ( id(i,j,kmax_).gt.0 .and. &
                (idp(i,j,kmax_,1).lt.idp(i,j,kmax_+1,1) .or. &
                idp(i,j,kmax_,2).lt.idp(i,j,kmax_+1,2) .or. &
                (kmax_.ne.kmax.and.idp(i,j,kmax_,3).lt.idp(i,j,kmax_+1,3))) ) then

              ! Initialize stop flag
              flag = 0

              ! start marching thru list, starting at first_struct
              my_struct => first_struct

              do while(associated(my_struct).and.flag.eq.0)
                 if (my_struct%id.eq.id(i,j,kmax_)) then

                    ! Make sure it is the structure that we want (may be several with same tag)
                    do n=1,my_struct%nnode
                       if (my_struct%node(n,1).eq.i.and.my_struct%node(n,2).eq.j.and.my_struct%node(n,3).eq.kmax_) flag = 1
                    end do

                    if (flag .eq.1) then 
                       ! Update global tag arrays id and idp
                       do n=1,my_struct%nnode
                          ii = my_struct%node(n,1)
                          jj = my_struct%node(n,2)
                          kk = my_struct%node(n,3)
                          ! i
                          stop_ = max(stop_,-(idp(ii,jj,kk,1)-idp(i,j,kmax_+1,1)))                          
                          idp(ii,jj,kk,1) = max(idp(ii,jj,kk,1),idp(i,j,kmax_+1,1))
                          ! j
                          stop_ = max(stop_,-(idp(ii,jj,kk,2)-idp(i,j,kmax_+1,2)))                          
                          idp(ii,jj,kk,2) = max(idp(ii,jj,kk,2),idp(i,j,kmax_+1,2))
                          ! k
                          if (kmax_.ne.kmax) then
                             stop_ = max(stop_,-(idp(ii,jj,kk,3)-idp(i,j,kmax_+1,3)))                          
                             idp(ii,jj,kk,3) = max(idp(ii,jj,kk,3),idp(i,j,kmax_+1,3))
                          end if
                       end do
                    end if
                 end if

                 ! Go to next structure
                 my_struct => my_struct%next

              end do
           end if
        end do
     end do

     ! Check if we did some changes
     call MPI_ALLREDUCE(stop_,stop_global,1,MPI_INTEGER,MPI_MAX,comm,ierr)

     ! Count how many global updates we do
     counter = counter + 1

  end do

  return

end subroutine struct_synch_per

! ----------------------------------------------- !
! Final update of struct tag id's in global array !
! Not sure we need this...perhaps for extension   !
! ----------------------------------------------- !
subroutine struct_tag_update
  use dump_struct
  use parallel
  implicit none

  integer :: i,ii,jj,kk,n_node,tag_final

  ! Start at first_struct
  my_struct => first_struct

  ! init local vars
  n_node = 0
  tag_final = 0

  ! re-init global tag array
  id = 0

  ! loop through list
  do while(associated(my_struct))
     n_node = my_struct%nnode
     tag_final = my_struct%id

     ! loop over nodes
     do i=1,n_node
        ! Update tag
        ii= my_struct%node(i,1)
        jj= my_struct%node(i,2)
        kk= my_struct%node(i,3)
        id(ii,jj,kk) = tag_final
        ! Update periodicity
        my_struct%per(1) = max(my_struct%per(1),idp(ii,jj,kk,1))
        my_struct%per(2) = max(my_struct%per(2),idp(ii,jj,kk,2))
        my_struct%per(3) = max(my_struct%per(3),idp(ii,jj,kk,3))
     end do

     my_struct => my_struct%next

  end do

  return
end subroutine struct_tag_update


! ------------------------- !
! Build array of meta_structures !
! ------------------------- !
subroutine struct_final
  use dump_struct
  use parallel
  use data

  implicit none

  integer :: i,ierr
  integer, dimension(:), pointer :: buf => null()

  ! Allocate global meta_structures array
  n_meta_struct = n_struct_max * nproc
  allocate(buf(1:n_meta_struct),meta_structures(1:n_meta_struct)) 

  ! Point to head element
  my_struct => first_struct

  ! Offset to part of array belonging to local proc
  i = (irank-1)*n_struct_max + 1

  ! Gather id of local structs
  buf = 0 
  do while(associated(my_struct))
     buf(i) = my_struct%id
     my_struct => my_struct%next
     i = i+1
  end do

  ! Collect data
  call MPI_ALLREDUCE(buf,meta_structures,n_meta_struct,MPI_INTEGER,MPI_SUM,comm,ierr)

  ! Sort, purge to single list of unique ID tags
  call meta_structures_sort

  ! Compute stats
  call meta_structures_stats

  ! Clean up
  deallocate(buf)
  nullify(buf)

  return
end subroutine struct_final

! ------------------------------------------------------------------- !
! Sort list of ID tags, purge extra elements, eliminate duplicates    !
! ------------------------------------------------------------------- !
subroutine meta_structures_sort
  use dump_struct
  use parallel
  use quicksort
  implicit none

  integer, dimension(:), pointer :: meta_structures_tmp => null()
  integer :: i,n_meta_struct_tmp

  ! Sort first
  call quick_sort_int(meta_structures)

  ! Compact list
  n_meta_struct_tmp = 1
  i = 1
  do while (i.le.n_meta_struct)
     if (meta_structures(i).eq.0) then
        i = i+1
        meta_structures(n_meta_struct_tmp) = meta_structures(i)
     elseif (meta_structures(i).gt.meta_structures(n_meta_struct_tmp)) then
        n_meta_struct_tmp = n_meta_struct_tmp + 1
        meta_structures(n_meta_struct_tmp) = meta_structures(i)
        i = i+1
     else
        i = i+1
     end if
  end do

  ! Resize meta_structures array
  n_meta_struct = n_meta_struct_tmp
  allocate(meta_structures_tmp(n_meta_struct))
  meta_structures_tmp = meta_structures(1:n_meta_struct)
  deallocate(meta_structures)
  nullify(meta_structures)
  meta_structures => meta_structures_tmp

  return
end subroutine meta_structures_sort

! -------------------------------------- !
! Compute stats for full meta_structures !
! -------------------------------------- !
subroutine meta_structures_stats
  use dump_struct
  use parallel
  use fileio
  use data
  use time_info
  use combustion
  use multiphase
  implicit none

  real(WP),dimension(:), pointer :: x_cg,y_cg,z_cg
  real(WP),dimension(:), pointer :: vol_struct_,vol_struct
  real(WP),dimension(:), pointer :: x_vol_,x_vol,y_vol_,y_vol,z_vol_,z_vol
  real(WP),dimension(:), pointer :: u_vol_,u_vol,v_vol,v_vol_,w_vol,w_vol_
  real(WP), dimension(:,:,:), pointer :: Imom_,Imom
  integer :: i,j,ii,jj,kk,ierr,iunit,var
  integer :: per_x,per_y,per_z
  real(WP) :: xtmp,ytmp,ztmp
  character(len=str_medium) :: filename

  ! Eigenvalues/eigenvectors
  real(WP),dimension(3,3) :: A,vec
  real(WP),dimension(3) :: d
  integer :: n,nrot

  ! allocate / initialize temps arrays for computation
  allocate(vol_struct(1:n_meta_struct),vol_struct_(1:n_meta_struct))
  allocate(x_vol(1:n_meta_struct),x_vol_(1:n_meta_struct))
  allocate(y_vol(1:n_meta_struct),y_vol_(1:n_meta_struct))
  allocate(z_vol(1:n_meta_struct),z_vol_(1:n_meta_struct))
  allocate(u_vol(1:n_meta_struct),u_vol_(1:n_meta_struct))
  allocate(v_vol(1:n_meta_struct),v_vol_(1:n_meta_struct))
  allocate(w_vol(1:n_meta_struct),w_vol_(1:n_meta_struct))
  allocate(Imom(1:n_meta_struct,3,3),Imom_(1:n_meta_struct,3,3))
  allocate(x_cg(1:n_meta_struct),y_cg(1:n_meta_struct),z_cg(1:n_meta_struct))

  ! initialize temp arrays for computation
  vol_struct(:) = 0.0_WP;vol_struct_(:)= 0.0_WP
  x_vol(:) = 0.0_WP;x_vol_(:) = 0.0_WP
  y_vol(:) = 0.0_WP;y_vol_(:) = 0.0_WP
  z_vol(:) = 0.0_WP;z_vol_(:) = 0.0_WP
  u_vol(:) = 0.0_WP;u_vol_(:) = 0.0_WP
  v_vol(:) = 0.0_WP;v_vol_(:) = 0.0_WP
  w_vol(:) = 0.0_WP;w_vol_(:) = 0.0_WP
  Imom(:,:,:) = 0.0_WP;Imom_(:,:,:) = 0.0_WP;

  ! fill in data (except moments of inertia)
  do i = 1,n_meta_struct
     my_struct => first_struct
     do while(associated(my_struct))
        if (my_struct%id.eq.meta_structures(i)) then

           ! Periodicity
           per_x = my_struct%per(1)
           per_y = my_struct%per(2)
           per_z = my_struct%per(3)

           do j = 1,my_struct%nnode

              ! Indices of struct node
              ii = my_struct%node(j,1)
              jj = my_struct%node(j,2)
              kk = my_struct%node(j,3)

              ! Location of struct node
              xtmp = xm(ii)-per_x*xL
              ytmp = ym(jj)-per_y*yL
              ztmp = zm(kk)-per_z*zL

              ! Volume
              vol_struct_(i) = vol_struct_(i) + vol(ii,jj,kk)*VOFavg(ii,jj,kk)

              ! Center of gravity
              x_vol_(i) = x_vol_(i) + xtmp*vol(ii,jj,kk)*VOFavg(ii,jj,kk)
              y_vol_(i) = y_vol_(i) + ytmp*vol(ii,jj,kk)*VOFavg(ii,jj,kk)
              z_vol_(i) = z_vol_(i) + ztmp*vol(ii,jj,kk)*VOFavg(ii,jj,kk)

              ! Average gas velocity inside struct
              u_vol_(i) = u_vol_(i) + U(ii,jj,kk)*vol(ii,jj,kk)*VOFavg(ii,jj,kk)
              v_vol_(i) = v_vol_(i) + V(ii,jj,kk)*vol(ii,jj,kk)*VOFavg(ii,jj,kk)
              w_vol_(i) = w_vol_(i) + W(ii,jj,kk)*vol(ii,jj,kk)*VOFavg(ii,jj,kk)

           end do
        end if
        my_struct => my_struct%next
     end do
  end do

  ! Sum parallel stats
  call parallel_sum(vol_struct_,vol_struct)
  call parallel_sum(x_vol_,x_vol)
  call parallel_sum(y_vol_,y_vol)
  call parallel_sum(z_vol_,z_vol)
  call parallel_sum(u_vol_,u_vol)
  call parallel_sum(v_vol_,v_vol)
  call parallel_sum(w_vol_,w_vol)

  ! fill in moments of inertia
  do i = 1,n_meta_struct
     my_struct => first_struct
     do while(associated(my_struct))
        if (my_struct%id.eq.meta_structures(i)) then

           ! Periodicity
           per_x = my_struct%per(1)
           per_y = my_struct%per(2)
           per_z = my_struct%per(3)

           do j = 1,my_struct%nnode

              ! Indices of struct node
              ii = my_struct%node(j,1)
              jj = my_struct%node(j,2)
              kk = my_struct%node(j,3)

              ! Location of struct node
              xtmp = xm(ii)-per_x*xL-x_vol(i)/vol_struct(i)
              ytmp = ym(jj)-per_y*yL-y_vol(i)/vol_struct(i)
              ztmp = zm(kk)-per_z*zL-z_vol(i)/vol_struct(i)

              ! Moment of Inertia
              Imom_(i,1,1) = Imom_(i,1,1) + (ytmp**2 + ztmp**2)*vol(ii,jj,kk)*VOFavg(ii,jj,kk)
              Imom_(i,2,2) = Imom_(i,2,2) + (xtmp**2 + ztmp**2)*vol(ii,jj,kk)*VOFavg(ii,jj,kk)
              Imom_(i,3,3) = Imom_(i,3,3) + (xtmp**2 + ytmp**2)*vol(ii,jj,kk)*VOFavg(ii,jj,kk)

              Imom_(i,1,2) = Imom_(i,1,2) - xtmp*ytmp*vol(ii,jj,kk)*VOFavg(ii,jj,kk)
              Imom_(i,1,3) = Imom_(i,1,3) - xtmp*ztmp*vol(ii,jj,kk)*VOFavg(ii,jj,kk)
              Imom_(i,2,3) = Imom_(i,2,3) - ytmp*ztmp*vol(ii,jj,kk)*VOFavg(ii,jj,kk)

              Imom_(i,2,1) = Imom_(i,1,2)               
              Imom_(i,3,1) = Imom_(i,1,3) 
              Imom_(i,3,2) = Imom_(i,2,3) 

           end do
        end if
        my_struct => my_struct%next
     end do
  end do

  ! Sum parallel stat on Imom
  do i=1,3
     do j=1,3
        call parallel_sum(Imom_(:,i,j),Imom(:,i,j))
     end do
  end do

  ! Allocate and initalize final list of meta_structures
  allocate(meta_structures_list(1:n_meta_struct))
  meta_structures_list(:)%id = 0
  meta_structures_list(:)%vol= 0.0_WP
  meta_structures_list(:)%x  = 0.0_WP
  meta_structures_list(:)%y  = 0.0_WP
  meta_structures_list(:)%z  = 0.0_WP
  meta_structures_list(:)%u  = 0.0_WP
  meta_structures_list(:)%v  = 0.0_WP
  meta_structures_list(:)%w  = 0.0_WP

  ! Store data
  do i=1,n_meta_struct

     ! Center of gravity
     x_cg(i) = x_vol(i)/vol_struct(i)
     y_cg(i) = y_vol(i)/vol_struct(i)
     z_cg(i) = z_vol(i)/vol_struct(i)

     ! Periodicity: transport back inside domain if needed
     if (x_cg(i).lt.x(imin)) x_cg(i) = x_cg(i)+xL
     if (y_cg(i).lt.y(jmin)) y_cg(i) = y_cg(i)+yL
     if (z_cg(i).lt.z(kmin)) z_cg(i) = z_cg(i)+zL

     ! tag, volume, location and velocity
     meta_structures_list(i)%id = meta_structures(i)
     meta_structures_list(i)%vol= vol_struct(i)
     meta_structures_list(i)%x  = x_cg(i)
     meta_structures_list(i)%y  = y_cg(i)
     meta_structures_list(i)%z  = z_cg(i)
     meta_structures_list(i)%u  = u_vol(i)/vol_struct(i)
     meta_structures_list(i)%v  = v_vol(i)/vol_struct(i)
     meta_structures_list(i)%w  = w_vol(i)/vol_struct(i)

     ! Eigenvalues/eigenvectors of moments of inertia tensor
     A = Imom(i,:,:)
     n = 3
     call eigensolver(A,n,d,vec,nrot)
     ! Get rid of very small negative values (due to machine accuracy)
     d = max(0.0_WP,d)

     ! Store characteristic lenths
     meta_structures_list(i)%lengths(1) = sqrt(5.0_WP/2.0_WP*abs(d(2)+d(3)-d(1))/vol_struct(i))
     meta_structures_list(i)%lengths(2) = sqrt(5.0_WP/2.0_WP*abs(d(3)+d(1)-d(2))/vol_struct(i))
     meta_structures_list(i)%lengths(3) = sqrt(5.0_WP/2.0_WP*abs(d(1)+d(2)-d(3))/vol_struct(i))

     ! Store principal axes
     meta_structures_list(i)%axes(:,:) = vec

  end do

  ! Write output
  if (irank.eq.iroot) then

     ! Open file with correct index
     iunit = iopen()
     filename = "structs/struct_stat."
     write(filename(len_trim(filename)+1:len_trim(filename)+6),'(i6.6)') nout_time
     open(iunit,file=filename,form="formatted",iostat=ierr,status="REPLACE")

     ! Header
     write(iunit,'(10000a20)') (trim(adjustl(meta_structures_name(var))),var=1,meta_structures_nname)

     ! Data
     do i=1,n_meta_struct
        write(iunit,'(I20,10000ES20.12)')       meta_structures_list(i)%id,        meta_structures_list(i)%vol,       &
             meta_structures_list(i)%x,         meta_structures_list(i)%y,         meta_structures_list(i)%z,         &
             meta_structures_list(i)%u,         meta_structures_list(i)%v,         meta_structures_list(i)%w,         &
             meta_structures_list(i)%lengths(1),meta_structures_list(i)%lengths(2),meta_structures_list(i)%lengths(3),&
             meta_structures_list(i)%axes(1,1), meta_structures_list(i)%axes(1,2), meta_structures_list(i)%axes(1,3), &
             meta_structures_list(i)%axes(2,1), meta_structures_list(i)%axes(2,2), meta_structures_list(i)%axes(2,3), &
             meta_structures_list(i)%axes(3,1), meta_structures_list(i)%axes(3,2), meta_structures_list(i)%axes(3,3)
     end do
     close(iclose(iunit))
  end if

  ! Log
  call monitor_log("STRUCT FILE WRITTEN")

  ! deallocate arrays
  deallocate(vol_struct_,x_vol_,y_vol_,z_vol_,u_vol_,v_vol_,w_vol_,Imom_)
  deallocate(vol_struct,x_vol,y_vol,z_vol,u_vol,v_vol,w_vol,Imom)
  deallocate(x_cg,y_cg,z_cg)
  nullify(vol_struct_,x_vol_,y_vol_,z_vol_,u_vol_,v_vol_,w_vol_,Imom_)
  nullify(vol_struct,x_vol,y_vol,z_vol,u_vol,v_vol,w_vol,Imom)
  nullify(x_cg,y_cg,z_cg)

  return
end subroutine meta_structures_stats

