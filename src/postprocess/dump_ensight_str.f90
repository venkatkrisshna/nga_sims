module dump_ensight_str
  use parallel
  use precision
  use geometry
  use partition
  use masks
  use fileio
  implicit none
  
  ! Time info
  integer :: nout_time
  real(WP), dimension(:), allocatable :: out_time
  
  ! SP buffers
  real(SP), dimension(:,:,:), allocatable :: buffer1_SP
  real(SP), dimension(:,:,:,:), allocatable :: buffer3_SP
  
  ! Fileviews
  integer :: fileview,datasize,gdatasize
  
  ! Monitoring
  integer  :: nfiles
  real(WP) :: time_open,time_close,time_write
  
end module dump_ensight_str


! ================================================= !
! Dump 3D binary ensight gold data - Initialization !
! ================================================= !
subroutine dump_ensight_str_3D_init
  use dump_ensight_str
  use parser
  use parallel
  use time_info
  implicit none
  integer :: ierr,i
  logical :: file_is_there
  integer, dimension(3) :: gsizes,lsizes,start
  
  ! Create & Start the timer
  call timing_create('ensight-str')
  call timing_start ('ensight-str')

  ! Allocate buffers
  allocate(buffer1_SP(imin_:imax_,jmin_:jmax_,kmin_:kmax_))
  allocate(buffer3_SP(imin_:imax_,jmin_:jmax_,kmin_:kmax_,3))
  
  ! Open the file
  inquire(file='ensight-3D/arts.case',exist=file_is_there)
  if (file_is_there) then
     ! Read the file
     call parser_parsefile('ensight-3D/arts.case')
     ! Get the time
     call parser_getsize('time values',nout_time)
     allocate(out_time(nout_time))
     call parser_read('time values',out_time)
     ! Remove future time
     future: do i=1,size(out_time)
        if (out_time(i).GE.time*0.99999_WP) then
           nout_time = i-1
           exit future
        end if
     end do future
  else
     ! Create directory
     if (irank.eq.iroot) call CREATE_FOLDER("ensight-3D")
     ! Set the time
     nout_time = 0
     allocate(out_time(1))
  end if
  
  ! Write the geometry
  if (irank.eq.iroot) call dump_ensight_str_3D_geometry
  
  ! Create the view
  gsizes(1) = nx
  gsizes(2) = ny
  gsizes(3) = nz
  lsizes(1) = nx_
  lsizes(2) = ny_
  lsizes(3) = nz_
  start(1) = imin_-imin
  start(2) = jmin_-jmin
  start(3) = kmin_-kmin
  datasize = lsizes(1)*lsizes(2)*lsizes(3)
  gdatasize= gsizes(1)*gsizes(2)*gsizes(3)
  call MPI_TYPE_CREATE_SUBARRAY(3,gsizes,lsizes,start,MPI_ORDER_FORTRAN,MPI_REAL_SP,fileview,ierr)
  call MPI_TYPE_COMMIT(fileview,ierr)
  
  ! Stop the timer
  call timing_stop ('ensight-str')
  
  ! Create nw file to monitor
  call monitor_create_file_step('ensight-str',4)
  call monitor_set_header(1, 'nfiles','i')
  call monitor_set_header(2, 'open [s/file]','r')
  call monitor_set_header(3, 'write [s/file]','r')
  call monitor_set_header(4, 'close [s/file]','r')
  
  return
end subroutine dump_ensight_str_3D_init


! ================================ !
! Dump 3D binary ensight gold data !
! ================================ !
subroutine dump_ensight_str_3D
  use dump_ensight_str
  use data
  use partition
  use string
  use interpolate
  use memory
  use lpt
  use combustion
  use finitechem
  use sgsmodel
  use velocity
  use onestep
  use soot
  use strainrate
  use levelset
  use multiphase
  use multiphase_vof
  use multiphase_ehd
  use pressure
  use strainrate
  use ib
  use lpt_acoustics
  implicit none
  character(len=str_short) :: name
  integer :: i,j,k,isc
  
  ! Reset timing
  nfiles = 0
  time_open  = 0.0_WP
  time_write = 0.0_WP
  time_close = 0.0_WP
  
  ! Start a timer
  call timing_start('ensight-str')

  ! Update the case file
  call dump_ensight_str_3D_case
  
  ! IB surface
  if (use_ib) then
     name="IB"
     buffer1_SP = Gib(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call dump_ensight_str_3D_scalar(buffer1_SP,name)
  end if
  
  ! Pressure
  name = "P"
  if (use_multiphase) then
     buffer1_SP = DP(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call dump_ensight_str_3D_scalar(buffer1_SP,name)
  else
     buffer1_SP = P(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call dump_ensight_str_3D_scalar(buffer1_SP,name)
  end if
     
  ! Viscosity
  name = "VISC"
  buffer1_SP = VISC(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
  call dump_ensight_str_3D_scalar(buffer1_SP,name)
  if (pope_crit) then
     name = "KSGS"
     buffer1_SP = k_sgs(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call dump_ensight_str_3D_scalar(buffer1_SP,name)
  end if
  
  ! Velocity
  name = "V"
  if (icyl.eq.0) then
     buffer3_SP(:,:,:,1) = Ui(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     buffer3_SP(:,:,:,2) = Vi(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     buffer3_SP(:,:,:,3) = Wi(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              buffer3_SP(i,j,k,1) = Ui(i,j,k)
              buffer3_SP(i,j,k,2) = Vi(i,j,k) * cos(zm(k)) - Wi(i,j,k) * sin(zm(k))
              buffer3_SP(i,j,k,3) = Vi(i,j,k) * sin(zm(k)) + Wi(i,j,k) * cos(zm(k))
           end do
        end do
     end do
  end if
  call dump_ensight_str_3D_vector(buffer3_SP,name)
  
  ! Scalars
  do isc=1,nscalar
     name = SC_name(isc)
     buffer1_SP = SC(imin_:imax_,jmin_:jmax_,kmin_:kmax_,isc)
     call dump_ensight_str_3D_scalar(buffer1_SP,name)
  end do
  if (nscalar.ge.1) then
     name = 'DIFF'
     buffer1_SP = DIFF(imin_:imax_,jmin_:jmax_,kmin_:kmax_,1)
     call dump_ensight_str_3D_scalar(buffer1_SP,name)
  end if
  
  ! Combustion
  if (combust) then
     if (isc_T.eq.0) then
        name = 'Temp'
        buffer1_SP = T(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
        call dump_ensight_str_3D_scalar(buffer1_SP,name)
     end if
     name = 'RHO'
     buffer1_SP = RHO(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call dump_ensight_str_3D_scalar(buffer1_SP,name)
     name = 'dRHO'
     buffer1_SP = dRHO(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call dump_ensight_str_3D_scalar(buffer1_SP,name)
     if (isc_ZVAR.eq.0 .and. use_sgs) then
        name = 'ZVAR'
        buffer1_SP = ZVAR(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
        call dump_ensight_str_3D_scalar(buffer1_SP,name)
     end if
     if (isc_ZMIX.ne.0) then
        name = 'CHI'
        buffer1_SP = CHI(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
        call dump_ensight_str_3D_scalar(buffer1_SP,name)
     end if
     if (trim(chemistry).eq.'one-step') then
        ! Compute mixture fraction
        call onestep_get_mixfrac
        name = 'Z'
        buffer1_SP = Mix_Frac(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
        call dump_ensight_str_3D_scalar(buffer1_SP,name)
        ! Compute ksi
        call onestep_get_flame_index
        name = 'ksi'
        buffer1_SP = ksi(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
        call dump_ensight_str_3D_scalar(buffer1_SP,name)
     end if
  end if
  
  ! Spray
  if (use_lpt) then
     ! Fluid volume fraction
     name = "EPSF"
     buffer1_SP = epsf(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call dump_ensight_str_3D_scalar(buffer1_SP,name)
     ! Density and density change
     name = 'RHO'
     buffer1_SP = RHO(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call dump_ensight_str_3D_scalar(buffer1_SP,name)
     name = 'dRHO'
     buffer1_SP = dRHO(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call dump_ensight_str_3D_scalar(buffer1_SP,name)
     ! Particles
     call dump_ensight_str_particle
  end if
  
  ! Resolved particle
  if (use_rpt) call dump_ensight_str_particle_rpt
  
  ! Level set
  if (use_lvlset) then
     name = "LVLSET"
     buffer1_SP = LVLSET(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call dump_ensight_str_3D_scalar(buffer1_SP,name)
  end if
  
  ! Multiphase
  if (use_multiphase) then
     name = "VOF"
     buffer1_SP = VOFavg(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call dump_ensight_str_3D_scalar(buffer1_SP,name)
     name = "curv"
     buffer1_SP = curv(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call dump_ensight_str_3D_scalar(buffer1_SP,name)
     ! name = "norm"
     ! buffer3_SP(:,:,:,1) = normx(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     ! buffer3_SP(:,:,:,2) = normy(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     ! buffer3_SP(:,:,:,3) = normz(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     ! call dump_ensight_str_3D_vector(buffer3_SP,name)
     name = "G"
     buffer1_SP = G(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call dump_ensight_str_3D_scalar(buffer1_SP,name)
  end if
  
  ! EHD
  if (use_ehd) then
     name = "phi"
     buffer1_SP = phi(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call dump_ensight_str_3D_scalar(buffer1_SP,name)
     name = "E"
     buffer3_SP(:,:,:,1) = Ex(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     buffer3_SP(:,:,:,2) = Ey(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     buffer3_SP(:,:,:,3) = Ez(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call dump_ensight_str_3D_vector(buffer3_SP,name)
  end if
  
  ! Soot
  if (use_soot) then
     name = "numdens"
     buffer1_SP = numdens(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call dump_ensight_str_3D_scalar(buffer1_SP,name)
     name = "fv"
     buffer1_SP = fv(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call dump_ensight_str_3D_scalar(buffer1_SP,name)
     name = "partdiam"
     buffer1_SP = partdiam(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call dump_ensight_str_3D_scalar(buffer1_SP,name)
     name = "partaggr"
     buffer1_SP = partaggr(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call dump_ensight_str_3D_scalar(buffer1_SP,name)
     name = "surfreac"
     buffer1_SP = surfreac(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call dump_ensight_str_3D_scalar(buffer1_SP,name)
  end if
  
  ! Acoustics
  if (use_acoustics) then
     name = "AcForce"
     buffer3_SP(:,:,:,1) = AcAccx(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     buffer3_SP(:,:,:,2) = AcAccy(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     buffer3_SP(:,:,:,3) = AcAccz(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call dump_ensight_str_3D_vector(buffer3_SP,name)
  end if
  
  ! Divergence
  name = "divg"
  buffer1_SP = divg
  call dump_ensight_str_3D_scalar(buffer1_SP,name)
  
!!$  ! PQR criterion
!!$  call strainrate_PQRcrit
!!$  name = 'Pcrit'
!!$  buffer1_SP = Pcrit(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
!!$  call dump_ensight_str_3D_scalar(buffer1_SP,name)
!!$  name = 'Qcrit'
!!$  buffer1_SP = Qcrit(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
!!$  call dump_ensight_str_3D_scalar(buffer1_SP,name)
!!$  name = 'Rcrit'
!!$  buffer1_SP = Rcrit(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
!!$  call dump_ensight_str_3D_scalar(buffer1_SP,name)
  
  ! Stop a timer
  call timing_stop('ensight-str')

  ! Transfer the values to monitor
  call monitor_select_file('ensight-str')
  call monitor_set_single_value(1,real(nfiles,WP))
  call monitor_set_single_value(2,time_open /real(nfiles,WP))
  call monitor_set_single_value(3,time_write/real(nfiles,WP))
  call monitor_set_single_value(4,time_close/real(nfiles,WP))

  return
end subroutine dump_ensight_str_3D


! =========================================== !
! Dump 3D binary ensight gold data - geometry !
! => one processor only - test before         !
! =========================================== !
subroutine dump_ensight_str_3D_geometry
  use dump_ensight_str
  implicit none
  integer :: ierr,ipart,iunit,i,j,k,ii,jj,kk
  integer, dimension(:,:,:), allocatable :: iblank
  character(len=80) :: buffer
  real(SP), dimension(:,:,:), allocatable :: xbuf,ybuf,zbuf
  real(SP) :: max_x,max_y,max_z
  real(SP) :: min_x,min_y,min_z
  
  ! Get single precision mesh
  allocate(xbuf(1:nx+1,1:ny+1,1:nz+1),ybuf(1:nx+1,1:ny+1,1:nz+1),zbuf(1:nx+1,1:ny+1,1:nz+1))
  if (icyl.eq.0) then
     do i=imin,imax+1
        do j=jmin,jmax+1
           do k=kmin,kmax+1
              ii=i-imin+1
              jj=j-jmin+1
              kk=k-kmin+1
              xbuf(ii,jj,kk)=x(i)
              ybuf(ii,jj,kk)=y(j)
              zbuf(ii,jj,kk)=z(k)
           end do
        end do
     end do
  else
     do i=imin,imax+1
        do j=jmin,jmax+1
           do k=kmin,kmax+1
              ii=i-imin+1
              jj=j-jmin+1
              kk=k-kmin+1
              xbuf(ii,jj,kk)=x(i)
              ybuf(ii,jj,kk)=y(j)*cos(z(k))
              zbuf(ii,jj,kk)=y(j)*sin(z(k))
           end do
        end do
     end do
  end if
  max_x = maxval(xbuf)
  max_y = maxval(ybuf)
  max_z = maxval(zbuf)
  min_x = minval(xbuf)
  min_y = minval(ybuf)
  min_z = minval(zbuf)
  
  ! Get the iblank in 3D
  allocate(iblank(nx+1,ny+1,nz+1))
  do k=1,nz+1
     iblank(1:nx+1,1:ny+1,k) = 1-mask_node(imin:imax+1,jmin:jmax+1)
  end do
  
  ! Open the file
  call BINARY_FILE_OPEN(iunit,"ensight-3D/geometry","w",ierr)
  
  ! Write the geometry
  buffer = 'C Binary'
  call BINARY_FILE_WRITE(iunit,buffer,80,kind(buffer),ierr)
  buffer = 'Ensight Gold Geometry File'
  call BINARY_FILE_WRITE(iunit,buffer,80,kind(buffer),ierr)
  buffer = 'Structured Geometry from ARTS'
  call BINARY_FILE_WRITE(iunit,buffer,80,kind(buffer),ierr)
  buffer = 'node id off'
  call BINARY_FILE_WRITE(iunit,buffer,80,kind(buffer),ierr)
  buffer = 'element id off'
  call BINARY_FILE_WRITE(iunit,buffer,80,kind(buffer),ierr)
  
  buffer = 'extents'
  call BINARY_FILE_WRITE(iunit,buffer,80,kind(buffer),ierr)
  call BINARY_FILE_WRITE(iunit,min_x,1,kind(min_x),ierr)
  call BINARY_FILE_WRITE(iunit,max_x,1,kind(max_x),ierr)
  call BINARY_FILE_WRITE(iunit,min_y,1,kind(min_y),ierr)
  call BINARY_FILE_WRITE(iunit,max_y,1,kind(max_y),ierr)
  call BINARY_FILE_WRITE(iunit,min_z,1,kind(min_z),ierr)
  call BINARY_FILE_WRITE(iunit,max_z,1,kind(max_z),ierr)
  
  buffer = 'part'
  call BINARY_FILE_WRITE(iunit,buffer,80,kind(buffer),ierr)
  ipart = 1
  call BINARY_FILE_WRITE(iunit,ipart,1,kind(ipart),ierr)
  
  buffer = 'Complete geometry'
  call BINARY_FILE_WRITE(iunit,buffer,80,kind(buffer),ierr)
  buffer = 'block curvilinear iblanked'
  call BINARY_FILE_WRITE(iunit,buffer,80,kind(buffer),ierr)
  
  call BINARY_FILE_WRITE(iunit,nx+1,1,kind(nx),ierr)
  call BINARY_FILE_WRITE(iunit,ny+1,1,kind(ny),ierr)
  call BINARY_FILE_WRITE(iunit,nz+1,1,kind(nz),ierr)
  
  call BINARY_FILE_WRITE(iunit,xbuf,(nx+1)*(ny+1)*(nz+1),kind(xbuf),ierr)
  call BINARY_FILE_WRITE(iunit,ybuf,(nx+1)*(ny+1)*(nz+1),kind(ybuf),ierr)
  call BINARY_FILE_WRITE(iunit,zbuf,(nx+1)*(ny+1)*(nz+1),kind(zbuf),ierr)
  call BINARY_FILE_WRITE(iunit,iblank,(nx+1)*(ny+1)*(nz+1),kind(iblank),ierr)
  
  ! Close the file
  call BINARY_FILE_CLOSE(iunit,ierr)
  
  return
end subroutine dump_ensight_str_3D_geometry


! ============================================ !
! Dump 3D binary ensight gold data - case file !
! ============================================ !
subroutine dump_ensight_str_3D_case
  use dump_ensight_str
  use lpt
  use combustion
  use sgsmodel
  use time_info
  use string
  use rpt
  use multiphase_ehd
  use lpt_acoustics
  implicit none
  integer :: iunit,ierr,isc
  real(WP), dimension(:), allocatable :: buffer
  character(len=80) :: str
  
  ! Update the time info
  allocate(buffer(nout_time))
  buffer(1:nout_time) = out_time(1:nout_time)
  deallocate(out_time)
  nout_time = nout_time + 1
  allocate(out_time(nout_time))
  out_time(1:nout_time-1) = buffer(1:nout_time-1)
  out_time(nout_time) = time
  
  ! Write - Single proc & parallel => only root writes (in ASCII)
  if (irank==iroot) then
     ! Open the file
     iunit = iopen()
     open(iunit,file="ensight-3D/arts.case",form="formatted",iostat=ierr,status="REPLACE")
     ! Write the case
     str='FORMAT'
     write(iunit,'(a80)') str
     str='type: ensight gold'
     write(iunit,'(a80)') str
     str='GEOMETRY'
     write(iunit,'(a80)') str
     str='model: geometry'
     write(iunit,'(a80)') str
     str='measured: 1 particles/particles.******'
     if (use_lpt .or. use_rpt) write(iunit,'(a80)') str
     str='VARIABLE'
     write(iunit,'(a80)') str
     ! IB
     if (use_IB) then
        str='scalar per element: 1 IB IB/IB.******'
        write(iunit,'(a80)') str
     end if
     ! Pressure
     str='scalar per element: 1 Pressure P/P.******'
     write(iunit,'(a80)') str
     ! Viscosity
     str='scalar per element: 1 Viscosity VISC/VISC.******'
     write(iunit,'(a80)') str
     if (pope_crit) then
        str='scalar per element: 1 KSGS KSGS/KSGS.******'
        write(iunit,'(a80)') str
     end if
     ! Diffusivity
     if (nscalar.ge.1) then
        str='scalar per element: 1 Diffusivity DIFF/DIFF.******'
        write(iunit,'(a80)') str
     end if
     ! Scalars
     do isc=1,nscalar
        str = 'scalar per element: 1 '//trim(SC_name(isc))//' '//trim(SC_name(isc))//'/'//trim(SC_name(isc))//'.******'
        write(iunit,'(a80)') str
     end do
     ! Chemistry
     if (combust) then
        if (isc_T.eq.0) then
           str = 'scalar per element: 1 Temp Temp/Temp.******'
           write(iunit,'(a80)') str
        end if
        str = 'scalar per element: 1 RHO RHO/RHO.******'
        write(iunit,'(a80)') str
        str = 'scalar per element: 1 dRHO dRHO/dRHO.******'
        write(iunit,'(a80)') str
        str = 'scalar per element: 1 ZVAR ZVAR/ZVAR.******'
        if (isc_ZVAR.eq.0 .and. use_sgs) write(iunit,'(a80)') str
        str = 'scalar per element: 1 CHI CHI/CHI.******'
        if (isc_ZMIX.ne.0) write(iunit,'(a80)') str
        if (trim(chemistry).eq.'one-step') then
           str = 'scalar per element: 1 Z Z/Z.******'
           write(iunit,'(a80)') str
           str = 'scalar per element: 1 ksi ksi/ksi.******'
           write(iunit,'(a80)') str
        end if
     end if
     ! Velocity
     str='vector per element: 1 V V/V.******'
     write(iunit,'(a80)') str
     ! Spray
     if (use_lpt) then
        str='scalar per element: 1 EPSF EPSF/EPSF.******'
        write(iunit,'(a80)') str
        str = 'scalar per element: 1 RHO RHO/RHO.******'
        write(iunit,'(a80)') str
        str = 'scalar per element: 1 dRHO dRHO/dRHO.******'
        write(iunit,'(a80)') str
        str='scalar per measured node: 1 id particles/id.******'
        write(iunit,'(a80)') str
        str='scalar per measured node: 1 Diameter particles/diameter.******'
        write(iunit,'(a80)') str
        str='vector per measured node: 1 Velocity particles/velocity.******'
        write(iunit,'(a80)') str
        str='vector per measured node: 1 AngVel particles/angvel.******'
        write(iunit,'(a80)') str
     end if
     ! Resolved particles
     if (use_rpt) then
        str='scalar per measured node: 1 Diameter particles/diameter.******'
        write(iunit,'(a80)') str
        str='vector per measured node: 1 Velocity particles/velocity.******'
        write(iunit,'(a80)') str
     end if
     ! Level set stuff
     if (use_lvlset) then
        str='scalar per element: 1 LVLSET LVLSET/LVLSET.******'
        write(iunit,'(a80)') str
     end if
     ! Multiphase stuff
     if (use_multiphase) then
        str='scalar per element: 1 VOF VOF/VOF.******'
        write(iunit,'(a80)') str
        str='scalar per element: 1 curv curv/curv.******'
        write(iunit,'(a80)') str
        str='vector per element: 1 norm norm/norm.******'
        write(iunit,'(a80)') str
        str='scalar per element: 1 G G/G.******'
        write(iunit,'(a80)') str
     end if
     ! EHD stuff
     if (use_ehd) then
        str='scalar per element: 1 phi phi/phi.******'
        write(iunit,'(a80)') str
        str='vector per element: 1 E E/E.******'
        write(iunit,'(a80)') str
     end if
     ! Soot stuff
     if (use_soot) then
        str='scalar per element: 1 numdens numdens/numdens.******'
        write(iunit,'(a80)') str
        str='scalar per element: 1 fv fv/fv.******'
        write(iunit,'(a80)') str
        str='scalar per element: 1 partdiam partdiam/partdiam.******'
        write(iunit,'(a80)') str
        str='scalar per element: 1 partaggr partaggr/partaggr.******'
        write(iunit,'(a80)') str
        str='scalar per element: 1 surfreac surfreac/surfreac.******'
        write(iunit,'(a80)') str
     end if
     ! Acoustics
     if (use_acoustics) then
        str='vector per element: 1 AcForce AcForce/AcForce.******'
        write(iunit,'(a80)') str
     end if
     ! Divergence
     str='scalar per element: 1 divg divg/divg.******'
     write(iunit,'(a80)') str
!!$     ! PQR criterion
!!$     str='scalar per element: 1 Pcrit Pcrit/Pcrit.******'
!!$     write(iunit,'(a80)') str
!!$     str='scalar per element: 1 Qcrit Qcrit/Qcrit.******'
!!$     write(iunit,'(a80)') str
!!$     str='scalar per element: 1 Rcrit Rcrit/Rcrit.******'
!!$     write(iunit,'(a80)') str
     ! Time section
     str='TIME'
     write(iunit,'(a80)') str
     str='time set: 1'
     write(iunit,'(a80)') str
     str='number of steps:'
     write(iunit,'(a16,x,i12)') str,nout_time
     str='filename start number: 1'
     write(iunit,'(a80)') str
     str='filename increment: 1'
     write(iunit,'(a80)') str
     str='time values:'
     write(iunit,'(a12,x,10000000(3(ES12.5,x),/))') str,out_time
     ! Close the file
     close(iclose(iunit))
  end if
  
  return
end subroutine dump_ensight_str_3D_case


! ========================================= !
! Dump 3D binary ensight gold data - scalar !
! ========================================= !
subroutine dump_ensight_str_3D_scalar(scalar,name)
  use dump_ensight_str
  use string
  use data
  use parallel
  implicit none
  integer :: iunit,ierr,size,ibuffer
  character(len=80) :: buffer
  character(len=str_short) :: name
  character(len=str_medium) :: file
  real(SP), dimension(imin_:imax_,jmin_:jmax_,kmin_:kmax_), intent(in) :: scalar
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer(kind=MPI_OFFSET_KIND) :: disp
  logical :: file_is_there
  
  ! Generate the file
  if (irank.eq.iroot) call CREATE_FOLDER("ensight-3D/" // trim(adjustl(name)))
  file = trim(mpiiofs)//"ensight-3D/" // trim(adjustl(name)) // "/" // trim(adjustl(name)) // "."
  write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nout_time
  
  ! Open the file
  inquire(file=file,exist=file_is_there)
  if (file_is_there .and. irank.eq.iroot) call MPI_FILE_DELETE(file,mpi_info,ierr)
  time_open = time_open-MPI_Wtime()
  call MPI_FILE_OPEN(comm,file,IOR(MPI_MODE_WRONLY,MPI_MODE_CREATE),mpi_info,iunit,ierr)
  time_open = time_open+MPI_Wtime()
  
  ! Write header (only root)
  time_write = time_write-MPI_Wtime()
  if (irank.eq.iroot) then
     buffer = trim(adjustl(name))
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
     buffer = 'part'
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
     ibuffer = 1
     size = 1
     call MPI_FILE_WRITE(iunit,ibuffer,size,MPI_INTEGER,status,ierr)
     buffer = 'block'
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
  end if
  
  ! Write the file
  disp = 3*80+4
  call MPI_FILE_SET_VIEW(iunit,disp,MPI_REAL_SP,fileview,"native",mpi_info,ierr)
  call MPI_FILE_WRITE_ALL(iunit,scalar,datasize,MPI_REAL_SP,status,ierr)
  time_write = time_write+MPI_Wtime()
  
  ! Close the file
  time_close = time_close-MPI_Wtime()
  call MPI_FILE_CLOSE(iunit,ierr)
  time_close = time_close+MPI_Wtime()
  
  ! One more file written
  nfiles = nfiles+1
  
  return
end subroutine dump_ensight_str_3D_scalar


! ========================================= !
! Dump 3D binary ensight gold data - vector !
! ========================================= !
subroutine dump_ensight_str_3D_vector(vec,name)
  use dump_ensight_str
  use string
  use parallel
  implicit none
  integer :: iunit,ierr,size,ibuffer
  character(len=80) :: buffer
  character(len=str_short) :: name
  character(len=str_medium) :: file
  real(SP), dimension(imin_:imax_,jmin_:jmax_,kmin_:kmax_,1:3), intent(in) :: vec
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer(kind=MPI_OFFSET_KIND) :: disp
  logical :: file_is_there
  integer :: var
  
  ! Generate the file
  if (irank.eq.iroot) call CREATE_FOLDER("ensight-3D/" // trim(adjustl(name)))
  file = trim(mpiiofs)//"ensight-3D/" // trim(adjustl(name)) // "/" // trim(adjustl(name)) // "."
  write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nout_time
  
  ! Open the file
  inquire(file=file,exist=file_is_there)
  if (file_is_there .and. irank.eq.iroot) call MPI_FILE_DELETE(file,mpi_info,ierr)
  time_open = time_open-MPI_Wtime()
  call MPI_FILE_OPEN(comm,file,IOR(MPI_MODE_WRONLY,MPI_MODE_CREATE),mpi_info,iunit,ierr)
  time_open = time_open+MPI_Wtime()
  
  ! Write header (only root)
  time_write = time_write-MPI_Wtime()
  if (irank.eq.iroot) then
     buffer = trim(adjustl(name))
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
     buffer = 'part'
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
     ibuffer = 1
     size = 1
     call MPI_FILE_WRITE(iunit,ibuffer,size,MPI_INTEGER,status,ierr)
     buffer = 'block'
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
  end if
  
  ! Write the data
  do var=1,3
     disp = 3*80+4+(var-1)*gdatasize*4
     call MPI_FILE_SET_VIEW(iunit,disp,MPI_REAL_SP,fileview,"native",mpi_info,ierr)
     call MPI_FILE_WRITE_ALL(iunit,vec(:,:,:,var),datasize,MPI_REAL_SP,status,ierr)
  end do
  time_write = time_write+MPI_Wtime()
  
  ! Close the file
  time_close = time_close-MPI_Wtime()
  call MPI_FILE_CLOSE(iunit,ierr)
  time_close = time_close+MPI_Wtime()
  
  ! One more file written
  nfiles = nfiles+1
  
  return
end subroutine dump_ensight_str_3D_vector


! ============================================ !
! Dump 3D binary ensight gold data - particles !
! ============================================ !
subroutine dump_ensight_str_particle
  use dump_ensight_str
  use lpt
  use string
  use parallel
  implicit none
  
  integer :: iunit,ierr,i,rank
  character(len=str_medium) :: file
  character(len=80) :: cbuffer
  integer :: ibuffer
  real(SP) :: rbuffer
  
  ! Generate the file - Particles
  if (irank.eq.iroot) call CREATE_FOLDER("ensight-3D/" // 'particles')
  file = "ensight-3D/" // 'particles' // "/" // 'particles' // "."
  write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nout_time
  ! Write positions
  do rank=1,nproc
     if (rank.eq.irank) then
        if (rank.eq.1) then
           ! Open the file
           call BINARY_FILE_OPEN(iunit,trim(file),"w",ierr)
           ! Write header
           cbuffer = 'C Binary'
           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
           cbuffer = 'Spray coordinates from ARTS'
           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
           cbuffer = 'particle coordinates'
           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
           ibuffer = max(npart,1)
           call BINARY_FILE_WRITE(iunit,ibuffer,1,kind(ibuffer),ierr)
           do i=1,npart
              ibuffer = i
              call BINARY_FILE_WRITE(iunit,ibuffer,1,kind(ibuffer),ierr)
           end do
           ! Spray coordinates
           do i=1,npart_
              rbuffer = part(i)%x
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%y
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%z
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end do
           ! Zero drop case
           if (npart.eq.0) then
              ibuffer = i
              call BINARY_FILE_WRITE(iunit,ibuffer,1,kind(ibuffer),ierr)
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end if
           ! Close the file
           call BINARY_FILE_CLOSE(iunit,ierr)
        else
           ! Open the file
           call BINARY_FILE_OPEN(iunit,trim(file),"a",ierr)
           ! Spray coordinates
           do i=1,npart_
              rbuffer = part(i)%x
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%y
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%z
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end do
           ! Close the file
           call BINARY_FILE_CLOSE(iunit,ierr)
        end if
     end if
     call MPI_BARRIER(comm,ierr)
  end do
  
  ! Generate the file - Particle ID
  if (irank.eq.iroot) call CREATE_FOLDER("ensight-3D/" // 'particles')
  file = "ensight-3D/" // 'particles' // "/" // 'id' // "."
  write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nout_time
  ! Write particle ID
  do rank=1,nproc
     if (rank.eq.irank) then
        if (rank.eq.1) then
           ! Open the file
           call BINARY_FILE_OPEN(iunit,trim(file),"w",ierr)
           ! Write header
           cbuffer = 'Spray ID'
           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
           ! Spray ID
           do i=1,npart_
              rbuffer = real(part(i)%id,WP)
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end do
           ! Zero drop case
           if (npart.eq.0) then
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end if
           ! Close the file
           call BINARY_FILE_CLOSE(iunit,ierr)
        else
           ! Open the file
           call BINARY_FILE_OPEN(iunit,trim(file),"a",ierr)
           ! Spray ID
           do i=1,npart_
              rbuffer = real(part(i)%id,WP)
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end do
           ! Close the file
           call BINARY_FILE_CLOSE(iunit,ierr)
        end if
     end if
     call MPI_BARRIER(comm,ierr)
  end do
  
  ! Generate the file - Diameters
  if (irank.eq.iroot) call CREATE_FOLDER("ensight-3D/" // 'particles')
  file = "ensight-3D/" // 'particles' // "/" // 'diameter' // "."
  write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nout_time
  ! Write diameters
  do rank=1,nproc
     if (rank.eq.irank) then
        if (rank.eq.1) then
           ! Open the file
           call BINARY_FILE_OPEN(iunit,trim(file),"w",ierr)
           ! Write header
           cbuffer = 'Spray diameter'
           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
           ! Spray diameter
           do i=1,npart_
              rbuffer = part(i)%d
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end do
           ! Zero drop case
           if (npart.eq.0) then
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end if
           ! Close the file
           call BINARY_FILE_CLOSE(iunit,ierr)
        else
           ! Open the file
           call BINARY_FILE_OPEN(iunit,trim(file),"a",ierr)
           ! Spray diameter
           do i=1,npart_
              rbuffer = part(i)%d
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end do
           ! Close the file
           call BINARY_FILE_CLOSE(iunit,ierr)
        end if
     end if
     call MPI_BARRIER(comm,ierr)
  end do
  
  ! Generate the file - Velocity
  if (irank.eq.iroot) call CREATE_FOLDER("ensight-3D/" // 'particles')
  file = "ensight-3D/" // 'particles' // "/" // 'velocity' // "."
  write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nout_time
  ! Write velocity
  do rank=1,nproc
     if (rank.eq.irank) then
        if (rank.eq.1) then
           ! Open the file
           call BINARY_FILE_OPEN(iunit,trim(file),"w",ierr)
           ! Write header
           cbuffer = 'Spray velocity'
           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
           ! Spray velocity
           do i=1,npart_
              rbuffer = part(i)%u
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%v
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%w
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end do
           ! Zero drop case
           if (npart.eq.0) then
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end if
           ! Close the file
           call BINARY_FILE_CLOSE(iunit,ierr)
        else
           ! Open the file
           call BINARY_FILE_OPEN(iunit,trim(file),"a",ierr)
           ! Spray velocity
           do i=1,npart_
              rbuffer = part(i)%u
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%v
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%w
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end do
           ! Close the file
           call BINARY_FILE_CLOSE(iunit,ierr)
        end if
     end if
     call MPI_BARRIER(comm,ierr)
  end do
  
  ! Generate the file - Angular Velocity
  if (irank.eq.iroot) call CREATE_FOLDER("ensight-3D/" // 'particles')
  file = "ensight-3D/" // 'particles' // "/" // 'angvel' // "."
  write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nout_time
  ! Write velocity
  do rank=1,nproc
     if (rank.eq.irank) then
        if (rank.eq.1) then
           ! Open the file
           call BINARY_FILE_OPEN(iunit,trim(file),"w",ierr)
           ! Write header
           cbuffer = 'Spray angular velocity'
           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
           ! Spray velocity
           do i=1,npart_
              rbuffer = part(i)%wx
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%wy
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%wz
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end do
           ! Zero drop case
           if (npart.eq.0) then
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end if
           ! Close the file
           call BINARY_FILE_CLOSE(iunit,ierr)
        else
           ! Open the file
           call BINARY_FILE_OPEN(iunit,trim(file),"a",ierr)
           ! Spray angular velocity
           do i=1,npart_
              rbuffer = part(i)%wx
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%wy
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%wz
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end do
           ! Close the file
           call BINARY_FILE_CLOSE(iunit,ierr)
        end if
     end if
     call MPI_BARRIER(comm,ierr)
  end do
  
  return
end subroutine dump_ensight_str_particle


!!$! ============================================= !
!!$! Dump 3D binary ensight gold data - quadrature !
!!$! ============================================= !
!!$subroutine dump_ensight_str_quadrature
!!$  use dump_ensight_str
!!$  use multiphase_sr
!!$  use multiphase_mk
!!$  use string
!!$  use parallel
!!$  implicit none
!!$  
!!$  integer :: iunit,ierr,rank
!!$  character(len=str_medium) :: file
!!$  character(len=80) :: cbuffer
!!$  integer :: ibuffer
!!$  real(SP) :: rbuffer
!!$  integer :: i,j,k
!!$  integer :: ii,jj,kk
!!$  integer :: quad_count,my_quad_count
!!$  
!!$  ! Count the quadrature points
!!$  my_quad_count=0
!!$  do k=kmin_,kmax_
!!$     do j=jmin_,jmax_
!!$        do i=imin_,imax_
!!$           my_quad_count=my_quad_count+Gsr(i,j,k)%px*Gsr(i,j,k)%py*Gsr(i,j,k)%pz
!!$           !my_quad_count=my_quad_count+Gmk(i,j,k)%nmk
!!$        end do
!!$     end do
!!$  end do
!!$  call parallel_sum(my_quad_count,quad_count)
!!$
!!$  ! Generate the file - Quadrature points coordinates
!!$  if (irank.eq.iroot) call CREATE_FOLDER("ensight-3D/" // 'quadrature')
!!$  file = "ensight-3D/" // 'quadrature' // "/" // 'quadrature' // "."
!!$  write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nout_time
!!$  ! Write positions
!!$  do rank=1,nproc
!!$     if (rank.eq.irank) then
!!$        if (rank.eq.1) then
!!$           ! Open the file
!!$           call BINARY_FILE_OPEN(iunit,trim(file),"w",ierr)
!!$           ! Write header
!!$           cbuffer = 'C Binary'
!!$           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
!!$           cbuffer = 'Quadrature coordinates from ARTS'
!!$           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
!!$           cbuffer = 'particle coordinates'
!!$           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
!!$           ibuffer = max(quad_count,1)
!!$           call BINARY_FILE_WRITE(iunit,ibuffer,1,kind(ibuffer),ierr)
!!$           do i=1,max(quad_count,1)
!!$              ibuffer = i
!!$              call BINARY_FILE_WRITE(iunit,ibuffer,1,kind(ibuffer),ierr)
!!$           end do
!!$           ! Quadrature points coordinates
!!$           do k=kmin_,kmax_
!!$              do j=jmin_,jmax_
!!$                 do i=imin_,imax_
!!$                    ! Get each quadrature point
!!$                    do kk=1,Gsr(i,j,k)%pz
!!$                       do jj=1,Gsr(i,j,k)%py
!!$                          do ii=1,Gsr(i,j,k)%px
!!$                             ! Get the position
!!$                             rbuffer = x(i)+quad(Gsr(i,j,k)%px)%x(ii)*dx(i)
!!$                             call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                             rbuffer = y(j)+quad(Gsr(i,j,k)%py)%x(jj)*dy(j)
!!$                             call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                             rbuffer = z(k)+quad(Gsr(i,j,k)%pz)%x(kk)*dz
!!$                             call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                          end do
!!$                       end do
!!$                    end do
!!$                    !do ii=1,Gmk(i,j,k)%nmk
!!$                    !   rbuffer=Gmk(i,j,k)%mk(ii)%x
!!$                    !   call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                    !   rbuffer=Gmk(i,j,k)%mk(ii)%y
!!$                    !   call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                    !   rbuffer=Gmk(i,j,k)%mk(ii)%z
!!$                    !   call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                    !end do
!!$                 end do
!!$              end do
!!$           end do
!!$           ! Zero points case
!!$           if (quad_count.eq.0) then
!!$              rbuffer = 0.0_WP
!!$              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$              rbuffer = 0.0_WP
!!$              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$              rbuffer = 0.0_WP
!!$              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$           end if
!!$           ! Close the file
!!$           call BINARY_FILE_CLOSE(iunit,ierr)
!!$        else
!!$           ! Open the file
!!$           call BINARY_FILE_OPEN(iunit,trim(file),"a",ierr)
!!$           ! Quadrature points coordinates
!!$           do k=kmin_,kmax_
!!$              do j=jmin_,jmax_
!!$                 do i=imin_,imax_
!!$                    ! Get each quadrature point
!!$                    do kk=1,Gsr(i,j,k)%pz
!!$                       do jj=1,Gsr(i,j,k)%py
!!$                          do ii=1,Gsr(i,j,k)%px
!!$                             ! Get the position
!!$                             rbuffer = x(i)+quad(Gsr(i,j,k)%px)%x(ii)*dx(i)
!!$                             call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                             rbuffer = y(j)+quad(Gsr(i,j,k)%py)%x(jj)*dy(j)
!!$                             call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                             rbuffer = z(k)+quad(Gsr(i,j,k)%pz)%x(kk)*dz
!!$                             call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                          end do
!!$                       end do
!!$                    end do
!!$                    !do ii=1,Gmk(i,j,k)%nmk
!!$                    !   rbuffer=Gmk(i,j,k)%mk(ii)%x
!!$                    !   call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                    !   rbuffer=Gmk(i,j,k)%mk(ii)%y
!!$                    !   call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                    !   rbuffer=Gmk(i,j,k)%mk(ii)%z
!!$                    !   call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                    !end do
!!$                 end do
!!$              end do
!!$           end do
!!$           ! Close the file
!!$           call BINARY_FILE_CLOSE(iunit,ierr)
!!$        end if
!!$     end if
!!$     call MPI_BARRIER(comm,ierr)
!!$  end do
!!$  
!!$  ! Generate the file - G value
!!$  if (irank.eq.iroot) call CREATE_FOLDER("ensight-3D/" // 'quadrature')
!!$  file = "ensight-3D/" // 'quadrature' // "/" // 'Gsr' // "."
!!$  write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nout_time
!!$  ! Write diameters
!!$  do rank=1,nproc
!!$     if (rank.eq.irank) then
!!$        if (rank.eq.1) then
!!$           ! Open the file
!!$           call BINARY_FILE_OPEN(iunit,trim(file),"w",ierr)
!!$           ! Write header
!!$           cbuffer = 'G value'
!!$           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
!!$           ! G value
!!$           do k=kmin_,kmax_
!!$              do j=jmin_,jmax_
!!$                 do i=imin_,imax_
!!$                    ! Get each quadrature point
!!$                    do kk=1,Gsr(i,j,k)%pz
!!$                       do jj=1,Gsr(i,j,k)%py
!!$                          do ii=1,Gsr(i,j,k)%px
!!$                             ! Get the G value
!!$                             rbuffer = Gsr(i,j,k)%G(ii,jj,kk)
!!$                             call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                          end do
!!$                       end do
!!$                    end do
!!$                    !do ii=1,Gmk(i,j,k)%nmk
!!$                    !   rbuffer=Gmk(i,j,k)%mk(ii)%normx
!!$                    !   call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                    !   rbuffer=Gmk(i,j,k)%mk(ii)%normy
!!$                    !   call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                    !   rbuffer=Gmk(i,j,k)%mk(ii)%normz
!!$                    !   call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                    !end do
!!$                 end do
!!$              end do
!!$           end do
!!$           ! Zero points case
!!$           if (quad_count.eq.0) then
!!$              rbuffer = 0.0_WP
!!$              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$              !rbuffer = 0.0_WP
!!$              !call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$              !rbuffer = 0.0_WP
!!$              !call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$           end if
!!$           ! Close the file
!!$           call BINARY_FILE_CLOSE(iunit,ierr)
!!$        else
!!$           ! Open the file
!!$           call BINARY_FILE_OPEN(iunit,trim(file),"a",ierr)
!!$           ! G value
!!$           do k=kmin_,kmax_
!!$              do j=jmin_,jmax_
!!$                 do i=imin_,imax_
!!$                    ! Get each quadrature point
!!$                    do kk=1,Gsr(i,j,k)%pz
!!$                       do jj=1,Gsr(i,j,k)%py
!!$                          do ii=1,Gsr(i,j,k)%px
!!$                             ! Get the G value
!!$                             rbuffer = Gsr(i,j,k)%G(ii,jj,kk)
!!$                             call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                          end do
!!$                       end do
!!$                    end do
!!$                    !do ii=1,Gmk(i,j,k)%nmk
!!$                    !   rbuffer=Gmk(i,j,k)%mk(ii)%normx
!!$                    !   call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                    !   rbuffer=Gmk(i,j,k)%mk(ii)%normy
!!$                    !   call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                    !   rbuffer=Gmk(i,j,k)%mk(ii)%normz
!!$                    !   call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                    !end do
!!$                 end do
!!$              end do
!!$           end do 
!!$           ! Close the file
!!$           call BINARY_FILE_CLOSE(iunit,ierr)
!!$        end if
!!$     end if
!!$     call MPI_BARRIER(comm,ierr)
!!$  end do
!!$  
!!$  return
!!$end subroutine dump_ensight_str_quadrature


! ============================= !
! Dump 3D particle data for RPT !
! ============================= !
subroutine dump_ensight_str_particle_rpt
  use dump_ensight_str
  use rpt
  use string
  use parallel
  implicit none
  
  integer :: iunit,ierr,i,rank
  character(len=str_medium) :: file
  character(len=80) :: cbuffer
  integer :: ibuffer
  real(SP) :: rbuffer
  
  ! Generate the file - Particles
  if (irank.eq.iroot) then
     call CREATE_FOLDER("ensight-3D/" // 'particles')
     file = "ensight-3D/" // 'particles' // "/" // 'particles' // "."
     write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nout_time
     ! Open the file
     call BINARY_FILE_OPEN(iunit,trim(file),"w",ierr)
     ! Write header
     cbuffer = 'C Binary'
     call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
     cbuffer = 'Spray coordinates from ARTS'
     call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
     cbuffer = 'particle coordinates'
     call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
     ibuffer = max(npart,1)
     call BINARY_FILE_WRITE(iunit,ibuffer,1,kind(ibuffer),ierr)
     do i=1,npart
        ibuffer = i
        call BINARY_FILE_WRITE(iunit,ibuffer,1,kind(ibuffer),ierr)
     end do
     ! Spray coordinates
     do i=1,npart
        rbuffer = part(i)%x
        call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
        rbuffer = part(i)%y
        call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
        rbuffer = part(i)%z
        call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
     end do
     ! Zero drop case
     if (npart.eq.0) then
        ibuffer = i
        call BINARY_FILE_WRITE(iunit,ibuffer,1,kind(ibuffer),ierr)
        rbuffer = 0.0_WP
        call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
        rbuffer = 0.0_WP
        call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
        rbuffer = 0.0_WP
        call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
     end if
     ! Close the file
     call BINARY_FILE_CLOSE(iunit,ierr)
  end if
  
  ! Generate the file - Diameters
  if (irank.eq.iroot) then
     call CREATE_FOLDER("ensight-3D/" // 'particles')
     file = "ensight-3D/" // 'particles' // "/" // 'diameter' // "."
     write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nout_time
     
     ! Open the file
     call BINARY_FILE_OPEN(iunit,trim(file),"w",ierr)
     ! Write header
     cbuffer = 'Spray diameter'
     call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
     ! Spray diameter
     do i=1,npart
        rbuffer = part(i)%d
        call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
     end do
     ! Zero drop case
     if (npart.eq.0) then
        rbuffer = 0.0_WP
        call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
     end if
     ! Close the file
     call BINARY_FILE_CLOSE(iunit,ierr)
  end if
  
  ! Generate the file - Velocity
  if (irank.eq.iroot) then
     call CREATE_FOLDER("ensight-3D/" // 'particles')
     file = "ensight-3D/" // 'particles' // "/" // 'velocity' // "."
     write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nout_time
     
     ! Open the file
     call BINARY_FILE_OPEN(iunit,trim(file),"w",ierr)
     ! Write header
     cbuffer = 'Spray velocity'
     call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
     ! Spray velocity
     do i=1,npart
        rbuffer = part(i)%u
        call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
        rbuffer = part(i)%v
        call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
        rbuffer = part(i)%w
        call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
     end do
     ! Zero drop case
     if (npart.eq.0) then
        rbuffer = 0.0_WP
        call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
        rbuffer = 0.0_WP
        call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
        rbuffer = 0.0_WP
        call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
     end if
     ! Close the file
     call BINARY_FILE_CLOSE(iunit,ierr)
  end if
  
  return
end subroutine dump_ensight_str_particle_rpt
