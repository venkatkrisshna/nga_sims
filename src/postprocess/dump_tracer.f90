! ================================ !
! Routine to track passive tracers !
! ================================ !
module dump_tracer
  use precision
  use geometry
  use partition
  use math
  use string
  use masks
  implicit none

  ! Time info
  integer :: nout_time
  real(WP), dimension(:), pointer :: out_time => null()

end module dump_tracer

! ---------------------------------- !
! Initialization for tracer routines !
! ---------------------------------- !
subroutine dump_tracer_init  
  use dump_tracer
  use parser
  use time_info
  use parallel
  implicit none

  logical :: file_is_there
  integer :: i,ierr

  ! Start the timer
  call timing_start('tracer')

  ! Open the file
  inquire(file='tracer_stats/times',exist=file_is_there)
  call MPI_BARRIER(comm,ierr)

  if (file_is_there) then
     ! Read the file
     call parser_parsefile('tracer_stats/times')
     ! Get the time
     call parser_getsize('time values',nout_time)
     allocate(out_time(nout_time))
     call parser_read('time values',out_time)
     ! Remove future time
     future: do i=1,size(out_time)
        if (out_time(i).GE.time*0.99999_WP) then
           nout_time = i-1
           exit future
        end if
     end do future
  else
     ! Create directory
     if (irank.eq.iroot) call CREATE_FOLDER("tracer_stats")
     ! Set the time
     nout_time = 0
     allocate(out_time(1))
  end if

  ! Stop the timer
  call timing_stop('tracer')

  return
end subroutine dump_tracer_init

! -------------------------------------------------- !
! Interpolate gas-phase variables to tracer location !
! Write to file                                      !
! -------------------------------------------------- !  
subroutine dump_tracers
  use dump_tracer
  use parallel 
  use time_info
  use fileio
  use interpolate
  use ppt
  use mechanism
  use finitechem
  implicit none

  ! i/o stuff
  integer :: n,ip,ierr,iunit
  character(len=str_medium) :: filename

  ! Data arrays
  integer, dimension(:), allocatable :: id_tr
  real(WP), dimension(:), allocatable :: t0_tr,X_tr,Y_tr,Z_tr,U_tr,V_tr,W_tr
  real(WP), dimension(:), allocatable :: EPSF_tr,RHO_tr,T_tr,N2_tr,VOL_tr,HC_tr,GAS_tr

  ! Interpolation parameters
  integer :: sx,sy,sz
  integer :: i1,j1,k1
  integer :: i2,j2,k2
  real(WP) :: wx1,wx2,wy1,wy2,wz1,wz2
  real(WP), dimension(2,2,2) :: wwd,wwn

  ! Only dump if tracers are present
  if (ntracer.eq.0) return

  ! Start the timer
  call timing_start('tracer')

  ! Update the times file
  call dump_tracer_times

  ! Allocate arrays
  allocate(id_tr(1:ntracer_))
  allocate(t0_tr(1:ntracer_))
  allocate(X_tr(1:ntracer_))
  allocate(Y_tr(1:ntracer_))
  allocate(Z_tr(1:ntracer_))
  allocate(U_tr(1:ntracer_))
  allocate(V_tr(1:ntracer_))
  allocate(W_tr(1:ntracer_))
  allocate(EPSF_tr(1:ntracer_))
  allocate(RHO_tr(1:ntracer_))
  allocate(T_tr(1:ntracer_))
  allocate(N2_tr(1:ntracer_))
  allocate(VOL_tr(1:ntracer_))
  allocate(HC_tr(1:ntracer_))
  allocate(GAS_tr(1:ntracer_))

  ! Interpolate gas-phase variables to tracer location
  do n=1,ntracer_
     ! Get interpolation cells w/r to centroids
     sx = -1
     sy = -1
     sz = -1
     if (tracer(n)%x.ge.xm(tracer(n)%i)) sx = 0
     if (tracer(n)%y.ge.ym(tracer(n)%j)) sy = 0
     if (tracer(n)%z.ge.zm(tracer(n)%k)) sz = 0
  
     ! Get the first interpolation point
     i1=tracer(n)%i+sx; i2=i1+1
     j1=tracer(n)%j+sy; j2=j1+1
     k1=tracer(n)%k+sz; k2=k1+1
  
     ! Compute the linear interpolation coefficients
     wx1 = (xm(i2)-tracer(n)%x )/(xm(i2)-xm(i1))
     wx2 = (tracer(n)%x -xm(i1))/(xm(i2)-xm(i1))
     wy1 = (ym(j2)-tracer(n)%y )/(ym(j2)-ym(j1))
     wy2 = (tracer(n)%y -ym(j1))/(ym(j2)-ym(j1))
     wz1 = (zm(k2)-tracer(n)%z )/(zm(k2)-zm(k1))
     wz2 = (tracer(n)%z -zm(k1))/(zm(k2)-zm(k1))
  
     ! Combine the interpolation coefficients to form a tri-linear interpolation
     wwd(1,1,1)=wx1*wy1*wz1
     wwd(2,1,1)=wx2*wy1*wz1
     wwd(1,2,1)=wx1*wy2*wz1
     wwd(2,2,1)=wx2*wy2*wz1
     wwd(1,1,2)=wx1*wy1*wz2
     wwd(2,1,2)=wx2*wy1*wz2
     wwd(1,2,2)=wx1*wy2*wz2
     wwd(2,2,2)=wx2*wy2*wz2
     
     ! Correct for walls with Dirichlet condition
     if (mask(i1,j1).eq.1) wwd(1,1,:) = 0.0_WP
     if (mask(i1,j2).eq.1) wwd(1,2,:) = 0.0_WP
     if (mask(i2,j1).eq.1) wwd(2,1,:) = 0.0_WP
     if (mask(i2,j2).eq.1) wwd(2,2,:) = 0.0_WP
     
     ! Correct for walls with Neumann condition
     wwn = wwd/sum(wwd)

     ! Tracer id, birth, and location
     id_tr(n) = tracer(n)%id
     t0_tr(n) = tracer(n)%t0
     X_tr(n) = tracer(n)%x
     Y_tr(n) = tracer(n)%y
     Z_tr(n) = tracer(n)%z

     ! Velocities
     U_tr(n)=sum(wwd*Ui(i1:i2,j1:j2,k1:k2))
     V_tr(n)=sum(wwd*Vi(i1:i2,j1:j2,k1:k2))
     W_tr(n)=sum(wwd*Wi(i1:i2,j1:j2,k1:k2))
     
     ! Volume fractions
     EPSF_tr(n)=sum(wwn*epsf(i1:i2,j1:j2,k1:k2))

     ! Density
     RHO_tr(n)=sum(wwn*RHO(i1:i2,j1:j2,k1:k2))

     ! Temperature
     T_tr(n)=sum(wwn*SC(i1:i2,j1:j2,k1:k2,isc_T))

     ! Nitrogen
     N2_tr(n)=sum(wwn*SC(i1:i2,j1:j2,k1:k2,isc_1+gN2-1))

     ! Volatile
     VOL_tr(n)=sum(wwn*SC(i1:i2,j1:j2,k1:k2,isc_1+gVOL-1))

     ! Hydro Carbons
     HC_tr(n)=sum(wwn*SC(i1:i2,j1:j2,k1:k2,isc_1+gHC-1))

     ! Gas
     GAS_tr(n)=sum(wwn*SC(i1:i2,j1:j2,k1:k2,isc_1+gGAS-1))

  end do

  ! Open file with correct index
  filename = "tracer_stats/tracer_stat."
  write(filename(len_trim(filename)+1:len_trim(filename)+6),'(i6.6)') nout_time

  ! Root process writes the header
  if (irank.eq.iroot) then
     iunit = iopen()
     open(iunit,file=filename,form="formatted",iostat=ierr,status="REPLACE")
     write(iunit,'(1a10,14a20)') 'ID','BIRTH','X','Y','Z','U','V','W','EPSF','RHO','T','N2','VOL','HC','GAS'
     close(iclose(iunit))
  end if
  
  ! Write data
  iunit = iopen()
  do ip=1,nproc
     if (irank.eq.ip) then
        open(iunit,file=filename,form="formatted",iostat=ierr,access="APPEND")
        do n=1,ntracer_
           write(iunit,'(I10,14ES20.12)') id_tr(n),t0_tr(n),X_tr(n),Y_tr(n),Z_tr(n),U_tr(n),V_tr(n),W_tr(n), &
                EPSF_tr(n),RHO_tr(n),T_tr(n),N2_tr(n),VOL_tr(n),HC_tr(n),GAS_tr(n)
        end do
        close(iclose(iunit))
     end if
     call MPI_BARRIER(comm,ierr)
  end do

  ! Stop the timer
  call timing_stop('tracer')

  return
end subroutine dump_tracers

! ---------------------------------------------------------------------- !
! Update file containing the times at which the tracers have been dumped !
! ---------------------------------------------------------------------- !
subroutine dump_tracer_times
  use dump_tracer
  use time_info
  use parallel
  use fileio
  implicit none

  integer :: iunit,ierr
  real(WP), dimension(:), allocatable :: buffer
  character(len=80) :: str

  ! Update the time info
  allocate(buffer(nout_time))
  buffer(1:nout_time) = out_time(1:nout_time)
  deallocate(out_time)
  nout_time = nout_time + 1
  allocate(out_time(nout_time))
  out_time(1:nout_time-1) = buffer(1:nout_time-1)
  out_time(nout_time) = time

  ! Write - Single proc & parallel => only root writes (in ASCII)
  if (irank==iroot) then

     ! Open the file
     iunit = iopen()
     open(iunit,file="tracer_stats/times",form="formatted",iostat=ierr,status="REPLACE")

     ! Time section
     str='TIME'
     write(iunit,'(a80)') str
     str='number of steps:'
     write(iunit,'(a16,x,i12)') str,nout_time
     str='time values:'
     write(iunit,'(a12,x,10000000(3(ES12.5,x),/))') str,out_time

     ! Close the file
     close(iclose(iunit))
  end if

  return
end subroutine dump_tracer_times
