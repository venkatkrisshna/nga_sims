module dump_data
  use precision
  use string
  implicit none
  
  ! Number of outputs
  integer :: nout
  
  ! Type of outputs
  character(str_medium), dimension(:), allocatable :: output_type
  
  ! Frequency of output
  real(WP), dimension(:), allocatable :: output_freq
  
end module dump_data

! ================================== !
! Initialize data dumping procedures !
! ================================== !
subroutine dump_init
  use dump_data
  use parser
  use time_info
  implicit none

  logical :: output_isdef
  integer :: i,nfreq
  real(WP) :: buffer
  
  ! Read the input file
  call parser_is_defined('Output type',output_isdef)
  nout = 0
  if (output_isdef) then
     
     call parser_getsize('Output type',nout)
     allocate(output_type(nout))
     call parser_read('Output type',output_type)
     
     call parser_getsize('Output frequency',nfreq)
     allocate(output_freq(nfreq))
     call parser_read('Output frequency',output_freq)
     if (nfreq.EQ.1) then
        buffer = output_freq(1)
        deallocate(output_freq)
        allocate(output_freq(nout))
        output_freq = buffer
     else if (nfreq.NE.nout) then
        call die('dump_data: not enough output frequencies specified')
     end if
     
  end if
  
  ! Initialize
  do i=1,nout
     select case (trim(output_type(i)))
     case ('ensight-3D')
        call dump_ensight_3D_init
     case ('ensight-str-3D')
        call dump_ensight_str_3D_init
     case ('plot3D-2D')
        call dump_plot3D_2D_init
     case ('plot3D-3D')
        call dump_plot3D_3D_init
     case ('povray')
        call dump_povray_init
     case ('combanal')
        call dump_combanal_init
     case ('structures')
        call dump_struct_init
     case ('visit')
        call dump_visit_init
     case ('tracers')
        call dump_tracer_init
     case('spectrum')
        call dump_spect_init
     case('HITA-2D-Stats')
        call dump_stat2dhit_visit_init
     case('structID')
        call dump_structID_init
     case default
     end select
  end do

  ! Dump results
  call dump_result
  
  return
end subroutine dump_init

! ======================================== !
! Dump_data at each time step if necessary !
! ======================================== !
subroutine dump_result
  use dump_data
  use time_info
  implicit none

  integer :: i
  
  ! Dump data if needed
  do i=1,nout
     ! Check if time to dump
     if ( mod(time,output_freq(i)).lt.0.5_WP*dt .or. &
          mod(time,output_freq(i)).ge.output_freq(i)-0.5_WP*dt ) then
        ! Select dump format
        select case (trim(output_type(i)))
        case ('ensight-3D')
           call dump_ensight_3D
           call monitor_log("3D ENSIGHT FILE WRITTEN")
        case ('ensight-str-3D')
           call dump_ensight_str_3D
           call monitor_log("3D ENSIGHT-STR FILE WRITTEN")
        case ('plot3D-2D')
           call dump_plot3D_2D
           call monitor_log("2D PLOT3D FILE WRITTEN")
        case ('plot3D-3D')
           call dump_plot3D_3D
           call monitor_log("3D PLOT3D FILE WRITTEN")
        case ('povray')
           call dump_povray_data
           call monitor_log("POVRAY FILE WRITTEN")
        case ('combanal')
           call dump_combanalyse
           call monitor_log("COMBANAL FILE WRITTEN")
        case ('structures')
           call dump_structures
           call monitor_log("STRUCTURE FILE WRITTEN")
        case ('visit')
           call dump_visit_data
           call monitor_log("VISIT FILE WRITTEN")
        case ('tracers')
           call dump_tracers
           call monitor_log("TRACER FILE WRITTEN")
        case ('spectrum')
           call monitor_log("SPECTRUM FILE WRITTEN")
           call dump_spectrum
        case('HITA-2D-Stats')
           call stat_2d_hit_buf_update
           call dump_stat2dhit_visit_data
           call monitor_log("HITA 2D STATS PLOT WRITTEN")
        case ('structID')
           ! call dump_structID_update ! Done in dump_structures
        case default
           call die('dump_data: unknown output type ('//trim(output_type(i))//')')
        end select
     end if
  end do
  
  return
end subroutine dump_result
