! ======================= !
! Spectrum postprocessing !
! ======================= !
module dump_spect
  use math
  implicit none
  
  ! Various spectra of interest
  real(WP), dimension(:,:), allocatable :: TKEspect
  real(WP), dimension(:,:), allocatable :: SRCspect
  
end module dump_spect


! ================================== !
! Initialize spectrum postprocessing !
! ================================== !
subroutine dump_spect_init
  use getspect
  use dump_spect
  use parallel
  use data
  implicit none
  
  ! Initialize spectrum calculation routine
  call getspect_init
  
  ! Initialize Fourier directions
  allocate(TKEspect(3,N_spect+1))
  allocate(SRCspect(3,N_spect+1))
  
  ! Create directory for storing files
  if (irank.eq.iroot) call CREATE_FOLDER("specs")
  
  return
end subroutine dump_spect_init


! =============== !
! Output spectrum !
! =============== !
subroutine dump_spectrum
 use dump_spect
 use data
 use parallel
 use getspect
 use fileio
 use time_info
 use velocity
 use bodyforce
  use parser
 implicit none

  integer :: i,iunit,ierr, order, cut
  character(len=str_medium) :: filename

  ! Compute TKE spectrum
  call getspect_vector(U,V,W)
  TKEspect=spect
  
  ! Compute SRC spectrum
! call parser_read('HIT filter order',order)
! call parser_read('HIT filter cutoff',cut)
! call raymond_filter(U,srcUmid,order,3.141592653590_WP/real(cut,WP),'+','ym')
! call raymond_filter(V,srcVmid,order,3.141592653590_WP/real(cut,WP),'-','y ')
! call raymond_filter(W,srcWmid,order,3.141592653590_WP/real(cut,WP),'-','ym')
! call getspect_vector(srcUmid,srcVmid,srcWmid)
  call getspect_vector(srcUmid/dt_uvw,srcVmid/dt_uvw,srcWmid/dt_uvw)
  SRCspect=spect
  
  ! Root outputs
  if (irank.eq.iroot) then
     write(filename,'(ES12.3)') time
     iunit=iopen()
     filename='specs/'//'spect_'//trim(adjustl(filename))
     open(iunit,file=filename,form='formatted')
     write(iunit,*) 'Wavenumer ','RawEnergy ','RawDissipation ','FilteredEnergy ','FilteredDissipation '
     do i=1,N_spect+1
        if (TKEspect(2,i).ne.0.0_WP) then
           write(iunit,*) spect(1,i),TKEspect(2,i),TKEspect(3,i),SRCspect(2,i),SRCspect(3,i)
        end if
     end do
     close(iclose(iunit)) 
  end if
  
  return
end subroutine dump_spectrum
