module stat_lpt
  use stat
  implicit none
  
  ! -----------------------------------------------------
  ! HOMOGENEOUS LPT STATISTICS 
  !  -> Reynolds-averaged and Phase-averaged values
  !  -> Conditional means on alpha
  ! -----------------------------------------------------

  ! Global mean stats
  integer :: nvar_eul,nvar_lag
  real(WP), dimension(:), allocatable :: lag_stat,lag_buf
  real(WP), dimension(:), allocatable :: eul_stat,eul_buf
  real(WP) :: Delta_t
  character(len=str_medium), dimension(:), pointer :: lag_name
  character(len=str_medium), dimension(:), pointer :: eul_name

  ! Conditional mean stats
  integer :: nbin,nvar_alpha
  real(WP) :: dbin
  real(WP), dimension(:), allocatable :: bin_alpha
  real(WP), dimension(:,:), allocatable :: alpha_stat,buf_alpha
  character(len=str_medium), dimension(:), pointer :: alpha_name

  ! Fluid stresses
  real(WP), dimension(:,:,:), allocatable :: stat_pres_x,stat_pres_y,stat_pres_z
  real(WP), dimension(:,:,:), allocatable :: stat_visc_x,stat_visc_y,stat_visc_z
  
contains

  ! ================================ !
  ! Initialize Lagrangian statistics !
  ! ================================ !
  subroutine stat_lpt_init_lag
    implicit none
    
    ! Return if init already done
    if (associated(lag_name)) return
    
    ! Allocate
    nvar_lag = 9
    allocate(lag_name(nvar_lag))

    ! Particle velocity statstics (Lagrangian)
    lag_name(1) = '<up>'
    lag_name(2) = '<vp>'
    lag_name(3) = '<wp>'
    lag_name(4) = '<up^2>'
    lag_name(5) = '<vp^2>'
    lag_name(6) = '<wp^2>'
    lag_name(7) = '<upvp>'
    lag_name(8) = '<upwp>'
    lag_name(9) = '<vpwp>'

    return
  end subroutine stat_lpt_init_lag


  ! ============================== !
  ! Initialize Eulerian statistics !
  ! ============================== !
  subroutine stat_lpt_init_eul
    implicit none
    
    integer :: ns
    
    ! Return if init already done
    if (associated(eul_name)) return
    
    nvar_eul = 0

    ! Volume fraction
    nvar_eul = nvar_eul+6
    ! Particle velocity (Eulerian)
    nvar_eul = nvar_eul+22
    ! Fluid velocity
    nvar_eul = nvar_eul+29
    ! Particle-fluid velocity correlation
    nvar_eul = nvar_eul+9
    ! Fluid pressure
    nvar_eul = nvar_eul+45
    ! Fluid viscosity
    nvar_eul = nvar_eul+45
    ! Collision frequency
    nvar_eul = nvar_eul+1
    
    ! Allocate
    allocate(eul_name(nvar_eul))
    
    ns = 0

    ! Volume fraction statistics
    eul_name(ns+1) = '<alpha>'
    eul_name(ns+2) = '<alpha^2>'
    eul_name(ns+3) = '<alpha^3>'
    eul_name(ns+4) = '<alpha^4>'
    eul_name(ns+5) = '<alpha^5>'
    eul_name(ns+6) = '<alpha^6>'
    ns = ns+6

    ! Particle velocity statistics (Eulerian)
    eul_name(ns+1) = '<u>'
    eul_name(ns+2) = '<u^2>'
    eul_name(ns+3) = '<alpha u>'
    eul_name(ns+4) = '<alpha u^2>'
    eul_name(ns+5) = '<v>'
    eul_name(ns+6) = '<v^2>'
    eul_name(ns+7) = '<alpha v>'
    eul_name(ns+8) = '<alpha v^2>'
    eul_name(ns+9) = '<w>'
    eul_name(ns+10) = '<w^2>'
    eul_name(ns+11) = '<alpha w>'
    eul_name(ns+12) = '<alpha w^2>'
    eul_name(ns+13) = '<alpha uv>'
    eul_name(ns+14) = '<alpha uw>'
    eul_name(ns+15) = '<alpha vw>'
    eul_name(ns+16) = '<alpha^2 u>'
    eul_name(ns+17) = '<alpha^3 u>'
    eul_name(ns+18) = '<alpha^2 u^2>'
    eul_name(ns+19) = '<alpha^3 u^2>'
    eul_name(ns+20) = '<alpha up^2>'
    eul_name(ns+21) = '<alpha^2 up^2>'
    eul_name(ns+22) = '<alpha^3 up^2>'
    ns = ns+22

    ! Fluid velocity statistics
    eul_name(ns+1) = '<uf>'
    eul_name(ns+2) = '<uf^2>'
    eul_name(ns+3) = '<(1-alpha)uf>'
    eul_name(ns+4) = '<(1-alpha)uf^2>'
    eul_name(ns+5) = '<alpha uf>'
    eul_name(ns+6) = '<alpha uf^2>'
    eul_name(ns+7) = '<vf>'
    eul_name(ns+8) = '<vf^2>'
    eul_name(ns+9) = '<(1-alpha)vf>'
    eul_name(ns+10) = '(1-alpha)vf^2>'
    eul_name(ns+11) = '<alpha vf>'
    eul_name(ns+12) = '<alpha vf^2>'
    eul_name(ns+13) = '<wf>'
    eul_name(ns+14) = '<wf^2>'
    eul_name(ns+15) = '<(1-alpha)wf>'
    eul_name(ns+16) = '<(1-alpha)wf^2>'
    eul_name(ns+17) = '<alpha wf>'
    eul_name(ns+18) = '<alpha wf^2>'
    eul_name(ns+19) = '<ufvf>'
    eul_name(ns+20) = '<ufwf>'
    eul_name(ns+21) = '<vfwf>'
    eul_name(ns+22) = '<(1-alpha)ufvf>'
    eul_name(ns+23) = '<(1-alpha)ufwf>'
    eul_name(ns+24) = '<(1-alpha)vfwf>'
    eul_name(ns+25) = '<alpha ufvf>'
    eul_name(ns+26) = '<alpha ufwf>'
    eul_name(ns+27) = '<alpha vfwf>'
    eul_name(ns+28) = '<alpha^2 uf>'
    eul_name(ns+29) = '<alpha^3 uf>'
    ns = ns+29

    ! Particle-fluid velocity correlation
    eul_name(ns+1) = '<alpha u uf>'
    eul_name(ns+2) = '<alpha u vf>'
    eul_name(ns+3) = '<alpha u wf>'
    eul_name(ns+4) = '<alpha v uf>'
    eul_name(ns+5) = '<alpha v vf>'
    eul_name(ns+6) = '<alpha v wf>'
    eul_name(ns+7) = '<alpha w uf>'
    eul_name(ns+8) = '<alpha w vf>'
    eul_name(ns+9) = '<alpha w wf>'
    ns = ns+9

    ! Fluid pressure terms
    eul_name(ns+1) = '<dP/dx>'
    eul_name(ns+2) = '<dP/dy>'
    eul_name(ns+3) = '<dP/dz>'
    eul_name(ns+4) = '<alpha dP/dx>'
    eul_name(ns+5) = '<alpha dP/dy>'
    eul_name(ns+6) = '<alpha dP/dz>'
    eul_name(ns+7) = '<(1-alpha) dP/dx>'
    eul_name(ns+8) = '<(1-alpha) dP/dy>'
    eul_name(ns+9) = '<(1-alpha) dP/dz>'
    eul_name(ns+10) = '<alpha u dP/dx>'
    eul_name(ns+11) = '<alpha u dP/dy>'
    eul_name(ns+12) = '<alpha u dP/dz>'
    eul_name(ns+13) = '<alpha v dP/dx>'
    eul_name(ns+14) = '<alpha v dP/dy>'
    eul_name(ns+15) = '<alpha v dP/dz>'
    eul_name(ns+16) = '<alpha w dP/dx>'
    eul_name(ns+17) = '<alpha w dP/dy>'
    eul_name(ns+18) = '<alpha w dP/dz>'
    eul_name(ns+19) = '<(1-alpha) uf dP/dx>'
    eul_name(ns+20) = '<(1-alpha) uf dP/dy>'
    eul_name(ns+21) = '<(1-alpha) uf dP/dz>'
    eul_name(ns+22) = '<(1-alpha) vf dP/dx>'
    eul_name(ns+23) = '<(1-alpha) vf dP/dy>'
    eul_name(ns+24) = '<(1-alpha) vf dP/dz>'
    eul_name(ns+25) = '<(1-alpha) wf dP/dx>'
    eul_name(ns+26) = '<(1-alpha) wf dP/dy>'
    eul_name(ns+27) = '<(1-alpha) wf dP/dz>'
    eul_name(ns+28) = '<alpha uf dP/dx>'
    eul_name(ns+29) = '<alpha uf dP/dy>'
    eul_name(ns+30) = '<alpha uf dP/dz>'
    eul_name(ns+31) = '<alpha vf dP/dx>'
    eul_name(ns+32) = '<alpha vf dP/dy>'
    eul_name(ns+33) = '<alpha vf dP/dz>'
    eul_name(ns+34) = '<alpha wf dP/dx>'
    eul_name(ns+35) = '<alpha wf dP/dy>'
    eul_name(ns+36) = '<alpha wf dP/dz>'
    eul_name(ns+37) = '<uf dP/dx>'
    eul_name(ns+38) = '<uf dP/dy>'
    eul_name(ns+39) = '<uf dP/dz>'
    eul_name(ns+40) = '<vf dP/dx>'
    eul_name(ns+41) = '<vf dP/dy>'
    eul_name(ns+42) = '<vf dP/dz>'
    eul_name(ns+43) = '<wf dP/dx>'
    eul_name(ns+44) = '<wf dP/dy>'
    eul_name(ns+45) = '<wf dP/dz>'
    ns=ns+45

    ! Fluid viscous terms
    eul_name(ns+1) = '<ViscX>'
    eul_name(ns+2) = '<ViscY>'
    eul_name(ns+3) = '<ViscZ>'
    eul_name(ns+4) = '<alpha ViscX>'
    eul_name(ns+5) = '<alpha ViscY>'
    eul_name(ns+6) = '<alpha ViscZ>'
    eul_name(ns+7) = '<(1-alpha) ViscX>'
    eul_name(ns+8) = '<(1-alpha) ViscY>'
    eul_name(ns+9) = '<(1-alpha) ViscZ>'
    eul_name(ns+10) = '<alpha u ViscX>'
    eul_name(ns+11) = '<alpha u ViscY>'
    eul_name(ns+12) = '<alpha u ViscZ>'
    eul_name(ns+13) = '<alpha v ViscX>'
    eul_name(ns+14) = '<alpha v ViscY>'
    eul_name(ns+15) = '<alpha v ViscZ>'
    eul_name(ns+16) = '<alpha w ViscX>'
    eul_name(ns+17) = '<alpha w ViscY>'
    eul_name(ns+18) = '<alpha w ViscZ>'
    eul_name(ns+19) = '<(1-alpha) uf ViscX>'
    eul_name(ns+20) = '<(1-alpha) uf ViscY>'
    eul_name(ns+21) = '<(1-alpha) uf ViscZ>'
    eul_name(ns+22) = '<(1-alpha) vf ViscX>'
    eul_name(ns+23) = '<(1-alpha) vf ViscY>'
    eul_name(ns+24) = '<(1-alpha) vf ViscZ>'
    eul_name(ns+25) = '<(1-alpha) wf ViscX>'
    eul_name(ns+26) = '<(1-alpha) wf ViscY>'
    eul_name(ns+27) = '<(1-alpha) wf ViscZ>'
    eul_name(ns+28) = '<alpha uf ViscX>'
    eul_name(ns+29) = '<alpha uf ViscY>'
    eul_name(ns+30) = '<alpha uf ViscZ>'
    eul_name(ns+31) = '<alpha vf ViscX>'
    eul_name(ns+32) = '<alpha vf ViscY>'
    eul_name(ns+33) = '<alpha vf ViscZ>'
    eul_name(ns+34) = '<alpha wf ViscX>'
    eul_name(ns+35) = '<alpha wf ViscY>'
    eul_name(ns+36) = '<alpha wf ViscZ>'
    eul_name(ns+37) = '<uf ViscX>'
    eul_name(ns+38) = '<uf ViscY>'
    eul_name(ns+39) = '<uf ViscZ>'
    eul_name(ns+40) = '<vf ViscX>'
    eul_name(ns+41) = '<vf ViscY>'
    eul_name(ns+42) = '<vf ViscZ>'
    eul_name(ns+43) = '<wf ViscX>'
    eul_name(ns+44) = '<wf ViscY>'
    eul_name(ns+45) = '<wf ViscZ>'
    ns=ns+45

    ! Collision frequency
    eul_name(ns+1) = '<fcol>'
    ns = ns+1

    return
  end subroutine stat_lpt_init_eul


  ! ====================================== !
  ! Initialize conditional mean statistics !
  ! ====================================== !
  subroutine stat_lpt_init_alpha
    implicit none
    
    integer :: ns
    
    ! Return if init already done
    if (associated(alpha_name)) return
    
    nvar_alpha=42
    
    ! Allocate
    allocate(alpha_name(nvar_alpha))

    alpha_name(1) = 'PDF'
    alpha_name(2) = "<u|alpha>"
    alpha_name(3) = "<v|alpha>"
    alpha_name(4) = "<w|alpha>"
    alpha_name(5) = "<uf|alpha>"
    alpha_name(6) = "<vf|alpha>"
    alpha_name(7) = "<wf|alpha>"
    alpha_name(8) = '<(u-uf)|alpha>'
    alpha_name(9) = '<up^2|alpha>'
    alpha_name(10) = '<vp^2|alpha>'
    alpha_name(11) = '<wp^2|alpha>'
    alpha_name(12) = '<upvp|alpha>'
    alpha_name(13) = '<upwp|alpha>'
    alpha_name(14) = '<vpwp|alpha>'
    alpha_name(15) = '<u^2|alpha>'
    alpha_name(16) = '<v^2|alpha>'
    alpha_name(17) = '<w^2|alpha>'
    alpha_name(18) = '<uv|alpha>'
    alpha_name(19) = '<uw|alpha>'
    alpha_name(20) = '<vw|alpha>'
    alpha_name(21) = '<uf^2|alpha>'
    alpha_name(22) = '<vf^2|alpha>'
    alpha_name(23) = '<wf^2|alpha>'
    alpha_name(24) = '<ufvf|alpha>'
    alpha_name(25) = '<ufwf|alpha>'
    alpha_name(26) = '<vfwf|alpha>'
    alpha_name(27) = '<u uf|alpha>'
    alpha_name(28) = '<u vf|alpha>'
    alpha_name(29) = '<u wf|alpha>'
    alpha_name(30) = '<v uf|alpha>'
    alpha_name(31) = '<v vf|alpha>'
    alpha_name(32) = '<v wf|alpha>'
    alpha_name(33) = '<w uf|alpha>'
    alpha_name(34) = '<w vf|alpha>'
    alpha_name(35) = '<w wf|alpha>'
    alpha_name(36) = '<dPdx|alpha>'
    alpha_name(37) = '<dPdy|alpha>'
    alpha_name(38) = '<dPdz|alpha>'
    alpha_name(39) = '<viscX|alpha>'
    alpha_name(40) = '<viscY|alpha>'
    alpha_name(41) = '<viscZ|alpha>'
    alpha_name(42) = '<fcol|alpha>'

    return
  end subroutine stat_lpt_init_alpha


  ! ============================== !
  ! Compute and store stress terms !
  ! ============================== !
  subroutine stat_lpt_stress
    use data
    use metric_generic
    use metric_velocity_conv
    use metric_velocity_visc
    use memory
    use bodyforce
    use velocity
    implicit none
    
    integer :: i,j,k
    integer :: ii,jj,kk

    ! Zero temporary arrays
    tmp7=0.0_WP
    tmp8=0.0_WP
    tmp9=0.0_WP
    tmp10=0.0_WP
    tmp11=0.0_WP
    tmp12=0.0_WP
    
    ! Viscous part in X
    do kk=kmin_-stv1,kmax_+stv2
       do jj=jmin_-stv1,jmax_+stv2
          do ii=imin_-stv1,imax_+stv2
             
             i = ii-1; j = jj-1; k = kk-1;
             
             FX(i,j,k) = &
                  + 2.0_WP*VISC(i,j,k)*( &
                  + sum(grad_u_x(i,j,k,:)*U(i-stv1:i+stv2,j,k)) &
                  - 1.0_WP/3.0_WP*( sum(divv_u(i,j,k,:)*U(i-stv1:i+stv2,j,k)) &
                  + sum(divv_v(i,j,k,:)*V(i,j-stv1:j+stv2,k)) &
                  + sum(divv_w(i,j,k,:)*W(i,j,k-stv1:k+stv2))))
             
             i = ii; j = jj; k = kk;
             
             FY(i,j,k) = &
                  + sum(interp_sc_xy(i,j,:,:)*VISC(i-st2:i+st1,j-st2:j+st1,k)) * &
                  ( sum(grad_u_y(i,j,k,:)*U(i,j-stv2:j+stv1,k)) &
                  + sum(grad_v_x(i,j,k,:)*V(i-stv2:i+stv1,j,k)) )
             
             FZ(i,j,k) = &
                  + sum(interp_sc_xz(i,j,:,:)*VISC(i-st2:i+st1,j,k-st2:k+st1)) * &
                  ( sum(grad_u_z(i,j,k,:)*U(i,j,k-stv2:k+stv1)) &
                  + sum(grad_w_x(i,j,k,:)*W(i-stv2:i+stv1,j,k)) )
          end do
       end do
    end do
    
    ! Residual in X
    do k=kmin_,kmax_
       do j=jmin_,jmax_
          do i=imin_,imax_
             tmp7(i,j,k) = -sum(grad_Px(i,j,k,:)*P(i-stc2:i+stc1,j,k))+meanSRC(1)
             tmp8(i,j,k) =  sum(divv_xx(i,j,k,:)*FX(i-stv2:i+stv1,j,k)) &
                  +sum(divv_xy(i,j,k,:)*FY(i,j-stv1:j+stv2,k)) &
                  +sum(divv_xz(i,j,k,:)*FZ(i,j,k-stv1:k+stv2))
          end do
       end do
    end do
    
    ! Viscous part in Y
    do kk=kmin_-stv1,kmax_+stv2
       do jj=jmin_-stv1,jmax_+stv2
          do ii=imin_-stv1,imax_+stv2
             
             i = ii-1; j = jj-1; k = kk-1;
             
             FY(i,j,k) = &
                  + 2.0_WP*VISC(i,j,k)*( &
                  + sum(grad_v_y(i,j,k,:)*V(i,j-stv1:j+stv2,k)) &
                  - 1.0_WP/3.0_WP*( sum(divv_u(i,j,k,:)*U(i-stv1:i+stv2,j,k)) &
                  + sum(divv_v(i,j,k,:)*V(i,j-stv1:j+stv2,k)) &
                  + sum(divv_w(i,j,k,:)*W(i,j,k-stv1:k+stv2))))
             
             Fcylv(i,j,k) = &
                  + 2.0_WP*VISC(i,j,k)*( &
                  + sum(grad_w_z(i,j,k,:)*W(i,j,k-stv1:k+stv2)) &
                  - 1.0_WP/3.0_WP*( sum(divv_u(i,j,k,:)*U(i-stv1:i+stv2,j,k))  &
                  + sum(divv_v(i,j,k,:)*V(i,j-stv1:j+stv2,k))  &
                  + sum(divv_w(i,j,k,:)*W(i,j,k-stv1:k+stv2))) &
                  + ymi(j)*sum(interpv_cyl_v_ym(i,j,:)*V(i,j-stv1:j+stv2,k)))
             
             i = ii; j = jj; k = kk;
             
             FX(i,j,k) = &
                  + sum(interp_sc_xy(i,j,:,:)*VISC(i-st2:i+st1,j-st2:j+st1,k)) * &
                  ( sum(grad_u_y(i,j,k,:)*U(i,j-stv2:j+stv1,k)) &
                  + sum(grad_v_x(i,j,k,:)*V(i-stv2:i+stv1,j,k)) )
             
             FZ(i,j,k) = &
                  + sum(interp_sc_yz(i,j,:,:)*VISC(i,j-st2:j+st1,k-st2:k+st1)) * &
                  ( sum(grad_v_z(i,j,k,:)*V(i,j,k-stv2:k+stv1)) &
                  + sum(grad_w_y(i,j,k,:)*W(i,j-stv2:j+stv1,k)) &
                  - yi(j)*sum(interpv_cyl_w_y(i,j,:)*W(i,j-stv2:j+stv1,k)) )
          end do
       end do
    end do
    
    ! Residual in Y
    do k=kmin_,kmax_
       do j=jmin_,jmax_
          do i=imin_,imax_
             tmp9(i,j,k) = -sum(grad_Py(i,j,k,:)*P(i,j-stc2:j+stc1,k))+meanSRC(2)
             tmp10(i,j,k) =  sum(divv_yx(i,j,k,:)*FX(i-stv1:i+stv2,j,k)) &
                  +sum(divv_yy(i,j,k,:)*FY(i,j-stv2:j+stv1,k)) &
                  +sum(divv_yz(i,j,k,:)*FZ(i,j,k-stv1:k+stv2)) &
                  -yi(j)*sum(interpv_cyl_F_y(i,j,:)*Fcylv(i,j-stv2:j+stv1,k))
          end do
       end do
    end do
    
    ! Viscous part in Z
    do kk=kmin_-stv1,kmax_+stv2
       do jj=jmin_-stv1,jmax_+stv2
          do ii=imin_-stv1,imax_+stv2
             
             i = ii-1; j = jj-1; k = kk-1;
             
             FZ(i,j,k) = &
                  + 2.0_WP*VISC(i,j,k)*( &
                  + sum(grad_w_z(i,j,k,:)*W(i,j,k-stv1:k+stv2)) &
                  - 1.0_WP/3.0_WP*( sum(divv_u(i,j,k,:)*U(i-stv1:i+stv2,j,k))  &
                  + sum(divv_v(i,j,k,:)*V(i,j-stv1:j+stv2,k))  &
                  + sum(divv_w(i,j,k,:)*W(i,j,k-stv1:k+stv2))) &
                  + ymi(j)*sum(interpv_cyl_v_ym(i,j,:)*V(i,j-stv1:j+stv2,k)))
             
             i = ii; j = jj; k = kk;
             
             FX(i,j,k) = &
                  + sum(interp_sc_xz(i,j,:,:)*VISC(i-st2:i+st1,j,k-st2:k+st1)) * &
                  ( sum(grad_u_z(i,j,k,:)*U(i,j,k-stv2:k+stv1)) &
                  + sum(grad_w_x(i,j,k,:)*W(i-stv2:i+stv1,j,k)) )
             
             FY(i,j,k) = &
                  + sum(interp_sc_yz(i,j,:,:)*VISC(i,j-st2:j+st1,k-st2:k+st1)) * &
                  ( sum(grad_v_z(i,j,k,:)*V(i,j,k-stv2:k+stv1)) &
                  + sum(grad_w_y(i,j,k,:)*W(i,j-stv2:j+stv1,k)) &
                  - yi(j)*sum(interpv_cyl_w_y(i,j,:)*W(i,j-stv2:j+stv1,k)) )
          end do
       end do
    end do
    
    ! Residual in Z
    do k=kmin_,kmax_
       do j=jmin_,jmax_
          do i=imin_,imax_
             tmp11(i,j,k) = -sum(grad_Pz(i,j,k,:)*P(i,j,k-stc2:k+stc1))+meanSRC(3)
             tmp12(i,j,k) = sum(divv_zx(i,j,k,:)*FX(i-stv1:i+stv2,j,k)) &
                  +sum(divv_zy(i,j,k,:)*FY(i,j-stv1:j+stv2,k)) &
                  +sum(divv_zz(i,j,k,:)*FZ(i,j,k-stv2:k+stv1)) &
                  +ymi(j)*sum(interpv_cyl_F_ym(i,j,:)*FY(i,j-stv1:j+stv2,k))
          end do
       end do
    end do
    
    ! Update borders
    call boundary_update_border(tmp7,'+','ym')
    call boundary_update_border(tmp8,'+','ym')
    call boundary_update_border(tmp9,'-','y')
    call boundary_update_border(tmp10,'-','y')
    call boundary_update_border(tmp11,'-','ym')
    call boundary_update_border(tmp12,'-','ym')
    call boundary_neumann(tmp7,'+ym')
    call boundary_neumann(tmp7,'-ym')
    call boundary_neumann(tmp7,'+x' )
    call boundary_neumann(tmp7,'-x' )
    call boundary_neumann(tmp8,'+ym')
    call boundary_neumann(tmp8,'-ym')
    call boundary_neumann(tmp8,'+x' )
    call boundary_neumann(tmp8,'-x' )
    call boundary_neumann(tmp9,'+y' )
    call boundary_neumann(tmp9,'-y' )
    call boundary_neumann(tmp9,'+xm')
    call boundary_neumann(tmp9,'-xm')
    call boundary_neumann(tmp10,'+y' )
    call boundary_neumann(tmp10,'-y' )
    call boundary_neumann(tmp10,'+xm')
    call boundary_neumann(tmp10,'-xm')
    call boundary_neumann(tmp11,'+ym')
    call boundary_neumann(tmp11,'-ym')
    call boundary_neumann(tmp11,'+xm')
    call boundary_neumann(tmp11,'-xm')
    call boundary_neumann(tmp12,'+ym')
    call boundary_neumann(tmp12,'-ym')
    call boundary_neumann(tmp12,'+xm')
    call boundary_neumann(tmp12,'-xm')
    
    ! Interpolate to cell centers
    do k=kmin_,kmax_
       do j=jmin_,jmax_
          do i=imin_,imax_
             stat_pres_x(i,j,k) = sum(interp_u_xm(i,j,:)*tmp7(i-st1:i+st2,j,k))
             stat_visc_x(i,j,k) = sum(interp_u_xm(i,j,:)*tmp8(i-st1:i+st2,j,k))
             stat_pres_y(i,j,k) = sum(interp_v_ym(i,j,:)*tmp9(i,j-st1:j+st2,k))
             stat_visc_y(i,j,k) = sum(interp_v_ym(i,j,:)*tmp10(i,j-st1:j+st2,k))
             stat_pres_z(i,j,k) = sum(interp_w_zm(i,j,:)*tmp11(i,j,k-st1:k+st2))
             stat_visc_z(i,j,k) = sum(interp_w_zm(i,j,:)*tmp12(i,j,k-st1:k+st2))
          end do
       end do
    end do
    
    ! Update borders
    call boundary_update_border(stat_pres_x,'+','ym')
    call boundary_update_border(stat_visc_x,'+','ym')
    call boundary_update_border(stat_pres_y,'-','ym')
    call boundary_update_border(stat_visc_y,'-','ym')
    call boundary_update_border(stat_pres_z,'-','ym')
    call boundary_update_border(stat_visc_z,'-','ym')
    call boundary_neumann(stat_pres_x,'+ym')
    call boundary_neumann(stat_pres_x,'-ym')
    call boundary_neumann(stat_pres_x,'+xm')
    call boundary_neumann(stat_pres_x,'-xm')
    call boundary_neumann(stat_visc_x,'+ym')
    call boundary_neumann(stat_visc_x,'-ym')
    call boundary_neumann(stat_visc_x,'+xm')
    call boundary_neumann(stat_visc_x,'-xm')
    call boundary_neumann(stat_pres_y,'+ym')
    call boundary_neumann(stat_pres_y,'-ym')
    call boundary_neumann(stat_pres_y,'+xm')
    call boundary_neumann(stat_pres_y,'-xm')
    call boundary_neumann(stat_visc_y,'+ym')
    call boundary_neumann(stat_visc_y,'-ym')
    call boundary_neumann(stat_visc_y,'+xm')
    call boundary_neumann(stat_visc_y,'-xm')
    call boundary_neumann(stat_pres_z,'+ym')
    call boundary_neumann(stat_pres_z,'-ym')
    call boundary_neumann(stat_pres_z,'+xm')
    call boundary_neumann(stat_pres_z,'-xm')
    call boundary_neumann(stat_visc_z,'+ym')
    call boundary_neumann(stat_visc_z,'-ym')
    call boundary_neumann(stat_visc_z,'+xm')
    call boundary_neumann(stat_visc_z,'-xm')
    
    return
  end subroutine stat_lpt_stress

end module stat_lpt


! =============================================== !
! Initialize the lpt statistic module             !
!  -> Reynolds-averaged and Phase-averaged values !
!  -> Conditional means on Alpha                  !
! =============================================== !
subroutine stat_lpt_init
  use stat_lpt
  use parallel
  use parser
  implicit none

  integer :: i,j,k
  logical :: isdef,file1_is_there,file2_is_there

  ! No stat if LPT is not used
  if (.not.use_lpt) call die('stat_lpt_init: Homoegneous statistics require LPT')

  ! No stat if not triply periodic
  if (xper.eq.0 .or. yper.eq.0) call die('stat_lpt_init: Homoegneous statistics require triply periodic domain')

  ! Create a timer
  call timing_create('stat_lpt')

  ! Start the timer
  call timing_start('stat_lpt')
  
  ! Initialize Eulerian statistics
  call stat_lpt_init_eul

  ! Initialize Lagrangian statistics
  call stat_lpt_init_lag

  ! Initialize conditional mean statistics
  call stat_lpt_init_alpha

  ! Allocate fluid stress arays
  allocate(stat_pres_x(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); stat_pres_x=0.0_WP
  allocate(stat_pres_y(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); stat_pres_y=0.0_WP
  allocate(stat_pres_z(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); stat_pres_z=0.0_WP
  allocate(stat_visc_x(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); stat_visc_x=0.0_WP
  allocate(stat_visc_y(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); stat_visc_y=0.0_WP
  allocate(stat_visc_z(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); stat_visc_z=0.0_WP
  
  ! Allocate Eulerian stat arrays
  allocate(eul_stat(nvar_eul))
  allocate(eul_buf(nvar_eul))
  eul_stat = 0.0_WP

  ! Allocate Lagrangian stat arrays
  allocate(lag_stat(nvar_lag))
  allocate(lag_buf(nvar_lag))
  lag_stat = 0.0_WP

  ! Allocate conditional mean stat arrays
  nbin=80 ! Why not?
  allocate(alpha_stat(nbin,nvar_alpha)); alpha_stat=0.0_WP
  allocate(buf_alpha(nbin,nvar_alpha))
  allocate(bin_alpha(nbin+1))

  ! Create bin
  dbin = 0.1_WP/real(nbin,WP)
  bin_alpha(1) = 0.0_WP
  do i=2,nbin+1
     bin_alpha(i) = bin_alpha(i-1)+dbin
  end do
  
  ! Open file if it exists
  inquire(file='stat/stat-lpt',exist=file1_is_there)
  inquire(file='stat/stat-lpt-alpha',exist=file2_is_there)
  if (file1_is_there .and. file2_is_there) then
     call stat_lpt_read
  else
     Delta_t = 0.0_WP
  end if
  
  ! Stop the timer
  call timing_stop('stat_lpt')
  
  return
end subroutine stat_lpt_init

subroutine stat_lpt_sample
  use stat_lpt
  use data
  use parallel
  use memory
  use interpolate
  use time_info
  use lpt
  implicit none
  
  ! Statistics
  integer  :: i,j,k
  integer :: ns
  real(WP) :: alpha,volp,buf

  ! Collision frequency
  integer :: ii,jj,kk,ip,jp,n,ind
  integer :: i1,i2,j1,j2,k1,k2
  integer, dimension(:), allocatable :: part_col
  real(WP), dimension(3) :: r1,r2
  real(WP) :: d12
  
  ! Start the timer
  call timing_start('stat_lpt')

  ! Compute fluid stresses
  call stat_lpt_stress

  ! Allocate collision frequency array
  allocate(part_col(1:npart_))
  part_col = 0

  ! Initialize arrays for particle localization
  npartincell_lpt =  0
  partincell_lpt  = -1
     
  ! Map particles to cell
  do i=1,npart_
     ii = part(i)%i
     jj = part(i)%j
     kk = part(i)%k
     npartincell_lpt(ii,jj,kk) = npartincell_lpt(ii,jj,kk) + 1
     partincell_lpt(ii,jj,kk,npartincell_lpt(ii,jj,kk)) = i
  end do
     
  ! Map ghost particles to cell
  do i=1,npart_gp_
     ii = part_gp(i)%i
     jj = part_gp(i)%j
     kk = part_gp(i)%k
     npartincell_lpt(ii,jj,kk) = npartincell_lpt(ii,jj,kk) + 1
     partincell_lpt(ii,jj,kk,npartincell_lpt(ii,jj,kk)) = -i    
  end do

  ! Compute collision frequency
  do ip=1,npart_

     ! Localize particles in cells
     i = part(ip)%i
     j = part(ip)%j
     k = part(ip)%k

     ! Particle data
     r1(1) = part(ip)%x
     r1(2) = part(ip)%y
     r1(3) = part(ip)%z

     ! Define bounds for loop on neighboring cells
     i1 = i-1
     i2 = i+1
     j1 = j-1
     j2 = j+1
     k1 = k-1
     k2 = k+1

     ! Loop over neighboring cells
     do kk=k1,k2
        do jj=j1,j2
           do ii=i1,i2
              
              ! Loop over particles in cells
              do n=1,npartincell_lpt(ii,jj,kk)
                 
                 ! Get particle id
                 jp=partincell_lpt(ii,jj,kk,n)

                 ! Particle data
                 if (jp.gt.0) then
                       ! Get particle data
                       r2(1) = part(jp)%x
                       r2(2) = part(jp)%y
                       r2(3) = part(jp)%z
                    else
                       ! Get position and velocity ! CAREFUL, GHOST CELL
                       jp=-jp
                       r2(1) = part_gp(jp)%x
                       r2(2) = part_gp(jp)%y
                       r2(3) = part_gp(jp)%z
                    end if

                 ! Find distance between pairs
                 d12=sqrt(sum((r1-r2)*(r1-r2)))
                 if (d12.le.dmean .and. ip.ne.jp) then
                    part_col(ip) = part_col(ip) + 1
                 end if

              end do

           end do
        end do
     end do
  end do

  ! Zero temporary arrays for filtering particle data to mesh
  tmp7=0.0_WP
  tmp8=0.0_WP
  tmp9=0.0_WP
  tmp10=0.0_WP
  tmp11=0.0_WP
  tmp12=0.0_WP
  tmp13=0.0_WP
  tmp14=0.0_WP
  tmp15=0.0_WP
  tmp16=0.0_WP
     
  do ip=1,npart_

     ! Gather Lagrangian statistics
     lag_stat(1) = lag_stat(1) + dt*part(ip)%u
     lag_stat(2) = lag_stat(2) + dt*part(ip)%v
     lag_stat(3) = lag_stat(3) + dt*part(ip)%w
     lag_stat(4) = lag_stat(4) + dt*part(ip)%u**2
     lag_stat(5) = lag_stat(5) + dt*part(ip)%v**2
     lag_stat(6) = lag_stat(6) + dt*part(ip)%w**2
     lag_stat(7) = lag_stat(7) + dt*part(ip)%u*part(i)%v
     lag_stat(8) = lag_stat(8) + dt*part(ip)%u*part(i)%w
     lag_stat(9) = lag_stat(9) + dt*part(ip)%v*part(i)%w

     ! Mollify Lagrangian data to mesh     
     volp=Pi/6.0_WP*part(ip)%d**3
     call lpt_mollify_sc(tmp7,part(ip)%x,part(ip)%y,part(ip)%z,&
          part(ip)%i,part(ip)%j,part(ip)%k,part(ip)%u*volp)
     call lpt_mollify_sc(tmp8,part(ip)%x,part(ip)%y,part(ip)%z,&
          part(ip)%i,part(ip)%j,part(ip)%k,part(ip)%v*volp)
     call lpt_mollify_sc(tmp9,part(ip)%x,part(ip)%y,part(ip)%z,&
          part(ip)%i,part(ip)%j,part(ip)%k,part(ip)%w*volp)
    call lpt_mollify_sc(tmp10,part(ip)%x,part(ip)%y,part(ip)%z,&
          part(ip)%i,part(ip)%j,part(ip)%k,part(ip)%u**2*volp)
    call lpt_mollify_sc(tmp11,part(ip)%x,part(ip)%y,part(ip)%z,&
          part(ip)%i,part(ip)%j,part(ip)%k,part(ip)%v**2*volp)
    call lpt_mollify_sc(tmp12,part(ip)%x,part(ip)%y,part(ip)%z,&
          part(ip)%i,part(ip)%j,part(ip)%k,part(ip)%w**2*volp)
    call lpt_mollify_sc(tmp13,part(ip)%x,part(ip)%y,part(ip)%z,&
          part(ip)%i,part(ip)%j,part(ip)%k,part(ip)%u*part(ip)%v*volp)
    call lpt_mollify_sc(tmp14,part(ip)%x,part(ip)%y,part(ip)%z,&
          part(ip)%i,part(ip)%j,part(ip)%k,part(ip)%u*part(ip)%w*volp)
    call lpt_mollify_sc(tmp15,part(ip)%x,part(ip)%y,part(ip)%z,&
          part(ip)%i,part(ip)%j,part(ip)%k,part(ip)%v*part(ip)%w*volp)
     call lpt_mollify_sc(tmp16,part(ip)%x,part(ip)%y,part(ip)%z,&
          part(ip)%i,part(ip)%j,part(ip)%k,0.5_WP*real(part_col(ip),WP)*volp)


  end do
  deallocate(part_col)
     
  ! Divide by volume
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           tmp7(i,j,k)=tmp7(i,j,k)/vol(i,j,k)
           tmp8(i,j,k)=tmp8(i,j,k)/vol(i,j,k)
           tmp9(i,j,k)=tmp9(i,j,k)/vol(i,j,k)
           tmp10(i,j,k)=tmp10(i,j,k)/vol(i,j,k)
           tmp11(i,j,k)=tmp11(i,j,k)/vol(i,j,k)
           tmp12(i,j,k)=tmp12(i,j,k)/vol(i,j,k)
           tmp13(i,j,k)=tmp13(i,j,k)/vol(i,j,k)
           tmp14(i,j,k)=tmp14(i,j,k)/vol(i,j,k)
           tmp15(i,j,k)=tmp15(i,j,k)/vol(i,j,k)
           tmp16(i,j,k)=tmp16(i,j,k)/vol(i,j,k)
        end do
     end do
  end do
     
  ! Add up source at boundaries
  call summation_border(tmp7)
  call summation_border(tmp8)
  call summation_border(tmp9)
  call summation_border(tmp10)
  call summation_border(tmp11)
  call summation_border(tmp12)
  call summation_border(tmp13)
  call summation_border(tmp14)
  call summation_border(tmp15)
  call summation_border(tmp16)

  ! Filter statistics
  call lpt_smooth(tmp7,'+')
  call lpt_smooth(tmp8,'+')
  call lpt_smooth(tmp9,'+')
  call lpt_smooth(tmp10,'+')
  call lpt_smooth(tmp11,'+')
  call lpt_smooth(tmp12,'+')
  call lpt_smooth(tmp13,'+')
  call lpt_smooth(tmp14,'+')
  call lpt_smooth(tmp15,'+')
  call lpt_smooth(tmp16,'+')
     
  ! Apply boundary conditions
  call boundary_dirichlet(tmp7,'+xm')
  call boundary_dirichlet(tmp7,'-xm')
  call boundary_dirichlet(tmp8,'+xm')
  call boundary_dirichlet(tmp8,'-xm')
  call boundary_dirichlet(tmp9,'+xm')
  call boundary_dirichlet(tmp9,'-xm')
  call boundary_dirichlet(tmp10,'+xm')
  call boundary_dirichlet(tmp10,'-xm')
  call boundary_dirichlet(tmp11,'+xm')
  call boundary_dirichlet(tmp11,'-xm')
  call boundary_dirichlet(tmp12,'+xm')
  call boundary_dirichlet(tmp12,'-xm')
  call boundary_dirichlet(tmp13,'+xm')
  call boundary_dirichlet(tmp13,'-xm')
  call boundary_dirichlet(tmp14,'+xm')
  call boundary_dirichlet(tmp14,'-xm')
  call boundary_dirichlet(tmp15,'+xm')
  call boundary_dirichlet(tmp15,'-xm')
  call boundary_dirichlet(tmp16,'+xm')
  call boundary_dirichlet(tmp16,'-xm')

  ! tmp7  ==>  filtered U
  tmp7=tmp7/(1.0_WP-EPSF)
  ! tmp8  ==>  filtered V
  tmp8=tmp8/(1.0_WP-EPSF)
  ! tmp9  ==>  filtered W
  tmp9=tmp9/(1.0_WP-EPSF)
  ! tmp10 ==>  filtered U^2
  tmp10=tmp10/(1.0_WP-EPSF)
  ! tmp11 ==>  filtered V^2
  tmp11=tmp11/(1.0_WP-EPSF)
  ! tmp12  ==>  filtered W^2
  tmp12=tmp12/(1.0_WP-EPSF)
  ! tmp13  ==>  filtered UV
  tmp13=tmp13/(1.0_WP-EPSF)
  ! tmp14  ==>  filtered UW
  tmp14=tmp14/(1.0_WP-EPSF)
  ! tmp15  ==>  filtered VW
  tmp15=tmp15/(1.0_WP-EPSF)
  ! tmp16 ==>  filtered fcol
  tmp16=tmp16/(1.0_WP-EPSF)
  
  ! Gather the stats
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           
           ns = 0

           ! Particle volume fraction
           alpha=1.0_WP-EPSF(i,j,k)
           
           ! Volume fraction
           eul_stat(ns+1) = eul_stat(ns+1) + dt*alpha
           eul_stat(ns+2) = eul_stat(ns+2) + dt*alpha**2
           eul_stat(ns+3) = eul_stat(ns+3) + dt*alpha**3
           eul_stat(ns+4) = eul_stat(ns+4) + dt*alpha**4
           eul_stat(ns+5) = eul_stat(ns+5) + dt*alpha**5
           eul_stat(ns+6) = eul_stat(ns+6) + dt*alpha**6
           ns = ns+6

           ! Particle velocity (U)
           eul_stat(ns+1) = eul_stat(ns+1) + dt*tmp7(i,j,k)
           eul_stat(ns+2) = eul_stat(ns+2) + dt*tmp7(i,j,k)**2
           eul_stat(ns+3) = eul_stat(ns+3) + dt*alpha*tmp7(i,j,k)
           eul_stat(ns+4) = eul_stat(ns+4) + dt*alpha*tmp7(i,j,k)**2

           ! Particle velocity (V)
           eul_stat(ns+5) = eul_stat(ns+5) + dt*tmp8(i,j,k)
           eul_stat(ns+6) = eul_stat(ns+6) + dt*tmp8(i,j,k)**2
           eul_stat(ns+7) = eul_stat(ns+7) + dt*alpha*tmp8(i,j,k)
           eul_stat(ns+8) = eul_stat(ns+8) + dt*alpha*tmp8(i,j,k)**2

           ! Particle velocity (W)
           eul_stat(ns+9)  = eul_stat(ns+9)  + dt*tmp9(i,j,k)
           eul_stat(ns+10) = eul_stat(ns+10) + dt*tmp9(i,j,k)**2
           eul_stat(ns+11) = eul_stat(ns+11) + dt*alpha*tmp9(i,j,k)
           eul_stat(ns+12) = eul_stat(ns+12) + dt*alpha*tmp9(i,j,k)**2

           ! Particle velocity (cross correlation)
           eul_stat(ns+13) = eul_stat(ns+13) + dt*alpha*tmp7(i,j,k)*tmp8(i,j,k)
           eul_stat(ns+14) = eul_stat(ns+14) + dt*alpha*tmp7(i,j,k)*tmp9(i,j,k)
           eul_stat(ns+15) = eul_stat(ns+15) + dt*alpha*tmp8(i,j,k)*tmp9(i,j,k)

           ! Particle velocity (U Alpha^k)
           eul_stat(ns+16) = eul_stat(ns+16) + dt*tmp7(i,j,k)*alpha**2
           eul_stat(ns+17) = eul_stat(ns+17) + dt*tmp7(i,j,k)*alpha**3
           eul_stat(ns+18) = eul_stat(ns+18) + dt*tmp7(i,j,k)**2*alpha**2
           eul_stat(ns+19) = eul_stat(ns+19) + dt*tmp7(i,j,k)**2*alpha**3
           eul_stat(ns+20) = eul_stat(ns+20) + dt*tmp10(i,j,k)*alpha
           eul_stat(ns+21) = eul_stat(ns+21) + dt*tmp10(i,j,k)*alpha**2
           eul_stat(ns+22) = eul_stat(ns+22) + dt*tmp10(i,j,k)*alpha**3

           ns = ns+22

           ! Fluid velocity (Uf)
           eul_stat(ns+1) = eul_stat(ns+1) + dt*Ui(i,j,k)
           eul_stat(ns+2) = eul_stat(ns+2) + dt*Ui(i,j,k)**2
           eul_stat(ns+3) = eul_stat(ns+3) + dt*EPSF(i,j,k)*Ui(i,j,k)
           eul_stat(ns+4) = eul_stat(ns+4) + dt*EPSF(i,j,k)*Ui(i,j,k)**2
           eul_stat(ns+5) = eul_stat(ns+5) + dt*alpha*Ui(i,j,k)
           eul_stat(ns+6) = eul_stat(ns+6) + dt*alpha*Ui(i,j,k)**2

           ! Fluid velocity (Vf)
           eul_stat(ns+7) = eul_stat(ns+7) + dt*Vi(i,j,k)
           eul_stat(ns+8) = eul_stat(ns+8) + dt*Vi(i,j,k)**2
           eul_stat(ns+9) = eul_stat(ns+9) + dt*EPSF(i,j,k)*Vi(i,j,k)
           eul_stat(ns+10) = eul_stat(ns+10) + dt*EPSF(i,j,k)*Vi(i,j,k)**2
           eul_stat(ns+11) = eul_stat(ns+11) + dt*alpha*Vi(i,j,k)
           eul_stat(ns+12) = eul_stat(ns+12) + dt*alpha*Vi(i,j,k)**2

           ! Fluid velocity (Wf)
           eul_stat(ns+13) = eul_stat(ns+13) + dt*Wi(i,j,k)
           eul_stat(ns+14) = eul_stat(ns+14) + dt*Wi(i,j,k)**2
           eul_stat(ns+15) = eul_stat(ns+15) + dt*EPSF(i,j,k)*Wi(i,j,k)
           eul_stat(ns+16) = eul_stat(ns+16) + dt*EPSF(i,j,k)*Wi(i,j,k)**2
           eul_stat(ns+17) = eul_stat(ns+17) + dt*alpha*Wi(i,j,k)
           eul_stat(ns+18) = eul_stat(ns+18) + dt*alpha*Wi(i,j,k)**2

           ! Fluid velocity (cross correlation)
           eul_stat(ns+19) = eul_stat(ns+19) + dt*Ui(i,j,k)*Vi(i,j,k)
           eul_stat(ns+20) = eul_stat(ns+20) + dt*Ui(i,j,k)*Wi(i,j,k)
           eul_stat(ns+21) = eul_stat(ns+21) + dt*Vi(i,j,k)*Wi(i,j,k)
           eul_stat(ns+22) = eul_stat(ns+22) + dt*EPSF(i,j,k)*Ui(i,j,k)*Vi(i,j,k)
           eul_stat(ns+23) = eul_stat(ns+23) + dt*EPSF(i,j,k)*Ui(i,j,k)*Wi(i,j,k)
           eul_stat(ns+24) = eul_stat(ns+24) + dt*EPSF(i,j,k)*Vi(i,j,k)*Wi(i,j,k)
           eul_stat(ns+25) = eul_stat(ns+25) + dt*alpha*Ui(i,j,k)*Vi(i,j,k)
           eul_stat(ns+26) = eul_stat(ns+26) + dt*alpha*Ui(i,j,k)*Wi(i,j,k)
           eul_stat(ns+27) = eul_stat(ns+27) + dt*alpha*Vi(i,j,k)*Wi(i,j,k)

           ! Fluid velocity alpha^k
           eul_stat(ns+28) = eul_stat(ns+28) + dt*Ui(i,j,k)*alpha**2
           eul_stat(ns+29) = eul_stat(ns+29) + dt*Ui(i,j,k)*alpha**3

           ns = ns+29

           ! Particle-fluid velocity correlation
           eul_stat(ns+1) = eul_stat(ns+1) + dt*alpha*tmp7(i,j,k)*Ui(i,j,k)
           eul_stat(ns+2) = eul_stat(ns+2) + dt*alpha*tmp7(i,j,k)*Vi(i,j,k)
           eul_stat(ns+3) = eul_stat(ns+3) + dt*alpha*tmp7(i,j,k)*Wi(i,j,k)
           eul_stat(ns+4) = eul_stat(ns+4) + dt*alpha*tmp8(i,j,k)*Ui(i,j,k)
           eul_stat(ns+5) = eul_stat(ns+5) + dt*alpha*tmp8(i,j,k)*Vi(i,j,k)
           eul_stat(ns+6) = eul_stat(ns+6) + dt*alpha*tmp8(i,j,k)*Wi(i,j,k)
           eul_stat(ns+7) = eul_stat(ns+7) + dt*alpha*tmp9(i,j,k)*Ui(i,j,k)
           eul_stat(ns+8) = eul_stat(ns+8) + dt*alpha*tmp9(i,j,k)*Vi(i,j,k)
           eul_stat(ns+9) = eul_stat(ns+9) + dt*alpha*tmp9(i,j,k)*Wi(i,j,k)

           ns = ns+9

           ! Fluid pressure terms
           eul_stat(ns+1) = eul_stat(ns+1) + dt*stat_pres_x(i,j,k)
           eul_stat(ns+2) = eul_stat(ns+2) + dt*stat_pres_y(i,j,k)
           eul_stat(ns+3) = eul_stat(ns+3) + dt*stat_pres_z(i,j,k)
           eul_stat(ns+4) = eul_stat(ns+4) + dt*alpha*stat_pres_x(i,j,k)
           eul_stat(ns+5) = eul_stat(ns+5) + dt*alpha*stat_pres_y(i,j,k)
           eul_stat(ns+6) = eul_stat(ns+6) + dt*alpha*stat_pres_z(i,j,k)
           eul_stat(ns+7) = eul_stat(ns+7) + dt*EPSF(i,j,k)*stat_pres_x(i,j,k)
           eul_stat(ns+8) = eul_stat(ns+8) + dt*EPSF(i,j,k)*stat_pres_y(i,j,k)
           eul_stat(ns+9) = eul_stat(ns+9) + dt*EPSF(i,j,k)*stat_pres_z(i,j,k)
           eul_stat(ns+10) = eul_stat(ns+10) + dt*alpha*tmp7(i,j,k)*stat_pres_x(i,j,k)
           eul_stat(ns+11) = eul_stat(ns+11) + dt*alpha*tmp7(i,j,k)*stat_pres_y(i,j,k)
           eul_stat(ns+12) = eul_stat(ns+12) + dt*alpha*tmp7(i,j,k)*stat_pres_z(i,j,k)
           eul_stat(ns+13) = eul_stat(ns+13) + dt*alpha*tmp8(i,j,k)*stat_pres_x(i,j,k)
           eul_stat(ns+14) = eul_stat(ns+14) + dt*alpha*tmp8(i,j,k)*stat_pres_y(i,j,k)
           eul_stat(ns+15) = eul_stat(ns+15) + dt*alpha*tmp8(i,j,k)*stat_pres_z(i,j,k)
           eul_stat(ns+16) = eul_stat(ns+16) + dt*alpha*tmp9(i,j,k)*stat_pres_x(i,j,k)
           eul_stat(ns+17) = eul_stat(ns+17) + dt*alpha*tmp9(i,j,k)*stat_pres_y(i,j,k)
           eul_stat(ns+18) = eul_stat(ns+18) + dt*alpha*tmp9(i,j,k)*stat_pres_z(i,j,k)
           eul_stat(ns+19) = eul_stat(ns+19) + dt*EPSF(i,j,k)*Ui(i,j,k)*stat_pres_x(i,j,k)
           eul_stat(ns+20) = eul_stat(ns+20) + dt*EPSF(i,j,k)*Ui(i,j,k)*stat_pres_y(i,j,k)
           eul_stat(ns+21) = eul_stat(ns+21) + dt*EPSF(i,j,k)*Ui(i,j,k)*stat_pres_z(i,j,k)
           eul_stat(ns+22) = eul_stat(ns+22) + dt*EPSF(i,j,k)*Vi(i,j,k)*stat_pres_x(i,j,k)
           eul_stat(ns+23) = eul_stat(ns+23) + dt*EPSF(i,j,k)*Vi(i,j,k)*stat_pres_y(i,j,k)
           eul_stat(ns+24) = eul_stat(ns+24) + dt*EPSF(i,j,k)*Vi(i,j,k)*stat_pres_z(i,j,k)
           eul_stat(ns+25) = eul_stat(ns+25) + dt*EPSF(i,j,k)*Wi(i,j,k)*stat_pres_x(i,j,k)
           eul_stat(ns+26) = eul_stat(ns+26) + dt*EPSF(i,j,k)*Wi(i,j,k)*stat_pres_y(i,j,k)
           eul_stat(ns+27) = eul_stat(ns+27) + dt*EPSF(i,j,k)*Wi(i,j,k)*stat_pres_z(i,j,k)
           eul_stat(ns+28) = eul_stat(ns+28) + dt*alpha*Ui(i,j,k)*stat_pres_x(i,j,k)
           eul_stat(ns+29) = eul_stat(ns+29) + dt*alpha*Ui(i,j,k)*stat_pres_y(i,j,k)
           eul_stat(ns+30) = eul_stat(ns+30) + dt*alpha*Ui(i,j,k)*stat_pres_z(i,j,k)
           eul_stat(ns+31) = eul_stat(ns+31) + dt*alpha*Vi(i,j,k)*stat_pres_x(i,j,k)
           eul_stat(ns+32) = eul_stat(ns+32) + dt*alpha*Vi(i,j,k)*stat_pres_y(i,j,k)
           eul_stat(ns+33) = eul_stat(ns+33) + dt*alpha*Vi(i,j,k)*stat_pres_z(i,j,k)
           eul_stat(ns+34) = eul_stat(ns+34) + dt*alpha*Wi(i,j,k)*stat_pres_x(i,j,k)
           eul_stat(ns+35) = eul_stat(ns+35) + dt*alpha*Wi(i,j,k)*stat_pres_y(i,j,k)
           eul_stat(ns+36) = eul_stat(ns+36) + dt*alpha*Wi(i,j,k)*stat_pres_z(i,j,k)
           eul_stat(ns+37) = eul_stat(ns+37) + dt*Ui(i,j,k)*stat_pres_x(i,j,k)
           eul_stat(ns+38) = eul_stat(ns+38) + dt*Ui(i,j,k)*stat_pres_y(i,j,k)
           eul_stat(ns+39) = eul_stat(ns+39) + dt*Ui(i,j,k)*stat_pres_z(i,j,k)
           eul_stat(ns+40) = eul_stat(ns+40) + dt*Vi(i,j,k)*stat_pres_x(i,j,k)
           eul_stat(ns+41) = eul_stat(ns+41) + dt*Vi(i,j,k)*stat_pres_y(i,j,k)
           eul_stat(ns+42) = eul_stat(ns+42) + dt*Vi(i,j,k)*stat_pres_z(i,j,k)
           eul_stat(ns+43) = eul_stat(ns+43) + dt*Wi(i,j,k)*stat_pres_x(i,j,k)
           eul_stat(ns+44) = eul_stat(ns+44) + dt*Wi(i,j,k)*stat_pres_y(i,j,k)
           eul_stat(ns+45) = eul_stat(ns+45) + dt*Wi(i,j,k)*stat_pres_z(i,j,k)

           ns = ns+45

           ! Fluid viscous terms
           eul_stat(ns+1) = eul_stat(ns+1) + dt*stat_visc_x(i,j,k)
           eul_stat(ns+2) = eul_stat(ns+2) + dt*stat_visc_y(i,j,k)
           eul_stat(ns+3) = eul_stat(ns+3) + dt*stat_visc_z(i,j,k)
           eul_stat(ns+4) = eul_stat(ns+4) + dt*alpha*stat_visc_x(i,j,k)
           eul_stat(ns+5) = eul_stat(ns+5) + dt*alpha*stat_visc_y(i,j,k)
           eul_stat(ns+6) = eul_stat(ns+6) + dt*alpha*stat_visc_z(i,j,k)
           eul_stat(ns+7) = eul_stat(ns+7) + dt*EPSF(i,j,k)*stat_visc_x(i,j,k)
           eul_stat(ns+8) = eul_stat(ns+8) + dt*EPSF(i,j,k)*stat_visc_y(i,j,k)
           eul_stat(ns+9) = eul_stat(ns+9) + dt*EPSF(i,j,k)*stat_visc_z(i,j,k)
           eul_stat(ns+10) = eul_stat(ns+10) + dt*alpha*tmp7(i,j,k)*stat_visc_x(i,j,k)
           eul_stat(ns+11) = eul_stat(ns+11) + dt*alpha*tmp7(i,j,k)*stat_visc_y(i,j,k)
           eul_stat(ns+12) = eul_stat(ns+12) + dt*alpha*tmp7(i,j,k)*stat_visc_z(i,j,k)
           eul_stat(ns+13) = eul_stat(ns+13) + dt*alpha*tmp8(i,j,k)*stat_visc_x(i,j,k)
           eul_stat(ns+14) = eul_stat(ns+14) + dt*alpha*tmp8(i,j,k)*stat_visc_y(i,j,k)
           eul_stat(ns+15) = eul_stat(ns+15) + dt*alpha*tmp8(i,j,k)*stat_visc_z(i,j,k)
           eul_stat(ns+16) = eul_stat(ns+16) + dt*alpha*tmp9(i,j,k)*stat_visc_x(i,j,k)
           eul_stat(ns+17) = eul_stat(ns+17) + dt*alpha*tmp9(i,j,k)*stat_visc_y(i,j,k)
           eul_stat(ns+18) = eul_stat(ns+18) + dt*alpha*tmp9(i,j,k)*stat_visc_z(i,j,k)
           eul_stat(ns+19) = eul_stat(ns+19) + dt*EPSF(i,j,k)*Ui(i,j,k)*stat_visc_x(i,j,k)
           eul_stat(ns+20) = eul_stat(ns+20) + dt*EPSF(i,j,k)*Ui(i,j,k)*stat_visc_y(i,j,k)
           eul_stat(ns+21) = eul_stat(ns+21) + dt*EPSF(i,j,k)*Ui(i,j,k)*stat_visc_z(i,j,k)
           eul_stat(ns+22) = eul_stat(ns+22) + dt*EPSF(i,j,k)*Vi(i,j,k)*stat_visc_x(i,j,k)
           eul_stat(ns+23) = eul_stat(ns+23) + dt*EPSF(i,j,k)*Vi(i,j,k)*stat_visc_y(i,j,k)
           eul_stat(ns+24) = eul_stat(ns+24) + dt*EPSF(i,j,k)*Vi(i,j,k)*stat_visc_z(i,j,k)
           eul_stat(ns+25) = eul_stat(ns+25) + dt*EPSF(i,j,k)*Wi(i,j,k)*stat_visc_x(i,j,k)
           eul_stat(ns+26) = eul_stat(ns+26) + dt*EPSF(i,j,k)*Wi(i,j,k)*stat_visc_y(i,j,k)
           eul_stat(ns+27) = eul_stat(ns+27) + dt*EPSF(i,j,k)*Wi(i,j,k)*stat_visc_z(i,j,k)
           eul_stat(ns+28) = eul_stat(ns+28) + dt*alpha*Ui(i,j,k)*stat_visc_x(i,j,k)
           eul_stat(ns+29) = eul_stat(ns+29) + dt*alpha*Ui(i,j,k)*stat_visc_y(i,j,k)
           eul_stat(ns+30) = eul_stat(ns+30) + dt*alpha*Ui(i,j,k)*stat_visc_z(i,j,k)
           eul_stat(ns+31) = eul_stat(ns+31) + dt*alpha*Vi(i,j,k)*stat_visc_x(i,j,k)
           eul_stat(ns+32) = eul_stat(ns+32) + dt*alpha*Vi(i,j,k)*stat_visc_y(i,j,k)
           eul_stat(ns+33) = eul_stat(ns+33) + dt*alpha*Vi(i,j,k)*stat_visc_z(i,j,k)
           eul_stat(ns+34) = eul_stat(ns+34) + dt*alpha*Wi(i,j,k)*stat_visc_x(i,j,k)
           eul_stat(ns+35) = eul_stat(ns+35) + dt*alpha*Wi(i,j,k)*stat_visc_y(i,j,k)
           eul_stat(ns+36) = eul_stat(ns+36) + dt*alpha*Wi(i,j,k)*stat_visc_z(i,j,k)
           eul_stat(ns+37) = eul_stat(ns+37) + dt*Ui(i,j,k)*stat_visc_x(i,j,k)
           eul_stat(ns+38) = eul_stat(ns+38) + dt*Ui(i,j,k)*stat_visc_y(i,j,k)
           eul_stat(ns+39) = eul_stat(ns+39) + dt*Ui(i,j,k)*stat_visc_z(i,j,k)
           eul_stat(ns+40) = eul_stat(ns+40) + dt*Vi(i,j,k)*stat_visc_x(i,j,k)
           eul_stat(ns+41) = eul_stat(ns+41) + dt*Vi(i,j,k)*stat_visc_y(i,j,k)
           eul_stat(ns+42) = eul_stat(ns+42) + dt*Vi(i,j,k)*stat_visc_z(i,j,k)
           eul_stat(ns+43) = eul_stat(ns+43) + dt*Wi(i,j,k)*stat_visc_x(i,j,k)
           eul_stat(ns+44) = eul_stat(ns+44) + dt*Wi(i,j,k)*stat_visc_y(i,j,k)
           eul_stat(ns+45) = eul_stat(ns+45) + dt*Wi(i,j,k)*stat_visc_z(i,j,k)

           ns = ns+45

           ! Collision frequency
           eul_stat(ns+1) = eul_stat(ns+1) + dt*tmp16(i,j,k)/dt_col

           ! --------------------------
           ! CONDITIONAL STATS
           
           ! Compute index of bin
           ind=ceiling(alpha/dbin)
           if (ind.le.nbin) then
              ! PDF
              alpha_stat(ind,1) = alpha_stat(ind,1) + dt
              alpha_stat(ind,2) = alpha_stat(ind,2) + dt*tmp7(i,j,k)
              alpha_stat(ind,3) = alpha_stat(ind,3) + dt*tmp8(i,j,k)
              alpha_stat(ind,4) = alpha_stat(ind,4) + dt*tmp9(i,j,k)
              alpha_stat(ind,5) = alpha_stat(ind,5) + dt*Ui(i,j,k)
              alpha_stat(ind,6) = alpha_stat(ind,6) + dt*Vi(i,j,k)
              alpha_stat(ind,7) = alpha_stat(ind,7) + dt*Wi(i,j,k)
              alpha_stat(ind,8) = alpha_stat(ind,8) + dt*abs(tmp7(i,j,k)-Ui(i,j,k))
              alpha_stat(ind,9) = alpha_stat(ind,9) + dt*tmp10(i,j,k)
              alpha_stat(ind,10) = alpha_stat(ind,10) + dt*tmp11(i,j,k)
              alpha_stat(ind,11) = alpha_stat(ind,11) + dt*tmp12(i,j,k)
              alpha_stat(ind,12) = alpha_stat(ind,12) + dt*tmp13(i,j,k)
              alpha_stat(ind,13) = alpha_stat(ind,13) + dt*tmp14(i,j,k)
              alpha_stat(ind,14) = alpha_stat(ind,14) + dt*tmp15(i,j,k)
              alpha_stat(ind,15) = alpha_stat(ind,15) + dt*tmp7(i,j,k)**2
              alpha_stat(ind,16) = alpha_stat(ind,16) + dt*tmp8(i,j,k)**2
              alpha_stat(ind,17) = alpha_stat(ind,17) + dt*tmp9(i,j,k)**2
              alpha_stat(ind,18) = alpha_stat(ind,18) + dt*tmp7(i,j,k)*tmp8(i,j,k)
              alpha_stat(ind,19) = alpha_stat(ind,19) + dt*tmp7(i,j,k)*tmp9(i,j,k)
              alpha_stat(ind,20) = alpha_stat(ind,20) + dt*tmp8(i,j,k)*tmp9(i,j,k)
              alpha_stat(ind,21) = alpha_stat(ind,21) + dt*Ui(i,j,k)**2
              alpha_stat(ind,22) = alpha_stat(ind,22) + dt*Vi(i,j,k)**2
              alpha_stat(ind,23) = alpha_stat(ind,23) + dt*Wi(i,j,k)**2
              alpha_stat(ind,24) = alpha_stat(ind,24) + dt*Ui(i,j,k)*Vi(i,j,k)
              alpha_stat(ind,25) = alpha_stat(ind,25) + dt*Ui(i,j,k)*Wi(i,j,k)
              alpha_stat(ind,26) = alpha_stat(ind,26) + dt*Vi(i,j,k)*Wi(i,j,k)
              alpha_stat(ind,27) = alpha_stat(ind,27) + dt*tmp7(i,j,k)*Ui(i,j,k)
              alpha_stat(ind,28) = alpha_stat(ind,28) + dt*tmp7(i,j,k)*Vi(i,j,k)
              alpha_stat(ind,29) = alpha_stat(ind,29) + dt*tmp7(i,j,k)*Wi(i,j,k)
              alpha_stat(ind,30) = alpha_stat(ind,30) + dt*tmp8(i,j,k)*Ui(i,j,k)
              alpha_stat(ind,31) = alpha_stat(ind,31) + dt*tmp8(i,j,k)*Vi(i,j,k)
              alpha_stat(ind,32) = alpha_stat(ind,32) + dt*tmp8(i,j,k)*Wi(i,j,k)
              alpha_stat(ind,33) = alpha_stat(ind,33) + dt*tmp9(i,j,k)*Ui(i,j,k)
              alpha_stat(ind,34) = alpha_stat(ind,34) + dt*tmp9(i,j,k)*Vi(i,j,k)
              alpha_stat(ind,35) = alpha_stat(ind,35) + dt*tmp9(i,j,k)*Wi(i,j,k)
              alpha_stat(ind,36) = alpha_stat(ind,36) + dt*stat_pres_x(i,j,k)
              alpha_stat(ind,37) = alpha_stat(ind,37) + dt*stat_pres_y(i,j,k)
              alpha_stat(ind,38) = alpha_stat(ind,38) + dt*stat_pres_z(i,j,k)
              alpha_stat(ind,39) = alpha_stat(ind,39) + dt*stat_visc_x(i,j,k)
              alpha_stat(ind,40) = alpha_stat(ind,40) + dt*stat_visc_y(i,j,k)
              alpha_stat(ind,41) = alpha_stat(ind,41) + dt*stat_visc_z(i,j,k)
              alpha_stat(ind,42) = alpha_stat(ind,42) + dt*tmp16(i,j,k)/dt_col
           end if
              
        end do
     end do
  end do


  ! Update accumulated time
  Delta_t = Delta_t + dt
  
  ! Stop the timer
  call timing_stop('stat_lpt')
  
  return
end subroutine stat_lpt_sample


! ------------------------- !
! Read statistics from file !
! ------------------------- !
subroutine stat_lpt_read
  use stat_lpt
  use parallel
  use data
  use lpt
  implicit none
  
  real(WP) :: t1,t2,Delta_t2
  integer :: dim
  integer, dimension(2) :: dims
  integer, dimension(MPI_STATUS_SIZE) :: status
  character(len=str_medium)  :: name
  character(len=str_medium) :: filename
  integer :: i,var,ierr,ifile

  if (irank.eq.iroot) write(*,*) 'Reading stats!'
  
  ! -------------------------------
  ! READ GLOBAL MEAN STATS

  ! Open the stats file to read
  filename = "stat/stat-lpt"
  call MPI_FILE_OPEN(comm,filename,MPI_MODE_RDONLY,mpi_info,ifile,ierr)
  
  ! Read dimensions from header
  call MPI_FILE_READ_ALL(ifile,dim,1,MPI_INTEGER,status,ierr)
  if (dim.ne.nvar_lag) call die('stat_lpt_read: Wrong number of Lagrangian variables in stat-lpt file')
  call MPI_FILE_READ_ALL(ifile,dim,1,MPI_INTEGER,status,ierr)
  if (dim.ne.nvar_eul) call die('stat_lpt_read: Wrong number of Eulerian variables in stat-lpt file')
  
  ! Read some headers
  call MPI_FILE_READ_ALL(ifile,Delta_t,1,MPI_REAL_WP,status,ierr)
  call MPI_FILE_READ_ALL(ifile,t1,1,MPI_REAL_WP,status,ierr)

  ! Read Lagrangian names
  do var=1,nvar_lag
     call MPI_FILE_READ_ALL(ifile,name,str_medium,MPI_CHARACTER,status,ierr)
     if (name.ne.lag_name(var)) then
        call die('stat_lpt_read: Lagrangian names in stat-lpt and data files do not correspond')
     end if
  end do
  
  ! Read Lagrangian data
  call MPI_FILE_READ_ALL(ifile,lag_buf,nvar_lag,MPI_REAL_WP,status,ierr)
  
  ! Read Eulerian names
  do var=1,nvar_eul
     call MPI_FILE_READ_ALL(ifile,name,str_medium,MPI_CHARACTER,status,ierr)
     if (name.ne.eul_name(var)) then
        call die('stat_lpt_read: Eulerian names in stat-lpt and data files do not correspond')
     end if
  end do
  
  ! Read Eulerian data
  call MPI_FILE_READ_ALL(ifile,eul_buf,nvar_eul,MPI_REAL_WP,status,ierr)

  ! Close the file
  call MPI_FILE_CLOSE(ifile,ierr)
  
  ! Restart the stats
  lag_stat = lag_buf*Delta_t*real(npart_,WP)
  eul_stat = eul_buf*Delta_t*real(nx_,WP)*real(ny_,WP)*real(nz_,WP)

  ! -------------------------------
  ! READ CONDITIONAL MEAN STATS

  ! Open the conditional stats file to read
  filename = "stat/stat-lpt-alpha"
  call MPI_FILE_OPEN(comm,filename,MPI_MODE_RDONLY,mpi_info,ifile,ierr)
  
  ! Read dimensions from header
  call MPI_FILE_READ_ALL(ifile,dims,2,MPI_INTEGER,status,ierr)
  if (dims(1).ne.nbin) then
     print*, 'expected = ',nbin
     print*, 'stat-lpt-alpha = ',dims(1)
     call die('stat_1dx_read: The size of stat-lpt-alpha is incorrect')
  end if
  if (dims(2).ne.nvar_alpha) call die('stat_lpt_read: Wrong number of variables in stat-lpt-alpha file')
  
  ! Read some headers
  call MPI_FILE_READ_ALL(ifile,Delta_t2,1,MPI_REAL_WP,status,ierr)
  if (Delta_t2.ne.Delta_t) call die('stat_lpt_read: stat-lpt-alpha Delta_t does not match stat-lpt Delta_t')
  call MPI_FILE_READ_ALL(ifile,t2,1,MPI_REAL_WP,status,ierr)
  if (t2.ne.t1) call die('stat_lpt_read: stat-lpt-alpha time does not match stat-lpt time')
  
  ! Read variable names
  do var=1,nvar_alpha
     call MPI_FILE_READ_ALL(ifile,name,str_medium,MPI_CHARACTER,status,ierr)
     if (name.ne.alpha_name(var)) then
        call die('stat_lpt_read: Variables names in stat-lpt-alpha and data files do not correspond')
     end if
  end do
  
  ! Read
  call MPI_FILE_READ_ALL(ifile,buf_alpha,nvar_alpha,MPI_REAL_WP,status,ierr)

  ! Close the file
  call MPI_FILE_CLOSE(ifile,ierr)
  
  ! Restart the stats
  do i=1,nbin
     ! PDF
     alpha_stat(i,1) = buf_alpha(i,1)*Delta_t
     ! Conditional mean
     alpha_stat(i,2:nvar_alpha) = buf_alpha(i,2:nvar_alpha)*alpha_stat(i,1)
  end do
  
  return
end subroutine stat_lpt_read

! --------------- !
! write stat data !
! --------------- !
subroutine stat_lpt_write
  use stat_lpt
  use parallel
  use time_info
  use fileio
  use data
  use lpt
  implicit none
  
  character(len=str_medium) :: filename
  integer  :: i,iunit,ierr,var
  
  ! Nothing to write
  if (Delta_t.eq.0.0_WP) return
  
  ! Start the timer
  call timing_start('stat_lpt')
  
  ! Gather the data
  lag_buf = 0.0_WP
  call parallel_sum(lag_stat,lag_buf)
  eul_buf = 0.0_WP
  call parallel_sum(eul_stat,eul_buf)
  buf_alpha = 0.0_WP
  call parallel_sum(alpha_stat,buf_alpha)
  
  ! Return if not the root process
  if (irank.ne.iroot) then
     call timing_stop('stat_lpt')
     return
  end if
  
  ! ------- WRITE THE GLOBAL MEAN FILES ------
  ! Normalize data
  lag_buf = lag_buf/(Delta_t*real(npart,WP))
  eul_buf = eul_buf/(Delta_t*real(nx,WP)*real(ny,WP)*real(nz,WP))

  ! Open the stats file
  filename = "stat/stat-lpt"
  call BINARY_FILE_OPEN(iunit,trim(filename),"w",ierr)
  
  ! Write dimensions
  call BINARY_FILE_WRITE(iunit,nvar_lag,1,kind(nvar_lag),ierr)
  call BINARY_FILE_WRITE(iunit,nvar_eul,1,kind(nvar_eul),ierr)
  call BINARY_FILE_WRITE(iunit,Delta_t,1,kind(Delta_t),ierr)
  call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr)

  ! Write the Lagrangian data
  do var=1,nvar_lag
     call BINARY_FILE_WRITE(iunit,lag_name(var),str_medium,kind(lag_name),ierr)
  end do
  call BINARY_FILE_WRITE(iunit,lag_buf,nvar_lag,kind(lag_buf),ierr)
  
  ! Write the Eulerian data
  do var=1,nvar_eul
     call BINARY_FILE_WRITE(iunit,eul_name(var),str_medium,kind(eul_name),ierr)
  end do
  call BINARY_FILE_WRITE(iunit,eul_buf,nvar_eul,kind(eul_buf),ierr)
  
  ! Close the file
  call BINARY_FILE_CLOSE(iunit,ierr)

  ! Write the ASCI file
  iunit = iopen()
  write(filename,'(a17)') "stat/stat-lpt.txt"
  open (iunit, file=filename, form="formatted", iostat=ierr)
  !write(iunit,'(10000a24)')  (trim(adjustl(eul_name(var))),var=1,nvar_eul)
  !write(iunit,'(10000ES24.12)') eul_buf
  do var=1,nvar_lag
     write(iunit,'(1a24,1ES24.12)') trim(adjustl(lag_name(var))), lag_buf(var)
  end do
  do var=1,nvar_eul
     write(iunit,'(1a24,1ES24.12)') trim(adjustl(eul_name(var))), eul_buf(var)
  end do
  close(iclose(iunit))

  ! ------- WRITE THE CONDITIONAL MEAN FILES ------
  ! Normalize data
  do i=1,nbin

     if (buf_alpha(i,1).gt.0.0_WP) then
        buf_alpha(i,2:nvar_alpha) = buf_alpha(i,2:nvar_alpha)/buf_alpha(i,1)
     else
        buf_alpha(i,2:nvar_alpha) = 0.0_WP
     end if
     buf_alpha(i,1) = buf_alpha(i,1)/Delta_t

  end do

  ! Open the stats file
  filename = "stat/stat-alpha"
  call BINARY_FILE_OPEN(iunit,trim(filename),"w",ierr)
  
  ! Write dimensions
  call BINARY_FILE_WRITE(iunit,nbin,1,kind(nbin),ierr)
  call BINARY_FILE_WRITE(iunit,nvar_alpha,1,kind(nvar_alpha),ierr)
  call BINARY_FILE_WRITE(iunit,Delta_t,1,kind(Delta_t),ierr)
  call BINARY_FILE_WRITE(iunit,time,1,kind(time),ierr)
  
  ! Write variable names
  do var=1,nvar_alpha
     call BINARY_FILE_WRITE(iunit,alpha_name(var),str_medium,kind(alpha_name),ierr)
  end do
  
  ! Write the stats
  call BINARY_FILE_WRITE(iunit,buf_alpha,nbin*nvar_alpha,kind(buf_alpha),ierr)
  
  ! Close the file
  call BINARY_FILE_CLOSE(iunit,ierr)
  
  ! Write the ASCI file
  iunit = iopen()
  write(filename,'(a19)') "stat/stat-alpha.txt"
  open (iunit, file=filename, form="formatted", iostat=ierr)
  write(iunit,'(10000a24)')  'Alpha', (trim(adjustl(alpha_name(var))),var=1,nvar_alpha)
  do i=1,nbin
     write(iunit,'(10000ES24.12)') 0.5_WP*(bin_alpha(i)+bin_alpha(i+1)),buf_alpha(i,:)
  end do
  close(iclose(iunit))
  
  ! Log
  call monitor_log("LPT STATISTICS FILE WRITTEN")

  ! Stop the timer
  call timing_stop('stat_lpt')
  
  return
end subroutine stat_lpt_write


