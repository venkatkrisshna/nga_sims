module dump_stat2dhit_visit
  use parallel
  use precision
  use geometry
  use partition
  use masks
  use fileio
  use memory

  ! Modules to dump
  use interpolate
  use stat_2d_hit
  
  implicit none

  ! For SILO_10.2, use f9x
  include "silo_f9x.inc"
  !include "silo.inc"

  ! Time info
  integer :: nout_time
  
  ! Output variables
  integer :: output_n,output_max,output_nSC
  character(len=str_medium), dimension(:),   allocatable :: output_types
  character(len=str_medium), dimension(:),   allocatable :: output_names
  
  ! Silo database
  integer :: silo_comm,silo_irank,Nproc_node
  integer, dimension(:), allocatable :: group_ids,proc_nums
  real(WP), dimension(:,:),   allocatable ::  spatial_extents, myspatial_extents
  real(WP), dimension(:,:),   allocatable :: Uspatial_extents,Umyspatial_extents
  real(WP), dimension(:,:),   allocatable :: Vspatial_extents,Vmyspatial_extents
  real(WP), dimension(:,:),   allocatable :: Wspatial_extents,Wmyspatial_extents
  real(WP), dimension(:,:,:,:), allocatable :: data_extents,   mydata_extents
  character(len=60), dimension(:), allocatable :: names
  integer, dimension(:), allocatable :: lnames,types
  
  ! Mesh 
  integer, parameter :: nGhost=2
  integer :: nxout,nyout,nzout
  integer :: noverxmin_out,noverymin_out,noverzmin_out
  integer :: noverxmax_out,noverymax_out,noverzmax_out
  integer :: imin_out,jmin_out,kmin_out
  integer :: imax_out,jmax_out,kmax_out
  integer, dimension(2) :: lo_offset, hi_offset
  
  ! Buffers
  real(SP), dimension(:,:), allocatable :: out_buf


contains
  
  ! =========================================== !
  !  Data to write to file                      !
  !   - Add data to write here                  !
  !   - Add quantity calculated to stat_2d_hit  !
  !                                             !
  ! =========================================== !
  subroutine dump_stat2dhit_visit_getData(n)
    implicit none
    integer, intent(in) :: n

    select case(trim(output_names(n)))
       
    ! ============ Core Variables =====================

    ! Statistics to dump to VisIt

    case ('Umean')
      output_types(n)='scalar'
      out_buf = real(buf(:,:,1),SP)

    case ('Vmean')
      output_types(n)='scalar'
      out_buf = real(buf(:,:,2),SP)

    case ('Wmean')
      output_types(n)='scalar'
      out_buf = real(buf(:,:,3),SP)

    case ('U2mean')
      output_types(n)='scalar'
      out_buf = real(buf(:,:,4),SP)

    case ('V2mean')
      output_types(n)='scalar'
      out_buf = real(buf(:,:,5),SP)

    case ('W2mean')
      output_types(n)='scalar'
      out_buf = real(buf(:,:,6),SP)

    case ('UVmean')
      output_types(n)='scalar'
      out_buf = real(buf(:,:,7),SP)

    case ('UWmean')
      output_types(n)='scalar'
      out_buf = real(buf(:,:,8),SP)

    case ('VWmean')
      output_types(n)='scalar'
      out_buf = real(buf(:,:,9),SP)

    case ('Alphmean')
      output_types(n)='scalar'
      out_buf = real(buf(:,:,10),SP)

    case ('Alph2mean')
      output_types(n)='scalar'
      out_buf = real(buf(:,:,11),SP)

    case ('AlphUmean')
      output_types(n)='scalar'
      out_buf = real(buf(:,:,12),SP)

    case ('AlphVmean')
      output_types(n)='scalar'
      out_buf = real(buf(:,:,13),SP)

    case ('AlphWmean')
      output_types(n)='scalar'
      out_buf = real(buf(:,:,14),SP)

    case ('SurfAmean')
      output_types(n)='scalar'
      out_buf = real(buf(:,:,15),SP)

    case ('SurfA2mean')
      output_types(n)='scalar'
      out_buf = real(buf(:,:,16),SP)

    case ('STmean')
      output_types(n)='scalar'
      out_buf = real(buf(:,:,17),SP)

    ! Unknown output
    case default
       call die('Unknown Visit output for 2D HIT Stats: '//output_names(n))
    end select
    
    return
  end subroutine dump_stat2dhit_visit_getData
  
end module dump_stat2dhit_visit

! ======================================================================================================== !
! ======================================================================================================== !
!        You do not have to modify the code below this point to add a variable on a pre-defined mesh       !
! ======================================================================================================== !
! ======================================================================================================== !

! ======================================== !
!  Dump Visit Silo files - Initialization  !
! ======================================== !
subroutine dump_stat2dhit_visit_init
  use dump_stat2dhit_visit
  use parser
  use time_info
  implicit none
  integer :: i,ierr,iunit,nchar
  logical :: file_exists
  character(len=5)  :: tmpchar
  character(len=60) :: tmpchar2
  real(WP),dimension(:),allocatable :: visit_times


  ! Create & Start the timer
  call timing_create('stat2dhit-visit')
  call timing_start ('stat2dhit-visit')
  

  ! Check for arts.visit file and create folder if needed
  if (irank.eq.iroot) then
    inquire(file='Stat_Visit/arts.visit',exist=file_exists)
    if (file_exists) then
      ! Get number of lines in file
      iunit = iopen()
      open(iunit,file="Stat_Visit/arts.visit",form="formatted",iostat=ierr,status='old')
      nout_time=0
      do
        read(iunit,*,end=1)
        nout_time=nout_time+1
      end do
1     continue
      close(iclose(iunit))
      ! Read the file and keep times less than current time
      allocate(visit_times(nout_time))
      iunit = iopen()
      open(iunit,file="Stat_Visit/arts.visit",form="formatted",iostat=ierr,status='old')
      do i=1,nout_time
        ! Read file
        read(iunit,'(5A,60A)') tmpchar,tmpchar2
        ! Extract time from string
        nchar=len_trim(tmpchar2)
        read(tmpchar2(1:nchar-11),*) visit_times(i)
        ! Check if it is in the future and exit if true
        if (visit_times(i).ge.time-dt*1e-10_WP) then
          nout_time=i-1
          exit
        end if
      end do
      close(iclose(iunit))
      ! Write new file with only past times
      iunit = iopen()
      open(iunit,file="Stat_Visit/arts.visit",form="formatted",iostat=ierr,status='replace')
      do i=1,nout_time
        write(tmpchar2,'(ES12.5)') visit_times(i)
        tmpchar2='time_'//trim(adjustl(tmpchar2))//'/Visit.silo'
        write(iunit,'(A)') trim(adjustl(tmpchar2))
      end do
      deallocate(visit_times)
      close(iclose(iunit))
    else
      nout_time=0
      ! Create visit directory
      if (irank.eq.iroot) call CREATE_FOLDER("Stat_Visit")
    end if
  end if

  ! Number of procs per file
  call parser_read('Processors per silo',Nproc_node,12)

  ! Create group_ids
  allocate(group_ids(nproc))
  do i=1,nproc
     group_ids(i)=ceiling(real(i,WP)/real(Nproc_node,WP))
  end do
  ! Create proc_nums
  allocate(proc_nums(nproc))
  do i=1,nproc
     proc_nums(i)=mod(i-1,Nproc_node)+1
  end do

  ! Split procs into new communicators
  call MPI_COMM_SPLIT(COMM,group_ids(irank),MPI_UNDEFINED,silo_comm,ierr)
  call MPI_COMM_RANK(silo_comm,silo_irank,ierr)
  silo_irank=silo_irank+1

  ! Number of Outputs taken from stat_2d_hit.mod
  output_n = stat_nvar
  allocate(output_names(output_n))

  ! Get output names from stat_2d_hit.mod
  output_names = stat_name

  ! Allocate arrays
  allocate(out_buf(imin_:imax_,jmin_:jmax_))
  output_max=output_n
  allocate(output_types(output_max))
  allocate(  spatial_extents(4,nproc)); allocate( myspatial_extents(4,nproc))
  allocate( Uspatial_extents(4,nproc)); allocate(Umyspatial_extents(4,nproc))
  allocate( Vspatial_extents(4,nproc)); allocate(Vmyspatial_extents(4,nproc))
  allocate( Wspatial_extents(4,nproc)); allocate(Wmyspatial_extents(4,nproc))
  allocate(  data_extents(2,nproc,1,output_max))
  allocate(mydata_extents(2,nproc,1,output_max))
  allocate(names (nproc))
  allocate(lnames(nproc))
  allocate(types (nproc))

  ! Create output limits because Visit doesn't support external ghost cells
  !noverxmin_out=nGhost; noverxmax_out=nGhost; imin_out=imin_-nGhost; imax_out=imax_+nGhost
  !noverymin_out=nGhost; noverymax_out=nGhost; jmin_out=jmin_-nGhost; jmax_out=jmax_+nGhost
  !noverzmin_out=nGhost; noverzmax_out=nGhost; kmin_out=kmin_-nGhost; kmax_out=kmax_+nGhost


  noverxmin_out=0; noverxmax_out=0; imin_out=imin_; imax_out=imax_
  noverymin_out=0; noverymax_out=0; jmin_out=jmin_; jmax_out=jmax_
  noverzmin_out=0; noverzmax_out=0; kmin_out=kmin_; kmax_out=kmax_


  if (iproc.eq.1  ) then; noverxmin_out=0; imin_out=imin_; end if
  if (jproc.eq.1  ) then; noverymin_out=0; jmin_out=jmin_; end if
  if (iproc.eq.npx) then; noverxmax_out=0; imax_out=imax_; end if
  if (jproc.eq.npy) then; noverymax_out=0; jmax_out=jmax_; end if

  nxout=imax_out-imin_out+1
  nyout=jmax_out-jmin_out+1
  hi_offset(1)=noverxmax_out
  hi_offset(2)=noverymax_out
  lo_offset(1)=noverxmin_out
  lo_offset(2)=noverymin_out


  call timing_stop('stat2dhit-visit')
  
  return 
end subroutine dump_stat2dhit_visit_init


! ===================================== !
!  Dump Visit Silo files - Write Files  !
! ===================================== !
subroutine dump_stat2dhit_visit_data
  use dump_stat2dhit_visit
  use time_info 
  implicit none
  integer :: i,n,out
  integer :: dbfile,err,ierr,optlist,iunit
  character(len=str_medium) :: med_buffer
  character(len=5) :: dirname
  character(len=43) :: siloname
  character(len=27) :: folder,buffer
  logical :: file_exists
  
  call timing_start ('stat2dhit-visit')

  ! Update output counter
  nout_time=nout_time+1

  ! Make foldername
  write(buffer,'(ES12.5)') time
  folder = 'Stat_Visit/time_'//trim(adjustl(buffer))

  if (irank.eq.iroot) then
     ! Create new directory
     call CREATE_FOLDER(trim(folder))
  end if
  call MPI_BARRIER(COMM,ierr)

  !  Create the group silo databases 
  ! =============================================

  ! Create the silo name for this processor
  write(siloname,'(A,I5.5,A)') folder//'/group',group_ids(irank),'.silo'

  ! Create the silo database
  if (silo_irank.eq.1) then
     err = dbcreate(siloname, len_trim(siloname), DB_CLOBBER, DB_LOCAL,"Silo database created with NGA", 30, DB_HDF5, dbfile)
     if(dbfile.eq.-1) call die('Could not create Silo file!')
     ierr = dbclose(dbfile)
  end if

  !  Write the data
  ! =============================================
  do n=1,Nproc_node
     if (n.eq.silo_irank) then

        ! Open silo file 
        err = dbopen(siloname,len_trim(siloname),DB_HDF5,DB_APPEND,dbfile)

        ! Create the silo directory
        write(dirname,'(I5.5)') proc_nums(irank)
        err = dbmkdir(dbfile,dirname,5,ierr)
        err = dbsetdir(dbfile,dirname,5)

        ! Write the variables
        mydata_extents=0.0_WP
        do out=1,output_n

           ! Get data to write
           call dump_stat2dhit_visit_getData(out)

           select case(trim(output_types(out)))

           case ('scalar') ! ---------------------------------------------------------------------------------------
              ! Write the scalar
              med_buffer=output_names(out)
              err = dbputqv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
                   "Mesh", 4, out_buf(imin_out:imax_out,jmin_out:jmax_out), &
                   (/nxout,nyout/), 2, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT,DB_F77NULL, ierr)
              mydata_extents(1,irank,1,out)=minval(out_buf(imin_out:imax_out,jmin_out:jmax_out))
              mydata_extents(2,irank,1,out)=maxval(out_buf(imin_out:imax_out,jmin_out:jmax_out))

           case ('scalar_Ucell') ! ---------------------------------------------------------------------------------
              ! Write the scalar
              med_buffer=output_names(out)
              err = dbputqv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
                   "UMesh", 5, out_buf(imin_out:imax_out+1,jmin_out:jmax_out), &
                   (/nxout+1,nyout/), 2, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT,DB_F77NULL, ierr)
              mydata_extents(1,irank,1,out)=minval(out_buf(imin_out:imax_out,jmin_out:jmax_out))
              mydata_extents(2,irank,1,out)=maxval(out_buf(imin_out:imax_out,jmin_out:jmax_out))

           case ('scalar_Vcell') ! ---------------------------------------------------------------------------------
              ! Write the scalar
              med_buffer=output_names(out)
              err = dbputqv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
                   "VMesh", 5, out_buf(imin_out:imax_out,jmin_out:jmax_out+1), &
                   (/nxout,nyout+1/), 2, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT,DB_F77NULL, ierr)
              mydata_extents(1,irank,1,out)=minval(out_buf(imin_out:imax_out,jmin_out:jmax_out))
              mydata_extents(2,irank,1,out)=maxval(out_buf(imin_out:imax_out,jmin_out:jmax_out))

           case ('scalar_Wcell') ! ---------------------------------------------------------------------------------
              ! Write the scalar
              med_buffer=output_names(out)
              err = dbputqv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
                   "WMesh", 5, out_buf(imin_out:imax_out,jmin_out:jmax_out), &
                   (/nxout,nyout/), 2, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT,DB_F77NULL, ierr)
              mydata_extents(1,irank,1,out)=minval(out_buf(imin_out:imax_out,jmin_out:jmax_out))
              mydata_extents(2,irank,1,out)=maxval(out_buf(imin_out:imax_out,jmin_out:jmax_out))
                                           
           case default ! ---------------------------------------------------------------------------------------------
              call die('output_type not defined correctly for '//trim(output_names(out))//' in data write')
           end select
              
        end do


        !  Write the meshes
        ! =============================================
        ! Write the regular mesh
        err = dbmkoptlist(2, optlist)
        err = dbaddiopt(optlist, DBOPT_HI_OFFSET, hi_offset)
        err = dbaddiopt(optlist, DBOPT_LO_OFFSET, lo_offset)
        err = dbputqm(dbfile,"Mesh",4,'xc',2,'yc',2,'zc',2, &
             real(x(imin_out:imax_out+1),SP),real(y(jmin_out:jmax_out+1),SP),DB_F77NULL, &
             (/nxout+1,nyout+1/),2,DB_FLOAT,DB_COLLINEAR,optlist,ierr)
        err = dbputqm(dbfile,"UMesh",5,'xc',2,'yc',2,'zc',2, &
             real(xm(imin_out-1:imax_out+1),SP),real(y(jmin_out:jmax_out+1),SP),DB_F77NULL, &
             (/nxout+2,nyout+1/),2,DB_FLOAT,DB_COLLINEAR,optlist,ierr)
        err = dbputqm(dbfile,"VMesh",5,'xc',2,'yc',2,'zc',2, &
             real(x(imin_out:imax_out+1),SP),real(ym(jmin_out-1:jmax_out+1),SP),DB_F77NULL, &
             (/nxout+1,nyout+2/),2,DB_FLOAT,DB_COLLINEAR,optlist,ierr)
        err = dbputqm(dbfile,"WMesh",5,'xc',2,'yc',2,'zc',2, &
             real(x(imin_out:imax_out+1),SP),real(y(jmin_out:jmax_out+1),SP),DB_F77NULL, &
             (/nxout+1,nyout+1/),2,DB_FLOAT,DB_COLLINEAR,optlist,ierr)
        err = dbfreeoptlist(optlist)

        ! Spatial extents (written in master silo's multimesh)
        myspatial_extents=0.0_WP
        myspatial_extents(1:2,irank)=(/x(imin_out  ),y(jmin_out  )/)
        myspatial_extents(3:4,irank)=(/x(imax_out+1),y(jmax_out+1)/)

        Umyspatial_extents=0.0_WP
        Umyspatial_extents(1:2,irank)=(/xm(imin_out-1),y(jmin_out  )/)
        Umyspatial_extents(3:4,irank)=(/xm(imax_out  ),y(jmax_out+1)/)

        Vmyspatial_extents=0.0_WP
        Vmyspatial_extents(1:2,irank)=(/x(imin_out  ),ym(jmin_out-1)/)
        Vmyspatial_extents(3:4,irank)=(/x(imax_out+1),ym(jmax_out  )/)

        Wmyspatial_extents=0.0_WP
        Wmyspatial_extents(1:2,irank)=(/x(imin_out  ),y(jmin_out  )/)
        Wmyspatial_extents(3:4,irank)=(/x(imax_out+1),y(jmax_out+1)/)

        ! Close silo file
        ierr = dbclose(dbfile)
     end if
     call MPI_BARRIER(silo_comm,ierr)
  end do

  ! Communicate extents
  call MPI_ALLREDUCE( myspatial_extents, spatial_extents,4*nproc,MPI_REAL_WP,MPI_SUM,COMM,ierr)
  call MPI_ALLREDUCE(Umyspatial_extents,Uspatial_extents,4*nproc,MPI_REAL_WP,MPI_SUM,COMM,ierr)
  call MPI_ALLREDUCE(Vmyspatial_extents,Vspatial_extents,4*nproc,MPI_REAL_WP,MPI_SUM,COMM,ierr)
  call MPI_ALLREDUCE(Wmyspatial_extents,Wspatial_extents,4*nproc,MPI_REAL_WP,MPI_SUM,COMM,ierr)
  call MPI_ALLREDUCE(mydata_extents,data_extents,2*nproc*1*output_max,MPI_REAL_WP,MPI_SUM,COMM,ierr)

  ! Create the master silo database (.visit file, multimesh, multivars)
  ! ===============================================================================
  if (irank.eq.iroot) then
     ! Append .visit file
     iunit = iopen()
     inquire(file="Stat_Visit/arts.visit",exist=file_exists)
     if (file_exists) then
        open(iunit,file="Stat_Visit/arts.visit",form="formatted",status="old",position="append",action="write")
     else
        open(iunit,file="Stat_Visit/arts.visit",form="formatted",status="new",action="write")
     end if
     write(iunit,'(A)') folder(12:)//'/Visit.silo'
     close(iclose(iunit))
     
     ! Set length of names
     err = dbset2dstrlen(len(names))
     
     ! Create the master silo database
     err = dbcreate(folder//'/Visit.silo', len_trim(folder//'/Visit.silo'), DB_CLOBBER, DB_LOCAL, &
          "Silo database created with NGA", 30, DB_HDF5, dbfile)
     if(dbfile.eq.-1) call die('Could not create Silo file!')

     !  Write the multimeshes
     ! =======================
     do i=1,nproc
        write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/Mesh'
        lnames(i)=len_trim(names(i))
        types(i)=DB_QUAD_RECT
     end do
     err = dbmkoptlist(4,optlist)
     err = dbaddiopt(optlist, DBOPT_DTIME, Delta_t);
     err = dbaddiopt(optlist, DBOPT_CYCLE, nout_time);
     err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 4)
     err = dbadddopt(optlist, DBOPT_EXTENTS, spatial_extents)
     err = dbputmmesh(dbfile,  "Mesh", 4, nproc, names,lnames, types, optlist, ierr)
     err = dbfreeoptlist(optlist)

     do i=1,nproc
        write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/UMesh'
        lnames(i)=len_trim(names(i))
        types(i)=DB_QUAD_RECT
     end do
     err = dbmkoptlist(4,optlist)
     err = dbaddiopt(optlist, DBOPT_DTIME, Delta_t);
     err = dbaddiopt(optlist, DBOPT_CYCLE, nout_time);
     err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 4)
     err = dbadddopt(optlist, DBOPT_EXTENTS, Uspatial_extents)
     err = dbputmmesh(dbfile,  "UMesh", 5, nproc, names,lnames, types, optlist, ierr)
     err = dbfreeoptlist(optlist)

     do i=1,nproc
        write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/VMesh'
        lnames(i)=len_trim(names(i))
        types(i)=DB_QUAD_RECT
     end do
     err = dbmkoptlist(4,optlist)
     err = dbaddiopt(optlist, DBOPT_DTIME, Delta_t);
     err = dbaddiopt(optlist, DBOPT_CYCLE, nout_time);
     err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 4)
     err = dbadddopt(optlist, DBOPT_EXTENTS, Vspatial_extents)
     err = dbputmmesh(dbfile,  "VMesh", 5, nproc, names,lnames, types, optlist, ierr)
     err = dbfreeoptlist(optlist)

     do i=1,nproc
        write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/WMesh'
        lnames(i)=len_trim(names(i))
        types(i)=DB_QUAD_RECT
     end do
     err = dbmkoptlist(4,optlist)
     err = dbaddiopt(optlist, DBOPT_DTIME, Delta_t);
     err = dbaddiopt(optlist, DBOPT_CYCLE, nout_time);
     err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 4)
     err = dbadddopt(optlist, DBOPT_EXTENTS, Wspatial_extents)
     err = dbputmmesh(dbfile,  "WMesh", 5, nproc, names,lnames, types, optlist, ierr)
     err = dbfreeoptlist(optlist)

     !  Write the multivars
     ! =======================
     do out=1,output_n
        select case(trim(output_types(out)))

        case ('scalar','scalar_Ucell','scalar_Vcell','scalar_Wcell')
           do i=1,nproc
              write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/'//trim(output_names(out))
              lnames(i)=len_trim(names(i))
              types(i)=DB_QUADVAR
           end do
           err = dbmkoptlist(2, optlist)
           err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 2)
           err = dbadddopt(optlist, DBOPT_EXTENTS, data_extents(:,:,1,out))
           err = dbputmvar(dbfile,output_names(out),len_trim(output_names(out)),nproc,names,lnames,types,optlist,ierr)
           err = dbfreeoptlist(optlist)


        case default
           call die('output_type not defined correctly for '//trim(output_names(out))//'in multivar write')
        end select

     end do

     ! Close database
     ierr = dbclose(dbfile)
  end if
  
  call timing_stop('stat2dhit-visit')
  
  return
end subroutine dump_stat2dhit_visit_data
