module dump_combanal
  use parallel
  use precision
  use geometry
  use partition
  use masks
  use fileio
  use unstruct
  use data
  use string
  use lpt
  use combustion
  use sgsmodel
  use interpolate
  use memory
  use pressure
  use multiphase
  use soot
  use onestep
  implicit none
  
end module dump_combanal


subroutine dump_combanal_init
  use dump_combanal
  implicit none
  
  ! Create directory
  if (irank.eq.iroot) call CREATE_FOLDER("combanal")
  
  ! Kill the code
  call die('Done with combanal.')
  
  return
end subroutine dump_combanal_init


subroutine dump_combanalyse
  use dump_combanal
  implicit none
  
  if (trim(simu_type).eq."") then
     call dump_combanalyse_jet
  else if (trim(simu_type).eq."hit") then
     call dump_combanalyse_hit
  end if
  
  return
end subroutine dump_combanalyse


subroutine dump_combanalyse_jet
  use dump_combanal
  use onestep
  implicit none
  
  integer  :: nXpos
  integer  :: i,j,k,n,iunit,n_1,n_2
  integer  :: nZbin
  integer  :: nSzbin
  integer  :: nXbin
  real(WP) :: Xwidth
  real(WP) :: Zwidth
  real(WP) :: Sz_min,Sz_max
  real(WP), dimension(:),   allocatable :: Xpos
  real(WP), dimension(:),   allocatable :: counter
  real(WP), dimension(:,:), allocatable :: CHImean
  real(WP), dimension(:,:), allocatable :: Szmean
  real(WP), dimension(:,:), allocatable :: Zpdf
  real(WP), dimension(:,:), allocatable :: Szpdf
  real(WP), dimension(:,:), allocatable :: Sz_Zpdf
  character(len=str_medium) :: file
  character(len=str_medium) :: buffer
  integer  :: nfft
  real(WP), dimension(:,:), allocatable :: fftloc
  real(WP), dimension(:,:), allocatable :: spectU
  real(WP), dimension(:,:), allocatable :: spectV
  real(WP), dimension(:,:), allocatable :: spectW
  real(WP), dimension(:), allocatable :: Xloc,ER,CR,CR_p,CR_d
  real(WP) :: area
  
  ! Assume one proc for now
  if (nproc.gt.1) call die('Combanal assumes 1 proc.')
  
  ! Compute Zmix, chi, ksi
  if (trim(chemistry).eq.'one-step') then
     ! Compute mixture fraction => Mix_Frac
     call onestep_get_mixfrac
     where(Mix_Frac.lt.0.0_WP)
        Mix_Frac=0.0_WP
     end where
     where(Mix_Frac.gt.1.0_WP)
        Mix_Frac=1.0_WP
     end where
     ! Compute CHI              => CHI
     call onestep_get_chi
     where(CHI.lt.0.0_WP)
        CHI=0.0_WP
     end where
     ! Compute ksi              => ksi
     call onestep_get_flame_index
  end if
  
  ! Compute Sz
  if (use_lpt) then
     call lpt_step
     Sz_min=minval(srcP/dt)
     Sz_max=maxval(srcP/dt)
  end if
  
  ! Read parameters
  call parser_read('Number of Z bins',nZbin,200)
  call parser_read('Number of Sz bins',nSzbin,200)
  call parser_getsize('X combanal positions',nXbin)
  allocate(Xpos(nXbin))
  call parser_read('X combanal positions',Xpos)
  call parser_read('X bin width',Xwidth)
  call parser_read('Z bin width',Zwidth)
  
  ! Allocate
  allocate(counter(nZbin))
  counter=0
  allocate(CHImean(2,nZbin))
  CHImean=0.0_WP
  allocate(Szmean(2,nZbin))
  Szmean=0.0_WP
  allocate(Zpdf(2,nZbin))
  Zpdf=0.0_WP
  allocate(Szpdf(2,nSzbin))
  Szpdf=0.0_WP
  allocate(Sz_Zpdf(nZbin,nSzbin))
  Sz_Zpdf=0.0_WP
  
  ! For each X position
  do nXpos=1,nXbin
     
     ! Prepare output name
     write(buffer,'(ES12.3)') Xpos(nXpos)
     
     ! Compute chi(Z) -----------------------------------------------------------
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (mask(i,j).eq.0) then
                 ! Only if at the right position
                 if (abs(xm(i)-Xpos(nXpos)).gt.0.5_WP*Xwidth) cycle
                 ! Find the bin
                 n=floor((Mix_Frac(i,j,k)-0.0_WP)/(1.0_WP-0.0_WP)*(nZbin-1))+1
                 !n=min(max(n,1),nZbin)
                 ! Increment counter and mean
                 CHImean(2,n)=CHImean(2,n)+CHI(i,j,k)
                 counter(n)=counter(n)+1.0_WP
              end if
           end do
        end do
     end do
     do n=1,nZbin-1
        CHImean(1,n) = real((n-1),WP)/real((nZbin-1),WP)
        CHImean(2,n) = CHImean(2,n)/counter(n)
     end do
     
     ! Dump it to a file
     file='combanal/CHIofZ'
     file=trim(adjustl(file))//'_'//trim(adjustl(buffer))
     iunit=iopen()
     open(iunit,file=file,form='formatted')
     do n=1,nZbin-1
        if (counter(n).gt.10.0_WP) write(iunit,'(ES12.3,x,ES12.3)') CHImean(1,n),CHImean(2,n)
     end do
     close(iclose(iunit))
     
     ! Compute Sz(Z) -----------------------------------------------------------
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (mask(i,j).eq.0) then
                 ! Only if at the right position
                 if (abs(xm(i)-Xpos(nXpos)).gt.0.5_WP*Xwidth) cycle
                 ! Find the bin
                 n=floor((Mix_Frac(i,j,k)-0.0_WP)/(1.0_WP-0.0_WP)*(nZbin-1))+1
                 !n=min(max(n,1),nZbin)
                 ! Increment counter and mean
                 Szmean(2,n)=Szmean(2,n)+srcP(i,j,k)/dt
                 counter(n)=counter(n)+1.0_WP
              end if
           end do
        end do
     end do
     do n=1,nZbin-1
        Szmean(1,n) = real((n-1),WP)/real((nZbin-1),WP)
        Szmean(2,n) = Szmean(2,n)/counter(n)
     end do
     
     ! Dump it to a file
     file='combanal/SzofZ'
     file=trim(adjustl(file))//'_'//trim(adjustl(buffer))
     iunit=iopen()
     open(iunit,file=file,form='formatted')
     do n=1,nZbin-1
        if (counter(n).gt.10.0_WP) write(iunit,'(ES12.3,x,ES12.3)') Szmean(1,n),Szmean(2,n)
     end do
     close(iclose(iunit))
     
     ! Compute pdf(Z) -----------------------------------------------------------
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (mask(i,j).eq.0) then
                 ! Only if at the right position
                 if (abs(xm(i)-Xpos(nXpos)).gt.0.5_WP*Xwidth) cycle
                 ! Find the bin
                 n=floor((Mix_Frac(i,j,k)-0.0_WP)/(1.0_WP-0.0_WP)*(nZbin-1))+1
                 !n=min(max(n,1),nZbin)
                 ! Increment counter and mean
                 Zpdf(2,n)=Zpdf(2,n)+1.0_WP
              end if
           end do
        end do
     end do
     do n=1,nZbin-1
        Zpdf(1,n) = real((n-1),WP)/real((nZbin-1),WP)
        Zpdf(2,n) = Zpdf(2,n)/sum(Zpdf(2,:))
     end do
     
     ! Dump it to a file
     file='combanal/Z'
     file=trim(adjustl(file))//'_'//trim(adjustl(buffer))
     iunit=iopen()
     open(iunit,file=file,form='formatted')
     do n=1,nZbin-1
        write(iunit,'(ES12.3,x,ES12.3)') Zpdf(1,n),Zpdf(2,n)
     end do
     close(iclose(iunit))
     
     ! Compute pdf(Sz) -----------------------------------------------------------
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (mask(i,j).eq.0) then
                 ! Only if at the right position
                 if (abs(xm(i)-Xpos(nXpos)).gt.0.5_WP*Xwidth) cycle
                 ! Find the bin
                 n=floor((srcP(i,j,k)/dt-Sz_min)/(Sz_max-Sz_min)*(nSzbin-1))+1
                 !n=min(max(n,1),nSzbin)
                 ! Increment counter and mean
                 Szpdf(2,n)=Szpdf(2,n)+1.0_WP
              end if
           end do
        end do
     end do
     do n=1,nSzbin-1
        Szpdf(1,n) = Sz_min+(Sz_max-Sz_min)*real((n-1),WP)/real((nSzbin-1),WP)
        Szpdf(2,n) = Szpdf(2,n)/sum(Szpdf(2,:))
     end do
     
     ! Dump it to a file
     file='combanal/Sz'
     file=trim(adjustl(file))//'_'//trim(adjustl(buffer))
     iunit=iopen()
     open(iunit,file=file,form='formatted')
     do n=1,nSzbin-1
        write(iunit,'(ES12.3,x,ES12.3)') Szpdf(1,n),Szpdf(2,n)
     end do
     close(iclose(iunit))
     
     ! Compute pdf(Z,Sz) -----------------------------------------------------------
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (mask(i,j).eq.0) then
                 ! Only if at the right position
                 if (abs(xm(i)-Xpos(nXpos)).gt.0.5_WP*Xwidth) cycle
                 ! Find the bin
                 n_1=floor((Mix_Frac(i,j,k)-0.0_WP)/(1.0_WP-0.0_WP)*(nZbin-1))+1
                 n_2=floor((srcP(i,j,k)/dt-Sz_min)/(Sz_max-Sz_min)*(nSzbin-1))+1
                 !n_1=min(max(n_1,1),nZbin)
                 !n_2=min(max(n_2,1),nSzbin)
                 ! Increment counter and mean
                 Sz_Zpdf(n_1,n_2)=Sz_Zpdf(n_1,n_2)+1.0_WP
              end if
           end do
        end do
     end do
     do n_1=1,nZbin-1
        do n_2=1,nSzbin-1
           Sz_Zpdf(n_1,n_2) = Sz_Zpdf(n_1,n_2)/sum(Sz_Zpdf)
        end do
     end do
     
     ! Dump it to a file
     file='combanal/Sz_Z'
     file=trim(adjustl(file))//'_'//trim(adjustl(buffer))
     iunit=iopen()
     open(iunit,file=file,form='formatted')
     do n_1=1,nZbin-1
        !do n_2=1,nSzbin-1
           write(iunit,'(100000(ES12.3,x))') Sz_Zpdf(n_1,:)
        !end do
     end do
     close(iclose(iunit))
     
     ! Compute T(Z) --------------------------------------------------------------
     !file='combanal/TofZ'
     !file=trim(adjustl(file))//'_'//trim(adjustl(buffer))
     !iunit=iopen()
     !open(iunit,file=file,form='formatted')
     !do k=kmin_,kmax_
     !   do j=jmin_,jmax_
     !      do i=imin_,imax_
     !         if (mask(i,j).eq.0) then
     !            ! Only if at the right position
     !            if (abs(xm(i)-Xpos(nXpos)).gt.0.5_WP*Xwidth) cycle
     !            ! Dump T(Z)
     !            write(iunit,'(ES12.3,x,ES12.3)') Mix_Frac(i,j,k),SC(i,j,k,isc_T)
     !         end if
     !      end do
     !   end do
     !end do
     !close(iclose(iunit))
     
     ! Compute T_st(CHI_st) --------------------------------------------------------------
     file='combanal/TofCHI_st'
     file=trim(adjustl(file))//'_'//trim(adjustl(buffer))
     iunit=iopen()
     open(iunit,file=file,form='formatted')
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (mask(i,j).eq.0) then
                 ! Only if at the right position
                 if (abs(xm(i)-Xpos(nXpos)).gt.0.5_WP*Xwidth) cycle
                 ! Only if at stoichiometry
                 if (abs(Mix_Frac(i,j,k)-Zst).gt.0.5_WP*Zwidth) cycle
                 ! Dump T_st(CHI_st)
                 write(iunit,'(ES12.3,x,ES12.3)') CHI(i,j,k),SC(i,j,k,isc_T)
              end if
           end do
        end do
     end do
     close(iclose(iunit))
     
     ! Compute T(Z,CHI) --------------------------------------------------------------------
     file='combanal/TofCHIofZ'
     file=trim(adjustl(file))//'_'//trim(adjustl(buffer))
     iunit=iopen()
     open(iunit,file=file,form='formatted')
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (mask(i,j).eq.0) then
                 ! Only if at the right position
                 if (abs(xm(i)-Xpos(nXpos)).gt.0.5_WP*Xwidth) cycle
                 ! Dump T(Z,CHI)
                 write(iunit,'(ES12.3,x,ES12.3,x,ES12.3)') Mix_Frac(i,j,k),CHI(i,j,k),SC(i,j,k,isc_T)
              end if
           end do
        end do
     end do
     close(iclose(iunit))
     
  end do
  
  ! Compute 1D spectra in z
  call parser_getsize('FFT probe locations',nfft)
  if (mod(nfft,2).ne.0) then
     call die('Combanal: incorrect number of coordinates for fft probes locations')
  else
     nfft = nfft/2
  end if
  allocate(fftloc(2,nfft))
  call parser_read('FFT probe locations',fftloc)
  
  do n=1,nfft
     
     ! Prepare output name
     write(buffer,'(i1)') n
     
     ! Compute (i,j)
     i = imin_
     do while(xm(i+1).le.fftloc(1,n)) 
        i = i+1
     end do
     j = jmin_
     do while(ym(j+1).le.fftloc(2,n)) 
        j = j+1
     end do
     
     ! Compute fft
     allocate(spectU(2,nz+1));spectU=0.0_WP
     allocate(spectV(2,nz+1));spectV=0.0_WP
     allocate(spectW(2,nz+1));spectW=0.0_WP
     call get_spectrum_z(nz,zL*ym(j),Ui(i,j,:),spectU)
     call get_spectrum_z(nz,zL*ym(j),Vi(i,j,:),spectV)
     call get_spectrum_z(nz,zL*ym(j),Wi(i,j,:),spectW)
     
     ! Output to a file
     file='combanal/spectrum'
     file=trim(adjustl(file))//'_'//trim(adjustl(buffer))
     iunit=iopen()
     open(iunit,file=file,form='formatted')
     do k=1,nz+1
        write(iunit,'(ES12.3,x,ES12.3,x,ES12.3,x,ES12.3)') spectU(1,k),spectU(2,k),spectV(2,k),spectW(2,k)
     end do
     close(iclose(iunit))
     
  end do
  
  ! ----------------- Compute fuel_evap_rate(x), fuel_cons_rate(x) -----------------
  allocate(Xloc(imin:imax))
  allocate(ER(imin:imax))
  allocate(CR(imin:imax))
  allocate(CR_p(imin:imax))
  allocate(CR_d(imin:imax))
  
  ! Compute source terms
  srcSC=0.0_WP
  call scalar_prestep
  
  do i=imin,imax
     ! X location
     Xloc(i)=xm(i)
     ! Evaporation rate
     ER(i)=0.0_WP
     area=0.0_WP
     do j=jmin,jmax
        do k=kmin,kmax
           ER(i)=ER(i)+dy(j)*dz*abs(ym(j))*srcP(i,j,k)/dt
           area=area+dy(j)*dz*abs(ym(j))
        end do
     end do
     ER(i)=ER(i)/area
     ! Combustion rate
     CR(i)=0.0_WP
     area=0.0_WP
     do j=jmin,jmax
        do k=kmin,kmax
           CR(i)=CR(i)+dy(j)*dz*abs(ym(j))*srcSC(i,j,k,isc_Yf)/dt
           area=area+dy(j)*dz*abs(ym(j))
        end do
     end do
     CR(i)=CR(i)/area
     ! Combustion rate
     CR_p(i)=0.0_WP
     CR_d(i)=0.0_WP
     area=0.0_WP
     do j=jmin,jmax
        do k=kmin,kmax
           if (abs(ksi(i,j,k)-1.0_WP).lt.1.0e-2_WP) then        
              CR_p(i)=CR_p(i)+dy(j)*dz*abs(ym(j))*srcSC(i,j,k,isc_Yf)/dt
           else if (abs(ksi(i,j,k)).lt.1.0e-2_WP) then
              CR_d(i)=CR_d(i)+dy(j)*dz*abs(ym(j))*srcSC(i,j,k,isc_Yf)/dt
           end if
           area=area+dy(j)*dz*abs(ym(j))
        end do
     end do
     CR_p(i)=CR_p(i)/area
     CR_d(i)=CR_d(i)/area
  end do
  
  ! Output to a file
  file='combanal/ER_CR'
  iunit=iopen()
  open(iunit,file=file,form='formatted')
  do i=imin,imax
     write(iunit,'(ES12.3,x,ES12.3,x,ES12.3,x,ES12.3,x,ES12.3)') Xloc(i),ER(i),CR(i),CR_p(i),CR_d(i)
  end do
  close(iclose(iunit))
  
  return
end subroutine dump_combanalyse_jet


subroutine get_spectrum_z(n,L,A,S)
  use precision
  implicit none
  include 'fftw3.f'
  
  ! In/out arguments
  integer,                    intent(in)  :: n
  real(WP),                   intent(in)  :: L
  real(WP), dimension(n),     intent(in)  :: A
  real(WP), dimension(2,n+1), intent(out) :: S
  ! FFTW variables
  complex(WP), dimension(:), allocatable :: in
  complex(WP), dimension(:), allocatable :: out
  integer(KIND=8) :: plan
  ! Local work variables
  real(WP), dimension(:), allocatable :: B
  real(WP) :: pi,dk,kc,eps,kk,kk_norm
  integer  :: i,ik
  
  ! Create the plan
  allocate(in (n))
  allocate(out(n))
  call dfftw_plan_dft_1d(plan,n,in,out,FFTW_FORWARD,FFTW_ESTIMATE)
  
  ! FFT
  allocate(B(n))
  in=A
  call dfftw_execute(plan)
  do i=1,n
     out(i) = out(i)/real(n,WP)
     B(i)   = sqrt(real(out(i)*conjg(out(i))))
  end do
  
  ! Compute the spectrum
  pi  = acos(-1.0_WP)
  dk  = 2.0_WP*pi/L
  kc  = pi*n/L
  eps = kc/1000000.0_WP
  do i=1,n
     ! Wavenumbers
     kk=real(i-1,WP)*dk
     if (i.gt.(n/2+1)) kk=-real(n-i+1,WP)*dk
     kk_norm=sqrt(kk**2)
     ! Spectrum
     ik=1+idint(kk_norm/dk+0.5_WP)
     if ((kk_norm.gt.eps).and.(kk_norm.le.kc)) then
        S(2,ik)=S(2,ik)+0.5_WP*(B(i)**2)/dk
     end if
  end do
  do ik=1,n+1
     S(1,ik)=dk*(ik-1)
  end do
  
  ! Clean up
  call dfftw_destroy_plan(plan)
  deallocate(B)
  deallocate(out)
  deallocate(in)
  
  return
end subroutine get_spectrum_z


subroutine dump_combanalyse_hit
  use dump_combanal
  implicit none
  
  integer  :: i,j,k,n,iunit,n_1,n_2
  integer  :: nZbin
  integer  :: nSzbin
  real(WP) :: Sz_min,Sz_max
  real(WP), dimension(:),   allocatable :: counter
  real(WP), dimension(:,:), allocatable :: CHImean
  real(WP), dimension(:,:), allocatable :: Szmean
  real(WP), dimension(:,:), allocatable :: Zpdf
  real(WP), dimension(:,:), allocatable :: Szpdf
  real(WP), dimension(:,:), allocatable :: Sz_Zpdf
  character(len=str_medium) :: file
  character(len=str_medium) :: buffer
  
  ! Assume one proc for now
  if (nproc.gt.1) call die('Combanal assumes 1 proc.')
  
  ! Compute Zmix, chi, ksi
  if (trim(chemistry).eq.'one-step') then
     ! Compute mixture fraction => Mix_Frac
     call onestep_get_mixfrac
     where(Mix_Frac.lt.0.0_WP)
        Mix_Frac=0.0_WP
     end where
     where(Mix_Frac.gt.1.0_WP)
        Mix_Frac=1.0_WP
     end where
     ! Compute CHI              => CHI
     call onestep_get_chi
     where(CHI.lt.0.0_WP)
        CHI=0.0_WP
     end where
  end if
  
  ! Compute Sz
  if (use_lpt) then
     call lpt_step
     Sz_min=minval(srcP/dt)
     Sz_max=maxval(srcP/dt)
  end if
  
  ! Read parameters
  call parser_read('Number of Z bins',nZbin,200)
  call parser_read('Number of Sz bins',nSzbin,200)
  
  ! Allocate
  allocate(counter(nZbin))
  counter=0
  allocate(CHImean(2,nZbin))
  CHImean=0.0_WP
  allocate(Szmean(2,nZbin))
  Szmean=0.0_WP
  allocate(Zpdf(2,nZbin))
  Zpdf=0.0_WP
  allocate(Szpdf(2,nSzbin))
  Szpdf=0.0_WP
  allocate(Sz_Zpdf(nZbin,nSzbin))
  Sz_Zpdf=0.0_WP
  
  ! Prepare output name
  write(buffer,'(ES12.3)') time
  
  ! Compute chi(Z) -----------------------------------------------------------
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Find the bin
           n=floor((Mix_Frac(i,j,k)-0.0_WP)/(1.0_WP-0.0_WP)*(nZbin-1))+1
           n=min(max(n,1),nZbin)
           ! Increment counter and mean
           CHImean(2,n)=CHImean(2,n)+CHI(i,j,k)
           counter(n)=counter(n)+1.0_WP
        end do
     end do
  end do
  do n=1,nZbin-1
     CHImean(1,n) = real((n-1),WP)/real((nZbin-1),WP)
     CHImean(2,n) = CHImean(2,n)/counter(n)
  end do
  
  ! Dump it to a file
  file='combanal/CHIofZ'
  file=trim(adjustl(file))//'_'//trim(adjustl(buffer))
  iunit=iopen()
  open(iunit,file=file,form='formatted')
  do n=1,nZbin-1
     if (counter(n).gt.10.0_WP) write(iunit,'(ES12.3,x,ES12.3)') CHImean(1,n),CHImean(2,n)
  end do
  close(iclose(iunit))
  
  ! Compute Sz(Z) -----------------------------------------------------------
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Find the bin
           n=floor((Mix_Frac(i,j,k)-0.0_WP)/(1.0_WP-0.0_WP)*(nZbin-1))+1
           n=min(max(n,1),nZbin)
           ! Increment counter and mean
           Szmean(2,n)=Szmean(2,n)+srcP(i,j,k)/dt
           counter(n)=counter(n)+1.0_WP
        end do
     end do
  end do
  do n=1,nZbin-1
     Szmean(1,n) = real((n-1),WP)/real((nZbin-1),WP)
     Szmean(2,n) = Szmean(2,n)/counter(n)
  end do
  
  ! Dump it to a file
  file='combanal/SzofZ'
  file=trim(adjustl(file))//'_'//trim(adjustl(buffer))
  iunit=iopen()
  open(iunit,file=file,form='formatted')
  do n=1,nZbin-1
     if (counter(n).gt.10.0_WP) write(iunit,'(ES12.3,x,ES12.3)') Szmean(1,n),Szmean(2,n)
  end do
  close(iclose(iunit))
  
  ! Compute pdf(Z) -----------------------------------------------------------
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Find the bin
           n=floor((Mix_Frac(i,j,k)-0.0_WP)/(1.0_WP-0.0_WP)*(nZbin-1))+1
           n=min(max(n,1),nZbin)
           ! Increment counter and mean
           Zpdf(2,n)=Zpdf(2,n)+1.0_WP
        end do
     end do
  end do
  do n=1,nZbin-1
     Zpdf(1,n) = real((n-1),WP)/real((nZbin-1),WP)
     Zpdf(2,n) = Zpdf(2,n)/sum(Zpdf(2,:))
  end do
  
  ! Dump it to a file
  file='combanal/Z'
  file=trim(adjustl(file))//'_'//trim(adjustl(buffer))
  iunit=iopen()
  open(iunit,file=file,form='formatted')
  do n=1,nZbin-1
     write(iunit,'(ES12.3,x,ES12.3)') Zpdf(1,n),Zpdf(2,n)
  end do
  close(iclose(iunit))
     
  ! Compute pdf(Sz) -----------------------------------------------------------
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Find the bin
           n=floor((srcP(i,j,k)/dt-Sz_min)/(Sz_max-Sz_min)*(nSzbin-1))+1
           n=min(max(n,1),nSzbin)
           ! Increment counter and mean
           Szpdf(2,n)=Szpdf(2,n)+1.0_WP
        end do
     end do
  end do
  do n=1,nSzbin-1
     Szpdf(1,n) = Sz_min+(Sz_max-Sz_min)*real((n-1),WP)/real((nSzbin-1),WP)
     Szpdf(2,n) = Szpdf(2,n)/sum(Szpdf(2,:))
  end do
  
  ! Dump it to a file
  file='combanal/Sz'
  file=trim(adjustl(file))//'_'//trim(adjustl(buffer))
  iunit=iopen()
  open(iunit,file=file,form='formatted')
  do n=1,nSzbin-1
     write(iunit,'(ES12.3,x,ES12.3)') Szpdf(1,n),Szpdf(2,n)
  end do
  close(iclose(iunit))
  
  ! Compute pdf(Z,Sz) -----------------------------------------------------------
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Find the bin
           n_1=floor((Mix_Frac(i,j,k)-0.0_WP)/(1.0_WP-0.0_WP)*(nZbin-1))+1
           n_2=floor((srcP(i,j,k)/dt-Sz_min)/(Sz_max-Sz_min)*(nSzbin-1))+1
           n_1=min(max(n_1,1),nZbin)
           n_2=min(max(n_2,1),nSzbin)
           ! Increment counter and mean
           Sz_Zpdf(n_1,n_2)=Sz_Zpdf(n_1,n_2)+1.0_WP
        end do
     end do
  end do
  do n_1=1,nZbin-1
     do n_2=1,nSzbin-1
        Sz_Zpdf(n_1,n_2) = Sz_Zpdf(n_1,n_2)/sum(Sz_Zpdf)
     end do
  end do
  
  ! Dump it to a file
  file='combanal/Sz_Z'
  file=trim(adjustl(file))//'_'//trim(adjustl(buffer))
  iunit=iopen()
  open(iunit,file=file,form='formatted')
  do n_1=1,nZbin-1
     !do n_2=1,nSzbin-1
     write(iunit,'(100000(ES12.3,x))') Sz_Zpdf(n_1,:)
     !end do
  end do
  close(iclose(iunit))
  
  return
end subroutine dump_combanalyse_hit
