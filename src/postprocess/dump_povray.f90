module dump_povray
  use parallel
  use precision
  use data
  use geometry
  implicit none
  
  ! POVRAY grid
  integer, dimension(3) :: pov_Msize
  integer :: pov_nx,pov_ny,pov_nz,pov_n
  real(WP), dimension(3) :: pov_Xstart,pov_Xdft,pov_Lbox,pov_Ldft
  logical :: pov_Xstart_isdef,pov_Lbox_isdef
  real(WP) :: pov_Lx,pov_Ly,pov_Lz
  real(WP) :: pov_dx,pov_dy,pov_dz
  real(WP), dimension(:), allocatable :: pov_x,pov_xm
  real(WP), dimension(:), allocatable :: pov_y,pov_ym
  real(WP), dimension(:), allocatable :: pov_z,pov_zm
  
  ! POVRAY data
  integer :: pov_nvar
  real(SP), dimension(:), allocatable :: pov_data
  
  ! POVRAY files
  integer :: nfile
  integer :: fileview
  
  ! POVRAY unstructured mapping
  integer, dimension(:,:), allocatable :: map
  integer, dimension(:,:), allocatable :: map_interp
  real(WP), dimension(:,:,:,:), allocatable :: coeff_interp
  integer :: nmap
  integer, dimension(:), allocatable :: pov_grid
  
end module dump_povray


! ==================================================== !
! Dump 3D binary povray density files - Initialization !
! ==================================================== !
subroutine dump_povray_init
  use dump_povray
  use parser
  use config
  use math
  use combustion
  implicit none
  
  integer :: i,j,k,test_n,ierr
  real(WP) :: my_x,my_y,my_z
  integer, dimension(:), allocatable :: blocklength
  
  ! Check what quantity to raytrace
  pov_nvar = 0
  if (use_multiphase) then
     ! Raytrace the liquid
     pov_nvar = 1
  else if (combust) then
     ! Can render volumetric flames
     ! Need to decide what species we need
     !call die('dump_povray_init: flame rendering not implemented yet.')
     pov_nvar = 1
  else if (isc_ZMIX.ne.0) then
     ! Assume smoke rendering => render ZMIX
     pov_nvar = 1
  else
     ! Nothing we can raytrace
     call die('dump_povray_init: nothing can be raytraced.')
  end if
  
  ! Read additional info
  call parser_read('Povray mesh size',pov_Msize)
  pov_nx=pov_Msize(1);pov_ny=pov_Msize(2);pov_nz=pov_Msize(3)
  pov_n = pov_nx*pov_ny*pov_nz
  
  ! Allocate the mesh
  allocate(pov_x(1:pov_nx+1));allocate(pov_xm(1:pov_nx))
  allocate(pov_y(1:pov_ny+1));allocate(pov_ym(1:pov_ny))
  allocate(pov_z(1:pov_nz+1));allocate(pov_zm(1:pov_nz))
  
  ! Generate box size
  if (icyl.eq.0) then
     pov_Ldft(1)=xL;pov_Ldft(2)=yL;pov_Ldft(3)=zL
     pov_Xdft(1)=x(imin);pov_Xdft(2)=y(jmin);pov_Xdft(3)=z(kmin)
  else
     pov_Ldft(1)=xL;pov_Ldft(2)=2.0_WP*yL;pov_Ldft(3)=2.0_WP*yL
     pov_Xdft(1)=x(imin);pov_Xdft(2)=-yL;pov_Xdft(3)=-yL
  end if
  call parser_is_defined('Povray box start',pov_Xstart_isdef)
  if (pov_Xstart_isdef) then
     call parser_read('Povray box start',pov_Xstart)
  else
     pov_Xstart=pov_Xdft
  end if
  call parser_is_defined('Povray box size',pov_Lbox_isdef)
  if (pov_Lbox_isdef) then
     call parser_read('Povray box size',pov_Lbox)
  else
     pov_Lbox=pov_Ldft
  end if
  pov_Lx=pov_Lbox(1);pov_Ly=pov_Lbox(2);pov_Lz=pov_Lbox(3)
  pov_x(1)=pov_Xstart(1);pov_y(1)=pov_Xstart(2);pov_z(1)=pov_Xstart(3)
  
  ! Generate mesh
  pov_dx=pov_Lx/real(pov_nx,WP)
  pov_dy=pov_Ly/real(pov_ny,WP)
  pov_dz=pov_Lz/real(pov_nz,WP)
  do i=2,pov_nx+1
     pov_x(i)=pov_x(1)+pov_dx*real(i-1,WP)
  end do
  do j=2,pov_ny+1
     pov_y(j)=pov_y(1)+pov_dy*real(j-1,WP)
  end do
  do k=2,pov_nz+1
     pov_z(k)=pov_z(1)+pov_dz*real(k-1,WP)
  end do
  do i=1,pov_nx
     pov_xm(i)=0.5_WP*(pov_x(i)+pov_x(i+1))
  end do
  do j=1,pov_ny
     pov_ym(j)=0.5_WP*(pov_y(j)+pov_y(j+1))
  end do
  do k=1,pov_nz
     pov_zm(k)=0.5_WP*(pov_z(k)+pov_z(k+1))
  end do
  
  ! Create directory and intialize file counter
  if (irank.eq.iroot) call CREATE_FOLDER("povray")
  nfile = 0
  
  ! Prepare the unstructured partitioning of the povray mesh - allocation
  nmap = 0
  if (icyl.eq.0) then
     do k=1,pov_nz
        do j=1,pov_ny
           do i=1,pov_nx
              ! Test if inside the partition
              if ( pov_xm(i).ge.x(imin_) .and. pov_xm(i).lt.x(imax_+1) .and. &
                   pov_ym(j).ge.y(jmin_) .and. pov_ym(j).lt.y(jmax_+1) .and. &
                   pov_zm(k).ge.z(kmin_) .and. pov_zm(k).lt.z(kmax_+1) ) nmap = nmap + 1
              ! Take outside points on root process for now
              if ((pov_xm(i).lt.x(imin) .or. pov_xm(i).ge.x(imax+1) .or. &
                   pov_ym(j).lt.y(jmin) .or. pov_ym(j).ge.y(jmax+1) .or. &
                   pov_zm(k).lt.z(kmin) .or. pov_zm(k).ge.z(kmax+1)) .and. irank.eq.iroot) nmap = nmap + 1
           end do
        end do
     end do
  else
     do k=1,pov_nz
        do j=1,pov_ny
           do i=1,pov_nx
              ! Generate position in cylindrical coordinates
              my_x = pov_xm(i)
              my_y = sqrt(pov_ym(j)**2+pov_zm(k)**2)
              my_z = atan(abs(pov_zm(k))/(abs(pov_ym(j))+tiny(pov_ym(j))))
              if      (pov_ym(j)>0.0_WP .and. pov_zm(k)>=0.0_WP) then
                 my_z = my_z
              else if (pov_ym(j)<0.0_WP .and. pov_zm(k)>=0.0_WP) then
                 my_z = Pi - my_z
              else if (pov_ym(j)<0.0_WP .and. pov_zm(k)< 0.0_WP) then
                 my_z = Pi + my_z
              else
                 my_z = twoPi - my_z
              end if
              if (my_y.lt.0.0_WP) then
                 my_y=-my_y
                 my_z=my_z+Pi
              end if
              my_z=modulo(my_z,zL)
              ! Test if inside the partition
              if ( my_x.ge.x(imin_) .and. my_x.lt.x(imax_+1) .and. &
                   my_y.ge.y(jmin_) .and. my_y.lt.y(jmax_+1) .and. &
                   my_z.ge.z(kmin_) .and. my_z.lt.z(kmax_+1) ) nmap = nmap + 1
              ! Take outside points on root process for now
              if ((my_x.lt.x(imin) .or. my_x.ge.x(imax+1) .or. &
                   my_y.lt.y(jmin) .or. my_y.ge.y(jmax+1) .or. &
                   my_z.lt.z(kmin) .or. my_z.ge.z(kmax+1)) .and. irank.eq.iroot) nmap = nmap + 1
           end do
        end do
     end do
  end if
  call parallel_sum(nmap,test_n)
  if (test_n.ne.pov_n) then
     call die('dump_povray_init: error constructing the povray mesh')
  end if
  allocate(map(nmap,3))
  allocate(pov_grid(nmap))
  allocate(pov_data(nmap))
  
  ! Prepare the unstructured partitioning of the povray mesh - mapping
  nmap = 0
  if (icyl.eq.0) then
     do k=1,pov_nz
        do j=1,pov_ny
           do i=1,pov_nx
              ! Test if inside the partition
              if ( pov_xm(i).ge.x(imin_) .and. pov_xm(i).lt.x(imax_+1) .and. &
                   pov_ym(j).ge.y(jmin_) .and. pov_ym(j).lt.y(jmax_+1) .and. &
                   pov_zm(k).ge.z(kmin_) .and. pov_zm(k).lt.z(kmax_+1) ) then
                 nmap = nmap + 1
                 map(nmap,1)=i;map(nmap,2)=j;map(nmap,3)=k
                 pov_grid(nmap)=i+(j-1)*pov_nx+(k-1)*pov_nx*pov_ny
              end if
              ! Take outside points on root process for now
              if ((pov_xm(i).lt.x(imin) .or. pov_xm(i).ge.x(imax+1) .or. &
                   pov_ym(j).lt.y(jmin) .or. pov_ym(j).ge.y(jmax+1) .or. &
                   pov_zm(k).lt.z(kmin) .or. pov_zm(k).ge.z(kmax+1)) .and. irank.eq.iroot) then
                 nmap = nmap + 1
                 map(nmap,1)=i;map(nmap,2)=j;map(nmap,3)=k
                 pov_grid(nmap)=i+(j-1)*pov_nx+(k-1)*pov_nx*pov_ny
              end if
           end do
        end do
     end do
  else
     do k=1,pov_nz
        do j=1,pov_ny
           do i=1,pov_nx
              ! Generate position in cylindrical coordinates
              my_x = pov_xm(i)
              my_y = sqrt(pov_ym(j)**2+pov_zm(k)**2)
              my_z = atan(abs(pov_zm(k))/(abs(pov_ym(j))+tiny(pov_ym(j))))
              if      (pov_ym(j)>0.0_WP .and. pov_zm(k)>=0.0_WP) then
                 my_z = my_z
              else if (pov_ym(j)<0.0_WP .and. pov_zm(k)>=0.0_WP) then
                 my_z = Pi - my_z
              else if (pov_ym(j)<0.0_WP .and. pov_zm(k)< 0.0_WP) then
                 my_z = Pi + my_z
              else
                 my_z = twoPi - my_z
              end if
              if (my_y.lt.0.0_WP) then
                 my_y=-my_y
                 my_z=my_z+Pi
              end if
              my_z=modulo(my_z,zL)
              ! Test if inside the partition
              if ( my_x.ge.x(imin_) .and. my_x.lt.x(imax_+1) .and. &
                   my_y.ge.y(jmin_) .and. my_y.lt.y(jmax_+1) .and. &
                   my_z.ge.z(kmin_) .and. my_z.lt.z(kmax_+1) ) then
                 nmap = nmap + 1
                 map(nmap,1)=i;map(nmap,2)=j;map(nmap,3)=k
                 pov_grid(nmap)=i+(j-1)*pov_nx+(k-1)*pov_nx*pov_ny
              end if
              ! Take outside points on root process for now
              if ((my_x.lt.x(imin) .or. my_x.ge.x(imax+1) .or. &
                   my_y.lt.y(jmin) .or. my_y.ge.y(jmax+1) .or. &
                   my_z.lt.z(kmin) .or. my_z.ge.z(kmax+1)) .and. irank.eq.iroot) then
                 nmap = nmap + 1
                 map(nmap,1)=i;map(nmap,2)=j;map(nmap,3)=k
                 pov_grid(nmap)=i+(j-1)*pov_nx+(k-1)*pov_nx*pov_ny
              end if
           end do
        end do
     end do
  end if
  
  ! Create the fileview
  allocate(blocklength(nmap))
  blocklength = 1
  pov_grid = pov_grid-1
  call MPI_TYPE_INDEXED(nmap,blocklength,pov_grid,MPI_REAL_SP,fileview,ierr)
  !call MPI_TYPE_CREATE_INDEXED_BLOCK(nmap,1,pov_grid,MPI_REAL_SP,fileview,ierr)
  call MPI_TYPE_COMMIT(fileview,ierr)
  
  ! Precompute interplation coefficients
  call dump_povray_interpolate_init
  
  return
end subroutine dump_povray_init

! =================================== !
! Dump 3D binary povray density files !
! =================================== !
subroutine dump_povray_data
  use dump_povray
  use data
  use combustion
  implicit none
  
  character(len=str_short) :: name
  
  ! Increment file counter
  nfile = nfile + 1
  
  ! Write the df3 files
  if (use_multiphase) then
     ! Raytrace the liquid
     name = "VOF"
     call dump_povray_scalar(VOF,name)
  else if (combust) then
     ! Can render volumetric flames
     ! Need to decide what species we need
     !call die('dump_povray_init: flame rendering not implemented yet.')
     name = "ZMIX"
     call dump_povray_scalar(SC(:,:,:,isc_ZMIX),name)
  else if (isc_ZMIX.ne.0) then
     ! Render ZMIX
     name = "ZMIX"
     call dump_povray_scalar(SC(:,:,:,isc_ZMIX),name)
  else
     ! Nothing we can raytrace
     call die('dump_povray_init: nothing can be raytraced.')
  end if
  
  return
end subroutine dump_povray_data


! =========================================== !
! Dump 3D binary povray density file - scalar !
! =========================================== !
subroutine dump_povray_scalar(scalar,name)
  use dump_povray
  use string
  use data
  use parallel
  use fileio
  implicit none
  integer :: iunit,ierr
  character(len=str_short) :: name
  character(len=str_medium) :: file,header
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_), intent(in) :: scalar
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer(kind=MPI_OFFSET_KIND) :: disp
  logical :: file_is_there
  
  ! Interpolate on the povray mesh
  call dump_povray_interpolate(scalar)
  
  ! Write header file (only root)
  if (irank.eq.iroot) then
     ! Generate the file
     call CREATE_FOLDER("povray/" // trim(adjustl(name)))
     header = "povray/" // trim(adjustl(name)) // "/" // trim(adjustl(name)) // ".head."
     write(header(len_trim(header)+1:len_trim(header)+6),'(i6.6)') nfile
     file = trim(adjustl(name)) // "."
     write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nfile
     ! Open it
     iunit = iopen()
     open(iunit,file=header,form="formatted",iostat=ierr,status="REPLACE")
     ! Write it
     write(iunit,'(a)') 'dfd'
     write(iunit,'(a,x,a)') '[file_name]',trim(file)
     write(iunit,'(a,x,a)') '[Type]','Float'
     write(iunit,'(a,x,a)') '[Format]','Binary'
     write(iunit,'(a,x,i5)') '[X_size]',pov_nx
     write(iunit,'(a,x,i5)') '[Y_size]',pov_ny
     write(iunit,'(a,x,i5)') '[Z_size]',pov_nz
     write(iunit,'(a,x,a)') '[Endian]','Little'
     write(iunit,'(a,x,a)') '[Cyclic]','On'
     write(iunit,'(a)') '[Data]'
     ! Close it
     close(iclose(iunit))
  end if
  
  ! Generate the file
  if (irank.eq.iroot) call CREATE_FOLDER("povray/" // trim(adjustl(name)))
  file = "povray/" // trim(adjustl(name)) // "/" // trim(adjustl(name)) // "."
  write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nfile
  
  ! Open the file
  if (irank.eq.iroot) then
     inquire(file=file,exist=file_is_there)
     if (file_is_there .and. irank.eq.iroot) call MPI_FILE_DELETE(file,mpi_info,ierr)
  end if
  call MPI_FILE_OPEN(comm,file,IOR(MPI_MODE_WRONLY,MPI_MODE_CREATE),mpi_info,iunit,ierr)
  
  ! Write the file
  disp = 0
  if (nmap.gt.0) then
     call MPI_FILE_SET_VIEW(iunit,disp,MPI_REAL_SP,fileview,"native",mpi_info,ierr)
     call MPI_FILE_WRITE_ALL(iunit,pov_data,nmap,MPI_REAL_SP,status,ierr)
  end if
  
  ! Close the file
  call MPI_FILE_CLOSE(iunit,ierr)
  
  return
end subroutine dump_povray_scalar


! ================================================== !
! Dump 3D binary povray density file - interpolation !
! ================================================== !
subroutine dump_povray_interpolate_init
  use dump_povray
  use math
  use masks
  implicit none
  integer :: i1,j1,k1
  integer :: i2,j2,k2
  integer :: n
  real(WP) :: xd,yd,zd
  real(WP) :: wx1,wx2,wy1,wy2,wz1,wz2
  
  ! Allocate the mappings
  allocate(map_interp(nmap,3))
  allocate(coeff_interp(nmap,2,2,2))
  
  ! Compute interpolation coefficients
  do n=1,nmap
     ! Povray mesh position
     i2=map(n,1);j2=map(n,2);k2=map(n,3)
     if (icyl.eq.0) then
        xd=pov_xm(i2);yd=pov_ym(j2);zd=pov_zm(k2)
     else
        xd = pov_xm(i2)
        yd = sqrt(pov_ym(j2)**2+pov_zm(k2)**2)
        zd = atan(abs(pov_zm(k2))/(abs(pov_ym(j2))+tiny(pov_ym(j2))))
        if      (pov_ym(j2)>0.0_WP .and. pov_zm(k2)>=0.0_WP) then
           zd = zd
        else if (pov_ym(j2)<0.0_WP .and. pov_zm(k2)>=0.0_WP) then
           zd = Pi - zd
        else if (pov_ym(j2)<0.0_WP .and. pov_zm(k2)< 0.0_WP) then
           zd = Pi + zd
        else
           zd = twoPi - zd
        end if
        if (yd.lt.0.0_WP) then
           yd=-yd
           zd=zd+Pi
        end if
        zd=modulo(zd,zL)
     end if
     ! Nearest point in arts
     call bisect(xd,i1,xm(imin_-1:imax_+1),nx_+1);i1=i1+imin_-1-1
     call bisect(yd,j1,ym(jmin_-1:jmax_+1),ny_+1);j1=j1+jmin_-1-1
     call bisect(zd,k1,zm(kmin_-1:kmax_+1),nz_+1);k1=k1+kmin_-1-1
     ! Save the position
     map_interp(n,1)=i1;map_interp(n,2)=j1;map_interp(n,3)=k1
     ! Construct the interpolation coefficients
     wx1 = (xm(i1+1)-xd    )/(xm(i1+1)-xm(i1))
     wx2 = (xd      -xm(i1))/(xm(i1+1)-xm(i1))
     wy1 = (ym(j1+1)-yd    )/(ym(j1+1)-ym(j1))
     wy2 = (yd      -ym(j1))/(ym(j1+1)-ym(j1))
     wz1 = (zm(k1+1)-zd    )/(zm(k1+1)-zm(k1))
     wz2 = (zd      -zm(k1))/(zm(k1+1)-zm(k1))
     ! Save the interpolation coefficients
     coeff_interp(n,1,1,1)=wx1*wy1*wz1
     coeff_interp(n,2,1,1)=wx2*wy1*wz1
     coeff_interp(n,1,2,1)=wx1*wy2*wz1
     coeff_interp(n,2,2,1)=wx2*wy2*wz1
     coeff_interp(n,1,1,2)=wx1*wy1*wz2
     coeff_interp(n,2,1,2)=wx2*wy1*wz2
     coeff_interp(n,1,2,2)=wx1*wy2*wz2
     coeff_interp(n,2,2,2)=wx2*wy2*wz2
  end do
  
  return
end subroutine dump_povray_interpolate_init


! ================================================== !
! Dump 3D binary povray density file - interpolation !
! ================================================== !
subroutine dump_povray_interpolate(scalar)
  use dump_povray
  implicit none
  integer :: n
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_), intent(in) :: scalar
  
  ! Interpolate
  do n=1,nmap
     pov_data(n) = real(sum(coeff_interp(n,:,:,:)*&
          scalar(map_interp(n,1):map_interp(n,1)+1,&
                 map_interp(n,2):map_interp(n,2)+1,&
                 map_interp(n,3):map_interp(n,3)+1)),SP)
  end do
  
  return
end subroutine dump_povray_interpolate


! ================= !
! Bisection routine !
! ================= !
subroutine bisect(xloc,iloc,x,nx)
  use precision
  implicit none
  
  real(WP), intent(in)  :: xloc
  integer,  intent(out) :: iloc
  integer,  intent(in)  :: nx
  real(WP), dimension(1:nx), intent(in) :: x
  
  integer :: il,im,iu
  
  ! Take care of outside points
  if (xloc.lt.x(1)) then
     iloc = 1
  else if (xloc.ge.x(nx)) then
     iloc = nx-1
  else
     ! Initialize lower and upper limits
     il=1
     iu=nx
     ! While not done
     do while (iu-il.gt.1)
        ! Compute a mid-point
        im=(iu+il)/2
        ! Replace lower of upper limit as appropriate
        if (xloc.ge.x(im)) then
           il=im
        else
           iu=im
        end if
     end do
     ! Return
     iloc = il
  end if
  
  return
end subroutine bisect
