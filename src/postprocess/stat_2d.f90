module stat_2d
  use stat
  implicit none

  ! Sampled variables
  integer :: stat_nvar
  character(len=str_medium), dimension(:), allocatable :: stat_name

  ! 2D Statistics
  real(WP), dimension(:,:,:), allocatable :: stat_xy
  real(WP), dimension(:,:,:), allocatable :: buf
  real(WP) :: Delta_t
  
  ! Sampled variables - spray
  integer :: stat_nvar_spray
  real(WP), dimension(:,:,:), allocatable :: buf_spray
  character(len=str_medium), dimension(:), allocatable :: stat_name_spray
  
  ! 2D Statistics - spray
  real(WP), dimension(:,:,:), allocatable :: stat_xy_spray
  real(WP) :: Delta_t_spray
  
  ! Fileview
  integer :: fileview
  
contains
  
  subroutine stat_2d_init_names
    use data
    use combustion
    use lpt
    implicit none
    
    integer :: isc,ns
    
    ! ================================================================================================ !
    ! Count the variables
    stat_nvar = 0
    ! Density 
    if (trim(chemistry).ne.'none') stat_nvar = stat_nvar+2
    ! Velocity
    stat_nvar = stat_nvar+6
    if (trim(chemistry).ne.'none') stat_nvar = stat_nvar+6
    ! Scalars
    do isc=1,nscalar
       stat_nvar = stat_nvar+2
       if (trim(chemistry).ne.'none') stat_nvar = stat_nvar+2
    end do
    ! Combustion
    select case (trim(chemistry))
    case ('diffusion')
       stat_nvar = stat_nvar+6
    case ('premixed')
       stat_nvar = stat_nvar+1
    case ('one-step')
       stat_nvar = stat_nvar+7
    end select
!!$    ! Spray coupling
!!$    if (use_lpt .and. lpt_twoway.eq.1) then
!!$       stat_nvar = stat_nvar + 6
!!$       ! Evaporation: SM
!!$       if (trim(lpt_evap).ne.'none' .and. nscalar.ne.0) then
!!$          stat_nvar = stat_nvar+1
!!$       end if
!!$       ! Evaporation: ST
!!$       if (trim(lpt_evap).ne.'none' .and. isc_T.ne.0) then
!!$          stat_nvar = stat_nvar+1
!!$       end if
!!$    end if
    ! Allocate
    allocate(stat_name(stat_nvar))
    
    ns = 0
    ! Density statistics
    if (trim(chemistry).ne.'none') then
       stat_name(ns+1) = 'RHO'
       stat_name(ns+2) = 'RHO^2'
       ns = ns+2
    end if
    ! Velocity statistics
    stat_name(ns+1) = 'U'
    stat_name(ns+2) = 'V'
    stat_name(ns+3) = 'W'
    stat_name(ns+4) = 'U^2'
    stat_name(ns+5) = 'V^2'
    stat_name(ns+6) = 'W^2'
    ns = ns+6
    if (trim(chemistry).ne.'none') then
       stat_name(ns+1) = 'rhoU'
       stat_name(ns+2) = 'rhoV'
       stat_name(ns+3) = 'rhoW'
       stat_name(ns+4) = 'rhoU^2'
       stat_name(ns+5) = 'rhoV^2'
       stat_name(ns+6) = 'rhoW^2'
       ns = ns+6
    end if
    
    ! Scalars
    do isc=1,nscalar
       stat_name(ns+1) = 'SC-'  // trim(adjustl(sc_name(isc)))
       stat_name(ns+2) = 'SC^2-'// trim(adjustl(sc_name(isc)))
       ns = ns+2
       if (trim(chemistry).ne.'none') then
          stat_name(ns+1) = 'rhoS-'  // trim(adjustl(sc_name(isc)))
          stat_name(ns+2) = 'rhoS^2-'// trim(adjustl(sc_name(isc)))
          ns = ns+2
       end if
    end do
    
    ! Combustion
    select case (trim(chemistry))
    case ('diffusion')
       stat_name(ns+1)  = 'rhoT'
       stat_name(ns+2)  = 'rhoY_F'
       stat_name(ns+3)  = 'rhoY_O2'
       stat_name(ns+4)  = 'rhoY_CO2'
       stat_name(ns+5)  = 'rhoY_H2O'
       stat_name(ns+6)  = 'rhoY_CO'
       ns = ns+6
    case ('premixed')
       stat_name(ns+1)  = 'LVLSET'
       ns = ns+1
    case ('one-step')
       stat_name(ns+1) = 'Zmix'
       stat_name(ns+2) = 'Zmix^2'
       stat_name(ns+3) = 'rhoZmix'
       stat_name(ns+4) = 'rhoZmix^2'
       stat_name(ns+5) = 'CHI'
       stat_name(ns+6) = 'rhoCHI'
       stat_name(ns+7) = 'Omega'
       ns = ns+7
    end select
    
!!$    ! Spray coupling: momentum sources
!!$    if (use_lpt .and. lpt_twoway.eq.1) then
!!$       stat_name(ns+1) = 'S1'
!!$       stat_name(ns+2) = 'US1'
!!$       stat_name(ns+3) = 'S2'
!!$       stat_name(ns+4) = 'VS2'
!!$       stat_name(ns+5) = 'S3'
!!$       stat_name(ns+6) = 'WS3'
!!$       ns = ns+6
!!$       ! Evaporation: SM
!!$       if (trim(lpt_evap).ne.'none' .and. nscalar.ne.0) then
!!$          stat_name(ns+1) = 'SM'
!!$          ns = ns+1
!!$       end if
!!$       ! Evaporation: ST
!!$       if (trim(lpt_evap).ne.'none' .and. isc_T.ne.0) then
!!$          stat_name(ns+1) = 'ST'
!!$          ns = ns+1
!!$       end if
!!$    end if
    
    ! ================================================================================================ !
!!$    if (use_lpt) then
!!$       
!!$       ! Count the spray variables
!!$       stat_nvar_spray = 0
!!$       ! Number - d - d^2 - d^3 - velocities - velocities^2 - T - T^2
!!$       stat_nvar_spray = stat_nvar_spray+12
!!$       ! Allocate
!!$       allocate(stat_name_spray(stat_nvar_spray))
!!$       
!!$       stat_nvar_spray = 0
!!$       ! Number - d - d^2 - d^3 - velocities - velocities^2 - T - T^2
!!$       stat_name_spray(stat_nvar_spray+1)  = 'number'
!!$       stat_name_spray(stat_nvar_spray+2)  = 'diameter'
!!$       stat_name_spray(stat_nvar_spray+3)  = 'diameter^2'
!!$       stat_name_spray(stat_nvar_spray+4)  = 'diameter^3'
!!$       stat_name_spray(stat_nvar_spray+5)  = 'u'
!!$       stat_name_spray(stat_nvar_spray+6)  = 'u^2'
!!$       stat_name_spray(stat_nvar_spray+7)  = 'v'
!!$       stat_name_spray(stat_nvar_spray+8)  = 'v^2'
!!$       stat_name_spray(stat_nvar_spray+9)  = 'w'
!!$       stat_name_spray(stat_nvar_spray+10) = 'w^2'
!!$       stat_name_spray(stat_nvar_spray+11) = 'temperature'
!!$       stat_name_spray(stat_nvar_spray+12) = 'temperature^2'
!!$       stat_nvar_spray = stat_nvar_spray+12
!!$       
!!$    end if
    
    return
  end subroutine stat_2d_init_names
  
end module stat_2d


! ================================== !
! Initialize the 2D statistic module !
! ================================== !
subroutine stat_2d_init
  use stat_2d
  use parallel
  use parser
  use lpt
  implicit none
  
  integer, dimension(2) :: gsizes,lsizes,start,ierr
  
  ! Test if we can gather stats
  if (xper.eq.1) call die('stat_2d_init: 2D statistics in x impossible (x is periodic)')
  if (yper.eq.1) call die('stat_2d_init: 2D statistics in y impossible (y is periodic)')
  
  ! Get the number of variables and names
  call stat_2d_init_names
  
  ! Allocate the storage space
  allocate(stat_xy(imin_:imax_,jmin_:jmax_,stat_nvar))
  allocate(buf(imin_:imax_,jmin_:jmax_,stat_nvar))
  stat_xy = 0.0_WP
  if (use_lpt) then
     allocate(stat_xy_spray(imin_:imax_,jmin_:jmax_,stat_nvar_spray))
     allocate(buf_spray(imin_:imax_,jmin_:jmax_,stat_nvar_spray))
     stat_xy_spray = 0.0_WP
  end if
  
  ! Generate the fileview
  gsizes(1) = nx
  lsizes(1) = nx_
  start(1)  = imin_-imin
  gsizes(2) = ny
  lsizes(2) = ny_
  start(2)  = jmin_-jmin
  call MPI_TYPE_CREATE_SUBARRAY(2,gsizes,lsizes,start,MPI_ORDER_FORTRAN,MPI_REAL_WP,fileview,ierr)
  call MPI_TYPE_COMMIT(fileview,ierr)
  
  ! Read the stat file
  call stat_2d_read
  
  return
end subroutine stat_2d_init


! ===================== !
! Sample the statistics !
! ===================== !
subroutine stat_2d_sample
  use stat_2d
  use data
  use combustion
  use time_info
  use memory
  use onestep
  use lpt
  implicit none

  integer :: i,j,k,isc,ns,n

  ! Prepare the chemical variable if necessary
  if     (trim(chemistry).eq.'diffusion') then
     call diffusion_lookup('Y_F'  ,tmp1)
     call diffusion_lookup('Y_O2' ,tmp2)
     call diffusion_lookup('Y_CO2',tmp3)
     call diffusion_lookup('Y_H2O',tmp4)
     call diffusion_lookup('Y_CO' ,tmp5)
  elseif (trim(chemistry).eq.'premixed') then
     !call premixed_lookup('Y_H2O',tmp1)
     !call premixed_lookup('Y_CO' ,tmp2)
     !call premixed_lookup('Y_CO2',tmp3)
     !call premixed_lookup('Y_O2' ,tmp4)
     !call permixed_lookup('Y_OH' ,tmp5)
  end if
  
  ! Gather the stats
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_

           ns = 0

           ! Density
           if (trim(chemistry).ne.'none') then
              stat_xy(i,j,ns+1) = stat_xy(i,j,ns+1) + dt*RHO(i,j,k)
              stat_xy(i,j,ns+2) = stat_xy(i,j,ns+2) + dt*RHO(i,j,k)**2
              ns = ns+2
           end if
           
           ! Velocity
           stat_xy(i,j,ns+1) = stat_xy(i,j,ns+1) + dt*U(i,j,k)
           stat_xy(i,j,ns+2) = stat_xy(i,j,ns+2) + dt*V(i,j,k)
           stat_xy(i,j,ns+3) = stat_xy(i,j,ns+3) + dt*W(i,j,k)
           stat_xy(i,j,ns+4) = stat_xy(i,j,ns+4) + dt*U(i,j,k)**2
           stat_xy(i,j,ns+5) = stat_xy(i,j,ns+5) + dt*V(i,j,k)**2
           stat_xy(i,j,ns+6) = stat_xy(i,j,ns+6) + dt*W(i,j,k)**2
           ns = ns+6
           
           if (trim(chemistry).ne.'none') then
              stat_xy(i,j,ns+1) = stat_xy(i,j,ns+1) + dt*rhoU(i,j,k)
              stat_xy(i,j,ns+2) = stat_xy(i,j,ns+2) + dt*rhoV(i,j,k)
              stat_xy(i,j,ns+3) = stat_xy(i,j,ns+3) + dt*rhoW(i,j,k)
              stat_xy(i,j,ns+4) = stat_xy(i,j,ns+4) + dt*rhoU(i,j,k)*U(i,j,k)
              stat_xy(i,j,ns+5) = stat_xy(i,j,ns+5) + dt*rhoV(i,j,k)*V(i,j,k)
              stat_xy(i,j,ns+6) = stat_xy(i,j,ns+6) + dt*rhoW(i,j,k)*W(i,j,k)
              ns = ns+6
           end if
           
           ! Scalars
           do isc=1,nscalar
              stat_xy(i,j,ns+1) = stat_xy(i,j,ns+1) + dt*SC(i,j,k,isc)
              stat_xy(i,j,ns+2) = stat_xy(i,j,ns+2) + dt*SC(i,j,k,isc)**2
              ns = ns+2
              if (trim(chemistry).ne.'none') then
                 stat_xy(i,j,ns+1) = stat_xy(i,j,ns+1) + dt*RHO(i,j,k)*SC(i,j,k,isc)
                 stat_xy(i,j,ns+2) = stat_xy(i,j,ns+2) + dt*RHO(i,j,k)*SC(i,j,k,isc)**2
                 ns = ns+2
              end if
           end do
           
           ! Combustion
           select case (trim(chemistry))
           case ('diffusion')
              stat_xy(i,j,ns+1) = stat_xy(i,j,ns+1) + dt*RHO(i,j,k)*T   (i,j,k)
              stat_xy(i,j,ns+2) = stat_xy(i,j,ns+2) + dt*RHO(i,j,k)*tmp1(i,j,k)
              stat_xy(i,j,ns+3) = stat_xy(i,j,ns+3) + dt*RHO(i,j,k)*tmp2(i,j,k)
              stat_xy(i,j,ns+4) = stat_xy(i,j,ns+4) + dt*RHO(i,j,k)*tmp3(i,j,k)
              stat_xy(i,j,ns+5) = stat_xy(i,j,ns+5) + dt*RHO(i,j,k)*tmp4(i,j,k)
              stat_xy(i,j,ns+6) = stat_xy(i,j,ns+6) + dt*RHO(i,j,k)*tmp5(i,j,k)
              ns = ns+6
           case ('premixed')
              stat_xy(i,j,ns+1) = stat_xy(i,j,ns+1) + dt*LVLSET(i,j,k)
           case ('one-step')
              ! All this stuff should be already computed before !
              stat_xy(i,j,ns+1) = stat_xy(i,j,ns+1) + dt*( Mix_Frac(i,j,k)               )
              stat_xy(i,j,ns+2) = stat_xy(i,j,ns+2) + dt*( Mix_Frac(i,j,k)**2            )
              stat_xy(i,j,ns+3) = stat_xy(i,j,ns+3) + dt*( RHO(i,j,k)*Mix_Frac(i,j,k)    )
              stat_xy(i,j,ns+4) = stat_xy(i,j,ns+4) + dt*( RHO(i,j,k)*Mix_Frac(i,j,k)**2 )
              stat_xy(i,j,ns+5) = stat_xy(i,j,ns+5) + dt*( CHI(i,j,k)                    )
              stat_xy(i,j,ns+6) = stat_xy(i,j,ns+6) + dt*( RHO(i,j,k)*CHI(i,j,k)         )
              stat_xy(i,j,ns+7) = stat_xy(i,j,ns+7) + dt*( Wchem(i,j,k)                  )
              ns = ns+7
           end select
           
!!$           ! Spray coupling statistics: momentum sources
!!$           if (use_lpt .and. lpt_twoway.eq.1) then
!!$              stat_xy(i,j,ns+1) = stat_xy(i,j,ns+1) + dt*( 1.0_WP/dt * srcU(i,j,k)            )
!!$              stat_xy(i,j,ns+2) = stat_xy(i,j,ns+2) + dt*( 1.0_WP/dt * srcU(i,j,k) * U(i,j,k) )
!!$              stat_xy(i,j,ns+3) = stat_xy(i,j,ns+3) + dt*( 1.0_WP/dt * srcV(i,j,k)            )
!!$              stat_xy(i,j,ns+4) = stat_xy(i,j,ns+4) + dt*( 1.0_WP/dt * srcV(i,j,k) * V(i,j,k) )
!!$              stat_xy(i,j,ns+5) = stat_xy(i,j,ns+5) + dt*( 1.0_WP/dt * srcW(i,j,k)            )
!!$              stat_xy(i,j,ns+6) = stat_xy(i,j,ns+6) + dt*( 1.0_WP/dt * srcW(i,j,k) * W(i,j,k) )
!!$              ns = ns+6
!!$              ! Evaporation: SM
!!$              if (trim(lpt_evap).ne.'none' .and. nscalar.ne.0) then
!!$                 stat_xy(i,j,ns+1) = stat_xy(i,j,ns+1) + dt*( 1.0_WP/dt * srcP(i,j,k) )
!!$                 ns = ns+1
!!$              end if
!!$              ! Evaporation: ST
!!$              if (trim(lpt_evap).ne.'none' .and. isc_T.ne.0) then
!!$                 stat_xy(i,j,ns+1) = stat_xy(i,j,ns+1) + dt*( 1.0_WP/dt * srcSC(i,j,k,isc_T) )
!!$                 ns = ns+1
!!$              end if
!!$           end if
           
        end do
     end do
  end do
  
  Delta_t = Delta_t+dt
  
!!$  ! Spray ----------------------------------------------------------------------
!!$  if (use_lpt) then
!!$     
!!$     do n=1,npart_
!!$        if (part(n)%stop.eq.0) then
!!$           i = part(n)%i
!!$           j = part(n)%j
!!$           
!!$           ns = 0
!!$           
!!$           ! Number - d - d^2 - d^3 - velocities - velocities^2 - T - T^2
!!$           stat_xy_spray(i,j,ns+1)  = stat_xy_spray(i,j,ns+1)  + dt*( 1.0_WP       )
!!$           stat_xy_spray(i,j,ns+2)  = stat_xy_spray(i,j,ns+2)  + dt*( part(n)%d    )
!!$           stat_xy_spray(i,j,ns+3)  = stat_xy_spray(i,j,ns+3)  + dt*( part(n)%d**2 )
!!$           stat_xy_spray(i,j,ns+4)  = stat_xy_spray(i,j,ns+4)  + dt*( part(n)%d**3 )
!!$           stat_xy_spray(i,j,ns+5)  = stat_xy_spray(i,j,ns+5)  + dt*( part(n)%u    )
!!$           stat_xy_spray(i,j,ns+6)  = stat_xy_spray(i,j,ns+6)  + dt*( part(n)%u**2 )
!!$           stat_xy_spray(i,j,ns+7)  = stat_xy_spray(i,j,ns+7)  + dt*( part(n)%v    )
!!$           stat_xy_spray(i,j,ns+8)  = stat_xy_spray(i,j,ns+8)  + dt*( part(n)%v**2 )
!!$           stat_xy_spray(i,j,ns+9)  = stat_xy_spray(i,j,ns+9)  + dt*( part(n)%w    )
!!$           stat_xy_spray(i,j,ns+10) = stat_xy_spray(i,j,ns+10) + dt*( part(n)%w**2 )
!!$           stat_xy_spray(i,j,ns+11) = stat_xy_spray(i,j,ns+11) + dt*( part(n)%T    )
!!$           stat_xy_spray(i,j,ns+12) = stat_xy_spray(i,j,ns+12) + dt*( part(n)%T**2 )
!!$           ns = ns+12
!!$           
!!$        end if
!!$     end do
!!$     
!!$     Delta_t_spray = Delta_t_spray+dt
!!$     
!!$  end if
  
  return
end subroutine stat_2d_sample


! ================================= !
! Read the statistics from the disk !
! ================================= !
subroutine stat_2d_read
  use stat_2d
  use parallel
  implicit none
  
  real(WP) :: time
  integer, dimension(4) :: dims
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer(kind=MPI_OFFSET_KIND) :: disp
  character(len=str_medium)  :: name
  character(len=str_medium) :: filename
  integer :: var,ierr,ifile,data_size
  logical :: file_is_there
  
  ! -- GAS PHASE ---------------------------------------------------------------------
  inquire(file='stat/stat-2D',exist=file_is_there)
  if (file_is_there) then
     
     ! Open the file to write
     filename = trim(mpiiofs) // "stat/stat-2D"
     call MPI_FILE_OPEN(comm,filename,MPI_MODE_RDONLY,mpi_info,ifile,ierr)
     
     ! Read dimensions from header
     call MPI_FILE_READ_ALL(ifile,dims,4,MPI_INTEGER,status,ierr)
     if ((dims(1).ne.nx) .or. (dims(2).ne.ny) .or. (dims(3).ne.1)) then
        print*, 'expected = ',nx,ny,1
        print*, 'stat = ',dims(1),dims(2),dims(3)
        call die('stat_2d_read: The size of the stat file is incorrect')
     end if
     if (dims(4).ne.stat_nvar) call die('stat_2d_read: Wrong number of variables in stat file')
     
     ! Read some headers
     call MPI_FILE_READ_ALL(ifile,Delta_t,1,MPI_REAL_WP,status,ierr)
     call MPI_FILE_READ_ALL(ifile,time,1,MPI_REAL_WP,status,ierr)
     
     ! Read variable names
     do var=1,stat_nvar
        call MPI_FILE_READ_ALL(ifile,name,str_medium,MPI_CHARACTER,status,ierr)
        if (name.ne.stat_name(var)) then
           call die('stat_2d_read: Variables names in stat are incorrect')
        end if
     end do
          
     ! Read each variables
     data_size = nx_*ny_
     do var=1,stat_nvar
        disp = 4*4 + str_medium*stat_nvar + 2*WP + real(var-1,WP)*nx*ny*WP
        call MPI_FILE_SET_VIEW(ifile,disp,MPI_REAL_WP,fileview,"native",mpi_info,ierr)
        call MPI_FILE_READ_ALL(ifile,buf(:,:,var),data_size,MPI_REAL_WP,status,ierr)
     end do
     
     ! Close the file
     call MPI_FILE_CLOSE(ifile,ierr)
     
     ! Recompute the stats
     stat_xy = buf*Delta_t*real(nz_)
     
  else
     
     ! Start from scratch
     Delta_t = 0.0_WP
     
  end if
  
  ! -- SPRAY PHASE -------------------------------------------------------------------
  if (use_lpt) then
     
     inquire(file='stat/stat-2D-spray',exist=file_is_there)
     if (file_is_there) then
        
        ! Open the file to write
        filename = trim(mpiiofs) // "stat/stat-2D-spray"
        call MPI_FILE_OPEN(comm,filename,MPI_MODE_RDONLY,mpi_info,ifile,ierr)
        
        ! Read dimensions from header
        call MPI_FILE_READ_ALL(ifile,dims,4,MPI_INTEGER,status,ierr)
        if ((dims(1).ne.nx) .or. (dims(2).ne.ny) .or. (dims(3).ne.1)) then
           print*, 'expected = ',nx,ny,1
           print*, 'stat = ',dims(1),dims(2),dims(3)
           call die('stat_2d_read: The size of the spray stat file is incorrect')
        end if
        if (dims(4).ne.stat_nvar_spray) call die('stat_2d_read: Wrong number of variables in spray stat file')
        
        ! Read some headers
        call MPI_FILE_READ_ALL(ifile,Delta_t_spray,1,MPI_REAL_WP,status,ierr)
        call MPI_FILE_READ_ALL(ifile,time,1,MPI_REAL_WP,status,ierr)
        
        ! Read variable names
        do var=1,stat_nvar_spray
           call MPI_FILE_READ_ALL(ifile,name,str_medium,MPI_CHARACTER,status,ierr)
           if (name.ne.stat_name_spray(var)) then
              call die('stat_2d_read: Variables names in spray stat are incorrect')
           end if
        end do
        
        ! Read each variables
        data_size = nx_*ny_
        do var=1,stat_nvar_spray
           disp = 4*4 + str_medium*stat_nvar_spray + 2*WP + real(var-1,WP)*nx*ny*WP
           call MPI_FILE_SET_VIEW(ifile,disp,MPI_REAL_WP,fileview,"native",mpi_info,ierr)
           call MPI_FILE_READ_ALL(ifile,buf_spray(:,:,var),data_size,MPI_REAL_WP,status,ierr)
        end do
        
        ! Close the file
        call MPI_FILE_CLOSE(ifile,ierr)
        
        ! Reconstruct the stats
        stat_xy_spray(:,:,1) = buf_spray(:,:,1) * Delta_t_spray
        do var=2,stat_nvar_spray
           stat_xy_spray(:,:,var) = buf_spray(:,:,var) * stat_xy_spray(:,:,1)
        end do
        
     else
        
        ! Start from scratch
        Delta_t_spray = 0.0_WP
        
     end if
     
  end if
  
  return
end subroutine stat_2d_read


! ================================ !
! Write the statistics to the disk !
! ================================ !
subroutine stat_2d_write
  use stat_2d
  use parallel
  use time_info
  implicit none
  
  integer, dimension(4) :: dims
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer(kind=MPI_OFFSET_KIND) :: disp
  character(len=str_medium) :: filename
  integer :: var,ierr,ifile,data_size,i,j
  
  ! -- GAS PHASE ---------------------------------------------------------------------
  
  ! Gather the data
  call parallel_sum_dir(stat_xy,buf,'z')
  buf = buf / (Delta_t*real(nz))
  
  ! Open the file to write
  filename = trim(mpiiofs) // "stat/stat-2D"
  if (irank.eq.iroot) call MPI_FILE_DELETE(filename,mpi_info,ierr)
  call MPI_FILE_OPEN(comm,filename,IOR(MPI_MODE_WRONLY,MPI_MODE_CREATE),mpi_info,ifile,ierr)
  
  ! Write the headers
  if (irank.eq.iroot) then
     ! Write dimensions
     dims(1) = nx
     dims(2) = ny
     dims(3) = 1
     dims(4) = stat_nvar
     call MPI_FILE_WRITE(ifile,dims,4,MPI_INTEGER,status,ierr)
     call MPI_FILE_WRITE(ifile,Delta_t,1,MPI_REAL_WP,status,ierr)
     call MPI_FILE_WRITE(ifile,time,1,MPI_REAL_WP,status,ierr)
     ! Write variable names
     do var=1,stat_nvar
        call MPI_FILE_WRITE(ifile,stat_name(var),str_medium,MPI_CHARACTER,status,ierr)
     end do
  end if
  
  ! Write each variables
  data_size = nx_*ny_
  if (kproc.ne.1) data_size = 0
  do var=1,stat_nvar
     disp = 4*4 + str_medium*stat_nvar + 2*WP + real(var-1,WP)*nx*ny*WP
     call MPI_FILE_SET_VIEW(ifile,disp,MPI_REAL_WP,fileview,"native",mpi_info,ierr)
     call MPI_FILE_WRITE_ALL(ifile,buf(:,:,var),data_size,MPI_REAL_WP,status,ierr)
  end do
  
  ! Close the file
  call MPI_FILE_CLOSE(ifile,ierr)
  
  ! -- SPRAY PHASE -------------------------------------------------------------------
  if (use_lpt) then
     
     ! Gather the data
     call parallel_sum_dir(stat_xy_spray,buf_spray,'z')
     do var=2,stat_nvar_spray
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (buf_spray(i,j,1).gt.0.0_WP) then
                 buf_spray(i,j,var) = buf_spray(i,j,var) / buf_spray(i,j,1)
              else
                 buf_spray(i,j,var) = 0.0_WP
              end if
           end do
        end do
     end do
     buf_spray(:,:,1) = buf_spray(:,:,1) / Delta_t_spray
     
     ! Open the file to write
     filename = trim(mpiiofs) // "stat/stat-2D-spray"
     call MPI_FILE_DELETE(filename,mpi_info,ierr)
     call MPI_FILE_OPEN(comm,filename,IOR(MPI_MODE_WRONLY,MPI_MODE_CREATE),mpi_info,ifile,ierr)
     
     ! Write the headers
     if (irank.eq.iroot) then
        ! Write dimensions
        dims(1) = nx
        dims(2) = ny
        dims(3) = 1
        dims(4) = stat_nvar_spray
        call MPI_FILE_WRITE(ifile,dims,4,MPI_INTEGER,status,ierr)
        call MPI_FILE_WRITE(ifile,Delta_t_spray,1,MPI_REAL_WP,status,ierr)
        call MPI_FILE_WRITE(ifile,time,1,MPI_REAL_WP,status,ierr)
        ! Write variable names
        do var=1,stat_nvar_spray
           call MPI_FILE_WRITE(ifile,stat_name_spray(var),str_medium,MPI_CHARACTER,status,ierr)
        end do
     end if
     
     ! Write each variables
     data_size = nx_*ny_
     if (kproc.ne.1) data_size = 0
     do var=1,stat_nvar_spray
        disp = 4*4 + str_medium*stat_nvar_spray + 2*WP + real(var-1,WP)*nx*ny*WP
        call MPI_FILE_SET_VIEW(ifile,disp,MPI_REAL_WP,fileview,"native",mpi_info,ierr)
        call MPI_FILE_WRITE_ALL(ifile,buf_spray(:,:,var),data_size,MPI_REAL_WP,status,ierr)
     end do
     
     ! Close the file
     call MPI_FILE_CLOSE(ifile,ierr)
     
  end if
  
  return
end subroutine stat_2d_write

