! ================================== !
! Module to track liquid structures  !
! and their ancestries               !
!                                    !
! SID = Structure IDs                !
!       - unique to each structure   !
! LID = Liquid IDs                   !
!       - moves with liquid parcels  !
! ================================== !
module dump_structID
  use multiphase

  ! Pointer to LsID in scalar
  real(WP), dimension(:,:,:), pointer :: LsID

  ! Number of splits and merges
  integer :: eventCount

end module dump_structID

! ============== !
! Initialization !
! ============== !
subroutine dump_structID_init
  use dump_data
  use dump_structID
  implicit none
  integer :: n
  logical :: found_LsID=.false.

  ! Initialize event counter
  eventCount=0
  
  ! Find scalar LsID
  do n=1,nscalar
     if (trim(SC_name(n)).eq.'LsID') then
        found_LsID=.true.
        exit
     end if
  end do
  
  if (found_LsID) then
     ! Set pointer to correct scalar
     LsID(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)=>SC(:,:,:,n)
  else 
     call die('LsID not found in list of scalars')
  end if
  
  ! Initialize output file
  open(1,file="merge_split.txt",form="formatted",status="replace",action="write")
  write(1,*) ' EventCount  Merge/Split     Old LID     Old SID     New LID     New SID', &
       '              Vol                      Time'
  close(1)
  
  return 
end subroutine dump_structID_init

! ============= !
!  Update sIDs  !
! ============= !
subroutine dump_structID_update
  use dump_data
  use dump_structID
  use dump_struct
  use quicksort
  use memory
  implicit none

  integer :: n,m,mm,i,j,k,nStruct,nRem,ierr
  integer, dimension(nproc) :: g_nStruct_array,displace
  integer :: largest,nStructNSplit,nStructNMerge
  ! Lists to identify splits and merges
  integer :: g_nStruct,newLID,LIDlargest
  integer,  dimension(:), allocatable :: SID,LID
  real(wp), dimension(:), allocatable :: sVol
  ! Global lisits
  integer,  dimension(:), allocatable :: g_SID,g_LID,g_IND,g_NewLID, g_tmpInt, sortI
  real(wp), dimension(:), allocatable :: g_VOL, g_tmpWP
  ! Update lists
  integer :: nUp
  integer, dimension(:), allocatable :: up_SID,up_LID

  ! Make LsID nearest integer to avoid accumulation of errors
  LsID=nint(LsID)

  ! Initialize arrays
  allocate( SID(10));  SID=huge(0)
  allocate( LID(10));  LID=huge(0)
  allocate(sVol(10)); sVol=huge(0.0_WP)

  ! Initialize structure count
  nStruct=0

  ! loop over domain to identify structures
  do i=imin_,imax_
     do j=jmin_,jmax_
        cell : do k=kmin_,kmax_

           ! Search existing lists for this structure
           if ((id(i,j,k)).ne.0) then
              ! Search array for same value
              do n=1,nStruct
                 if ( SID(n) .eq. id(i,j,k) .and. &
                      LID(n) .eq. nint(LsID(i,j,k)) ) then ! Found match
                    ! Add to vlume of this structure
                    sVol(n)= sVol(n)+VOFavg(i,j,k)*vol(i,j,k)
                    ! Go to next cell
                    cycle cell 
                 end if
              end do
              
              ! Didn't find a match in current lists -> Add new row
              nStruct = nStruct +1
              SID(nStruct)=id(i,j,k)
              LID(nStruct)=nint(LsID(i,j,k))
              sVOL(nStruct)=VOFavg(i,j,k)*vol(i,j,k)

              ! Reallocate arrays if necessary
              if (nStruct.eq.size(SID)) then
                 call reallocate_int_1D(SID,100)
                 call reallocate_int_1D(LID,100)
                 call reallocate_real_1D(sVol,100)
                 ! Set initial values
                 SID (nStruct+1:size( SID))=huge(0)
                 LID (nStruct+1:size( LID))=huge(0)
                 sVOL(nStruct+1:size(sVOL))=huge(0.0_WP)
              end if
           end if

        end do cell
     end do
  end do

  ! ! Testing !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! if (irank.eq.1) then
  !    nStruct=2
  !    SID(1)=1; LID(1)=2; sVOL(1)=0.5_WP  
  !    SID(2)=1; LID(2)=2; sVOL(2)=1.0_WP
  ! else if (irank.eq.2) then
  !    nStruct=10
  !    SID(1   )=2; LID(1)=2; sVOL(1)=0.5_WP  
  !    SID(2:10)=3; LID(2:10)=2; sVOL(2:10)=0.5_WP
  ! else if (irank.eq.3) then
  !    nStruct=2
  !    SID(1)=1; LID(1)=3; sVOL(1)=0.5_WP  
  !    SID(2)=1; LID(2)=3; sVOL(2)=0.5_WP
  ! else
  !    nStruct=0
  ! end if
  ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  ! ! Print lists
  ! do m=1,nproc
  !    if (irank.eq.m) then
  !       print *,'irank,,SID(n),LID(n),Vol(n)'
  !       do n=1,nStruct
  !          print *,irank,SID(n),LID(n),Vol(n)
  !       end do
  !    end if
  !    call MPI_BARRIER(comm,ierr)
  ! end do

  ! ----------------------------
  ! Sync lists across processors
  ! ----------------------------
  ! Number of structures on each processor
  call MPI_AllGather(nStruct,1,MPI_INTEGER,g_nStruct_array,1,MPI_INTEGER,comm,ierr)
  g_nStruct=sum(g_nStruct_array)

  ! Root allocates global lists
  if (irank.eq.iroot) then
     ! Allocate global list
     allocate(g_SID(g_nStruct)); g_SID=0
     allocate(g_LID(g_nStruct)); g_LID=0
     allocate(g_IND(g_nStruct)); g_IND=0
     allocate(g_VOL(g_nStruct)); g_VOL=0.0_WP
     allocate(sortI(g_nStruct))
     ! Compute displacements for data from each processor in global arrays
     do n=1,nproc
        displace(n)=sum(g_nStruct_array(1:n-1))
     end do
  end if

  ! Gather lists onto root
  call MPI_GATHERV(SID(1:nStruct),nStruct,MPI_INTEGER, &
                 g_SID,g_nStruct_array,displace,MPI_INTEGER,iroot-1,comm,ierr)
  call MPI_GATHERV(LID(1:nStruct),nStruct,MPI_INTEGER, &
                 g_LID,g_nStruct_array,displace,MPI_INTEGER,iroot-1,comm,ierr)
  call MPI_GATHERV(sVOL(1:nStruct),nStruct,MPI_REAL_WP, &
                 g_VOL,g_nStruct_array,displace,MPI_REAL_WP,iroot-1,comm,ierr)

  ! Root works with global lists
  if (irank.eq.iroot) then

     ! Construct index to identify each structure
     g_IND=(/ (n,n=1,g_nStruct) /)

     ! ! Print lists
     ! if (irank.eq.iroot) then
     !    print *,'Global lists: n,g_IND(n),g_SID(n),g_LID(n),g_VOL(n)'
     !    do n=1,g_nStruct
     !       print *,n,g_IND(n),g_SID(n),g_LID(n),g_VOL(n)
     !    end do
     ! end if
     
     ! Check for duplicates in global lists
     ! ------------------------------------
     ! Sort by SID
     sortI=(/ (n,n=1,g_nStruct) /)
     call quick_sort_dint(g_SID,sortI)
     g_LID=g_LID(sortI)
     g_VOL=g_VOL(sortI)
     g_IND=g_IND(sortI)

     ! ! Print lists
     ! if (irank.eq.iroot) then
     !    print *,'Global lists after sorting: n,g_IND(n),g_SID(n),g_LID(n),g_VOL(n)'
     !    do n=1,g_nStruct
     !       print *,n,g_IND(n),g_SID(n),g_LID(n),g_VOL(n)
     !    end do
     ! end if

     ! Loop through list and combine structures with the same SID and LID
     nRem=0
     SIDloop : do n=1,g_nStruct-1
        ! Update g_nStruct and nRem
        g_nStruct=g_nStruct-nRem
        nRem=0
        ! Loop over other structures in list
        do m=n+1,g_nStruct-nRem
           ! Check if SID's match
           if (g_SID(n).eq.g_SID(m-nRem)) then
              ! Check if LIDs match
              if (g_LID(n).eq.g_LID(m-nRem)) then ! combine structures
                 ! Sum volumnes
                 g_VOL(n)=g_VOL(n)+g_VOL(m-nRem)
                 ! Shift lists
                 g_SID(m-nRem:g_nStruct-1)=g_SID(m-nRem+1:g_nStruct)
                 g_LID(m-nRem:g_nStruct-1)=g_LID(m-nRem+1:g_nStruct)
                 g_VOL(m-nRem:g_nStruct-1)=g_VOL(m-nRem+1:g_nStruct)
                 g_IND(m-nRem:g_nStruct-1)=g_IND(m-nRem+1:g_nStruct)
                 ! Update counter on number of rows that have been removed
                 nRem=nRem+1
              else
                 ! Do nothing, different structures
              end if
           else  ! SID's do not match => move onto next SID
              cycle SIDloop
           end if
        end do
     end do SIDloop
     ! Update g_nStruct without removed rows
     g_nStruct=g_nStruct-nRem

     ! Remove deleted rows
     allocate(g_tmpInt(g_nStruct))
     allocate(g_tmpWP (g_nStruct))
     g_tmpInt=g_SID(1:g_nStruct); deallocate(g_SID); allocate(g_SID(g_nStruct)); g_SID=g_tmpInt
     g_tmpInt=g_LID(1:g_nStruct); deallocate(g_LID); allocate(g_LID(g_nStruct)); g_LID=g_tmpInt
     g_tmpWP =g_VOL(1:g_nStruct); deallocate(g_VOL); allocate(g_VOL(g_nStruct)); g_VOL=g_tmpWP
     deallocate(g_tmpInt,g_tmpWP)

     ! Recreate g_IND with combined structures
     deallocate(g_IND)
     allocate(g_IND(g_nStruct))
     g_IND=(/ (n,n=1,g_nStruct) /)
  

     ! ! Print lists
     ! print *,'Global lists after combining duplicates: g_IND(n),g_SID(n),g_LID(n),g_VOL(n)'
     ! do n=1,g_nStruct
     !    print *,g_IND(n),g_SID(n),g_LID(n),g_VOL(n)
     ! end do

     ! Allocate update lists
     nUp=0
     allocate(up_SID(g_nStruct))
     allocate(up_LID(g_nStruct))

     ! ---------------------------
     !      SPLITS  
     ! ---------------------------
     ! Sort by LIDlist
     sortI=(/ (n,n=1,g_nStruct) /)
     call quick_sort_dint(g_LID,sortI) 
     g_SID   =g_SID   (sortI)
     !g_NewLID=g_NewLID(sortI)
     g_VOL   =g_VOL   (sortI)
     g_IND   =g_IND   (sortI)

     ! Initialize split counter
     nStructNSplit=1
     
     ! Loop over structures looking for split (same LID, diff SID)
     do n=1,g_nStruct-1

        ! If this structure was part of a previous split -> skip
        if (nStructNSplit.gt.1) then
           nStructNSplit=nStructNSplit-1
           cycle
        end if

        ! If the next LID equals the current LID split occured
        do m=n+1,g_nStruct
           if (g_LID(n).eq.g_LID(m)) then
              nStructNSplit=nStructNSplit+1
           else
              exit
           end if
        end do

        ! Cycle if not a split
        if (nStructNSplit.eq.1) cycle

        ! Identify largest structure
        largest=maxloc(g_VOL(n:n+nStructNSplit-1),1)
        LIDlargest=g_LID(n+largest-1)
        

        ! Loop over structures in this split event to update LIDs
        do m=1,nStructNSplit
           
           ! All structures but largest get new LID 
           if (m.ne.largest) then
              
              ! Check if strucutre already has a new LID from a merge event
              !if (g_NewLID(n+m-1).eq.LIDlargest) then
                 
                 ! Structure needs new LID
                 newLID=maxval(g_LID)+1
                 g_LID(n+m-1)=newLID
                 
                 ! Update any other structures with this SID to the new LID
                 !do mm=1,g_nStruct
                 !   if (g_SID(mm).eq.g_SID(n+m-1)) g_NewLID(mm)=newLID
                 !end do
                 
                 ! Store new LID in update lists to send to procs
                 nUp=nUp+1
                 up_SID(nUp)=g_SID(n+m-1)
                 up_LID(nUp)=newLID
                 
             ! end if
           end if
        end do

        ! Update event counter
        eventCount=eventCount+1

        ! Write Split information to file
        open(1,file="merge_split.txt",form="formatted",status="old",position="append",action="write")
        do m=1,nStructNSplit
           if (m.ne.largest) then
              print *,'SPLITTING  ',g_IND(n+m-1),' from ',g_IND(n+largest-1),'!!!! newLID=',g_LID(n+m-1)
              write(1,*) eventCount, '       Split',  g_LID(n+largest-1), g_SID(n+largest-1), &
                   g_LID(n+m-1), g_SID(n+m-1), g_VOL(n+m-1), time
           end if
        end do
        close(1)
        
     end do
     
     ! ! Print lists
     ! print *,'Global lists after splits: g_IND(n),g_SID(n),g_LID(n),g_VOL(n)'
     ! do n=1,g_nStruct
     !    print *,g_IND(n),g_SID(n),g_LID(n),g_VOL(n)
     ! end do

     ! ---------------------------
     !      MERGES  
     ! ---------------------------
     ! Sort by SIDlist
     sortI=(/ (n,n=1,g_nStruct) /)
     call quick_sort_dint(g_SID,sortI) 
     g_LID   =g_LID   (sortI)
     !g_NewLID=g_NewLID(sortI)
     g_VOL   =g_VOL   (sortI)
     g_IND   =g_IND   (sortI)

     ! ! Print lists
     ! print *,'Global lists after sorting by SID: g_IND(n),g_SID(n),g_LID(n),g_VOL(n)'
     ! do n=1,g_nStruct
     !    print *,g_IND(n),g_SID(n),g_LID(n),g_VOL(n)
     ! end do

     ! Initialize split counter
     nStructNMerge=1

     ! Create list to hold new LID's needed by split routine
     !allocate(g_NewLID(g_nStruct)); g_NewLID=g_LID
     do n=1,g_nStruct-1

        ! If this structure was part of a previous merge -> skip
        if (nStructNMerge.gt.1) then
           nStructNMerge=nStructNMerge-1
           cycle
        end if

        ! If the next SID equals the current SID merge occurred
        do m=n+1,g_nStruct
           if (g_SID(n).eq.g_SID(m)) then
              nStructNMerge=nStructNMerge+1
           else
              exit
           end if
        end do

        ! Cycle if not a merge
        if (nStructNMerge.eq.1) cycle

        ! Identify largest structure
        largest=maxloc(g_VOL(n:n+nStructNMerge-1),1)

        ! Update struture volume to the combined volume
        g_VOL(n:n+nStructNMerge-1)=sum(g_VOL(n:n+nStructNMerge-1))

        ! New LID of merged structure
        newLID=g_LID(n+largest-1)

        ! Put new LID into lists 
        !g_NewLID(n:n+nStructNMerge-1)=newLID

        ! Store new LID in update lists to send to procs
        nUp=nUp+1
        up_SID(nUp)=g_SID(n)
        up_LID(nUp)=newLID

        ! Update event counter
        eventCount=eventCount+1

        ! Write merge information to file
        open(1,file="merge_split.txt",form="formatted",status="old",position="append",action="write")
        do m=1,nStructNMerge
           if (m.ne.largest) then
              print *,'MERGING    ',g_IND(n+largest-1),' and ',g_IND(n+m-1),'!!!! Seting LsID to ',newLID
              write(1,*) eventCount, '       Merge', g_LID(n+m-1), g_SID(n+m-1), g_LID(n+largest-1), g_SID(n+largest-1), g_VOL(n+m-1), time
           end if
        end do
        close(1)
     end do
     
     ! ! Print lists
     ! print *,'Global lists after merges: g_IND(n),g_SID(n),g_LID(n),g_VOL(n)'
     ! do n=1,g_nStruct
     !    print *,g_IND(n),g_SID(n),g_LID(n),g_VOL(n)
     ! end do

     ! Deallocate global lists
     deallocate(g_SID,g_LID,g_IND,g_VOL) !,g_NewLID)
     
  end if ! Root done working on global lists

  ! ------------------------------
  ! Update LsID using update lists
  ! ------------------------------
  ! Number of updates in lists
  call MPI_Bcast(nUp,1,MPI_INTEGER,iroot-1,comm,ierr)

  ! Communicate update lists to all procs
  if (irank.ne.iroot) then
     allocate(up_SID(nUp))
     allocate(up_LID(nUp))
  end if
  call MPI_Bcast(up_SID(1:nUp),nUp,MPI_INTEGER,iroot-1,comm,ierr)
  call MPI_Bcast(up_LID(1:nUp),nUp,MPI_INTEGER,iroot-1,comm,ierr)
  
  ! Each proc updates LsID (global array)
  do n=1,nUp
     where (id.eq.up_SID(n)) LsID=up_LID(n)
  end do

  ! Deallocate local lists
  deallocate(SID,LID,sVOL,up_SID,up_LID)
  
  return
end subroutine dump_structID_update 
  
 
