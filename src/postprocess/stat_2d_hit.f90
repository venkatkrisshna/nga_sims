module stat_2d_hit
	use stat
	implicit none

	! Sampled Variables
	integer :: stat_nvar
	character(len=str_medium), dimension (:), allocatable ::stat_name
	
	! 2D Statistics
  real(WP), dimension(:,:,:), allocatable :: stat_xy, buf
	  real(WP) :: Delta_t
  
  ! Fileview
  integer :: fileview
 
 contains
 
     
! ====================================================== !
! Allocate memory for each statistic to be calculated		 !
! ====================================================== !
    subroutine stat_2d_hit_init_names
	    use data
	    implicit none
	
			stat_nvar = 0
			stat_nvar = stat_nvar + 1			! Umean
			stat_nvar = stat_nvar + 1     ! Vmean
			stat_nvar = stat_nvar + 1     ! Wmean
			stat_nvar = stat_nvar + 1			! U2mean
			stat_nvar = stat_nvar + 1			! V2mean
			stat_nvar = stat_nvar + 1			! W2mean
			stat_nvar = stat_nvar + 1			! UVmean
			stat_nvar = stat_nvar + 1			! UWmean
			stat_nvar = stat_nvar + 1			! VWmean
			stat_nvar = stat_nvar + 1			! Alphmean
			stat_nvar = stat_nvar + 1			! Alph2mean
			stat_nvar = stat_nvar + 1			! AlphUmean
			stat_nvar = stat_nvar + 1			! AlphVmean
			stat_nvar = stat_nvar + 1			! AlphWmean
			stat_nvar = stat_nvar + 1			! SurfAmean
			stat_nvar = stat_nvar + 1			! SurfA2mean
			stat_nvar = stat_nvar + 1			! STmean
			
			! Allocate the stat_name matrix
			allocate(stat_name(stat_nvar))
			
			! Fill stat_name matrix with names for each stat to be computed										
	    stat_name(1) = 		'Umean'
	    stat_name(2) = 		'Vmean'
	    stat_name(3) = 		'Wmean'
	    stat_name(4) = 		'U2mean'
	    stat_name(5) = 		'V2mean'
	    stat_name(6) = 		'W2mean'
	    stat_name(7) = 		'UVmean'
	    stat_name(8) = 		'UWmean'
	    stat_name(9) = 		'VWmean'
	    stat_name(10) = 	'Alphmean'
	    stat_name(11) = 	'Alph2mean'
	    stat_name(12) = 	'AlphUmean'
	    stat_name(13) = 	'AlphVmean'
	    stat_name(14) = 	'AlphWmean'
	    stat_name(15) = 	'SurfAmean'
	    stat_name(16) = 	'SurfA2mean'
	    stat_name(17) = 	'STmean'
	    
	    return
    end subroutine stat_2d_hit_init_names
end module stat_2d_hit

! ======================================= !
! Initialize the 2D HIT statistics module !
! ======================================= !
subroutine stat_2d_hit_init
	use stat_2d_hit
	use parallel
	use parser
	implicit none
	
	integer, dimension(2) :: gsizes, lsizes, start, ierr
	
	if(xper.eq.1 .and. yper.eq.1 .and. zper.eq.1) call die('stat_2d_hit_init: 2D statistics impossible, fully periodic domain')
	
	! Get the number of variables and names
	call stat_2d_hit_init_names
	
	! Allocate the storage space for each processor
	allocate(stat_xy(imin_:imax_,jmin_:jmax_,stat_nvar))
	allocate(buf(imin_:imax_,jmin_:jmax_,stat_nvar))

	stat_xy = 0.0_WP
  buf = 0.0_WP

	! Generate the fileview formatting for MPIIO
	gsizes(1) = nx
	lsizes(1) = nx_
	start(1)  = imin_-imin
	gsizes(2) = ny
	lsizes(2) = ny
	start(2)  = jmin_-jmin
	
	call MPI_TYPE_CREATE_SUBARRAY(2,gsizes,lsizes,start,MPI_ORDER_FORTRAN,MPI_REAL_WP,fileview,ierr)
	call MPI_TYPE_COMMIT(fileview, ierr)

	! Read existing stat file for restarts
	call stat_2d_hit_read


	return
end subroutine stat_2d_hit_init

! =================================== !
! Sample and calculate HIT statistics !
! =================================== !
subroutine stat_2d_hit_sample
	use stat_2d_hit
  use data
	use time_info
	use metric_generic
	use math
	use memory
  use multiphase
  use compgeom
  use multiphase_gfm
	implicit none

  real(WP) :: my_surf, my_vol
  real(WP), dimension(3,8) :: pos
  real(WP), dimension(  8) :: val
  real(WP), dimension(  3) :: com_, coms_, nvec
	integer :: i,j,k

	! Compute the stats
	do k=kmin_,kmax_
		do j=jmin_,jmax_
			do i=imin_,imax_ 
        stat_xy(i,j,1)   =	stat_xy(i,j,1)  + dt*sum(interp_u_xm(i,j,:)*U(i-st1:i+st2,j,k))                                               ! Umean
        stat_xy(i,j,2)   =	stat_xy(i,j,2)  + dt*sum(interp_v_ym(i,j,:)*V(i,j-st1:j+st2,k))                                               ! Vmean
        stat_xy(i,j,3)   =	stat_xy(i,j,3)  + dt*sum(interp_w_zm(i,j,:)*W(i,j,k-st1:k+st2))                                               ! Wmean
        stat_xy(i,j,4)   =	stat_xy(i,j,4)  + dt*sum(interp_u_xm(i,j,:)*U(i-st1:i+st2,j,k)*U(i-st1:i+st2,j,k))                            ! U2mean
        stat_xy(i,j,5)   =	stat_xy(i,j,5)  + dt*sum(interp_v_ym(i,j,:)*V(i,j-st1:j+st2,k)*V(i,j-st1:j+st2,k))                            ! V2mean
        stat_xy(i,j,6)   =	stat_xy(i,j,6)  + dt*sum(interp_w_zm(i,j,:)*W(i,j,k-st1:k+st2)*W(i,j,k-st1:k+st2))                            ! W2mean
        stat_xy(i,j,7)   =	stat_xy(i,j,7)  + dt*sum(interp_u_xm(i,j,:)*U(i-st1:i+st2,j,k))*sum(interp_v_ym(i,j,:)*V(i,j-st1:j+st2,k))    ! UVmean
        stat_xy(i,j,8)   =	stat_xy(i,j,8)  + dt*sum(interp_u_xm(i,j,:)*U(i-st1:i+st2,j,k))*sum(interp_w_zm(i,j,:)*W(i,j,k-st1:k+st2))    ! UWmean
        stat_xy(i,j,9)   =	stat_xy(i,j,9)  + dt*sum(interp_v_ym(i,j,:)*V(i,j-st1:j+st2,k))*sum(interp_w_zm(i,j,:)*W(i,j,k-st1:k+st2))    ! VWmean
        stat_xy(i,j,10)  =	stat_xy(i,j,10) + dt*VOFavg(i,j,k)                                                                            ! Alphmean
        stat_xy(i,j,11)  =	stat_xy(i,j,11) + dt*VOFavg(i,j,k)*VOFavg(i,j,k)                                                              ! Alph2mean
        stat_xy(i,j,12)  =	stat_xy(i,j,12) + dt*VOFavg(i,j,k)*sum(interp_u_xm(i,j,:)*U(i-st1:i+st2,j,k))                        ! AlphUmean
        stat_xy(i,j,13)  =	stat_xy(i,j,13) + dt*VOFavg(i,j,k)*sum(interp_v_ym(i,j,:)*V(i,j-st1:j+st2,k))                        ! AlphVmean
        stat_xy(i,j,14)  =	stat_xy(i,j,14) + dt*VOFavg(i,j,k)*sum(interp_w_zm(i,j,:)*W(i,j,k-st1:k+st2))                        ! AlphWmean

!        ! Calculate surface area of cell i,j,k
!        if( (VOF(i,j,k).gt.VOFlo) .and. (VOF(i,j,k).lt.VOFhi) ) then
!          ! Use marching tets to obtain surface-in-cell
!          pos=reshape((/ &
!            x(i  ),y(j  ),z(k  ), &
!            x(i+1),y(j  ),z(k  ), &
!            x(i  ),y(j+1),z(k  ), &
!            x(i+1),y(j+1),z(k  ), &
!            x(i  ),y(j  ),z(k+1), &
!            x(i+1),y(j  ),z(k+1), &
!            x(i  ),y(j+1),z(k+1), &
!            x(i+1),y(j+1),z(k+1) /),(/3,8/))
!
!          val=-(normx(i,j,k)*pos(1,:)+normy(i,j,k)*pos(2,:)+normz(i,j,k)*pos(3,:)-dist(i,j,k))
!          call cut_3cube(0.0_WP,pos,val,my_vol,com_,my_surf,coms_,nvec)
!        else
!          my_surf = 0.0_WP
!        end if

        my_surf = 0.0_WP

        stat_xy(i,j,15)  =	stat_xy(i,j,15) + dt*my_surf                      ! SurfAmean, STILL NEED TO CHECK
        stat_xy(i,j,16)  =	stat_xy(i,j,16) + dt*my_surf*my_surf              ! SurfA2Umean, STILL NEED TO CHECK
        stat_xy(i,j,17)  =	stat_xy(i,j,17) + dt*RP_gfm(i,j,k)                ! STmean, STILL NEED TO CHECK
	    end do
	  end do
	end do

  ! For variance <u'u'> = <u*u> - <u><u>
	


	! Update time to current time
	Delta_t = Delta_t + dt

	return

end subroutine stat_2d_hit_sample


! ========================================================== !
! Updates buf for use in visit output and binary/HDF5 output !
! ========================================================== !
subroutine stat_2d_hit_buf_update
  use stat_2d_hit
  use parallel
  implicit none

  call parallel_sum_dir(stat_xy,buf,'z')
  buf = buf / (Delta_t*real(nz))

  return
end subroutine


	
! ========================================== !
! Read HIT Statistics from disk if available !	
! ========================================== !
subroutine stat_2d_hit_read
	use stat_2d_hit
	use parallel
	implicit none
	
  real(WP) :: time
  integer, dimension(4) :: dims
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer(kind=MPI_OFFSET_KIND) :: disp
  character(len=str_medium)  :: name
  character(len=str_medium) :: filename
  integer :: var,ierr,ifile,data_size
  logical :: file_is_there

  inquire(file='stat/stat_2D_hit',exist=file_is_there)
  if(file_is_there) then
  
		! Open the file to write
		filename = trim(mpiiofs)// "stat/stat_2D_hit"
		call MPI_FILE_OPEN(comm,filename,MPI_MODE_RDONLY,mpi_info,ifile, ierr)
		
		! Read dimensions from header
		call MPI_FILE_READ_ALL(ifile, dims, 4, MPI_INTEGER,status,ierr)
		if ((dims(1).ne.nx) .or. (dims(2).ne.ny) .or. (dims(3).ne.1)) then
			print*, 'expected = ',nx,ny,1
			print*, 'stat = ',dims(1),dims(2),dims(3)
			call die('stat_2d_hit_read: The size of the stat file is incorrect')
		end if
		
		! Read headers
		call MPI_FILE_READ_ALL(ifile,Delta_t,1,MPI_REAL_WP,status,ierr)
		call MPI_FILE_READ_ALL(ifile,time,1,MPI_REAL_WP,status,ierr)
		
		! Read variable names
		do var = 1, stat_nvar
			call MPI_FILE_READ_ALL(ifile,name,str_medium,MPI_CHARACTER,status,ierr)
			if(name.ne.stat_name(var)) then
				call die('stat_2d_hit_read: Variable name in stat is incorrect')
			end if
		end do
		
		! Read each variable into memory
		data_size = nx_*ny_
		do var=1,stat_nvar
			disp = 4*4 + str_medium*stat_nvar + 2*WP + real(var-1,WP)*nx*ny*WP
			call MPI_FILE_SET_VIEW(ifile,disp,MPI_REAL_WP,fileview,"native",mpi_info,ierr)
			call MPI_FILE_READ_ALL(ifile,buf(:,:,var),data_size,MPI_REAL_WP,status,ierr)
		end do
		
		! Close the file
		call MPI_FILE_CLOSE(ifile, ierr)
		
		! Recompute the stats
		stat_xy = buf*Delta_t*real(nz_)
		
	else
		
		! Start from scratch
		Delta_t = 0.0_WP
	
	end if
	
	return
end subroutine stat_2d_hit_read


! ================================= !
! Write stats out to a file on disk ! 
! ================================= !
subroutine stat_2d_hit_write
	use stat_2d_hit
	use parallel
	use time_info
	implicit none

  integer, dimension(4) :: dims
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer(kind=MPI_OFFSET_KIND) :: disp
  character(len=str_medium) :: filename
  integer :: var,ierr,ifile,data_size,i,j
  
  call stat_2d_hit_buf_update
  
  ! Open the file to write
  filename = trim(mpiiofs) // "stat/stat_2D_hit"
  if (irank.eq.iroot) call MPI_FILE_DELETE(filename,mpi_info,ierr)
  call MPI_FILE_OPEN(comm,filename,IOR(MPI_MODE_WRONLY,MPI_MODE_CREATE),mpi_info,ifile,ierr)

  ! Write the headers
  if (irank.eq.iroot) then
  ! Write dimensions
    dims(1) = nx
    dims(2) = ny
    dims(3) = 1
    dims(4) = stat_nvar
    call MPI_FILE_WRITE(ifile,dims,4,MPI_INTEGER,status,ierr)
    call MPI_FILE_WRITE(ifile,Delta_t,1,MPI_REAL_WP,status,ierr)
    call MPI_FILE_WRITE(ifile,time,1,MPI_REAL_WP,status,ierr)
  ! Write variable names
    do var=1,stat_nvar
      call MPI_FILE_WRITE(ifile,stat_name(var),str_medium,MPI_CHARACTER,status,ierr)
    end do
  end if

  ! Write each variables
  data_size = nx_*ny_
  if (kproc.ne.1) data_size = 0
    do var=1,stat_nvar
      disp = 4*4 + str_medium*stat_nvar + 2*WP + real(var-1,WP)*nx*ny*WP
      call MPI_FILE_SET_VIEW(ifile,disp,MPI_REAL_WP,fileview,"native",mpi_info,ierr)
      call MPI_FILE_WRITE_ALL(ifile,buf(:,:,var),data_size,MPI_REAL_WP,status,ierr)
    end do

    ! Close the file
    call MPI_FILE_CLOSE(ifile,ierr)

  call stat_2d_hit_PDF_write

	return
end subroutine stat_2d_hit_write

! ============================================= !
! Compute and export PDF of stat variables      !
! Called at same frequency as stat_2d_hit_write !
! ============================================= !
subroutine stat_2d_hit_PDF_write
  use stat_2d_hit
  use time_info
  use parallel
  implicit none
  integer :: nbins, bin
  real(WP),dimension(:,:), allocatable :: pdf, binloc, group_pdf
  real(WP), dimension(:), allocatable :: varmin, varmax
  real(WP) :: varmin_, varmax_

  integer, dimension(4) :: dims
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer(kind=MPI_OFFSET_KIND) :: disp
  character(len=str_medium) :: filename
  integer :: var,ierr,ifile,data_size,i,j


  ! Set number of bins to number of samples (ny)
  nbins = ny

  ! Allocate required variables
  allocate(pdf(imin_:imax_,nbins))
  allocate(group_pdf(imin_:imax_,nbins))
  allocate(binloc(imin_:imax_,nbins))
  allocate(varmin(imin_:imax_))
  allocate(varmax(imin_:imax_))

  ! Open the file to write
  filename = trim(mpiiofs) // "stat/PDFs_stat_2D_hit"
  if (irank.eq.iroot) call MPI_FILE_DELETE(filename,mpi_info,ierr)
  call MPI_FILE_OPEN(comm,filename,IOR(MPI_MODE_WRONLY,MPI_MODE_CREATE),mpi_info,ifile,ierr)

  ! Write the headers
  if (irank.eq.iroot) then
    ! Write dimensions
    dims(1) = nx
    dims(2) = ny
    dims(3) = 1
    dims(4) = stat_nvar
    call MPI_FILE_WRITE(ifile,dims,4,MPI_INTEGER,status,ierr)
    call MPI_FILE_WRITE(ifile,Delta_t,1,MPI_REAL_WP,status,ierr)
    call MPI_FILE_WRITE(ifile,time,1,MPI_REAL_WP,status,ierr)
    ! Write variable names
    do var=1,stat_nvar
      call MPI_FILE_WRITE(ifile,stat_name(var),str_medium,MPI_CHARACTER,status,ierr)
    end do
  end if

  do var = 1,stat_nvar
    pdf = 0.0
    group_pdf = 0.0
    binloc = 0.0
    varmin = 0.0
    varmax = 0.0

    ! Find min and max for the variable in the x-slice (y-z plane)
    do i = imin_,imax_
      varmax_ = maxval(buf(i,:,var))
      varmin_ = minval(buf(i,:,var))
      call parallel_max_dir(varmax_, varmax(i),'y')
      call parallel_min_dir(varmin_, varmin(i),'y')
    end do

    ! Create bin locations for current variable
    do i = imin_, imax_
      do bin = 1, nbins
        binloc(i,bin) = (real(bin,WP)-1.0_WP)*(varmax(i)-varmin(i))/(real(nbins,WP)-1.0_WP) + varmin(i)
      end do
    end do

    ! Place each value into a bin and tally
    do j=jmin_,jmax_
      do i=imin_,imax_
        bin = NINT((buf(i,j,var)-varmin(i))/(varmax(i)-varmin(i)+tiny(1.0_WP))*(real(nbins,WP)-1.0_WP))+1
        pdf(i,bin) = pdf(i,bin) + 1
      end do
    end do

    ! Normalize the PDF to sum to 1
    do i = imin_,imax_
      pdf(i,:) = pdf(i,:)/(real(ny,WP))
    end do

    ! Gather complete PDF to each processor
    do i = imin_,imax_
      call parallel_sum_dir_real_1d(pdf(i,:),group_pdf(i,:),'y')
    end do

    ! Write the PDF data to binary file
    data_size = nx_*nbins
    if (kproc.ne.1) data_size = 0
    if (jproc.ne.1) data_size = 0
    disp = 4*4 + str_medium*stat_nvar + 2*WP + real(var-1,WP)*nx*nbins*WP + real(var-1,WP)*nx*nbins*WP
    call MPI_FILE_SET_VIEW(ifile,disp,MPI_REAL_WP,fileview,"native",mpi_info,ierr)
    call MPI_FILE_WRITE_ALL(ifile,pdf,data_size,MPI_REAL_WP,status,ierr)
    call MPI_FILE_WRITE_ALL(ifile,binloc,data_size,MPI_REAL_WP,status,ierr)

  end do

  ! Close the file
  call MPI_FILE_CLOSE(ifile,ierr)

  return
end subroutine stat_2d_hit_PDF_write


! ===================================== !
! Read stats in from HDF5, for restart  !
! ===================================== !
subroutine stat_2d_hit_read_hdf5
  use stat_2d_hit
  use parallel
  !use HDF5
  implicit none

  real(WP) :: time
  integer, dimension(4) :: dims
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer(kind=MPI_OFFSET_KIND) :: disp
  character(len=str_medium)  :: name
  character(len=str_medium) :: filename
  integer :: var,ierr,ifile,data_size
  logical :: file_is_there

  inquire(file='stat/stat_2D_hit',exist=file_is_there)
  if(file_is_there) then

    ! Open the file to write
    filename = trim(mpiiofs)// "stat/stat_2D_hit"
    call MPI_FILE_OPEN(comm,filename,MPI_MODE_RDONLY,mpi_info,ifile, ierr)


    ! Check dimensions from the header, make sure dimensions of data match simulation dimensions (nx,ny)
    ! Create error, call simulation die() if different dimensions (see above)


    ! Read in Delta_t and time


    ! Read in variable names, kill if not the same as simulation


    ! Read variable data into memory


    ! Recompute stats


  else
    ! No file exists, Delta_t = 0.0
    Delta_t = 0.0_WP

  end if

  return
end subroutine stat_2d_hit_read_hdf5


! ================================ !
! Write stats out using HDF5       !
! ================================ !
!subroutine stat_2d_hit_write_hdf5
!  use stat_2d_hit
!  use parallel
!  use time_info
!  use HDF5
!  implicit none
!  character(len=str_medium) :: filename, buffer, attr_buffer
!  character(len=str_medium), dimension(:), allocatable :: aname, attr_data
!  integer(SIZE_T) :: attrlen
!  integer(HID_T) :: plist_id, maingroup_id
!  integer(HID_T) :: mytime_id, mytimedat_id
!  integer :: i,j,q,attr_max,l, error
!  integer(HID_T) :: file_id, achar_id, attr_id
!  integer(HSIZE_T), dimension(1) :: attr_dims
!  integer(HSIZE_T), dimension(2) :: dims
!  integer(HID_T), dimension(:), allocatable ::group_id, space_id, dset, atype_id, attr_space
!
!  ! Allocate memory for the HDF5 group id's
!  allocate(group_id(stat_nvar))
!  allocate(dset(stat_nvar))
!  allocate(space_id(stat_nvar))
!
!
!  ! See below for VizSchema HDF5 Formatting
!  ! https://ice.txcorp.com/trac/vizschema/wiki/Variables
!  ! https://ice.txcorp.com/trac/vizschema/wiki/OtherMetaData
!  ! Type of data
!
!  attr_max = 5
!  allocate(aname(attr_max))
!  allocate(attr_data(attr_max))
!  allocate(atype_id(attr_max))
!  allocate(attr_space(attr_max))
!
!  aname(1) = "vsType"         ; attr_data(1) = "variable"
!  aname(2) = "vsMesh"         ; attr_data(2) = "mesh" 
!  aname(3) = "vsCentering"    ; attr_data(3) = "nodal"
!  aname(4) = "vsIndexOrder"   ; attr_data(4) = "compMinorC"
!  aname(5) = "vsTimeGroup"    ; attr_data(5) = "/mytime"
!
!  attr_dims(1) = 1
!  attrlen = 200
!
!  ! Create the data to print (taken from module)
!  call stat_2d_hit_buf_update
!
!  dims(1) = imax - imin +1
!  dims(2) = jmax - jmin +1
!
!  ! Assign filename
!  filename = trim(mpiiofs)//"stat/stat_2D_hit"
!
!  ! Initialize FORTRAN interface (lets you use HDF5)
!  call h5open_f(error)
!  if(error.eq.-1) print*,'open error'
!  ! Create file to write into, H5F_FILE_ACCESS_F means file will be overwritten if it exists
!  call h5pcreate_f(H5P_FILE_ACCESS_F, plist_id, error)
!if(error.eq.-1) print*,'p list create error'
!  ! Tell HDF5 about current MPI configuration
!  call h5pset_fapl_mpio_f(plist_id, comm, mpi_info, error)
!if(error.eq.-1) print*,'set mpi error'
!  call h5fcreate_f(filename,H5F_ACC_TRUNC_F,file_id,error,access_prp=plist_id)
!if(error.eq.-1) print*,'file create error'
!
!  ! Create the Stats Group, /HITStatsData
!  call h5gcreate_f(file_id, "HITStatsData", maingroup_id, error)
!if(error.eq.-1) print*,'create main file group error'
!
!
!  ! Create the /mytime group, having the time, Delta_t, and and simulation cycles
!  ! FIGURE OUT THE H5AWRITE COMMAND!!!
!
!  call h5gcreate_f(file_id, "mytime",mytime_id,error)
!  call h5screate_f(H5S_SIMPLE_F,mytime_id,error)
!  call h5tcopy_f(H5T_C_S1, achar_id,error)
!  call h5tset_size_f(achar_id,attrlen,error)
!  ! Denote that this is time data
!  call h5acreate_f(mytimedat_id,"vsType",achar_id,mytime_id,attr_id,error)
!  !call h5awrite_f(attr_id,H5T_NATIVE_CHARACTER,"time",attr_dims(1),error)
!  ! Include Delta_t
!  call h5acreate_f(mytimedat_id,"vsTime",H5T_NATIVE_DOUBLE,mytime_id,attr_id,error)
!  !call h5awrite_f(attr_id,H5T_NATIVE_REAL,Delta_t,attr_dims(1),error)
!  ! Include simulation cycle number
!  call h5acreate_f(mytime_id,"vsStep",H5T_NATIVE_INTEGER,mytime_id,attr_id,error)
!  !call h5awrite_f(attr_id,H5T_NATIVE_INTEGER,ntime,attr_dims(1),error)
!  ! Close attribute, dataspace, group
!  call h5aclose_f(attr_id,error)
!  call h5sclose_f(mytime_id,error)
!
!
!
!  do q = 1, stat_nvar
!   ! Create individual groups for each statistic being created and write dataset to it
!   write(buffer,'(A)') stat_name(q)
!   call h5gcreate_f(maingroup_id, buffer , group_id(q), error)
!if(error.eq.-1) print*,'named group create error', buffer
!   call h5screate_f(H5S_SIMPLE_F,space_id(q), error)
!if(error.eq.-1) print*,'data space create error', buffer
!   call h5dcreate_f(group_id(q),buffer,H5T_NATIVE_DOUBLE, space_id(q),dset(q),error)
!if(error.eq.-1) print*,'create database error', buffer
!
!    do l = 1, attr_max
!      write(attr_buffer, '(A)') attr_data(l)
!      call h5acreate_f(dset(q),aname(l),achar_id,attr_space(l),attr_id,error)
!      !call h5awrite_f(attr_id,achar_id,attr_buffer,attr_dims(1),error)
!      call h5aclose_f(attr_id,error)
!    end do
!
!    call h5dwrite_f(dset(q),H5T_NATIVE_DOUBLE,buf(:,:,q),dims,error)
!if(error.eq.-1) print*,'write data error', buffer
!    call h5dclose_f(dset(q), error)
!if(error.eq.-1) print*,'close data error', buffer
!    call h5sclose_f(space_id(q), error)
!if(error.eq.-1) print*,'close data space error', buffer
!    call h5gclose_f(group_id(q),error)
!if(error.eq.-1) print*,'close group error', buffer
!  end do
!  call h5gclose_f(maingroup_id,error)
!  call h5fclose_f(file_id, error)
!if(error.eq.-1) print*,'close file error'
!call h5pclose_f(plist_id, error)
!if(error.eq.-1) print*,' close p list error'
!  call h5close_f(error)
!if(error.eq.-1) print*,'close error error'
!
!  return
!end subroutine stat_2d_hit_write_hdf5
!
!
!
