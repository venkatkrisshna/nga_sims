module dump_visit
  use parallel
  use precision
  use geometry
  use partition
  use masks
  use fileio
  use memory
  
  ! Modules to dump
  use combustion
  use multiphase
  use multiphase_vof
  use multiphase_velocity
  use multiphase_ehd
  use multiphase_fluxes
  use multiphase_curv3L
  use compgeom_lookup
  use lpt
  use strainrate
  use metric_velocity_visc
  use interpolate
  use ib
  use dump_struct, only : ID
  use dump_structID, only : LsID
  
  implicit none

  ! For SILO_10.2, use f9x
  include "silo_f9x.inc"
  !include "silo.inc"

  ! Time info
  integer :: nout_time
  
  ! Output variables
  integer :: output_n,output_max,output_nSC
  character(len=str_medium), dimension(:),   allocatable :: output_types
  character(len=str_medium), dimension(:),   allocatable :: output_names
  character(len=str_short),  dimension(:,:), allocatable :: output_compNames
  
  ! Silo database
  integer :: silo_comm,silo_irank,Nproc_node
  integer, dimension(:), allocatable :: group_ids,proc_nums
  real(WP), dimension(:,:),   allocatable ::  spatial_extents, myspatial_extents
  real(WP), dimension(:,:),   allocatable :: Uspatial_extents,Umyspatial_extents
  real(WP), dimension(:,:),   allocatable :: Vspatial_extents,Vmyspatial_extents
  real(WP), dimension(:,:),   allocatable :: Wspatial_extents,Wmyspatial_extents
  real(WP), dimension(:,:,:,:), allocatable :: data_extents,   mydata_extents
  character(len=60), dimension(:), allocatable :: names
  integer, dimension(:), allocatable :: lnames,types
  
  ! Mesh 
  integer :: nGhost
  integer :: nxout,nyout,nzout
  integer :: noverxmin_out,noverymin_out,noverzmin_out
  integer :: noverxmax_out,noverymax_out,noverzmax_out
  integer :: imin_out,jmin_out,kmin_out
  integer :: imax_out,jmax_out,kmax_out
  integer, dimension(3) :: lo_offset, hi_offset
  
  ! Buffers
  real(SP), dimension(:,:,:,:), allocatable :: out_buf
  
  ! Particles
  logical :: write_partmesh
  real(SP), dimension(:,:), allocatable :: Part_buf

  ! Interface
  logical  :: write_interface
  integer  :: nNodes,nZones
  integer,  dimension(:), allocatable :: nodeList
  real(SP), dimension(:), allocatable :: xNode,yNode,zNode,ind
  integer, dimension(:,:), allocatable :: zoneIndex
  integer :: nZoneAllocated
  real(SP), dimension(:), allocatable :: interface_buf

  ! Submesh
  logical :: write_submesh
  integer :: nxsub,nysub,nzsub
  integer :: imin_sub,imax_sub,jmin_sub,jmax_sub,kmin_sub,kmax_sub
  real(SP), dimension(:,:,:,:), allocatable :: sub_buf
  real(SP), dimension(:), allocatable :: xsub,ysub,zsub
    
contains
    
  ! ======================================= !
  !  Data to write to file                  !
  !   - Add data to write here              !
  !   - Include name of case in input file  !
  !      e.g. Visit outputs : Velocity      !
  ! ======================================= !
  subroutine dump_visit_getData(n)
    implicit none
    integer, intent(in) :: n
    integer :: i,j,k
    
    select case(trim(output_names(n)))
       
    case ('ID') 
       output_types(n)='scalar'
       out_buf(:,:,:,1)=real(ID(:,:,:),SP)

    ! ============ Core Variables =====================

    ! Velocity field
    case ('Velocity') 
       output_types(n)='vector'
       output_compNames(:,n)=(/ 'Ui', 'Vi', 'Wi' /)
       out_buf(:,:,:,1)=real(Ui,SP)
       out_buf(:,:,:,2)=real(Vi,SP)
       out_buf(:,:,:,3)=real(Wi,SP)
       
    ! Face velocity field
    case ('Uface') 
       output_types(n)='scalar_Ucell'
       out_buf(:,:,:,1)=real(U(:,:,:),SP)
     case ('Vface') 
       output_types(n)='scalar_Vcell'
       out_buf(:,:,:,1)=real(V(:,:,:),SP)
     case ('Wface') 
       output_types(n)='scalar_Wcell'
       out_buf(:,:,:,1)=real(W(:,:,:),SP)

    ! Velocity gradient
    case ('gradU')
       output_types(n)='vector_Ucell'
       output_compNames(:,n)=(/ 'gradUx','gradUy','gradUz'/)
       out_buf(:,:,:,1)=real(gradU(1,:,:,:),SP)
       out_buf(:,:,:,2)=real(gradU(2,:,:,:),SP)
       out_buf(:,:,:,3)=real(gradU(3,:,:,:),SP)
    case ('gradV')
       output_types(n)='vector_Vcell'
       output_compNames(:,n)=(/ 'gradVx','gradVy','gradVz'/)
       out_buf(:,:,:,1)=real(gradV(1,:,:,:),SP)
       out_buf(:,:,:,2)=real(gradV(2,:,:,:),SP)
       out_buf(:,:,:,3)=real(gradV(3,:,:,:),SP)
    case ('gradW')
       output_types(n)='vector_Wcell'
       output_compNames(:,n)=(/ 'gradWx','gradWy','gradWz'/)
       out_buf(:,:,:,1)=real(gradW(1,:,:,:),SP)
       out_buf(:,:,:,2)=real(gradW(2,:,:,:),SP)
       out_buf(:,:,:,3)=real(gradW(3,:,:,:),SP)
       
       
    ! Viscosity
    case ('visc') 
       output_types(n)='scalar'
       out_buf(:,:,:,1)=real(visc,SP)
       
    ! Pressure
    case ('P')
       output_types(n)='scalar'
       out_buf(:,:,:,1)=real(P,SP)
       
    ! Divergence
    case ('Divg') ! Divergence
       output_types(n)='scalar'
       out_buf=0.0_WP
       out_buf(imin_:imax_,jmin_:jmax_,kmin_:kmax_,1)=real(divg,SP)
       
    ! Q-criterion
    case ('Qcrit')
       output_types(n)='scalar'
       out_buf(:,:,:,1)=real(Qcrit,SP)
       
    ! i,j,k
    case ('Index')
       output_types(n)='vector'
       output_compNames(:,n)=(/ 'i', 'j', 'k' /) 
       do k=kmino_,kmaxo_
          do j=jmino_,jmaxo_
             do i=imino_,imaxo_
                out_buf(i,j,k,1)=real(i,SP)
                out_buf(i,j,k,2)=real(j,SP)
                out_buf(i,j,k,3)=real(k,SP)
             end do
          end do
       end do

    ! Velocity residual
    case ('ResU') 
       output_types(n)='scalar_Ucell'
       out_buf(imin_:imax_,jmin_:jmax_,kmin_:kmax_,1)=real(ResU(:,:,:),SP)
    case ('ResV')
       output_types(n)='scalar_Vcell'
       out_buf(imin_:imax_,jmin_:jmax_,kmin_:kmax_,1)=real(ResV(:,:,:),SP)
    case ('ResW')
       output_types(n)='scalar_Wcell'
       out_buf(imin_:imax_,jmin_:jmax_,kmin_:kmax_,1)=real(ResW(:,:,:),SP)

    ! Source terms
    case ('srcUmid')
       output_types(n)='scalar_Ucell'
       out_buf(:,:,:,1)=real(srcUmid(:,:,:),SP)
    case ('srcVmid')
       output_types(n)='scalar_Vcell'
       out_buf(:,:,:,1)=real(srcVmid(:,:,:),SP)
    case ('srcWmid')
       output_types(n)='scalar_Wcell'
       out_buf(:,:,:,1)=real(srcWmid(:,:,:),SP)

    case ('srcmid')
       output_types(n)='vector'
       output_compNames(:,n)=(/ 'srcUmidi', 'srcVmidi', 'srcWmidi' /) 
       do k=kmino_,kmaxo_-1
          do j=jmino_,jmaxo_-1
             do i=imino_,imaxo_-1
                out_buf(i,j,k,1)=real(0.5_WP*(srcUmid(i,j,k)+srcUmid(i+1,j,k)),SP)
                out_buf(i,j,k,2)=real(0.5_WP*(srcVmid(i,j,k)+srcVmid(i,j+1,k)),SP)
                out_buf(i,j,k,3)=real(0.5_WP*(srcWmid(i,j,k)+srcWmid(i,j,k+1)),SP)
             end do
          end do
       end do

    ! Dynamic viscosity
    case ('Visc')
       output_types(n)='scalar'
       out_buf(:,:,:,1)=real(VISC,SP)
    case ('Visc_xy')
       output_types(n)='scalar_Vcell'
       do k=kmin_,kmax_
          do j=jmin_,jmax_
             do i=imin_,imax_
                out_buf(i,j,k,1)=real(sum(interp_sc_xy(i,j,:,:)*VISC(i-st2:i+st1,j-st2:j+st1,k)),SP)
             end do
          end do
       end do

    ! Mask
    case ('Mask')
       output_types(n)='scalar'
       do k=kmino_,kmaxo_
          do j=jmino_,jmaxo_
             do i=imino_,imaxo_
                out_buf(i,j,k,1)=real(mask(i,j),SP)
             end do
          end do
       end do

    ! Immersed boundaries
    case ('IB') 
       output_types(n)='scalar'
       out_buf(:,:,:,1)=real(Gib,SP)
    
    ! ============ Combustion variables ====================

    ! Density field
    case ('RHO') 
       output_types(n)='scalar'
       out_buf(:,:,:,1)=real(RHO(:,:,:),SP)
       
    ! ZMIX
    case ('ZMIX') 
       output_types(n)='scalar'
       out_buf(:,:,:,1)=real(SC(:,:,:,isc_ZMIX),SP)
    
    ! Temperature field
    !case ('Temperature')
    !   output_types(n)='scalar'
    !   out_buf(:,:,:,1)=real(T(:,:,:),SP)

    ! ============ Geometric Transport =====================

    ! Liquid volume fraction
    case ('VOF')
       if (.not.use_multiphase) call die('Must use multiphase for VOF VisIt output')
       output_types(n)='scalar_subcell'
       call subVar2buf(1,VOF(:,imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out) &
            ,imin_out,imax_out,jmin_out,jmax_out,kmin_out,kmax_out)

    case ('VOF_U')
       if (.not.use_multiphase) call die('Must use multiphase for VOF_U VisIt output')
       output_types(n)='scalar_Ucell'
       out_buf(:,:,:,1)=real(VOF_U(:,:,:),SP)
    case ('VOF_V')
       if (.not.use_multiphase) call die('Must use multiphase for VOF_V VisIt output')
       output_types(n)='scalar_Vcell'
       out_buf(:,:,:,1)=real(VOF_V(:,:,:),SP)
    case ('VOF_W')
       if (.not.use_multiphase) call die('Must use multiphase for VOF_W VisIt output')
       output_types(n)='scalar_Wcell'
       out_buf(:,:,:,1)=real(VOF_W(:,:,:),SP)
       
    ! Liquid volume fraction (average)
    case ('VOFavg') 
       if (.not.use_multiphase) call die('Must use multiphase for VOF VisIt output')
       output_types(n)='scalar'
       out_buf(:,:,:,1)=real(VOFavg,SP)

    ! Distance in PLIC plane equation
    case ('Dist')
       if (.not.use_multiphase) call die('Must use multiphase for VOF VisIt output')
       output_types(n)='scalar_subcell'
       call subVar2buf(1,dist(:,imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out) &
            ,imin_out,imax_out,jmin_out,jmax_out,kmin_out,kmax_out)

    ! Interface normal vector (subcell)
    case ('Norm')
       if (.not.use_multiphase) call die('Must use multiphase for VOF VisIt output')
       output_types(n)='vector_subcell'
       output_compNames(:,n)=(/ 'normx', 'normy', 'normz' /) 
       call subVar2buf(1,normx(:,imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out) &
            ,imin_out,imax_out,jmin_out,jmax_out,kmin_out,kmax_out)

       call subVar2buf(2,normy(:,imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out) &
            ,imin_out,imax_out,jmin_out,jmax_out,kmin_out,kmax_out)

       call subVar2buf(3,normz(:,imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out) &
            ,imin_out,imax_out,jmin_out,jmax_out,kmin_out,kmax_out)


    ! Semi-Lagrangian fluxes
    case ('SLF_vol')
       if (.not.use_multiphase) call die('Must use multiphase for VOF VisIt output')
       output_types(n)='vector_subcell'
       output_compNames(:,n)=(/ 'SLF_volx', 'SLF_voly', 'SLF_volz' /) 
       call subVar2buf(1,SLF_volx(:,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1) &
            ,imin_-1,imax_+1,jmin_-1,jmax_+1,kmin_-1,kmax_+1)
       call subVar2buf(2,SLF_voly(:,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1) &
            ,imin_-1,imax_+1,jmin_-1,jmax_+1,kmin_-1,kmax_+1)
       call subVar2buf(3,SLF_volz(:,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1) &
            ,imin_-1,imax_+1,jmin_-1,jmax_+1,kmin_-1,kmax_+1)

    case ('SLF_gas')
       if (.not.use_multiphase) call die('Must use multiphase for VOF VisIt output')
       output_types(n)='vector_subcell'
       output_compNames(:,n)=(/ 'SLF_gasx', 'SLF_gasy', 'SLF_gasz' /) 
       call subVar2buf(1,SLF_gasx(:,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1) &
            ,imin_-1,imax_+1,jmin_-1,jmax_+1,kmin_-1,kmax_+1)
       call subVar2buf(2,SLF_gasy(:,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1) &
            ,imin_-1,imax_+1,jmin_-1,jmax_+1,kmin_-1,kmax_+1)
       call subVar2buf(3,SLF_gasz(:,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1) &
            ,imin_-1,imax_+1,jmin_-1,jmax_+1,kmin_-1,kmax_+1)
    case ('SLF_liq')
       if (.not.use_multiphase) call die('Must use multiphase for VOF VisIt output')
       output_types(n)='vector_subcell'
       output_compNames(:,n)=(/ 'SLF_liqx', 'SLF_liqy', 'SLF_liqz' /) 
       call subVar2buf(1,SLF_liqx(:,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1) &
            ,imin_-1,imax_+1,jmin_-1,jmax_+1,kmin_-1,kmax_+1)
       call subVar2buf(2,SLF_liqy(:,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1) &
            ,imin_-1,imax_+1,jmin_-1,jmax_+1,kmin_-1,kmax_+1)
       call subVar2buf(3,SLF_liqz(:,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1) &
            ,imin_-1,imax_+1,jmin_-1,jmax_+1,kmin_-1,kmax_+1)

    ! cmask
    case ('cmask')
       if (.not.use_multiphase) call die('Must use multiphase for cmask VisIt output')
       output_types(n)='vector_subcell'
       output_compNames(:,n)=(/ 'cmask_x', 'cmask_y', 'cmask_z' /)
       call subVar2buf(1,cmask_x(:,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1) &
            ,imin_-1,imax_+1,jmin_-1,jmax_+1,kmin_-1,kmax_+1)
       call subVar2buf(2,cmask_y(:,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1) &
            ,imin_-1,imax_+1,jmin_-1,jmax_+1,kmin_-1,kmax_+1)
       call subVar2buf(3,cmask_z(:,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1) &
            ,imin_-1,imax_+1,jmin_-1,jmax_+1,kmin_-1,kmax_+1)
       
    ! Band around interface
    case ('Band')
       if (.not.use_multiphase) call die('Must use multiphase for Band VisIt output')
       output_types(n)='scalar'
       out_buf(:,:,:,1)=real(band,SP)
       
    ! Density in velocity cells
    case ('rho_U')
       if (.not.use_multiphase) call die('Must use multiphase for rho_U VisIt output')
       output_types(n)='scalar_Ucell'
       out_buf(:,:,:,1)=real(rho_U,SP)
    case ('rho_V')
       if (.not.use_multiphase) call die('Must use multiphase for rho_V VisIt output')
       output_types(n)='scalar_Vcell'
       out_buf(:,:,:,1)=real(rho_V,SP)
    case ('rho_W')
       if (.not.use_multiphase) call die('Must use multiphase for rho_W VisIt output')
       output_types(n)='scalar_Wcell'
       out_buf(:,:,:,1)=real(rho_W,SP)

    ! Curvature
    case ('Curv')
       if (.not.use_multiphase) call die('Must use multiphase for Curv VisIt output')
       output_types(n)='scalar'
       out_buf(:,:,:,1)=real(curv,SP)
    case ('CurvU')
       if (.not.use_multiphase) call die('Must use multiphase for CurvU VisIt output')
       output_types(n)='scalar_Ucell'
       out_buf(:,:,:,1)=real(curvU,SP)
    case ('CurvV')
       if (.not.use_multiphase) call die('Must use multiphase for CurvV VisIt output')
       output_types(n)='scalar_Vcell'
       out_buf(:,:,:,1)=real(curvV,SP)
    case ('CurvW')
       if (.not.use_multiphase) call die('Must use multiphase for CurvW VisIt output')
       output_types(n)='scalar_Wcell'
       out_buf(:,:,:,1)=real(curvW,SP)
    case ('CurvFlag')
       if (.not.use_multiphase) call die('Must use multiphase for CurvFlag VisIt output')
       output_types(n)='scalar'
       out_buf(:,:,:,1)=real(curvFlag,SP)

       case ('hasCurv')
       if (.not.use_multiphase) call die('Must use multiphase for CurvFlag VisIt output')
       output_types(n)='scalar'
       out_buf(:,:,:,1)=real(hasCurv,SP)

    ! Level set
    ! case ('F')
    !    if (.not.use_multiphase) call die('Must use multiphase for Curv VisIt output')
    !    output_types(n)='scalar'
    !    out_buf(:,:,:,1)=0.0_SP
    !    do k=kmino_,kmaxo_
    !       do j=jmino_,jmaxo_
    !          do i=imino_,imaxo_
    !             do nn=1,nmon
    !                out_buf(i,j,k,1)=out_buf(i,j,k,1) + real(aout(nn) &
    !                     *(xm(i)-xpout)**mono(nn,1) &
    !                     *(ym(j)-ypout)**mono(nn,2) &
    !                     *(zm(k)-zpout)**mono(nn,3),SP)
    !             end do
    !          end do
    !       end do
    !    end do
       
    ! Gas-Liquid interface
    case ('Interface')
       if (.not.use_multiphase) call die('Must use multiphase for Interface VisIt output')
       output_types(n)='interface'
       call VOF2Mesh
       write_interface=.true.

    ! LsID on interface mesh
    case ('LsID_int')
       if (.not.write_interface) call die("Must output 'Interface' before LsID_int")
       output_types(n)='scalar_interface'
       do i=1,nZones
          interface_buf(i)=real(LsID(zoneIndex(1,i),zoneIndex(2,i),zoneIndex(3,i)),SP)
       end do

    ! Subcell divergence
    case ('SCdiv')
       if (.not.use_multiphase) call die('Must use multiphase for SCdiv VisIt output')
       output_types(n)='scalar_subcell'
       call subVar2buf(1,SCdiv(:,imin_-1:imax_+1,jmin_-1:jmax_+1,kmin_-1:kmax_+1) &
            ,imin_-1,imax_+1,jmin_-1,jmax_+1,kmin_-1,kmax_+1)


    ! ============ Level Set Method =====================
    
    ! Level set
    case ('G')
       if (.not.use_multiphase) call die('Must use multiphase for G VisIt output')
       output_types(n)='scalar'
       out_buf(:,:,:,1)=real(G,SP)     
       
    ! ============ Lagrangian Particle Tracking =====================

    ! Particle ID, position,velocity
    case ('PartVel') 
       if (.not.write_partmesh) then
          allocate(part_buf(npart_,3))
          write_partmesh=.true.
       end if
       output_types(n)='PartVector'
       output_compNames(:,n)=(/'partU','partV','partW'/)
       part_buf(:,1)=real(part(1:npart_)%U,SP)
       part_buf(:,2)=real(part(1:npart_)%V,SP)
       part_buf(:,3)=real(part(1:npart_)%W,SP)

    ! Particle angular velocity
    case ('AngVel')
       if (.not.write_partmesh) then
          allocate(part_buf(npart_,3))
          write_partmesh=.true.
       end if
       output_types(n)='PartVector'
       output_compNames(:,n)=(/'partWx','partWy','partWz'/)
       part_buf(:,1)=real(part(1:npart_)%Wx,SP)
       part_buf(:,2)=real(part(1:npart_)%Wy,SP)
       part_buf(:,3)=real(part(1:npart_)%Wz,SP)
       
    ! Particle id
    case ('Part-id') 
      if (.not.write_partmesh) then
          allocate(part_buf(npart_,3))
          write_partmesh=.true.
       end if
       output_types(n)='PartScalar'
      part_buf(:,1)=real(part(1:npart_)%id,SP)
       
    ! Particle diameter
    case ('Diam') 
       if (.not.write_partmesh) then
          allocate(part_buf(npart_,3))
          write_partmesh=.true.
       end if
       output_types(n)='PartScalar'
       part_buf(:,1)=real(part(1:npart_)%D,SP)
       
    ! Particle volume fraction
    case ('Vfp')
       if (.not.use_lpt) call die('Must use LPT for Vfp VisIt output')
       output_types(n)='scalar'
       out_buf(:,:,:,1)=real(1.0_WP-epsf,SP)   

    ! ============ Electohydrodynamics (EHD) =====================
    
    ! Electric potential
    case ('phi')
       if (.not.use_ehd) call die('Must use EHD for phi VisIt output')
       output_types(n)='scalar'
       out_buf(:,:,:,1)=real(phi,SP)

    ! Electric field
    case ('Ex')
       if (.not.use_ehd) call die('Must use EHD for Ex VisIt output')
       output_types(n)='scalar_Ucell'
       out_buf(:,:,:,1)=real(Ex,SP)
    case ('Ey')
       if (.not.use_ehd) call die('Must use EHD for Ey VisIt output')
       output_types(n)='scalar_Vcell'
       out_buf(:,:,:,1)=real(Ey,SP)
    case ('Ez')
       if (.not.use_ehd) call die('Must use EHD for Ez VisIt output')
       output_types(n)='scalar_Wcell'
       out_buf(:,:,:,1)=real(Ez,SP)

    case ('E')
       if (.not.use_ehd) call die('Must use EHD for E VisIt output')
       output_types(n)='vector'
       output_compNames(:,n)=(/'Exi','Eyi','Ezi'/)
       do k=kmino_,kmaxo_-1
          do j=jmino_,jmaxo_-1
             do i=imino_,imaxo_-1
                out_buf(i,j,k,1)=real(0.5_WP*(Ex(i,j,k)+Ex(i+1,j,k)),SP)
                out_buf(i,j,k,2)=real(0.5_WP*(Ey(i,j,k)+Ey(i,j+1,k)),SP)
                out_buf(i,j,k,3)=real(0.5_WP*(Ez(i,j,k)+Ez(i,j,k+1)),SP)
             end do
          end do
       end do

    case ('DIFFx')
       if (.not.use_ehd) call die('Must use EHD for DIFFx VisIt output')
       output_types(n)='scalar_Ucell'
       out_buf(imin_-st1:imax_+st2,jmin_-st1:jmax_+st2,kmin_-st1:kmax_+st2,1)=real(DIFFx,SP)
    case ('DIFFy')
       if (.not.use_ehd) call die('Must use EHD for DIFFy VisIt output')
       output_types(n)='scalar_Vcell'
       out_buf(imin_-st1:imax_+st2,jmin_-st1:jmax_+st2,kmin_-st1:kmax_+st2,1)=real(DIFFy,SP)
    case ('DIFFz')
       if (.not.use_ehd) call die('Must use EHD for DIFFz VisIt output')
       output_types(n)='scalar_Wcell'
       out_buf(imin_-st1:imax_+st2,jmin_-st1:jmax_+st2,kmin_-st1:kmax_+st2,1)=real(DIFFz,SP)
       
       
    ! Unknown output
    case default
       call die('Unknown Visit output: '//output_names(n))
    end select
    
    return
  end subroutine dump_visit_getData
  
end module dump_visit

! ======================================================================================================== !
! ======================================================================================================== !
!        You do not have to modify the code below this point to add a variable on a pre-defined mesh       !
! ======================================================================================================== !
! ======================================================================================================== !

! ======================================== !
!  Dump Visit Silo files - Initialization  !
! ======================================== !
subroutine dump_visit_init
  use dump_visit
  use parser
  use time_info
  implicit none
  integer :: i,ierr,iunit,nchar
  logical :: file_exists
  character(len=5)  :: tmpchar
  character(len=60) :: tmpchar2
  character(len=22) :: buffer
  real(WP) :: current_time
  real(WP),dimension(:),allocatable :: visit_times
  
  ! Create & Start the timer
  call timing_create('visit')
  call timing_start ('visit')
  
  ! Check for arts.visit file and create folder if needed
  if (irank.eq.iroot) then
     inquire(file='Visit/arts.visit',exist=file_exists)
     if (file_exists) then
        ! Get number of lines in file
        iunit = iopen()
        open(iunit,file="Visit/arts.visit",form="formatted",iostat=ierr,status='old')  
        nout_time=0
        do
           read(iunit,*,end=1)
           nout_time=nout_time+1
        end do
1       continue
        close(iclose(iunit))        
        ! Read the file and keep times less than current time
        allocate(visit_times(nout_time))
        iunit = iopen()
        open(iunit,file="Visit/arts.visit",form="formatted",iostat=ierr,status='old')
        ! Get current time (formatted correctly)
        write(buffer,'(ES12.5)') time-dt*1e-10_WP
        read(buffer,*) current_time
        do i=1,nout_time
           ! Read file
           read(iunit,'(5A,60A)') tmpchar,tmpchar2
           ! Extract time from string
           nchar=len_trim(tmpchar2)
           read(tmpchar2(1:nchar-11),*) visit_times(i)
           ! Check if it is in the future and exit if true
           if (visit_times(i).ge.current_time) then
              nout_time=i-1
              exit
           end if
        end do
        close(iclose(iunit))        
        ! Write new file with only past times
        iunit = iopen()
        open(iunit,file="Visit/arts.visit",form="formatted",iostat=ierr,status='replace')  
        do i=1,nout_time
           write(tmpchar2,'(ES12.5)') visit_times(i)
           tmpchar2='time_'//trim(adjustl(tmpchar2))//'/Visit.silo'
           write(iunit,'(A)') trim(adjustl(tmpchar2))
        end do
        deallocate(visit_times)
        close(iclose(iunit))        
     else
        nout_time=0
        ! Create visit directory
        if (irank.eq.iroot) call CREATE_FOLDER("Visit")
     end if
  end if
  
  ! Number of procs per file
  call parser_read('Processors per silo',Nproc_node,12)
  
  ! Create group_ids 
  allocate(group_ids(nproc))
  do i=1,nproc
     group_ids(i)=ceiling(real(i,WP)/real(Nproc_node,WP))
  end do
  ! Create proc_nums
  allocate(proc_nums(nproc))
  do i=1,nproc
     proc_nums(i)=mod(i-1,Nproc_node)+1
  end do
  
  ! Split procs into new communicators
  call MPI_COMM_SPLIT(COMM,group_ids(irank),MPI_UNDEFINED,silo_comm,ierr)
  call MPI_COMM_RANK(silo_comm,silo_irank,ierr)
  silo_irank=silo_irank+1
  
  ! Parse variables to output
  call parser_getsize('Visit outputs',output_n)
  ! Number of scalars to output
  output_nSC=nscalar
  allocate(output_names(output_n+output_nSC))
  call parser_readchararray('Visit outputs',output_names(1:output_n))
  
  ! Allocate arrays
  allocate(out_buf(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3))
  output_max=3*output_n+output_nSC
  allocate(output_types(output_max))
  allocate(output_compNames(3,output_n))
  allocate(  spatial_extents(6,nproc)); allocate( myspatial_extents(6,nproc))
  allocate( Uspatial_extents(6,nproc)); allocate(Umyspatial_extents(6,nproc))
  allocate( Vspatial_extents(6,nproc)); allocate(Vmyspatial_extents(6,nproc))
  allocate( Wspatial_extents(6,nproc)); allocate(Wmyspatial_extents(6,nproc))
  allocate(  data_extents(2,nproc,3,output_max)) 
  allocate(mydata_extents(2,nproc,3,output_max))
  allocate(names (nproc))
  allocate(lnames(nproc))
  allocate(types (nproc))
  
  ! Create output limits because Visit doesn't support external ghost cells
  nGhost=min(2,nover-1)
  noverxmin_out=nGhost; noverxmax_out=nGhost; imin_out=imin_-nGhost; imax_out=imax_+nGhost
  noverymin_out=nGhost; noverymax_out=nGhost; jmin_out=jmin_-nGhost; jmax_out=jmax_+nGhost
  noverzmin_out=nGhost; noverzmax_out=nGhost; kmin_out=kmin_-nGhost; kmax_out=kmax_+nGhost
  if (iproc.eq.1  ) then; noverxmin_out=0; imin_out=imin_; end if
  if (jproc.eq.1  ) then; noverymin_out=0; jmin_out=jmin_; end if
  if (kproc.eq.1  ) then; noverzmin_out=0; kmin_out=kmin_; end if
  if (iproc.eq.npx) then; noverxmax_out=0; imax_out=imax_; end if
  if (jproc.eq.npy) then; noverymax_out=0; jmax_out=jmax_; end if
  if (kproc.eq.npz) then; noverzmax_out=0; kmax_out=kmax_; end if
  nxout=imax_out-imin_out+1
  nyout=jmax_out-jmin_out+1
  nzout=kmax_out-kmin_out+1
  hi_offset(1)=noverxmax_out
  hi_offset(2)=noverymax_out
  hi_offset(3)=noverzmax_out
  lo_offset(1)=noverxmin_out
  lo_offset(2)=noverymin_out
  lo_offset(3)=noverzmin_out

  ! Initialize logicals
  write_interface = .false.
  write_submesh   = .false.
  
  call timing_stop('visit')
  
  return 
end subroutine dump_visit_init


! ===================================== !
!  Dump Visit Silo files - Write Files  !
! ===================================== !
subroutine dump_visit_data
  use dump_visit
  use time_info 
  implicit none
  integer :: i,n,out
  integer :: dbfile,err,ierr,optlist,iunit
  character(len=str_medium) :: med_buffer
  character(len=5) :: dirname
  character(len=38) :: siloname
  character(len=22) :: folder,buffer
  logical :: file_exists

  call timing_start ('visit')

  ! Update output counter
  nout_time=nout_time+1

  ! Make foldername
  write(buffer,'(ES12.5)') time
  folder = 'Visit/time_'//trim(adjustl(buffer))

  if (irank.eq.iroot) then
     ! Create new directory
     call CREATE_FOLDER(trim(folder))
  end if
  call MPI_BARRIER(COMM,ierr)

  !  Create the group silo databases 
  ! =============================================

  ! Create the silo name for this processor
  write(siloname,'(A,I5.5,A)') folder//'/group',group_ids(irank),'.silo'

  ! Create the silo database
  if (silo_irank.eq.1) then
     err = dbcreate(siloname, len_trim(siloname), DB_CLOBBER, DB_LOCAL,"Silo database created with NGA", 30, DB_HDF5, dbfile)
     if(dbfile.eq.-1) call die('Could not create Silo file!')
     ierr = dbclose(dbfile)
  end if

  ! Initialize partmesh logicals
  write_partmesh  = .false.
  
  !  Write the data
  ! =============================================
  do n=1,Nproc_node
     if (n.eq.silo_irank) then

        ! Open group silo file 
        err = dbopen(siloname,len_trim(siloname),DB_HDF5,DB_APPEND,dbfile)

        ! Create the processor directory within silo file
        write(dirname,'(I5.5)') proc_nums(irank)
        err = dbmkdir(dbfile,dirname,5,ierr)
        err = dbsetdir(dbfile,dirname,5)

        ! Write the variables
        mydata_extents=0.0_WP
        do out=1,output_n

           ! Get data to write
           call dump_visit_getData(out)
           
           ! Write data to group silo
           call dump_visit_writeData
              
        end do

        ! Write simulation scalars to group silo
        call dump_visit_writeScalars
        
        ! Write meshes to group silo
        call dump_visit_writeMeshes

        ! Close group silo file
        ierr = dbclose(dbfile)

     end if
     call MPI_BARRIER(silo_comm,ierr)
  end do

  ! Deallocate particle buffers (if needed)
  if (write_partmesh) deallocate(part_buf)

  ! Communicate extents of meshes and data
  call MPI_ALLREDUCE( myspatial_extents, spatial_extents,6*nproc,MPI_REAL_WP,MPI_SUM,COMM,ierr)
  call MPI_ALLREDUCE(Umyspatial_extents,Uspatial_extents,6*nproc,MPI_REAL_WP,MPI_SUM,COMM,ierr)
  call MPI_ALLREDUCE(Vmyspatial_extents,Vspatial_extents,6*nproc,MPI_REAL_WP,MPI_SUM,COMM,ierr)
  call MPI_ALLREDUCE(Wmyspatial_extents,Wspatial_extents,6*nproc,MPI_REAL_WP,MPI_SUM,COMM,ierr)
  call MPI_ALLREDUCE(mydata_extents,data_extents,2*nproc*3*output_max,MPI_REAL_WP,MPI_SUM,COMM,ierr)

  ! Create the master silo database (.visit file, multimesh, multivars)
  ! ===============================================================================
  if (irank.eq.iroot) then
     ! Append .visit file
     iunit = iopen()
     inquire(file="Visit/arts.visit",exist=file_exists)
     if (file_exists) then
        open(iunit,file="Visit/arts.visit",form="formatted",status="old",position="append",action="write")
     else
        open(iunit,file="Visit/arts.visit",form="formatted",status="new",action="write")
     end if
     write(iunit,'(A)') folder(7:)//'/Visit.silo'
     close(iclose(iunit))
     
     ! Set length of names
     err = dbset2dstrlen(len(names))
     
     ! Create the master silo database
     err = dbcreate(folder//'/Visit.silo', len_trim(folder//'/Visit.silo'), DB_CLOBBER, DB_LOCAL, &
          "Silo database created with NGA", 30, DB_HDF5, dbfile)
     if(dbfile.eq.-1) call die('Could not create Silo file!')

     ! Write multiMeshes to master silo 
     call dump_visit_writeMultiMeshes

     !  Write the multivars to master silo 
     call dump_visit_writeMultiVars

     ! Close master silo database
     ierr = dbclose(dbfile)

  end if
  
  call timing_stop('visit')
  
  return

contains 
  
  ! ======================= !
  ! Write data to silo file !
  ! ======================= !
  subroutine dump_visit_writeData
    implicit none

    ! Write data based on type
    select case(trim(output_types(out)))

    case ('scalar') ! ---------------------------------------------------------------------------------------
       ! Write the scalar
       med_buffer=output_names(out)
       err = dbputqv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
            "Mesh", 4, out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out,1), &
            (/nxout,nyout,nzout/), 3, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT,DB_F77NULL, ierr)
       mydata_extents(1,irank,1,out)=minval(out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out,1))
       mydata_extents(2,irank,1,out)=maxval(out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out,1))

    case ('scalar_Ucell') ! ---------------------------------------------------------------------------------
       ! Write the scalar
       med_buffer=output_names(out)
       err = dbputqv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
            "UMesh", 5, out_buf(imin_out:imax_out+1,jmin_out:jmax_out,kmin_out:kmax_out,1), &
            (/nxout+1,nyout,nzout/), 3, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT,DB_F77NULL, ierr)
       mydata_extents(1,irank,1,out)=minval(out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out,1))
       mydata_extents(2,irank,1,out)=maxval(out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out,1))

    case ('scalar_Vcell') ! ---------------------------------------------------------------------------------
       ! Write the scalar
       med_buffer=output_names(out)
       err = dbputqv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
            "VMesh", 5, out_buf(imin_out:imax_out,jmin_out:jmax_out+1,kmin_out:kmax_out,1), &
            (/nxout,nyout+1,nzout/), 3, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT,DB_F77NULL, ierr)
       mydata_extents(1,irank,1,out)=minval(out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out,1))
       mydata_extents(2,irank,1,out)=maxval(out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out,1))

    case ('scalar_Wcell') ! ---------------------------------------------------------------------------------
       ! Write the scalar
       med_buffer=output_names(out)
       err = dbputqv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
            "WMesh", 5, out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out+1,1), &
            (/nxout,nyout,nzout+1/), 3, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT,DB_F77NULL, ierr)
       mydata_extents(1,irank,1,out)=minval(out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out,1))
       mydata_extents(2,irank,1,out)=maxval(out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out,1))

    case ('scalar_subcell') ! ---------------------------------------------------------------------------------------
       ! Write the scalar
       med_buffer=output_names(out)
       err = dbputqv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
            "subMesh", 7, sub_buf(imin_sub:imax_sub,jmin_sub:jmax_sub,kmin_sub:kmax_sub,1), &
            (/nxsub,nysub,nzsub/), 3, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT,DB_F77NULL, ierr)
       mydata_extents(1,irank,1,out)=minval(sub_buf(imin_sub:imax_sub,jmin_sub:jmax_sub,kmin_sub:kmax_sub,1))
       mydata_extents(2,irank,1,out)=maxval(sub_buf(imin_sub:imax_sub,jmin_sub:jmax_sub,kmin_sub:kmax_sub,1))

    case ('vector') ! ---------------------------------------------------------------------------------------
       ! Write the components
       do i=1,3
          med_buffer=output_compNames(i,out)
          err = dbputqv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
               "Mesh", 4, out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out,i), &
               (/nxout,nyout,nzout/), 3, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT,DB_F77NULL, ierr)
          mydata_extents(1,irank,i,out)=minval(out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out,i))
          mydata_extents(2,irank,i,out)=maxval(out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out,i))
       end do
       ! Write the expression
       med_buffer='{'//output_compNames(1,out)//','//output_compNames(2,out)//','//output_compNames(3,out)//'}'
       err = dbputdefvars(dbfile,'defvars',7,1,output_names(out),len_trim(output_names(out)), &
            DB_VARTYPE_VECTOR,med_buffer,len_trim(med_buffer),DB_F77NULL,ierr)

    case ('vector_Ucell') ! -----------------------------------------------------------------------------------
       ! Write the components
       do i=1,3
          med_buffer=output_compNames(i,out)
          err = dbputqv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
               "UMesh", 5, out_buf(imin_out:imax_out+1,jmin_out:jmax_out,kmin_out:kmax_out,i), &
               (/nxout+1,nyout,nzout/), 3, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT,DB_F77NULL, ierr)
          mydata_extents(1,irank,i,out)=minval(out_buf(imin_out:imax_out+1,jmin_out:jmax_out,kmin_out:kmax_out,i))
          mydata_extents(2,irank,i,out)=maxval(out_buf(imin_out:imax_out+1,jmin_out:jmax_out,kmin_out:kmax_out,i))
       end do
       ! Write the expression
       med_buffer='{'//output_compNames(1,out)//','//output_compNames(2,out)//','//output_compNames(3,out)//'}'
       err = dbputdefvars(dbfile,'defvars',7,1,output_names(out),len_trim(output_names(out)), &
            DB_VARTYPE_VECTOR,med_buffer,len_trim(med_buffer),DB_F77NULL,ierr)

    case ('vector_Vcell') ! ------------------------------------------------------------------------------------
       ! Write the components
       do i=1,3
          med_buffer=output_compNames(i,out)
          err = dbputqv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
               "VMesh", 5, out_buf(imin_out:imax_out,jmin_out:jmax_out+1,kmin_out:kmax_out,i), &
               (/nxout,nyout+1,nzout/), 3, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT,DB_F77NULL, ierr)
          mydata_extents(1,irank,i,out)=minval(out_buf(imin_out:imax_out,jmin_out:jmax_out+1,kmin_out:kmax_out,i))
          mydata_extents(2,irank,i,out)=maxval(out_buf(imin_out:imax_out,jmin_out:jmax_out+1,kmin_out:kmax_out,i))
       end do
       ! Write the expression
       med_buffer='{'//output_compNames(1,out)//','//output_compNames(2,out)//','//output_compNames(3,out)//'}'
       err = dbputdefvars(dbfile,'defvars',7,1,output_names(out),len_trim(output_names(out)), &
            DB_VARTYPE_VECTOR,med_buffer,len_trim(med_buffer),DB_F77NULL,ierr)

    case ('vector_Wcell') ! ------------------------------------------------------------------------------------
       ! Write the components
       do i=1,3
          med_buffer=output_compNames(i,out)
          err = dbputqv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
               "WMesh", 5, out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out+1,i), &
               (/nxout,nyout,nzout+1/), 3, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT,DB_F77NULL, ierr)
          mydata_extents(1,irank,i,out)=minval(out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out+1,i))
          mydata_extents(2,irank,i,out)=maxval(out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out+1,i))
       end do
       ! Write the expression
       med_buffer='{'//output_compNames(1,out)//','//output_compNames(2,out)//','//output_compNames(3,out)//'}'
       err = dbputdefvars(dbfile,'defvars',7,1,output_names(out),len_trim(output_names(out)), &
            DB_VARTYPE_VECTOR,med_buffer,len_trim(med_buffer),DB_F77NULL,ierr)

    case ('vector_subcell') ! ---------------------------------------------------------------------------------------
       ! Write the components
       do i=1,3
          med_buffer=output_compnames(i,out)
          err = dbputqv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
               "subMesh", 7, sub_buf(imin_sub:imax_sub,jmin_sub:jmax_sub,kmin_sub:kmax_sub,i), &
               (/nxsub,nysub,nzsub/), 3, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT,DB_F77NULL, ierr)
          mydata_extents(1,irank,i,out)=minval(sub_buf(imin_sub:imax_sub,jmin_sub:jmax_sub,kmin_sub:kmax_sub,i))
          mydata_extents(2,irank,i,out)=maxval(sub_buf(imin_sub:imax_sub,jmin_sub:jmax_sub,kmin_sub:kmax_sub,i))
       end do
       ! Write the expression
       med_buffer='{'//output_compNames(1,out)//','//output_compNames(2,out)//','//output_compNames(3,out)//'}'
       err = dbputdefvars(dbfile,'defvars',7,1,output_names(out),len_trim(output_names(out)), &
            DB_VARTYPE_VECTOR,med_buffer,len_trim(med_buffer),DB_F77NULL,ierr)

    case ('PartScalar') ! --------------------------------------------------------------------------------------
       ! Write the particle scalar
       med_buffer=output_names(out)
       err = dbputpv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
            "PartMesh", 8, Part_buf(1:npart_,1),npart_,DB_FLOAT,DB_F77NULL, ierr)
       mydata_extents(1,irank,1,out)=minval(Part_buf(1:npart_,1))
       mydata_extents(2,irank,1,out)=maxval(Part_buf(1:npart_,1))

    case ('PartVector') ! --------------------------------------------------------------------------------------
       ! Write the particle vector
       do i=1,3
          med_buffer=output_compNames(i,out)
          err = dbputpv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
               "PartMesh", 8, Part_buf(1:npart_,i),npart_,DB_FLOAT,DB_F77NULL, ierr)
          mydata_extents(1,irank,1,out)=minval(Part_buf(1:npart_,i))
          mydata_extents(2,irank,1,out)=maxval(Part_buf(1:npart_,i))
       end do

    case ('scalar_interface') ! ---------------------------------------------------------------------------------------
       ! Write values on interface
       med_buffer=output_names(out)
       err = dbputuv1(dbfile,trim(med_buffer),len_trim(med_buffer),"Interface",9,interface_buf(1:nZones),nZones &
            ,DB_F77NULL,0,DB_FLOAT,DB_ZONECENT,DB_F77NULL, ierr)
       mydata_extents(1,irank,1,out)=minval(interface_buf(1:nZones))
       mydata_extents(2,irank,1,out)=maxval(interface_buf(1:nZones))

    case ('interface') ! ---------------------------------------------------------------------------------------
       ! Do Nothing

    case default ! ---------------------------------------------------------------------------------------------
       call die('output_type not defined correctly for '//trim(output_names(out))//' in dump_visit_writeData')
    end select

    return
  end subroutine dump_visit_writeData

  ! ========================= !
  ! Write NGA scalars to silo !
  ! ========================= !
  subroutine dump_visit_writeScalars
    implicit none
    integer :: out
    ! Write the simulation scalars
    do out=1,output_nSC
       output_types(output_n+out)='scalar'
       output_names(output_n+out)=SC_name(out)
       out_buf(:,:,:,1)=real(SC(:,:,:,out),SP)
       med_buffer=output_names(output_n+out)
       err = dbputqv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
            "Mesh", 4, out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out,1), &
            (/nxout,nyout,nzout/), 3, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT,DB_F77NULL, ierr)
       mydata_extents(1,irank,1,output_n+out)=minval(out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out,1))
       mydata_extents(2,irank,1,output_n+out)=maxval(out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out,1))
    end do
    return
  end subroutine dump_visit_writeScalars

  ! ==================== !
  ! Write meshes to silo !
  ! ==================== !
  subroutine dump_visit_writeMeshes
    implicit none
    integer :: i,j,k
    real(WP), dimension(:,:,:), pointer :: Xmesh,Ymesh,Zmesh

    if (icyl.eq.1) then ! Cylindrical

       ! Compute the coordiantes of every grid point
       Xmesh => tmp1
       Ymesh => tmp2
       Zmesh => tmp3
       do k=kmin_out-1,kmax_out+1
          do j=jmin_out-1,jmax_out+1
             do i=imin_out-1,imax_out+1
                Xmesh(i,j,k)=x(i)
                Ymesh(i,j,k)=y(j)*cos(z(k))
                Zmesh(i,j,k)=y(j)*sin(z(k))
             end do
          end do
       end do
       
       ! Write the regular mesh
       err = dbmkoptlist(2, optlist)
       err = dbaddiopt(optlist, DBOPT_HI_OFFSET, hi_offset)
       err = dbaddiopt(optlist, DBOPT_LO_OFFSET, lo_offset)
       err = dbputqm(dbfile,"Mesh",4,'xc',2,'yc',2,'zc',2, &
            real(Xmesh(imin_out:imax_out+1,jmin_out:jmax_out+1,kmin_out:kmax_out+1),SP), &
            real(Ymesh(imin_out:imax_out+1,jmin_out:jmax_out+1,kmin_out:kmax_out+1),SP), &
            real(Zmesh(imin_out:imax_out+1,jmin_out:jmax_out+1,kmin_out:kmax_out+1),SP), &
            (/nxout+1,nyout+1,nzout+1/),3,DB_FLOAT,DB_NONCOLLINEAR,optlist,ierr)
       err = dbputqm(dbfile,"UMesh",5,'xc',2,'yc',2,'zc',2, &
            real(Xmesh(imin_out-1:imax_out+1,jmin_out:jmax_out+1,kmin_out:kmax_out+1),SP), &
            real(Ymesh(imin_out-1:imax_out+1,jmin_out:jmax_out+1,kmin_out:kmax_out+1),SP), &
            real(Zmesh(imin_out-1:imax_out+1,jmin_out:jmax_out+1,kmin_out:kmax_out+1),SP), &
            (/nxout+2,nyout+1,nzout+1/),3,DB_FLOAT,DB_NONCOLLINEAR,optlist,ierr)
       err = dbputqm(dbfile,"VMesh",5,'xc',2,'yc',2,'zc',2, &
            real(Xmesh(imin_out:imax_out+1,jmin_out-1:jmax_out+1,kmin_out:kmax_out+1),SP), &
            real(Ymesh(imin_out:imax_out+1,jmin_out-1:jmax_out+1,kmin_out:kmax_out+1),SP), &
            real(Zmesh(imin_out:imax_out+1,jmin_out-1:jmax_out+1,kmin_out:kmax_out+1),SP), &
            (/nxout+1,nyout+2,nzout+1/),3,DB_FLOAT,DB_NONCOLLINEAR,optlist,ierr)
       err = dbputqm(dbfile,"WMesh",5,'xc',2,'yc',2,'zc',2, &
            real(Xmesh(imin_out:imax_out+1,jmin_out:jmax_out+1,kmin_out-1:kmax_out+1),SP), &
            real(Ymesh(imin_out:imax_out+1,jmin_out:jmax_out+1,kmin_out-1:kmax_out+1),SP), &
            real(Zmesh(imin_out:imax_out+1,jmin_out:jmax_out+1,kmin_out-1:kmax_out+1),SP), &
            (/nxout+1,nyout+1,nzout+2/),3,DB_FLOAT,DB_NONCOLLINEAR,optlist,ierr)
       err = dbfreeoptlist(optlist)

       
       ! Spatial extents (written in master silo's multimesh)
       myspatial_extents=0.0_WP
       myspatial_extents(1:3,irank)=(/x(imin_out  ), &
            minval(Ymesh(imin_out,jmin_out:jmax_out+1,kmin_out:kmax_out+1)), &
            minval(Zmesh(imin_out,jmin_out:jmax_out+1,kmin_out:kmax_out+1)) /)
       myspatial_extents(4:6,irank)=(/x(imax_out+1), &
            maxval(Ymesh(imin_out,jmin_out:jmax_out+1,kmin_out:kmax_out+1)), &
            maxval(Zmesh(imin_out,jmin_out:jmax_out+1,kmin_out:kmax_out+1)) /)

       Umyspatial_extents=0.0_WP
       Umyspatial_extents(1:3,irank)=(/x(imin_out-1), &
            minval(Ymesh(imin_out,jmin_out:jmax_out+1,kmin_out:kmax_out+1)), &
            minval(Zmesh(imin_out,jmin_out:jmax_out+1,kmin_out:kmax_out+1)) /)
       Umyspatial_extents(4:6,irank)=(/x(imax_out  ), &
            maxval(Ymesh(imin_out,jmin_out:jmax_out+1,kmin_out:kmax_out+1)), &
            maxval(Zmesh(imin_out,jmin_out:jmax_out+1,kmin_out:kmax_out+1)) /)

       Vmyspatial_extents=0.0_WP
       Vmyspatial_extents(1:3,irank)=(/x(imin_out  ), &
            minval(Ymesh(imin_out,jmin_out-1:jmax_out,kmin_out:kmax_out+1)), &
            minval(Zmesh(imin_out,jmin_out-1:jmax_out,kmin_out:kmax_out+1)) /)
       Vmyspatial_extents(4:6,irank)=(/x(imax_out+1), &
            maxval(Ymesh(imin_out,jmin_out-1:jmax_out,kmin_out:kmax_out+1)), &
            maxval(Zmesh(imin_out,jmin_out-1:jmax_out,kmin_out:kmax_out+1)) /)

       Wmyspatial_extents=0.0_WP
       Wmyspatial_extents(1:3,irank)=(/x(imin_out  ), &
            minval(Ymesh(imin_out,jmin_out:jmax_out+1,kmin_out-1:kmax_out)), &
            minval(Zmesh(imin_out,jmin_out:jmax_out+1,kmin_out-1:kmax_out)) /)
       Wmyspatial_extents(4:6,irank)=(/x(imax_out+1), &
            maxval(Ymesh(imin_out,jmin_out:jmax_out+1,kmin_out-1:kmax_out)), &
            maxval(Zmesh(imin_out,jmin_out:jmax_out+1,kmin_out-1:kmax_out)) /)
       
    else ! Cartesian
       
       ! Write the regular mesh
       err = dbmkoptlist(2, optlist)
       err = dbaddiopt(optlist, DBOPT_HI_OFFSET, hi_offset)
       err = dbaddiopt(optlist, DBOPT_LO_OFFSET, lo_offset)
       err = dbputqm(dbfile,"Mesh",4,'xc',2,'yc',2,'zc',2, &
            real(x(imin_out:imax_out+1),SP),real(y(jmin_out:jmax_out+1),SP),real(z(kmin_out:kmax_out+1),SP), &
            (/nxout+1,nyout+1,nzout+1/),3,DB_FLOAT,DB_COLLINEAR,optlist,ierr)
       err = dbputqm(dbfile,"UMesh",5,'xc',2,'yc',2,'zc',2, &
            real(xm(imin_out-1:imax_out+1),SP),real(y(jmin_out:jmax_out+1),SP),real(z(kmin_out:kmax_out+1),SP), &
            (/nxout+2,nyout+1,nzout+1/),3,DB_FLOAT,DB_COLLINEAR,optlist,ierr)
       err = dbputqm(dbfile,"VMesh",5,'xc',2,'yc',2,'zc',2, &
            real(x(imin_out:imax_out+1),SP),real(ym(jmin_out-1:jmax_out+1),SP),real(z(kmin_out:kmax_out+1),SP), &
            (/nxout+1,nyout+2,nzout+1/),3,DB_FLOAT,DB_COLLINEAR,optlist,ierr)
       err = dbputqm(dbfile,"WMesh",5,'xc',2,'yc',2,'zc',2, &
            real(x(imin_out:imax_out+1),SP),real(y(jmin_out:jmax_out+1),SP),real(zm(kmin_out-1:kmax_out+1),SP), &
            (/nxout+1,nyout+1,nzout+2/),3,DB_FLOAT,DB_COLLINEAR,optlist,ierr)
       err = dbfreeoptlist(optlist)
       
       ! Spatial extents (written in master silo's multimesh)
       myspatial_extents=0.0_WP
       myspatial_extents(1:3,irank)=(/x(imin_out  ),y(jmin_out  ),z(kmin_out  )/)
       myspatial_extents(4:6,irank)=(/x(imax_out+1),y(jmax_out+1),z(kmax_out+1)/)

       Umyspatial_extents=0.0_WP
       Umyspatial_extents(1:3,irank)=(/xm(imin_out-1),y(jmin_out  ),z(kmin_out  )/)
       Umyspatial_extents(4:6,irank)=(/xm(imax_out  ),y(jmax_out+1),z(kmax_out+1)/)

       Vmyspatial_extents=0.0_WP
       Vmyspatial_extents(1:3,irank)=(/x(imin_out  ),ym(jmin_out-1),z(kmin_out  )/)
       Vmyspatial_extents(4:6,irank)=(/x(imax_out+1),ym(jmax_out  ),z(kmax_out+1)/)

       Wmyspatial_extents=0.0_WP
       Wmyspatial_extents(1:3,irank)=(/x(imin_out  ),y(jmin_out  ),zm(kmin_out-1)/)
       Wmyspatial_extents(4:6,irank)=(/x(imax_out+1),y(jmax_out+1),zm(kmax_out  )/)

       ! Write particle mesh
       if (write_partmesh) then
          err = dbputpm(dbfile,"PartMesh",8,3,real(part(1:npart_)%x,SP), &
               real(part(1:npart_)%y,SP),real(part(1:npart_)%z,SP),npart_,DB_FLOAT,DB_F77NULL,ierr)
       end if

       ! Write interface mesh
       if (write_interface) then
          ! Write interface as unstructured mesh made of tetrahedra
          err = dbputzl2(dbfile,"zonelist",8,nZones,3,nodeList,3*nZones,1,0,0 &
               ,DB_ZONETYPE_TRIANGLE,3,nZones,1,DB_F77NULL,ierr) 
          err = dbputum(dbfile,"Interface",9,3,xNode(1:nNodes),yNode(1:nNodes),zNode(1:nNodes) &
               ,"xInt",4,"yInt",4,"zInt",4,DB_FLOAT,nNodes,nZones,"zonelist",8,DB_F77NULL,0,DB_F77NULL,ierr)
          deallocate(xNode,yNode,zNode,nodeList)
       end if

       ! Write the sub mesh
       if (write_submesh) then
          err = dbmkoptlist(2, optlist)
          err = dbaddiopt(optlist, DBOPT_HI_OFFSET, hi_offset*2) ! 2 subcells in each direction
          err = dbaddiopt(optlist, DBOPT_LO_OFFSET, lo_offset*2)
          err = dbputqm(dbfile,"subMesh",7,'xc',2,'yc',2,'zc',2, &
               xsub(imin_sub:imax_sub+1),ysub(jmin_sub:jmax_sub+1),zsub(kmin_sub:kmax_sub+1), &
               (/nxsub+1,nysub+1,nzsub+1/),3,DB_FLOAT,DB_COLLINEAR,optlist,ierr)
       end if

    end if
    
    return
  end subroutine dump_visit_writeMeshes

  ! ===================================== !
  ! Write multiMeshes to silo file        !
  ! (Information on all the group meshes) !
  ! ===================================== !
  subroutine dump_visit_writeMultiMeshes 
    implicit none

    integer :: mesh_type

    if (icyl.eq.1) then
       mesh_type=DB_QUAD_CURV
    else
       mesh_type=DB_QUAD_RECT
    end if

    !  Write the multimeshes
    ! =======================
    do i=1,nproc
       write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/Mesh'
       lnames(i)=len_trim(names(i))
       types(i)=mesh_type
    end do
    err = dbmkoptlist(4,optlist)
    err = dbaddiopt(optlist, DBOPT_DTIME, time);
    err = dbaddiopt(optlist, DBOPT_CYCLE, nout_time);
    err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 6)
    err = dbadddopt(optlist, DBOPT_EXTENTS, spatial_extents)
    err = dbputmmesh(dbfile,  "Mesh", 4, nproc, names,lnames, types, optlist, ierr);
    err = dbfreeoptlist(optlist)

    do i=1,nproc
       write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/UMesh'
       lnames(i)=len_trim(names(i))
       types(i)=mesh_type
    end do
    err = dbmkoptlist(4,optlist)
    err = dbaddiopt(optlist, DBOPT_DTIME, time);
    err = dbaddiopt(optlist, DBOPT_CYCLE, nout_time);
    err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 6)
    err = dbadddopt(optlist, DBOPT_EXTENTS, Uspatial_extents)
    err = dbputmmesh(dbfile,  "UMesh", 5, nproc, names,lnames, types, optlist, ierr)
    err = dbfreeoptlist(optlist)

    do i=1,nproc
       write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/VMesh'
       lnames(i)=len_trim(names(i))
       types(i)=mesh_type
    end do
    err = dbmkoptlist(4,optlist)
    err = dbaddiopt(optlist, DBOPT_DTIME, time);
    err = dbaddiopt(optlist, DBOPT_CYCLE, nout_time);
    err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 6)
    err = dbadddopt(optlist, DBOPT_EXTENTS, Vspatial_extents)
    err = dbputmmesh(dbfile,  "VMesh", 5, nproc, names,lnames, types, optlist, ierr)
    err = dbfreeoptlist(optlist)

    do i=1,nproc
       write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/WMesh'
       lnames(i)=len_trim(names(i))
       types(i)=mesh_type
    end do
    err = dbmkoptlist(4,optlist)
    err = dbaddiopt(optlist, DBOPT_DTIME, time);
    err = dbaddiopt(optlist, DBOPT_CYCLE, nout_time);
    err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 6)
    err = dbadddopt(optlist, DBOPT_EXTENTS, Wspatial_extents)
    err = dbputmmesh(dbfile,  "WMesh", 5, nproc, names,lnames, types, optlist, ierr)
    err = dbfreeoptlist(optlist)

    ! Write particle multimesh
    if (write_partmesh) then
       do i=1,nproc
          write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/PartMesh'
          lnames(i)=len_trim(names(i))
          types(i)=DB_POINTMESH
       end do
       err = dbmkoptlist(4,optlist)
       err = dbaddiopt(optlist, DBOPT_DTIME, time);
       err = dbaddiopt(optlist, DBOPT_CYCLE, nout_time);
       err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 6)
       err = dbadddopt(optlist, DBOPT_EXTENTS, spatial_extents)
       err = dbputmmesh(dbfile, "PartMesh", 8, nproc, names,lnames, types, optlist, ierr)
       err = dbfreeoptlist(optlist)
    end if

    ! Write interface multimesh
    if (write_interface) then
       do i=1,nproc
          write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/Interface'
          lnames(i)=len_trim(names(i))
          types(i)=DB_UCDMESH
       end do
       err = dbmkoptlist(4,optlist)
       err = dbaddiopt(optlist, DBOPT_DTIME, time);
       err = dbaddiopt(optlist, DBOPT_CYCLE, nout_time);
       err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 6)
       err = dbadddopt(optlist, DBOPT_EXTENTS, spatial_extents)
       err = dbputmmesh(dbfile, "Interface", 9, nproc, names,lnames, types, optlist, ierr)
       err = dbfreeoptlist(optlist)
    end if

    ! Write the subMesh multimesh
    if (write_submesh) then
       do i=1,nproc
          write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/subMesh'
          lnames(i)=len_trim(names(i))
          types(i)=mesh_type
       end do
       err = dbmkoptlist(4,optlist)
       err = dbaddiopt(optlist, DBOPT_DTIME, time);
       err = dbaddiopt(optlist, DBOPT_CYCLE, nout_time);
       err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 6)
       err = dbadddopt(optlist, DBOPT_EXTENTS, spatial_extents)
       err = dbputmmesh(dbfile,  "subMesh", 7, nproc, names,lnames, types, optlist, ierr)
       err = dbfreeoptlist(optlist)
    end if

    return
  end subroutine dump_visit_writeMultiMeshes

  ! ================================ !
  ! Write the multiVariables         !
  ! (Information on group variables) !
  ! ================================ !
  subroutine dump_visit_writeMultiVars
    implicit none
    integer :: out

    do out=1,output_n+output_nSC
       select case(trim(output_types(out)))

       case ('scalar','scalar_Ucell','scalar_Vcell','scalar_Wcell','scalar_subcell')
          do i=1,nproc
             write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/'//trim(output_names(out))
             lnames(i)=len_trim(names(i))
             types(i)=DB_QUADVAR
          end do
          err = dbmkoptlist(2, optlist)
          err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 2)
          err = dbadddopt(optlist, DBOPT_EXTENTS, data_extents(:,:,1,out))
          err = dbputmvar(dbfile,output_names(out),len_trim(output_names(out)),nproc,names,lnames,types,optlist,ierr)
          err = dbfreeoptlist(optlist)

       case ('vector','vector_Ucell','vector_Vcell','vector_Wcell','vector_subcell')
          do n=1,3
             do i=1,nproc
                write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/'//trim(output_compNames(n,out))
                lnames(i)=len_trim(names(i))
                types(i)=DB_QUADVAR
             end do
             err = dbmkoptlist(2, optlist)
             err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 2)
             err = dbadddopt(optlist, DBOPT_EXTENTS, data_extents(:,:,n,out))
             err = dbputmvar(dbfile,output_compNames(n,out),len_trim(output_compNames(n,out)), &
                  nproc,names,lnames,types,optlist,ierr)
             err = dbfreeoptlist(optlist)

          end do
          ! Write the expression
          med_buffer='{'//trim(output_compNames(1,out))// &
               ','//trim(output_compNames(2,out))//','//trim(output_compNames(3,out))//'}'
          err = dbputdefvars(dbfile,output_names(out),len_trim(output_names(out)),1, &
               output_names(out),len_trim(output_names(out)), &
               DB_VARTYPE_VECTOR,med_buffer,len_trim(med_buffer),DB_F77NULL,ierr)

       case ('PartScalar')
          do i=1,nproc
             write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/'//trim(output_names(out))
             lnames(i)=len_trim(names(i))
             types(i)=DB_POINTVAR
          end do
          err = dbmkoptlist(2, optlist)
          err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 2)
          err = dbadddopt(optlist, DBOPT_EXTENTS, data_extents(:,:,1,out))
          err = dbputmvar(dbfile,output_names(out),len_trim(output_names(out)),nproc,names,lnames,types,optlist,ierr)
          err = dbfreeoptlist(optlist)

       case ('PartVector')
          do n=1,3
             do i=1,nproc
                write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/'//trim(output_compNames(n,out))
                lnames(i)=len_trim(names(i))
                types(i)=DB_POINTVAR
             end do
             err = dbmkoptlist(2, optlist)
             err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 2)
             err = dbadddopt(optlist, DBOPT_EXTENTS, data_extents(:,:,n,out))
             err = dbputmvar(dbfile,output_compNames(n,out),len_trim(output_compNames(n,out)),&
                  nproc,names,lnames,types,optlist,ierr)
             err = dbfreeoptlist(optlist)
          end do
          ! Write the expression
          med_buffer='{'//trim(output_compNames(1,out))// &
               ','//trim(output_compNames(2,out))//','//trim(output_compNames(3,out))//'}'
          err = dbputdefvars(dbfile,output_names(out),len_trim(output_names(out)),1, &
               output_names(out),len_trim(output_names(out)), &
               DB_VARTYPE_VECTOR,med_buffer,len_trim(med_buffer),DB_F77NULL,ierr)

       case ('scalar_interface')
          do i=1,nproc
             write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/'//trim(output_names(out))
             lnames(i)=len_trim(names(i))
             types(i)=DB_UCDVAR
          end do
          err = dbmkoptlist(2, optlist)
          err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 2)
          err = dbadddopt(optlist, DBOPT_EXTENTS, data_extents(:,:,1,out))
          err = dbputmvar(dbfile,output_names(out),len_trim(output_names(out)),nproc,names,lnames,types,optlist,ierr)
          err = dbfreeoptlist(optlist)

       case ('interface') ! ---------------------------------------------------------------------------------------
          ! Do Nothing

       case default
          call die('output_type not defined correctly for '//trim(output_names(out))//'in multivar write')
       end select

    end do

    return
  end subroutine dump_visit_writeMultiVars

end subroutine dump_visit_data

! ====================================== !
! Creates unstructured mesh on interface !
! ====================================== !
subroutine VOF2Mesh
  use dump_visit
  use multiphase_fluxes
  implicit none
  integer :: i,j,k,ii,jj,kk
  integer :: c,cc,ntet,n,nn,nnn,dim,ntri
  integer :: case,case2,v1,v2
  integer :: nAllocated,nListAllocated
  real(SP), dimension(:), allocatable :: tmpNode
  integer,  dimension(:), allocatable :: tmpNodeList
  integer,  dimension(:,:), allocatable :: tmpZoneIndex
  real(WP), parameter :: VOFlo_visit=0.00001
  real(WP), parameter :: VOFhi_visit=0.99999
  real(WP), dimension(3,8) :: verts
  real(WP), dimension(3,5) :: tverts,tverts2
  real(WP), dimension(3,4) :: mytet
  real(WP), dimension(4) :: d
  real(WP), dimension(3) :: node,cen
  real(WP) :: mu
  real(WP) :: sdx2,sdy2,sdz2

  ! Initialize arrays
  nAllocated    =1000
  nListAllocated=1000
  allocate(xNode   (nAllocated))
  allocate(yNode   (nAllocated))
  allocate(zNode   (nAllocated))
  allocate(nodeList(nListAllocated))
  ! Allocate buffers for interface variables
  if (.not.allocated(zoneIndex)) then
     nZoneAllocated=1000
     allocate(zoneIndex(3,nZoneAllocated))
  end if
  nNodes=0
  nZones=0

  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_

           do c=1,8 ! loop over subcells

              ! Cell center
              cen=(/ xm(i)+subx(c)*dx(i), &
                     ym(j)+suby(c)*dy(j), &
                     zm(k)+subz(c)*dz    /)

              ! Subcell size/2
              sdx2=0.25_WP*dx(i);
              sdy2=0.25_WP*dy(j);
              sdz2=0.25_WP*dz;

              ! Cells with interface
              if (VOF(c,i,j,k).ge.VOFlo_visit.and.VOF(c,i,j,k).le.VOFhi_visit) then

                 ! Cell vertices
                 verts(:,1)=cen+(/-sdx2,-sdy2,-sdz2/)
                 verts(:,2)=cen+(/+sdx2,-sdy2,-sdz2/)
                 verts(:,3)=cen+(/-sdx2,+sdy2,-sdz2/)
                 verts(:,4)=cen+(/+sdx2,+sdy2,-sdz2/)
                 verts(:,5)=cen+(/-sdx2,-sdy2,+sdz2/)
                 verts(:,6)=cen+(/+sdx2,-sdy2,+sdz2/)
                 verts(:,7)=cen+(/-sdx2,+sdy2,+sdz2/)
                 verts(:,8)=cen+(/+sdx2,+sdy2,+sdz2/)

                 ! Loop over tets
                 do ntet=1,5
                    ! Create tet
                    do n=1,4
                       mytet(:,n)=verts(:,verts2tets(n,ntet))
                    end do
                    ! Calculate distance from each vertex to cut plane
                    do n=1,4
                       d(n) = normx(c,i,j,k)*mytet(1,n) &
                            + normy(c,i,j,k)*mytet(2,n) &
                            + normz(c,i,j,k)*mytet(3,n) &
                            - dist(c,i,j,k)
                    end do
                    ! Find cut case
                    case=1+int(0.5_WP+sign(0.5_WP,d(1)))+&
                         2*int(0.5_WP+sign(0.5_WP,d(2)))+&
                         4*int(0.5_WP+sign(0.5_WP,d(3)))+&
                         8*int(0.5_WP+sign(0.5_WP,d(4)))
                    ! Add points on cut plane to nodes
                    do n=1,cut_nvert(case)
                       v1=cut_v1(n,case); v2=cut_v2(n,case)
                       mu=min(1.0_WP,max(0.0_WP,-d(v1)/(sign(abs(d(v2)-d(v1))+epsilon(1.0_WP),d(v2)-d(v1)))))
                       node=(1.0_WP-mu)*mytet(:,v1)+mu*mytet(:,v2)
                       nNodes=nNodes+1
                       xNode(nNodes)=real(node(1),SP)
                       yNode(nNodes)=real(node(2),SP)
                       zNode(nNodes)=real(node(3),SP)
                    end do
                    ! Create node list to tris on interface
                    do n=1,cut_ntris(case)
                       do nn=1,3
                          nodeList(3*nZones+nn)=nNodes-cut_nvert(case)+(cut_vtri(nn,n,case)-4)
                       end do
                       nZones=nZones+1
                       zoneIndex(:,nZones)=(/ i,j,k /)
                    end do
                 end do
              end if

              ! Inherent inferface between cells
              do dim=1,3
                 ! Neighboring cell indices
                 select case(dim)
                 case (1); cc=sc_sx(c); ii=i+sc_im(c); jj=j; kk=k;
                 case (2); cc=sc_sy(c); ii=i; jj=j+sc_jm(c); kk=k;
                 case (3); cc=sc_sz(c); ii=i; jj=j; kk=k+sc_km(c);
                 end select
                 ! Check for inherent interface
                 if ( (VOF(c ,i ,j ,k ).ge.VOFlo_visit.and.VOF(c ,i ,j ,k ).le.VOFhi_visit) .or. & ! - VOF(c ,i ,j ,k ) has interface
                      (VOF(cc,ii,jj,kk).ge.VOFlo_visit.and.VOF(cc,ii,jj,kk).le.VOFhi_visit) .or. & ! - VOF(cc,ii,jj,kk) has interface
                      (VOF(c ,i ,j ,k ).lt.VOFlo_visit.and.VOF(cc,ii,jj,kk).gt.VOFhi_visit) .or. & ! - Interface between VOF(c,i,j,k)
                      (VOF(cc,ii,jj,kk).lt.VOFlo_visit.and.VOF(c ,i ,j ,k ).gt.VOFhi_visit) ) then !    & VOF(cc,ii,jj,kk)
                    ! Cell face vertices
                    select case(dim)
                    case (1)
                       verts(:,1)=cen+(/-sdx2,-sdy2,-sdz2/)
                       verts(:,2)=cen+(/-sdx2,+sdy2,-sdz2/)
                       verts(:,3)=cen+(/-sdx2,-sdy2,+sdz2/)
                       verts(:,4)=cen+(/-sdx2,+sdy2,+sdz2/)
                    case (2)
                       verts(:,1)=cen+(/-sdx2,-sdy2,-sdz2/)
                       verts(:,2)=cen+(/+sdx2,-sdy2,-sdz2/)
                       verts(:,3)=cen+(/-sdx2,-sdy2,+sdz2/)
                       verts(:,4)=cen+(/+sdx2,-sdy2,+sdz2/)
                    case (3)
                       verts(:,1)=cen+(/-sdx2,-sdy2,-sdz2/)
                       verts(:,2)=cen+(/+sdx2,-sdy2,-sdz2/)
                       verts(:,3)=cen+(/-sdx2,+sdy2,-sdz2/)
                       verts(:,4)=cen+(/+sdx2,+sdy2,-sdz2/)
                    end select
                 
                    ! Loop over tris
                    do ntri=1,2
                       ! Create tri
                       do n=1,3
                          tverts(:,n)=verts(:,verts2tris(n,ntri))
                       end do

                       ! Calculate distance to interface
                       if (VOF(c,i,j,k).lt.VOFlo_visit) then
                          d(:)=+1.0_WP
                       else if (VOF(c,i,j,k).gt.VOFhi_visit) then
                          d(:)=-1.0_WP
                       else 
                          d(1:3) = normx(c,i,j,k)*tverts(1,1:3) &
                                 + normy(c,i,j,k)*tverts(2,1:3) &
                                 + normz(c,i,j,k)*tverts(3,1:3) &
                                 - dist (c,i,j,k)
                       end if
                       ! Find cut case
                       case=1+int(0.5_WP+sign(0.5_WP,d(1)))+&
                            2*int(0.5_WP+sign(0.5_WP,d(2)))+&
                            4*int(0.5_WP+sign(0.5_WP,d(3)))
                       ! Add points on cut plane to nodes 
                       do n=1,cutTri_nvert(case)
                          v1=cutTri_v1(n,case); v2=cutTri_v2(n,case)
                          mu=min(1.0_WP,max(0.0_WP,-d(v1)/(sign(abs(d(v2)-d(v1))+epsilon(1.0_WP),d(v2)-d(v1)))))
                          tverts(:,3+n)=(1.0_WP-mu)*tverts(:,v1)+mu*tverts(:,v2)
                       end do

                       ! Cut tris on liquid side by neighboring cell interface
                       do n=cutTri_np(case)+1,cutTri_np(case)+cutTri_nn(case)
                          ! Create liquid tris
                          do nn=1,3
                             tverts2(:,nn)=tverts(:,cutTri_v(nn,n,case))
                          end do
                          ! Compute distances
                          if (VOF(cc,ii,jj,kk).lt.VOFlo_visit) then
                             d(:)=+1.0_WP
                          else if (VOF(cc,ii,jj,kk).gt.VOFhi_visit) then
                             d(:)=-1.0_WP
                          else
                             d(1:3) = normx(cc,ii,jj,kk)*tverts2(1,1:3) &
                                    + normy(cc,ii,jj,kk)*tverts2(2,1:3) &
                                    + normz(cc,ii,jj,kk)*tverts2(3,1:3) &
                                    - dist (cc,ii,jj,kk)
                          end if
                          ! Find cut case
                          case2=1+int(0.5_WP+sign(0.5_WP,d(1)))+&
                                2*int(0.5_WP+sign(0.5_WP,d(2)))+&
                                4*int(0.5_WP+sign(0.5_WP,d(3)))
                          ! Add points on cut plane to nodes 
                          do nn=1,cutTri_nvert(case2)
                             v1=cutTri_v1(nn,case2); v2=cutTri_v2(nn,case2)
                             mu=min(1.0_WP,max(0.0_WP,-d(v1)/(sign(abs(d(v2)-d(v1))+epsilon(1.0_WP),d(v2)-d(v1)))))
                             tverts2(:,3+nn)=(1.0_WP-mu)*tverts2(:,v1)+mu*tverts2(:,v2)
                          end do
                          ! Tris on liquid side of (i,j,k) and gas side of (ii,jj,kk) cut planes
                          do nn=1,cutTri_np(case2)
                             ! Create tris and save in Visit output
                             do nnn=1,3
                                nNodes=nNodes+1
                                xNode(nNodes)=real(tverts2(1,cutTri_v(nnn,nn,case2)),SP)
                                yNode(nNodes)=real(tverts2(2,cutTri_v(nnn,nn,case2)),SP)
                                zNode(nNodes)=real(tverts2(3,cutTri_v(nnn,nn,case2)),SP)
                                nodeList(3*nZones+nnn)=nNodes
                             end do
                             nZones=nZones+1
                             zoneIndex(:,nZones)=(/ i,j,k /)
                          end do
                       end do

                       ! Cut tris on gas side by neighboring cell interface
                       do n=1,cutTri_np(case)
                          ! Transfer verts
                          do nn=1,3
                             tverts2(:,nn)=tverts(:,cutTri_v(nn,n,case))
                          end do
                          ! Compute distances
                          if (VOF(cc,ii,jj,kk).lt.VOFlo_visit) then
                             d(:)=+1.0_WP
                          else if (VOF(cc,ii,jj,kk).gt.VOFhi_visit) then
                             d(:)=-1.0_WP
                          else
                             d(1:3) = normx(cc,ii,jj,kk)*tverts2(1,1:3) &
                                    + normy(cc,ii,jj,kk)*tverts2(2,1:3) &
                                    + normz(cc,ii,jj,kk)*tverts2(3,1:3) &
                                    - dist (cc,ii,jj,kk)
                          end if
                          ! Find cut case
                          case2=1+int(0.5_WP+sign(0.5_WP,d(1)))+&
                                2*int(0.5_WP+sign(0.5_WP,d(2)))+&
                                4*int(0.5_WP+sign(0.5_WP,d(3)))
                          ! Add points on cut plane to nodes 
                          do nn=1,cutTri_nvert(case2)
                             v1=cutTri_v1(nn,case2); v2=cutTri_v2(nn,case2)
                             mu=min(1.0_WP,max(0.0_WP,-d(v1)/(sign(abs(d(v2)-d(v1))+epsilon(1.0_WP),d(v2)-d(v1)))))
                             tverts2(:,3+nn)=(1.0_WP-mu)*tverts2(:,v1)+mu*tverts2(:,v2)
                          end do
                          ! Tris on gas side of (i,j,k) and liquid side of (ii,jj,kk) cut planes
                          do nn=cutTri_np(case2)+1,cutTri_np(case2)+cutTri_nn(case2)
                             ! Create tris and save in Visit output
                             do nnn=1,3
                                nNodes=nNodes+1
                                xNode(nNodes)=real(tverts2(1,cutTri_v(nnn,nn,case2)),SP)
                                yNode(nNodes)=real(tverts2(2,cutTri_v(nnn,nn,case2)),SP)
                                zNode(nNodes)=real(tverts2(3,cutTri_v(nnn,nn,case2)),SP)
                                nodeList(3*nZones+nnn)=nNodes
                             end do
                             nZones=nZones+1
                             zoneIndex(:,nZones)=(/ ii,jj,kk /)
                          end do
                        end do
                    end do
                 end if
              end do

              ! Reallocate arrays if necessary
              if (nAllocated-nNodes.lt.200) then
                 allocate(tmpNode(nAllocated))
                 tmpNode=xNode; deallocate(xNode); allocate(xNode(nAllocated+1000)); xNode(1:nAllocated)=tmpNode
                 tmpNode=yNode; deallocate(yNode); allocate(yNode(nAllocated+1000)); yNode(1:nAllocated)=tmpNode
                 tmpNode=zNode; deallocate(zNode); allocate(zNode(nAllocated+1000)); zNode(1:nAllocated)=tmpNode
                 deallocate(tmpNode)
                 nAllocated=nAllocated+1000
              end if
              if (nListAllocated-3*nZones.lt.200) then
                 allocate(tmpNodeList(nListAllocated))
                 tmpNodeList=nodeList; 
                 deallocate(nodeList); 
                 allocate(nodeList(nListAllocated+1000)); 
                 nodeList(1:nListAllocated)=tmpNodeList
                 deallocate(tmpNodeList)
                 nListAllocated=nListAllocated+1000
              end if
              if (nZoneAllocated-nZones.lt.200) then
                 allocate(tmpZoneIndex(3,nZoneallocated))
                 tmpZoneIndex=zoneIndex
                 deallocate(zoneIndex)
                 allocate(zoneIndex(3,nZoneAllocated+1000))
                 Zoneindex(:,1:nZoneAllocated)=tmpZoneIndex
                 deallocate(tmpZoneIndex)
                 nZoneAllocated=nZoneAllocated+1000
              end if
           end do

        end do
     end do
  end do

  ! Add a zero-area tri if this proc doesn't have one
  if (nZones.eq.0) then
     nZones=1
     nNodes=3
     xNode(1:3)=real(xm(imin_),SP)
     yNode(1:3)=real(ym(jmin_),SP)
     zNode(1:3)=real(zm(kmin_),SP)
     nodeList(1:3)=(/1,2,3/)
     zoneIndex(:,nZones)=(/ imin_,jmin_,kmin_ /)
  end if

  ! Allocate buffer to hold interface variables
  if (allocated(interface_buf)) deallocate(interface_buf)
  allocate(interface_buf(nZones))

end subroutine VOF2Mesh

! ===================================== !
! Variable on subcells to output buffer !
! ===================================== !
subroutine subVar2buf(buf,var,i1,i2,j1,j2,k1,k2)
  use dump_visit
  use multiphase_fluxes
  implicit none
  integer, intent(in) :: i1,i2,j1,j2,k1,k2
  real(WP), dimension(8,i1:i2,j1:j2,k1:k2), intent(in) :: var
  integer, intent(in) :: buf
  integer :: i,j,k,ii,jj,kk,c

  ! Initialize quantities and buffers if first sub-variable 
  if (.not.write_submesh) then

     ! Update logical
     write_submesh=.true.

     ! Index extents & number of sub cells
     imin_sub=sub2i(imin_out,1); imax_sub=sub2i(imax_out,8); nxsub=imax_sub-imin_sub+1
     jmin_sub=sub2j(jmin_out,1); jmax_sub=sub2j(jmax_out,8); nysub=jmax_sub-jmin_sub+1
     kmin_sub=sub2k(kmin_out,1); kmax_sub=sub2k(kmax_out,8); nzsub=kmax_sub-kmin_sub+1

     ! Subcell meshes
     allocate(xsub(imin_sub:imax_sub+1))
     allocate(ysub(jmin_sub:jmax_sub+1))
     allocate(zsub(kmin_sub:kmax_sub+1))

     ! X submesh
     do i=imin_out,imax_out
        xsub(sub2i(i,1))=real(x(i),SP)
        xsub(sub2i(i,8))=real(xm(i),SP)
     end do
     xsub(sub2i(imax_out+1,1))=real(x(imax_out+1),SP)
     ! Y submesh
     do j=jmin_out,jmax_out
        ysub(sub2j(j,1))=real(y(j),SP)
        ysub(sub2j(j,8))=real(ym(j),SP)
     end do
     ysub(sub2j(jmax_out+1,1))=real(y(jmax_out+1),SP)
     ! Z submesh
     do k=kmin_out,kmax_out
        zsub(sub2k(k,1))=real(z(k),SP)
        zsub(sub2k(k,8))=real(zm(k),SP)
     end do
     zsub(sub2k(kmax_out+1,1))=real(z(kmax_out+1),SP)
        
     ! Allocate subVar buffer
     allocate(sub_buf(imin_sub:imax_sub,jmin_sub:jmax_sub,kmin_sub:kmax_sub,3))

  end if

  ! Transfer variable to buffer
  sub_buf(:,:,:,buf)=0.0_WP
  do k=max(k1,kmin_out),min(k2,kmax_out)
     do j=max(j1,jmin_out),min(j2,jmax_out)
        do i=max(i1,imin_out),min(i2,imax_out)
           do c=1,8
              ii=sub2i(i,c); jj=sub2j(j,c); kk=sub2k(k,c)
              sub_buf(ii,jj,kk,buf)=real(var(c,i,j,k),SP)
           end do
        end do
     end do
  end do  

  return
end subroutine subVar2buf



! ================================================ !
! Write n tets to silo file (useful for debugging) !
! - Not paralleized, each proc. can write a file   !
! ================================================ !
subroutine tets2silo(ntets,tets,siloname)
  use dump_visit
  implicit none

  ! Inputs
  integer, intent(in) :: ntets
  real(WP), dimension(ntets,4,3), intent(in) :: tets
  character(len=*), intent(in) :: siloname
  ! Variables
  integer :: dbfile, ierr
  integer :: n,nn
  real(WP) :: tetvol

  ! Create database
  ierr = dbcreate(siloname,len_trim(siloname),DB_CLOBBER,DB_LOCAL,'Tets',4,DB_HDF5,dbfile)
  if (dbfile.eq.-1) call die('Could not create tet silo file in tets2silo')

  ! Allocate arrays
  allocate(nodelist(ntets*4))
  allocate(xNode(ntets*4))
  allocate(yNode(ntets*4))
  allocate(zNode(ntets*4))

  ! Create arrays to write
  nNodes=0; nZones=0;
  do n=1,ntets ! Loop over tets
     tetvol=tet_vol(tets(n,:,:))
     if (tetvol.gt.0.0_WP) then
        nodeList(4*nZones+1:4*nZones+4)=nNodes+(/1,2,3,4/)
     else 
        nodeList(4*nZones+1:4*nZones+4)=nNodes+(/4,2,3,1/)
     end if
     do nn=1,4 ! Loop over nodes
        nNodes=nNodes+1
        xNode(nNodes)=real(tets(n,nn,1),SP)
        yNode(nNodes)=real(tets(n,nn,2),SP)
        zNode(nNodes)=real(tets(n,nn,3),SP)
     end do
     nZones=nZones+1
  end do

  ! Write arrays
  ierr = dbputzl2(dbfile,"zonelist",8,nZones,3,nodeList,nNodes & 
       ,1,0,0,DB_ZONETYPE_TET,4,nZones,1,DB_F77NULL,ierr) 
  ierr = dbputum(dbfile,"tets",4,3,xNode(1:nNodes),yNode(1:nNodes),zNode(1:nNodes) &
       ,"xtet",4,"ytet",4,"ztet",4,DB_FLOAT,nNodes,nZones,"zonelist",8,DB_F77NULL,0,DB_F77NULL,ierr)

  ! Close silo database
  ierr = dbclose(dbfile)

  ! Deallocate arrays
  deallocate(nodelist,xNode,yNode,zNode)

  return
end subroutine tets2silo

! ================================================ !
! Write points to silo file (useful for debugging) !
! - Not paralleized, each proc. can write a file   !
! ================================================ !
subroutine pts2silo(npts,pts,siloname)
  use dump_visit
  implicit none

  ! Inputs
  integer, intent(in) :: npts
  real(WP), dimension(npts,3), intent(in) :: pts
  character(len=*), intent(in) :: siloname
  ! Variables
  integer :: dbfile, ierr

  ! Create database
  ierr = dbcreate(siloname,len_trim(siloname),DB_CLOBBER,DB_LOCAL,'Pts',3,DB_HDF5,dbfile)
  if (dbfile.eq.-1) call die('Could not create tet silo file in pts2silo')

  ! Write points to particle mesh
  ierr = dbputpm(dbfile,"PartMesh",8,3,real(pts(:,1),SP),real(pts(:,2),SP),real(pts(:,3),SP), &
       npts,DB_FLOAT,DB_F77NULL,ierr)

  ! Close silo database
  ierr = dbclose(dbfile)

  return
end subroutine pts2silo

! ============================================== !
! Write vectors at points to silo file           !
! - Not paralleized, each proc. can write a file !
! ============================================== !
subroutine vector2silo(npts,pts,vector,siloname)
  use dump_visit
  implicit none

  ! Inputs
  integer, intent(in) :: npts
  real(WP), dimension(npts,3), intent(in) :: pts
  real(WP), dimension(npts,3), intent(in) :: vector
  character(len=*), intent(in) :: siloname
  ! Variables
  integer :: dbfile, ierr

  ! Create database
  ierr = dbcreate(siloname,len_trim(siloname),DB_CLOBBER,DB_LOCAL,'Pts',3,DB_HDF5,dbfile)
  if (dbfile.eq.-1) call die('Could not create tet silo file in vector2silo')

  ! Write points to particle mesh
  ierr = dbputpm(dbfile,"PartMesh",8,3,real(pts(:,1),SP),real(pts(:,2),SP),real(pts(:,3),SP), &
       npts,DB_FLOAT,DB_F77NULL,ierr)
  if (dbfile.eq.-1) call die('Could not write mesh in vector2silo')


  ! Write components of vector at each point
  ierr = dbputpv1(dbfile,'Vx',2,"PartMesh",8,real(vector(:,1),SP),npts,DB_FLOAT,DB_F77NULL,ierr)
  if (dbfile.eq.-1) call die('Could not write Vx in vector2silo')
  ierr = dbputpv1(dbfile,'Vy',2,"PartMesh",8,real(vector(:,2),SP),npts,DB_FLOAT,DB_F77NULL,ierr)
  if (dbfile.eq.-1) call die('Could not write Vy in vector2silo')
  ierr = dbputpv1(dbfile,'Vz',2,"PartMesh",8,real(vector(:,3),SP),npts,DB_FLOAT,DB_F77NULL,ierr)
  if (dbfile.eq.-1) call die('Could not write Vz in vector2silo')

  ! Write the expression
  ierr = dbputdefvars(dbfile,'defvars',7,1,'Vector',6, &
       DB_VARTYPE_VECTOR,'{Vx,Vy,Vz}',10,DB_F77NULL,ierr)
  if (dbfile.eq.-1) call die('Could not write expression in vector2silo')

  ! Close silo database
  ierr = dbclose(dbfile)

  return
end subroutine vector2silo
