module stat_1d_spray
  use stat
  implicit none
  
  ! Sampled variables
  integer :: stat_nvar
  character(len=str_medium), dimension(:), allocatable :: stat_name
  
  ! Fileviews
  integer :: fileview_x
  integer :: fileview_y
  
  ! ----------------------------------------------------
  ! 1D-Y STATISTICS 
  !  -> as a function of y
  !  -> at a given station in x
  !
  ! nxloc = -1  for no station (x periodic)
  ! xloc belongs to [ x(iloc)  ,x(iloc+1)   [
  ! xloc belongs to [ xm(imloc),xm(imloc+1) [
  ! -----------------------------------------------------
  integer :: nxloc
  real(WP), dimension(:),     allocatable :: xloc
  integer,  dimension(:),     allocatable :: iloc
  integer,  dimension(:),     allocatable :: iloc_ifwrite
  integer,  dimension(:),     allocatable :: imloc
  real(WP), dimension(:,:,:), allocatable :: stat_y
  real(WP), dimension(:,:),   allocatable :: lpt_stat_y
  real(WP), dimension(:,:,:), allocatable :: buf_y
  real(WP), dimension(:,:,:), allocatable :: dt_y
  
  ! -----------------------------------------------------
  ! 1D-X STATISTICS 
  !  -> as a function of x
  !  -> at a given station in y
  !
  ! nyloc = -1  for no station (y periodic)
  ! yloc belongs to [ y(jloc)  ,y(jloc+1)   [
  ! yloc belongs to [ ym(jmloc),ym(jmloc+1) [
  ! -----------------------------------------------------
  integer :: nyloc
  real(WP), dimension(:),     allocatable :: yloc
  integer,  dimension(:),     allocatable :: jloc
  integer,  dimension(:),     allocatable :: jloc_ifwrite
  integer,  dimension(:),     allocatable :: jmloc
  real(WP), dimension(:,:,:), allocatable :: stat_x
  real(WP), dimension(:,:),   allocatable :: lpt_stat_x
  real(WP), dimension(:,:,:), allocatable :: buf_x
  real(WP), dimension(:,:,:), allocatable :: dt_x
  
contains
  
  ! ================================= !
  ! General initialization of STAT-1D !
  ! ================================= !
  subroutine stat_1d_spray_init
    implicit none
    
    ! Allocate if not already done
    if (.not.associated(stat_name)) then
       stat_nvar = 8
       allocate(stat_name(stat_nvar))
       stat_name(1)  = 'number'
       stat_name(2)  = 'dd'
       stat_name(3)  = 'dd^2'
       stat_name(4)  = 'dd^3'
       stat_name(5)  = 'ud'
       stat_name(6)  = 'vd'
       stat_name(7)  = 'wd'
       stat_name(8)  = 'Td'
    end if
    
    return
  end subroutine stat_1d_spray_init
  
  ! ============================== !
  ! Setup the 1D metric            !
  !  -> indices                    !
  ! ============================== !
  subroutine stat_1dx_spray_metric
    implicit none
    integer :: i,loc
    
    ! Allocate the arrays
    allocate(iloc(nxloc))
    allocate(iloc_ifwrite(nxloc))
    allocate(imloc(nxloc))
    
    ! Locate the first i index to the left
    do loc=1,nxloc
       if (x(imin_).gt.xloc(loc) .or. x(imax_+1).le.xloc(loc)) then
          iloc(loc) = imin_
          imloc(loc) = imin_
          iloc_ifwrite(loc) = 0
       else
          i = imin_
          do while(i.le.imax_ .and. x(i+1).le.xloc(loc))
             i = i+1
          end do
          iloc(loc) = i
          if (xloc(loc).ge.xm(i)) then
             imloc(loc) = i
          else
             imloc(loc) = i-1
          end if
          iloc_ifwrite(loc) = 1
       end if
    end do
    
    return
  end subroutine stat_1dx_spray_metric
  
  subroutine stat_1dy_spray_metric
    implicit none
    integer :: j,loc
    
    ! Allocate the arrays
    allocate(jloc(nyloc))
    allocate(jloc_ifwrite(nyloc))
    allocate(jmloc(nyloc))
    
    ! Locate the first j index to the bottom - cell face
    do loc=1,nyloc
       if (y(jmin_).gt.yloc(loc) .or. y(jmax_+1).le.yloc(loc)) then
          jloc(loc) = jmin_
          jmloc(loc) = jmin_
          jloc_ifwrite(loc) = 0
       else
          j = jmin_
          do while(j.le.jmax_ .and. y(j+1).le.yloc(loc))
             j = j+1
          end do
          jloc(loc) = j
          if (yloc(loc).ge.ym(j)) then
             jmloc(loc) = j
          else
             jmloc(loc) = j-1
          end if
          jloc_ifwrite(loc) = 1
       end if
    end do
    
    return
  end subroutine stat_1dy_spray_metric
  
end module stat_1d_spray


! ================================== !
! Initialize the 1D statistic module !
!   -> at a given x                  !
!   -> as a function of y            !
! ================================== !
subroutine stat_1dy_spray_init
  use stat_1d_spray
  use parallel
  use parser
  use lpt
  implicit none
  
  logical :: isdef,file_is_there
  integer :: gsize,lsize,start
  integer :: ierr
  
  ! General 1D non specific initialization
  call stat_1d_spray_init
  
  ! No stat at a given x (as a function of y) if y periodic
  if (yper.eq.1) call die('stat_1dy_spray_init: 1D-y statistics impossible (y is periodic)')
  
  ! Any x locations ?
  call parser_is_defined('Spray statistics locations x',isdef)
  if (isdef) then
     call parser_getsize('Spray statistics locations x',nxloc)
     allocate(xloc(nxloc))
     call parser_read('Spray statistics locations x',xloc)
     call stat_1dx_spray_metric
  else
     call die('stat_1dy_spray_init: x locations not specified')
  end if
  
  ! Allocate stat arrays
  allocate(stat_y(nxloc,jmin_:jmax_,stat_nvar))
  allocate(lpt_stat_y(jmin_:jmax_,stat_nvar))
  allocate(buf_y(nxloc,jmin_:jmax_,stat_nvar))
  allocate(dt_y(nxloc,jmin_:jmax_,stat_nvar))
  stat_y = 0.0_WP
  buf_y = 0.0_WP
  dt_y = 0.0_WP
  
  ! Open file if it exists
  inquire(file='stat/stat-1D-spray-y',exist=file_is_there)
  if (file_is_there) then
     call stat_1dy_spray_read
  end if
  
  ! Prepare the fileview
  gsize = ny
  lsize = ny_
  start = jmin_-jmin
  call MPI_TYPE_CREATE_SUBARRAY(1,gsize,lsize,start,MPI_ORDER_FORTRAN,MPI_REAL_WP,fileview_y,ierr)
  call MPI_TYPE_COMMIT(fileview_y,ierr)
  
  return
end subroutine stat_1dy_spray_init


! ================================== !
! Initialize the 1D statistic module !
!   -> at a given y                  !
!   -> as a function of x            !
! ================================== !
subroutine stat_1dx_spray_init
  use stat_1d_spray
  use parallel
  use parser
  use lpt
  implicit none
  
  logical :: isdef,file_is_there
  integer :: gsize,lsize,start
  integer :: ierr
  
  ! General 1D non specific initialization
  call stat_1d_spray_init
  
  ! No stat at a given y (as a function of x) if x periodic
  if (xper.eq.1) call die('stat_1dx_spray_init: 1D-x statistics impossible (x is periodic)')
  
  ! Any y locations ?
  call parser_is_defined('Spray statistics locations y',isdef)
  if (isdef) then
     call parser_getsize('Spray statistics locations y',nyloc)
     allocate(yloc(nyloc))
     call parser_read('Spray statistics locations y',yloc)
     call stat_1dy_spray_metric
  else 
     call die('stat_1dx_spray_init: y locations not specified')
  end if
  
  ! Allocate stat arrays
  allocate(stat_x(nyloc,imin_:imax_,stat_nvar))
  allocate(lpt_stat_x(imin_:imax_,stat_nvar))
  allocate(buf_x(nyloc,imin_:imax_,stat_nvar))
  allocate(dt_x(nyloc,imin_:imax_,stat_nvar))
  stat_x = 0.0_WP
  buf_x = 0.0_WP
  dt_x = 0.0_WP
  
  ! Open file if it exists
  inquire(file='stat/stat-1D-spray-x',exist=file_is_there)
  if (file_is_there) then
     call stat_1dx_spray_read
  end if
  
  ! Prepare the fileview
  gsize = nx
  lsize = nx_
  start = imin_-imin
  call MPI_TYPE_CREATE_SUBARRAY(1,gsize,lsize,start,MPI_ORDER_FORTRAN,MPI_REAL_WP,fileview_x,ierr)
  call MPI_TYPE_COMMIT(fileview_x,ierr)
  
  return
end subroutine stat_1dx_spray_init


! ======================= !
! Sample the statistics   !
! at the center: xm or ym !
! ======================= !
subroutine stat_1dy_spray_sample
  use stat_1d_spray
  use data
  use combustion
  use parallel
  use lpt
  use memory
  use lpt_cyl
  use time_info
  implicit none
  
  real(WP) :: x_tmp,y_tmp,z_tmp
  real(WP) :: u_tmp,v_tmp,w_tmp
  integer :: ipart,j,loc
  
  ! Gather the stats
  do loc=1,nxloc
     
     ! Loop over the droplets
     lpt_stat_y = 0.0_WP
     do ipart=1,npart_
        ! Test if the droplet just crossed the plane
        if ( part(ipart)%x.ge.xloc(loc) .and. part(ipart)%x-dt*part(ipart)%u.lt.xloc(loc) .OR. &
             part(ipart)%x.lt.xloc(loc) .and. part(ipart)%x-dt*part(ipart)%u.ge.xloc(loc) ) then
           ! Increment a counter per j position
           lpt_stat_y(part(ipart)%j,1) = lpt_stat_y(part(ipart)%j,1) + part(ipart)%nparcel
           ! Add the diameters
           lpt_stat_y(part(ipart)%j,2) = lpt_stat_y(part(ipart)%j,2) + part(ipart)%d**1 *part(ipart)%nparcel
           lpt_stat_y(part(ipart)%j,3) = lpt_stat_y(part(ipart)%j,3) + part(ipart)%d**2 *part(ipart)%nparcel
           lpt_stat_y(part(ipart)%j,4) = lpt_stat_y(part(ipart)%j,4) + part(ipart)%d**3 *part(ipart)%nparcel
           ! Convert the velocity if necessary
           if (icyl.eq.1) then
              call lpt_cart2cyl(part(ipart)%x,part(ipart)%y,part(ipart)%z,x_tmp,y_tmp,&
                   z_tmp,part(ipart)%u,part(ipart)%v,part(ipart)%w,u_tmp,v_tmp,w_tmp)
           else
              u_tmp = part(ipart)%u
              v_tmp = part(ipart)%v
              w_tmp = part(ipart)%w
           end if
           ! Add the velocity
           lpt_stat_y(part(ipart)%j,5) = lpt_stat_y(part(ipart)%j,5) + u_tmp *part(ipart)%nparcel
           lpt_stat_y(part(ipart)%j,6) = lpt_stat_y(part(ipart)%j,6) + v_tmp *part(ipart)%nparcel
           lpt_stat_y(part(ipart)%j,7) = lpt_stat_y(part(ipart)%j,7) + w_tmp *part(ipart)%nparcel
           lpt_stat_y(part(ipart)%j,8) = lpt_stat_y(part(ipart)%j,8) + part(ipart)%T *part(ipart)%nparcel
        end if
     end do
     ! Gather stats
     do j=jmin_,jmax_
        if (lpt_stat_y(j,1).gt.0.0_WP) then
           ! Divide by counter to get a mean(j)
           lpt_stat_y(j,2:) = lpt_stat_y(j,2:)/lpt_stat_y(j,1)
           ! Add the current mean to the stats
           dt_y(loc,j,:) = dt_y(loc,j,:) + dt
           stat_y(loc,j,:) = stat_y(loc,j,:) + dt*lpt_stat_y(j,:)
        end if
     end do
  end do
  
  return
end subroutine stat_1dy_spray_sample

subroutine stat_1dx_spray_sample
  use stat_1d_spray
  use data
  use combustion
  use parallel
  use lpt
  use lpt_cyl
  use memory
  use time_info
  implicit none
  
  integer :: i,loc,ipart
  real(WP) :: x_tmp,y_tmp,z_tmp
  real(WP) :: u_tmp,v_tmp,w_tmp
  
  ! Gather the stats
  do loc=1,nyloc
     
     ! Loop over the droplets
     lpt_stat_x = 0.0_WP
     do ipart=1,npart_
        ! Test if the droplet just crossed the plane
        if ( part(ipart)%j.eq.jloc(loc)) then
           ! Increment a counter per j position
           lpt_stat_x(part(ipart)%i,1) = lpt_stat_x(part(ipart)%i,1) + part(ipart)%nparcel
           ! Add the diameters
           lpt_stat_x(part(ipart)%i,2) = lpt_stat_x(part(ipart)%i,2) + part(ipart)%d**1 *part(ipart)%nparcel
           lpt_stat_x(part(ipart)%i,3) = lpt_stat_x(part(ipart)%i,3) + part(ipart)%d**2 *part(ipart)%nparcel
           lpt_stat_x(part(ipart)%i,4) = lpt_stat_x(part(ipart)%i,4) + part(ipart)%d**3 *part(ipart)%nparcel
           ! Convert the velocity if necessary
           if (icyl.eq.1) then
              call lpt_cart2cyl(part(ipart)%x,part(ipart)%y,part(ipart)%z,x_tmp,y_tmp,&
                   z_tmp,part(ipart)%u,part(ipart)%v,part(ipart)%w,u_tmp,v_tmp,w_tmp)
           else
              u_tmp = part(ipart)%u
              v_tmp = part(ipart)%v
              w_tmp = part(ipart)%w
           end if
           ! Add the velocity
           lpt_stat_x(part(ipart)%i,5) = lpt_stat_x(part(ipart)%i,5) + u_tmp *part(ipart)%nparcel
           lpt_stat_x(part(ipart)%i,6) = lpt_stat_x(part(ipart)%i,6) + v_tmp *part(ipart)%nparcel
           lpt_stat_x(part(ipart)%i,7) = lpt_stat_x(part(ipart)%i,7) + w_tmp *part(ipart)%nparcel
           lpt_stat_x(part(ipart)%i,8) = lpt_stat_x(part(ipart)%i,8) + part(ipart)%T *part(ipart)%nparcel
        end if
     end do
     ! Gather stats
     do i=imin_,imax_
        if (lpt_stat_x(i,1).gt.0.0_WP) then
           ! Divide by counter to get a mean(i)
           lpt_stat_x(i,2:) = lpt_stat_x(i,2:)/lpt_stat_x(i,1)
           ! Add the current mean to the stats
           dt_x(loc,i,:) = dt_x(loc,i,:) + dt
           stat_x(loc,i,:) = stat_x(loc,i,:) + dt*lpt_stat_x(i,:)
        end if
     end do
  end do
  
  return
end subroutine stat_1dx_spray_sample


! ================================= !
! Read the statistics from the disk !
! ================================= !
subroutine stat_1dy_spray_read
  use stat_1d_spray
  use parallel
  implicit none
  
  integer, dimension(3) :: dims
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer(kind=MPI_OFFSET_KIND) :: disp
  character(len=str_medium)  :: name
  character(len=str_medium) :: filename
  integer :: var,ierr,ifile,data_size,loc
  
  ! Open the file to write
  filename = "ufs:stat/stat-1D-spray-y"
  call MPI_FILE_OPEN(comm,filename,MPI_MODE_RDONLY,mpi_info,ifile,ierr)
  
  ! Read dimensions from header
  call MPI_FILE_READ_ALL(ifile,dims,3,MPI_INTEGER,status,ierr)
  if (dims(1).ne.ny .or. dims(3).ne.nxloc) then
     print*, 'expected = ',nxloc,ny
     print*, 'stat = ',dims(3),dims(1)
     call die('stat_1dy_spray_read: The size of the stat file is incorrect')
  end if
  if (dims(2).ne.stat_nvar) call die('stat_1dy_spray_read: Wrong number of variables in stat file')
  
  ! Read variable names
  do var=1,stat_nvar
     call MPI_FILE_READ_ALL(ifile,name,str_medium,MPI_CHARACTER,status,ierr)
     if (name.ne.stat_name(var)) then
        print*,irank,name,stat_name(var)
        call die('stat_1dy_spray_read: Variables names in stat and data files do not correspond')
     end if
  end do
  
  ! Read the delta t
  do loc=1,nxloc
     data_size = ny_
     if (iloc_ifwrite(loc).eq.0) data_size = 0
     do var=1,stat_nvar
        disp = 4*3 + str_medium*stat_nvar + WP*ny*(loc-1)*stat_nvar + WP*ny*(var-1)
        call MPI_FILE_SET_VIEW(ifile,disp,MPI_REAL_WP,fileview_y,"native",mpi_info,ierr)
        call MPI_FILE_READ_ALL(ifile,dt_y(loc,:,var),data_size,MPI_REAL_WP,status,ierr)
     end do
  end do
  
  ! Read the stats
  do loc=1,nxloc
     data_size = ny_
     if (iloc_ifwrite(loc).eq.0) data_size = 0
     do var=1,stat_nvar
        disp = 4*3 + str_medium*stat_nvar + WP*ny*nxloc*stat_nvar + WP*ny*(loc-1)*stat_nvar + WP*ny*(var-1)
        call MPI_FILE_SET_VIEW(ifile,disp,MPI_REAL_WP,fileview_y,"native",mpi_info,ierr)
        call MPI_FILE_READ_ALL(ifile,buf_y(loc,:,var),data_size,MPI_REAL_WP,status,ierr)
     end do
  end do
  
  ! Close the file
  call MPI_FILE_CLOSE(ifile,ierr)
  
  ! Restart the stats
  stat_y = buf_y * dt_y
  
  return
end subroutine stat_1dy_spray_read

subroutine stat_1dx_spray_read
  use stat_1d_spray
  use parallel
  implicit none
  
  integer, dimension(3) :: dims
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer(kind=MPI_OFFSET_KIND) :: disp
  character(len=str_medium)  :: name
  character(len=str_medium) :: filename
  integer :: var,ierr,ifile,data_size,loc
  
  ! Open the file to write
  filename = "ufs:stat/stat-1D-spray-x"
  call MPI_FILE_OPEN(comm,filename,MPI_MODE_RDONLY,mpi_info,ifile,ierr)
  
  ! Read dimensions from header
  call MPI_FILE_READ_ALL(ifile,dims,3,MPI_INTEGER,status,ierr)
  if ((dims(1).ne.nx) .or. (dims(3).ne.nyloc)) then
     print*, 'expected = ',nyloc,nx
     print*, 'stat = ',dims(3),dims(1)
     call die('stat_1dx_spray_read: The size of the stat file is incorrect')
  end if
  if (dims(2).ne.stat_nvar) call die('stat_1dx_spray_read: Wrong number of variables in stat file')
  
  ! Read variable names
  do var=1,stat_nvar
     call MPI_FILE_READ_ALL(ifile,name,str_medium,MPI_CHARACTER,status,ierr)
     if (name.ne.stat_name(var)) then
        call die('stat_1dx_spray_read: Variables names in stat and data files do not correspond')
     end if
  end do
  
  ! Read the delta t
  do loc=1,nyloc
     data_size = nx_
     if (jloc_ifwrite(loc).eq.0) data_size = 0
     do var=1,stat_nvar
        disp = 4*3 + str_medium*stat_nvar + WP*nx*(loc-1)*stat_nvar + WP*nx*(var-1)
        call MPI_FILE_SET_VIEW(ifile,disp,MPI_REAL_WP,fileview_x,"native",mpi_info,ierr)
        call MPI_FILE_READ_ALL(ifile,dt_x(loc,:,var),data_size,MPI_REAL_WP,status,ierr)
     end do
  end do
  
  ! Read the stats
  do loc=1,nyloc
     data_size = nx_
     if (jloc_ifwrite(loc).eq.0) data_size = 0
     do var=1,stat_nvar
        disp = 4*3 + str_medium*stat_nvar + WP*nx*nyloc*stat_nvar + WP*nx*(loc-1)*stat_nvar + WP*nx*(var-1)
        call MPI_FILE_SET_VIEW(ifile,disp,MPI_REAL_WP,fileview_x,"native",mpi_info,ierr)
        call MPI_FILE_READ_ALL(ifile,buf_x(loc,:,var),data_size,MPI_REAL_WP,status,ierr)
     end do
  end do
  
  ! Close the file
  call MPI_FILE_CLOSE(ifile,ierr)
  
  ! Restart the stats
  stat_x = buf_x * dt_x
  
  return
end subroutine stat_1dx_spray_read


! ================================ !
! Write the statistics to the disk !
! ================================ !
subroutine stat_1dy_spray_write
  use stat_1d_spray
  use parallel
  implicit none
  
  integer :: var,loc,ierr,ifile,data_size,j
  integer(kind=MPI_OFFSET_KIND) :: disp
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer, dimension(3) :: dims
  character(len=str_medium) :: filename
  
  ! Divide by the delta t
  do var=1,stat_nvar
     do j=jmin_,jmax_
        do loc=1,nxloc
           if (dt_y(loc,j,var).gt.0.0_WP) then
              buf_y(loc,j,var) = stat_y(loc,j,var) / dt_y(loc,j,var)
           else
              buf_y(loc,j,var) = 0.0_WP
           end if
        end do
     end do
  end do
  
  ! Open the file to write
  filename = "ufs:stat/stat-1D-spray-y"
  if (irank.eq.iroot) call MPI_FILE_DELETE(filename,mpi_info,ierr)
  call MPI_FILE_OPEN(comm,filename,IOR(MPI_MODE_WRONLY,MPI_MODE_CREATE),mpi_info,ifile,ierr)
  
  ! Write the headers
  if (irank.eq.iroot) then
     ! Write dimensions
     dims(1) = ny
     dims(2) = stat_nvar
     dims(3) = nxloc
     call MPI_FILE_WRITE(ifile,dims,3,MPI_INTEGER,status,ierr)
     ! Write variable names
     do var=1,stat_nvar
        call MPI_FILE_WRITE(ifile,stat_name(var),str_medium,MPI_CHARACTER,status,ierr)
     end do
  end if
  
  ! Write the delta t
  do loc=1,nxloc
     data_size = ny_
     if (iloc_ifwrite(loc).eq.0 .or. kproc.ne.1) data_size = 0
     do var=1,stat_nvar
        disp = 4*3 + str_medium*stat_nvar + WP*ny*(loc-1)*stat_nvar + WP*ny*(var-1)
        call MPI_FILE_SET_VIEW(ifile,disp,MPI_REAL_WP,fileview_y,"native",mpi_info,ierr)
        call MPI_FILE_WRITE_ALL(ifile,dt_y(loc,:,var),data_size,MPI_REAL_WP,status,ierr)
     end do
  end do
  
  ! Write the stats
  do loc=1,nxloc
     data_size = ny_
     if (iloc_ifwrite(loc).eq.0 .or. kproc.ne.1) data_size = 0
     do var=1,stat_nvar
        disp = 4*3 + str_medium*stat_nvar + WP*ny*nxloc*stat_nvar + WP*ny*(loc-1)*stat_nvar + WP*ny*(var-1)
        call MPI_FILE_SET_VIEW(ifile,disp,MPI_REAL_WP,fileview_y,"native",mpi_info,ierr)
        call MPI_FILE_WRITE_ALL(ifile,buf_y(loc,:,var),data_size,MPI_REAL_WP,status,ierr)
     end do
  end do
  
  ! Close the file
  call MPI_FILE_CLOSE(ifile,ierr)
  
  ! Log
  call monitor_log("1D-Y SPRAY STATISTICS FILE WRITTEN")
  
  ! Convert it to asci file
  if (irank.eq.iroot) call stat_1dy_spray_asci
  
  return
end subroutine stat_1dy_spray_write

subroutine stat_1dx_spray_write
  use stat_1d_spray
  use parallel
  implicit none
  
  integer :: var,loc,ierr,ifile,data_size,i
  integer(kind=MPI_OFFSET_KIND) :: disp
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer, dimension(3) :: dims
  character(len=str_medium) :: filename
  
  ! Divide by delta t
  do var=1,stat_nvar
     do i=imin_,imax_
        do loc=1,nyloc
           if (dt_x(loc,i,var).gt.0.0_WP) then
              buf_x(loc,i,var) = stat_x(loc,i,var) / dt_x(loc,i,var)
           else
              buf_x(loc,i,var) = 0.0_WP
           end if
        end do
     end do
  end do
  
  ! Open the file to write
  filename = "ufs:stat/stat-1D-spray-x"
  if (irank.eq.iroot) call MPI_FILE_DELETE(filename,mpi_info,ierr)
  call MPI_FILE_OPEN(comm,filename,IOR(MPI_MODE_WRONLY,MPI_MODE_CREATE),mpi_info,ifile,ierr)
  
  ! Write the headers
  if (irank.eq.iroot) then
     ! Write dimensions
     dims(1) = nx
     dims(2) = stat_nvar
     dims(3) = nyloc
     call MPI_FILE_WRITE(ifile,dims,3,MPI_INTEGER,status,ierr)
     ! Write variable names
     do var=1,stat_nvar
        call MPI_FILE_WRITE(ifile,stat_name(var),str_medium,MPI_CHARACTER,status,ierr)
     end do
  end if
  
  ! Write the delta t
  do loc=1,nyloc
     data_size = nx_
     if (jloc_ifwrite(loc).eq.0 .or. kproc.ne.1) data_size = 0
     do var=1,stat_nvar
        disp = 4*3 + str_medium*stat_nvar + WP*nx*(loc-1)*stat_nvar + WP*nx*(var-1)
        call MPI_FILE_SET_VIEW(ifile,disp,MPI_REAL_WP,fileview_x,"native",mpi_info,ierr)
        call MPI_FILE_WRITE_ALL(ifile,dt_x(loc,:,var),data_size,MPI_REAL_WP,status,ierr)
     end do
  end do
  
  ! Write the stats
  do loc=1,nyloc
     data_size = nx_
     if (jloc_ifwrite(loc).eq.0 .or. kproc.ne.1) data_size = 0
     do var=1,stat_nvar
        disp = 4*3 + str_medium*stat_nvar + WP*nx*nyloc*stat_nvar + WP*nx*(loc-1)*stat_nvar + WP*nx*(var-1)
        call MPI_FILE_SET_VIEW(ifile,disp,MPI_REAL_WP,fileview_x,"native",mpi_info,ierr)
        call MPI_FILE_WRITE_ALL(ifile,buf_x(loc,:,var),data_size,MPI_REAL_WP,status,ierr)
     end do
  end do
  
  ! Close the file
  call MPI_FILE_CLOSE(ifile,ierr)
  
  ! Log
  call monitor_log("1D-X SPRAY STATISTICS FILE WRITTEN")
  
  ! Convert it to asci file
  if (irank.eq.iroot) call stat_1dx_spray_asci
  
  return
end subroutine stat_1dx_spray_write


! ================================= !
! Read a stat file as a binary file !
! Dump it as a text file            !
! ================================= !
subroutine stat_1dx_spray_asci
  use stat_1d_spray
  use fileio
  implicit none
  
  character(len=str_medium) :: filename
  character(len=str_medium), dimension(:), allocatable :: names
  real(WP), dimension(:,:,:), allocatable :: data8
  integer, dimension(3) :: dims
  integer :: iunit,ierr,i,i3,k
  
  ! ** Open the stat file to read **
  filename = "stat/stat-1D-spray-x"
  call BINARY_FILE_OPEN(iunit,trim(filename), "r", ierr)
  call BINARY_FILE_READ(iunit,dims,3,kind(dims),ierr)
  allocate(names(dims(2)))
  call BINARY_FILE_READ(iunit,names,dims(2)*str_medium,kind(names),ierr)
  allocate(data8(dims(1),dims(2),dims(3)))
  call BINARY_FILE_READ(iunit,data8,dims(1)*dims(2)*dims(3),kind(data8),ierr)
  call BINARY_FILE_READ(iunit,data8,dims(1)*dims(2)*dims(3),kind(data8),ierr)
  call BINARY_FILE_CLOSE(iunit,ierr)
  
  ! ** Open the asci file to write **
  do i3=1,dims(3)
     iunit = iopen()
     write(filename,'(a21,f12.10,a4)') "stat/stat-1D-spray-x-",yloc(i3),".txt"
     open (iunit, file=filename, form="formatted", iostat=ierr)
     write(iunit,'(10000a20)') 'x', 'xm', (trim(adjustl(names(k))),k=1,dims(2))
     do i=1,dims(1)
        write(iunit,'(10000ES20.12)') x(i+imin-1)+epsilon(x(i+imin-1)),xm(i+imin-1)+epsilon(xm(i+imin-1)),data8(i,:,i3)
     end do
     close(iclose(iunit))
  end do
  
  return
end subroutine stat_1dx_spray_asci


! ================================= !
! Read a stat file as a binary file !
! Dump it as a text file            !
! ================================= !
subroutine stat_1dy_spray_asci
  use stat_1d
  use fileio
  implicit none
  
  character(len=str_medium) :: filename
  character(len=str_medium), dimension(:), allocatable :: names
  real(WP), dimension(:,:,:), allocatable :: data8
  integer, dimension(3) :: dims
  integer :: iunit,ierr,j,i3,k
  
  ! ** Open the stat file to read **
  filename = "stat/stat-1D-spray-y"
  call BINARY_FILE_OPEN(iunit,trim(filename), "r", ierr)
  call BINARY_FILE_READ(iunit,dims,3,kind(dims),ierr)
  allocate(names(dims(2)))
  call BINARY_FILE_READ(iunit,names,dims(2)*str_medium,kind(names),ierr)
  allocate(data8(dims(1),dims(2),dims(3)))
  call BINARY_FILE_READ(iunit,data8,dims(1)*dims(2)*dims(3),kind(data8),ierr)
  call BINARY_FILE_READ(iunit,data8,dims(1)*dims(2)*dims(3),kind(data8),ierr)
  call BINARY_FILE_CLOSE(iunit,ierr)
  
  ! ** Open the asci file to write **
  do i3=1,dims(3)
     iunit = iopen()
     write(filename,'(a21,f12.10,a4)') "stat/stat-1D-spray-y-",xloc(i3),".txt"
     open (iunit, file=filename, form="formatted", iostat=ierr)
     write(iunit,'(10000a20)') 'y', 'ym', (trim(adjustl(names(k))),k=1,dims(2))
     do j=1,dims(1)
        write(iunit,'(10000ES20.12)') y(j+jmin-1)+epsilon(y(j+jmin-1)),ym(j+jmin-1)+epsilon(ym(j+jmin-1)),data8(j,:,i3)
     end do
     close(iclose(iunit))
  end do
  
  return
end subroutine stat_1dy_spray_asci
