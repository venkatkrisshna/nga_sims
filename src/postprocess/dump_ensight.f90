module dump_ensight
  use parallel
  use precision
  use geometry
  use partition
  use masks
  use fileio
  use unstruct
  implicit none
  
  ! Time info
  integer :: nout_time
  real(WP), dimension(:), allocatable :: out_time
  
  ! SP buffers
  real(SP), dimension(:), allocatable :: buffer_hexa
  real(SP), dimension(:), allocatable :: buffer_wedge
  real(SP), dimension(:,:), allocatable :: buffer3_hexa
  real(SP), dimension(:,:), allocatable :: buffer3_wedge
  
  ! Fileviews
  integer :: fileview_hexa
  integer :: fileview_wedge
  integer :: fileview_node
  integer :: fileview_hexa_conn
  integer :: fileview_wedge_conn
  
end module dump_ensight


! ================================================= !
! Dump 3D binary ensight gold data - Initialization !
! ================================================= !
subroutine dump_ensight_3D_init
  use dump_ensight
  use parser
  use parallel
  use time_info
  implicit none
  integer :: ierr,i
  logical :: file_is_there
  integer, dimension(:), allocatable :: blocklength
  integer, dimension(:), allocatable :: map
  
  ! Create & Start the timer
  call timing_create('ensight')
  call timing_start ('ensight')

  ! Allocate buffers
  allocate(buffer_hexa(ncells_hexa_))
  allocate(buffer_wedge(ncells_wedge_))
  allocate(buffer3_hexa(ncells_hexa_,3))
  allocate(buffer3_wedge(ncells_wedge_,3))
  
  ! Open the file
  inquire(file='ensight-3D/arts.case',exist=file_is_there)
  if (file_is_there) then
     ! Read the file
     call parser_parsefile('ensight-3D/arts.case')
     ! Get the time
     call parser_getsize('time values',nout_time)
     allocate(out_time(nout_time))
     call parser_read('time values',out_time)
     ! Remove future time
     future: do i=1,size(out_time)
        if (out_time(i).GE.time*0.99999_WP) then
           nout_time = i-1
           exit future
        end if
     end do future
  else
     ! Create directory
     if (irank.eq.iroot) call CREATE_FOLDER("ensight-3D")
     ! Set the time
     nout_time = 0
     allocate(out_time(1))
  end if
  
!!$  ! Create the views
!!$  allocate(map(1:ncells_hexa_))
!!$  map = hexa_-1
!!$  call MPI_TYPE_CREATE_INDEXED_BLOCK(ncells_hexa_,1,map,MPI_REAL_SP,fileview_hexa,ierr)
!!$  call MPI_TYPE_COMMIT(fileview_hexa,ierr)
!!$  map = map * 8
!!$  call MPI_TYPE_CREATE_INDEXED_BLOCK(ncells_hexa_,8,map,MPI_INTEGER,fileview_hexa_conn,ierr)
!!$  call MPI_TYPE_COMMIT(fileview_hexa_conn,ierr)
!!$  deallocate(map)
!!$  allocate(map(1:ncells_wedge_))
!!$  map = wedge_-1
!!$  call MPI_TYPE_CREATE_INDEXED_BLOCK(ncells_wedge_,1,map,MPI_REAL_SP,fileview_wedge,ierr)
!!$  call MPI_TYPE_COMMIT(fileview_wedge,ierr)
!!$  map = map * 6
!!$  call MPI_TYPE_CREATE_INDEXED_BLOCK(ncells_wedge_,6,map,MPI_INTEGER,fileview_wedge_conn,ierr)
!!$  call MPI_TYPE_COMMIT(fileview_wedge_conn,ierr)
!!$  deallocate(map)
!!$  allocate(map(1:nnodes_))
!!$  map = nodes_-1
!!$  !call MPI_TYPE_CREATE_INDEXED_BLOCK(nnodes_,1,map,MPI_REAL_SP,fileview_node,ierr)
!!$  blocklength = 1
!!$  call MPI_TYPE_INDEXED(nnodes_,blocklength,map,MPI_INTEGER,fileview_node,ierr)
!!$  call MPI_TYPE_COMMIT(fileview_node,ierr)
!!$  deallocate(blocklength)
  
  ! Create the views  - hex
  allocate(map(ncells_hexa_),blocklength(ncells_hexa_))
  map = hexa_-1
  blocklength = 1
  call MPI_TYPE_INDEXED(ncells_hexa_,blocklength,map,MPI_REAL_SP,fileview_hexa,ierr)
  call MPI_TYPE_COMMIT(fileview_hexa,ierr)
  map = map * 8
  blocklength = 8
  call MPI_TYPE_INDEXED(ncells_hexa_,blocklength,map,MPI_INTEGER,fileview_hexa_conn,ierr)
  call MPI_TYPE_COMMIT(fileview_hexa_conn,ierr)
  deallocate(map,blocklength)
  ! Create the views  - wedge
  allocate(map(ncells_wedge_),blocklength(ncells_wedge_))
  map = wedge_-1
  blocklength = 1
  call MPI_TYPE_INDEXED(ncells_wedge_,blocklength,map,MPI_REAL_SP,fileview_wedge,ierr)
  call MPI_TYPE_COMMIT(fileview_wedge,ierr)
  map = map * 6
  blocklength = 6
  call MPI_TYPE_INDEXED(ncells_wedge_,blocklength,map,MPI_INTEGER,fileview_wedge_conn,ierr)
  call MPI_TYPE_COMMIT(fileview_wedge_conn,ierr)
  deallocate(map,blocklength)
  ! Create the views  - nodes
  allocate(map(nnodes_),blocklength(nnodes_))
  map = nodes_-1
  blocklength = 1
  call MPI_TYPE_INDEXED(nnodes_,blocklength,map,MPI_INTEGER,fileview_node,ierr)
  call MPI_TYPE_COMMIT(fileview_node,ierr)
  deallocate(map,blocklength)
  
  ! Stop the timer
  call timing_stop ('ensight')
  
  ! Write the geometry
  call dump_ensight_3D_geometry
  
  return
end subroutine dump_ensight_3D_init


! ================================ !
! Dump 3D binary ensight gold data !
! ================================ !
subroutine dump_ensight_3D
  use dump_ensight
  use data
  use partition
  use string
  use lpt
  use combustion
  use sgsmodel
  use interpolate
  use memory
  use pressure
  use multiphase_vof
  use multiphase
  use soot
  use onestep
  use strainrate
  use finitechem
  use ib
  implicit none
  character(len=str_short) :: name
  integer :: i,j,k
  ! Start the timer
  call timing_start('ensight')

  ! Update the case file
  call dump_ensight_3D_case
  
  ! IB surface
  if (use_ib) then
     name="IB"
     call dump_ensight_3D_scalar(Gib,name)
  end if
  
  ! Pressure
  name = "P"
  if (use_multiphase) then
     call dump_ensight_3D_scalar(DP,name)
  else
     call dump_ensight_3D_scalar(P,name)
  end if
  
  ! Viscosity
  name = "VISC"
  call dump_ensight_3D_scalar(VISC,name)
  if (pope_crit) then
     name = "KSGS"
     call dump_ensight_3D_scalar(k_sgs,name)
  end if
  
  ! Velocity
  name = "V"
  if (icyl.eq.0) then
     call dump_ensight_3D_vector(Ui,Vi,Wi,name)
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              tmp1(i,j,k) = Ui(i,j,k)
              tmp2(i,j,k) = Vi(i,j,k) * cos(zm(k)) - Wi(i,j,k) * sin(zm(k))
              tmp3(i,j,k) = Vi(i,j,k) * sin(zm(k)) + Wi(i,j,k) * cos(zm(k))
           end do
        end do
     end do
     call dump_ensight_3D_vector(tmp1,tmp2,tmp3,name)
  end if
  
  ! Scalars
  do i=1,nscalar
     name = SC_name(i)
     call dump_ensight_3D_scalar(SC(:,:,:,i),name)
  end do
  if (nscalar.ge.1) then
     name = 'DIFF'
     call dump_ensight_3D_scalar(DIFF(:,:,:,1),name)
  end if
  
  ! Combustion
  if (combust) then
     if (isc_T.eq.0) then
        name = 'Temp'
        call dump_ensight_3D_scalar(T,name)
     end if
     name = 'RHO'
     call dump_ensight_3D_scalar(RHO,name)
     name = 'dRHO'
     call dump_ensight_3D_scalar(dRHO,name)
     if (isc_ZVAR.eq.0 .and. use_sgs) then
        name = 'ZVAR'
        call dump_ensight_3D_scalar(ZVAR,name)
     end if
     if (isc_ZMIX.ne.0) then
        name = 'CHI'
        call dump_ensight_3D_scalar(CHI,name)
     end if
     if (trim(chemistry).eq.'one-step') then
        ! Compute mixture fraction
        call onestep_get_mixfrac
        name = 'Z'
        call dump_ensight_3D_scalar(Mix_Frac,name)
        ! Compute ksi
        call onestep_get_flame_index
        name = 'ksi'
        call dump_ensight_3D_scalar(ksi,name)
     end if
     ! Finite chem output
     if (trim(chemistry).eq.'finite chem') then
        ! Dump heat release
        name = "HR"
        call dump_ensight_3D_scalar(HR,name)
     end if
  end if
  
  ! Spray
  if (use_lpt) then
     ! Fluid volume fraction
     name = 'EPSF'
     call dump_ensight_3D_scalar(epsf,name)
     ! Density and density change
     name = 'RHO'
     call dump_ensight_3D_scalar(RHO,name)
     name = 'dRHO'
     call dump_ensight_3D_scalar(dRHO,name)
     ! Particles
     call dump_ensight_particle
  end if
  
  ! Level set
  if (use_lvlset) then
     name = "LVLSET"
     call dump_ensight_3D_scalar(LVLSET,name)
  end if
  
  ! Multiphase
  if (use_multiphase) then
     name = "VOF"
     call dump_ensight_3D_scalar(VOF,name)
     name = "G"
     call dump_ensight_3D_scalar(G,name)
     name = "curv"
     call dump_ensight_3D_scalar(curv,name)
  end if
  
  ! Soot
  if (use_soot) then
     name = "numdens"
     call dump_ensight_3D_scalar(numdens,name)
     name = "fv"
     call dump_ensight_3D_scalar(fv,name)
     name = "partdiam"
     call dump_ensight_3D_scalar(partdiam,name)
     name = "partaggr"
     call dump_ensight_3D_scalar(partaggr,name)
     name = "surfreac"
     call dump_ensight_3D_scalar(surfreac,name)
  end if

  ! Divergence
  name = "divg"
  tmp1(imin_:imax_,jmin_:jmax_,kmin_:kmax_) = divg
  call dump_ensight_3D_scalar(tmp1,name)
  
  ! PQR criterion
  call strainrate_PQRcrit
  name = "Pcrit"
  call dump_ensight_3D_scalar(Pcrit,name)
  name = "Qcrit"
  call dump_ensight_3D_scalar(Qcrit,name)
  name = "Rcrit"
  call dump_ensight_3D_scalar(Rcrit,name)
  
  ! Stop the timer
  call timing_stop('ensight')

  return
end subroutine dump_ensight_3D


! =========================================== !
! Dump 3D binary ensight gold data - geometry !
! => one processor only - test before         !
! =========================================== !
subroutine dump_ensight_3D_geometry
  use dump_ensight
  implicit none
  integer :: ierr,ibuffer,iunit,i
  character(len=80) :: buffer
  character(len=str_medium) :: file
  real(SP), dimension(:), allocatable :: xbuf,ybuf,zbuf
  real(SP) :: max_x,max_y,max_z
  real(SP) :: min_x,min_y,min_z
  real(SP) :: buf
  logical  :: file_is_there
  integer  :: size
  integer(kind=MPI_OFFSET_KIND) :: disp
  integer, dimension(MPI_STATUS_SIZE) :: status
  
  ! Get single precision mesh
  allocate(xbuf(nnodes_),ybuf(nnodes_),zbuf(nnodes_))
  if (icyl.eq.0) then
     do i=1,nnodes_
        xbuf(i)=x(unstr2str(i,1))
        ybuf(i)=y(unstr2str(i,2))
        zbuf(i)=z(unstr2str(i,3))
     end do
  else
     do i=1,nnodes_
        xbuf(i)=x(unstr2str(i,1))
        ybuf(i)=y(unstr2str(i,2))*cos(z(unstr2str(i,3)))
        zbuf(i)=y(unstr2str(i,2))*sin(z(unstr2str(i,3)))
     end do
  end if
  buf=minval(xbuf); call MPI_ALLREDUCE(buf,min_x,1,MPI_REAL_SP,MPI_MIN,comm,ierr)
  buf=minval(ybuf); call MPI_ALLREDUCE(buf,min_y,1,MPI_REAL_SP,MPI_MIN,comm,ierr)
  buf=minval(zbuf); call MPI_ALLREDUCE(buf,min_z,1,MPI_REAL_SP,MPI_MIN,comm,ierr)
  buf=maxval(xbuf); call MPI_ALLREDUCE(buf,max_x,1,MPI_REAL_SP,MPI_MAX,comm,ierr)
  buf=maxval(ybuf); call MPI_ALLREDUCE(buf,max_y,1,MPI_REAL_SP,MPI_MAX,comm,ierr)
  buf=maxval(zbuf); call MPI_ALLREDUCE(buf,max_z,1,MPI_REAL_SP,MPI_MAX,comm,ierr)
  
  ! Generate the geometry file in parallel
  file=trim(mpiiofs) // "ensight-3D/geometry"
  inquire(file=file,exist=file_is_there)
  if (file_is_there .and. irank.eq.iroot) call MPI_FILE_DELETE(file,mpi_info,ierr)
  call MPI_FILE_OPEN(comm,file,IOR(MPI_MODE_WRONLY,MPI_MODE_CREATE),mpi_info,iunit,ierr)
  
  ! Write header (only root)
  if (irank.eq.iroot) then
     ! Global header
     buffer = 'C Binary'
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
     buffer = 'Ensight Gold Geometry File'
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
     buffer = 'Unstructured Geometry from ARTS'
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
     buffer = 'node id given'
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
     buffer = 'element id given'
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
     ! Extents
     buffer = 'extents'
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
     size = 1
     call MPI_FILE_WRITE(iunit,min_x,size,MPI_REAL_SP,status,ierr)
     call MPI_FILE_WRITE(iunit,max_x,size,MPI_REAL_SP,status,ierr)
     call MPI_FILE_WRITE(iunit,min_y,size,MPI_REAL_SP,status,ierr)
     call MPI_FILE_WRITE(iunit,max_y,size,MPI_REAL_SP,status,ierr)
     call MPI_FILE_WRITE(iunit,min_z,size,MPI_REAL_SP,status,ierr)
     call MPI_FILE_WRITE(iunit,max_z,size,MPI_REAL_SP,status,ierr)
     ! Part header
     buffer = 'part'
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
     ibuffer = 1
     size = 1
     call MPI_FILE_WRITE(iunit,ibuffer,size,MPI_INTEGER,status,ierr)
     buffer = 'ARTS 3D geometry'
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
     ! Nodes list
     buffer = 'coordinates'
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
     size = 1
     call MPI_FILE_WRITE(iunit,nnodes,size,MPI_INTEGER,status,ierr)
  end if
  
  ! Write the node positions
  disp = 752
  call MPI_FILE_SET_VIEW(iunit,disp,MPI_INTEGER,fileview_node,"native",mpi_info,ierr)
  call MPI_FILE_WRITE_ALL(iunit,nodes_,nnodes_,MPI_INTEGER,status,ierr)
  disp = disp+4*nnodes
  call MPI_FILE_SET_VIEW(iunit,disp,MPI_REAL_SP,fileview_node,"native",mpi_info,ierr)
  call MPI_FILE_WRITE_ALL(iunit,xbuf,nnodes_,MPI_REAL_SP,status,ierr)
  disp = disp+4*nnodes
  call MPI_FILE_SET_VIEW(iunit,disp,MPI_REAL_SP,fileview_node,"native",mpi_info,ierr)
  call MPI_FILE_WRITE_ALL(iunit,ybuf,nnodes_,MPI_REAL_SP,status,ierr)
  disp = disp+4*nnodes
  call MPI_FILE_SET_VIEW(iunit,disp,MPI_REAL_SP,fileview_node,"native",mpi_info,ierr)
  call MPI_FILE_WRITE_ALL(iunit,zbuf,nnodes_,MPI_REAL_SP,status,ierr)
  
  ! Write the hexa connectivity
  disp = disp+4*nnodes
  call MPI_FILE_SET_VIEW(iunit,disp,MPI_CHARACTER,MPI_CHARACTER,"native",mpi_info,ierr)
  if (irank.eq.iroot) then
     buffer = 'hexa8'
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
     size = 1
     call MPI_FILE_WRITE(iunit,ncells_hexa,size,MPI_INTEGER,status,ierr)
     !do i=1,ncells_hexa
     !   call MPI_FILE_WRITE(iunit,i,size,MPI_INTEGER,status,ierr)
     !end do
  end if
  disp = disp+84
  call MPI_FILE_SET_VIEW(iunit,disp,MPI_INTEGER,fileview_hexa_conn,"native",mpi_info,ierr)
  call MPI_FILE_WRITE_ALL(iunit,hexa_,ncells_hexa_,MPI_INTEGER,status,ierr)
  disp = disp+4*ncells_hexa
  call MPI_FILE_SET_VIEW(iunit,disp,MPI_INTEGER,fileview_hexa_conn,"native",mpi_info,ierr)
  size = 8*ncells_hexa_
  call MPI_FILE_WRITE_ALL(iunit,conn_hexa,size,MPI_INTEGER,status,ierr)
  
  ! Write the file - wedge
  if (ncells_wedge.gt.0) then
     disp = disp+8*4*ncells_hexa
     call MPI_FILE_SET_VIEW(iunit,disp,MPI_CHARACTER,MPI_CHARACTER,"native",mpi_info,ierr)
     if (irank.eq.iroot) then
        buffer = 'penta6'
        size = 80
        call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
        size = 1
        call MPI_FILE_WRITE(iunit,ncells_wedge,size,MPI_INTEGER,status,ierr)
        !do i=1,ncells_wedge
        !   call MPI_FILE_WRITE(iunit,i,size,MPI_INTEGER,status,ierr)
        !end do
     end if
     disp = disp+84
     call MPI_FILE_SET_VIEW(iunit,disp,MPI_INTEGER,fileview_wedge_conn,"native",mpi_info,ierr)
     call MPI_FILE_WRITE_ALL(iunit,wedge_,ncells_wedge_,MPI_INTEGER,status,ierr)
     disp = disp+4*ncells_wedge
     call MPI_FILE_SET_VIEW(iunit,disp,MPI_INTEGER,fileview_wedge_conn,"native",mpi_info,ierr)
     size = 6*ncells_wedge_
     call MPI_FILE_WRITE_ALL(iunit,conn_wedge,size,MPI_INTEGER,status,ierr)
  end if
  
  ! Close the file
  call MPI_FILE_CLOSE(iunit,ierr)

  ! Deallocate arrays
  deallocate(xbuf,ybuf,zbuf)
  
  return
end subroutine dump_ensight_3D_geometry


! ============================================ !
! Dump 3D binary ensight gold data - case file !
! ============================================ !
subroutine dump_ensight_3D_case
  use dump_ensight
  use lpt
  use combustion
  use sgsmodel
  use time_info
  use finitechem
  implicit none
  integer :: iunit,ierr,i
  real(WP), dimension(:), allocatable :: buffer
  character(len=80) :: str
  character(len=str_short) :: temp
  
  ! Update the time info
  allocate(buffer(nout_time))
  buffer(1:nout_time) = out_time(1:nout_time)
  deallocate(out_time)
  nout_time = nout_time + 1
  allocate(out_time(nout_time))
  out_time(1:nout_time-1) = buffer(1:nout_time-1)
  out_time(nout_time) = time
  
  ! Write - Single proc & parallel => only root writes (in ASCII)
  if (irank==iroot) then
     ! Open the file
     iunit = iopen()
     open(iunit,file="ensight-3D/arts.case",form="formatted",iostat=ierr,status="REPLACE")
     ! Write the case
     str='FORMAT'
     write(iunit,'(a80)') str
     str='type: ensight gold'
     write(iunit,'(a80)') str
     str='GEOMETRY'
     write(iunit,'(a80)') str
     str='model: geometry'
     write(iunit,'(a80)') str
     str='measured: 1 particles/particles.******'
     if (use_lpt) write(iunit,'(a80)') str
     str='VARIABLE'
     write(iunit,'(a80)') str
     ! IB
     if (use_IB) then
        str='scalar per element: 1 IB IB/IB.******'
        write(iunit,'(a80)') str
     end if
     ! Pressure
     str='scalar per element: 1 Pressure P/P.******'
     write(iunit,'(a80)') str
     ! Viscosity
     str='scalar per element: 1 Viscosity VISC/VISC.******'
     write(iunit,'(a80)') str
     if (pope_crit) then
        str='scalar per element: 1 KSGS KSGS/KSGS.******'
        write(iunit,'(a80)') str
     end if
     ! Diffusivity
     str='scalar per element: 1 Diffusivity DIFF/DIFF.******'
     if (nscalar.ge.1) write(iunit,'(a80)') str
     ! Scalars
     do i=1,nscalar
        str = 'scalar per element: 1 '//trim(SC_name(i))//' '//trim(SC_name(i))//'/'//trim(SC_name(i))//'.******'
        write(iunit,'(a80)') str
     end do
     ! Combustion stuff
     if (combust) then
        if (isc_T.eq.0) then
           str = 'scalar per element: 1 Temp Temp/Temp.******'
           write(iunit,'(a80)') str
        end if
        str = 'scalar per element: 1 RHO RHO/RHO.******'
        write(iunit,'(a80)') str
        str = 'scalar per element: 1 dRHO dRHO/dRHO.******'
        write(iunit,'(a80)') str
        str = 'scalar per element: 1 ZVAR ZVAR/ZVAR.******'
        if (isc_ZVAR.eq.0 .and. use_sgs) write(iunit,'(a80)') str
        str = 'scalar per element: 1 CHI CHI/CHI.******'
        if (isc_ZMIX.ne.0) write(iunit,'(a80)') str
        if (trim(chemistry).eq.'one-step') then
           str = 'scalar per element: 1 Z Z/Z.******'
           write(iunit,'(a80)') str
           str = 'scalar per element: 1 ksi ksi/ksi.******'
           write(iunit,'(a80)') str
        end if
     end if
     ! Finite chem stuff
     if (chemistry.eq.'finite chem') then
        str='scalar per element: 1 HR HR/HR.******'
        write(iunit,'(a80)') str
     end if
     ! Velocity
     str='vector per element: 1 V V/V.******'
     write(iunit,'(a80)') str
     ! Spray stuff
     if (use_lpt) then
        str='scalar per element: 1 EPSF EPSF/EPSF.******'
        write(iunit,'(a80)') str
        str='scalar per element: 1 RHO RHO/RHO.******'
        write(iunit,'(a80)') str
        str='scalar per element: 1 dRHO dRHO/dRHO.******'
        write(iunit,'(a80)') str
        str='scalar per measured node: 1 id particles/id.******'
        write(iunit,'(a80)') str
        str='scalar per measured node: 1 Diameter particles/diameter.******'
        write(iunit,'(a80)') str
        str='vector per measured node: 1 Velocity particles/velocity.******'
        write(iunit,'(a80)') str
        str='vector per measured node: 1 AngVel particles/angvel.******'
        write(iunit,'(a80)') str
     end if
     ! Level set stuff
     if (use_lvlset) then
        str='scalar per element: 1 LVLSET LVLSET/LVLSET.******'
        write(iunit,'(a80)') str
     end if
     ! Multiphase stuff
     if (use_multiphase) then
        str='scalar per element: 1 VOF VOF/VOF.******'
        write(iunit,'(a80)') str
        str='scalar per element: 1 G G/G.******'
        write(iunit,'(a80)') str
        str='scalar per element: 1 curv curv/curv.******'
        write(iunit,'(a80)') str
     end if
     ! Soot stuff
     if (use_soot) then
        str='scalar per element: 1 numdens numdens/numdens.******'
        write(iunit,'(a80)') str
        str='scalar per element: 1 fv fv/fv.******'
        write(iunit,'(a80)') str
        str='scalar per element: 1 partdiam partdiam/partdiam.******'
        write(iunit,'(a80)') str
        str='scalar per element: 1 partaggr partaggr/partaggr.******'
        write(iunit,'(a80)') str
        str='scalar per element: 1 surfreac surfreac/surfreac.******'
        write(iunit,'(a80)') str
     end if
     ! Divergence
     str='scalar per element: 1 divg divg/divg.******'
     write(iunit,'(a80)') str
     ! PQR criterion
     str='scalar per element: 1 Pcrit Pcrit/Pcrit.******'
     write(iunit,'(a80)') str
     str='scalar per element: 1 Qcrit Qcrit/Qcrit.******'
     write(iunit,'(a80)') str
     str='scalar per element: 1 Rcrit Rcrit/Rcrit.******'
     write(iunit,'(a80)') str
     ! Time section
     str='TIME'
     write(iunit,'(a80)') str
     str='time set: 1'
     write(iunit,'(a80)') str
     str='number of steps:'
     write(iunit,'(a16,x,i12)') str,nout_time
     str='filename start number: 1'
     write(iunit,'(a80)') str
     str='filename increment: 1'
     write(iunit,'(a80)') str
     str='time values:'
     write(iunit,'(a12,x,10000000(3(ES12.5,x),/))') str,out_time
     ! Close the file
     close(iclose(iunit))
  end if
  
  return
end subroutine dump_ensight_3D_case


! ========================================= !
! Dump 3D binary ensight gold data - scalar !
! ========================================= !
subroutine dump_ensight_3D_scalar(scalar,name)
  use dump_ensight
  use string
  use data
  use parallel
  implicit none
  integer :: iunit,ierr,size,ibuffer,i,j,k,count_wedge_,count_hexa_
  character(len=80) :: buffer
  character(len=str_short) :: name
  character(len=str_medium) :: file
  real(WP),dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: scalar
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer(kind=MPI_OFFSET_KIND) :: disp
  logical :: file_is_there
  
  ! Extract the data
  count_wedge_ = 0
  count_hexa_ = 0
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (mask(i,j).eq.0) then
              if (icyl.eq.1 .and. j.eq.jmin) then
                 count_wedge_ = count_wedge_ + 1
                 buffer_wedge(count_wedge_) = real(scalar(i,j,k),SP)
              else
                 count_hexa_ = count_hexa_ + 1
                 buffer_hexa(count_hexa_) = real(scalar(i,j,k),SP)
              end if
           end if
        end do
     end do
  end do
  
  ! Generate the file
  if (irank.eq.iroot) call CREATE_FOLDER("ensight-3D/" // trim(adjustl(name)))
  file = trim(mpiiofs) // "ensight-3D/" // trim(adjustl(name)) // "/" // trim(adjustl(name)) // "."
  write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nout_time
  
  ! Open the file
  inquire(file=file,exist=file_is_there)
  if (file_is_there .and. irank.eq.iroot) call MPI_FILE_DELETE(file,mpi_info,ierr)
  call MPI_FILE_OPEN(comm,file,IOR(MPI_MODE_WRONLY,MPI_MODE_CREATE),mpi_info,iunit,ierr)
  ! Write header (only root)
  if (irank.eq.iroot) then
     buffer = trim(adjustl(name))
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
     buffer = 'part'
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
     ibuffer = 1
     size = 1
     call MPI_FILE_WRITE(iunit,ibuffer,size,MPI_INTEGER,status,ierr)
     buffer = 'hexa8'
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
  end if
  
  ! Write the file - hexa
  disp = 3*80+4
  call MPI_FILE_SET_VIEW(iunit,disp,MPI_REAL_SP,fileview_hexa,"native",mpi_info,ierr)
  call MPI_FILE_WRITE_ALL(iunit,buffer_hexa,ncells_hexa_,MPI_REAL_SP,status,ierr)
  
  ! Write the file - wedge
  if (ncells_wedge.gt.0) then
     disp = 3*80+4+4*ncells_hexa
     call MPI_FILE_SET_VIEW(iunit,disp,MPI_CHARACTER,MPI_CHARACTER,"native",mpi_info,ierr)
     if (irank.eq.iroot) then
        buffer = 'penta6'
        size = 80
        call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
     end if
     disp = 3*80+4+80+4*ncells_hexa
     call MPI_FILE_SET_VIEW(iunit,disp,MPI_REAL_SP,fileview_wedge,"native",mpi_info,ierr)
     call MPI_FILE_WRITE_ALL(iunit,buffer_wedge,ncells_wedge_,MPI_REAL_SP,status,ierr)
  end if
  
  ! Close the file
  call MPI_FILE_CLOSE(iunit,ierr)
  
  return
end subroutine dump_ensight_3D_scalar


! ========================================= !
! Dump 3D binary ensight gold data - vector !
! ========================================= !
subroutine dump_ensight_3D_vector(vec1,vec2,vec3,name)
  use dump_ensight
  use string
  use parallel
  implicit none
  integer :: iunit,ierr,size,ibuffer,i,j,k,count_wedge_,count_hexa_
  character(len=80) :: buffer
  character(len=str_short) :: name
  character(len=str_medium) :: file
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: vec1
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: vec2
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: vec3
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer(kind=MPI_OFFSET_KIND) :: disp
  logical :: file_is_there
  
  ! Extract the data
  count_wedge_ = 0
  count_hexa_ = 0
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (mask(i,j).eq.0) then
              if (icyl.eq.1 .and. j.eq.jmin) then
                 count_wedge_ = count_wedge_ + 1
                 buffer3_wedge(count_wedge_,1) = real(vec1(i,j,k),SP)
                 buffer3_wedge(count_wedge_,2) = real(vec2(i,j,k),SP)
                 buffer3_wedge(count_wedge_,3) = real(vec3(i,j,k),SP)
              else
                 count_hexa_ = count_hexa_ + 1
                 buffer3_hexa(count_hexa_,1) = real(vec1(i,j,k),SP)
                 buffer3_hexa(count_hexa_,2) = real(vec2(i,j,k),SP)
                 buffer3_hexa(count_hexa_,3) = real(vec3(i,j,k),SP)
              end if
           end if
        end do
     end do
  end do
  
  ! Generate the file
  if (irank.eq.iroot) call CREATE_FOLDER("ensight-3D/" // trim(adjustl(name)))
  file = trim(mpiiofs) // "ensight-3D/" // trim(adjustl(name)) // "/" // trim(adjustl(name)) // "."
  write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nout_time
  
  ! Open the file
  inquire(file=file,exist=file_is_there)
  if (file_is_there .and. irank.eq.iroot) call MPI_FILE_DELETE(file,mpi_info,ierr)
  call MPI_FILE_OPEN(comm,file,IOR(MPI_MODE_WRONLY,MPI_MODE_CREATE),mpi_info,iunit,ierr)
  
  ! Write header (only root)
  if (irank.eq.iroot) then
     buffer = trim(adjustl(name))
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
     buffer = 'part'
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
     ibuffer = 1
     size = 1
     call MPI_FILE_WRITE(iunit,ibuffer,size,MPI_INTEGER,status,ierr)
     buffer = 'hexa8'
     size = 80
     call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
  end if
  
  ! Write the data
  disp = 3*80+4+0*ncells_hexa*4
  call MPI_FILE_SET_VIEW(iunit,disp,MPI_REAL_SP,fileview_hexa,"native",mpi_info,ierr)
  call MPI_FILE_WRITE_ALL(iunit,buffer3_hexa(:,1),ncells_hexa_,MPI_REAL_SP,status,ierr)
  disp = 3*80+4+1*ncells_hexa*4
  call MPI_FILE_SET_VIEW(iunit,disp,MPI_REAL_SP,fileview_hexa,"native",mpi_info,ierr)
  call MPI_FILE_WRITE_ALL(iunit,buffer3_hexa(:,2),ncells_hexa_,MPI_REAL_SP,status,ierr)
  disp = 3*80+4+2*ncells_hexa*4
  call MPI_FILE_SET_VIEW(iunit,disp,MPI_REAL_SP,fileview_hexa,"native",mpi_info,ierr)
  call MPI_FILE_WRITE_ALL(iunit,buffer3_hexa(:,3),ncells_hexa_,MPI_REAL_SP,status,ierr)
  
  ! Write the file - wedge
  if (ncells_wedge.gt.0) then
     disp = 3*80+4+3*ncells_hexa*4+0*ncells_wedge*4
     call MPI_FILE_SET_VIEW(iunit,disp,MPI_CHARACTER,MPI_CHARACTER,"native",mpi_info,ierr)
     if (irank.eq.iroot) then
        buffer = 'penta6'
        size = 80
        call MPI_FILE_WRITE(iunit,buffer,size,MPI_CHARACTER,status,ierr)
     end if
     disp = 3*80+4+3*ncells_hexa*4+80+0*ncells_wedge*4
     call MPI_FILE_SET_VIEW(iunit,disp,MPI_REAL_SP,fileview_wedge,"native",mpi_info,ierr)
     call MPI_FILE_WRITE_ALL(iunit,buffer3_wedge(:,1),ncells_wedge_,MPI_REAL_SP,status,ierr)
     disp = 3*80+4+3*ncells_hexa*4+80+1*ncells_wedge*4
     call MPI_FILE_SET_VIEW(iunit,disp,MPI_REAL_SP,fileview_wedge,"native",mpi_info,ierr)
     call MPI_FILE_WRITE_ALL(iunit,buffer3_wedge(:,2),ncells_wedge_,MPI_REAL_SP,status,ierr)
     disp = 3*80+4+3*ncells_hexa*4+80+2*ncells_wedge*4
     call MPI_FILE_SET_VIEW(iunit,disp,MPI_REAL_SP,fileview_wedge,"native",mpi_info,ierr)
     call MPI_FILE_WRITE_ALL(iunit,buffer3_wedge(:,3),ncells_wedge_,MPI_REAL_SP,status,ierr)
  end if
  
  ! Close the file
  call MPI_FILE_CLOSE(iunit,ierr)
  
  return
end subroutine dump_ensight_3D_vector


! ============================================ !
! Dump 3D binary ensight gold data - particles !
! ============================================ !
subroutine dump_ensight_particle
  use dump_ensight
  use lpt
  use string
  use parallel
  implicit none
  
  integer :: iunit,ierr,i,rank
  character(len=str_medium) :: file
  character(len=80) :: cbuffer
  integer :: ibuffer
  real(SP) :: rbuffer
  
  ! Generate the file - Particles
  if (irank.eq.iroot) call CREATE_FOLDER("ensight-3D/" // 'particles')
  file = "ensight-3D/" // 'particles' // "/" // 'particles' // "."
  write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nout_time
  ! Write positions
  do rank=1,nproc
     if (rank.eq.irank) then
        if (rank.eq.1) then
           ! Open the file
           call BINARY_FILE_OPEN(iunit,trim(file),"w",ierr)
           ! Write header
           cbuffer = 'C Binary'
           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
           cbuffer = 'Spray coordinates from ARTS'
           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
           cbuffer = 'particle coordinates'
           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
           ibuffer = max(npart,1)
           call BINARY_FILE_WRITE(iunit,ibuffer,1,kind(ibuffer),ierr)
           do i=1,npart
              ibuffer = i
              call BINARY_FILE_WRITE(iunit,ibuffer,1,kind(ibuffer),ierr)
           end do
           ! Spray coordinates
           do i=1,npart_
              rbuffer = part(i)%x
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%y
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%z
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end do
           ! Zero drop case
           if (npart.eq.0) then
              ibuffer = i
              call BINARY_FILE_WRITE(iunit,ibuffer,1,kind(ibuffer),ierr)
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end if
           ! Close the file
           call BINARY_FILE_CLOSE(iunit,ierr)
        else
           ! Open the file
           call BINARY_FILE_OPEN(iunit,trim(file),"a",ierr)
           ! Spray coordinates
           do i=1,npart_
              rbuffer = part(i)%x
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%y
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%z
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end do
           ! Close the file
           call BINARY_FILE_CLOSE(iunit,ierr)
        end if
     end if
     call MPI_BARRIER(comm,ierr)
  end do
  
  ! Generate the file - Particle ID
  if (irank.eq.iroot) call CREATE_FOLDER("ensight-3D/" // 'particles')
  file = "ensight-3D/" // 'particles' // "/" // 'id' // "."
  write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nout_time
  ! Write particle ID
  do rank=1,nproc
     if (rank.eq.irank) then
        if (rank.eq.1) then
           ! Open the file
           call BINARY_FILE_OPEN(iunit,trim(file),"w",ierr)
           ! Write header
           cbuffer = 'Spray ID'
           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
           ! Spray ID
           do i=1,npart_
              rbuffer = real(part(i)%id,WP)
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end do
           ! Zero drop case
           if (npart.eq.0) then
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end if
           ! Close the file
           call BINARY_FILE_CLOSE(iunit,ierr)
        else
           ! Open the file
           call BINARY_FILE_OPEN(iunit,trim(file),"a",ierr)
           ! Spray ID
           do i=1,npart_
              rbuffer = real(part(i)%id,WP)
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end do
           ! Close the file
           call BINARY_FILE_CLOSE(iunit,ierr)
        end if
     end if
     call MPI_BARRIER(comm,ierr)
  end do
  
  ! Generate the file - Diameters
  if (irank.eq.iroot) call CREATE_FOLDER("ensight-3D/" // 'particles')
  file = "ensight-3D/" // 'particles' // "/" // 'diameter' // "."
  write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nout_time
  ! Write diameters
  do rank=1,nproc
     if (rank.eq.irank) then
        if (rank.eq.1) then
           ! Open the file
           call BINARY_FILE_OPEN(iunit,trim(file),"w",ierr)
           ! Write header
           cbuffer = 'Spray diameter'
           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
           ! Spray diameter
           do i=1,npart_
              rbuffer = part(i)%d
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end do
           ! Zero drop case
           if (npart.eq.0) then
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end if
           ! Close the file
           call BINARY_FILE_CLOSE(iunit,ierr)
        else
           ! Open the file
           call BINARY_FILE_OPEN(iunit,trim(file),"a",ierr)
           ! Spray diameter
           do i=1,npart_
              rbuffer = part(i)%d
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end do
           ! Close the file
           call BINARY_FILE_CLOSE(iunit,ierr)
        end if
     end if
     call MPI_BARRIER(comm,ierr)
  end do
       
  ! Generate the file - Velocity
  if (irank.eq.iroot) call CREATE_FOLDER("ensight-3D/" // 'particles')
  file = "ensight-3D/" // 'particles' // "/" // 'velocity' // "."
  write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nout_time
  ! Write velocity
  do rank=1,nproc
     if (rank.eq.irank) then
        if (rank.eq.1) then
           ! Open the file
           call BINARY_FILE_OPEN(iunit,trim(file),"w",ierr)
           ! Write header
           cbuffer = 'Spray velocity'
           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
           ! Spray velocity
           do i=1,npart_
              rbuffer = part(i)%u
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%v
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%w
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end do
           ! Zero drop case
           if (npart.eq.0) then
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end if
           ! Close the file
           call BINARY_FILE_CLOSE(iunit,ierr)
        else
           ! Open the file
           call BINARY_FILE_OPEN(iunit,trim(file),"a",ierr)
           ! Spray velocity
           do i=1,npart_
              rbuffer = part(i)%u
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%v
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%w
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end do
           ! Close the file
           call BINARY_FILE_CLOSE(iunit,ierr)
        end if
     end if
     call MPI_BARRIER(comm,ierr)
  end do
  
  ! Generate the file - Angular Velocity
  if (irank.eq.iroot) call CREATE_FOLDER("ensight-3D/" // 'particles')
  file = "ensight-3D/" // 'particles' // "/" // 'angvel' // "."
  write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nout_time
  ! Write velocity
  do rank=1,nproc
     if (rank.eq.irank) then
        if (rank.eq.1) then
           ! Open the file
           call BINARY_FILE_OPEN(iunit,trim(file),"w",ierr)
           ! Write header
           cbuffer = 'Spray angular velocity'
           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
           ! Spray velocity
           do i=1,npart_
              rbuffer = part(i)%wx
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%wy
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%wz
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end do
           ! Zero drop case
           if (npart.eq.0) then
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = 0.0_WP
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end if
           ! Close the file
           call BINARY_FILE_CLOSE(iunit,ierr)
        else
           ! Open the file
           call BINARY_FILE_OPEN(iunit,trim(file),"a",ierr)
           ! Spray angular velocity
           do i=1,npart_
              rbuffer = part(i)%wx
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%wy
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
              rbuffer = part(i)%wz
              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
           end do
           ! Close the file
           call BINARY_FILE_CLOSE(iunit,ierr)
        end if
     end if
     call MPI_BARRIER(comm,ierr)
  end do
  
  return
end subroutine dump_ensight_particle


!!$! ============================================= !
!!$! Dump 3D binary ensight gold data - quadrature !
!!$! ============================================= !
!!$subroutine dump_ensight_quadrature
!!$  use dump_ensight
!!$  use multiphase_sr
!!$  use string
!!$  use parallel
!!$  implicit none
!!$  
!!$  integer :: iunit,ierr,rank
!!$  character(len=str_medium) :: file
!!$  character(len=80) :: cbuffer
!!$  integer :: ibuffer
!!$  real(SP) :: rbuffer
!!$  integer :: i,j,k
!!$  integer :: ii,jj,kk
!!$  integer :: quad_count,my_quad_count
!!$  
!!$  ! Count the quadrature points
!!$  my_quad_count=0
!!$  do k=kmin_,kmax_
!!$     do j=jmin_,jmax_
!!$        do i=imin_,imax_
!!$           my_quad_count=my_quad_count+Gsr(i,j,k)%px*Gsr(i,j,k)%py*Gsr(i,j,k)%pz
!!$        end do
!!$     end do
!!$  end do
!!$  call parallel_sum(my_quad_count,quad_count)
!!$  
!!$  ! Generate the file - Quadrature points coordinates
!!$  if (irank.eq.iroot) call CREATE_FOLDER("ensight-3D/" // 'quadrature')
!!$  file = "ensight-3D/" // 'quadrature' // "/" // 'quadrature' // "."
!!$  write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nout_time
!!$  ! Write positions
!!$  do rank=1,nproc
!!$     if (rank.eq.irank) then
!!$        if (rank.eq.1) then
!!$           ! Open the file
!!$           call BINARY_FILE_OPEN(iunit,trim(file),"w",ierr)
!!$           ! Write header
!!$           cbuffer = 'C Binary'
!!$           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
!!$           cbuffer = 'Quadrature coordinates from ARTS'
!!$           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
!!$           cbuffer = 'particle coordinates'
!!$           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
!!$           ibuffer = max(quad_count,1)
!!$           call BINARY_FILE_WRITE(iunit,ibuffer,1,kind(ibuffer),ierr)
!!$           do i=1,max(quad_count,1)
!!$              ibuffer = i
!!$              call BINARY_FILE_WRITE(iunit,ibuffer,1,kind(ibuffer),ierr)
!!$           end do
!!$           ! Quadrature points coordinates
!!$           do k=kmin_,kmax_
!!$              do j=jmin_,jmax_
!!$                 do i=imin_,imax_
!!$                    ! Get each quadrature point
!!$                    do kk=1,Gsr(i,j,k)%pz
!!$                       do jj=1,Gsr(i,j,k)%py
!!$                          do ii=1,Gsr(i,j,k)%px
!!$                             ! Get the position
!!$                             rbuffer = x(i)+quad(Gsr(i,j,k)%px)%x(ii)*dx(i)
!!$                             call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                             rbuffer = y(j)+quad(Gsr(i,j,k)%py)%x(jj)*dy(j)
!!$                             call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                             rbuffer = z(k)+quad(Gsr(i,j,k)%pz)%x(kk)*dz
!!$                             call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                          end do
!!$                       end do
!!$                    end do
!!$                 end do
!!$              end do
!!$           end do
!!$           ! Zero points case
!!$           if (quad_count.eq.0) then
!!$              rbuffer = 0.0_WP
!!$              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$              rbuffer = 0.0_WP
!!$              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$              rbuffer = 0.0_WP
!!$              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$           end if
!!$           ! Close the file
!!$           call BINARY_FILE_CLOSE(iunit,ierr)
!!$        else
!!$           ! Open the file
!!$           call BINARY_FILE_OPEN(iunit,trim(file),"a",ierr)
!!$           ! Quadrature points coordinates
!!$           do k=kmin_,kmax_
!!$              do j=jmin_,jmax_
!!$                 do i=imin_,imax_
!!$                    ! Get each quadrature point
!!$                    do kk=1,Gsr(i,j,k)%pz
!!$                       do jj=1,Gsr(i,j,k)%py
!!$                          do ii=1,Gsr(i,j,k)%px
!!$                             ! Get the position
!!$                             rbuffer = x(i)+quad(Gsr(i,j,k)%px)%x(ii)*dx(i)
!!$                             call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                             rbuffer = y(j)+quad(Gsr(i,j,k)%py)%x(jj)*dy(j)
!!$                             call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                             rbuffer = z(k)+quad(Gsr(i,j,k)%pz)%x(kk)*dz
!!$                             call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                          end do
!!$                       end do
!!$                    end do
!!$                 end do
!!$              end do
!!$           end do
!!$           ! Close the file
!!$           call BINARY_FILE_CLOSE(iunit,ierr)
!!$        end if
!!$     end if
!!$     call MPI_BARRIER(comm,ierr)
!!$  end do
!!$  
!!$  ! Generate the file - G value
!!$  if (irank.eq.iroot) call CREATE_FOLDER("ensight-3D/" // 'quadrature')
!!$  file = "ensight-3D/" // 'quadrature' // "/" // 'Gsr' // "."
!!$  write(file(len_trim(file)+1:len_trim(file)+6),'(i6.6)') nout_time
!!$  ! Write diameters
!!$  do rank=1,nproc
!!$     if (rank.eq.irank) then
!!$        if (rank.eq.1) then
!!$           ! Open the file
!!$           call BINARY_FILE_OPEN(iunit,trim(file),"w",ierr)
!!$           ! Write header
!!$           cbuffer = 'G value'
!!$           call BINARY_FILE_WRITE(iunit,cbuffer,80,kind(cbuffer),ierr)
!!$           ! G value
!!$           do k=kmin_,kmax_
!!$              do j=jmin_,jmax_
!!$                 do i=imin_,imax_
!!$                    ! Get each quadrature point
!!$                    do kk=1,Gsr(i,j,k)%pz
!!$                       do jj=1,Gsr(i,j,k)%py
!!$                          do ii=1,Gsr(i,j,k)%px
!!$                             ! Get the G value
!!$                             rbuffer = Gsr(i,j,k)%G(ii,jj,kk)
!!$                             call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                          end do
!!$                       end do
!!$                    end do
!!$                 end do
!!$              end do
!!$           end do
!!$           ! Zero points case
!!$           if (quad_count.eq.0) then
!!$              rbuffer = 0.0_WP
!!$              call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$           end if
!!$           ! Close the file
!!$           call BINARY_FILE_CLOSE(iunit,ierr)
!!$        else
!!$           ! Open the file
!!$           call BINARY_FILE_OPEN(iunit,trim(file),"a",ierr)
!!$           ! G value
!!$           do k=kmin_,kmax_
!!$              do j=jmin_,jmax_
!!$                 do i=imin_,imax_
!!$                    ! Get each quadrature point
!!$                    do kk=1,Gsr(i,j,k)%pz
!!$                       do jj=1,Gsr(i,j,k)%py
!!$                          do ii=1,Gsr(i,j,k)%px
!!$                             ! Get the G value
!!$                             rbuffer = Gsr(i,j,k)%G(ii,jj,kk)
!!$                             call BINARY_FILE_WRITE(iunit,rbuffer,1,kind(rbuffer),ierr)
!!$                          end do
!!$                       end do
!!$                    end do
!!$                 end do
!!$              end do
!!$           end do 
!!$           ! Close the file
!!$           call BINARY_FILE_CLOSE(iunit,ierr)
!!$        end if
!!$     end if
!!$     call MPI_BARRIER(comm,ierr)
!!$  end do
!!$  
!!$  return
!!$end subroutine dump_ensight_quadrature
