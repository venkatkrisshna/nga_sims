! ================================== !
! Computation of reference cell size !
! Local information, but filtered    !
! ================================== !
module mesh_size
  use geometry
  use partition
  use parallel
  implicit none
  
  ! Mesh uniformity
  logical :: uniform_x
  logical :: uniform_y
  logical :: uniform_z
  
end module mesh_size


subroutine mesh_size_init
  use mesh_size
  implicit none
  
  integer :: i,j,k
  real(WP), parameter :: eps=1.0e-12_WP
  
  ! Allocate the array
  allocate(meshsize(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  
  ! Form local characteristic mesh size
  if (icyl.eq.0) then

     if (nx.gt.1 .and. ny.gt.1 .and. nz.gt.1) then
        ! 3D
        do k=kmino_,kmaxo_
           do j=jmino_,jmaxo_
              do i=imino_,imaxo_
                 meshsize(i,j,k)=(dx(i)*dy(j)*dz)**(1.0_WP/3.0_WP)
              end do
           end do
        end do
        
     else if (nx.gt.1 .and. ny.gt.1 .and. nz.eq.1) then
        ! 2D - z
        do k=kmino_,kmaxo_
           do j=jmino_,jmaxo_
              do i=imino_,imaxo_
                 meshsize(i,j,k)=(dx(i)*dy(j))**(1.0_WP/2.0_WP)
              end do
           end do
        end do
        
     else if (nx.gt.1 .and. nz.gt.1 .and. ny.eq.1) then
        ! 2D - y
        do k=kmino_,kmaxo_
           do j=jmino_,jmaxo_
              do i=imino_,imaxo_
                 meshsize(i,j,k)=(dx(i)*dz)**(1.0_WP/2.0_WP)
              end do
           end do
        end do
        
     else if (ny.gt.1 .and. nz.gt.1 .and. nx.eq.1) then
        ! 2D - x
        do k=kmino_,kmaxo_
           do j=jmino_,jmaxo_
              do i=imino_,imaxo_
                 meshsize(i,j,k)=(dz*dy(j))**(1.0_WP/2.0_WP)
              end do
           end do
        end do

     else if (nx.gt.1 .and. ny.eq.1 .and. nz.eq.1) then
        ! 1D - x
        do k=kmino_,kmaxo_
           do j=jmino_,jmaxo_
              do i=imino_,imaxo_
                 meshsize(i,j,k)=dx(i)
              end do
           end do
        end do

     else if (ny.gt.1 .and. nx.eq.1 .and. nz.eq.1) then
        ! 1D - y
        do k=kmino_,kmaxo_
           do j=jmino_,jmaxo_
              do i=imino_,imaxo_
                 meshsize(i,j,k)=dy(j)
              end do
           end do
        end do

     else if (nz.gt.1 .and. nx.eq.1 .and. ny.eq.1) then
        ! 1D - z
        do k=kmino_,kmaxo_
           do j=jmino_,jmaxo_
              do i=imino_,imaxo_
                 meshsize(i,j,k)=dz
              end do
           end do
        end do

     end if

  else

    if (nx.gt.1 .and. ny.gt.1 .and. nz.gt.1) then
        ! 3D
        do k=kmino_,kmaxo_
           do j=jmino_,jmaxo_
              do i=imino_,imaxo_
                 meshsize(i,j,k)=(dx(i)*dy(j)*abs(ym(j))*dz)**(1.0_WP/3.0_WP)
              end do
           end do
        end do

     else if (nx.gt.1 .and. ny.gt.1 .and. nz.eq.1) then
        ! 2D - z
        do k=kmino_,kmaxo_
           do j=jmino_,jmaxo_
              do i=imino_,imaxo_
                 meshsize(i,j,k)=(dx(i)*dy(j))**(1.0_WP/2.0_WP)
              end do
           end do
        end do

     else if (nx.gt.1 .and. nz.gt.1 .and. ny.eq.1) then
        ! 2D - y
        do k=kmino_,kmaxo_
           do j=jmino_,jmaxo_
              do i=imino_,imaxo_
                 meshsize(i,j,k)=(dx(i)*abs(ym(j))*dz)**(1.0_WP/2.0_WP)
              end do
           end do
        end do

     else if (ny.gt.1 .and. nz.gt.1 .and. nx.eq.1) then
        ! 2D - x
        do k=kmino_,kmaxo_
           do j=jmino_,jmaxo_
              do i=imino_,imaxo_
                 meshsize(i,j,k)=(abs(ym(j))*dz*dy(j))**(1.0_WP/2.0_WP)
              end do
           end do
        end do

     else if (nx.gt.1 .and. ny.eq.1 .and. nz.eq.1) then
        ! 1D - x
        do k=kmino_,kmaxo_
           do j=jmino_,jmaxo_
              do i=imino_,imaxo_
                 meshsize(i,j,k)=dx(i)
              end do
           end do
        end do

     else if (ny.gt.1 .and. nx.eq.1 .and. nz.eq.1) then
        ! 1D - y
        do k=kmino_,kmaxo_
           do j=jmino_,jmaxo_
              do i=imino_,imaxo_
                 meshsize(i,j,k)=dy(j)
              end do
           end do
        end do

     else if (nz.gt.1 .and. nx.eq.1 .and. ny.eq.1) then
        ! 1D - z
        do k=kmino_,kmaxo_
           do j=jmino_,jmaxo_
              do i=imino_,imaxo_
                 meshsize(i,j,k)=abs(ym(j))*dz
              end do
           end do
        end do

     end if

  end if

  ! Filter it twice for smoothness
  do i=1,2
     call filter_global_3D(meshsize,meshsize,'+','n')
  end do
  
  ! Determine smallest mesh size
  call parallel_min(minval(meshsize),min_meshsize)
  
  ! Check mesh uniformity
  uniform_x=.false.
  if (abs((maxval(dx)-minval(dx))/maxval(dx)).lt.eps) uniform_x=.true.
  uniform_y=.false.
  if (abs((maxval(dy)-minval(dy))/maxval(dy)).lt.eps) uniform_y=.true.
  uniform_z=.true.
    
  return
end subroutine mesh_size_init
