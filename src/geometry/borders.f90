module borders
  use precision
  use geometry
  use masks
  implicit none


  ! Definition of type bc
  ! -> location (of walls)
  ! -> area of the cross section
  type bc
     integer :: jmin, jmax
     integer :: jmino, jmaxo
     real(WP) :: A
  end type bc

  type bcy
     integer :: imin, imax
     integer :: imino,imaxo
     real(WP) :: A
  end type bcy

  ! Number of inlets and array of inlets
  integer :: ninlet,ninlety
  type(bc),  dimension(:), allocatable :: inlet
  type(bcy), dimension(:), allocatable :: inlety

  ! Number of outlets and array of outlets
  integer :: noutlet,noutlety
  type(bc),  dimension(:), allocatable :: outlet
  type(bcy), dimension(:), allocatable :: outlety
  
  ! Total outlet area
  real(WP) :: outlet_area

contains

  ! ====================================== !
  ! Detect the inlets on the left boundary !
  ! ====================================== !
  subroutine borders_inlet
    implicit none
    integer :: nflow,j

    ! Allocate
    allocate(inlet(ny))

    ! Locate the inlet boundary points
    ninlet = 0
    if (mask(imino,jmin-1).eq.2) then
       ninlet = 1
       inlet(ninlet)%jmin = jmin
       inlet(ninlet)%jmax = 0
    end if

    do j=jmin,jmax     
       if ((mask(imino,j).eq.2) .and. (mask(imino,j+1).eq.1)) then
          inlet(ninlet)%jmax = j
       else if ((mask(imino,j).eq.1) .and. (mask(imino,j+1).eq.2)) then
          ninlet = ninlet + 1
          inlet(ninlet)%jmin = j+1
          inlet(ninlet)%jmax = 0
       end if
    end do

    ! If no inlet return 
    if (ninlet.eq.0) return

    if (inlet(ninlet)%jmax.eq.0) then
       inlet(ninlet)%jmax = jmax
    end if

    ! Compute cross section area of the inlets
    if (icyl.eq.0) then
       do nflow=1,ninlet
          inlet(nflow)%A = (y(inlet(nflow)%jmax+1)-y(inlet(nflow)%jmin)) * (z(kmax+1)-z(kmin))
       end do
    else
       do nflow=1,ninlet
          inlet(nflow)%A = 0.5_WP* &
               (y(inlet(nflow)%jmax+1)**2-y(inlet(nflow)%jmin)**2 ) * (z(kmax+1)-z(kmin))
       end do
    end if

    ! Get the min and max index with ghost cells
    do nflow=1,ninlet
       ! Min
       inlet(nflow)%jmino = inlet(nflow)%jmin
       loop1:do j=1,nover
          if (mask(imino,inlet(nflow)%jmin-j).eq.2) then
             inlet(nflow)%jmino = inlet(nflow)%jmin-j
          else
             exit loop1
          end if
       end do loop1
       ! Max
       inlet(nflow)%jmaxo = inlet(nflow)%jmax
       loop2:do j=1,nover
          if (mask(imino,inlet(nflow)%jmax+j).eq.2) then
             inlet(nflow)%jmaxo = inlet(nflow)%jmax+j
          else
             exit loop2
          end if
       end do loop2
    end do

    return
  end subroutine borders_inlet

  ! ======================================== !
  ! Detect the inlets on the bottom boundary !
  ! ======================================== !
  subroutine borders_inlety
    use parser
    implicit none
    integer :: nflow,i

    ! Allocate
    allocate(inlety(nx))

    ! Locate the inlet boundary points
    call parser_is_defined('Inlet y velocity type',use_inlety)
    ninlety = 0
    if (.not.use_inlety) return
    if (mask(imin-1,jmino).eq.2) then
       ninlety = 1
       inlety(ninlety)%imin = imin
       inlety(ninlety)%imax = 0
    end if

    do i=imin,imax     
       if ((mask(i,jmino).eq.2) .and. (mask(i+1,jmino).eq.1)) then
          inlety(ninlety)%imax = i
       else if ((mask(i,jmino).eq.1) .and. (mask(i+1,jmino).eq.2)) then
          ninlety = ninlety + 1
          inlety(ninlety)%imin = i+1
          inlety(ninlety)%imax = 0
       end if
    end do

    ! If no inlet return 
    if (ninlety.eq.0) return

    if (inlety(ninlety)%imax.eq.0) then
       inlety(ninlety)%imax = imax
    end if

    ! Compute cross section area of the inlets
    if (icyl.eq.0) then
       do nflow=1,ninlety
          inlety(nflow)%A = (x(inlety(nflow)%imax+1)-x(inlety(nflow)%imin)) * (z(kmax+1)-z(kmin))
       end do
    else
       call die('Inflow in y direction in cylindrical domain not implemented')
    end if

    ! Get the min and max index with ghost cells
    do nflow=1,ninlety
       ! Min
       inlety(nflow)%imino = inlety(nflow)%imin
       loop1:do i=1,nover
          if (mask(inlety(nflow)%imin-i,jmino).eq.2) then
             inlety(nflow)%imino = inlety(nflow)%imin-i
          else
             exit loop1
          end if
       end do loop1
       ! Max
       inlety(nflow)%imaxo = inlety(nflow)%imax
       loop2:do i=1,nover
          if (mask(inlety(nflow)%imax+i,jmino).eq.2) then
             inlety(nflow)%imaxo = inlety(nflow)%imax+i
          else
             exit loop2
          end if
       end do loop2
    end do

    return
  end subroutine borders_inlety

  ! ======================================== !
  ! Detect the outlets on the right boundary !
  ! ======================================== !
  subroutine borders_outlet
    implicit none
    integer :: nflow,j

    ! Allocate
    allocate(outlet(ny))

    ! Locate the outlet boundary points
    noutlet = 0
    if (mask(imaxo,jmin-1).EQ.2) then
       noutlet = 1
       outlet(noutlet)%jmin = jmin
       outlet(noutlet)%jmax = 0
    end if

    do j=jmin,jmax     
       if ((mask(imaxo,j).eq.2) .and. (mask(imaxo,j+1).eq.1)) then
          outlet(noutlet)%jmax = j
       else if ((mask(imaxo,j).eq.1) .and. (mask(imaxo,j+1).eq.2)) then
          noutlet = noutlet + 1
          outlet(noutlet)%jmin = j+1
          outlet(noutlet)%jmax = 0
       end if
    end do
    
    ! If no outlet return 
    if (noutlet.eq.0) return

    if (outlet(noutlet)%jmax.eq.0) then
       outlet(noutlet)%jmax = jmax
    end if
    
    ! Compute cross section area of the outlets
    outlet_area = 0.0_WP
    if (icyl.eq.0) then
       do nflow=1,noutlet
          outlet(nflow)%A = (y(outlet(nflow)%jmax+1)-y(outlet(nflow)%jmin)) * (z(kmax+1)-z(kmin))
          outlet_area = outlet_area + outlet(nflow)%A
       end do
    else
       do nflow=1,noutlet
          outlet(nflow)%A = 0.5_WP* (y(outlet(nflow)%jmax+1)**2-y(outlet(nflow)%jmin)**2 ) * (z(kmax+1)-z(kmin))
          outlet_area = outlet_area + outlet(nflow)%A
       end do
    end if

    ! Get the min and max index with ghost cells
    do nflow=1,noutlet
       ! Min
       outlet(nflow)%jmino = outlet(nflow)%jmin
       loop1:do j=1,nover
          if (mask(imaxo,outlet(nflow)%jmin-j).eq.2) then
             outlet(nflow)%jmino = outlet(nflow)%jmin-j
          else
             exit loop1
          end if
       end do loop1
       ! Max
       outlet(nflow)%jmaxo = outlet(nflow)%jmax
       loop2:do j=1,nover
          if (mask(imaxo,outlet(nflow)%jmax+j).eq.2) then
             outlet(nflow)%jmaxo = outlet(nflow)%jmax+j
          else
             exit loop2
          end if
       end do loop2
    end do

    return
  end subroutine borders_outlet

  ! ====================================== !
  ! Detect the outlets on the top boundary !
  ! ====================================== !
  subroutine borders_outlety
    use parser
    implicit none
    integer :: nflow,i

    ! Check for y-outlet
    call parser_readlogical('Use y outlet',use_outlety,.false.)
    if (.not.use_outlety) then
       noutlety=0
       return
    end if

    ! Allocate
    allocate(outlety(nx))

    ! Locate the outlet boundary points
    noutlety = 0
    if (mask(imin-1,jmaxo).EQ.2) then
       noutlety = 1
       outlety(noutlety)%imin = imin
       outlety(noutlety)%imax = 0
    end if

    do i=imin,imax     
       if ((mask(i,jmaxo).eq.2) .and. (mask(i+1,jmaxo).eq.1)) then
          outlety(noutlety)%imax = i
       else if ((mask(i,jmaxo).eq.1) .and. (mask(i+1,jmaxo).eq.2)) then
          noutlety = noutlety + 1
          outlety(noutlety)%imin = i+1
          outlety(noutlety)%imax = 0
       end if
    end do
    
    ! If no outlet return 
    if (noutlety.eq.0) return

    if (outlety(noutlety)%imax.eq.0) then
       outlety(noutlety)%imax = imax
    end if
    
    ! Compute cross section area of the outlets
    if (icyl.eq.0) then
       do nflow=1,noutlety
          outlety(nflow)%A = (x(outlety(nflow)%imax+1)-x(outlety(nflow)%imin)) * (z(kmax+1)-z(kmin))
          outlet_area = outlet_area + outlety(nflow)%A
       end do
    else
       do nflow=1,noutlety
          outlety(nflow)%A = (x(outlety(nflow)%imax+1)-x(outlety(nflow)%imin)) * (z(kmax+1)-z(kmin)) * y(jmax+1)
          outlet_area = outlet_area + outlety(nflow)%A
       end do
    end if

    ! Get the min and max index with ghost cells
    do nflow=1,noutlety
       ! Min
       outlety(nflow)%imino = outlety(nflow)%imin
       loop1:do i=1,nover
          if (mask(outlety(nflow)%imin-i,jmaxo).eq.2) then
             outlety(nflow)%imino = outlety(nflow)%imin-i
          else
             exit loop1
          end if
       end do loop1
       ! Max
       outlety(nflow)%imaxo = outlety(nflow)%imax
       loop2:do i=1,nover
          if (mask(outlety(nflow)%imax+i,jmaxo).eq.2) then
             outlety(nflow)%imaxo = outlety(nflow)%imax+i
          else
             exit loop2
          end if
       end do loop2
    end do

    return
  end subroutine borders_outlety

end module borders

! ====================== !
! Initialize the borders !
! -> Detect inlets       !
! -> Detect outlets      !
! ====================== !
subroutine borders_init
  use borders
  implicit none

  call borders_inlet
  call borders_inlety
  call borders_outlet
  call borders_outlety
  
  return
end subroutine borders_init
