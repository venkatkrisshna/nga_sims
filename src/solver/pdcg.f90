! =========================================== !
! Parallel Deflated Conjugate Gradient module !
! =========================================== !
module pdcg
  use pressure
  implicit none

  ! Maximum residual
  real(WP) :: normres,normres0
  ! Iteration number
  integer  :: iter
  
  ! Work variables
  ! --------------
  
  ! RP = B
  ! DP = X
  ! res = B-AX
  real(WP), dimension(:,:,:), allocatable :: res,pp,zz,Ap,Wd
  
  ! For CG
  real(WP) :: alpha,beta,rho1,rho2
  
  ! Restriction
  ! -----------
  integer, dimension(:),     allocatable :: ncell_per_grp
  integer, dimension(:,:,:), allocatable :: igrp
  integer :: ng,ngx,ngy,ngz
  real(WP), dimension(:,:),   allocatable :: deflap,deflap2
  real(WP), dimension(:),     allocatable :: defsol,defbuf
  
end module pdcg


! ========================== !
! Initialize the PDCG module !
! ========================== !
subroutine pdcg_init
  use pdcg
  use partition
  use parser
  use masks
  use parallel
  use metric_velocity_conv
  implicit none
  
  integer :: i,j,k
  integer :: ig,jg,kg
  logical :: is_def
  
  ! Allocation of work arrays
  allocate(res (imin_:imax_,jmin_:jmax_,kmin_:kmax_))
  allocate(Ap  (imin_:imax_,jmin_:jmax_,kmin_:kmax_))
  allocate(pp  (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(zz  (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(Wd  (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(igrp(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  
  ! Create lower dimensional problem - first find number of groups
  call parser_is_defined('Pressure groups in x',is_def)
  if (is_def) then
     call parser_read('Pressure groups in x',ngx)
     call parser_read('Pressure groups in y',ngy)
     call parser_read('Pressure groups in z',ngz)
     ng=ngx*ngy*ngz
  else
     call parser_read('Pressure groups',ng,50)
     ngx=ceiling(real(nx,WP)*real(ng,WP)**(1.0_WP/3.0_WP)/(real(nx*ny*nz,WP)**(1.0_WP/3.0_WP)))
     ngy=ceiling(real(ny,WP)*real(ng,WP)**(1.0_WP/3.0_WP)/(real(nx*ny*nz,WP)**(1.0_WP/3.0_WP)))
     ngz=ceiling(real(nz,WP)*real(ng,WP)**(1.0_WP/3.0_WP)/(real(nx*ny*nz,WP)**(1.0_WP/3.0_WP)))
     ng=ngx*ngy*ngz
  end if
  
  ! Treat ng=0 case
  if (ng.eq.0) then
     igrp=0
     return
  end if
  
  ! Allocate work arrays
  allocate(ncell_per_grp(ng))
  allocate(deflap(ng,ng))
  allocate(deflap2(ng,ng))
  allocate(defsol(ng))
  allocate(defbuf(ng))
  
  ! Associate each cell with group
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Treat walls
           if (mask(i,j).ne.0) then
              igrp(i,j,k)=0
              cycle
           end if
           ! Treat IBs
           if (sum(lap(i,j,k,:,0)).eq.0.0_WP) then
              igrp(i,j,k)=0
              cycle
           end if
           ! Find group number
           ig=i-imin;jg=j-jmin;kg=k-kmin
           ig=(ig*ngx)/nx+1;jg=(jg*ngy)/ny+1;kg=(kg*ngz)/nz+1
           ig=min(ig,ngx);jg=min(jg,ngy);kg=min(kg,ngz)
           ! Use lexicographic order
           igrp(i,j,k)=ig+(jg-1)*ngx+(kg-1)*ngx*ngy
        end do
     end do
  end do
  call boundary_update_border_int(igrp,'+','ym')
  
  ! Count number of cells per group
  defsol=0
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (igrp(i,j,k).gt.0) defsol(igrp(i,j,k))=defsol(igrp(i,j,k))+1
        end do
     end do
  end do
  call parallel_sum(defsol,defbuf)
  do i=1,ng
     ncell_per_grp(i)=int(defbuf(i))
  end do
  
  return
end subroutine pdcg_init


! =========================================== !
! Parallel Deflated Conjugate Gradient solver !
! Solves Ax=b                                 !
! RP = B                                      !
! DP = X                                      !
! res = B-AX                                  !
! =========================================== !
subroutine pdcg_solve
  use pdcg
  use parallel
  implicit none
  
  ! Private stuff
  integer  :: done
  
  ! Loop initialization
  iter = 0
  done = 0
  
  ! res=RP-A.DP
  call pressure_operator(DP,res)
  res=RP-res
  
  ! Check initial residual norm
  call parallel_max(maxval(abs(res)),normres0)
  normres=normres0
  if (normres0.eq.0.0_WP) done = 1
  
  ! Restrict res
  call pdcg_restrict(res)
  
  ! Solve restricted problem
  call pdcg_restrict_solve
  
  ! Update DP
  DP=DP+Wd
  
  ! res=RP-A.DP
  call pressure_operator(DP,res)
  res=RP-res
  
  ! Conjugate gradient
  loop:do while(done.ne.1)
     
     ! Increment loop counter
     iter=iter+1
     
     ! M.ww=res
     call pdcg_apply_precond(res,zz)
     
     ! Ap=A.zz
     call pressure_operator(zz,Ap)
     
     ! Restrict Ap
     call pdcg_restrict(Ap)
     
     ! Solve restricted problem
     call pdcg_restrict_solve
     
     ! (ww.res)
     rho2=rho1
     call parallel_sum(sum(res*zz(imin_:imax_,jmin_:jmax_,kmin_:kmax_)),rho1)
     
     ! beta=rho1/rho2
     if (iter.eq.1) then
        beta=0.0_WP
     else
        beta=rho1/rho2
     end if
     
     ! pp=zz+beta.pp
     pp=zz+beta*pp-Wd
     
     ! Ap=A.pp
     call pressure_operator(pp,Ap)
     
     ! alpha=rho1/(pp.Ap)
     call parallel_sum(sum(pp(imin_:imax_,jmin_:jmax_,kmin_:kmax_)*Ap),alpha)
     alpha=rho1/alpha
     
     ! DP=DP+alpha.pp
     DP=DP+alpha*pp
     
     ! res=res-alpha.Ap
     res=res-alpha*Ap
     
     ! Check if done
     call parallel_max(maxval(abs(res)),normres)
     normres=normres/normres0
     if (normres.lt.cvg .or. iter.ge.max_iter) done=1
     
     ! Monitoring convergence
     !if (irank.eq.1) print*,iter,normres
     
  end do loop
  
  ! Transfer to monitor
  max_resP = normres
  it_p  = iter
  
  return
end subroutine pdcg_solve


! ================================================= !
! Select wich operator to use to get the residuals  !
! Compute the product of vec by the matrix K: K.vec !
! ================================================= !
subroutine pdcg_apply_precond(vec,Kvec)
  use pdcg
  use partition
  implicit none

  real(WP), dimension(imin_:imax_,jmin_:jmax_,kmin_:kmax_) :: vec
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: Kvec
  
  select case(trim(pressure_precond))
  case('none')
     Kvec = 0.0_WP
     Kvec(imin_:imax_,jmin_:jmax_,kmin_:kmax_) = vec
  case('diag')
     call pressure_precond_diag(vec,Kvec)
  case('tridiag')
     call pressure_precond_tridiag(vec,Kvec)
  case('icc')
     call pressure_precond_icc(vec,Kvec)
  case default
     call die('bicgstab_apply_precond: unknown preconditioner')
  end select
  
  return
end subroutine pdcg_apply_precond


! ==================== !
! Restriction operator !
! Solution in defsol   !
! ==================== !
subroutine pdcg_restrict(A)
  use pdcg
  use parallel
  implicit none
  real(WP), dimension(imin_:imax_,jmin_:jmax_,kmin_:kmax_) :: A
  integer :: i,j,k
  
  ! Treat ng=0 case
  if (ng.eq.0) return
  
  ! Apply restriction
  defbuf=0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (igrp(i,j,k).gt.0) then
              defbuf(igrp(i,j,k)) = defbuf(igrp(i,j,k))+A(i,j,k)
           end if
        end do
     end do
  end do
  call parallel_sum(defbuf,defsol)
  
  return
end subroutine pdcg_restrict


! ==================== !
! Restricted Laplacian !
! ==================== !
subroutine pdcg_compute_deflap
  use pdcg
  use metric_velocity_conv
  use math
  use parallel
  implicit none
  
  integer :: i,j,k,ig1,ig2
  integer :: ii,jj,kk,dim,dir
  integer, dimension(3) :: pos
  
  ! Treat ng=0 case
  if (ng.eq.0) return
  
  ! Apply restriction on Laplacian operator
  deflap2=0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! First group
           ig1=igrp(i,j,k)
           ! Skip if not in group
           if (ig1.eq.0) cycle
           ! Add diagonal coefficient
           deflap2(ig1,ig1)=deflap2(ig1,ig1)+sum(lap(i,j,k,:,0))
           ! Follow stencil
           do dim=1,3
              do dir=-1,+1,2
                 ! Find neighboring point
                 pos=0;pos(dim)=dir
                 ii=i+pos(1);jj=j+pos(2);kk=k+pos(3)
                 ! Second group
                 ig2=igrp(ii,jj,kk)
                 ! Skip if not in group
                 if (ig2.eq.0) cycle
                 ! Add off-diagonal coefficient
                 deflap2(ig1,ig2)=deflap2(ig1,ig2)+lap(i,j,k,dim,dir)
              end do
           end do
        end do
     end do
  end do
  call parallel_sum(deflap2,deflap)
  
  ! Backup copy of deflap
  deflap2=deflap
  
  return
end subroutine pdcg_compute_deflap


! ================================================ !
! Solve restricted problem and prolongate solution !
! ================================================ !
subroutine pdcg_restrict_solve
  use pdcg
  use math
  implicit none
  
  integer :: ig1,ig2
  integer :: i,j,k
  
  ! Treat ng=0 case
  if (ng.eq.0) then
     Wd=0.0_WP
     return
  end if
  
  ! Compute deflap.defbuf=defsol
  call solve_linear_system_safe(deflap,defsol,defbuf,ng)
  deflap=deflap2
  
  ! Apply prolongation to form Wd
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (igrp(i,j,k).gt.0) then
              Wd(i,j,k)=defbuf(igrp(i,j,k))
           else
              Wd(i,j,k)=0.0_WP
           end if
        end do
     end do
  end do
  
  ! Communicate Wd
  call boundary_update_border(Wd,'+','ym')
  
  return
end subroutine pdcg_restrict_solve
