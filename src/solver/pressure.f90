! ======================================== !
! Pressure solver for low-Mach number flow !
! LLNL's HYPRE libraries :                 !
!  - multigrid :                           !
!         = SMG                            !
!         = PFMG                           !
!  - Krylov-based solvers :                !
!         = PCG                            !
!         = BICGSTAB                       !
!         = GMRES                          !
!         = SMG-PCG                        !
!         = PFMG-PCG                       !
!         = SMG-GMRES                      !
!         = PFMG-GMRES                     !
!         = SMG-BICGSTAB                   !
!         = PFMG-BICGSTAB                  !
!  - hybrid solvers                        !
!                                          !
! Homemade solvers :                       !
!  - bicgstab                              !
!  - bicgstab(2)                           !
!  - bbmg                                  !
!  - pcg-bbmg                              !
!  - ctridiag                              !
! ======================================== !
module pressure
  use precision
  use geometry
  use partition
  use structure
  use string
  use config
  implicit none
  
  ! RHS arrays
  real(WP), dimension(:,:,:), allocatable :: RP
  
  ! Solution array
  real(WP), dimension(:,:,:), pointer :: DP
  
  ! Solver parameters
  character(len=str_medium) :: pressure_solver
  character(len=str_medium) :: pressure_precond
  real(WP) :: cvg
  integer  :: max_iter
  logical  :: use_HYPRE
  logical  :: use_HYPRE_AMG
  logical  :: use_ctridiag
  
  ! Array of diagonal values for ICC
  real(WP), dimension(:,:,:), allocatable :: diag
  
  ! Constant laplacian (0=constant / 1=variable-timestep / 2=variable-subiterations)
  integer :: pressure_cst_lap=0
  
  ! Values to monitor
  real(WP) :: max_resP,int_RP
  integer  :: it_p

  ! Pressure FFT flag
  logical :: pressure_fft
  
  ! Pressure recycling
  integer :: pressure_recycling,nrec
  real(WP), dimension(:,:,:,:), allocatable :: DPrec,RPrec
  real(WP), dimension(:,:), allocatable :: Arec
  real(WP), dimension(:), allocatable :: Crec,Brec
  
end module pressure


! ============================== !
! Initialize the pressure solver !
! ============================== !
subroutine pressure_init
  use pressure
  use parser
  implicit none
  
  ! Create & Start the timer
  call timing_create('pressure')
  call timing_start ('pressure')
    
  ! Allocate the RHS and DP
  allocate(RP(imin_ :imax_ ,jmin_ :jmax_ ,kmin_ :kmax_ )); RP=0.0_WP
  allocate(DP(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); DP=0.0_WP
  
  ! Initialize pressure solvers from input file
  call parser_read("Pressure solver",pressure_solver)
  call parser_read("Pressure precond",pressure_precond,'none')
  
  ! Initialize convergence criteria
  call parser_read("Pressure cvg",cvg)
  call parser_read("Pressure iterations",max_iter)
  
  ! Initialize pressure recycling
  call parser_read("Pressure recycling",pressure_recycling,0)
  if (pressure_recycling.gt.0) then
     allocate(Arec(pressure_recycling,pressure_recycling)); Arec=0.0_WP
     allocate(Brec(pressure_recycling)); Brec=0.0_WP
     allocate(Crec(pressure_recycling)); Crec=0.0_WP
     allocate(DPrec(pressure_recycling,imin_:imax_,jmin_:jmax_,kmin_:kmax_)); DPrec=0.0_WP
     allocate(RPrec(pressure_recycling,imin_:imax_,jmin_:jmax_,kmin_:kmax_)); RPrec=0.0_WP
     nrec=0
  end if
  
  ! Initialize the Fourier routines
  call parser_read("Pressure fft",pressure_fft,.false.)
  if (pressure_fft) call fourier_init

  ! Rescale the operator
  if (pressure_cst_lap.eq.0) then
     call pressure_fourier
     call pressure_rescale
  end if
  
  ! Detection of the solver
  use_HYPRE     = .false.
  use_HYPRE_AMG = .false.
  use_ctridiag  = .false.
  select case (trim(pressure_solver))
  ! HYPRE multigrid solvers
  case ('AMG')
     use_HYPRE_AMG=.true.
  case ('SMG')
     use_HYPRE=.true.
  case ('PFMG')
     use_HYPRE=.true.
  case ('PCG')
     use_HYPRE=.true.
  case ('BICGSTAB')
     use_HYPRE=.true.
  case ('GMRES')
     use_HYPRE=.true.
  case ('HYBRID')
     use_HYPRE=.true.
  ! Homemade Krylov solvers
  case ('pdcg')
     call pdcg_init
     if (pressure_cst_lap.eq.0) call pdcg_compute_deflap
  case ('bicgstab')
     call bicgstab_init
  case ('bicgstab(2)')
     call bicgstab2_init
  ! Homemade BBMG solver
  case ('bbmg')
     call bbmg_init
  case ('pcg-bbmg')
     call bbmg_pcg_init
  ! Homemade solver for Houssem
  case ('ctridiag')
     use_ctridiag=.true.
     call ctridiag_init
  ! Unknown solver
  case default
     call die('Pressure_init: unknown pressure solver')
  end select
  
  ! Initialization of Hypre - Struct
  if (use_HYPRE) then
     if (vel_conv_order.gt.2) call die('HYPRE requires a 2nd order accurate velocity scheme.')
     call hypre_init_operator
     if (pressure_cst_lap.eq.0) call hypre_solver_init
  end if
  
  ! Initialization of Hypre - AMG
  if (use_HYPRE_AMG) then
     call hypre_amg_init_operator
     call hypre_amg_init_solver
     if (pressure_cst_lap.eq.0) call hypre_amg_solver_update
  end if
  
  ! Initialization of the preconditioner
  if (.not.use_HYPRE) then
     select case(trim(pressure_precond))
     case('none')
        ! Nothing to do
     case('diag')
        ! Nothing to do
     case('tridiag')
        ! Nothing to do
     case('icc')
        call pressure_icc_init
        if (pressure_cst_lap.eq.0) call pressure_icc_diag
     case default
        call die('Unknown pressure preconditioner')
     end select
  end if
  
  ! Create new file to monitor
  call monitor_create_file_iter('convergence_pressure',3)
  call monitor_set_header(1,'it_p', 'i')
  call monitor_set_header(2,'res_p','r')
  call monitor_set_header(3,'int_RP','r')
  
  ! Stop the timer
  call timing_stop('pressure')

  return
end subroutine pressure_init


! ================================================== !
! Compute the scaling coefficients for the laplacian !
! Ensure that the matrix is symmetric                !
! ================================================== !
subroutine pressure_rescale
  use pressure
  use metric_velocity_conv
  implicit none
  integer  :: i,j,k,st
  
  ! Apply scaling
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           lap(i,j,k,:,:) = -vol(i,j,k)*lap(i,j,k,:,:)
        end do
     end do
  end do
    
  do st=-stcp,stcp
     call boundary_update_border(lap(:,:,:,1,st),'+','ym')
     call boundary_update_border(lap(:,:,:,2,st),'+','ym')
     call boundary_update_border(lap(:,:,:,3,st),'+','ym')
  end do
  
  return
end subroutine pressure_rescale


! ============================= !
! Solve the system using HYPRE  !
! P(n+1/2) = P(n-1/2) + DP(n+1) !
! ============================= !
subroutine pressure_step
  use pressure
  use data
  use time_info
  implicit none
  real(WP) :: my_int
  integer :: i,j,k

  ! Initialize the solution
  DP = 0.0_WP
  
  ! Zero iteration => no pressure solver
  if (max_iter.eq.0) return
  
  ! Multiphase - recompute the operator as well as the RHS
  if (use_multiphase) call multiphase_gfm_pressure
  
  ! Start the timer
  call timing_start('pressure')
  
  ! Rescale operator
  if (pressure_cst_lap.eq.2 .or. &
      pressure_cst_lap.eq.1.and.niter.eq.1) then
     call pressure_fourier
     call pressure_rescale
  end if
  
  ! Prepare preconditioner
  if (pressure_cst_lap.eq.2 .or. &
      pressure_cst_lap.eq.1.and.niter.eq.1) then
     if (.not.use_HYPRE) then
        select case(trim(pressure_precond))
        case('none')
           ! Nothing to do
        case('diag')
           ! Nothing to do
        case('tridiag')
           ! Nothing to do
        case('icc')
           call pressure_icc_diag
        end select
     end if
  end if
  
  ! Check the integral of RP
  my_int = 0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           my_int = my_int+RP(i,j,k)*vol(i,j,k)
        end do
     end do
  end do
  call parallel_sum(my_int,int_RP)
  int_RP = int_RP/vol_total
  
  ! Prepare the RHS
  call pressure_RHS
  
  ! Solve
  call pressure_SOLVE
  
  ! Update boundaries of DP
  call pressure_DP_BC
  call boundary_update_border(DP,'+','ym')
  
  ! Update the pressure
  if (use_pcorr) then
     P=P+DP
  else
     P=DP
  end if
   
  ! Pass the values to monitor
  call monitor_select_file('convergence_pressure')
  call monitor_set_single_value(1,real(it_p,WP))
  call monitor_set_single_value(2,max_resP)
  call monitor_set_single_value(3,int_RP)

  ! Stop the timer
  call timing_stop('pressure')

  return
end subroutine pressure_step


! ============================== !
! Transfer the RHS to the solver !
! ============================== !
subroutine pressure_RHS
  use pressure
  implicit none
  integer :: i,j,k
  
  ! Shortcut for ctridiag
  if (use_ctridiag) then
     call ctridiag_RHS
     return
  end if
  
  ! Preprocess the RHS with FFT
  if (pressure_fft) call fourier_transform(RP)
  
  ! Rescale the RHS
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           RP(i,j,k) = -vol(i,j,k)*RP(i,j,k)
        end do
     end do
  end do
  
  ! If necessary, transfer to HYPRE
  if (use_HYPRE)     call hypre_rhs_transfer
  if (use_HYPRE_AMG) call hypre_amg_rhs_transfer
  
  return
end subroutine pressure_RHS


! ========================== !
! Solve the Poisson equation !
! ========================== !
subroutine pressure_SOLVE
  use pressure
  use parallel
  use time_info
  implicit none
  
  ! Find optimal starting solution from recycling
  if (pressure_recycling.gt.0) call pressure_recycle
  
  ! Call the solver
  if (use_HYPRE) then
     call hypre_set_ig
     call hypre_solve
     call hypre_sol_transfer
  else if (use_HYPRE_AMG) then
     call hypre_amg_set_ig
     call hypre_amg_solve
     call hypre_amg_sol_transfer
  else if (use_ctridiag) then
     call ctridiag_solve
     call ctridiag_get_sol
     return
  else
     select case (trim(pressure_solver))
     case ('bbmg')
        call bbmg_solve
     case ('pcg-bbmg')
        call bbmg_pcg_solve
     case ('pdcg')
        if (pressure_cst_lap.eq.2 .or. &
            pressure_cst_lap.eq.1.and.niter.eq.1) call pdcg_compute_deflap
        call pdcg_solve
     case ('bicgstab')
        call bicgstab_solve
     case ('bicgstab(2)')
        call bicgstab2_solve
     end select
  end if
  
  ! Store RP & DP for recycling
  if (pressure_recycling.gt.0) then
     nrec=minloc(abs(Crec),1)
     RPrec(nrec,:,:,:)=RP(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     DPrec(nrec,:,:,:)=DP(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
  end if
  
  ! Postprocess the RHS with FFT
  if (pressure_fft) then
     RP=DP(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
     call fourier_inverse(RP)
     DP(imin_:imax_,jmin_:jmax_,kmin_:kmax_)=RP
  end if
  
  return
end subroutine pressure_SOLVE


! ========================================= !
! Pressure BC on the pressure correction DP !
! ========================================= !
subroutine pressure_DP_BC
  use pressure
  use parallel
  use masks
  use metric_velocity_conv
  implicit none
  integer  :: i,j,k
  real(WP) :: DPmean,DPtmp
  real(WP) :: my_vol,vol_tot
  
  ! Fix DPmean = 0
  DPtmp = 0.0_WP
  my_vol= 0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (mask(i,j).eq.0 .and. sum(lap(i,j,k,:,0)).ne.0.0_WP) then
              DPtmp  = DPtmp  + vol(i,j,k)*DP(i,j,k)
              my_vol = my_vol + vol(i,j,k)
           end if
        end do
     end do
  end do
  call parallel_sum(DPtmp ,DPmean )
  call parallel_sum(my_vol,vol_tot)
  DPmean = DPmean/vol_tot
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (mask(i,j).eq.0 .and. sum(lap(i,j,k,:,0)).ne.0.0_WP) then
              DP(i,j,k) = DP(i,j,k) - DPmean
           else
              DP(i,j,k) = 0.0_WP
           end if
        end do
     end do
  end do
  
  return
end subroutine pressure_DP_BC


! ============================= !
! Pressure operator: B = lap(A) !
! A is with overlap             !
! B is interior only            !
! COMMUNICATION OF A DONE HERE  !
! ============================= !
subroutine pressure_operator(A,B)
  use pressure
  use metric_velocity_conv
  implicit none
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: A
  real(WP), dimension(imin_ :imax_ ,jmin_ :jmax_ ,kmin_ :kmax_ ) :: B
  integer :: i,j,k
  
  ! Communicate A
  call boundary_update_border(A,'+','ym')
  
  ! Apply operator
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           B(i,j,k) = &
                sum(A(i-stcp:i+stcp,j,k)*lap(i,j,k,1,:)) + & ! X-direction
                sum(A(i,j-stcp:j+stcp,k)*lap(i,j,k,2,:)) + & ! Y-direction
                sum(A(i,j,k-stcp:k+stcp)*lap(i,j,k,3,:))     ! Z-direction
           
        end do
     end do
  end do
  
  return
end subroutine pressure_operator


! ================================================ !
! Initialize the Incomplete Cholesky factorization !
! ================================================ !
subroutine pressure_icc_init
  use pressure
  implicit none
  
  ! Allocate the array for the diagonal values
  allocate(diag(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  
  return
end subroutine pressure_icc_init


! ============================================= !
! Prepare the Incomplete Cholesky factorization !
! ============================================= !
subroutine pressure_icc_diag
  use pressure
  use metric_velocity_conv
  use masks
  implicit none
  integer  :: i,j,k,st,n
  
  ! Preset diagonal
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           if (mask(i,j).eq.0) then
              diag(i,j,k) = sum(lap(i,j,k,:,0))
           else
              diag(i,j,k) = 1.0_WP
           end if
        end do
     end do
  end do
  call boundary_update_border(diag,'+','ym')  
  
  ! Compute this diagonal
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           do st=-stcp,-1
              !n=-st
              do n=1,stcp
                 diag(i,j,k) = diag(i,j,k) &
                      - lap(i,j,k,1,st)*lap(i+st,j,k,1,n)/diag(i+st,j,k) &
                      - lap(i,j,k,2,st)*lap(i,j+st,k,2,n)/diag(i,j+st,k) &
                      - lap(i,j,k,3,st)*lap(i,j,k+st,3,n)/diag(i,j,k+st)
              end do
           end do
        end do
     end do
  end do

  return
end subroutine pressure_icc_diag


! =========================================================== !
! Pressure preconditioner : Incomplete Cholesky factorization !
! K = (D+L)*D^1*(D+U)                                         !
! with A = L+diag(A)+U,   U=transpose(L)                      !
! =========================================================== !
subroutine pressure_precond_icc(A,B)
  use pressure
  use metric_velocity_conv
  implicit none

  real(WP), dimension(imin_:imax_,jmin_:jmax_,kmin_:kmax_) :: A
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: B
  integer :: i,j,k,st
  
  ! Communicate everything first
  B(imin_:imax_,jmin_:jmax_,kmin_:kmax_) = A
  call boundary_update_border(B,'+','ym')
  
  ! Lower triangular
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           do st=-stcp,-1
              B(i,j,k) =  B(i,j,k) &
                   - lap(i,j,k,1,st)*B(i+st,j,k) &
                   - lap(i,j,k,2,st)*B(i,j+st,k) &
                   - lap(i,j,k,3,st)*B(i,j,k+st) 
           end do
           B(i,j,k) = B(i,j,k) / diag(i,j,k)
        end do
     end do
  end do
  
  ! Diagonal
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           B(i,j,k) = diag(i,j,k)*B(i,j,k) 
        end do
     end do
  end do
  
  ! Communicate everything
  call boundary_update_border(B,'+','ym')

  ! Upper triangular
  do i=imax_,imin_,-1
     do j=jmax_,jmin_,-1
        do k=kmax_,kmin_,-1
           do st=1,stcp
              B(i,j,k) =  B(i,j,k) &
                   - lap(i,j,k,1,st)*B(i+st,j,k) &
                   - lap(i,j,k,2,st)*B(i,j+st,k) &
                   - lap(i,j,k,3,st)*B(i,j,k+st) 
           end do
           B(i,j,k) = B(i,j,k) / diag(i,j,k)
        end do
     end do
  end do
  
  return
end subroutine pressure_precond_icc


! ================================== !
! Pressure preconditioner : Diagonal !
! ================================== !
subroutine pressure_precond_diag(A,B)
  use pressure
  use metric_velocity_conv
  use masks
  implicit none
  
  real(WP), dimension(imin_:imax_,jmin_:jmax_,kmin_:kmax_) :: A
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: B
  integer :: i,j,k
  
  ! Diagonal
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (sum(lap(i,j,k,:,0)).ne.0.0_WP) then
              B(i,j,k) = A(i,j,k) / sum(lap(i,j,k,:,0))
           else
              B(i,j,k) = 0.0_WP
           end if
        end do
     end do
  end do
  
  return
end subroutine pressure_precond_diag


! ===================================== !
! Pressure preconditioner : tridiagonal !
! ===================================== !
subroutine pressure_precond_tridiag(A,B)
  use pressure
  use metric_velocity_conv
  use metric_generic
  use masks
  use memory
  use parallel
  use fourier
  implicit none
  
  real(WP), dimension(imin_:imax_,jmin_:jmax_,kmin_:kmax_) :: A
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: B
  integer  :: i,j,k,st
  
  ! Store initial solution
  B(imin_:imax_,jmin_:jmax_,kmin_:kmax_) = A
  call boundary_update_border(B,'+','ym')
  
  ! Select direction
  if (.not.fft_x .and. .not.fct_x .and. nx.ne.1) then
     
     ! X-direction
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (mask(i,j).eq.0) then
                 do st=-stcp,stcp
                    Ax(j,k,i,st) = lap(i,j,k,1,st)
                 end do
                 Ax(j,k,i,0) = Ax(j,k,i,0) + lap(i,j,k,2,0) + lap(i,j,k,3,0)
                 Rx(j,k,i) = B(i,j,k)
              else
                 Ax(j,k,i,:) = 0.0_WP
                 Ax(j,k,i,0) = 1.0_WP
                 Rx(j,k,i) = 0.0_WP
              end if
           end do
        end do
     end do
     call linear_solver_x(2*stcp+1)
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              B(i,j,k) = Rx(j,k,i)
           end do
        end do
     end do
     call boundary_update_border(B,'+','ym')
     
  else if (.not.fft_y .and. .not.fct_y .and. ny.ne.1) then
     
     ! Y-direction
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (mask(i,j).eq.0) then
                 do st=-stcp,stcp
                    Ay(i,k,j,st) = lap(i,j,k,2,st)
                 end do
                 Ay(i,k,j,0) = Ay(i,k,j,0) + lap(i,j,k,1,0) + lap(i,j,k,3,0)
                 Ry(i,k,j) = B(i,j,k)
              else
                 Ay(i,k,j,:) = 0.0_WP
                 Ay(i,k,j,0) = 1.0_WP
                 Ry(i,k,j) = 0.0_WP
              end if
           end do
        end do
     end do
     call linear_solver_y(2*stcp+1)
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              B(i,j,k) = Ry(i,k,j)
           end do
        end do
     end do
     call boundary_update_border(B,'+','ym')
     
  else if (.not.fft_z .and. .not.fct_z .and. nz.ne.1) then
     
     ! Z-direction
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (mask(i,j).eq.0) then
                 do st=-stcp,stcp
                    Az(i,j,k,st) = lap(i,j,k,3,st)
                 end do
                 Az(i,j,k,0) = Az(i,j,k,0) + lap(i,j,k,1,0) + lap(i,j,k,2,0)
                 Rz(i,j,k) = B(i,j,k)
              else
                 Az(i,j,k,:) = 0.0_WP
                 Az(i,j,k,0) = 1.0_WP
                 Rz(i,j,k) = 0.0_WP
              end if
           end do
        end do
     end do
     call linear_solver_z(2*stcp+1)
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              B(i,j,k) = Rz(i,j,k)
           end do
        end do
     end do
     call boundary_update_border(B,'+','ym')
     
  end if
  
  return
end subroutine pressure_precond_tridiag


! ======================================= !
! Fourier transform of Laplacian operator !
! ======================================= !
subroutine pressure_fourier
  use fourier
  use pressure
  use parallel
  use metric_velocity_conv
  use math
  use masks
  implicit none
  
  real(WP) :: k2eff,kk,dk
  integer  :: i,j,k,st,k0,n
  complex(WP), parameter :: ii = (0.0_WP,1.0_WP)
  
  ! Nothing to do if no FFT
  if (.not.pressure_fft) return

  ! Compute the keff^2 - X
  if (fft_x) then
     dk = twoPi/xL
     do i=imin_,imax_
        kk = real(i-imin,WP)*dk
        if (i-imin.gt.(nx/2)) kk = real(nx-i+imin,WP)*dk
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              k2eff = real(sum(lap(i,j,k,1,:)*exp(ii*kk*xm(i-stcp:i+stcp)))/exp(ii*kk*xm(i)),WP)
              lap(i,j,k,1,:) = 0.0_WP
              lap(i,j,k,1,0) = k2eff
           end do
        end do
     end do
  else if (fct_x) then
     dk = Pi/xL
     do i=imin_,imax_
        kk = real(i-imin,WP)*dk
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              k2eff = sum(lap(i,j,k,1,:)*cos(kk*(xm(i-stcp:i+stcp)-x(imin))))/cos(kk*(xm(i)-x(imin)))
              lap(i,j,k,1,:) = 0.0_WP
              lap(i,j,k,1,0) = k2eff
           end do
        end do
     end do
  end if
  
  ! Compute the keff^2 - Y
  if (fft_y) then
     dk = twoPi/yL
     do j=jmin_,jmax_
        kk = real(j-jmin,WP)*dk
        if (j-jmin.gt.(ny/2)) kk = real(ny-j+jmin,WP)*dk
        do k=kmin_,kmax_
           do i=imin_,imax_
              k2eff = real(sum(lap(i,j,k,2,:)*exp(ii*kk*ym(j-stcp:j+stcp)))/exp(ii*kk*ym(j)),WP)
              lap(i,j,k,2,:) = 0.0_WP
              lap(i,j,k,2,0) = k2eff
           end do
        end do
     end do
  else if (fct_y) then
     dk = Pi/yL
     do j=jmin_,jmax_
        kk = real(j-jmin,WP)*dk
        do k=kmin_,kmax_
           do i=imin_,imax_
              k2eff = sum(lap(i,j,k,2,:)*cos(kk*(ym(j-stcp:j+stcp)-y(jmin))))/cos(kk*(ym(j)-y(jmin)))
              lap(i,j,k,2,:) = 0.0_WP
              lap(i,j,k,2,0) = k2eff
           end do
        end do
     end do
  end if
  
  ! Compute the keff^2 - Z
  if (fft_z) then
     dk = twoPi/zL
     do k=kmin_,kmax_
        kk = real(k-kmin,WP)*dk
        if (k-kmin.gt.(nz/2)) kk = real(nz-k+kmin,WP)*dk
        do j=jmin_,jmax_
           do i=imin_,imax_
              k2eff = real(sum(lap(i,j,k,3,:)*exp(ii*kk*zm(k-stcp:k+stcp)))/exp(ii*kk*zm(k)),WP)
              lap(i,j,k,3,:) = 0.0_WP
              lap(i,j,k,3,0) = k2eff
           end do
        end do
     end do
     ! Point on the other side of the axis
     if (icyl.eq.1 .and. jproc.eq.1) then
        do k=kmin_,kmax_
           k0 = k-kmin
           if (k0.gt.(nz/2)) k0 = nz-k+kmin
           do i=imin_,imax_
              if (mask(i,jmin).ne.1) then
                 do st=0,stcp-1
                    do n=-stcp,-st-1
                       lap(i,jmin+st,k,2,-n-2*st-1) = lap(i,jmin+st,k,2,-n-2*st-1) + lap(i,jmin+st,k,2,n)*(-1.0_WP)**k0
                       lap(i,jmin+st,k,2,n) = 0.0_WP
                    end do
                 end do
              end if
           end do
        end do
     end if
  else if (fct_z) then
     dk = Pi/zL
     do k=kmin_,kmax_
        kk = real(k-kmin,WP)*dk
        do j=jmin_,jmax_
           do i=imin_,imax_
              k2eff = sum(lap(i,j,k,3,:)*cos(kk*(zm(k-stcp:k+stcp)-z(kmin))))/cos(kk*(zm(k)-z(kmin)))
              lap(i,j,k,3,:) = 0.0_WP
              lap(i,j,k,3,0) = k2eff
           end do
        end do
     end do
  end if

  ! Necessary communications done in pressure_rescale
  
  ! Oddball
  if (oddball) lap(imin_,jmin_,kmin_,1,0) = 1.0_WP
  
  return
end subroutine pressure_fourier


! ====================================== !
! Solution recycling for pressure solver !
! ====================================== !
subroutine pressure_recycle
  use pressure
  use parallel
  use metric_velocity_conv
  use math
  use memory
  implicit none
  
  real(WP) :: buf
  integer :: i,j
  
  ! If needed, recompute RPrec
  if (pressure_cst_lap.ne.0) then
     do i=1,pressure_recycling
        DP(imin_:imax_,jmin_:jmax_,kmin_:kmax_)=DPrec(i,imin_:imax_,jmin_:jmax_,kmin_:kmax_)
        call pressure_operator(DP,RPrec(i,imin_:imax_,jmin_:jmax_,kmin_:kmax_))
     end do
  end if
  
  ! Form system of equations
  Arec=0.0_WP
  Brec=0.0_WP
  do i=1,pressure_recycling
     ! Form operator
     do j=1,pressure_recycling
        call parallel_sum(sum(DPrec(i,:,:,:)*RPrec(j,:,:,:)),buf)
        Arec(i,j)=buf
     end do
     ! Form RHS
     call parallel_sum(sum(DPrec(i,:,:,:)*RP(:,:,:)),buf)
     Brec(i)=buf
  end do
  
  ! Inverse matrix in place
  call QRinverse_matrix(pressure_recycling,Arec,Arec)
  
  ! Compute solution
  Crec=0.0_WP
  do i=1,pressure_recycling
     Crec(i)=dot_product(Arec(1:pressure_recycling,i),Brec(1:pressure_recycling))
  end do
  
  ! Form optimal initial guess
  DP=0.0_WP
  do i=1,pressure_recycling
     DP(imin_:imax_,jmin_:jmax_,kmin_:kmax_)=DP(imin_:imax_,jmin_:jmax_,kmin_:kmax_) &
          +Crec(i)*DPrec(i,imin_:imax_,jmin_:jmax_,kmin_:kmax_)
  end do
  
  ! Communicate
  call boundary_update_border(DP,'+','ym')
  call boundary_neumann_alldir(DP)
  
  return
end subroutine pressure_recycle
