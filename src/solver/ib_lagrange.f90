module ib_lagrange
  use ib
  use velocity
  use lagrange_geom
  use memory

  ! make use of types to resuse code
  type r1_ptr
     real(WP), dimension(:), pointer :: p
  end type r1_ptr
  type lagrange_struct
     real(WP), dimension(:,:,:), pointer :: rhovel
     type(r1_ptr), dimension(3) :: plane
     integer, dimension(3) :: shift
     character :: dir
  end type lagrange_struct

  type(lagrange_struct), dimension(3) :: lgrgn_ptrs

  ! triangles to use for ib_lagrange extrusion
  type(lagrange_tri), dimension(max_ltris) :: lag_tris
  ! tets forming extruded face
  type(face_tet), dimension(3) :: face_tets
  type(face_tet), dimension(3) :: fsub_tets
  ! ensure we don't clip planes outside of grid
  integer , dimension(2,3) :: ijk_bounds
  real(WP), dimension(:,:,:), pointer :: lgn_srcU,lgn_srcV,lgn_srcW
  real(WP), dimension(:,:,:), pointer :: lgn_volu,lgn_volv,lgn_volw
  ! rescale semilagrangian fluxes based on fluxed volume
  logical :: rescale_vol
  ! distance based tagging
  real(WP) :: tag_distance
  ! skip lagrange treatment
  logical :: skip_lagrange

contains

  ! ===========================================================
  ! simple localization routine to find the i,j,k coordinate of 
  ! a point based on x,y,z arrays
  ! faster than bisection with a good initial guess
  subroutine ib_localize(ip_old,jp_old,kp_old,ip,jp,kp,xp,yp,zp)
    implicit none

    integer,  intent(in)  :: ip_old,jp_old,kp_old
    real(WP), intent(in)  :: xp,yp,zp
    integer,  intent(out) :: ip,jp,kp

    ! Find right i index
    ip=ip_old
    do while (xp-x(ip  ).lt.0.0_WP .and. ip.gt.imino_)
       ip=ip-1
    end do
    do while (x(ip+1)-xp.le.0.0_WP .and. ip.lt.imaxo_-1)
       ip=ip+1
    end do

    ! Find right j index
    jp=jp_old
    do while (yp-y(jp  ).lt.0.0_WP .and. jp.gt.jmino_)
       jp=jp-1
    end do
    do while (y(jp+1)-yp.le.0.0_WP .and. jp.lt.jmaxo_-1)
       jp=jp+1
    end do

    ! Find right k index
    kp=kp_old
    do while (zp-z(kp  ).lt.0.0_WP .and. kp.gt.kmino_)
       kp=kp-1
    end do
    do while (z(kp+1)-zp.le.0.0_WP .and. kp.lt.kmaxo_-1)
       kp=kp+1
    end do

    return
  end subroutine ib_localize

  ! ============================================ !
  ! Actual interpolation routine                 !
  ! Takes in (xp,yp,zp) and (ip,jp,kp)           !
  ! as well as any 'big' array A                 !
  ! and returns Ap, interpolation of A @ p       !
  ! Assumes everything is available: consistency !
  ! checks need to be done before                !
  ! ============================================ !
  ! ------ U ------ | ------ V ------ | ------ W ------ | ------ C ------ |
  ! ip    - ip+1    | ip+sx - ip+sx+1 | ip+sx - ip+sx+1 | ip+sx - ip+sx+1 |
  ! jp+sy - jp+sy+1 | jp    - jp+1    | jp+sy - jp+sy+1 | jp+sy - jp+sy+1 |
  ! kp+sz - kp+sz+1 | kp+sz - kp+sz+1 | kp    - kp+1    | kp+sz - kp+sz+1 |
  ! --------------- | --------------- | --------------- | --------------- |
  subroutine ib_interpolate(xd,yd,zd,id,jd,kd,Ap,dir,vel_func)
    implicit none
    real(WP), intent(in) :: xd,yd,zd
    integer, intent(in) :: id,jd,kd
    real(WP), intent(out) :: Ap
    character(len=*) :: dir
    interface
       function vel_func(i,j,k,pos,dt_proj)
         use ib
         implicit none
         integer, intent(in) :: i,j,k
         real(WP), dimension(3), intent(in) :: pos
         real(WP), intent(in) :: dt_proj
         real(WP) :: vel_func
       end function vel_func
    end interface
    !---------------------------------
    ! pointers to velocity variable (U,U_ib...)
    real(WP), dimension(:,:,:), pointer :: A,Aib
    ! pointers to barycenters (Cbaru,Sbaru...)
    real(WP), dimension(:,:,:,:), pointer :: cb,sb
    ! pointers to Volume fraction and surface area (VFu,SAu...)
    real(WP), dimension(:,:,:), pointer :: VF,SA
    ! pointers to grid
    real(WP), dimension(:), pointer :: lerp_x,lerp_y,lerp_z
    ! array of points and velocity data
    real(WP), dimension(3,16) :: interp_coords
    real(WP), dimension(  16) :: interp_vel
    real(WP) :: xp,yp,zp
    integer :: nn,i,j,k
    integer :: sx,sy,sz
    integer :: i1,i2
    integer :: j1,j2
    integer :: k1,k2

    ! Get the position
    xp = xd; yp = yd; zp = zd

    ! Get interpolation cells w/r to staggering
    sx = -1; sy = -1; sz = -1
    ! Get the first interpolation point
    select case (trim(adjustl(dir)))
    case ('U')
       A=> U; Aib=> U_ib; Cb=> Cbaru; Sb=> Sbaru; VF=> VFu; SA=> SAu
       lerp_x => x; lerp_y => ym; lerp_z => zm
       if (yp.ge.Cb(id,jd,kd,2)) sy = 0
       if (zp.ge.Cb(id,jd,kd,3)) sz = 0
       i1=id
       j1=jd+sy
       k1=kd+sz
    case ('V')
       A=> V; Aib=> V_ib; Cb=> Cbarv; Sb=> Sbarv; VF=> VFv; SA=> SAv
       lerp_x => xm; lerp_y => y; lerp_z => zm
       if (xp.ge.Cb(id,jd,kd,1)) sx = 0
       if (zp.ge.Cb(id,jd,kd,3)) sz = 0
       i1=id+sx
       j1=jd
       k1=kd+sz
    case ('W')
       A=> W; Aib=> W_ib; Cb=> Cbarw; Sb=> Sbarw; VF=> VFw; SA=> SAw
       lerp_x => xm; lerp_y => ym; lerp_z => z
       if (xp.ge.Cb(id,jd,kd,1)) sx = 0
       if (yp.ge.Cb(id,jd,kd,2)) sy = 0
       i1=id+sx
       j1=jd+sy
       k1=kd
    end select

    ! Set the second interpolation point
    i2=i1+1
    j2=j1+1
    k2=k1+1

    ! fill array with barycenter data for linear interpolation
    nn=0
    do k=k1,k2
       do j=j1,j2
          do i=i1,i2
             if(VF(i,j,k).gt.epsilon(1.0_WP)) then
                nn=nn+1
                interp_coords(:,nn) = Cb(i,j,k,:)
                interp_vel(nn) = A(i,j,k)
                if(SA(i,j,k).gt.epsilon(1.0_WP)) then
                   nn=nn+1
                   interp_coords(:,nn) = Sb(i,j,k,:)
                   interp_vel(nn) = Aib(i,j,k)
                end if
             else
                nn=nn+1
                interp_coords(:,nn) = (/lerp_x(i),lerp_y(j),lerp_z(k)/)
                interp_vel(nn) = vel_func(i,j,k,interp_coords(:,nn),0.0_WP)
             end if
          end do
       end do
    end do
    ! this point is inside the IB .. use single cell value
    if(nn.le.1) then
       Ap = A(id,jd,kd)
    else
       Ap = least_squares_interp(xp,yp,zp,interp_coords(:,1:nn),interp_vel(1:nn),nn)
    end if

  end subroutine ib_interpolate

  ! least squares based interpolation 
  ! if 1<n<4 - fit : a0
  ! if 4<n<8 - fit : a0+a1x+a2y+a3z
  ! if n>=8  - fit : a0+a1x+a2y+a3z+a4xy+a5xz+a6yz+a7xyz
  function least_squares_interp(xp,yp,zp,xyz,vel,nn) result(f)
    implicit none
    real(WP) :: f
    real(WP), intent(in) :: xp,yp,zp
    real(WP), dimension(:,:), intent(in) :: xyz
    real(WP), dimension(  :), intent(in) :: vel
    integer, intent(in) :: nn
    real(WP), dimension(:,:), allocatable :: A
    real(WP), dimension(  :), allocatable :: sol
    integer :: i

    if(nn.lt.4) then
       f = sum(vel)/real(nn,WP)
    elseif(nn.lt.8) then

       allocate(A(nn,4))
       allocate(sol(4))

       A(:,1) = 1.0_WP
       A(:,2) = xyz(1,:)
       A(:,3) = xyz(2,:)
       A(:,4) = xyz(3,:)

       call solve_linear_system_safe( &
            matmul(transpose(A),A),matmul(transpose(A),vel),sol,4)

       f = sol(1) + xp*sol(2) + yp*sol(3) + zp*sol(4)

       deallocate(A,sol)
    else
       allocate(A(nn,8))
       allocate(sol(8))

       A(:,1) = 1.0_WP
       A(:,2) = xyz(1,:)
       A(:,3) = xyz(2,:)
       A(:,4) = xyz(3,:)
       A(:,5) = xyz(1,:)*xyz(2,:)
       A(:,6) = xyz(1,:)*xyz(3,:)
       A(:,7) = xyz(2,:)*xyz(3,:)
       A(:,8) = xyz(1,:)*xyz(2,:)*xyz(3,:)

       call solve_linear_system_safe( &
            matmul(transpose(A),A),matmul(transpose(A),vel),sol,8)

       f = sol(1) + xp*sol(2) + yp*sol(3) + zp*sol(4) + &
            xp*yp*sol(5)+xp*zp*sol(6)+yp*zp*sol(7)+xp*yp*zp*sol(8)

       deallocate(A,sol)
    end if

    ! clip interpolated value in the event that extrapolation has occured...
    f = min(max(f,minval(vel)),maxval(vel))

    return

  end function least_squares_interp
    

  ! -- fills array of face vertices and face G-vals for U-faces
  ! ilgrng_ux(i,j,k) - > xm(i)    ,y(j:j+1),z(k:k+1)
  ! ilgrng_uy(i,j,k) - > xm(i-1:i),y(j)    ,z(k:k+1)
  ! ilgrng_uz(i,j,k) - > xm(i-1:i),y(j:j+1),z(k)
  subroutine set_u_face(fpos,fval,i,j,k,im,jm,km,ip,jp,kp)
    implicit none
    real(WP), dimension(:,:), intent(out) :: fpos
    real(WP), dimension(  :), intent(out) :: fval
    integer, intent(in) :: i,j,k,im,jm,km,ip,jp,kp
    integer :: ss,si,sj,sk
    ss = 1
    do sk=km,kp
       do sj=jm,jp
          do si=im,ip
             fpos(:,ss) = (/xm(i+si),y(j+sj),z(k+sk)/)
             fval(  ss) = sum(interp_sc_yz(i+si,j+sj,:,:)*&
                  Gib(i+si,j+sj-st2:j+sj+st1,k+sk-st2:k+sk+st1))
             ss = ss + 1
          end do
       end do
    end do
  end subroutine set_u_face

  ! -- fills array of face vertices and face G-vals for V-faces
  ! ilgrng_vx(i,j,k) - > x(i)    ,ym(j-1:j),z(k:k+1)
  ! ilgrng_vy(i,j,k) - > x(i:i+1),ym(j)    ,z(k:k+1)
  ! ilgrng_vz(i,j,k) - > x(i:i+1),ym(j-1:j),z(k)
  subroutine set_v_face(fpos,fval,i,j,k,im,jm,km,ip,jp,kp)
    implicit none
    real(WP), dimension(:,:), intent(out) :: fpos
    real(WP), dimension(  :), intent(out) :: fval
    integer, intent(in) :: i,j,k,im,jm,km,ip,jp,kp
    integer :: ss,si,sj,sk
    ss = 1
    do sk=km,kp
       do sj=jm,jp
          do si=im,ip
             fpos(:,ss) = (/x(i+si),ym(j+sj),z(k+sk)/)
             fval(  ss) = sum(interp_sc_xz(i+si,j+sj,:,:)*&
                  Gib(i+si-st2:i+si+st1,j+sj,k+sk-st2:k+sk+st1))
             ss = ss + 1
          end do
       end do
    end do
  end subroutine set_v_face

  ! -- fills array of face vertices and face G-vals for W-faces
  ! ilgrng_wx(i,j,k) - > x(i)    ,y(j:j+1),zm(k-1:k)
  ! ilgrng_wy(i,j,k) - > x(i:i+1),y(j)    ,zm(k-1:k)
  ! ilgrng_wz(i,j,k) - > x(i:i+1),y(j:j+1),zm(k) 
  subroutine set_w_face(fpos,fval,i,j,k,im,jm,km,ip,jp,kp)
    implicit none
    real(WP), dimension(:,:), intent(out) :: fpos
    real(WP), dimension(  :), intent(out) :: fval
    integer, intent(in) :: i,j,k,im,jm,km,ip,jp,kp
    integer :: ss,si,sj,sk
    ss = 1
    do sk=km,kp
       do sj=jm,jp
          do si=im,ip
             fpos(:,ss) = (/x(i+si),y(j+sj),zm(k+sk)/)
             fval(  ss) = sum(interp_sc_xy(i+si,j+sj,:,:)*&
                  Gib(i+si-st2:i+si+st1,j+sj-st2:j+sj+st1,k+sk))
             ss = ss + 1
          end do
       end do
    end do
  end subroutine set_w_face

  ! ==================================================
  ! project triangle from a cartesian face 
  ! ==================================================
  subroutine project_face_triangle(proj_verts,triangle,dt_proj,fi,fj,fk)
    implicit none
    real(WP), dimension(3,3), intent(out) :: proj_verts
    type(lagrange_tri), intent(in) :: triangle
    real(WP), intent(in) :: dt_proj
    integer, intent(in) :: fi,fj,fk
    !----------------------------------
    integer :: nn
    real(WP), dimension(3) :: v1,v2,v3,v4

    do nn=1,3
       if(triangle%is_ib(nn).eq.0) then
          v1 = point_velocity(triangle%vertex(:,nn),fi,fj,fk)
          v2 = point_velocity(triangle%vertex(:,nn)+0.5_WP*dt_proj*v1,fi,fj,fk)
          v3 = point_velocity(triangle%vertex(:,nn)+0.5_WP*dt_proj*v2,fi,fj,fk)
          v4 = point_velocity(triangle%vertex(:,nn)+       dt_proj*v3,fi,fj,fk)
          proj_verts(:,nn)=triangle%vertex(:,nn)+r16*dt_proj*(v1+2.0_WP*v2+2.0_WP*v3+v4)          
       else
          ! need to project ib back in time to find location...
          v1(1) = ib_get_velocity_u(fi,fj,fk,triangle%vertex(:,nn),dt_proj)
          v1(2) = ib_get_velocity_v(fi,fj,fk,triangle%vertex(:,nn),dt_proj)
          v1(3) = ib_get_velocity_w(fi,fj,fk,triangle%vertex(:,nn),dt_proj)
          v2 = point_velocity(triangle%vertex(:,nn)+0.5_WP*dt_proj*v1,fi,fj,fk)
          v3 = point_velocity(triangle%vertex(:,nn)+0.5_WP*dt_proj*v2,fi,fj,fk)
          v4 = point_velocity(triangle%vertex(:,nn)+       dt_proj*v3,fi,fj,fk)
          proj_verts(:,nn) = triangle%vertex(:,nn)+r16*dt_proj*(v1+2.0_WP*v2+2.0_WP*v3+v4)          
       end if
    end do

  end subroutine project_face_triangle

  function point_velocity(pxyz,i0,j0,k0) result(pvel)
    implicit none
    real(WP), dimension(3), intent(in) :: pxyz
    integer, intent(in) :: i0,j0,k0
    integer :: pi,pj,pk
    real(WP), dimension(3) :: pvel

    call ib_localize(i0,j0,k0,pi,pj,pk,pxyz(1),pxyz(2),pxyz(3))
    call ib_interpolate(pxyz(1),pxyz(2),pxyz(3),pi,pj,pk,pvel(1),"U",ib_get_velocity_u)
    call ib_interpolate(pxyz(1),pxyz(2),pxyz(3),pi,pj,pk,pvel(2),"V",ib_get_velocity_v)
    call ib_interpolate(pxyz(1),pxyz(2),pxyz(3),pi,pj,pk,pvel(3),"W",ib_get_velocity_w)

  end function point_velocity
  
  ! -- computes the lagrangian flux through face:
  ! lgrngn_[uvw][xyz](face_i,face_j,face_k)
  subroutine ib_lagrange_vel(fpos,fval,fi,fj,fk,base_hand,ls_ptr,int_rhovel,int_vol,ib_surf)
    implicit none
    integer, intent(in) :: fi,fj,fk,base_hand
    type(lagrange_struct), intent(in) :: ls_ptr
    real(WP), dimension(:,:), intent(in) :: fpos
    real(WP), dimension(  :), intent(in) :: fval
    real(WP), intent(out) :: int_rhovel,int_vol
    logical, intent(in) :: ib_surf
    real(WP), dimension(3,3) :: proj_verts
    integer :: nface_tris,i,ii,ftet,id,itet,nn
    real(WP) :: abvol
    logical, dimension(4) :: masked

    ! get the triangles which make up face
    if(ib_surf) then
       call ib_surf_triangles(fpos,fval,lag_tris,nface_tris)
    else
       masked=.false.
       call face_triangles(fpos,fval,base_hand,lag_tris,nface_tris,masked)
    end if

    int_rhovel = 0.0_WP; int_vol = 0.0_WP
    do i=1,nface_tris
       ! get projected vertices
       call project_face_triangle(proj_verts,lag_tris(i),-dt_uvw,fi,fj,fk)
       ! break extruded shape into tets
       call cut_extruded_triangle(proj_verts,lag_tris(i),face_tets,fi,fj,fk)

       do ftet=1,3
          ! cut tet with old IB
          do ii=1,4
             face_tets(ftet)%G(ii) = get_oldGib(face_tets(ftet)%vertex(:,ii),fi,fj,fk)
          end do
          call cut_tet_by_oldG(face_tets(ftet),fsub_tets,nn)
          do itet=1,nn
             ! pass tets to plane cutting routines and get signed-int(rhovel)
             abvol = abs(fsub_tets(itet)%vol)
             if(abvol.gt.epsilon(1.0_WP)) then
                int_rhovel = int_rhovel + (fsub_tets(itet)%vol/abvol) * &
                     tet_momentum(fsub_tets(itet),ls_ptr,0)
                int_vol = int_vol + fsub_tets(itet)%vol
             end if
          end do
       end do
    end do
  end subroutine ib_lagrange_vel

  !============================================================
  ! cut a tet recursively by planes specified by ls_ptr struct
  ! to get i,j,k of momentum cell and tet volume in that cell
  !
  ! U-cell
  ! dir=+1 :  xm(ftet%i  ),y(ftet%j+1),z(ftet%k+1)
  ! dir=-1 :  xm(ftet%i-1),y(ftet%j  ),z(ftet%k  )
  ! V-cell
  ! dir=+1 :  x(ftet%i+1),ym(ftet%j  ),z(ftet%k+1)
  ! dir=-1 :  x(ftet%i  ),ym(ftet%j-1),z(ftet%k  )
  ! W-cell
  ! dir=+1 :  x(ftet%i+1),y(ftet%j+1),zm(ftet%k  )
  ! dir=-1 :  x(ftet%i  ),y(ftet%j  ),zm(ftet%k-1)
  !============================================================
  recursive function tet_momentum(ftet,ls_ptr,dir) result(tmu)
    implicit none
    real(WP) :: tmu ! momentum in tet
    type(face_tet), intent(inout) :: ftet
    type(lagrange_struct), intent(in) :: ls_ptr
    integer, intent(in) :: dir
    !---------------------------
    type(face_tet),dimension(3) :: ptets,mtets
    integer :: nptets,nmtets,i,si

    ! tet is done
    if(ftet%dir.eq.4) then
          tmu = abs(ftet%vol)*ls_ptr%rhovel(ftet%ijk(1),ftet%ijk(2),ftet%ijk(3))
       return
    end if
    ! initialize 
    tmu = 0.0_WP

    if(ftet%first) then
       si = ftet%ijk(ftet%dir)+ls_ptr%shift(ftet%dir)+1
       call cut_tet_by_plane(ftet,ptets,mtets,nptets,nmtets,&
            ls_ptr%plane(ftet%dir)%p(si),ftet%dir,1,ijk_bounds(:,ftet%dir))
       do i=1,nptets
          tmu = tmu+tet_momentum(ptets(i),ls_ptr,1)
       end do
       do i=1,nmtets
          tmu = tmu+tet_momentum(mtets(i),ls_ptr,-1)
       end do
    else
       si = ftet%ijk(ftet%dir)+ls_ptr%shift(ftet%dir)+max(0,dir)
       call cut_tet_by_plane(ftet,ptets,mtets,nptets,nmtets,&
            ls_ptr%plane(ftet%dir)%p(si),ftet%dir,dir,ijk_bounds(:,ftet%dir))
       do i=1,nptets
          tmu = tmu+tet_momentum(ptets(i),ls_ptr,dir)
       end do
       do i=1,nmtets
          tmu = tmu+tet_momentum(mtets(i),ls_ptr,dir)
       end do
    end if

    return
  end function tet_momentum

end module ib_lagrange

subroutine ib_lagrange_init()
  use ib_lagrange
  implicit none
  
  ! Allocate integer flags for lagrangian flux update
  allocate(ilgrgn_ux(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(ilgrgn_uy(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(ilgrgn_uz(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(ilgrgn_vx(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(ilgrgn_vy(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(ilgrgn_vz(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(ilgrgn_wx(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(ilgrgn_wy(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(ilgrgn_wz(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))

  ! initialize lagrangian fluxes
  ilgrgn_ux=0;ilgrgn_uy=0;ilgrgn_uz=0
  ilgrgn_vx=0;ilgrgn_vy=0;ilgrgn_vz=0
  ilgrgn_wx=0;ilgrgn_wy=0;ilgrgn_wz=0

  ! set lagrangian pointers (U_ib,V_ib,W_ib already use tmp1-3
  lgn_srcU => tmp4; lgn_srcV => tmp5; lgn_srcW => tmp6
  lgn_volu => tmp7; lgn_volv => tmp8; lgn_volw => tmp9

  lgrgn_ptrs = (/ &
       ! pointers to U-cell data
       lagrange_struct(rhoUold,(/r1_ptr(xm),r1_ptr(y),r1_ptr(z)/),(/-1,0,0/),'U'), &
       ! pointers to V-cell data
       lagrange_struct(rhoVold,(/r1_ptr(x),r1_ptr(ym),r1_ptr(z)/),(/0,-1,0/),'V'), &
       ! pointers to W-cell data
       lagrange_struct(rhoWold,(/r1_ptr(x),r1_ptr(y),r1_ptr(zm)/),(/0,0,-1/),'W') /)
 
  call parser_read('Rescale flux volume',rescale_vol,.true.)
  call parser_read('Tag distance',tag_distance,-huge(1.0_WP))
  call parser_read('Skip IB lagrange',skip_lagrange,.false.)
  ijk_bounds = reshape((/imino_,imaxo_,jmino_,jmaxo_,kmino_,kmaxo_/),(/2,3/))

end subroutine ib_lagrange_init

subroutine ib_lagrange_tag(do_init)
  use ib_lagrange
  implicit none
  integer :: i,j,k,stl1,stl2
  integer :: i1,i2,j1,j2,k1,k2
  logical, intent(in) :: do_init

  if(do_init) then
     ! initialize lagrangian fluxes
     ilgrgn_ux=0;ilgrgn_uy=0;ilgrgn_uz=0
     ilgrgn_vx=0;ilgrgn_vy=0;ilgrgn_vz=0
     ilgrgn_wx=0;ilgrgn_wy=0;ilgrgn_wz=0
  end if
  if(skip_lagrange) return

  if(tag_distance.gt.0.0_WP) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ! interpolate G to U-cell centroid
              if(sum(interp_sc_x(i,j,:)*Gib(i-st2:i+st1,j,k)).lt.tag_distance) then
                 ilgrgn_ux(i-1:i,j    ,k    ) = 1
                 ilgrgn_uy(i    ,j:j+1,k    ) = 1
                 ilgrgn_uz(i    ,j    ,k:k+1) = 1
              end if
              ! interpolate G to V-cell centroid
              if(sum(interp_sc_y(i,j,:)*Gib(i,j-st2:j+st1,k)).lt.tag_distance) then
                 ilgrgn_vx(i:i+1,j    ,k    ) = 1
                 ilgrgn_vy(i    ,j-1:j,k    ) = 1
                 ilgrgn_vz(i    ,j    ,k:k+1) = 1
              end if
              ! interpolate G to W-cell centroid
              if(sum(interp_sc_z(i,j,:)*Gib(i,j,k-st2:k+st1)).lt.tag_distance) then
                 ilgrgn_wx(i:i+1,j    ,k    ) = 1
                 ilgrgn_wy(i    ,j:j+1,k    ) = 1
                 ilgrgn_wz(i    ,j    ,k-1:k) = 1
              end if
           end do
        end do
     end do
  else
     stl2=2;stl1=1
     ! include first layer of ghost cells
     do k=kmin_-1,kmax_+1
        do j=jmin_-1,jmax_+1
           do i=imin_-1,imax_+1
              if(SAu(i,j,k).gt.epsilon(1.0_WP)) then
                 ! set indices
                 i1=max(i-stl2,imino_); j1=max(j-stl1,jmino_); k1=max(k-stl1,kmino_)
                 i2=min(i+stl1,imaxo_); j2=min(j+stl1,jmaxo_); k2=min(k+stl1,kmaxo_)
                 ilgrgn_ux(i1:i2,j1:j2,k1:k2) = 1
                 ! reset indices
                 i1=max(i-stl1,imino_); j2=min(j+stl2,jmaxo_)
                 ilgrgn_uy(i1:i2,j1:j2,k1:k2) = 1
                 ! reset indices
                 j2=min(j+stl1,jmaxo_); k2=min(k+stl2,kmaxo_)
                 ilgrgn_uz(i1:i2,j1:j2,k1:k2) = 1
              end if
              if(SAv(i,j,k).gt.epsilon(1.0_WP)) then
                 ! set indices
                 i1=max(i-stl1,imino_); j1=max(j-stl1,jmino_); k1=max(k-stl1,kmino_)
                 i2=min(i+stl2,imaxo_); j2=min(j+stl1,jmaxo_); k2=min(k+stl1,kmaxo_)
                 ilgrgn_vx(i1:i2,j1:j2,k1:k2) = 1
                 ! reset indices
                 j1=max(j-stl2,jmino_); i2=min(i+stl1,imaxo_)
                 ilgrgn_vy(i1:i2,j1:j2,k1:k2) = 1
                 ! reset indices
                 j1=max(j-stl1,jmino_); k2=min(k+stl2,kmaxo_)
                 ilgrgn_vz(i1:i2,j1:j2,k1:k2) = 1
              end if
              if(SAw(i,j,k).gt.epsilon(1.0_WP)) then
                 ! set indices
                 i1=max(i-stl1,imino_); j1=max(j-stl1,jmino_); k1=max(k-stl1,kmino_)
                 i2=min(i+stl2,imaxo_); j2=min(j+stl1,jmaxo_); k2=min(k+stl1,kmaxo_)
                 ilgrgn_wx(i1:i2,j1:j2,k1:k2) = 1
                 ! reset indices
                 i2=min(i+stl1,imaxo_); j2=min(j+stl2,jmaxo_)
                 ilgrgn_wy(i1:i2,j1:j2,k1:k2) = 1
                 ! reset indices
                 k1=max(k-stl2,kmino_); j2=min(j+stl1,jmaxo_)
                 ilgrgn_wz(i1:i2,j1:j2,k1:k2) = 1
              end if
           end do
        end do
     end do
  end if

  if(.not.do_init) then
     ! remove tags from faces which are all IB at n+1
     where(AFux.le.epsilon(1.0_WP)) ilgrgn_ux = 0
     where(AFuy.le.epsilon(1.0_WP)) ilgrgn_uy = 0
     where(AFuz.le.epsilon(1.0_WP)) ilgrgn_uz = 0
     where(AFvx.le.epsilon(1.0_WP)) ilgrgn_vx = 0
     where(AFvy.le.epsilon(1.0_WP)) ilgrgn_vy = 0
     where(AFvz.le.epsilon(1.0_WP)) ilgrgn_vz = 0
     where(AFwx.le.epsilon(1.0_WP)) ilgrgn_wx = 0
     where(AFwy.le.epsilon(1.0_WP)) ilgrgn_wy = 0
     where(AFwz.le.epsilon(1.0_WP)) ilgrgn_wz = 0
  end if

end subroutine ib_lagrange_tag

subroutine ib_lagrange_fluxes
  use ib_lagrange
  implicit none

  integer :: i,j,k,ii,jj,kk,nn,st
  ! face coordinates,Gib vals
  real(WP), dimension(3,4) :: fpos
  real(WP), dimension(  4) :: fval
  ! cell coordinates,Gib vals
  real(WP), dimension(3,8) :: cpos
  real(WP), dimension(  8) :: cval
  real(WP) :: ib_flux,vol_flux

  ! compute fluxes based on tagged faces (tagging done in ib_update)
  lgn_srcu=0.0_WP;lgn_srcv=0.0_WP;lgn_srcw=0.0_WP
  lgn_volu(imino_+1:imaxo_,:,:)=volu
  lgn_volv(:,jmino_+1:jmaxo_,:)=volv
  lgn_volw(:,:,kmino_+1:kmaxo_)=volw

  if(skip_lagrange) return

  do kk=kmin_-stc1,kmax_+stc2
     do jj=jmin_-stc1,jmax_+stc2
        do ii=imin_-stc1,imax_+stc2

           ! lagragian flux calculation routines need the i,j,k
           ! of the lgrngf_ vector and face verts/G-vals (order is important)
           i=ii-1;j=jj-1;k=kk-1

           if(ilgrgn_ux(i,j,k).eq.1) then
              ! don't waste time computing ghost cell fluxes
              if(j.ge.jmin_.and.j.le.jmax_.and.k.ge.kmin_.and.k.le.kmax_) then
                 call set_u_face(fpos,fval,i,j,k, 0,0,0, 0,1,1)
                    call ib_lagrange_vel(fpos,fval,i,j,k,1,lgrgn_ptrs(1),&
                         ib_flux,vol_flux,ib_surf=.false.)
                 do st=0,1
                    lgn_srcu(i+st,j,k) = lgn_srcu(i+st,j,k) + real(2*st-1,WP) * ib_flux
                    lgn_volu(i+st,j,k) = lgn_volu(i+st,j,k) + real(2*st-1,WP) * vol_flux
                 end do
              end if
           end if
           if(ilgrgn_vy(i,j,k).eq.1) then
              if(i.ge.imin_.and.i.le.imax_.and.k.ge.kmin_.and.k.le.kmax_) then
                 call set_v_face(fpos,fval,i,j,k, 0,0,0, 1,0,1)
                 call ib_lagrange_vel(fpos,fval,i,j,k,2,lgrgn_ptrs(2),&
                      ib_flux,vol_flux,ib_surf=.false.)
                 do st=0,1
                    lgn_srcv(i,j+st,k) = lgn_srcv(i,j+st,k) + real(2*st-1,WP) * ib_flux
                    lgn_volv(i,j+st,k) = lgn_volv(i,j+st,k) + real(2*st-1,WP) * vol_flux
                 end do
              end if
           end if
           if(ilgrgn_wz(i,j,k).eq.1) then
              if(i.ge.imin_.and.i.le.imax_.and.j.ge.jmin_.and.j.le.jmax_) then
                 call set_w_face(fpos,fval,i,j,k, 0,0,0, 1,1,0)
                 call ib_lagrange_vel(fpos,fval,i,j,k,1,lgrgn_ptrs(3),&
                      ib_flux,vol_flux,ib_surf=.false.)
                 do st=0,1
                    lgn_srcw(i,j,k+st) = lgn_srcw(i,j,k+st) + real(2*st-1,WP) * ib_flux
                    lgn_volw(i,j,k+st) = lgn_volw(i,j,k+st) + real(2*st-1,WP) * vol_flux
                 end do
              end if
           end if

           i=ii;j=jj;k=kk
           if(ilgrgn_uy(i,j,k).eq.1) then
              if(i.ge.imin_.and.i.le.imax_.and.k.ge.kmin_.and.k.le.kmax_) then
                 call set_u_face(fpos,fval,i,j,k, -1,0,0, 0,0,1)
                 call ib_lagrange_vel(fpos,fval,i,j,k,2,lgrgn_ptrs(1),&
                      ib_flux,vol_flux,ib_surf=.false.)
                 do st=-1,0
                    lgn_srcu(i,j+st,k) = lgn_srcu(i,j+st,k) + real(2*st+1,WP) * ib_flux
                    lgn_volu(i,j+st,k) = lgn_volu(i,j+st,k) + real(2*st+1,WP) * vol_flux
                 end do
              end if
           end if
           if(ilgrgn_uz(i,j,k).eq.1) then
              if(i.ge.imin_.and.i.le.imax_.and.j.ge.jmin_.and.j.le.jmax_) then
                 call set_u_face(fpos,fval,i,j,k, -1,0,0, 0,1,0)
                 call ib_lagrange_vel(fpos,fval,i,j,k,1,lgrgn_ptrs(1),&
                      ib_flux,vol_flux,ib_surf=.false.)
                 do st=-1,0
                    lgn_srcu(i,j,k+st) = lgn_srcu(i,j,k+st) + real(2*st+1,WP) * ib_flux
                    lgn_volu(i,j,k+st) = lgn_volu(i,j,k+st) + real(2*st+1,WP) * vol_flux
                 end do
              end if
           end if

           if(ilgrgn_vx(i,j,k).eq.1) then
              if(j.ge.jmin_.and.j.le.jmax_.and.k.ge.kmin_.and.k.le.kmax_) then
                 call set_v_face(fpos,fval,i,j,k, 0,-1,0, 0,0,1) 
                 call ib_lagrange_vel(fpos,fval,i,j,k,1,lgrgn_ptrs(2),&
                      ib_flux,vol_flux,ib_surf=.false.)
                 do st=-1,0
                    lgn_srcv(i+st,j,k) = lgn_srcv(i+st,j,k) + real(2*st+1,WP) * ib_flux
                    lgn_volv(i+st,j,k) = lgn_volv(i+st,j,k) + real(2*st+1,WP) * vol_flux
                 end do
              end if
           end if
           if(ilgrgn_vz(i,j,k).eq.1) then
              if(j.ge.jmin_.and.j.le.jmax_.and.i.ge.imin_.and.i.le.imax_) then
                 call set_v_face(fpos,fval,i,j,k, 0,-1,0, 1,0,0)
                 call ib_lagrange_vel(fpos,fval,i,j,k,1,lgrgn_ptrs(2),&
                      ib_flux,vol_flux,ib_surf=.false.)
                 do st=-1,0
                    lgn_srcv(i,j,k+st) = lgn_srcv(i,j,k+st) + real(2*st+1,WP) * ib_flux
                    lgn_volv(i,j,k+st) = lgn_volv(i,j,k+st) + real(2*st+1,WP) * vol_flux
                 end do
              end if
           end if
           if(ilgrgn_wx(i,j,k).eq.1) then
              if(j.ge.jmin_.and.j.le.jmax_.and.k.ge.kmin_.and.k.le.kmax_) then
                 call set_w_face(fpos,fval,i,j,k, 0,0,-1, 0,1,0)
                 call ib_lagrange_vel(fpos,fval,i,j,k,1,lgrgn_ptrs(3),&
                      ib_flux,vol_flux,ib_surf=.false.)
                 do st=-1,0
                    lgn_srcw(i+st,j,k) = lgn_srcw(i+st,j,k) + real(2*st+1,WP) * ib_flux
                    lgn_volw(i+st,j,k) = lgn_volw(i+st,j,k) + real(2*st+1,WP) * vol_flux
                 end do
              end if
           end if
           if(ilgrgn_wy(i,j,k).eq.1) then
              if(i.ge.imin_.and.i.le.imax_.and.k.ge.kmin_.and.k.le.kmax_) then
                 call set_w_face(fpos,fval,i,j,k, 0,0,-1, 1,0,0)
                 call ib_lagrange_vel(fpos,fval,i,j,k,2,lgrgn_ptrs(3),&
                      ib_flux,vol_flux,ib_surf=.false.)
                 do st=-1,0
                    lgn_srcw(i,j+st,k) = lgn_srcw(i,j+st,k) + real(2*st+1,WP) * ib_flux
                    lgn_volw(i,j+st,k) = lgn_volw(i,j+st,k) + real(2*st+1,WP) * vol_flux
                 end do
              end if
           end if
        end do
     end do
  end do

  ! update lagragian cell volumes for faces with eulerian transport
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_

           do st=-stc2,stc1
              if(ilgrgn_ux(i+st,j,k).eq.0) then
                 lgn_volu(i,j,k) = lgn_volu(i,j,k)-real(2*st+1,WP)*dt_uvw* &
                      sum(interp_Ju_xm(i+st,j,:)*U(i+st-stc1:i+st+stc2,j,k))*&
                      dy(j)*dz*AFux(i+st,j,k)
              end if
              if(ilgrgn_vy(i,j+st,k).eq.0) then
                 lgn_volv(i,j,k) = lgn_volv(i,j,k)-real(2*st+1,WP)*dt_uvw* &
                      sum(interp_Jv_ym(i,j+st,:)*V(i,j+st-stc1:j+st+stc2,k))*&
                      dx(i)*dz*AFvy(i,j+st,k)
              end if
              if(ilgrgn_wz(i,j,k+st).eq.0) then
                 lgn_volw(i,j,k) = lgn_volw(i,j,k)-real(2*st+1,WP)*dt_uvw* &
                      sum(interp_Jw_zm(i,j,:)*W(i,j,k+st-stc1:k+st+stc2))* &
                      dx(i)*dy(j)*AFwz(i,j,k+st)
              end if
           end do
           do st=-stc1,stc2
              if(ilgrgn_uy(i,j+st,k).eq.0) then
                 lgn_volu(i,j,k) = lgn_volu(i,j,k)-real(2*st-1,WP)*dt_uvw* &
                      sum(interp_Jv_x(i,j+st,:)*V(i-stc2:i+stc1,j+st,k))* &
                      dxm(i-1)*dz*AFuy(i,j+st,k)
              end if
              if(ilgrgn_uz(i,j,k+st).eq.0) then
                 lgn_volu(i,j,k) = lgn_volu(i,j,k)-real(2*st-1,WP)*dt_uvw* &
                      sum(interp_Jw_x(i,j,:)*W(i-stc2:i+stc1,j,k+st))* &
                      dxm(i-1)*dy(j)*AFuz(i,j,k+st)
              end if
              if(ilgrgn_vx(i+st,j,k).eq.0) then
                 lgn_volv(i,j,k) = lgn_volv(i,j,k)-real(2*st-1,WP)*dt_uvw* &
                      sum(interp_Ju_y(i+st,j,:)*U(i+st,j-stc2:j+stc1,k))* &
                      dym(j-1)*dz*AFvx(i+st,j,k)
              end if
              if(ilgrgn_vz(i,j,k+st).eq.0) then
                 lgn_volv(i,j,k) = lgn_volv(i,j,k)-real(2*st-1,WP)*dt_uvw* &
                      sum(interp_Jw_y(i,j,:)*W(i,j-stc2:j+stc1,k+st))* &
                      dx(i)*dym(j)*AFvz(i,j,k+st)
              end if
              if(ilgrgn_wx(i+st,j,k).eq.0) then
                 lgn_volw(i,j,k) = lgn_volw(i,j,k)-real(2*st-1,WP)*dt_uvw* &
                      sum(interp_Ju_z(i+st,j,:)*U(i+st,j,k-stc2:k+stc1))* &
                      dy(j)*dz*AFwx(i+st,j,k)
              end if
              if(ilgrgn_wy(i,j+st,k).eq.0) then
                 lgn_volw(i,j,k) = lgn_volw(i,j,k)-real(2*st-1,WP)*dt_uvw* &
                      sum(interp_Jv_z(i,j+st,:)*V(i,j+st,k-stc2:k+stc1))* &
                      dx(i)*dz*AFwy(i,j+st,k)
              end if
           end do

        end do
     end do
  end do

  ! update srcUVW with [bounded] transport scheme
  if(.not.rescale_vol) then
     ! set fluxed volumes to zero to ensure vol[uvw] are used
     lgn_volu=0.0_WP;lgn_volv=0.0_WP;lgn_volw=0.0_WP
  end if
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           
           if(abs(lgn_srcu(i,j,k)).gt.epsilon(1.0_WP)) then
              srcUmid(i,j,k) = srcUmid(i,j,k) + &
                   lgn_srcu(i,j,k)/max(volu(i,j,k)+epsilon(1.0_WP),lgn_volu(i,j,k))
           end if
           if(abs(lgn_srcv(i,j,k)).gt.epsilon(1.0_WP)) then
              srcVmid(i,j,k) = srcVmid(i,j,k) + &
                   lgn_srcv(i,j,k)/max(volv(i,j,k)+epsilon(1.0_WP),lgn_volv(i,j,k))
           end if
           if(abs(lgn_srcw(i,j,k)).gt.epsilon(1.0_WP)) then
              srcWmid(i,j,k) = srcWmid(i,j,k) + &
                   lgn_srcw(i,j,k)/max(volw(i,j,k)+epsilon(1.0_WP),lgn_volw(i,j,k))
           end if
        end do
     end do
  end do

  ! -- compute convective fluxes from IB face
  if(ibmove) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_

              if(SAu(i,j,k).gt.0.0_WP) then
                 call set_u_face(cpos,cval,i,j,k,-1,0,0, 0,1,1)
                 call ib_lagrange_vel(cpos,cval,i,j,k,1,lgrgn_ptrs(1),ib_flux,vol_flux,ib_surf=.true.)   
                 srcUmid(i,j,k) = srcUmid(i,j,k) + ib_flux*dt_uvw/(volu(i,j,k)+epsilon(1.0_WP))
              end if
              if(SAv(i,j,k).gt.0.0_WP) then
                 call set_v_face(cpos,cval,i,j,k,0,-1,0, 1,0,1)
                 call ib_lagrange_vel(cpos,cval,i,j,k,1,lgrgn_ptrs(2),ib_flux,vol_flux,ib_surf=.true.)
                 srcVmid(i,j,k) = srcVmid(i,j,k) + ib_flux*dt_uvw/(volv(i,j,k)+epsilon(1.0_WP))
              end if
              if(SAw(i,j,k).gt.0.0_WP) then
                 call set_w_face(cpos,cval,i,j,k,0,0,-1, 1,1,0)
                 call ib_lagrange_vel(cpos,cval,i,j,k,1,lgrgn_ptrs(3),ib_flux,vol_flux,ib_surf=.true.)
                 srcWmid(i,j,k) = srcWmid(i,j,k) + ib_flux*dt_uvw/(volw(i,j,k)+epsilon(1.0_WP))
              end if
           end do
        end do
     end do
  end if

  return

end subroutine ib_lagrange_fluxes


