module ctridiag
  use metric_velocity_conv
  use pressure
  use parallel
  use cfourier
  implicit none
  
  ! Prepare storage for complex Poisson equation
  complex(WP), dimension(:,:,:,:,:), allocatable :: clap
  complex(WP), dimension(:,:,:), allocatable :: cRP,cDP
  
  ! For complex communication
  real(WP), dimension(:,:,:), allocatable :: rwork,iwork
  
end module ctridiag


subroutine ctridiag_init
  use ctridiag
  implicit none
  
  integer :: i,j,k
  
  ! Initialize fft solver
  call cfourier_init
  
  ! Check proper usage
  if (.not.fft_x .or. fft_y .or. .not.fft_z) call die('ctridiag requires fft in x and z, and no fft in y')
  
  ! Allocate arrays
  allocate( cRP(imin_ :imax_ ,jmin_ :jmax_ ,kmin_ :kmax_ )); cRP=0.0_WP
  allocate( cDP(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); cDP=0.0_WP
  allocate(clap(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3,-stcp:stcp))
  allocate(rwork(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(iwork(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  
  ! Initialize complex Laplacian operator
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           clap(i,j,k,:,:) = dcmplx(lap(i,j,k,:,:))
        end do
     end do
  end do
  
  return
end subroutine ctridiag_init



subroutine ctridiag_RHS
  use ctridiag
  implicit none
  
  integer :: i,j,k
  
  ! Rescale the RHS
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           RP(i,j,k) = -vol(i,j,k)*RP(i,j,k)
        end do
     end do
  end do
  
  ! Copy RHS to complex storage
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           cRP(i,j,k) = dcmplx(RP(i,j,k))
        end do
     end do
  end do
  
  ! Fourier transform
  call cfourier_transform(cRP)
  
  return
end subroutine ctridiag_RHS


subroutine ctridiag_solve
  use ctridiag
  use memory
  implicit none
  
  integer :: i,j,k,st
  real(WP) :: mymax_resP
  complex(WP) :: myres
  
  ! Inverse system in the Y-direction
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (vol(i,j,k).gt.0.0_WP) then
              do st=-stcp,stcp
                 cAy(i,k,j,st) = clap(i,j,k,2,st)
              end do
              cAy(i,k,j,0) = cAy(i,k,j,0) + clap(i,j,k,1,0) + clap(i,j,k,3,0)
              cRy(i,k,j) = cRP(i,j,k)
           else
              cAy(i,k,j,:) = 0.0_WP
              cAy(i,k,j,0) = 1.0_WP
              cRy(i,k,j) = 0.0_WP
           end if
        end do
     end do
  end do
  call clinear_solver_y(2*stcp+1)
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           cDP(i,j,k) = cRy(i,k,j)
        end do
     end do
  end do
  
  ! Complex communication
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           rwork(i,j,k)=dreal(cDP(i,j,k))
           iwork(i,j,k)=dimag(cDP(i,j,k))
        end do
     end do
  end do
  call boundary_update_border(rwork,'+','ym')
  call boundary_update_border(iwork,'+','ym')
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           cDP(i,j,k)=dcmplx(rwork(i,j,k),iwork(i,j,k))
        end do
     end do
  end do
  
  ! Generate residual
  mymax_resP=0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           myres=sum(cDP(i-stcp:i+stcp,j,k)*clap(i,j,k,1,:))+&
                 sum(cDP(i,j-stcp:j+stcp,k)*clap(i,j,k,2,:))+&
                 sum(cDP(i,j,k-stcp:k+stcp)*clap(i,j,k,3,:))-cRP(i,j,k) 
           mymax_resP=max(mymax_resP,abs(myres))
        end do
     end do
  end do
  call parallel_max(mymax_resP,max_resP)
  it_P=1
  
  return
end subroutine ctridiag_solve


subroutine ctridiag_get_sol
  use ctridiag
  implicit none
  
  integer :: i,j,k
  
  ! Copy cDP in RP for size restriction
  cRP=cDP(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
  
  ! Transform back
  call cfourier_inverse(cRP)
  
  ! Put back in cDP
  cDP(imin_:imax_,jmin_:jmax_,kmin_:kmax_)=cRP
  
  ! Get real DP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           DP(i,j,k) = real(cDP(i,j,k),WP)
        end do
     end do
  end do
  
  return
end subroutine ctridiag_get_sol
