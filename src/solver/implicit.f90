module implicit
  use precision
  use string
  use velocity
  use ib
  use bodyforce
  use optdata
  implicit none
  
  ! Use of implicit solver
  logical :: implicit_x
  logical :: implicit_y
  logical :: implicit_z
  logical :: implicit_any
  logical :: implicit_auto
  
  ! Specific use of implicit solver
  logical :: imp_conv_x,imp_conv_y,imp_conv_z
  logical :: imp_visc_x,imp_visc_y,imp_visc_z
  integer :: ndx,ndy,ndz
  
  ! Work variables
  real(WP), dimension(:), allocatable :: Rcyl
  real(WP), dimension(:), allocatable :: Rcyl2
  
  real(WP) :: inverse_burning_timescale
  
  ! Values to monitor
  real(WP) :: CFLc_x,CFLc_y,CFLc_z,CFLc_axis
  real(WP) :: CFLv_x,CFLv_y,CFLv_z,CFLv_axis
  real(WP) :: CFLd_x,CFLd_y,CFLd_z,CFLd_axis
  real(WP) :: CFL_lvl
  
contains
  
  ! Check which terms should be implicit
  subroutine implicit_check
    use parallel
    implicit none
    
    if (.not.implicit_auto) return
    
    ! Check if implicit is possible -> at best:
    ! - pentadiagonal in parallel
    ! - polydiagonal in serial
    imp_conv_x = .false.;imp_visc_x = .false.
    if (implicit_x) then
       if ((vel_conv_order.eq.2 .or. npx.eq.1) .and. (CFLc_x.ge.0.5_WP)) imp_conv_x = .true.
       if ((vel_visc_order.eq.2 .or. npx.eq.1) .and. (CFLv_x.ge.0.5_WP)) imp_visc_x = .true.
    end if
    imp_conv_y = .false.;imp_visc_y = .false.
    if (implicit_y) then
       if ((vel_conv_order.eq.2 .or. npy.eq.1) .and. (CFLc_y.ge.0.5_WP)) imp_conv_y = .true.
       if ((vel_visc_order.eq.2 .or. npy.eq.1) .and. (CFLv_y.ge.0.5_WP)) imp_visc_y = .true.
    end if
    imp_conv_z = .false.;imp_visc_z = .false.
    if (implicit_z) then
       if ((vel_conv_order.eq.2 .or. npz.eq.1) .and. (CFLc_z.ge.0.5_WP)) imp_conv_z = .true.
       if ((vel_visc_order.eq.2 .or. npz.eq.1) .and. (CFLv_z.ge.0.5_WP)) imp_visc_z = .true.
    end if

!!$    if(use_ib) then
!!$       imp_conv_x=.false.;imp_conv_y=.false.;imp_conv_z=.false.
!!$    end if
    
    ! Obtain number of diagonals - scheme dependent
    if (imp_conv_x) ndx = 2*vel_conv_order-1
    if (imp_visc_x) ndx = max(ndx,2*vel_visc_order-1)
    if (imp_conv_y) ndy = 2*vel_conv_order-1
    if (imp_visc_y) ndy = max(ndy,2*vel_visc_order-1)
    if (imp_conv_z) ndz = 2*vel_conv_order-1
    if (imp_visc_z) ndz = max(ndz,2*vel_visc_order-1)
    
    return
  end subroutine implicit_check
  
end module implicit


! ============================== !
! Initialize the implicit module !
! ============================== !
subroutine implicit_init
  use implicit
  use parser
  use parallel
  use data
  implicit none

  character(len=str_medium) :: implicit_dir

  implicit_x = .false.
  implicit_y = .false.
  implicit_z = .false.

  ! Read in the implicit directions
  call parser_read('Implicit directions',implicit_dir)
  if (index(implicit_dir,'x').ne.0) implicit_x = .true.
  if (index(implicit_dir,'y').ne.0) implicit_y = .true.
  if (index(implicit_dir,'z').ne.0) implicit_z = .true.
  if (trim(implicit_dir).eq.'auto') then
     implicit_x = .true.
     implicit_y = .true.
     implicit_z = .true.
     implicit_auto = .true.
  end if

  ! Test if everything has been correctly detected
  implicit_any = implicit_x .or. implicit_y .or. implicit_z
  if (.not.(implicit_any .or. trim(implicit_dir).eq.'none')) call die('implicit_init: unknown directions')

  ! Account for 2D and 1D problems
  if (nx.eq.1) implicit_x = .false.
  if (ny.eq.1) implicit_y = .false.
  if (nz.eq.1) implicit_z = .false.
  implicit_any = implicit_x .or. implicit_y .or. implicit_z

  ! Check if implicit is possible -> at best:
  ! - pentadiagonal in parallel
  ! - polydiagonal in serial
  imp_conv_x = .false.;imp_visc_x = .false.
  if (implicit_x) then
     if (vel_conv_order.eq.2 .or. npx.eq.1) imp_conv_x = .true.
     if (vel_visc_order.eq.2 .or. npx.eq.1) imp_visc_x = .true.
  end if
  imp_conv_y = .false.;imp_visc_y = .false.
  if (implicit_y) then
     if (vel_conv_order.eq.2 .or. npy.eq.1) imp_conv_y = .true.
     if (vel_visc_order.eq.2 .or. npy.eq.1) imp_visc_y = .true.
  end if
  imp_conv_z = .false.;imp_visc_z = .false.
  if (implicit_z) then
     if (vel_conv_order.eq.2 .or. npz.eq.1) imp_conv_z = .true.
     if (vel_visc_order.eq.2 .or. npz.eq.1) imp_visc_z = .true.
  end if

!!$  if(use_ib) then
!!$     imp_conv_x=.false.;imp_conv_y=.false.;imp_conv_z=.false.
!!$  end if

  ! Obtain number of diagonals - scheme dependent
  if (imp_conv_x) ndx = 2*vel_conv_order-1
  if (imp_visc_x) ndx = max(ndx,2*vel_visc_order-1)
  if (imp_conv_y) ndy = 2*vel_conv_order-1
  if (imp_visc_y) ndy = max(ndy,2*vel_visc_order-1)
  if (imp_conv_z) ndz = 2*vel_conv_order-1
  if (imp_visc_z) ndz = max(ndz,2*vel_visc_order-1)

  ! Implicit boundary conditions in cylindrical coordinates
  if (icyl.eq.1) allocate(Rcyl (imin_:imax_))
  if (icyl.eq.1) allocate(Rcyl2(imin_:imax_))

  ! Create new file to monitor at each iterations
  if (nscalar.eq.0) then
     call monitor_create_file_step('timestep',10)
  else
     call monitor_create_file_step('timestep',14)
  end if

  call monitor_set_header(1, 'dt','r')
  call monitor_set_header(2, 'CFL_max','r')
  call monitor_set_header(3, 'CFLc_x','r')
  call monitor_set_header(4, 'CFlc_y','r')
  call monitor_set_header(5, 'CFLc_z','r')
  call monitor_set_header(6, 'CFLc_axis','r')
  call monitor_set_header(7, 'CFLv_x','r')
  call monitor_set_header(8, 'CFLv_y','r')
  call monitor_set_header(9, 'CFLv_z','r')
  call monitor_set_header(10,'CFLv_axis','r')
  if (nscalar.ne.0) then
     call monitor_set_header(11, 'CFLd_x','r')
     call monitor_set_header(12, 'CFLd_y','r')
     call monitor_set_header(13, 'CFLd_z','r')
     call monitor_set_header(14, 'CFLd_axis','r')
  end if

  return
end subroutine implicit_init


! ================================================ !
! Compute the maximum CFL from velocity components !
! ================================================ !
subroutine velocity_CFL
  use implicit
  use data
  use parallel
  implicit none

  integer  :: i,j,k
  real(WP) :: max_CFLc_x,max_CFLc_y,max_CFLc_z,max_CFLc_axis
  real(WP) :: max_CFLv_x,max_CFLv_y,max_CFLv_z,max_CFLv_axis
  real(WP) :: max_CFLd_x,max_CFLd_y,max_CFLd_z,max_CFLd_axis

  ! Set the CFLs to zero
  max_CFLc_x = 0.0_WP
  max_CFLc_y = 0.0_WP
  max_CFLc_z = 0.0_WP
  max_CFLc_axis = 0.0_WP
  max_CFLv_x = 0.0_WP
  max_CFLv_y = 0.0_WP
  max_CFLv_z = 0.0_WP
  max_CFLv_axis = 0.0_WP
  max_CFLd_x = 0.0_WP
  max_CFLd_y = 0.0_WP
  max_CFLd_z = 0.0_WP
  max_CFLd_axis = 0.0_WP

  ! Calculate the CFLs
  if (icyl.EQ.1) then
     ! Cylindrical
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              CFLc_x = abs( U(i,j,k) ) * dxmi(i-1)
              CFLc_y = abs( V(i,j,k) ) * dymi(j-1)
              CFLc_z = abs( W(i,j,k) ) * dzi*ymi(j)
              CFLv_x = abs( 4.0_WP*VISC(i,j,k) ) * dxi(i)**2       / RHOmid(i,j,k)
              CFLv_y = abs( 4.0_WP*VISC(i,j,k) ) * dyi(j)**2       / RHOmid(i,j,k)
              CFLv_z = abs( 4.0_WP*VISC(i,j,k) ) * (dzi*ymi(j))**2 / RHOmid(i,j,k)
              max_CFLc_x = max(max_CFLc_x,CFLc_x)
              max_CFLc_y = max(max_CFLc_y,CFLc_y)
              max_CFLc_z = max(max_CFLc_z,CFLc_z)
              max_CFLv_x = max(max_CFLv_x,CFLv_x)
              max_CFLv_y = max(max_CFLv_y,CFLv_y)
              max_CFLv_z = max(max_CFLv_z,CFLv_z)
           end do
        end do
     end do
     ! Centerline CFL
     if (jproc.eq.1) then
        do k=kmin_,kmax_
           j = jmin_
           do i=imin_,imax_
              CFLc_axis = abs( V(i,j,k) ) * dymi(j-1)
              CFLv_axis = abs( 4.0_WP*VISC(i,j,k) ) * dyi(j)**2 / RHOmid(i,j,k)
              max_CFLc_axis = max(max_CFLc_axis,CFLc_axis)
              max_CFLv_axis = max(max_CFLv_axis,CFLv_axis)
           end do
        end do
     end if
     ! For scalars
     if (nscalar.ne.0) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 CFLd_x = abs( 4.0_WP*maxval(DIFF(i,j,k,:)) ) * dxi(i)**2       / RHOmid(i,j,k)
                 CFLd_y = abs( 4.0_WP*maxval(DIFF(i,j,k,:)) ) * dyi(j)**2       / RHOmid(i,j,k)
                 CFLd_z = abs( 4.0_WP*maxval(DIFF(i,j,k,:)) ) * (dzi*ymi(j))**2 / RHOmid(i,j,k)
                 max_CFLd_x = max(max_CFLd_x,CFLd_x)
                 max_CFLd_y = max(max_CFLd_y,CFLd_y)
                 max_CFLd_z = max(max_CFLd_z,CFLd_z)
              end do
           end do
        end do
        if (jproc.eq.1) then
           do k=kmin_,kmax_
              j = jmin_
              do i=imin_,imax_
                 CFLd_axis = abs( 4.0_WP*maxval(DIFF(i,j,k,:)) ) * dyi(j)**2 / RHOmid(i,j,k)
                 max_CFLd_axis = max(max_CFLd_axis,CFLd_axis)
              end do
           end do
        end if
     end if
  else
     if (use_ib) then
        call ib_set_velocity
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 CFLc_x = abs( U_ib(i,j,k) ) * dxmi(i-1)
                 CFLc_y = abs( V_ib(i,j,k) ) * dymi(j-1)
                 CFLc_z = abs( W_ib(i,j,k) ) * dzi
                 max_CFLc_x = max(max_CFLc_x,CFLc_x)
                 max_CFLc_y = max(max_CFLc_y,CFLc_y)
                 max_CFLc_z = max(max_CFLc_z,CFLc_z)
              end do
           end do
        end do
     end if
     ! Cartesian
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              CFLc_x = abs( U(i,j,k) ) * dxmi(i-1)
              CFLc_y = abs( V(i,j,k) ) * dymi(j-1)
              CFLc_z = abs( W(i,j,k) ) * dzi
              CFLv_x = abs( 4.0_WP*VISC(i,j,k) ) * dxi(i)**2 / RHOmid(i,j,k)
              CFLv_y = abs( 4.0_WP*VISC(i,j,k) ) * dyi(j)**2 / RHOmid(i,j,k)
              CFLv_z = abs( 4.0_WP*VISC(i,j,k) ) * dzi**2    / RHOmid(i,j,k)
              max_CFLc_x = max(max_CFLc_x,CFLc_x)
              max_CFLc_y = max(max_CFLc_y,CFLc_y)
              max_CFLc_z = max(max_CFLc_z,CFLc_z)
              max_CFLv_x = max(max_CFLv_x,CFLv_x)
              max_CFLv_y = max(max_CFLv_y,CFLv_y)
              max_CFLv_z = max(max_CFLv_z,CFLv_z)
           end do
        end do
     end do
     ! For scalars
     if (nscalar.ne.0) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 CFLd_x = abs( 4.0_WP*maxval(DIFF(i,j,k,:)) ) * dxi(i)**2 / RHOmid(i,j,k)
                 CFLd_y = abs( 4.0_WP*maxval(DIFF(i,j,k,:)) ) * dyi(j)**2 / RHOmid(i,j,k)
                 CFLd_z = abs( 4.0_WP*maxval(DIFF(i,j,k,:)) ) * dzi**2    / RHOmid(i,j,k)
                 max_CFLd_x = max(max_CFLd_x,CFLd_x)
                 max_CFLd_y = max(max_CFLd_y,CFLd_y)
                 max_CFLd_z = max(max_CFLd_z,CFLd_z)
              end do
           end do
        end do
     end if
  end if

  ! Get the global maxs
  max_CFLc_x = max_CFLc_x * dt_uvw
  max_CFLc_y = max_CFLc_y * dt_uvw
  max_CFLc_z = max_CFLc_z * dt_uvw
  max_CFLc_axis = max_CFLc_axis * dt_uvw
  max_CFLv_x = max_CFLv_x * dt_uvw
  max_CFLv_y = max_CFLv_y * dt_uvw
  max_CFLv_z = max_CFLv_z * dt_uvw
  max_CFLv_axis = max_CFLv_axis * dt_uvw
  max_CFLd_x = max_CFLd_x * dt_uvw
  max_CFLd_y = max_CFLd_y * dt_uvw
  max_CFLd_z = max_CFLd_z * dt_uvw
  max_CFLd_axis = max_CFLd_axis * dt_uvw
  call parallel_max(max_CFLc_x,CFLc_x)
  call parallel_max(max_CFLc_y,CFLc_y)
  call parallel_max(max_CFLc_z,CFLc_z)
  call parallel_max(max_CFLc_axis,CFLc_axis)
  call parallel_max(max_CFLv_x,CFLv_x)
  call parallel_max(max_CFLv_y,CFLv_y)
  call parallel_max(max_CFLv_z,CFLv_z)
  call parallel_max(max_CFLv_axis,CFLv_axis)
  call parallel_max(max_CFLd_x,CFLd_x)
  call parallel_max(max_CFLd_y,CFLd_y)
  call parallel_max(max_CFLd_z,CFLd_z)
  call parallel_max(max_CFLd_axis,CFLd_axis)

  ! Set the main CFL based on the implicit directions
  if (icyl.eq.1) then
     CFL = max(CFLc_x,CFLc_y)
  else
     CFL = max(CFLc_x,CFLc_y,CFLc_z)
  end if
  if (.not.imp_visc_x .and. nx.gt.1) CFL = max(CFL,CFLv_x,CFLd_x)

  if (.not.imp_conv_y .and. ny.gt.1) CFL = max(CFL,CFLc_y)
  if (.not.imp_visc_y .and. ny.gt.1) CFL = max(CFL,CFLv_y,CFLd_y)

  if (.not.imp_conv_z .and. nz.gt.1) CFL = max(CFL,CFLc_z)
  if (.not.imp_visc_z .and. nz.gt.1) CFL = max(CFL,CFLv_z,CFLd_z)

  if (icyl.eq.1 .and. nz.gt.1 .and. isect.eq.0) then
     CFL = max(CFL,CFLc_axis)
     CFL = max(CFL,CFLv_axis)
     CFL = max(CFL,CFLd_axis)
  end if
  if (icyl.eq.1 .and. nz.gt.1) then
     CFL = max(CFL,0.1_WP*CFLc_z)
  end if

  ! Consider the level set source term
  if (use_lvlset) then
     CFL_lvl = inverse_burning_timescale * dt_uvw
     CFL = max(CFL,CFL_lvl)
  end if

  ! Check if we can do better for the implicit solver
  call implicit_check

  return
end subroutine velocity_CFL


! ================================== !
! Treatment of the centerline CFL    !
! Modification of the modeled VISC   !
! and DIFF such that viscous effects !
! can never be cfl-limiting          !
! ================================== !
subroutine velocity_CFL_centerline
  use implicit
  use data
  use parallel
  implicit none
  
  integer :: i,j,k,isc
  real(WP) :: pred_CFLv_axis
  real(WP), parameter :: CFLv_max = 0.5_WP
  
  ! Only if cylindrical and not DNS
  if (icyl.eq.0 .or. .not.use_sgs) return
  
  ! For each centerline point
  if (jproc.eq.1) then
     do k=kmin_,kmax_
        j=jmin_
        do i=imin_,imax_
           ! Predict diffusive centerline CFL
           pred_CFLv_axis = dt_uvw * abs( 4.0_WP*VISC(i,j,k) ) * dyi(j)**2 / RHOmid(i,j,k)
           ! Adjust VISC
           if (pred_CFLv_axis.gt.CFLv_max) then
              VISC(i,j,k) = VISC(i,j,k)*CFLv_max/pred_CFLv_axis
           end if
           ! Same for scalars
           do isc=1,nscalar
              ! Predict diffusive centerline CFL
              pred_CFLv_axis = dt_uvw * abs( 4.0_WP*DIFF(i,j,k,isc) ) * dyi(j)**2 / RHOmid(i,j,k)
              ! Adjust DIFF
              if (pred_CFLv_axis.gt.CFLv_max) then
                 DIFF(i,j,k,isc) = DIFF(i,j,k,isc)*CFLv_max/pred_CFLv_axis
              end if
           end do
        end do
     end do
  end if
  
  ! Update borders
  call boundary_update_border(VISC,'+','ym')
  do isc=1,nscalar
     call boundary_update_border(DIFF(:,:,:,isc),'+','ym')
  end do
  
  return
end subroutine velocity_CFL_centerline


! ================================== !
! Form DDADI operator for U velocity !
! ================================== !
subroutine velocity_operator_u
  use implicit
  use data
  use metric_generic
  use metric_velocity_conv
  use metric_velocity_visc
  use memory
  use time_info
  use masks
  implicit none

  integer  :: i,j,k,st,n
  real(WP) :: dt2,mydiag,VISCi

  ! Prepare scaled time step size for second order
  dt2 = dt_uvw/2.0_WP

  ! Zero operators =====================================
  Ax=0.0_WP; Ay=0.0_WP; Az=0.0_WP

  ! X-direction ========================================
  ! Convective part
  if (imp_conv_x) then
     if(use_ib) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 do st=-stc2,+stc1
                    if(ilgrgn_ux(i+st,j,k).eq.1) cycle
                    n = interp_xx(i,j,st)
                    Ax(j,k,i,st-stc1:st+stc2) = Ax(j,k,i,st-stc1:st+stc2) + 0.5_WP*dt2*divc_xx(i,j,k,st)*(rhoU(i+st+n+1,j,k)+rhoU(i+st-n,j,k))*interp_Ju_xm(i+st,j,:)
                    Ax(j,k,i,st+n+1) = Ax(j,k,i,st+n+1) + 0.5_WP*dt2*divc_xx(i,j,k,st)*rhoUi(i+st,j,k)
                    Ax(j,k,i,st-n)   = Ax(j,k,i,st-n)   + 0.5_WP*dt2*divc_xx(i,j,k,st)*rhoUi(i+st,j,k)
                 end do
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 do st=-stc2,+stc1
                    n = interp_xx(i,j,st)
                    Ax(j,k,i,st-stc1:st+stc2) = Ax(j,k,i,st-stc1:st+stc2) + 0.5_WP*dt2*divc_xx(i,j,k,st)*(rhoU(i+st+n+1,j,k)+rhoU(i+st-n,j,k))*interp_Ju_xm(i+st,j,:)
                    Ax(j,k,i,st+n+1) = Ax(j,k,i,st+n+1) + 0.5_WP*dt2*divc_xx(i,j,k,st)*rhoUi(i+st,j,k)
                    Ax(j,k,i,st-n)   = Ax(j,k,i,st-n)   + 0.5_WP*dt2*divc_xx(i,j,k,st)*rhoUi(i+st,j,k)
                 end do
              end do
           end do
        end do
     end if
  end if
  ! Viscous part
  if (imp_visc_x) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv2,stv1
                 do n=-stv1,stv2
                    Ax(j,k,i,st+n) = Ax(j,k,i,st+n) - dt2*divv_xx(i,j,k,st)*2.0_WP*VISC(i+st,j,k)*(grad_u_x(i+st,j,k,n)-1.0_WP/3.0_WP*divv_u(i+st,j,k,n))
                 end do
              end do
           end do
        end do
     end do
  end if
  ! Contribuiton of FCM
  if (imp_visc_x .and. use_fcm_momentum) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv2,stv1
                 do n=-stv1,stv2
                    Ax(j,k,i,st+n) = Ax(j,k,i,st+n) - dt2*divv_xx(i,j,k,st)*fcm_viscu(i+st,j,k,1)*grad_u_x(i+st,j,k,n)
                 end do
              end do
           end do
        end do
     end do
  end if

  ! Y-direction ========================================
  ! Convective part
  if (imp_conv_y) then
     if(use_ib) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 do st=-stc1,+stc2
                    if(ilgrgn_uy(i,j+st,k).eq.1) cycle
                    n = interp_xy(i,j,st)
                    Ay(i,k,j,st+n-1) = Ay(i,k,j,st+n-1) + 0.5_WP*dt2*divc_xy(i,j,k,st)*rhoVi(i,j+st,k)
                    Ay(i,k,j,st-n)   = Ay(i,k,j,st-n)   + 0.5_WP*dt2*divc_xy(i,j,k,st)*rhoVi(i,j+st,k)
                 end do
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 do st=-stc1,+stc2
                    n = interp_xy(i,j,st)
                    Ay(i,k,j,st+n-1) = Ay(i,k,j,st+n-1) + 0.5_WP*dt2*divc_xy(i,j,k,st)*rhoVi(i,j+st,k)
                    Ay(i,k,j,st-n)   = Ay(i,k,j,st-n)   + 0.5_WP*dt2*divc_xy(i,j,k,st)*rhoVi(i,j+st,k)
                 end do
              end do
           end do
        end do
     end if
  end if
  ! Viscous part
  if (imp_visc_y) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv1,stv2
                 VISCi = sum(interp_sc_xy(i,j+st,:,:)*VISC(i-st2:i+st1,j+st-st2:j+st+st1,k))
                 do n=-stv2,stv1
                    Ay(i,k,j,st+n) = Ay(i,k,j,st+n) - dt2*divv_xy(i,j,k,st)*VISCi*grad_u_y(i,j+st,k,n)
                 end do
              end do
           end do
        end do
     end do
  end if
  ! Contribution of FCM
  if (imp_visc_y .and. use_fcm_momentum) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv1,stv2
                 do n=-stv2,stv1
                    Ay(i,k,j,st+n) = Ay(i,k,j,st+n) - dt2*divv_xy(i,j,k,st)*fcm_viscu(i,j+st,k,2)*grad_u_y(i,j+st,k,n)
                 end do
              end do
           end do
        end do
     end do
  end if
  ! Centerline boundary conditions
  if (icyl.eq.1 .and. jproc.eq.1 .and. (imp_conv_y .or. imp_visc_y)) then
     do k=kmin_,kmax_
        do n=0,(ndy-1)/2-1
           do st=-(ndy-1)/2,-n-1
              Ay(:,k,jmin+n,-n) = Ay(:,k,jmin+n,-n) + Ay(:,k,jmin+n,st)
              Ay(:,k,jmin+n,st) = 0.0_WP
           end do
        end do
     end do
  end if

  ! Z-direction ========================================
  ! Convective part
  if (imp_conv_z) then
     if(use_ib) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 do st=-stc1,+stc2
                    if(ilgrgn_uz(i,j,k+st).eq.1) cycle
                    n = interp_xz(i,j,st)
                    Az(i,j,k,st+n-1) = Az(i,j,k,st+n-1) + 0.5_WP*dt2*divc_xz(i,j,k,st)*rhoWi(i,j,k+st)
                    Az(i,j,k,st-n)   = Az(i,j,k,st-n)   + 0.5_WP*dt2*divc_xz(i,j,k,st)*rhoWi(i,j,k+st)
                 end do
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 do st=-stc1,+stc2
                    n = interp_xz(i,j,st)
                    Az(i,j,k,st+n-1) = Az(i,j,k,st+n-1) + 0.5_WP*dt2*divc_xz(i,j,k,st)*rhoWi(i,j,k+st)
                    Az(i,j,k,st-n)   = Az(i,j,k,st-n)   + 0.5_WP*dt2*divc_xz(i,j,k,st)*rhoWi(i,j,k+st)
                 end do
              end do
           end do
        end do
     end if
  end if
  ! Viscous part
  if (imp_visc_z) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv1,stv2
                 VISCi = sum(interp_sc_xz(i,j,:,:)*VISC(i-st2:i+st1,j,k+st-st2:k+st+st1))
                 do n=-stv2,stv1
                    Az(i,j,k,st+n) = Az(i,j,k,st+n) - dt2*divv_xz(i,j,k,st)*VISCi*grad_u_z(i,j,k+st,n)
                 end do
              end do
           end do
        end do
     end do
  end if
  ! FCM contribution
  if (imp_visc_z .and. use_fcm_momentum) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv1,stv2
                 do n=-stv2,stv1
                    Az(i,j,k,st+n) = Az(i,j,k,st+n) - dt2*divv_xz(i,j,k,st)*fcm_viscu(i,j,k+st,3)*grad_u_z(i,j,k+st,n)
                 end do
              end do
           end do
        end do
     end do
  end if

  ! Create full diagonal ===============================
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           mydiag = sum(interp_sc_x(i,j,:)*RHOmid(i-st2:i+st1,j,k))+Ax(j,k,i,0)+Ay(i,k,j,0)+Az(i,j,k,0)
           Ax(j,k,i,0) = mydiag
           Ay(i,k,j,0) = mydiag
           Az(i,j,k,0) = mydiag
        end do
     end do
  end do

  ! Implicit IB treatment ==============================
  if (use_ib) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (SAu(i,j,k).gt.0.0_WP.and.VFu(i,j,k).gt.0.0_WP.and.mask_u(i,j).eq.0) then
                 mydiag=dt2*sum(interp_sc_x(i,j,:)*VISCmol(i-st2:i+st1,j,k))*SAu(i,j,k)/(VHu(i,j,k)*VFu(i,j,k)*dxm(i-1)*dy(j)*dz)
                 Ax(j,k,i,0) = Ax(j,k,i,0) + mydiag
                 Ay(i,k,j,0) = Ay(i,k,j,0) + mydiag
                 Az(i,j,k,0) = Az(i,j,k,0) + mydiag
              end if
           end do
        end do
     end do
  end if
  
  ! Implicit porous treatment ==========================
  if (use_porous) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (volu(i,j,k).gt.0.0_WP) then
                 mydiag=dt2*sum(interp_sc_x(i,j,:)*iperm(i-st2:i+st1,j,k)*poros(i-st2:i+st1,j,k)*VISCmol(i-st2:i+st1,j,k))
                 Ax(j,k,i,0) = Ax(j,k,i,0) + mydiag
                 Ay(i,k,j,0) = Ay(i,k,j,0) + mydiag
                 Az(i,j,k,0) = Az(i,j,k,0) + mydiag
              end if
           end do
        end do
     end do
  end if
  
  return
end subroutine velocity_operator_u


! ================================== !
! Inverse the Residual of u          !
! By using approximate factorization !
! ================================== !
subroutine velocity_inverse_u
  use implicit
  use data
  use metric_generic
  use metric_velocity_conv
  use metric_velocity_visc
  use memory
  use time_info
  use masks
  implicit none
  
  integer  :: i,j,k
    
  ! If purely explicit return
  if (.not.(imp_conv_x .or. imp_visc_x .or. &
            imp_conv_y .or. imp_visc_y .or. &
            imp_conv_z .or. imp_visc_z)) return
  
  ! Prepare the operator
  call velocity_operator_u
  
  ! X-direction ========================================
  if (imp_conv_x .or. imp_visc_x) then
     
     ! Prepare RHS
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Rx(j,k,i) = ResU(i,j,k)*sum(interp_sc_x(i,j,:)*RHOmid(i-st2:i+st1,j,k))
           end do
        end do
     end do
     
     ! Solve
     call linear_solver_x(ndx)
     
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Rx(j,k,i) = ResU(i,j,k)
           end do
        end do
     end do
  end if
  
  ! Y-direction ========================================
  if (imp_conv_y .or. imp_visc_y) then
     
     ! Prepare RHS
     if (imp_conv_x .or. imp_visc_x) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Ry(i,k,j) = Rx(j,k,i)*Ay(i,k,j,0)
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Ry(i,k,j) = Rx(j,k,i)*sum(interp_sc_x(i,j,:)*RHOmid(i-st2:i+st1,j,k))
              end do
           end do
        end do
     end if
     
     ! Solve
     call linear_solver_y(ndy)
     
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Ry(i,k,j) = Rx(j,k,i)
           end do
        end do
     end do
  end if
  
  ! Z-direction ========================================
  if (imp_conv_z .or. imp_visc_z) then
     
     ! Prepare RHS
     if (imp_conv_x .or. imp_visc_x .or. imp_conv_y .or. imp_visc_y) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Rz(i,j,k) = Ry(i,k,j)*Az(i,j,k,0)
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Rz(i,j,k) = Ry(i,k,j)*sum(interp_sc_x(i,j,:)*RHOmid(i-st2:i+st1,j,k))
              end do
           end do
        end do
     end if
     
     ! Solve
     call linear_solver_z(ndz)
     
     ! Get back the residual
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ResU(i,j,k) = Rz(i,j,k)
           end do
        end do
     end do
     
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ResU(i,j,k) = Ry(i,k,j)
           end do
        end do
     end do
  end if
  
  return
end subroutine velocity_inverse_u


! ================================== !
! Form DDADI operator for V velocity !
! ================================== !
subroutine velocity_operator_v
  use implicit
  use data
  use metric_generic
  use metric_velocity_conv
  use metric_velocity_visc
  use memory
  use time_info
  use masks
  implicit none

  integer  :: i,j,k,st,n
  real(WP) :: dt2,mydiag,VISCi

  ! Prepare scaled time step size for second order
  dt2 = dt_uvw/2.0_WP

  ! Zero operators =====================================
  Ax=0.0_WP; Ay=0.0_WP; Az=0.0_WP

  ! X-direction ========================================
  ! Convective part
  if (imp_conv_x) then
     if(use_ib) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 do st=-stc1,+stc2
                    if(ilgrgn_vx(i+st,j,k).eq.1) cycle
                    n = interp_yx(i,j,st)
                    Ax(j,k,i,st+n-1) = Ax(j,k,i,st+n-1) + 0.5_WP*dt2*divc_yx(i,j,k,st)*rhoUi(i+st,j,k)
                    Ax(j,k,i,st-n)   = Ax(j,k,i,st-n)   + 0.5_WP*dt2*divc_yx(i,j,k,st)*rhoUi(i+st,j,k)
                 end do
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 do st=-stc1,+stc2
                    n = interp_yx(i,j,st)
                    Ax(j,k,i,st+n-1) = Ax(j,k,i,st+n-1) + 0.5_WP*dt2*divc_yx(i,j,k,st)*rhoUi(i+st,j,k)
                    Ax(j,k,i,st-n)   = Ax(j,k,i,st-n)   + 0.5_WP*dt2*divc_yx(i,j,k,st)*rhoUi(i+st,j,k)
                 end do
              end do
           end do
        end do
     end if
  end if
  ! Viscous part
  if (imp_visc_x) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv1,stv2
                 VISCi = sum(interp_sc_xy(i+st,j,:,:)*VISC(i+st-st2:i+st+st1,j-st2:j+st1,k))
                 do n=-stv2,stv1
                    Ax(j,k,i,st+n) = Ax(j,k,i,st+n) - dt2*divv_yx(i,j,k,st)*VISCi*grad_v_x(i+st,j,k,n)
                 end do
              end do
           end do
        end do
     end do
  end if
  ! FCM contribution
  if (imp_visc_x .and. use_fcm_momentum) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv1,stv2
                 do n=-stv2,stv1
                    Ax(j,k,i,st+n) = Ax(j,k,i,st+n) - dt2*divv_yx(i,j,k,st)*fcm_viscv(i+st,j,k,1)*grad_v_x(i+st,j,k,n)
                 end do
              end do
           end do
        end do
     end do
  end if

  ! Y-direction ========================================
  ! Convective part
  if (imp_conv_y) then
     if(use_ib) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 do st=-stc2,+stc1
                    if(ilgrgn_vy(i,j+st,k).eq.1) cycle
                    n = interp_yy(i,j,st)
                    Ay(i,k,j,st-stc1:st+stc2) = Ay(i,k,j,st-stc1:st+stc2) + &
                         0.5_WP*dt2*divc_yy(i,j,k,st)*(rhoV(i,j+st+n+1,k)+rhoV(i,j+st-n,k))*interp_Jv_ym(i,j+st,:)
                    Ay(i,k,j,st+n+1) = Ay(i,k,j,st+n+1) + 0.5_WP*dt2*divc_yy(i,j,k,st)*rhoVi(i,j+st,k)
                    Ay(i,k,j,st-n)   = Ay(i,k,j,st-n)   + 0.5_WP*dt2*divc_yy(i,j,k,st)*rhoVi(i,j+st,k)
                 end do
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 do st=-stc2,+stc1
                    n = interp_yy(i,j,st)
                    Ay(i,k,j,st-stc1:st+stc2) = Ay(i,k,j,st-stc1:st+stc2) + &
                         0.5_WP*dt2*divc_yy(i,j,k,st)*(rhoV(i,j+st+n+1,k)+rhoV(i,j+st-n,k))*interp_Jv_ym(i,j+st,:)
                    Ay(i,k,j,st+n+1) = Ay(i,k,j,st+n+1) + 0.5_WP*dt2*divc_yy(i,j,k,st)*rhoVi(i,j+st,k)
                    Ay(i,k,j,st-n)   = Ay(i,k,j,st-n)   + 0.5_WP*dt2*divc_yy(i,j,k,st)*rhoVi(i,j+st,k)
                 end do
              end do
           end do
        end do
     end if
  end if
  ! Viscous part
  if (imp_visc_y) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv2,stv1
                 do n=-stv1,stv2
                    Ay(i,k,j,st+n) = Ay(i,k,j,st+n) - dt2*2.0_WP*VISC(i,j+st,k)*( &
                         +divv_yy(i,j,k,st)*(grad_v_y(i,j+st,k,n)-1.0_WP/3.0_WP*divv_v(i,j+st,k,n)) &
                         -yi(j)*interpv_cyl_F_y(i,j,st)*( ymi(j+st)*interpv_cyl_v_ym(i,j+st,n)-1.0_WP/3.0_WP*divv_v(i,j+st,k,n)) )
                 end do
              end do
           end do
        end do
     end do
  end if
  ! FCM contribution
  if (imp_visc_y .and. use_fcm_momentum) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv2,stv1
                 do n=-stv1,stv2
                    Ay(i,k,j,st+n) = Ay(i,k,j,st+n) - dt2*fcm_viscv(i,j+st,k,2)*divv_yy(i,j,k,st)*grad_v_y(i,j+st,k,n)
                 end do
              end do
           end do
        end do
     end do
  end if
  ! Boundary conditions
  if (icyl.eq.1 .and. jproc.eq.1 .and. (imp_conv_y .or. imp_visc_y)) then
     do k=kmin_,kmax_
        do n=0,(ndy-1)/2-1
           do st=-(ndy-1)/2,-n-1
              Ay(:,k,jmin+n,-n) = Ay(:,k,jmin+n,-n) + Ay(:,k,jmin+n,st)
              Ay(:,k,jmin+n,st) = 0.0_WP
           end do
        end do
     end do
  end if

  ! Z-direction ========================================
  ! Convective part
  if (imp_conv_z) then
     if(use_ib) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 do st=-stc1,+stc2
                    if(ilgrgn_vz(i,j,k+st).eq.1) cycle
                    n = interp_yz(i,j,st)
                    Az(i,j,k,st+n-1) = Az(i,j,k,st+n-1) + 0.5_WP*dt2*divc_yz(i,j,k,st)*rhoWi(i,j,k+st)
                    Az(i,j,k,st-n)   = Az(i,j,k,st-n)   + 0.5_WP*dt2*divc_yz(i,j,k,st)*rhoWi(i,j,k+st)
                 end do
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 do st=-stc1,+stc2
                    n = interp_yz(i,j,st)
                    Az(i,j,k,st+n-1) = Az(i,j,k,st+n-1) + 0.5_WP*dt2*divc_yz(i,j,k,st)*rhoWi(i,j,k+st)
                    Az(i,j,k,st-n)   = Az(i,j,k,st-n)   + 0.5_WP*dt2*divc_yz(i,j,k,st)*rhoWi(i,j,k+st)
                 end do
              end do
           end do
        end do
     end if
  end if
  ! Viscous part
  if (imp_visc_z) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv1,stv2
                 VISCi = sum(interp_sc_yz(i,j,:,:)*VISC(i,j-st2:j+st1,k+st-st2:k+st+st1))
                 do n=-stv2,stv1
                    Az(i,j,k,st+n) = Az(i,j,k,st+n) - dt2*divv_yz(i,j,k,st)*VISCi*grad_v_z(i,j,k+st,n)
                 end do
              end do
           end do
        end do
     end do
  end if
  ! FCM contribution
  if (imp_visc_z .and. use_fcm_momentum) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv1,stv2
                 do n=-stv2,stv1
                    Az(i,j,k,st+n) = Az(i,j,k,st+n) - dt2*divv_yz(i,j,k,st)*fcm_viscv(i,j,k+st,3)*grad_v_z(i,j,k+st,n)
                 end do
              end do
           end do
        end do
     end do
  end if

  ! Create full diagonal ===============================
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           mydiag = sum(interp_sc_y(i,j,:)*RHOmid(i,j-st2:j+st1,k))+Ax(j,k,i,0)+Ay(i,k,j,0)+Az(i,j,k,0)
           Ax(j,k,i,0) = mydiag
           Ay(i,k,j,0) = mydiag
           Az(i,j,k,0) = mydiag
        end do
     end do
  end do

  ! Implicit IB treatment ==============================
  if (use_ib) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (SAv(i,j,k).gt.0.0_WP.and.VFv(i,j,k).gt.0.0_WP.and.mask_v(i,j).eq.0) then
                 mydiag=dt2*sum(interp_sc_y(i,j,:)*VISCmol(i,j-st2:j+st1,k))*SAv(i,j,k)/(VHv(i,j,k)*VFv(i,j,k)*dx(i)*dym(j-1)*dz)
                 Ax(j,k,i,0) = Ax(j,k,i,0) + mydiag
                 Ay(i,k,j,0) = Ay(i,k,j,0) + mydiag
                 Az(i,j,k,0) = Az(i,j,k,0) + mydiag
              end if
           end do
        end do
     end do
  end if
  
  ! Implicit porous treatment ==========================
  if (use_porous) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (volv(i,j,k).gt.0.0_WP) then
                 mydiag=dt2*sum(interp_sc_y(i,j,:)*iperm(i,j-st2:j+st1,k)*poros(i,j-st2:j+st1,k)*VISCmol(i,j-st2:j+st1,k))
                 Ax(j,k,i,0) = Ax(j,k,i,0) + mydiag
                 Ay(i,k,j,0) = Ay(i,k,j,0) + mydiag
                 Az(i,j,k,0) = Az(i,j,k,0) + mydiag
              end if
           end do
        end do
     end do
  end if
  
  return
end subroutine velocity_operator_v


! ================================== !
! Inverse the Residual of v          !
! By using approximate factorization !
! ================================== !
subroutine velocity_inverse_v
  use implicit
  use data
  use metric_generic
  use metric_velocity_conv
  use metric_velocity_visc
  use memory
  use time_info
  use masks
  implicit none
  
  integer  :: i,j,k
  real(WP) :: Acos,Asin
  
  ! If purely explicit return
  if (.not.(imp_conv_x .or. imp_visc_x .or. &
            imp_conv_y .or. imp_visc_y .or. &
            imp_conv_z .or. imp_visc_z)) return
  
  ! Prepare the operator
  call velocity_operator_v
  
  ! X-direction ========================================
  if (imp_conv_x .or. imp_visc_x) then
     
     ! Prepare RHS
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Rx(j,k,i) = ResV(i,j,k)*sum(interp_sc_y(i,j,:)*RHOmid(i,j-st2:j+st1,k))
           end do
        end do
     end do
     
     ! Solve
     call linear_solver_x(ndx)
     
     ! Axis treatment
     if (icyl.eq.1 .and. jproc.eq.1) then
        do i=imin_,imax_
           Acos = 2.0_WP*sum(Rx(jmin,:,i)*cos(zm(kmin:kmax)))/real(nz,WP)
           Asin = 2.0_WP*sum(Rx(jmin,:,i)*sin(zm(kmin:kmax)))/real(nz,WP)
           Rx(jmin,:,i) = Acos*cos(zm(kmin:kmax)) + Asin*sin(zm(kmin:kmax))
        end do
     end if
     
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Rx(j,k,i) = ResV(i,j,k)
           end do
        end do
     end do
  end if
  
  ! Y-direction ========================================
  if (imp_conv_y .or. imp_visc_y) then
     
     ! Prepare RHS
     if (imp_conv_x .or. imp_visc_x) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Ry(i,k,j) = Rx(j,k,i)*Ay(i,k,j,0)
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Ry(i,k,j) = Rx(j,k,i)*sum(interp_sc_y(i,j,:)*RHOmid(i,j-st2:j+st1,k))
              end do
           end do
        end do
     end if
          
     ! Solve
     call linear_solver_y(ndy)
     
     ! Axis treatment
     if (icyl.eq.1 .and. jproc.eq.1) then
        do i=imin_,imax_
           Acos = 2.0_WP*sum(Ry(i,:,jmin)*cos(zm(kmin:kmax)))/real(nz,WP)
           Asin = 2.0_WP*sum(Ry(i,:,jmin)*sin(zm(kmin:kmax)))/real(nz,WP)
           Ry(i,:,jmin) = Acos*cos(zm(kmin:kmax)) + Asin*sin(zm(kmin:kmax))
        end do
     end if
     
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Ry(i,k,j) = Rx(j,k,i)
           end do
        end do
     end do
  end if
  
  ! Z-direction ========================================
  if (imp_conv_z .or. imp_visc_z) then
     
     ! Prepare RHS
     if (imp_conv_x .or. imp_visc_x .or. imp_conv_y .or. imp_visc_y) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Rz(i,j,k) = Ry(i,k,j)*Az(i,j,k,0)
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Rz(i,j,k) = Ry(i,k,j)*sum(interp_sc_y(i,j,:)*RHOmid(i,j-st2:j+st1,k))
              end do
           end do
        end do
     end if
     
     ! Solve
     call linear_solver_z(ndz)
     
     ! Morinishi's axis treatment
     if (icyl.eq.1 .and. jproc.eq.1) then
        do i=imin_,imax_
           Acos = 2.0_WP*sum(ResV(i,jmin,:)*cos(zm(kmin:kmax)))/real(nz,WP)
           Asin = 2.0_WP*sum(ResV(i,jmin,:)*sin(zm(kmin:kmax)))/real(nz,WP)
           Rz(i,jmin,:) = Acos*cos(zm(kmin:kmax)) + Asin*sin(zm(kmin:kmax))
        end do
     end if
     
     ! Get back the residual
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ResV(i,j,k) = Rz(i,j,k)
           end do
        end do
     end do
     
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ResV(i,j,k) = Ry(i,k,j)
           end do
        end do
     end do
  end if
  
  return
end subroutine velocity_inverse_v


! ================================== !
! Form DDADI operator for W velocity !
! ================================== !
subroutine velocity_operator_w
  use implicit
  use data
  use metric_generic
  use metric_velocity_conv
  use metric_velocity_visc
  use memory
  use time_info
  use masks
  implicit none

  integer  :: i,j,k,st,n
  real(WP) :: dt2,mydiag,VISCi

  ! Prepare scaled time step size for second order
  dt2 = dt_uvw/2.0_WP

  ! Zero operators =====================================
  Ax=0.0_WP; Ay=0.0_WP; Az=0.0_WP

  ! X-direction ========================================
  ! Convective part
  if (imp_conv_x) then
     if (use_ib) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 do st=-stc1,+stc2
                    if(ilgrgn_wx(i+st,j,k).eq.1) cycle
                    n = interp_zx(i,j,st)
                    Ax(j,k,i,st+n-1) = Ax(j,k,i,st+n-1) + 0.5_WP*dt2*divc_zx(i,j,k,st)*rhoUi(i+st,j,k)
                    Ax(j,k,i,st-n)   = Ax(j,k,i,st-n)   + 0.5_WP*dt2*divc_zx(i,j,k,st)*rhoUi(i+st,j,k)
                 end do
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 do st=-stc1,+stc2
                    n = interp_zx(i,j,st)
                    Ax(j,k,i,st+n-1) = Ax(j,k,i,st+n-1) + 0.5_WP*dt2*divc_zx(i,j,k,st)*rhoUi(i+st,j,k)
                    Ax(j,k,i,st-n)   = Ax(j,k,i,st-n)   + 0.5_WP*dt2*divc_zx(i,j,k,st)*rhoUi(i+st,j,k)
                 end do
              end do
           end do
        end do
     end if
  end if
  ! Viscous part
  if (imp_visc_x) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv1,stv2
                 VISCi = sum(interp_sc_xz(i+st,j,:,:)*VISC(i+st-st2:i+st+st1,j,k-st2:k+st1))
                 do n=-stv2,stv1
                    Ax(j,k,i,st+n) = Ax(j,k,i,st+n) - dt2*divv_zx(i,j,k,st)*VISCi*grad_w_x(i+st,j,k,n)
                 end do
              end do
           end do
        end do
     end do
  end if
  ! FCM contribution
  if (imp_visc_x .and. use_fcm_momentum) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv1,stv2
                 do n=-stv2,stv1
                    Ax(j,k,i,st+n) = Ax(j,k,i,st+n) - dt2*divv_zx(i,j,k,st)*fcm_viscw(i+st,j,k,1)*grad_w_x(i+st,j,k,n)
                 end do
              end do
           end do
        end do
     end do
  end if

  ! Y-direction ========================================
  ! Convective part
  if (imp_conv_y) then
     if(use_ib) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 do st=-stc1,+stc2
                    if(ilgrgn_wy(i,j+st,k).eq.1) cycle
                    n = interp_zy(i,j,st)
                    Ay(i,k,j,st+n-1) = Ay(i,k,j,st+n-1) + 0.5_WP*dt2*divc_zy(i,j,k,st)*rhoVi(i,j+st,k)
                    Ay(i,k,j,st-n)   = Ay(i,k,j,st-n)   + 0.5_WP*dt2*divc_zy(i,j,k,st)*rhoVi(i,j+st,k)
                 end do
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 do st=-stc1,+stc2
                    n = interp_zy(i,j,st)
                    Ay(i,k,j,st+n-1) = Ay(i,k,j,st+n-1) + 0.5_WP*dt2*divc_zy(i,j,k,st)*rhoVi(i,j+st,k)
                    Ay(i,k,j,st-n)   = Ay(i,k,j,st-n)   + 0.5_WP*dt2*divc_zy(i,j,k,st)*rhoVi(i,j+st,k)
                 end do
              end do
           end do
        end do
     end if
  end if
  ! Viscous part
  if (imp_visc_y) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv1,stv2
                 VISCi = sum(interp_sc_yz(i,j+st,:,:)*VISC(i,j+st-st2:j+st+st1,k-st2:k+st1))
                 do n=-stv2,stv1
                    Ay(i,k,j,st+n) = Ay(i,k,j,st+n) - dt2*VISCi*( &
                         +divv_zy(i,j,k,st)*grad_w_y(i,j+st,k,n) &
                         +ymi(j)*interpv_cyl_F_ym(i,j,st)*(grad_w_y(i,j+st,k,n)-yi(j+st)*interpv_cyl_w_y(i,j+st,n)) )
                 end do
              end do
           end do
        end do
     end do
  end if
  ! FCM contribution
  if (imp_visc_y .and. use_fcm_momentum) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv1,stv2
                 do n=-stv2,stv1
                    Ay(i,k,j,st+n) = Ay(i,k,j,st+n) - dt2*fcm_viscw(i,j+st,k,2)*divv_zy(i,j,k,st)*grad_w_y(i,j+st,k,n)
                 end do
              end do
           end do
        end do
     end do
  end if
  ! Boundary conditions
  if (icyl.eq.1 .and. jproc.eq.1 .and. (imp_conv_y .or. imp_visc_y)) then
     do k=kmin_,kmax_
        do n=0,(ndy-1)/2-1
           do st=-(ndy-1)/2,-n-1
              Ay(:,k,jmin+n,-n) = Ay(:,k,jmin+n,-n) + Ay(:,k,jmin+n,st)
              Ay(:,k,jmin+n,st) = 0.0_WP
           end do
        end do
     end do
  end if

  ! Z-direction ========================================
  ! Convective part
  if (imp_conv_z) then
     if(use_ib) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 do st=-stc2,+stc1
                    if(ilgrgn_wz(i,j,k+st).eq.1) cycle
                    n = interp_zz(i,j,st)
                    Az(i,j,k,st-stc1:st+stc2) = Az(i,j,k,st-stc1:st+stc2) + &
                         0.5_WP*dt2*divc_zz(i,j,k,st)*(rhoW(i,j,k+st+n+1)+rhoW(i,j,k+st-n))*interp_Jw_zm(i,j,:) + &
                         dt2*ymi(j)*interp_cyl_F_z(i,j,st)*sum(interp_cyl_v_ym(i,j,:)*rhoV(i,j-stc1:j+stc2,k+st))*interp_cyl_w_zm(i,j,:)
                    Az(i,j,k,st+n+1) = Az(i,j,k,st+n+1) + 0.5_WP*dt2*divc_zz(i,j,k,st)*rhoWi(i,j,k+st)
                    Az(i,j,k,st-n)   = Az(i,j,k,st-n)   + 0.5_WP*dt2*divc_zz(i,j,k,st)*rhoWi(i,j,k+st)
                 end do
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 do st=-stc2,+stc1
                    n = interp_zz(i,j,st)
                    Az(i,j,k,st-stc1:st+stc2) = Az(i,j,k,st-stc1:st+stc2) + &
                         0.5_WP*dt2*divc_zz(i,j,k,st)*(rhoW(i,j,k+st+n+1)+rhoW(i,j,k+st-n))*interp_Jw_zm(i,j,:) + &
                         dt2*ymi(j)*interp_cyl_F_z(i,j,st)*sum(interp_cyl_v_ym(i,j,:)*rhoV(i,j-stc1:j+stc2,k+st))*interp_cyl_w_zm(i,j,:)
                    Az(i,j,k,st+n+1) = Az(i,j,k,st+n+1) + 0.5_WP*dt2*divc_zz(i,j,k,st)*rhoWi(i,j,k+st)
                    Az(i,j,k,st-n)   = Az(i,j,k,st-n)   + 0.5_WP*dt2*divc_zz(i,j,k,st)*rhoWi(i,j,k+st)
                 end do
              end do
           end do
        end do
     end if
  end if
  ! Viscous part
  if (imp_visc_z) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv2,stv1
                 do n=-stv1,stv2
                    Az(i,j,k,st+n) = Az(i,j,k,st+n) - &
                         dt2*divv_zz(i,j,k,st)*2.0_WP*VISC(i,j,k+st)*(grad_w_z(i,j,k+st,n)-1.0_WP/3.0_WP*divv_w(i,j,k+st,n))
                 end do
              end do
           end do
        end do
     end do
  end if
  ! FCM contribution
  if (imp_visc_z .and. use_fcm_momentum) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              do st=-stv2,stv1
                 do n=-stv1,stv2
                    Az(i,j,k,st+n) = Az(i,j,k,st+n) - dt2*divv_zz(i,j,k,st)*fcm_viscw(i,j,k+st,3)*grad_w_z(i,j,k+st,n)
                 end do
              end do
           end do
        end do
     end do
  end if
  ! Create full diagonal ===============================
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           mydiag = sum(interp_sc_z(i,j,:)*RHOmid(i,j,k-st2:k+st1))+Ax(j,k,i,0)+Ay(i,k,j,0)+Az(i,j,k,0)
           Ax(j,k,i,0) = mydiag
           Ay(i,k,j,0) = mydiag
           Az(i,j,k,0) = mydiag
        end do
     end do
  end do

  ! Implicit IB treatment ==============================
  if (use_ib) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (SAw(i,j,k).gt.0.0_WP.and.VFw(i,j,k).gt.0.0_WP.and.mask_w(i,j).eq.0) then
                 mydiag=dt2*sum(interp_sc_z(i,j,:)*VISCmol(i,j,k-st2:k+st1))*SAw(i,j,k)/(VHw(i,j,k)*VFw(i,j,k)*dx(i)*dy(j)*dz)
                 Ax(j,k,i,0) = Ax(j,k,i,0) + mydiag
                 Ay(i,k,j,0) = Ay(i,k,j,0) + mydiag
                 Az(i,j,k,0) = Az(i,j,k,0) + mydiag
              end if
           end do
        end do
     end do
  end if
  
  ! Implicit porous treatment ==========================
  if (use_porous) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (volw(i,j,k).gt.0.0_WP) then
                 mydiag=dt2*sum(interp_sc_z(i,j,:)*iperm(i,j,k-st2:k+st1)*poros(i,j,k-st2:k+st1)*VISCmol(i,j,k-st2:k+st1))
                 Ax(j,k,i,0) = Ax(j,k,i,0) + mydiag
                 Ay(i,k,j,0) = Ay(i,k,j,0) + mydiag
                 Az(i,j,k,0) = Az(i,j,k,0) + mydiag
              end if
           end do
        end do
     end do
  end if
  
  return
end subroutine velocity_operator_w


! ================================== !
! Inverse the Residual of w          !
! By using approximate factorization !
! ================================== !
subroutine velocity_inverse_w
  use implicit
  use data
  use metric_generic
  use metric_velocity_conv
  use metric_velocity_visc
  use memory
  use time_info
  use masks
  implicit none
  
  integer  :: i,j,k
  
  ! If purely explicit return
  if (.not.(imp_conv_x .or. imp_visc_x .or. &
            imp_conv_y .or. imp_visc_y .or. &
            imp_conv_z .or. imp_visc_z)) return
  
  ! Prepare the operator
  call velocity_operator_w
  
  ! X-direction ========================================
  if (imp_conv_x .or. imp_visc_x) then
          
     ! Prepare RHS
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Rx(j,k,i) = ResW(i,j,k)*sum(interp_sc_z(i,j,:)*RHOmid(i,j,k-st2:k+st1))
           end do
        end do
     end do
     
     ! Solve
     call linear_solver_x(ndx)
     
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Rx(j,k,i) = ResW(i,j,k)
           end do
        end do
     end do
  end if
  
  ! Y-direction ========================================
  if (imp_conv_y .or. imp_visc_y) then
     
     ! Prepare RHS
     if (imp_conv_x .or. imp_visc_x) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Ry(i,k,j) = Rx(j,k,i)*Ay(i,k,j,0)
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Ry(i,k,j) = Rx(j,k,i)*sum(interp_sc_z(i,j,:)*RHOmid(i,j,k-st2:k+st1))
              end do
           end do
        end do
     end if
     
     ! Solve
     call linear_solver_y(ndy)
     
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Ry(i,k,j) = Rx(j,k,i)
           end do
        end do
     end do
  end if
  
  ! Z-direction ========================================
  if (imp_conv_z .or. imp_visc_z) then
     
     ! Prepare RHS
     if (imp_conv_x .or. imp_visc_x .or. imp_conv_y .or. imp_visc_y) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Rz(i,j,k) = Ry(i,k,j)*Az(i,j,k,0)
              end do
           end do
        end do
     else
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Rz(i,j,k) = Ry(i,k,j)*sum(interp_sc_z(i,j,:)*RHOmid(i,j,k-st2:k+st1))
              end do
           end do
        end do
     end if
     
     ! Solve
     call linear_solver_z(ndz)
     
     ! Get back the residual
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ResW(i,j,k) = Rz(i,j,k)
           end do
        end do
     end do
     
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ResW(i,j,k) = Ry(i,k,j)
           end do
        end do
     end do
  end if
  
  return
end subroutine velocity_inverse_w


! ============================ !
! Monitor the implicit solvers !
! ============================ !
subroutine implicit_monitor
  use implicit
  use time_info
  use data
  implicit none
  
  ! Compute the CFL
  call velocity_CFL
  
  ! Transfer the values to monitor
  call monitor_select_file('timestep')
  call monitor_set_single_value(1,dt)
  call monitor_set_single_value(2,CFL)
  call monitor_set_single_value(3,CFLc_x)
  call monitor_set_single_value(4,CFLc_y)
  call monitor_set_single_value(5,CFLc_z)
  call monitor_set_single_value(6,CFLc_axis)
  call monitor_set_single_value(7,CFLv_x)
  call monitor_set_single_value(8,CFLv_y)
  call monitor_set_single_value(9,CFLv_z)
  call monitor_set_single_value(10,CFLv_axis)
  if (nscalar.ne.0) then
     call monitor_set_single_value(11,CFLd_x)
     call monitor_set_single_value(12,CFLd_y)
     call monitor_set_single_value(13,CFLd_z)
     call monitor_set_single_value(14,CFLd_axis)     
  end if
  
  return
end subroutine implicit_monitor
