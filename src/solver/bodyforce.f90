module bodyforce
  use precision
  use geometry
  use borders
  use partition
  implicit none
  
  ! Mass flow rate forcing
  logical :: force_mfr,force_x,force_y,force_z
  real(WP), dimension(3) :: meanSRC,MFRinit
  
  ! Body force for channels and pipes
  real(WP), dimension(:), allocatable :: BFx
  real(WP), dimension(:), allocatable :: BFz
  real(WP), dimension(:), allocatable :: umean
  real(WP), dimension(:), allocatable :: wmean
  real(WP), dimension(:), allocatable :: ubulk
  real(WP), dimension(:), allocatable :: wbulk
  
  ! Forcing coefficient for HIT
  real(WP) :: Ahit
  ! Filter constants for HIT
  logical :: filter_hit
  integer :: hit_order
  real(WP) :: hit_length
  
  ! Body force for boundary layers
  real(WP), dimension(:), allocatable :: Uy
  real(WP) :: deltas,deltas0
  
  ! Gravity term
  logical :: use_gravity
  real(WP), dimension(3) :: gravity
  
  ! Pressure gradient forcing
  logical :: use_pgrad
  real(WP), dimension(3) :: pgrad
  
  ! Porous region
  logical :: use_porous
  real(WP), dimension(:,:,:), allocatable :: iperm

  ! Rotating reference frame
  logical :: use_rotating
  real(WP), dimension(3) :: rot_xo, rot_omega
  
contains
  
  ! Compute friction force on the walls
  subroutine compute_friction
    use data
    use metric_generic
    use metric_velocity_visc
    use parallel
    use masks
    implicit none
    
    integer :: i,j,k,nflow
    real(WP) :: tmp_x,tmp_z
    
    do nflow=1,ninlet
       
       ! Lower wall
       tmp_x = 0.0_WP
       tmp_z = 0.0_WP
       if (inlet(nflow)%jmin.ge.jmin_ .and. inlet(nflow)%jmin.le.jmax_) then 
          j = max(inlet(nflow)%jmin,jmin_)
          if (mask(imin_,j-1).eq.1) then
             do k=kmin_,kmax_
                do i=imin_,imax_
                   tmp_x = tmp_x + dz_v(j) * dxm(i-1) * &
                        sum(interp_sc_xy(i,j,:,:)*VISC(i-st2:i+st1,j-st2:j+st1,k))* &
                        sum(grad_u_y(i,j,k,:)*U(i,j-stv2:j+stv1,k))
                   
                   tmp_z = tmp_z + dz_v(j) * dxm(i-1) * &
                        sum(interp_sc_yz(i,j,:,:)*VISC(i,j-st2:j+st1,k-st2:k+st1)) * &
                        ( sum(grad_w_y(i,j,k,:)*W(i,j-stv2:j+stv1,k)) &
                        - ymmi(j)*sum(interpv_cyl_w_y(i,j,:)*W(i,j-stv2:j+stv1,k)) )
                end do
             end do
          end if
       end if
       
       ! Upper wall
       if (inlet(nflow)%jmax+1.ge.jmin_ .and. inlet(nflow)%jmax+1.le.jmax_) then
          j = min(inlet(nflow)%jmax+1,jmax_+1)
          if (mask(imin_,j).eq.1) then
             do k=kmin_,kmax_
                do i=imin_,imax_
                   tmp_x = tmp_x - dz_v(j) * dxm(i-1) * &
                        sum(interp_sc_xy(i,j,:,:)*VISC(i-st2:i+st1,j-st2:j+st1,k))* &
                        sum(grad_u_y(i,j,k,:)*U(i,j-stv2:j+stv1,k))
                   
                   tmp_z = tmp_z - dz_v(j) * dxm(i-1) * &
                        sum(interp_sc_yz(i,j,:,:)*VISC(i,j-st2:j+st1,k-st2:k+st1)) * &
                        ( sum(grad_w_y(i,j,k,:)*W(i,j-stv2:j+stv1,k)) &
                        - ymmi(j)*sum(interpv_cyl_w_y(i,j,:)*W(i,j-stv2:j+stv1,k)) )
                end do
             end do
          end if
       end if
       
       call parallel_sum(tmp_x,BFx(nflow))
       call parallel_sum(tmp_z,BFz(nflow))
       
       BFx(nflow) = BFx(nflow)/inlet(nflow)%A
       BFz(nflow) = BFz(nflow)/inlet(nflow)%A
    end do
    
    BFx  = BFx /(xm(imax)-xm(imin-1))
    BFz  = BFz /(xm(imax)-xm(imin-1))

    return
  end subroutine compute_friction
  
  ! Compute mass flow rate between the walls
  subroutine compute_massflowrate
    use parallel
    use data
    implicit none
    
    integer :: i,j,k,nflow
    real(WP) :: tmp_x
    real(WP) :: tmp_z
    
    do nflow=1,ninlet
       tmp_x = 0.0_WP
       tmp_z = 0.0_WP
       
       do j = max(inlet(nflow)%jmin,jmin_),min(inlet(nflow)%jmax,jmax_)
          do k=kmin_,kmax_
             do i=imin_,imax_
                tmp_x = tmp_x + rhoU(i,j,k)*dA(i,j,k)*dxm(i-1)
                tmp_z = tmp_z + rhoW(i,j,k)*dA(i,j,k)*dxm(i-1)
             end do
          end do
       end do
       
       call parallel_sum(tmp_x,umean(nflow))
       call parallel_sum(tmp_z,wmean(nflow))

       umean(nflow) = umean(nflow)/inlet(nflow)%A
       wmean(nflow) = wmean(nflow)/inlet(nflow)%A
    end do
    
    umean = umean/(xm(imax)-xm(imin-1))
    wmean = wmean/(xm(imax)-xm(imin-1))
    
    return
  end subroutine compute_massflowrate
  
  ! Compute mass flow rate in a duct
  subroutine compute_mfr_duct
    use parallel
    use data
    implicit none
    
    integer :: i,j,k
    real(WP) :: mybuf
    
    mybuf=0.0_WP
    do k=kmin_,kmax_
       do j=jmin_,jmax_
          do i=imin_,imax_
             mybuf=mybuf+vol(i,j,k)*rhoW(i,j,k)
          end do
       end do
    end do
    call parallel_sum(mybuf,wmean(1))
    wmean(1)=wmean(1)/vol_total
    
    return
  end subroutine compute_mfr_duct
  
  ! Compute mass flow rate
  ! Assumes a call during velocity_step and recomputes rhoU^n+1
  subroutine compute_mfr(mfr)
    use metric_generic
    use data
    use velocity
    implicit none
    
    integer :: i,j,k
    real(WP), dimension(3), intent(out) :: mfr
    real(WP) :: buf1,buf2,buf3
    
    ! Integrate mass flow rate
    buf1=0.0_WP; buf2=0.0_WP; buf3=0.0_WP
    do k=kmin_,kmax_
       do j=jmin_,jmax_
          do i=imin_,imax_
             buf1 = buf1 + volu(i,j,k)*(2.0_WP*rhoU(i,j,k)-rhoUold(i,j,k))
             buf2 = buf2 + volv(i,j,k)*(2.0_WP*rhoV(i,j,k)-rhoVold(i,j,k))
             buf3 = buf3 + volw(i,j,k)*(2.0_WP*rhoW(i,j,k)-rhoWold(i,j,k))
          end do
       end do
    end do
    call parallel_sum(buf1,mfr(1)); mfr(1)=mfr(1)/volu_total
    call parallel_sum(buf2,mfr(2)); mfr(2)=mfr(2)/volv_total
    call parallel_sum(buf3,mfr(3)); mfr(3)=mfr(3)/volw_total
    
    return
  end subroutine compute_mfr
  
  ! Compute momentum thickness
  subroutine compute_disp_thickness
    use parallel
    use data
    implicit none
    
    integer  :: i,j,k
    real(WP) :: tmp,Uinft
    
    ! Compute mean profile
    Uy = 0.0_WP
    do j = jmin_,jmax_
       do k=kmin_,kmax_
          do i=imin_,imax_
             Uy(j) = Uy(j) + U(i,j,k)
          end do
       end do
    end do
    do j=jmin,jmax
       call parallel_sum(Uy(j),tmp)
       Uy(j) = tmp/real(nx*nz)
    end do
    
    ! Normalize
    Uinft = Uy(jmax) + epsilon(1.0_WP)
    Uy = Uy / Uinft

    ! Compute momentum thickness and gamma terms
    deltas = 0.0_WP
    do j=jmin,jmax
       deltas = deltas + (1.0_WP-Uy(j))*dy(j)
    end do

    return
  end subroutine compute_disp_thickness
  

  ! ================================================== !
  ! Source term for pipes and channels                 !
  !   -> relaxation towards mean bulk velocity (alpha) !
  !   -> counter pressure drop due to friction         !
  ! *** Pre step ***                                   !
  ! ================================================== !
  subroutine bodyforce_pipe_channel
    use velocity
    implicit none
    
    integer :: j,nflow
    real(WP), parameter :: alpha = 1.0_WP
    
    call compute_friction
    call compute_massflowrate
    
    do nflow=1,ninlet
       do j = max(inlet(nflow)%jmin,jmin_),min(inlet(nflow)%jmax,jmax_)
          srcU(:,j,:) = alpha*(ubulk(nflow)-umean(nflow)) + dt_uvw*BFx(nflow)
          srcW(:,j,:) = alpha*(wbulk(nflow)-wmean(nflow)) + dt_uvw*BFz(nflow)
       end do
    end do
    
    return
  end subroutine bodyforce_pipe_channel
  
  
  ! ================================================== !
  ! Source term for duct flow                          !
  !   -> relaxation towards mean bulk velocity (alpha) !
  !   -> counter pressure drop due to friction         !
  ! *** Pre step ***                                   !
  ! ================================================== !
  subroutine bodyforce_duct
    use data
    use metric_generic
    use metric_velocity_visc
    use parallel
    use masks
    use velocity
    implicit none
    
    integer :: i,j,k
    real(WP), parameter :: alpha=1.0_WP
    real(WP) :: buf
    
    ! Compute friction in the duct
    buf=0.0_WP
    ! X- wall
    if (iproc.eq.1) then
       i=imin+1
       do k=kmin_,kmax_
          do j=max(jmin_,jmin+1),min(jmax_,jmax-1)
             buf=buf+dz*dy(j)*sum(interp_sc_xz(i,j,:,:)*VISC(i-st2:i+st1,j,k-st2:k+st1))*sum(grad_w_x(i,j,k,:)*W(i-stv2:i+stv1,j,k))
          end do
       end do
    end if
    ! X+ wall
    if (iproc.eq.npx) then
       i=imax
       do k=kmin_,kmax_
          do j=max(jmin_,jmin+1),min(jmax_,jmax-1)
             buf=buf+dz*dy(j)*sum(interp_sc_xz(i,j,:,:)*VISC(i-st2:i+st1,j,k-st2:k+st1))*sum(grad_w_x(i,j,k,:)*W(i-stv2:i+stv1,j,k))
          end do
       end do
    end if
    ! Y- wall
    if (jproc.eq.1) then
       j=jmin+1
       do k=kmin_,kmax_
          do i=max(imin_,imin+1),min(imax_,imax-1)
             buf=buf+dz*dx(i)*sum(interp_sc_yz(i,j,:,:)*VISC(i,j-st2:j+st1,k-st2:k+st1))*sum(grad_w_y(i,j,k,:)*W(i,j-stv2:j+stv1,k))
          end do
       end do
    end if
    ! Y+ wall
    if (jproc.eq.npy) then
       j=jmax
       do k=kmin_,kmax_
          do i=max(imin_,imin+1),min(imax_,imax-1)
             buf=buf+dz*dx(i)*sum(interp_sc_yz(i,j,:,:)*VISC(i,j-st2:j+st1,k-st2:k+st1))*sum(grad_w_y(i,j,k,:)*W(i,j-stv2:j+stv1,k))
          end do
       end do
    end if
    ! Gather data
    call parallel_sum(buf,BFz(1))
    BFz(1)=BFz(1)/vol_total
    
    ! Get mass flow rate
    call compute_mfr_duct
    
    ! Add forcing term
    do k=kmin_,kmax_
       do j=jmin_,jmax_
          do i=imin_,imax_
             if (mask(i,j).ne.1) srcW(i,j,k)=alpha*(wbulk(1)-wmean(1))+dt_uvw*BFz(1)
          end do
       end do
    end do
    
    return
  end subroutine bodyforce_duct
  

  ! ================================================== !
  ! Source term for boundary layers                    !
  !   -> relaxation towards mean momentum thickness    !
  !   -> counter pressure drop due to friction         !
  ! *** Pre step ***                                   !
  ! ================================================== !
  subroutine bodyforce_boundary_layer
    use velocity
    implicit none
    
    real(WP), parameter :: alpha = 1.0_WP
    integer :: j

    call compute_friction
    call compute_massflowrate
    !call compute_disp_thickness
    
    do j = max(inlet(1)%jmin,jmin_),min(inlet(1)%jmax,jmax_)
       srcU(:,j,:) = alpha*(ubulk(1)-umean(1)) + dt_uvw*BFx(1)
    end do
    
    return
  end subroutine bodyforce_boundary_layer
  
  
  ! ================================================== !
  ! Source term for forced isotropic turbulence        !
  ! Following Meneveau PoF 17 (2005), Lundgren (2003)  !
  ! *** Mid step ***                                   !
  ! ================================================== !
  subroutine bodyforce_hit
    use data
    use velocity
    use memory
    implicit none
    
    real(WP) :: mysrcUtmp,mysrcVtmp,mysrcWtmp
    real(WP) :: srcUtmp,srcVtmp,srcWtmp
    integer :: i,j,k
    
    ! Return if no forcing
    if (Ahit.eq.0.0_WP) return
    
    ! Filter velocity field
    if (filter_hit) then
       call raymond_filter(rhoU,tmp1,hit_order,hit_length,'+','ym')
       call raymond_filter(rhoV,tmp2,hit_order,hit_length,'-','y')
       call raymond_filter(rhoW,tmp3,hit_order,hit_length,'-','ym')
    else
       tmp1 = rhoU
       tmp2 = rhoV
       tmp3 = rhoW
    end if
    
    ! Ensure that <srcU/V/W> = 0
    mysrcUtmp=0.0_WP
    mysrcVtmp=0.0_WP
    mysrcWtmp=0.0_WP
    do k=kmin_,kmax_
       do j=jmin_,jmax_
          do i=imin_,imax_
             mysrcUtmp=mysrcUtmp+tmp1(i,j,k)*volu(i,j,k)
             mysrcVtmp=mysrcVtmp+tmp2(i,j,k)*volv(i,j,k)
             mysrcWtmp=mysrcWtmp+tmp3(i,j,k)*volw(i,j,k)
          end do
       end do
    end do
    call parallel_sum(mysrcUtmp,srcUtmp)
    call parallel_sum(mysrcVtmp,srcVtmp)
    call parallel_sum(mysrcWtmp,srcWtmp)
    tmp1=tmp1-srcUtmp/volu_total
    tmp2=tmp2-srcVtmp/volv_total
    tmp3=tmp3-srcWtmp/volw_total
    
    ! Add filtered velocity as source
    srcUmid = srcUmid+dt_uvw*Ahit*tmp1
    srcVmid = srcVmid+dt_uvw*Ahit*tmp2
    srcWmid = srcWmid+dt_uvw*Ahit*tmp3
    
    return
  end subroutine bodyforce_hit
  
  
  ! =================== !
  ! Gravity source term !
  ! *** Mid step ***    !
  ! =================== !
  subroutine bodyforce_gravity
    use data
    use velocity
    use masks
    use metric_generic
    
    integer :: i,j,k
    real(WP) :: RHOx,RHOy,RHOz
    
    ! Call another routine if multiphase
    if (use_multiphase) then
       call multiphase_gravity
       return
    end if
    
    ! Calculate gravitational force
    do k=kmin_,kmax_
       do j=jmin_,jmax_
          do i=imin_,imax_
             RHOx = sum(interp_sc_x(i,j,:)*RHO(i-st2:i+st1,j,k))
             RHOy = sum(interp_sc_y(i,j,:)*RHO(i,j-st2:j+st1,k))
             RHOz = sum(interp_sc_z(i,j,:)*RHO(i,j,k-st2:k+st1))
             if (mask_u(i,j).eq.0) srcUmid(i,j,k) = srcUmid(i,j,k)+dt_uvw*RHOx*gravity(1)
             if (mask_v(i,j).eq.0) srcVmid(i,j,k) = srcVmid(i,j,k)+dt_uvw*RHOy*gravity(2)
             if (mask_w(i,j).eq.0) srcWmid(i,j,k) = srcWmid(i,j,k)+dt_uvw*RHOz*gravity(3)
          end do
       end do
    end do
    
    return
  end subroutine bodyforce_gravity
  
  
  ! ==================== !
  ! Porosity source term !
  !   *** Mid step ***   !
  ! ==================== !
  subroutine bodyforce_porous
    use data
    use velocity
    use masks
    use metric_generic
    use optdata
    
    integer :: i,j,k
    real(WP) :: PORx,PORy,PORz
    
    ! Calculate porous drag force based on Darcy's law
    do k=kmin_,kmax_
       do j=jmin_,jmax_
          do i=imin_,imax_
             PORx = sum(interp_sc_x(i,j,:)*VISCmol(i-st2:i+st1,j,k)*iperm(i-st2:i+st1,j,k)*poros(i-st2:i+st1,j,k))
             PORy = sum(interp_sc_y(i,j,:)*VISCmol(i,j-st2:j+st1,k)*iperm(i,j-st2:j+st1,k)*poros(i,j-st2:j+st1,k))
             PORz = sum(interp_sc_z(i,j,:)*VISCmol(i,j,k-st2:k+st1)*iperm(i,j,k-st2:k+st1)*poros(i,j,k-st2:k+st1))
             if (volu(i,j,k).gt.0.0_WP) srcUmid(i,j,k) = srcUmid(i,j,k)-dt_uvw*U(i,j,k)*PORx
             if (volv(i,j,k).gt.0.0_WP) srcVmid(i,j,k) = srcVmid(i,j,k)-dt_uvw*V(i,j,k)*PORy
             if (volw(i,j,k).gt.0.0_WP) srcWmid(i,j,k) = srcWmid(i,j,k)-dt_uvw*W(i,j,k)*PORz
          end do
       end do
    end do
    
    return
  end subroutine bodyforce_porous

  ! ====================== !
  ! Rotational source term !
  ! *** Mid step ***       !
  ! ====================== !
  subroutine bodyforce_rotating
    use data
    use velocity
    use masks
    use metric_generic
    
    ! Call another routine if multiphase
    if (use_multiphase) then
       call multiphase_rotating
       return
    end if

    call die('Rotating reference frame not implemented for single phase simulation')
    
    return
  end subroutine bodyforce_rotating
  
  
  ! ============================= !
  ! Pressure gradient source term !
  ! *** Pre step ***              !
  ! ============================= !
  subroutine bodyforce_pgrad
    use data
    use velocity
    use masks
    use metric_generic
    
    integer :: i,j,k
    
    ! Impose pressure gradient
    do k=kmin_,kmax_
       do j=jmin_,jmax_
          do i=imin_,imax_
             if (mask_u(i,j).eq.0) srcU(i,j,k) = srcU(i,j,k)+dt_uvw*pgrad(1)
             if (mask_v(i,j).eq.0) srcV(i,j,k) = srcV(i,j,k)+dt_uvw*pgrad(2)
             if (mask_w(i,j).eq.0) srcW(i,j,k) = srcW(i,j,k)+dt_uvw*pgrad(3)
          end do
       end do
    end do
    
    return
  end subroutine bodyforce_pgrad
  
end module bodyforce


! ===================== !
! INITIALIZE the module !
! ===================== !
subroutine bodyforce_init
  use bodyforce
  use parser
  use config
  use optdata
  use data
  implicit none
  
  character(len=str_short) :: force_dir
  
  ! Check if mass flow rate needs to be forced
  force_mfr=.false.
  call parser_read('Force mass flow rate',force_dir,'none')
  force_x=.false.
  if (index(force_dir,'x').ne.0) then
     force_x=.true.
     if (xper.eq.0) call die('Cannot force mass flow rate in x, direction not periodic')
  end if
  force_y=.false.
  if (index(force_dir,'y').ne.0) then
     force_y=.true.
     if (yper.eq.0) call die('Cannot force mass flow rate in y, direction not periodic')
  end if
  force_z=.false.
  if (index(force_dir,'z').ne.0) then
     force_z=.true.
     if (zper.eq.0) call die('Cannot force mass flow rate in z, direction not periodic')
  end if
  if (force_x.or.force_y.or.force_z) force_mfr=.true.
  
  ! Prepare MFR forcing
  if (force_mfr) then
     
     ! Zero mean sources
     meanSRC=0.0_WP
     
     ! Compute initial mass flow rate
     call compute_mfr(MFRinit)
     
     ! Prepare monitoring
     call monitor_create_file_step('bodyforce',3)
     call monitor_set_header(1,'Force x','r')
     call monitor_set_header(2,'Force y','r')
     call monitor_set_header(3,'Force z','r')
     
  end if
  
  ! Channels and Pipes and boundary layers
  if (trim(simu_type).eq."channel" .or. trim(simu_type).eq."pipe") then
     
     ! Allocate the arrays
     allocate(BFx(ninlet))
     allocate(BFz(ninlet))
     allocate(umean(ninlet))
     allocate(wmean(ninlet))
     allocate(ubulk(ninlet))
     allocate(wbulk(ninlet))
     
     ! Compute initial mass flow rates and impose them 
     call compute_massflowrate
     ubulk = umean
     wbulk = wmean
     
  end if
  
  ! Duct case
  if (trim(simu_type).eq."duct") then
     
     ! Allocate the arrays
     allocate(BFx(1))
     allocate(BFz(1))
     allocate(umean(1))
     allocate(wmean(1))
     allocate(ubulk(1))
     allocate(wbulk(1))
     
     ! Compute initial mass flow rate and store
     call compute_mfr_duct
     wbulk = wmean
     
  end if
  
  ! HIT
  if (trim(simu_type).eq."hit") then
     call parser_read('Forcing coefficient',Ahit,0.0_WP)
     call parser_read('Filter HIT source',filter_hit,.false.)
     if (filter_hit) then
        call raymond_init
        call parser_read('HIT filter length',hit_length)
        call parser_read('HIT filter order',hit_order,2)
     end if
  end if
  
  ! Boundary Layer
  if (trim(simu_type).eq."boundary layer") then
     
     ! Allocate the arrays
     allocate(BFx(ninlet))
     allocate(BFz(ninlet))
     allocate(Uy(jmin:jmax))
     
     ! Compute inital displacement thickness
     call compute_disp_thickness
     deltas0 = deltas

     allocate(umean(ninlet))
     allocate(wmean(ninlet))
     allocate(ubulk(ninlet))
     allocate(wbulk(ninlet))
     
     ! Compute initial mass flow rates and impose them 
     call compute_massflowrate
     ubulk = umean
     wbulk = wmean

  end if
  
  ! Gravity
  gravity = 0.0_WP
  call parser_is_defined('Gravity',use_gravity)
  if (use_gravity) call parser_read('Gravity',gravity)
  if (icyl.eq.1) then
     if (gravity(2).ne.0.0_WP .or. gravity(3).ne.0.0_WP) then
        call die("bodyforce_init: in cylindrical coordinates, gravity must be aligned with x.")
     end if
  end if
  
  ! Pressure gradient forcing
  pgrad = 0.0_WP
  call parser_is_defined('Pressure gradient',use_pgrad)
  if (use_pgrad) call parser_read('Pressure gradient',pgrad)
  if (icyl.eq.1) then
     if (pgrad(2).ne.0.0_WP .or. pgrad(3).ne.0.0_WP) then
        call die("bodyforce_init: in cylindrical coordinates, pressure gradient must be aligned with x.")
     end if
  end if

  ! Rotating reference frame
  rot_xo=0.0_WP; rot_omega=0.0_WP
  call parser_is_defined('Rotational Velocity',use_rotating)
  if (use_rotating) call parser_read('Rotational Velocity',rot_omega)
  if (use_rotating) call parser_read('Rotational Center',  rot_xo)
  
  return
end subroutine bodyforce_init


! ============================================= !
! Separate initialization for porous flow model !
! ============================================= !
subroutine bodyforce_porous_init
  use bodyforce
  use parser
  use data
  use optdata
  implicit none
  
  character(len=str_short) :: name
  integer :: iod1,iod2,i
  
  ! Porous region
  call parser_read('Use porous',use_porous,.false.)
  if (use_porous) then
     iod1=0; iod2=0
     do i=1,nod
        read(OD_name(i),*) name
        ! Find inverse of permeability
        if (trim(name).eq.'IPERM') iod1=i
        ! Find inverse of permeability
        if (trim(name).eq.'POROS') iod2=i
     end do
     if (iod1.eq.0) call die("bodyforce_init: Porous model requires IPERM variable in optdata file.")
     if (iod2.eq.0) call die("bodyforce_init: Porous model requires POROS variable in optdata file.")
     allocate(iperm(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
     iperm=0.0_WP
     iperm(imin_:imax_,jmin_:jmax_,kmin_:kmax_)=OD(imin_:imax_,jmin_:jmax_,kmin_:kmax_,iod1)
     poros(imin_:imax_,jmin_:jmax_,kmin_:kmax_)=OD(imin_:imax_,jmin_:jmax_,kmin_:kmax_,iod2)
     call boundary_update_border(iperm,'+','ym')
     call boundary_update_border(poros,'+','ym')
  end if
  
  return
end subroutine bodyforce_porous_init


! ================================================= !
! Compute the SOURCE TERM for the momentum equation !
! *** Pre step ***                                  !
! ================================================= !
subroutine bodyforce_src
  use bodyforce
  use config
  implicit none
  
  ! Channels and Pipes
  if (trim(simu_type).eq."channel" .or. trim(simu_type).eq."pipe") then
     call bodyforce_pipe_channel
  end if
  
  ! Duct
  if (trim(simu_type).eq."duct") then
     call bodyforce_duct
  end if
  
  ! Boundary Layer
  if (trim(simu_type).eq."boundary layer") then
     call bodyforce_boundary_layer
  end if
  
  ! Pressure gradient
  if (use_pgrad) call bodyforce_pgrad
  
  return
end subroutine bodyforce_src


! ================================================= !
! Compute the SOURCE TERM for the momentum equation !
! *** Mid step ***                                  !
! ================================================= !
subroutine bodyforce_src_mid
  use bodyforce
  use config
  implicit none
  
  ! HIT - Meneveau forcing
  if (trim(simu_type).eq."hit") then
     call bodyforce_hit
  end if
  
  ! Gravity
  if (use_gravity) call bodyforce_gravity
  
  ! Porous region
  if (use_porous) call bodyforce_porous

  ! Rotating refernce frame
  if (use_rotating) call bodyforce_rotating
  
  return
end subroutine bodyforce_src_mid


! ==================================== !
! Add mass flow rate forcing term in   !
! the form of a mean pressure gradient !
! ==================================== !
subroutine bodyforce_mfr
  use bodyforce
  use velocity
  use parallel
  implicit none
  
  integer :: i,j,k
  real(WP) :: buf1,buf2,buf3
  real(WP), dimension(3) :: MFRnow
  
  ! Check if needed
  if (.not.force_MFR) return
  
  ! Compute new MFR
  call compute_mfr(MFRnow)
    
  ! Compute average source term
  buf1=0.0_WP; buf2=0.0_WP; buf3=0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           buf1=buf1-volu(i,j,k)*srcUmid(i,j,k)
           buf2=buf2-volv(i,j,k)*srcVmid(i,j,k)
           buf3=buf3-volw(i,j,k)*srcWmid(i,j,k)
        end do
     end do
  end do
  call parallel_sum(buf1,meanSRC(1)); meanSRC(1)=meanSRC(1)/volu_total
  call parallel_sum(buf2,meanSRC(2)); meanSRC(2)=meanSRC(2)/volv_total
  call parallel_sum(buf3,meanSRC(3)); meanSRC(3)=meanSRC(3)/volw_total
  
  ! Compute mean source
  meanSRC=meanSRC+MFRinit-MFRnow
  
  ! Zero direction if not forced
  if (.not.force_x) meanSRC(1)=0.0_WP
  if (.not.force_y) meanSRC(2)=0.0_WP
  if (.not.force_z) meanSRC(3)=0.0_WP
  
  ! Remove from srcmid
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (volu(i,j,k).gt.0.0_WP) srcUmid(i,j,k)=srcUmid(i,j,k)+meanSRC(1)
           if (volv(i,j,k).gt.0.0_WP) srcVmid(i,j,k)=srcVmid(i,j,k)+meanSRC(2)
           if (volw(i,j,k).gt.0.0_WP) srcWmid(i,j,k)=srcWmid(i,j,k)+meanSRC(3)
        end do
     end do
  end do
  
  return
end subroutine bodyforce_mfr


! ==================== !
! Bodyforce monitoring !
! ==================== !
subroutine bodyforce_monitor
  use bodyforce
  use time_info
  implicit none

  ! If not used, return
  if (.not.force_mfr) return

  ! Transfer to monitor
  call monitor_select_file('bodyforce')
  call monitor_set_single_value(1,meanSRC(1)/dt_uvw)
  call monitor_set_single_value(2,meanSRC(2)/dt_uvw)
  call monitor_set_single_value(3,meanSRC(3)/dt_uvw)
  
  return
end subroutine bodyforce_monitor
