module ib
  use precision
  use geometry
  use partition
  use optdata
  use metric_generic
  use metric_velocity_conv
  use metric_velocity_visc
  use pressure
  use time_info
  use ib_velocity
  implicit none

  ! Moving IB flag
  logical :: ibmove

  ! IB geometry type
  character(len=str_medium) :: ibgeom

  ! Fictitious cell merging ===== pressure =======
  logical :: use_fcm_pressure
  ! clipping values
  real(WP) :: Aclip,Vclip
  ! mixing fractions for volumetric and face data
  real(WP), dimension(:,:,:,:,:), allocatable :: mix_frac_vol
  real(WP), dimension(:,:,:,:,:,:), allocatable :: mix_frac_face

  ! Fictitious cell merging ===== momentum =======
  logical :: use_fcm_momentum
  logical :: use_ib_VA
  ! ensure that diffusive dominates convective flux by FCM_factor
  real(WP) :: FCM_factor
  ! ensure that face viscosity does not grow beyond FCM_clip*VISC
  real(WP) :: FCM_clip
  ! apply FCM procedure for cells smaller the FCM_volume_clip
  real(WP) :: FCM_volume_clip
  ! face viscosity
  real(WP), dimension(:,:,:,:), allocatable :: fcm_viscu,fcm_viscv,fcm_viscw
  
  ! correction for small (i.e. AFp{xyz} = 0) cells (Default is OFF)
  logical :: correct_momentum_cells
  ! correct small momentum cells based on volume fraction rather than AFp[xyz]
  real(WP) :: MVF_clip

  ! Distance to the IB
  integer :: iGib
  real(WP), dimension(:,:,:), allocatable :: Gib,oldGib

  ! IB normal
  real(WP), dimension(:,:,:), allocatable :: Nxib,Nyib,Nzib

  ! Barycenter distance
  real(WP), dimension(:,:,:), allocatable :: VHu,VHv,VHw

  ! Geometric information ==================================
  ! Immersed surface area
  real(WP), dimension(:,:,:), allocatable, target :: SAp,SAu,SAv,SAw
  ! Cell fluid volume fraction
  real(WP), dimension(:,:,:), allocatable, target :: VFp,VFu,VFv,VFw,VFuv,VFuw,VFvw
  ! Face fluid area fraction
  real(WP), dimension(:,:,:), allocatable :: AFpx,AFux,AFvx,AFwx,AFuvx,AFuwx
  real(WP), dimension(:,:,:), allocatable :: AFpy,AFuy,AFvy,AFwy,AFuvy,AFvwy
  real(WP), dimension(:,:,:), allocatable :: AFpz,AFuz,AFvz,AFwz,AFuwz,AFvwz
  ! Cell barycenter
  real(WP), dimension(:,:,:,:), allocatable :: Cbar
  ! ========================================================

  ! Cell barycenter
  real(WP), dimension(:,:,:,:), pointer :: Cbaru,Cbarv,Cbarw,Cbars
  ! Surface barycenter
  real(WP), dimension(:,:,:,:), pointer :: Sbar,Sbaru,Sbarv,Sbarw,Sbars
  real(WP), dimension(:,:,:)  , pointer :: SAs,VFs,AFsx,AFsy,AFsz
  
  ! Integer flag
  integer, dimension(:,:,:), allocatable :: ctag,ctag2
  integer :: fcm_p_maxtag
  integer :: fcm_p_count

  ! Clipping values
  real(WP) :: Gclip
  real(WP), parameter :: my_epsilon=1.0e-9_WP
  real(WP), parameter :: VFmix=0.5_WP

  ! Pressure and viscous forces
  real(WP), dimension(:,:,:), allocatable :: PFx,PFy,PFz
  real(WP), dimension(:,:,:), allocatable :: VFx,VFy,VFz
  ! Viscous forces that come from small cells
  real(WP), dimension(:,:,:), allocatable :: VFx_sc,VFy_sc,VFz_sc

  ! IB velocity in Momentum cells
  real(WP), dimension(:,:,:), pointer :: U_ib,V_ib,W_ib

  ! Test case parameters ===================
  ! Parameters for moving cylinder
  real(WP), parameter :: Dcyl=0.06_WP!1.0_WP
  real(WP), parameter :: ampx=1.0_WP
  real(WP), parameter :: perx=1.0_WP
  real(WP), parameter :: ampy=1.0_WP
  real(WP), parameter :: pery=1.0_WP
  ! Parameters for square
  real(WP), parameter :: xsq1=4.0_WP
  real(WP), parameter :: xsq2=6.0_WP
  real(WP), parameter :: ysq1=3.0_WP
  real(WP), parameter :: ysq2=7.0_WP
  ! ========================================

contains

  ! ==================== !
  ! Get the index of Gib !
  ! ==================== !
  subroutine get_index
    implicit none

    character(len=str_short) :: name
    integer :: iod

    iGib = -1
    loop: do iod=1,nod
       read(OD_name(iod),*) name
       if (trim(name).eq.'Gib'.or.trim(name).eq.'GIB') then
          iGib=iod
          exit loop
       end if
    end do loop

    return
  end subroutine get_index

  ! ======================== !
  ! Get IB velocity at i,j,k !
  ! ======================== !
  function ib_get_velocity_u(i,j,k,pos,dt_proj) result(v1)
    implicit none
    integer,  intent(in)  :: i,j,k
    real(WP), dimension(3), intent(in) :: pos
    real(WP), intent(in) :: dt_proj
    real(WP) :: v1,v2,v3,vtime

    ! assume check for ibmove was already done
    vtime = time + dt_proj

    select case (ibgeom)
    case('file-move')
       v1 = ibfile_velocity_u(pos,vtime)
    case ('rpt')
       call rpt_get_velocity(i,j,k,v1,v2,v3)
    case ('RPT')
       call rpt_get_velocity(i,j,k,v1,v2,v3)
    case ('moving cylinder')
       v1=-ampx/perx*sin(time/perx)
       v2=+ampy/pery*cos(time/pery)
       v3=0.0_WP
    case default
       v1=0.0_WP
    end select

    return
  end function ib_get_velocity_u

  ! ======================== !
  ! Get IB velocity at i,j,k !
  ! ======================== !
  function ib_get_velocity_v(i,j,k,pos,dt_proj) result(v2)
    implicit none
    integer,  intent(in)  :: i,j,k
    real(WP), dimension(3), intent(in) :: pos
    real(WP), intent(in) :: dt_proj
    real(WP) :: v1,v2,v3
    real(WP) :: vtime

    ! assume check for ibmove was already done
    vtime = time + dt_proj

    select case (ibgeom)
    case('file-move')
       v2 = ibfile_velocity_v(pos,vtime)
    case ('rpt')
       call rpt_get_velocity(i,j,k,v1,v2,v3)
    case ('RPT')
       call rpt_get_velocity(i,j,k,v1,v2,v3)
    case ('moving cylinder')
       v1=-ampx/perx*sin(time/perx)
       v2=+ampy/pery*cos(time/pery)
       v3=0.0_WP
    case default
       v2=0.0_WP
    end select

    return
  end function ib_get_velocity_v

  ! ======================== !
  ! Get IB velocity at i,j,k !
  ! ======================== !
  function ib_get_velocity_w(i,j,k,pos,dt_proj) result(v3)
    implicit none
    integer,  intent(in)  :: i,j,k
    real(WP), dimension(3), intent(in) :: pos
    real(WP), intent(in) :: dt_proj
    real(WP) :: v1,v2,v3
    real(WP) :: vtime
    ! assume check for ibmove was already done
    vtime = time + dt_proj

    select case (ibgeom)
    case('file-move')
       v3 = ibfile_velocity_w(pos,vtime)
    case ('rpt')
       call rpt_get_velocity(i,j,k,v1,v2,v3)
    case ('RPT')
       call rpt_get_velocity(i,j,k,v1,v2,v3)
    case ('moving cylinder')
       v1=-ampx/perx*sin(time/perx)
       v2=+ampy/pery*cos(time/pery)
       v3=0.0_WP
    case default
       v3=0.0_WP
    end select

    return
  end function ib_get_velocity_w

  ! ======================== !
  ! Get IB velocity at i,j,k !
  ! ======================== !
  subroutine ib_get_velocity(i,j,k,v1,v2,v3)
    implicit none

    integer,  intent(in)  :: i,j,k
    real(WP), intent(out) :: v1,v2,v3

    if (.not.ibmove) then
       ! Fixed IB
       v1=0.0_WP
       v2=0.0_WP
       v3=0.0_WP
    else
       ! Moving IB
       select case (ibgeom)
       case ('rpt')
          call rpt_get_velocity(i,j,k,v1,v2,v3)
       case ('RPT')
          call rpt_get_velocity(i,j,k,v1,v2,v3)
       case ('moving cylinder')
          v1=-ampx/perx*sin(time/perx)
          v2=+ampy/pery*cos(time/pery)
          v3=0.0_WP
       case default
          ! Should have been captured already
       end select
    end if

    return
  end subroutine ib_get_velocity

  ! ============================================= !
  ! Interpolation of Gib at pos close to cell ind !
  ! ============================================= !
  function get_Gib(pos,i0,j0,k0)
    implicit none

    real(WP) :: get_Gib
    real(WP), dimension(3), intent(in) :: pos
    integer, intent(in) :: i0,j0,k0
    integer :: i,j,k
    real(WP) :: wx1,wy1,wz1,wx2,wy2,wz2

    ! Find right i index
    i=i0
    do while (pos(1)-xm(i  ).lt.0.0_WP .and. i.gt.imino_)
       i=i-1
    end do
    do while (xm(i+1)-pos(1).le.0.0_WP .and. i.lt.imaxo_-1)
       i=i+1
    end do

    ! Find right j index
    j=j0
    do while (pos(2)-ym(j  ).lt.0.0_WP .and. j.gt.jmino_)
       j=j-1
    end do
    do while (ym(j+1)-pos(2).le.0.0_WP .and. j.lt.jmaxo_-1)
       j=j+1
    end do

    ! Find right k index
    k=k0
    do while (pos(3)-zm(k  ).lt.0.0_WP .and. k.gt.kmino_)
       k=k-1
    end do
    do while (zm(k+1)-pos(3).le.0.0_WP .and. k.lt.kmaxo_-1)
       k=k+1
    end do

    ! Prepare tri-linear interpolation coefficients
    wx1 = (pos(1)-xm(i))/(xm(i+1)-xm(i))
    wx2 = 1.0_WP - wx1
    wy1 = (pos(2)-ym(j))/(ym(j+1)-ym(j))
    wy2 = 1.0_WP - wy1
    wz1 = (pos(3)-zm(k))/(zm(k+1)-zm(k))
    wz2 = 1.0_WP - wz1

    ! Tri-linear interpolation of G
    get_Gib = wz1*(wy1*(wx1*Gib(i+1,j+1,k+1)   + &
         wx2*Gib(i  ,j+1,k+1))  + &
         wy2*(wx1*Gib(i+1,j  ,k+1)   + &
         wx2*Gib(i  ,j  ,k+1))) + &
         wz2*(wy1*(wx1*Gib(i+1,j+1,k  )   + &
         wx2*Gib(i  ,j+1,k  ))  + &
         wy2*(wx1*Gib(i+1,j  ,k  )   + &
         wx2*Gib(i  ,j  ,k  )))

    ! Clip G
    if (get_Gib.ge.0.0_WP.and.get_Gib.lt.Gclip) get_Gib=-my_epsilon

    return
  end function get_Gib

  ! ============================================= !
  ! Interpolation of Gib at pos close to cell ind !
  ! ============================================= !
  function get_oldGib(pos,i0,j0,k0)
    implicit none

    real(WP) :: get_oldGib
    real(WP), dimension(3), intent(in) :: pos
    integer, intent(in) :: i0,j0,k0
    integer :: i,j,k
    real(WP) :: wx1,wy1,wz1,wx2,wy2,wz2

    ! Find right i index
    i=i0
    do while (pos(1)-xm(i  ).lt.0.0_WP .and. i.gt.imino_)
       i=i-1
    end do
    do while (xm(i+1)-pos(1).le.0.0_WP .and. i.lt.imaxo_-1)
       i=i+1
    end do

    ! Find right j index
    j=j0
    do while (pos(2)-ym(j  ).lt.0.0_WP .and. j.gt.jmino_)
       j=j-1
    end do
    do while (ym(j+1)-pos(2).le.0.0_WP .and. j.lt.jmaxo_-1)
       j=j+1
    end do

    ! Find right k index
    k=k0
    do while (pos(3)-zm(k  ).lt.0.0_WP .and. k.gt.kmino_)
       k=k-1
    end do
    do while (zm(k+1)-pos(3).le.0.0_WP .and. k.lt.kmaxo_-1)
       k=k+1
    end do

    ! Prepare tri-linear interpolation coefficients
    wx1 = (pos(1)-xm(i))/(xm(i+1)-xm(i))
    wx2 = 1.0_WP - wx1
    wy1 = (pos(2)-ym(j))/(ym(j+1)-ym(j))
    wy2 = 1.0_WP - wy1
    wz1 = (pos(3)-zm(k))/(zm(k+1)-zm(k))
    wz2 = 1.0_WP - wz1

    ! Tri-linear interpolation of G
    get_oldGib = wz1*(wy1*(wx1*oldGib(i+1,j+1,k+1)   + &
         wx2*oldGib(i  ,j+1,k+1))  + &
         wy2*(wx1*oldGib(i+1,j  ,k+1)   + &
         wx2*oldGib(i  ,j  ,k+1))) + &
         wz2*(wy1*(wx1*oldGib(i+1,j+1,k  )   + &
         wx2*oldGib(i  ,j+1,k  ))  + &
         wy2*(wx1*oldGib(i+1,j  ,k  )   + &
         wx2*oldGib(i  ,j  ,k  )))

    ! Clip G
    if (get_oldGib.ge.0.0_WP.and.get_oldGib.lt.Gclip) get_oldGib=-my_epsilon

    return
  end function get_oldGib

  ! ======================================= !
  ! Interpolation of Gib to cell barycenter !
  ! ======================================= !
  function get_Gib_bar(pos,cell,val)
    implicit none

    real(WP) :: get_Gib_bar
    real(WP), dimension(3), intent(in) :: pos
    real(WP), dimension(3,8) :: cell
    real(WP), dimension(  8) :: val
    real(WP) :: wx1,wy1,wz1,wx2,wy2,wz2

    ! Prepare tri-linear interpolation coefficients
    wx2 = (pos(1)-cell(1,1))/(cell(1,2)-cell(1,1))
    wx1 = 1.0_WP - wx2
    wy2 = (pos(2)-cell(2,1))/(cell(2,3)-cell(2,1))
    wy1 = 1.0_WP - wy2
    wz2 = (pos(3)-cell(3,1))/(cell(3,5)-cell(3,1))
    wz1 = 1.0_WP - wz2

    ! Tri-linear interpolation of G
    get_Gib_bar = wz1*(wy1*(wx1*val(1)+wx2*val(2))+wy2*(wx1*val(3)+wx2*val(4)))+&
         wz2*(wy1*(wx1*val(5)+wx2*val(6))+wy2*(wx1*val(7)+wx2*val(8)))

    return
  end function get_Gib_bar

end module ib


! ================================== ! 
! Immersed boundaries initialization !
! ================================== !
subroutine ib_init
  use ib
  use parser
  use memory
  use velocity
  implicit none

  integer :: i,j,k

  ! Check whether IB are used
  call parser_read('Use IB',use_ib,.false.)
  ! parameters for small cell fixes
  call parser_read('Correct small momentum cells',correct_momentum_cells,.true.)
  call parser_read('Momentum volume clip',MVF_clip,-1.0_WP)
  call parser_read('FCM pressure',use_fcm_pressure,.true.)
  call parser_read('IB volume clip',Vclip,-1.0_WP)
  call parser_read('IB area clip',Aclip,-1.0_WP)
  call parser_read('FCM momentum',use_fcm_momentum,.false.)
  call parser_read('FCM clip',FCM_clip,10.0_WP)
  call parser_read('FCM factor',FCM_factor,2.0_WP)
  call parser_read('FCM volume clip',FCM_volume_clip,0.5_WP)
  
  call parser_read('IB artifical visc',use_ib_VA,.false.)

  ! If not used, return
  if (.not.use_ib) then
     ! ensure that FCM is turned off for implicit.f90
     use_fcm_pressure = .false.
     use_fcm_momentum = .false.
     ! Still allocate VF fields and set to one
     allocate(VFp(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); VFp=1.0_WP
     allocate(VFu(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); VFu=1.0_WP
     allocate(VFv(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); VFv=1.0_WP
     allocate(VFw(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); VFw=1.0_WP
     ! Return
     return
  end if
  
  ! Create & start the timer
  call timing_create('ib')
  call timing_start ('ib')

  ! Check order of accuracy
  if (vel_conv_order.gt.2.or.vel_visc_order.gt.2) &
       call die('IB require second order accuracy.')

  ! If volume or area clipping is not chosen - default to area clipping
  if(use_fcm_pressure.and.Vclip.le.0.0_WP.and.Aclip.le.0.0_WP) &
       Aclip = 0.2_WP

  ! Allocate distance array
  allocate(Gib (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(oldGib (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))

  ! Allocate normal vector
  allocate(Nxib(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(Nyib(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(Nzib(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))

  ! Allocate geometric data
  allocate(SAp (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(SAu (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(SAv (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(SAw (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(SAs (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))

  allocate(VFp (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(VFu (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(VFv (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(VFw (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(VFuv (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(VFuw (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(VFvw (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(VFs (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))

  allocate(AFpx(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AFux(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AFvx(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AFwx(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AFsx(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AFpy(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AFuy(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AFvy(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AFwy(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AFsy(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AFpz(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AFuz(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AFvz(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AFwz(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AFsz(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AFuvx(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AFuvy(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AFuwx(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AFuwz(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AFvwy(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(AFvwz(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))

  allocate(VHu (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(VHv (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(VHw (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))

  allocate(Cbar(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3))
  allocate(Cbaru(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3))
  allocate(Cbarv(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3))
  allocate(Cbarw(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3))
  allocate(Cbars(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3))
  allocate(Sbaru(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3))
  allocate(Sbarv(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3))
  allocate(Sbarw(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3))
  allocate(Sbars(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3))
  allocate(Sbar (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3))

  ! Allocate integer flag
  allocate(ctag(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(ctag2(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))

  ! Allocate FCM data
  if(use_fcm_pressure) then
     allocate(mix_frac_vol(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3,0:1))
     allocate(mix_frac_face(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3,3,0:1))
  end if
  if(use_fcm_momentum) then
     !allocate(fcm_visc(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
     allocate(fcm_viscu(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3))
     allocate(fcm_viscv(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3))
     allocate(fcm_viscw(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3))
  end if

  ! Allocate pressure and viscous forces
  allocate(PFx(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(PFy(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(PFz(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(VFx(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(VFy(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(VFz(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(VFx_sc(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(VFy_sc(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(VFz_sc(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))

  ! initialize to zero since ib_update_source will be called before ib_correct_momentum
  VFx_sc = 0.0_WP; VFy_sc = 0.0_WP; VFz_sc = 0.0_WP
  ! Set Gib clipping value
  Gclip=1.0e-5_WP*min_meshsize

  ! set pointers for IB velocity
  U_ib => tmp1; V_ib => tmp2; W_ib => tmp3

  ! Find the source of IB geometry
  call parser_read('IB geometry',ibgeom)
  select case (trim(ibgeom))
  case ('RPT')
     ibmove=.true.
     call rpt_init
     call rpt_get_distance(Gib)
  case ('rpt')
     ibmove=.true.
     call rpt_init
     call rpt_get_distance(Gib)
  case ('file')
     ibmove=.false.
     call get_index
     if (iGib.le.0) then
        call die('Need implicit definition of IB in optdata.')
     else
        Gib=OD(:,:,:,iGib)
     end if
  case('file-move')
     ibmove=.true.
     call get_index
     if (iGib.le.0) then
        call die('Need implicit definition of IB in optdata.')
     else
        Gib=OD(:,:,:,iGib)
     end if
  case ('sphere')
     ibmove=.false.
     do k=kmino_,kmaxo_
        do j=jmino_,jmaxo_
           do i=imino_,imaxo_
              Gib(i,j,k)=sqrt(xm(i)**2+ym(j)**2+zm(k)**2)-0.5_WP*Dcyl
           end do
        end do
     end do
  case ('cylinder')
     ibmove=.false.
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           Gib(i,j,:)=sqrt(xm(i)**2+ym(j)**2)-0.5_WP*Dcyl
        end do
     end do
  case ('square')
     ibmove=.false.
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           if (xm(i).gt.xsq2) then
              ! Right of square
              if (ym(j).gt.ysq2) then
                 ! Top right corner
                 Gib(i,j,:)=sqrt((ym(j)-ysq2)**2+(xm(i)-xsq2)**2)
              else if (ym(j).lt.ysq1) then
                 ! Bottom right corner
                 Gib(i,j,:)=sqrt((-ym(j)+ysq1)**2+(xm(i)-xsq2)**2)
              else
                 ! Center right
                 Gib(i,j,:)=xm(i)-xsq2
              end if
           else if (xm(i).lt.xsq1) then
              ! Left of square
              if (ym(j).gt.ysq2) then
                 ! Top left corner
                 Gib(i,j,:)=sqrt((ym(j)-ysq2)**2+(-xm(i)+xsq1)**2)
              else if (ym(j).lt.ysq1) then
                 ! Bottom left corner
                 Gib(i,j,:)=sqrt((-ym(j)+ysq1)**2+(-xm(i)+xsq1)**2)
              else
                 ! Center left
                 Gib(i,j,:)=-xm(i)+xsq1
              end if
           else
              ! Center of square
              if (ym(j).gt.ysq2) then
                 ! Top center corner
                 Gib(i,j,:)=ym(j)-ysq2
              else if (ym(j).lt.ysq1) then
                 ! Bottom center corner
                 Gib(i,j,:)=-ym(j)+ysq1
              else
                 ! Center
                 Gib(i,j,:)=max(-ym(j)+ysq1,ym(j)-ysq2,-xm(i)+xsq1,xm(i)-xsq2)
              end if
           end if
        end do
     end do
  case ('moving cylinder')
     ibmove=.true.
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           Gib(i,j,:)=sqrt((xm(i)-ampx*cos(time/perx))**2+(ym(j)-ampy*sin(time/pery))**2)-0.5_WP*Dcyl
        end do
     end do
  case default
     call die('Unknown source of IB geometry.')
  end select

  ! Communicate Gib
  call ib_update_border(Gib,'+','ym')
  oldGib = Gib

  ! Update operators here
  call ib_update_geometry
  call ib_update_operator

  ! If not static, let pressure solver know
  if (ibmove) pressure_cst_lap=1

  ! monitor file for ib merging
  if(use_fcm_pressure.or.use_fcm_momentum) then
     call monitor_create_file_step('ib_fcm',8)
     call monitor_set_header(1,'P_max','r')
     call monitor_set_header(2,'P_count','r')
     call monitor_set_header(3,'VISCu_max','r')
     call monitor_set_header(4,'VISCu_count','r')
     call monitor_set_header(5,'VISCv_max','r')
     call monitor_set_header(6,'VISCv_count','r')
     call monitor_set_header(7,'VISCw_max','r')
     call monitor_set_header(8,'VISCw_count','r')
  end if

  call monitor_create_file_step('ib_force',9)
  call monitor_set_header(1,'VFx','r')
  call monitor_set_header(2,'VFy','r')
  call monitor_set_header(3,'VFz','r')
  call monitor_set_header(4,'PFx','r')
  call monitor_set_header(5,'PFy','r')
  call monitor_set_header(6,'PFz','r')
  call monitor_set_header(7,'Fx' ,'r')
  call monitor_set_header(8,'Fy' ,'r')
  call monitor_set_header(9,'Fz' ,'r')

  ! Stop timer
  call timing_stop('ib')

  return
end subroutine ib_init


! ================================ !
! IB update of operator and source !
! ================================ !
subroutine ib_update
  use ib
  implicit none

  integer :: i,j,k

  ! If not used, return
  if (.not.use_ib) return
  
  ! Update geometry and operator for moving IB
  if (ibmove) then

     ! Calculate pressure force
     call ib_get_pforce

     ! Get new IB geometry
     select case (trim(ibgeom))
     case ('file-move')
        oldGib = Gib
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 Gib(i,j,k) = ibfile_velocity_Gib((/xm(i),ym(j),zm(k)/),time)
              end do
           end do
        end do
     case ('RPT')
        call rpt_step
        call rpt_get_distance(Gib)
     case ('rpt')
        call rpt_step
        call rpt_get_distance(Gib)
     case ('moving cylinder')
        do j=jmino_,jmaxo_
           do i=imino_,imaxo_
              Gib(i,j,:)=sqrt((xm(i)-ampx*cos(time/perx))**2+(ym(j)-ampy*sin(time/pery))**2)-0.5_WP*Dcyl
           end do
        end do
     case default
        ! Should have been caught already
     end select

     ! Time this part
     call timing_start('ib')

     ! Communicate Gib
     call ib_update_border(Gib,'+','ym')

     ! Update aperture data
     call ib_update_geometry

     ! Update operators
     call ib_update_operator
     
     ! Stop timer
     call timing_stop('ib')

     ! First clean up pressure field
     !P=0.0_WP
  else
     ! Calculate pressure force
     call ib_get_pforce

     ! Get new IB geometry
     if(trim(ibgeom) == 'rpt') then
        call rpt_step
        call rpt_get_distance(Gib)
     end if

  end if



  return
end subroutine ib_update

! ============================== !
! Set IB velocity momentum cells !
! ============================== !
subroutine ib_set_velocity
  use ib
  implicit none
  integer :: i,j,k

  ! initialize to zero
  U_ib=0.0_WP;V_ib=0.0_WP;W_ib=0.0_WP

  if(.not.ibmove) return

  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! if cell is cut by the IB, set velocity based on surface barycenter
           ! if cell is inside the IB set velocity as well - is this necessary??
           if(SAu(i,j,k).gt.epsilon(1.0_WP)) U_ib(i,j,k) = ib_get_velocity_u(i,j,k,Sbaru(i,j,k,:),0.0_WP)
           if(SAv(i,j,k).gt.epsilon(1.0_WP)) V_ib(i,j,k) = ib_get_velocity_v(i,j,k,Sbarv(i,j,k,:),0.0_WP)
           if(SAw(i,j,k).gt.epsilon(1.0_WP)) W_ib(i,j,k) = ib_get_velocity_w(i,j,k,Sbarw(i,j,k,:),0.0_WP)
        end do
     end do
  end do

  call ib_update_border(U_ib ,'+','ym')
  call ib_update_border(V_ib ,'+','y')
  call ib_update_border(W_ib ,'+','ym')

end subroutine ib_set_velocity


! ============================= !
! Calculation of pressure force !
! ============================= !
subroutine ib_get_pforce
  use ib
  use pressure
  implicit none
  
  integer :: i,j,k
  real(WP), dimension(:,:,:), pointer :: myP
  
  ! Zero force
  PFx=0.0_WP
  PFy=0.0_WP
  PFz=0.0_WP

  ! Point to proper array
  myP=>P
  if (use_multiphase) myP=>DP

  ! Loop over domain
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Test for the interface in momentum cells (testing for SAp results in asymmetries)
           if(SAu(i,j,k).gt.0.0_WP .and. VFu(i,j,k).gt.0.0_WP) &
                PFx(i,j,k)=VFu(i,j,k)*dxm(i-1)*dy(j)*dz*sum(myP(i-stc2:i+stc1,j,k)*(grad_Px(i,j,k,:)-divc_xx(i,j,k,:)))
           if(SAv(i,j,k).gt.0.0_WP .and. VFv(i,j,k).gt.0.0_WP) &
                PFy(i,j,k)=VFv(i,j,k)*dx(i)*dym(j-1)*dz*sum(myP(i,j-stc2:j+stc1,k)*(grad_Py(i,j,k,:)-divc_yy(i,j,k,:)))
           if(SAw(i,j,k).gt.0.0_WP .and. VFw(i,j,k).gt.0.0_WP) &
                PFz(i,j,k)=VFw(i,j,k)*dx(i)*dy(j)*dz   *sum(myP(i,j,k-stc2:k+stc1)*(grad_Pz(i,j,k,:)-divc_zz(i,j,k,:)))

        end do
     end do
  end do

  return
end subroutine ib_get_pforce


! ===================================== !
! IB update of all divergence operators !
! ===================================== !
subroutine ib_update_operator
  use ib
  use borders
  implicit none

  integer :: i,j,k,s1,s2,ii,jj,kk
  integer :: nflow
  real(WP) :: local_area

  ! Regenerate all divergences
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_

           ! Generic divergence
           do s1=-st1,st2
              div_u(i,j,k,s1)=div2d_u(i,j,s1)*AFsx(i+s1,j,k)/(VFs(i,j,k)+epsilon(1.0_WP))
              div_v(i,j,k,s1)=div2d_v(i,j,s1)*AFsy(i,j+s1,k)/(VFs(i,j,k)+epsilon(1.0_WP))
              div_w(i,j,k,s1)=div2d_w(i,j,s1)*AFsz(i,j,k+s1)/(VFs(i,j,k)+epsilon(1.0_WP))
           end do

           ! Convective divergences
           do s1=-stc1,stc2
              divc_u(i,j,k,s1)=divc2d_u(i,j,s1)*AFpx(i+s1,j,k)/(VFp(i,j,k)+epsilon(1.0_WP))
              divc_v(i,j,k,s1)=divc2d_v(i,j,s1)*AFpy(i,j+s1,k)/(VFp(i,j,k)+epsilon(1.0_WP))
              divc_w(i,j,k,s1)=divc2d_w(i,j,s1)*AFpz(i,j,k+s1)/(VFp(i,j,k)+epsilon(1.0_WP))

              divc_xy(i,j,k,s1)=divc2d_xy(i,j,s1)*AFuy(i,j+s1,k)/(VFu(i,j,k)+epsilon(1.0_WP))
              divc_xz(i,j,k,s1)=divc2d_xz(i,j,s1)*AFuz(i,j,k+s1)/(VFu(i,j,k)+epsilon(1.0_WP))

              divc_yx(i,j,k,s1)=divc2d_yx(i,j,s1)*AFvx(i+s1,j,k)/(VFv(i,j,k)+epsilon(1.0_WP))
              divc_yz(i,j,k,s1)=divc2d_yz(i,j,s1)*AFvz(i,j,k+s1)/(VFv(i,j,k)+epsilon(1.0_WP))

              divc_zx(i,j,k,s1)=divc2d_zx(i,j,s1)*AFwx(i+s1,j,k)/(VFw(i,j,k)+epsilon(1.0_WP))
              divc_zy(i,j,k,s1)=divc2d_zy(i,j,s1)*AFwy(i,j+s1,k)/(VFw(i,j,k)+epsilon(1.0_WP))
           end do

           do s1=-stc2,stc1
              divc_xx(i,j,k,s1)=divc2d_xx(i,j,s1)*AFux(i+s1,j,k)/(VFu(i,j,k)+epsilon(1.0_WP))
              divc_yy(i,j,k,s1)=divc2d_yy(i,j,s1)*AFvy(i,j+s1,k)/(VFv(i,j,k)+epsilon(1.0_WP))
              divc_zz(i,j,k,s1)=divc2d_zz(i,j,s1)*AFwz(i,j,k+s1)/(VFw(i,j,k)+epsilon(1.0_WP))
           end do

           ! Viscous divergences
           do s1=-stv1,stv2
              divv_xy(i,j,k,s1)=divv2d_xy(i,j,s1)*AFuy(i,j+s1,k)/(VFu(i,j,k)+epsilon(1.0_WP))
              divv_xz(i,j,k,s1)=divv2d_xz(i,j,s1)*AFuz(i,j,k+s1)/(VFu(i,j,k)+epsilon(1.0_WP))

              divv_yx(i,j,k,s1)=divv2d_yx(i,j,s1)*AFvx(i+s1,j,k)/(VFv(i,j,k)+epsilon(1.0_WP))
              divv_yz(i,j,k,s1)=divv2d_yz(i,j,s1)*AFvz(i,j,k+s1)/(VFv(i,j,k)+epsilon(1.0_WP))

              divv_zx(i,j,k,s1)=divv2d_zx(i,j,s1)*AFwx(i+s1,j,k)/(VFw(i,j,k)+epsilon(1.0_WP))
              divv_zy(i,j,k,s1)=divv2d_zy(i,j,s1)*AFwy(i,j+s1,k)/(VFw(i,j,k)+epsilon(1.0_WP))
           end do

           do s1=-stv2,stv1
              divv_xx(i,j,k,s1)=divv2d_xx(i,j,s1)*AFux(i+s1,j,k)/(VFu(i,j,k)+epsilon(1.0_WP))
              divv_yy(i,j,k,s1)=divv2d_yy(i,j,s1)*AFvy(i,j+s1,k)/(VFv(i,j,k)+epsilon(1.0_WP))
              divv_zz(i,j,k,s1)=divv2d_zz(i,j,s1)*AFwz(i,j,k+s1)/(VFw(i,j,k)+epsilon(1.0_WP))
           end do

        end do
     end do
  end do

  ! Pressure gradient on extended domain
  do k=kmin_-stc1,kmax_+stc2
     do j=jmin_-stc1,jmax_+stc2
        do i=imin_-stc1,imax_+stc2
           grad_Px(i,j,k,:) = grad2d_Px(i,j,:)*AFpx(i,j,k)/(VFu(i,j,k)+epsilon(1.0_WP))
           grad_Py(i,j,k,:) = grad2d_Py(i,j,:)*AFpy(i,j,k)/(VFv(i,j,k)+epsilon(1.0_WP))
           grad_Pz(i,j,k,:) = grad2d_Pz(i,j,:)*AFpz(i,j,k)/(VFw(i,j,k)+epsilon(1.0_WP))
        end do
     end do
  end do

  ! Viscous divergence on extended domain
  do k=kmin_-stv2,kmax_+stv1
     do j=jmin_-stv2,jmax_+stv1
        do i=imin_-stv2,imax_+stv1
           do s1=-stv1,stv2
              divv_u(i,j,k,s1)=divv2d_u(i,j,s1)*AFsx(i+s1,j,k)/(VFs(i,j,k)+epsilon(1.0_WP))
              divv_v(i,j,k,s1)=divv2d_v(i,j,s1)*AFsy(i,j+s1,k)/(VFs(i,j,k)+epsilon(1.0_WP))
              divv_w(i,j,k,s1)=divv2d_w(i,j,s1)*AFsz(i,j,k+s1)/(VFs(i,j,k)+epsilon(1.0_WP))
           end do
        end do
     end do
  end do

  ! Velocity gradient operators
  do kk=kmin_-stv1,kmax_+stv2
     do jj=jmin_-stv1,jmax_+stv2
        do ii=imin_-stv1,imax_+stv2
           i = ii-1; j = jj-1; k = kk-1
           do s1=-stv1,stv2
              grad_u_x(i,j,k,s1) = grad2d_u_x(i,j,s1)*AFsx(i+s1,j,k)/(VFs(i,j,k)+epsilon(1.0_WP))
              grad_v_y(i,j,k,s1) = grad2d_v_y(i,j,s1)*AFsy(i,j+s1,k)/(VFs(i,j,k)+epsilon(1.0_WP))
              grad_w_z(i,j,k,s1) = grad2d_w_z(i,j,s1)*AFsz(i,j,k+s1)/(VFs(i,j,k)+epsilon(1.0_WP))
           end do
           i = ii; j=jj; k=kk
           do s1=-stv2,stv1
              grad_u_y(i,j,k,s1) = grad2d_u_y(i,j,s1)*AFuvy(i,j+s1,k)/(VFuv(i,j,k)+epsilon(1.0_WP))
              grad_v_x(i,j,k,s1) = grad2d_v_x(i,j,s1)*AFuvx(i+s1,j,k)/(VFuv(i,j,k)+epsilon(1.0_WP))

              grad_u_z(i,j,k,s1) = grad2d_u_z(i,j,s1)*AFuwz(i,j,k+s1)/(VFuw(i,j,k)+epsilon(1.0_WP))
              grad_w_x(i,j,k,s1) = grad2d_w_x(i,j,s1)*AFuwx(i+s1,j,k)/(VFuw(i,j,k)+epsilon(1.0_WP))

              grad_v_z(i,j,k,s1) = grad2d_v_z(i,j,s1)*AFvwz(i,j,k+s1)/(VFvw(i,j,k)+epsilon(1.0_WP))
              grad_w_y(i,j,k,s1) = grad2d_w_y(i,j,s1)*AFvwy(i,j+s1,k)/(VFvw(i,j,k)+epsilon(1.0_WP))
           end do
        end do
     end do
  end do

  ! Regenerate Laplacian operator: lap = div( grad )
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           lap(i,j,k,:,:)=0.0_WP
           do s1=-stc1,stc2
              do s2=-stc2,stc1
                 lap(i,j,k,1,s1+s2)=lap(i,j,k,1,s1+s2)+divc_u(i,j,k,s1)*grad_Px(i+s1,j,k,s2)
                 lap(i,j,k,2,s1+s2)=lap(i,j,k,2,s1+s2)+divc_v(i,j,k,s1)*grad_Py(i,j+s1,k,s2)
                 lap(i,j,k,3,s1+s2)=lap(i,j,k,3,s1+s2)+divc_w(i,j,k,s1)*grad_Pz(i,j,k+s1,s2)
              end do
           end do
        end do
     end do
  end do
  do s1=-stcp,stcp
     call boundary_update_border(lap(:,:,:,1,s1),'+','ym')
     call boundary_update_border(lap(:,:,:,2,s1),'+','ym')
     call boundary_update_border(lap(:,:,:,3,s1),'+','ym')
  end do

  ! Regenerate cell volume
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           vol(i,j,k)=vol2d(i,j)*VFp(i,j,k)
           if (i.gt.imino_) volu(i,j,k)=volu2d(i,j)*VFu(i,j,k)
           if (j.gt.jmino_) volv(i,j,k)=volv2d(i,j)*VFv(i,j,k)
           if (k.gt.kmino_) volw(i,j,k)=volw2d(i,j)*VFw(i,j,k)
        end do
     end do
  end do
  call parallel_sum(sum(vol (imin_:imax_,jmin_:jmax_,kmin_:kmax_)),vol_total )
  call parallel_sum(sum(volu(imin_:imax_,jmin_:jmax_,kmin_:kmax_)),volu_total)
  call parallel_sum(sum(volv(imin_:imax_,jmin_:jmax_,kmin_:kmax_)),volv_total)
  call parallel_sum(sum(volw(imin_:imax_,jmin_:jmax_,kmin_:kmax_)),volw_total)

! Regenerate cell area (cartesian for now)
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           dA(i,j,k) = dA2d(j)*AFpx(i,j,k)
           dAy(i,j,k)= dA2dy(i)*AFpy(i,j,k)
        end do
     end do
  end do

  ! Compute cross section area of the inlets in x
  do nflow=1,ninlet
     local_area=0.0_WP
     if (iproc.eq.1) then
        do k=kmin_,kmax_
           do j=max(jmin_,inlet(nflow)%jmin),min(jmax_,inlet(nflow)%jmax)
              local_area=local_area+dA(imin,j,k)
           end do
        end do
     end if
     call parallel_sum(local_area,inlet(nflow)%A)
  end do

  ! Compute cross section area of the outlets in x
  outlet_area=0.0_WP
  do nflow=1,noutlet
     local_area=0.0_WP
     if (iproc.eq.npx) then
        do k=kmin_,kmax_
           do j=max(jmin_,outlet(nflow)%jmin),min(jmax_,outlet(nflow)%jmax)
              local_area=local_area+dA(imax+1,j,k)
           end do
        end do
     end if
     call parallel_sum(local_area,outlet(nflow)%A)
     outlet_area=outlet_area+outlet(nflow)%A
  end do

  ! Compute cross section area of the inlets in y
  do nflow=1,ninlety
     local_area=0.0_WP
     if (jproc.eq.1) then
        do k=kmin_,kmax_
           do i=max(imin_,inlety(nflow)%imin),min(imax_,inlety(nflow)%imax)
              local_area=local_area+dAy(i,jmin,k)
           end do
        end do
     end if
     call parallel_sum(local_area,inlety(nflow)%A)
  end do


  return
end subroutine ib_update_operator


! ==================================== !
! IB source term for no-slip condition !
! ==================================== !
subroutine ib_update_source
  use ib
  use velocity
  use masks
  use memory
  use interpolate
  implicit none

  integer :: i,j,k
  real(WP) :: norm,my_vol,my_visc
  real(WP) :: my_rho,velnorm
  real(WP), dimension(1:3) :: mynorm,vel,velib,locnorm
  real(WP) :: buf1,buf2,buf3,vol1,vol2,vol3
  real(WP) :: forceU,forceV,forceW,int_srcP,my_int,my_srcP
  ! modified divergence operator for RHS of pressure 
  real(WP), dimension(-stc1:stc2) :: mdivu_ib,mdivv_ib,mdivw_ib

  ! If not used, return
  if (.not.use_ib) return

  ! Start the timer
  call timing_start('ib')

  ! set the IB velocity in momentum cells
  call ib_set_velocity
  
  ! compute source terms due to modified velocity gradient
  call ib_viscous_srcU
  call ib_viscous_srcV
  call ib_viscous_srcW

  ! Create source term for pressure
  if (ibmove) then

     ! Zero integral of pressure source
     my_int=0.0_WP

     ! Loop over inner domain
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_

              ! Pressure cell
              if (SAp(i,j,k).gt.0.0_WP) then
                 mdivu_ib = divc2d_u(i,j,:)*AFux(i,j,k)/(VFp(i,j,k)+epsilon(1.0_WP))-divc_u(i,j,k,:)
                 mdivv_ib = divc2d_v(i,j,:)*AFvy(i,j,k)/(VFp(i,j,k)+epsilon(1.0_WP))-divc_v(i,j,k,:)
                 mdivw_ib = divc2d_w(i,j,:)*AFwz(i,j,k)/(VFp(i,j,k)+epsilon(1.0_WP))-divc_w(i,j,k,:)

                 ! Get source term                 
                 my_srcP =  dt*RHO(i,j,k) * &
                      ( sum(mdivu_ib(-stc1:stc2)*U_ib(i-stc1:i+stc2,j,k)) &
                      + sum(mdivv_ib(-stc1:stc2)*V_ib(i,j-stc1:j+stc2,k)) &
                      + sum(mdivw_ib(-stc1:stc2)*W_ib(i,j,k-stc1:k+stc2)) )
                 
                 srcPmid(i,j,k) = srcPmid(i,j,k) + my_srcP
                 
                 ! Integrate source term to ensure zero integral
                 my_int=my_int+my_srcP*vol(i,j,k)

              end if

           end do
        end do
     end do

     ! Remove integral of pressure source
     call parallel_sum(my_int,int_srcP)
     int_srcP=int_srcP/vol_total
     srcPmid=srcPmid-int_srcP

  end if

  ! Zero source terms
  VFx=0.0_WP
  VFy=0.0_WP
  VFz=0.0_WP

  ! Create cell-centered velocities
  call interpolate_velocities

  ! Create source terms for momentum
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_

           ! U momentum cell
           if (SAu(i,j,k).gt.0.0_WP.and.VFu(i,j,k).gt.0.0_WP) then
              my_visc=sum(interp_sc_x(i,j,:)*VISCmol(i-st2:i+st1,j,k))
              VFx(i,j,k)=my_visc*SAu(i,j,k)*(U(i,j,k)-U_ib(i,j,k))/VHu(i,j,k)
           end if

           ! V momentum cell
           if (SAv(i,j,k).gt.0.0_WP.and.VFv(i,j,k).gt.0.0_WP) then
              my_visc=sum(interp_sc_y(i,j,:)*VISCmol(i,j-st2:j+st1,k))
              VFy(i,j,k)=my_visc*SAv(i,j,k)*(V(i,j,k)-V_ib(i,j,k))/VHv(i,j,k)
           end if

           ! W momentum cell
           if (SAw(i,j,k).gt.0.0_WP.and.VFw(i,j,k).gt.0.0_WP) then
              my_visc=sum(interp_sc_z(i,j,:)*VISCmol(i,j,k-st2:k+st1))
              VFz(i,j,k)=my_visc*SAw(i,j,k)*(W(i,j,k)-W_ib(i,j,k))/VHw(i,j,k)
           end if

        end do
     end do
  end do

  ! Add to source
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (volu(i,j,k).gt.0.0_WP) srcUmid(i,j,k)=srcUmid(i,j,k)-dt_uvw*VFx(i,j,k)/volu(i,j,k)
           if (volv(i,j,k).gt.0.0_WP) srcVmid(i,j,k)=srcVmid(i,j,k)-dt_uvw*VFy(i,j,k)/volv(i,j,k)
           if (volw(i,j,k).gt.0.0_WP) srcWmid(i,j,k)=srcWmid(i,j,k)-dt_uvw*VFz(i,j,k)/volw(i,j,k)
        end do
     end do
  end do

  ! Add force from small cells set to zero - do it here to avoid adding to source term
  VFx = VFx + VFx_sc
  VFy = VFy + VFy_sc
  VFz = VFz + VFz_sc

  ! Momentum FCM
  call ib_fcm_u
  call ib_fcm_v
  call ib_fcm_w

  ! Stop the timer
  call timing_stop('ib')

  return
end subroutine ib_update_source

subroutine ib_viscous_srcU
  use ib
  use velocity
  use memory
  use metric_velocity_visc
  use metric_generic
  implicit none

  integer :: i,j,k,ii,jj,kk
  real(WP),dimension(-stv1:+stv2) :: mgrd_u_x
  real(WP),dimension(-stv2:+stv1) :: mgrd_u_y,mgrd_u_z
  real(WP),dimension(-stv2:+stv1) :: mgrd_v_x,mgrd_w_x

  if(.not.ibmove) return
  
  ! Viscous part
  do kk=kmin_-stv1,kmax_+stv2
     do jj=jmin_-stv1,jmax_+stv2
        do ii=imin_-stv1,imax_+stv2

           i = ii-1; j = jj-1; k = kk-1;

           ! construct modified, modified gradient of u_x
           mgrd_u_x =  & 
                + grad2d_u_x(i,j,:)*AFux(i,j,k)/(VFs(i,j,k)+epsilon(1.0_WP)) &
                - grad_u_x(i,j,k,:)

           FX(i,j,k) = &
                + 2.0_WP*VISC(i,j,k)* &
                ( sum(mgrd_u_x*U_ib(i-stv1:i+stv2,j,k)))
           i = ii; j = jj; k = kk;
           
           ! construct modified, modified gradient of u_y and v_x

           mgrd_u_y = &
                + grad2d_u_y(i,j,:)*AFuy(i,j,k)/(VFuv(i,j,k)+epsilon(1.0_WP)) &
                - grad_u_y(i,j,k,:)
           mgrd_v_x = &
                + grad2d_v_x(i,j,:)*AFvx(i,j,k)/(VFuv(i,j,k)+epsilon(1.0_WP)) &
                - grad_v_x(i,j,k,:)

           FY(i,j,k) = &
                + sum(interp_sc_xy(i,j,:,:)*VISC(i-st2:i+st1,j-st2:j+st1,k)) * &
                ( sum(mgrd_u_y*U_ib(i,j-stv2:j+stv1,k)) & 
                + sum(mgrd_v_x*V_ib(i-stv2:i+stv1,j,k)) )

           ! construct modified, modified gradient of u_z and w_x

           mgrd_u_z = &
                + grad2d_u_z(i,j,:)*AFuz(i,j,k)/(VFuw(i,j,k)+epsilon(1.0_WP)) &
                - grad_u_z(i,j,k,:)
           mgrd_w_x = &
                + grad2d_w_x(i,j,:)*AFwx(i,j,k)/(VFuw(i,j,k)+epsilon(1.0_WP)) &
                - grad_w_x(i,j,k,:)

           FZ(i,j,k) = &
                + sum(interp_sc_xz(i,j,:,:)*VISC(i-st2:i+st1,j,k-st2:k+st1)) * &
                ( sum(mgrd_u_z*U_ib(i,j,k-stv2:k+stv1)) &
                + sum(mgrd_w_x*W_ib(i-stv2:i+stv1,j,k)) )
        end do
     end do
  end do

  ! compute source term
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           srcUmid(i,j,k) = srcUmid(i,j,k)+dt_uvw*( &
                + sum(divv_xx(i,j,k,:)*FX(i-stv2:i+stv1,j,k)) &
                + sum(divv_xy(i,j,k,:)*FY(i,j-stv1:j+stv2,k)) &
                + sum(divv_xz(i,j,k,:)*FZ(i,j,k-stv1:k+stv2)) )
        end do
     end do
  end do

end subroutine ib_viscous_srcU

subroutine ib_viscous_srcV
  use ib
  use velocity
  use memory
  use metric_velocity_visc
  use metric_generic
  implicit none

  integer :: i,j,k,ii,jj,kk
  real(WP),dimension(-stv1:+stv2) :: mgrd_v_y
  real(WP),dimension(-stv2:+stv1) :: mgrd_v_x,mgrd_v_z
  real(WP),dimension(-stv2:+stv1) :: mgrd_u_y,mgrd_w_y

  if(.not.ibmove) return

  ! Viscous part
  do kk=kmin_-stv1,kmax_+stv2
     do jj=jmin_-stv1,jmax_+stv2
        do ii=imin_-stv1,imax_+stv2

           i = ii-1; j = jj-1; k = kk-1;

           ! construct modified, modified gradient of v_y
           mgrd_v_y =  & 
                + grad2d_v_y(i,j,:)*AFvy(i,j,k)/(VFs(i,j,k)+epsilon(1.0_WP)) &
                - grad_v_y(i,j,k,:)

           FY(i,j,k) = &
                + 2.0_WP*VISC(i,j,k)* &
                ( sum(mgrd_v_y*V_ib(i,j-stv1:j+stv2,k)))

           i = ii; j = jj; k = kk;
           
           ! construct modified, modified gradient of u_y and v_x

           mgrd_u_y = &
                + grad2d_u_y(i,j,:)*AFuy(i,j,k)/(VFuv(i,j,k)+epsilon(1.0_WP)) &
                - grad_u_y(i,j,k,:)
           mgrd_v_x = &
                + grad2d_v_x(i,j,:)*AFvx(i,j,k)/(VFuv(i,j,k)+epsilon(1.0_WP)) &
                - grad_v_x(i,j,k,:)

           FX(i,j,k) = &
                + sum(interp_sc_xy(i,j,:,:)*VISC(i-st2:i+st1,j-st2:j+st1,k)) * &
                ( sum(mgrd_u_y*U_ib(i,j-stv2:j+stv1,k)) & 
                + sum(mgrd_v_x*V_ib(i-stv2:i+stv1,j,k)) )

           ! construct modified, modified gradient of v_z and w_y

           mgrd_v_z = &
                + grad2d_v_z(i,j,:)*AFvz(i,j,k)/(VFvw(i,j,k)+epsilon(1.0_WP)) &
                - grad_v_z(i,j,k,:)
           mgrd_w_y = &
                + grad2d_w_y(i,j,:)*AFwy(i,j,k)/(VFvw(i,j,k)+epsilon(1.0_WP)) &
                - grad_w_y(i,j,k,:)

           FZ(i,j,k) = &
                + sum(interp_sc_yz(i,j,:,:)*VISC(i,j-st2:j+st1,k-st2:k+st1)) * &
                ( sum(mgrd_v_z*V_ib(i,j,k-stv2:k+stv1)) &
                + sum(mgrd_w_y*W_ib(i,j-stv2:j+stv1,k)) )
        end do
     end do
  end do

  ! compute source term
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           srcVmid(i,j,k) = srcVmid(i,j,k)+dt_uvw*( &
                + sum(divv_yx(i,j,k,:)*FX(i-stv1:i+stv2,j,k)) &
                + sum(divv_yy(i,j,k,:)*FY(i,j-stv2:j+stv1,k)) &
                + sum(divv_yz(i,j,k,:)*FZ(i,j,k-stv1:k+stv2)) )
        end do
     end do
  end do

end subroutine ib_viscous_srcV

subroutine ib_viscous_srcW
  use ib
  use velocity
  use memory
  use metric_velocity_visc
  use metric_generic
  implicit none

  integer :: i,j,k,ii,jj,kk
  real(WP),dimension(-stv1:+stv2) :: mgrd_w_z
  real(WP),dimension(-stv2:+stv1) :: mgrd_w_x,mgrd_w_y
  real(WP),dimension(-stv2:+stv1) :: mgrd_u_z,mgrd_v_z

  if(.not.ibmove) return

  ! Viscous part
  do kk=kmin_-stv1,kmax_+stv2
     do jj=jmin_-stv1,jmax_+stv2
        do ii=imin_-stv1,imax_+stv2

           i = ii-1; j = jj-1; k = kk-1;

           ! construct modified, modified gradient of v_y
           mgrd_w_z =  & 
                + grad2d_w_z(i,j,:)*AFwz(i,j,k)/(VFs(i,j,k)+epsilon(1.0_WP)) &
                - grad_w_z(i,j,k,:)

           FZ(i,j,k) = &
                + 2.0_WP*VISC(i,j,k)* &
                ( sum(mgrd_w_z*W_ib(i,j,k-stv1:k+stv2)))

           i = ii; j = jj; k = kk;
           
           ! construct modified, modified gradient of u_z and w_x

           mgrd_u_z = &
                + grad2d_u_z(i,j,:)*AFuz(i,j,k)/(VFuw(i,j,k)+epsilon(1.0_WP)) &
                - grad_u_z(i,j,k,:)
           mgrd_w_x = &
                + grad2d_w_x(i,j,:)*AFwx(i,j,k)/(VFuw(i,j,k)+epsilon(1.0_WP)) &
                - grad_w_x(i,j,k,:)

           FX(i,j,k) = &
                + sum(interp_sc_xz(i,j,:,:)*VISC(i-st2:i+st1,j,k-st2:k+st1)) * &
                ( sum(mgrd_u_z*U_ib(i,j,k-stv2:k+stv1)) & 
                + sum(mgrd_w_x*W_ib(i-stv2:i+stv1,j,k)) )

           ! construct modified, modified gradient of v_z and w_y

           mgrd_v_z = &
                + grad2d_v_z(i,j,:)*AFvz(i,j,k)/(VFvw(i,j,k)+epsilon(1.0_WP)) &
                - grad_v_z(i,j,k,:)
           mgrd_w_y = &
                + grad2d_w_y(i,j,:)*AFwy(i,j,k)/(VFvw(i,j,k)+epsilon(1.0_WP)) &
                - grad_w_y(i,j,k,:)

           FY(i,j,k) = &
                + sum(interp_sc_yz(i,j,:,:)*VISC(i,j-st2:j+st1,k-st2:k+st1)) * &
                ( sum(mgrd_v_z*V_ib(i,j,k-stv2:k+stv1)) &
                + sum(mgrd_w_y*W_ib(i,j-stv2:j+stv1,k)) )
        end do
     end do
  end do

  ! compute source term
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           
           srcWmid(i,j,k) = srcWmid(i,j,k)+dt_uvw*( &
                + sum(divv_zx(i,j,k,:)*FX(i-stv1:i+stv2,j,k)) &
                + sum(divv_zy(i,j,k,:)*FY(i,j-stv1:j+stv2,k)) &
                + sum(divv_zz(i,j,k,:)*FZ(i,j,k-stv2:k+stv1)) )
        end do
     end do
  end do

end subroutine ib_viscous_srcW

! compute flux at faces and add to srcUmid
subroutine ib_fcm_u
  use ib
  use velocity
  use masks
  use memory
  use metric_velocity_visc
  use metric_generic
  implicit none

  ! FX(i,j,k) -> xm(i),ym(j),zm(k)
  ! FY(i,j,k) -> x(i),y(j),zm(k)
  ! FZ(i,j,k) -> x(i),ym(j),z(k)
  real(WP) :: rhs,convective,diffusive
  integer  :: i,j,k,ss,ii,jj,kk

  if(.not.use_fcm_momentum) return

  fcm_viscu = 0.0_WP

  ! compute fcm_viscu based on convective and diffusive fluxes
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if(VFu(i,j,k) .lt. FCM_volume_clip .and. SAu(i,j,k).gt.0.0_WP) then

              ! Use finite-difference gradients
              ! X
              do ss= i-1,i
                 if(AFux(ss,j,k).gt.0.0_WP) then
                    convective = &
                         +sum(interp_Ju_xm(ss,j,:)*rhoU(ss-stc1:ss+stc2,j,k)) &
                         *sum(interp_Ju_xm(ss,j,:)*U(ss-stc1:ss+stc2,j,k))
                    diffusive = epsilon(1.0_WP) + &
                         VISC(ss,j,k)*sum(grad2d_u_x(ss,j,:)*U(ss-stv1:ss+stv2,j,k))
                    fcm_viscu(ss,j,k,1) = VISC(i,j,k)*&
                         min(max(FCM_factor*abs(convective/diffusive)-1.0_WP,0.0_WP),FCM_clip)
                 end if
              end do
              ! Y
              do ss=j,j+1
                 if(AFuy(i,ss,k).gt.0.0_WP) then
                    convective = &
                         +sum(interp_Jv_x(i,ss,:)*rhoV(i-stc2:i+stc1,ss,k)) &
                         *sum(interp_Ju_y(i,ss,:)*U(i,ss-stc2:ss+stc1,k))
                    diffusive = epsilon(1.0_WP) + &
                         sum(interp_sc_xy(i,ss,:,:)*VISC(i-st2:i+st1,ss-st2:ss+st1,k)) * &
                         sum(grad2d_u_y(i,ss,:)*U(i,ss-stv2:ss+stv1,k))
                    fcm_viscu(i,ss,k,2) = sum(interp_sc_xy(i,ss,:,:)*VISC(i-st2:i+st1,ss-st2:ss+st1,k))*&
                         min(max(FCM_factor*abs(convective/diffusive)-1.0_WP,0.0_WP),FCM_clip)
                 end if
              end do
              ! Z
              do ss=k,k+1
                 if(AFuz(i,j,ss).gt.0.0_WP) then
                    convective = &
                         +sum(interp_Jw_x(i,j,:)*rhoW(i-stc2:i+stc1,j,ss)) &
                         *sum(interp_Ju_z(i,j,:)*U(i,j,ss-stc2:ss+stc1))
                    diffusive = epsilon(1.0_WP) + &
                         sum(interp_sc_xz(i,j,:,:)*VISC(i-st2:i+st1,j,ss-st2:ss+st1)) * &
                         sum(grad2d_u_z(i,j,:)*U(i,j,ss-stv2:ss+stv1))
                    fcm_viscu(i,j,ss,3) = sum(interp_sc_xz(i,j,:,:)*VISC(i-st2:i+st1,j,ss-st2:ss+st1))*&
                         min(max(FCM_factor*abs(convective/diffusive)-1.0_WP,0.0_WP),FCM_clip)
                 end if
              end do
           end if

        end do
     end do
  end do

  do i=1,3
     call ib_update_border(fcm_viscu(:,:,:,i),'+','ym')
  end do
  ! Face fluxes
  do kk=kmin_-stv1,kmax_+stv2
     do jj=jmin_-stv1,jmax_+stv2
        do ii=imin_-stv1,imax_+stv2

           i = ii-1; j = jj-1; k = kk-1;

           FX(i,j,k) = &
                + fcm_viscu(i,j,k,1)*&
                sum(grad_u_x(i,j,k,:)*U(i-stv1:i+stv2,j,k))

           i = ii; j = jj; k = kk;

           FY(i,j,k) = &
                + fcm_viscu(i,j,k,2) &
                * sum(grad_u_y(i,j,k,:)*U(i,j-stv2:j+stv1,k))

           FZ(i,j,k) = &
                + fcm_viscu(i,j,k,3) &
                * sum(grad_u_z(i,j,k,:)*U(i,j,k-stv2:k+stv1))
        end do
     end do
  end do

  ! add to source term
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if(mask_u(i,j).eq.0 .and. VFu(i,j,k).gt.0.0_WP) then
              rhs = &
                   +sum(divv_xx(i,j,k,:)*FX(i-stv2:i+stv1,j,k)) &
                   +sum(divv_xy(i,j,k,:)*FY(i,j-stv1:j+stv2,k)) &
                   +sum(divv_xz(i,j,k,:)*FZ(i,j,k-stv1:k+stv2)) 
              srcUmid(i,j,k) = srcUmid(i,j,k) + dt_uvw*rhs
           end if
        end do
     end do
  end do

end subroutine ib_fcm_u

! compute flux at faces and add to srcVmid
subroutine ib_fcm_v
  use ib
  use velocity
  use masks
  use memory
  use metric_velocity_visc
  use metric_generic
  implicit none

  ! FX(i,j,k) -> x(i),y(j),zm(k)
  ! FY(i,j,k) -> xm(i),ym(j),zm(k)
  ! FZ(i,j,k) -> xm(i),y(j),z(k)
  real(WP) :: rhs,convective,diffusive
  integer  :: i,j,k,ss,ii,jj,kk

  if(.not.use_fcm_momentum) return

  fcm_viscv = 0.0_WP

  ! compute fcm_viscv based on convective and diffusive fluxes
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if(VFv(i,j,k) .lt. FCM_volume_clip .and. SAv(i,j,k).gt.0.0_WP) then

              ! Use finite-difference gradients
              ! Y
              do ss= j-1,j
                 if(AFvy(i,ss,k) .gt.0.0_WP) then
                    convective = &
                         +sum(interp_Jv_ym(i,ss,:)*rhoV(i,ss-stc1:ss+stc2,k)) &
                         *sum(interp_Jv_ym(i,ss,:)*V(i,ss-stc1:ss+stc2,k))
                    diffusive = epsilon(1.0_WP) + &
                         VISC(i,ss,k)*sum(grad2d_v_y(i,ss,:)*V(i,ss-stv1:ss+stv2,k))
                    fcm_viscv(i,ss,k,2) = VISC(i,j,k)*&
                         min(max(FCM_factor*abs(convective/diffusive)-1.0_WP,0.0_WP),FCM_clip)
                 end if
              end do
              ! X
              do ss=i,i+1
                 if(AFvx(ss,j,k).gt.0.0_WP) then
                    convective = &
                         +sum(interp_Ju_y(ss,j,:)*rhoU(ss,j-stc2:j+stc1,k)) &
                         *sum(interp_Jv_x(ss,j,:)*V(ss-stc2:ss+stc1,j,k))
                    diffusive = epsilon(1.0_WP) + &
                         sum(interp_sc_xy(ss,j,:,:)*VISC(ss-st2:ss+st1,j-st2:j+st1,k)) * &
                         sum(grad2d_v_x(ss,j,:)*V(ss-stv2:ss+stv1,j,k))
                    fcm_viscv(ss,j,k,1) = sum(interp_sc_xy(ss,j,:,:)*VISC(ss-st2:ss+st1,j-st2:j+st1,k))*&
                         min(max(FCM_factor*abs(convective/diffusive)-1.0_WP,0.0_WP),FCM_clip)
                 end if
              end do
              ! Z
              do ss=k,k+1
                 if(AFvz(i,j,ss).gt.0.0_WP) then
                    convective = &
                         +sum(interp_Jw_y(i,j,:)*rhoW(i,j-stc2:j+stc1,ss)) &
                         *sum(interp_Jv_z(i,j,:)*V(i,j,ss-stc2:ss+stc1))
                    diffusive = epsilon(1.0_WP) + &
                         sum(interp_sc_yz(i,j,:,:)*VISC(i,j-st2:j+st1,ss-st2:ss+st1)) * &
                         sum(grad2d_v_z(i,j,:)*V(i,j,ss-stv2:ss+stv1))
                    fcm_viscv(i,j,ss,3) =  sum(interp_sc_yz(i,j,:,:)*VISC(i,j-st2:j+st1,ss-st2:ss+st1))*&
                         min(max(FCM_factor*abs(convective/diffusive)-1.0_WP,0.0_WP),FCM_clip)
                 end if
              end do
           end if

        end do
     end do
  end do

  do i=1,3
     call ib_update_border(fcm_viscv(:,:,:,i),'+','ym')
  end do

  ! Face fluxes
  do kk=kmin_-stv1,kmax_+stv2
     do jj=jmin_-stv1,jmax_+stv2
        do ii=imin_-stv1,imax_+stv2

           i = ii-1; j = jj-1; k = kk-1;

           FY(i,j,k) = &
                + fcm_viscv(i,j,k,2) &
                * sum(grad_v_y(i,j,k,:)*V(i,j-stv1:j+stv2,k))

           i = ii; j = jj; k = kk;

           FX(i,j,k) = &
                + fcm_viscv(i,j,k,1) &
                * sum(grad_v_x(i,j,k,:)*V(i-stv2:i+stv1,j,k))

           FZ(i,j,k) = &
                + fcm_viscv(i,j,k,3)  &
                * sum(grad_v_z(i,j,k,:)*V(i,j,k-stv2:k+stv1)) 
        end do
     end do
  end do

  ! add to source term
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if(mask_v(i,j).eq.0 .and. VFv(i,j,k).gt.0.0_WP) then
              rhs = &
                   +sum(divv_yx(i,j,k,:)*FX(i-stv1:i+stv2,j,k)) &
                   +sum(divv_yy(i,j,k,:)*FY(i,j-stv2:j+stv1,k)) &
                   +sum(divv_yz(i,j,k,:)*FZ(i,j,k-stv1:k+stv2)) 
              srcVmid(i,j,k) = srcVmid(i,j,k) + dt_uvw*rhs
           end if
        end do
     end do
  end do

end subroutine ib_fcm_v

! compute flux at faces and add to srcWmid
subroutine ib_fcm_w
  use ib
  use velocity
  use masks
  use memory
  use metric_velocity_visc
  use metric_generic
  implicit none

  ! FX(i,j,k) -> x(i),ym(j),z(k)
  ! FY(i,j,k) -> xm(i),y(j),z(k)
  ! FZ(i,j,k) -> xm(i),ym(j),zm(k)
  real(WP) :: rhs,convective,diffusive
  integer  :: i,j,k,ss,ii,jj,kk

  if(.not.use_fcm_momentum) return

  fcm_viscw = 0.0_WP

  ! compute fcm_viscu based on convective and diffusive fluxes
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if(VFw(i,j,k) .lt. FCM_volume_clip .and. SAw(i,j,k).gt.0.0_WP) then
              ! Use finite-difference gradients
              ! Z
              do ss= k-1,k
                 if(AFwz(i,j,ss) .gt.0.0_WP) then
                    convective =  &
                         +sum(interp_Jw_zm(i,j,:)*rhoW(i,j,ss-stc1:ss+stc2)) &
                         *sum(interp_Jw_zm(i,j,:)*W(i,j,ss-stc1:ss+stc2))
                    diffusive = epsilon(1.0_WP) + &
                         VISC(i,j,ss)*sum(grad2d_w_z(i,j,:)*W(i,j,ss-stv1:ss+stv2))
                    fcm_viscw(i,j,ss,3) = VISC(i,j,ss)*&
                         min(max(FCM_factor*abs(convective/diffusive)-1.0_WP,0.0_WP),FCM_clip)
                 end if
              end do
              ! Y
              do ss=j,j+1
                 if(AFwy(i,ss,k).gt.0.0_WP) then
                    convective =  &
                         +sum(interp_Jv_z(i,ss,:)*rhoV(i,ss,k-stc2:k+stc1)) &
                         *sum(interp_Jw_y(i,ss,:)*W(i,ss-stc2:ss+stc1,k))
                    diffusive = epsilon(1.0_WP) + &
                         sum(interp_sc_yz(i,ss,:,:)*VISC(i,ss-st2:ss+st1,k-st2:k+st1)) * &
                         sum(grad2d_w_y(i,ss,:)*W(i,ss-stv2:ss+stv1,k))
                    fcm_viscw(i,ss,k,2) = sum(interp_sc_yz(i,ss,:,:)*VISC(i,ss-st2:ss+st1,k-st2:k+st1)) * &
                         min(max(FCM_factor*abs(convective/diffusive)-1.0_WP,0.0_WP),FCM_clip)
                 end if
              end do
              ! X
              do ss=i,i+1
                 if(AFwx(ss,j,k).gt.0.0_WP) then
                    convective =  &
                         +sum(interp_Ju_z(ss,j,:)*rhoU(ss,j,k-stc2:k+stc1)) &
                         *sum(interp_Jw_x(ss,j,:)*W(ss-stc2:ss+stc1,j,k))
                    diffusive = epsilon(1.0_WP) + &
                         sum(interp_sc_xz(ss,j,:,:)*VISC(ss-st2:ss+st1,j,k-st2:k+st1)) * &
                         sum(grad2d_w_x(ss,j,:)*W(ss-stv2:ss+stv1,j,k))
                    fcm_viscw(ss,j,k,1) = sum(interp_sc_xz(ss,j,:,:)*VISC(ss-st2:ss+st1,j,k-st2:k+st1)) * &
                         min(max(FCM_factor*abs(convective/diffusive)-1.0_WP,0.0_WP),FCM_clip)
                 end if
              end do
           end if

        end do
     end do
  end do

  do i=1,3
     call ib_update_border(fcm_viscw(:,:,:,i),'+','ym')
  end do

  ! Face fluxes
  do kk=kmin_-stv1,kmax_+stv2
     do jj=jmin_-stv1,jmax_+stv2
        do ii=imin_-stv1,imax_+stv2

           i = ii-1; j = jj-1; k = kk-1;

           FZ(i,j,k) = &
                + fcm_viscw(i,j,k,3) &
                * sum(grad_w_z(i,j,k,:)*W(i,j,k-stv1:k+stv2))

           i = ii; j = jj; k = kk;

           FX(i,j,k) = &
                + fcm_viscw(i,j,k,1) &
                * sum(grad_w_x(i,j,k,:)*W(i-stv2:i+stv1,j,k))

           FY(i,j,k) = &
                + fcm_viscw(i,j,k,2) &
                * sum(grad_w_y(i,j,k,:)*W(i,j-stv2:j+stv1,k))
        end do
     end do
  end do

  ! add to source term
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if(mask_w(i,j).eq.0 .and. VFw(i,j,k).gt.0.0_WP) then
              rhs = &
                   +sum(divv_zx(i,j,k,:)*FX(i-stv1:i+stv2,j,k)) &
                   +sum(divv_zy(i,j,k,:)*FY(i,j-stv1:j+stv2,k)) &
                   +sum(divv_zz(i,j,k,:)*FZ(i,j,k-stv2:k+stv1)) 
              srcWmid(i,j,k) = srcWmid(i,j,k) + dt_uvw*rhs
           end if
        end do
     end do
  end do

end subroutine ib_fcm_w


! ================================ !
! Computation of the aperture data !
! from level set function          !
!                                  !
! From compgeom module:            !
!                                  !
!      7----8                      !
!     /|   /|          3------4    !
!    5----6 |          |      |    !
!    | 3--|-4          |      |    !
!    |/   |/           |      |    !
!    1----2            1------2    !
!      HEX                REC      !
! ================================ !
subroutine ib_update_geometry
  use ib
  implicit none

  integer :: i,j,k,n
  integer :: si,sj,sk
  real(WP), dimension(3,8) :: cpos
  real(WP), dimension(  8) :: cval
  real(WP), dimension(3,4) :: fpos
  real(WP), dimension(  4) :: fval
  real(WP) :: my_length,my_surf,my_vol
  real(WP), dimension(  3) :: my_cent,my_cents,my_norm

  ! Compute the cell centered normal
  call ib_get_normal

  ! Pressure cell
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Form 3d cell
           n=0
           do sk=0,1
              do sj=0,1
                 do si=0,1
                    n=n+1
                    cpos(:,n)=(/x(i+si),y(j+sj),z(k+sk)/)
                    cval(  n)=get_Gib(cpos(:,n),i,j,k)
                 end do
              end do
           end do
           ! Obtain volume and surface
           call marching_tets(cpos,cval,my_vol,my_cent,my_surf,my_cents,my_norm)
           VFp(i,j,k)=my_vol*dxi(i)*dyi(j)*dzi
           SAp(i,j,k)=my_surf
           Cbar(i,j,k,:)=my_cent
           Sbar(i,j,k,:)=my_cents
           ! Form 2d cell in x and get surface fraction
           fpos(:,1)=cpos(:,1); fpos(:,2)=cpos(:,3); fpos(:,3)=cpos(:,5); fpos(:,4)=cpos(:,7)
           fval(  1)=cval(  1); fval(  2)=cval(  3); fval(  3)=cval(  5); fval(  4)=cval(  7)
           call marching_tris(fpos,fval,my_length,my_surf,my_cent)
           AFpx(i,j,k)=my_surf*dyi(j)*dzi
           ! Form 2d cell in y and get surface fraction
           fpos(:,1)=cpos(:,1); fpos(:,2)=cpos(:,2); fpos(:,3)=cpos(:,5); fpos(:,4)=cpos(:,6)
           fval(  1)=cval(  1); fval(  2)=cval(  2); fval(  3)=cval(  5); fval(  4)=cval(  6)
           call marching_tris(fpos,fval,my_length,my_surf,my_cent)
           AFpy(i,j,k)=my_surf*dxi(i)*dzi
           ! Form 2d cell in z and get surface fraction
           fpos(:,1)=cpos(:,1); fpos(:,2)=cpos(:,2); fpos(:,3)=cpos(:,3); fpos(:,4)=cpos(:,4)
           fval(  1)=cval(  1); fval(  2)=cval(  2); fval(  3)=cval(  3); fval(  4)=cval(  4)
           call marching_tris(fpos,fval,my_length,my_surf,my_cent)
           AFpz(i,j,k)=my_surf*dxi(i)*dyi(j)
        end do
     end do
  end do

  ! U cell
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Form 3d cell
           n=0
           do sk=0,1
              do sj=0,1
                 do si=-1,0
                    n=n+1
                    cpos(:,n)=(/xm(i+si),y(j+sj),z(k+sk)/)
                    cval(  n)=get_Gib(cpos(:,n),i,j,k)
                 end do
              end do
           end do
           ! Obtain volume and surface
           call marching_tets(cpos,cval,my_vol,my_cent,my_surf,my_cents,my_norm)
           VFu(i,j,k)=my_vol*dxmi(i-1)*dyi(j)*dzi
           SAu(i,j,k)=my_surf
           VHu(i,j,k)=max(get_Gib_bar(my_cent,cpos,cval),Gclip)
           Cbaru(i,j,k,:) = my_cent
           Sbaru(i,j,k,:) = my_cents
           ! Form 2d cell in x and get surface fraction
           fpos(:,1)=cpos(:,2); fpos(:,2)=cpos(:,4); fpos(:,3)=cpos(:,6); fpos(:,4)=cpos(:,8)
           fval(  1)=cval(  2); fval(  2)=cval(  4); fval(  3)=cval(  6); fval(  4)=cval(  8)
           call marching_tris(fpos,fval,my_length,my_surf,my_cent)
           AFux(i,j,k)=my_surf*dyi(j)*dzi
           ! Form 2d cell in y and get surface fraction
           fpos(:,1)=cpos(:,1); fpos(:,2)=cpos(:,2); fpos(:,3)=cpos(:,5); fpos(:,4)=cpos(:,6)
           fval(  1)=cval(  1); fval(  2)=cval(  2); fval(  3)=cval(  5); fval(  4)=cval(  6)
           call marching_tris(fpos,fval,my_length,my_surf,my_cent)
           AFuy(i,j,k)=my_surf*dxmi(i-1)*dzi
           ! Form 2d cell in z and get surface fraction
           fpos(:,1)=cpos(:,1); fpos(:,2)=cpos(:,2); fpos(:,3)=cpos(:,3); fpos(:,4)=cpos(:,4)
           fval(  1)=cval(  1); fval(  2)=cval(  2); fval(  3)=cval(  3); fval(  4)=cval(  4)
           call marching_tris(fpos,fval,my_length,my_surf,my_cent)
           AFuz(i,j,k)=my_surf*dxmi(i-1)*dyi(j)
        end do
     end do
  end do

  ! V cell
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Form 3d cell
           n=0
           do sk=0,1
              do sj=-1,0
                 do si=0,1
                    n=n+1
                    cpos(:,n)=(/x(i+si),ym(j+sj),z(k+sk)/)
                    cval(  n)=get_Gib(cpos(:,n),i,j,k)
                 end do
              end do
           end do
           ! Obtain volume and surface
           call marching_tets(cpos,cval,my_vol,my_cent,my_surf,my_cents,my_norm)
           VFv(i,j,k)=my_vol*dxi(i)*dymi(j-1)*dzi
           SAv(i,j,k)=my_surf
           VHv(i,j,k)=max(get_Gib_bar(my_cent,cpos,cval),Gclip)
           Cbarv(i,j,k,:) = my_cent
           Sbarv(i,j,k,:) = my_cents
           ! Form 2d cell in x and get surface fraction
           fpos(:,1)=cpos(:,1); fpos(:,2)=cpos(:,3); fpos(:,3)=cpos(:,5); fpos(:,4)=cpos(:,7)
           fval(  1)=cval(  1); fval(  2)=cval(  3); fval(  3)=cval(  5); fval(  4)=cval(  7)
           call marching_tris(fpos,fval,my_length,my_surf,my_cent)
           AFvx(i,j,k)=my_surf*dymi(j-1)*dzi
           ! Form 2d cell in y and get surface fraction
           fpos(:,1)=cpos(:,3); fpos(:,2)=cpos(:,4); fpos(:,3)=cpos(:,7); fpos(:,4)=cpos(:,8)
           fval(  1)=cval(  3); fval(  2)=cval(  4); fval(  3)=cval(  7); fval(  4)=cval(  8)
           call marching_tris(fpos,fval,my_length,my_surf,my_cent)
           AFvy(i,j,k)=my_surf*dxi(i)*dzi
           ! Form 2d cell in z and get surface fraction
           fpos(:,1)=cpos(:,1); fpos(:,2)=cpos(:,2); fpos(:,3)=cpos(:,3); fpos(:,4)=cpos(:,4)
           fval(  1)=cval(  1); fval(  2)=cval(  2); fval(  3)=cval(  3); fval(  4)=cval(  4)
           call marching_tris(fpos,fval,my_length,my_surf,my_cent)
           AFvz(i,j,k)=my_surf*dxi(i)*dymi(j-1)
        end do
     end do
  end do

  ! W cell
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Form 3d cell
           n=0
           do sk=-1,0
              do sj=0,1
                 do si=0,1
                    n=n+1
                    cpos(:,n)=(/x(i+si),y(j+sj),zm(k+sk)/)
                    cval(  n)=get_Gib(cpos(:,n),i,j,k)
                 end do
              end do
           end do
           ! Obtain volume and surface
           call marching_tets(cpos,cval,my_vol,my_cent,my_surf,my_cents,my_norm)
           VFw(i,j,k)=my_vol*dxi(i)*dyi(j)*dzi
           SAw(i,j,k)=my_surf
           VHw(i,j,k)=max(get_Gib_bar(my_cent,cpos,cval),Gclip)
           Cbarw(i,j,k,:) = my_cent
           Sbarw(i,j,k,:) = my_cents
           ! Form 2d cell in x and get surface fraction
           fpos(:,1)=cpos(:,1); fpos(:,2)=cpos(:,3); fpos(:,3)=cpos(:,5); fpos(:,4)=cpos(:,7)
           fval(  1)=cval(  1); fval(  2)=cval(  3); fval(  3)=cval(  5); fval(  4)=cval(  7)
           call marching_tris(fpos,fval,my_length,my_surf,my_cent)
           AFwx(i,j,k)=my_surf*dyi(j)*dzi
           ! Form 2d cell in y and get surface fraction
           fpos(:,1)=cpos(:,1); fpos(:,2)=cpos(:,2); fpos(:,3)=cpos(:,5); fpos(:,4)=cpos(:,6)
           fval(  1)=cval(  1); fval(  2)=cval(  2); fval(  3)=cval(  5); fval(  4)=cval(  6)
           call marching_tris(fpos,fval,my_length,my_surf,my_cent)
           AFwy(i,j,k)=my_surf*dxi(i)*dzi
           ! Form 2d cell in z and get surface fraction
           fpos(:,1)=cpos(:,5); fpos(:,2)=cpos(:,6); fpos(:,3)=cpos(:,7); fpos(:,4)=cpos(:,8)
           fval(  1)=cval(  5); fval(  2)=cval(  6); fval(  3)=cval(  7); fval(  4)=cval(  8)
           call marching_tris(fpos,fval,my_length,my_surf,my_cent)
           AFwz(i,j,k)=my_surf*dxi(i)*dyi(j)
        end do
     end do
  end do


  ! CV centered at x(i),y(j),zm(k) - VFuv
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Form 3d cell
           n=0
           do sk=0,1
              do sj=-1,0
                 do si=-1,0
                    n=n+1
                    cpos(:,n)=(/xm(i+si),ym(j+sj),z(k+sk)/)
                    cval(  n)=get_Gib(cpos(:,n),i,j,k)
                 end do
              end do
           end do
           ! Obtain volume and surface
           call marching_tets(cpos,cval,my_vol,my_cent,my_surf,my_cents,my_norm)
           VFuv(i,j,k)=my_vol*dxmi(i-1)*dymi(j-1)*dzi
           ! Form 2d cell in x @ xm(i),y(j),zm(k) and get surface fraction
           fpos(:,1)=cpos(:,2); fpos(:,2)=cpos(:,4); fpos(:,3)=cpos(:,6); fpos(:,4)=cpos(:,8)
           fval(  1)=cval(  2); fval(  2)=cval(  4); fval(  3)=cval(  6); fval(  4)=cval(  8)
           call marching_tris(fpos,fval,my_length,my_surf,my_cent)
           AFuvx(i,j,k)=my_surf*dymi(j-1)*dzi
           ! Form 2d cell in y @ x(i),ym(j),zm(k) and get surface fraction
           fpos(:,1)=cpos(:,3); fpos(:,2)=cpos(:,4); fpos(:,3)=cpos(:,7); fpos(:,4)=cpos(:,8)
           fval(  1)=cval(  3); fval(  2)=cval(  4); fval(  3)=cval(  7); fval(  4)=cval(  8)
           call marching_tris(fpos,fval,my_length,my_surf,my_cent)
           AFuvy(i,j,k)=my_surf*dxmi(i-1)*dzi
        end do
     end do
  end do

  ! CV centered at x(i),ym(j),z(k) - VFuw
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Form 3d cell
           n=0
           do sk=-1,0
              do sj=0,1
                 do si=-1,0
                    n=n+1
                    cpos(:,n)=(/xm(i+si),y(j+sj),zm(k+sk)/)
                    cval(  n)=get_Gib(cpos(:,n),i,j,k)
                 end do
              end do
           end do
           ! Obtain volume and surface
           call marching_tets(cpos,cval,my_vol,my_cent,my_surf,my_cents,my_norm)
           VFuw(i,j,k)=my_vol*dxmi(i-1)*dyi(j)*dzi
           ! Form 2d cell in x @ xm(i),ym(j),z(k) and get surface fraction
           fpos(:,1)=cpos(:,2); fpos(:,2)=cpos(:,4); fpos(:,3)=cpos(:,6); fpos(:,4)=cpos(:,8)
           fval(  1)=cval(  2); fval(  2)=cval(  4); fval(  3)=cval(  6); fval(  4)=cval(  8)
           call marching_tris(fpos,fval,my_length,my_surf,my_cent)
           AFuwx(i,j,k)=my_surf*dyi(j)*dzi
           ! Form 2d cell in z @ x(i),ym(j),zm(k) and get surface fraction
           fpos(:,1)=cpos(:,5); fpos(:,2)=cpos(:,6); fpos(:,3)=cpos(:,7); fpos(:,4)=cpos(:,8)
           fval(  1)=cval(  5); fval(  2)=cval(  6); fval(  3)=cval(  7); fval(  4)=cval(  8)
           call marching_tris(fpos,fval,my_length,my_surf,my_cent)
           AFuwz(i,j,k)=my_surf*dxmi(i-1)*dyi(j)
        end do
     end do
  end do

  ! CV centered at xm(i),y(j),z(k) - VFvw
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Form 3d cell
           n=0
           do sk=-1,0
              do sj=-1,0
                 do si=0,1
                    n=n+1
                    cpos(:,n)=(/x(i+si),ym(j+sj),zm(k+sk)/)
                    cval(  n)=get_Gib(cpos(:,n),i,j,k)
                 end do
              end do
           end do
           ! Obtain volume and surface
           call marching_tets(cpos,cval,my_vol,my_cent,my_surf,my_cents,my_norm)
           VFvw(i,j,k)=my_vol*dxi(i)*dymi(j-1)*dzi
           ! Form 2d cell in y @ xm(i),ym(j),z(k) and get surface fraction
           fpos(:,1)=cpos(:,3); fpos(:,2)=cpos(:,4); fpos(:,3)=cpos(:,7); fpos(:,4)=cpos(:,8)
           fval(  1)=cval(  3); fval(  2)=cval(  4); fval(  3)=cval(  7); fval(  4)=cval(  8)
           call marching_tris(fpos,fval,my_length,my_surf,my_cent)
           AFvwy(i,j,k)=my_surf*dxi(i)*dzi
           ! Form 2d cell in z @ xm(i),y(j),zm(k) and get surface fraction
           fpos(:,1)=cpos(:,5); fpos(:,2)=cpos(:,6); fpos(:,3)=cpos(:,7); fpos(:,4)=cpos(:,8)
           fval(  1)=cval(  5); fval(  2)=cval(  6); fval(  3)=cval(  7); fval(  4)=cval(  8)
           call marching_tris(fpos,fval,my_length,my_surf,my_cent)
           AFvwz(i,j,k)=my_surf*dxi(i)*dymi(j-1)
        end do
     end do
  end do

  ! Update boundaries
  call ib_update_border(Cbar(:,:,:,1),'+','ym')
  call ib_update_border(Cbar(:,:,:,2),'-','ym')
  call ib_update_border(Cbar(:,:,:,3),'-','ym')
  if (xper.eq.1.and.iproc.eq. 1 ) Cbar(imino :imin-1,:,:,1)=Cbar(imino :imin-1,:,:,1)-xL
  if (xper.eq.1.and.iproc.eq.npx) Cbar(imax+1:imaxo ,:,:,1)=Cbar(imax+1:imaxo ,:,:,1)+xL
  if (yper.eq.1.and.jproc.eq. 1 ) Cbar(:,jmino :jmin-1,:,2)=Cbar(:,jmino :jmin-1,:,2)-yL
  if (yper.eq.1.and.jproc.eq.npy) Cbar(:,jmax+1:jmaxo ,:,2)=Cbar(:,jmax+1:jmaxo ,:,2)+yL
  if (zper.eq.1.and.kproc.eq. 1 ) Cbar(:,:,kmino :kmin-1,3)=Cbar(:,:,kmino :kmin-1,3)-zL
  if (zper.eq.1.and.kproc.eq.npz) Cbar(:,:,kmax+1:kmaxo ,3)=Cbar(:,:,kmax+1:kmaxo ,3)+zL

  call ib_update_border(SAp ,'+','ym')
  call ib_update_border(VFp ,'+','ym')
  call ib_update_border(AFpx,'+','ym')
  call ib_update_border(AFpy,'+','y' )
  call ib_update_border(AFpz,'+','ym')

  call ib_update_border(SAu ,'+','ym')
  call ib_update_border(VFu ,'+','ym')
  call ib_update_border(AFux,'+','ym')
  call ib_update_border(AFuy,'+','y' )
  call ib_update_border(AFuz,'+','ym')
  call ib_update_border(VHu ,'+','ym')

  call ib_update_border(SAv ,'+','y' )
  call ib_update_border(VFv ,'+','y' )
  call ib_update_border(AFvx,'+','y' )
  call ib_update_border(AFvy,'+','ym')
  call ib_update_border(AFvz,'+','y' )
  call ib_update_border(VHv ,'+','y' )

  call ib_update_border(SAw ,'+','ym')
  call ib_update_border(VFw ,'+','ym')
  call ib_update_border(AFwx,'+','ym')
  call ib_update_border(AFwy,'+','y' )
  call ib_update_border(AFwz,'+','ym')
  call ib_update_border(VHw ,'+','ym')

  call ib_update_border(VFuv ,'+','ym')
  call ib_update_border(AFuvx,'+','ym')
  call ib_update_border(AFuvy,'+','y' )

  call ib_update_border(VFuw ,'+','ym')
  call ib_update_border(AFuwx,'+','ym')
  call ib_update_border(AFuwz,'+','ym')

  call ib_update_border(VFvw ,'+','ym')
  call ib_update_border(AFvwy,'+','y' )
  call ib_update_border(AFvwz,'+','ym')

  ! update barycenters
  call ib_update_border(Sbar(:,:,:,1),'+','ym')
  call ib_update_border(Sbar(:,:,:,2),'+','ym')
  call ib_update_border(Sbar(:,:,:,3),'+','ym')
  call ib_update_border(Cbaru(:,:,:,1),'+','ym')
  call ib_update_border(Cbaru(:,:,:,2),'+','ym')
  call ib_update_border(Cbaru(:,:,:,3),'+','ym')
  call ib_update_border(Sbaru(:,:,:,1),'+','ym')
  call ib_update_border(Sbaru(:,:,:,2),'+','ym')
  call ib_update_border(Sbaru(:,:,:,3),'+','ym')
  call ib_update_border(Cbarv(:,:,:,1),'+','ym')
  call ib_update_border(Cbarv(:,:,:,2),'+','ym')
  call ib_update_border(Cbarv(:,:,:,3),'+','ym')
  call ib_update_border(Sbarv(:,:,:,1),'+','ym')
  call ib_update_border(Sbarv(:,:,:,2),'+','ym')
  call ib_update_border(Sbarv(:,:,:,3),'+','ym')
  call ib_update_border(Cbarw(:,:,:,1),'+','ym')
  call ib_update_border(Cbarw(:,:,:,2),'+','ym')
  call ib_update_border(Cbarw(:,:,:,3),'+','ym')
  call ib_update_border(Sbarw(:,:,:,1),'+','ym')
  call ib_update_border(Sbarw(:,:,:,2),'+','ym')
  call ib_update_border(Sbarw(:,:,:,3),'+','ym')

  ! unmerged (i.e. scalar) geometry
  ! VFs = VFp
  ! SAs = SAp
  ! AFsx = AFpx
  ! AFsy = AFpy
  ! AFsz = AFpz
  ! Cbars = Cbar
  ! Sbars = Sbar
  
  ! apply fcm procedure to pressure cells
  call fcm_pressure
  ! merged geometry for scalars
  VFs = VFp
  SAs = SAp
  AFsx = AFpx
  AFsy = AFpy
  AFsz = AFpz
  Cbars = Cbar
  Sbars = Sbar
  
  call extend_cent(Cbar)
  call extend_cent(Cbaru)
  call extend_cent(Cbarv)
  call extend_cent(Cbarw)
  call extend_cent(Cbars)

  !call ib_dump_pressure
  return
end subroutine ib_update_geometry


! =================== !
! Merge small P-cells !
! =================== !
subroutine fcm_pressure
  use ib
  implicit none

  integer :: i,j,k,ii,jj,kk,s1,s2,dim,dir,pos(3),cf
  integer :: max_tag,new_max_tag,my_max_tag,tgt_tag
  real(WP) :: myA,help,local_sum

  if(.not.use_fcm_pressure) return

  ! initialize data
  ctag=0
  mix_frac_vol = 0.0_WP
  mix_frac_face=0.0_WP

  if(Aclip.gt.0.0_WP) then
     ! small cell identification based on area fraction
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if(VFp(i,j,k).le.0.0_WP) then
                 ctag(i,j,k) = -1
                 cycle
              end if
              myA = min(maxval(AFpx(i:i+1,j,k)),maxval(AFpy(i,j:j+1,k)),maxval(AFpz(i,j,k:k+1)))
              if (myA.lt.Aclip.and.SAp(i,j,k).gt.0.0_WP) ctag(i,j,k)=1

           end do
        end do
     end do
  else
     ! small cell identification based on volume fraction
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_  
              ! mark small cells 
              if(VFp(i,j,k).le.Vclip.and.SAp(i,j,k).gt.0.0_WP) ctag(i,j,k) = 1
              ! mark cells inside IB
              if(VFp(i,j,k).le.Vclip.and.SAp(i,j,k).le.0.0_WP) ctag(i,j,k) = -1
           end do
        end do
     end do
  end if
  ! number of pressure cells to be fcm'ed
  fcm_p_count = count(ctag.eq.1)

  call boundary_update_border_int(ctag,'+','ym')

  ! determine maximum tag via recursive tagging
  max_tag = 0
  new_max_tag = 1
  call tag_cells(max_tag,new_max_tag)

  fcm_p_maxtag = max_tag

  ! compute mixing fraction for volumetric data
  do my_max_tag = max_tag,1,-1
     ! a tagged cell can only send data to a cell tagged with one less
     tgt_tag = my_max_tag-1
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_  
              if(ctag(i,j,k).ne.my_max_tag) cycle
              do dim = 1,3
                 do dir=-1,1,2
                    pos=0;pos(dim)=dir
                    ii=i+pos(1);jj=j+pos(2);kk=k+pos(3)
                    s1 = max(0,dir)

                    if(ctag(ii,jj,kk).eq.tgt_tag) then
                       if(dim.eq.1) mix_frac_vol(i,j,k,dim,s1) = AFpx(i+s1,j,k)**2*VFp(ii,jj,kk)
                       if(dim.eq.2) mix_frac_vol(i,j,k,dim,s1) = AFpy(i,j+s1,k)**2*VFp(ii,jj,kk)
                       if(dim.eq.3) mix_frac_vol(i,j,k,dim,s1) = AFpz(i,j,k+s1)**2*VFp(ii,jj,kk)
                    end if
                 end do
              end do

              local_sum = sum(mix_frac_vol(i,j,k,:,:))
              mix_frac_vol(i,j,k,:,:) = mix_frac_vol(i,j,k,:,:)/local_sum
           end do
        end do
     end do
  end do
  do s1=0,1
     call boundary_update_border(mix_frac_vol(:,:,:,1,s1),'+','ym')
     call boundary_update_border(mix_frac_vol(:,:,:,2,s1),'+','ym')
     call boundary_update_border(mix_frac_vol(:,:,:,3,s1),'+','ym')
  end do
  ! use volumetric mixing fractions to determine mixing fractions for face based quantities
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_  
           do dim = 1,3
              do dir=-1,1,2
                 pos=0;pos(dim)=dir
                 ii=i+pos(1);jj=j+pos(2);kk=k+pos(3)
                 s1 = max(0,dir)
                 ! Transfer AFp{xyz} of this cell to target cell in dimension dim and direction s1
                 !  -- only do this if the cells containing target face do not have a ctag ge than mine
                 if(dim.ne.1) then
                    if (max(ctag(i-1,j,k),ctag(i,j,k)).gt.max(ctag(ii-1,jj,kk),ctag(ii,jj,kk))) &
                         mix_frac_face(i,j,k,1,dim,s1) = AFpx(i,j,k)*AFpx(ii,jj,kk)*sqrt(mix_frac_vol(i,j,k,dim,s1)**2+mix_frac_vol(i-1,j,k,dim,s1)**2)
                 end if
                 if(dim.ne.2) then
                    if (max(ctag(i,j-1,k),ctag(i,j,k)).gt.max(ctag(ii,jj-1,kk),ctag(ii,jj,kk))) &
                         mix_frac_face(i,j,k,2,dim,s1) = AFpy(i,j,k)*AFpy(ii,jj,kk)*sqrt(mix_frac_vol(i,j,k,dim,s1)**2+mix_frac_vol(i,j-1,k,dim,s1)**2)
                 end if
                 if(dim.ne.3) then
                    if (max(ctag(i,j,k-1),ctag(i,j,k)).gt.max(ctag(ii,jj,kk-1),ctag(ii,jj,kk))) &
                         mix_frac_face(i,j,k,3,dim,s1) = AFpz(i,j,k)*AFpz(ii,jj,kk)*sqrt(mix_frac_vol(i,j,k,dim,s1)**2+mix_frac_vol(i,j,k-1,dim,s1)**2)
                 end if
              end do
           end do
           ! normalize
           do dim = 1,3
              local_sum = sum(mix_frac_face(i,j,k,dim,:,:))
              if(local_sum.gt.0.0_WP) mix_frac_face(i,j,k,dim,:,:) = mix_frac_face(i,j,k,dim,:,:)/(local_sum+epsilon(1.0_WP))
           end do
        end do
     end do
  end do
  do dim=1,3
     do s1=0,1
        call boundary_update_border(mix_frac_face(:,:,:,1,dim,s1),'+','ym')
        call boundary_update_border(mix_frac_face(:,:,:,2,dim,s1),'+','ym')
        call boundary_update_border(mix_frac_face(:,:,:,3,dim,s1),'+','ym')
     end do
  end do
  ! Perform fcm
  do my_max_tag = max_tag,1,-1
     do k=kmino_,kmaxo_
        do j=jmino_,jmaxo_
           do i=imino_,imaxo_

              ! Cycle if not tagged with my_max_tag
              if (ctag(i,j,k).ne.my_max_tag) cycle

              do dim=1,3
                 do dir=-1,1,2
                    pos=0;pos(dim)=dir
                    s1 = max(0,dir)
                    ii=i+pos(1);jj=j+pos(2);kk=k+pos(3)

                    ! Handle inside cells
                    if ( ii.ge.imino_ .and. ii.le.imaxo_ .and. &
                         jj.ge.jmino_ .and. jj.le.jmaxo_ .and. &
                         kk.ge.kmino_ .and. kk.le.kmaxo_ ) then
                       ! modify volumetric geometric info
                       help = mix_frac_vol(i,j,k,dim,s1)*dxi(ii)*dyi(jj)*dzi*dx(i)*dy(j)*dz*VFp(i,j,k)
                       Cbar(ii,jj,kk,:) = (Cbar(ii,jj,kk,:)*VFp(ii,jj,kk)+Cbar(i,j,k,:)*help)/(VFp(ii,jj,kk)+help)
                       VFp(ii,jj,kk)=VFp(ii,jj,kk)+help

                       SAp(ii,jj,kk)=SAp(ii,jj,kk)+mix_frac_vol(i,j,k,dim,s1)*SAp(i,j,k)

                       ! merge AFpx
                       if(dim.ne.1) then
                          do s2=i,min(i+1,imaxo_)
                             AFpx(s2,jj,kk)=AFpx(s2,jj,kk)+mix_frac_face(s2,j,k,1,dim,s1)*dyi(jj)*dzi*dy(j)*dz*AFpx(s2,j,k)
                          end do
                       end if
                       ! merge AFpy
                       if(dim.ne.2) then
                          do s2=j,min(j+1,jmaxo_)
                             AFpy(ii,s2,kk)=AFpy(ii,s2,kk)+mix_frac_face(i,s2,k,2,dim,s1)*dxi(ii)*dzi*dx(i)*dz*AFpy(i,s2,k)
                          end do
                       end if
                       ! merge AFpz
                       if(dim.ne.3) then
                          do s2=k,min(k+1,kmaxo_)
                             AFpz(ii,jj,s2)=AFpz(ii,jj,s2)+mix_frac_face(i,j,s2,3,dim,s1)*dxi(ii)*dyi(jj)*dx(i)*dy(j)*AFpz(i,j,s2)
                          end do
                       end if

                    end if
                 end do
              end do

              ! Remove small cell
              VFp(i,j,k)=0.0_WP
              SAp(i,j,k)=0.0_WP
              Cbar(i,j,k,:) = 0.0_WP
              AFpx(i:min(i+1,imaxo_),j,k)=0.0_WP
              AFpy(i,j:min(j+1,jmaxo_),k)=0.0_WP
              AFpz(i,j,k:min(k+1,kmaxo_))=0.0_WP
           end do
        end do
     end do

     ! Communicate after mixing of cells with max_tag has been completed
     call ib_update_border(SAp ,'+','ym')
     call ib_update_border(VFp ,'+','ym')
     call ib_update_border(AFpx,'+','ym')
     call ib_update_border(AFpy,'+','y' )
     call ib_update_border(AFpz,'+','ym')
  end do
  return
end subroutine fcm_pressure

! tag cells "recursively" if surrounded by tagged cells
subroutine tag_cells(max_tag,new_max_tag)
  use ib
  use parallel
  implicit none
  integer,intent(inout) :: max_tag,new_max_tag
  integer :: i,j,k,n,nhelp,my_max_tag

  do while(max_tag.ne.new_max_tag)
     max_tag = new_max_tag
     ctag2 = ctag
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_  
              if(ctag(i,j,k).ne.max_tag) cycle
              ! cell is tagged with current max_tag
              n = ctag(i+1,j,k)*ctag(i-1,j,k)*ctag(i,j-1,k)*ctag(i,j+1,k)*ctag(i,j,k-1)*ctag(i,j,k+1)
              if(n.eq.0) cycle
              ! all neighbor cells have been tagged

              ! number of cells _not_ in IB (i.e. [-ctag(i,j,k)+abs(ctag(i,j,k))]/2 = 1 if ctag(i,j,k) == -1)
              nhelp = 6-(-ctag(i+1,j,k)+abs(ctag(i+1,j,k))&
                   -ctag(i-1,j,k)+abs(ctag(i-1,j,k))&
                   -ctag(i,j+1,k)+abs(ctag(i,j+1,k))&
                   -ctag(i,j-1,k)+abs(ctag(i,j-1,k))&
                   -ctag(i,j,k+1)+abs(ctag(i,j,k+1))&
                   -ctag(i,j,k-1)+abs(ctag(i,j,k-1)))/2
              ! sum(tags) for cells not in IB
              n = (ctag(i+1,j,k)+abs(ctag(i+1,j,k))&
                   +ctag(i-1,j,k)+abs(ctag(i-1,j,k))&
                   +ctag(i,j+1,k)+abs(ctag(i,j+1,k))&
                   +ctag(i,j-1,k)+abs(ctag(i,j-1,k))&
                   +ctag(i,j,k+1)+abs(ctag(i,j,k+1))&
                   +ctag(i,j,k-1)+abs(ctag(i,j,k-1)))/2
              ! if all neighbor cells tagged with max_tag increase by one
              if(n.eq.nhelp*max_tag) then
                 ctag2(i,j,k) = max_tag+1
              end if
           end do
        end do
     end do
     ctag = ctag2
     my_max_tag = maxval(ctag)
     call parallel_max(my_max_tag,new_max_tag)
     call boundary_update_border_int(ctag,'+','ym')
  end do

end subroutine tag_cells

! ================ !
! Shortcut for BCs !
! ================ !
subroutine ib_update_border(A,sym,axis)
  use ib
  implicit none

  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: A
  character(len=*), intent(in) :: sym,axis

  call boundary_update_border(A,sym,axis)
  call boundary_neumann(A,'-ym')
  call boundary_neumann(A,'+ym')
  call boundary_neumann(A,'-xm')
  call boundary_neumann(A,'+xm')

  return
end subroutine ib_update_border


! ================ !
! Shortcut for BCs !
! ================ !
subroutine ib_update_border_int(A,sym,axis)
  use ib
  implicit none

  integer, dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: A
  character(len=*), intent(in) :: sym,axis

  call boundary_update_border_int(A,sym,axis)
  call boundary_neumann_int(A,'-ym')
  call boundary_neumann_int(A,'+ym')
  call boundary_neumann_int(A,'-xm')
  call boundary_neumann_int(A,'+xm')

  return
end subroutine ib_update_border_int


! ======================== !
! Normal of the IB surface !
! ======================== !
subroutine ib_get_normal
  use ib
  implicit none

  integer :: i,j,k
  real(WP) :: norm

  ! Precompute grad(Gib)
  call gradient_vector(Gib,Nxib,Nyib,Nzib)

  ! Form the norms
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           norm = sqrt(Nxib(i,j,k)**2+Nyib(i,j,k)**2+Nzib(i,j,k)**2)
           norm = norm+epsilon(1.0_WP)
           Nxib(i,j,k) = Nxib(i,j,k)/norm
           Nyib(i,j,k) = Nyib(i,j,k)/norm
           Nzib(i,j,k) = Nzib(i,j,k)/norm
        end do
     end do
  end do

  ! Update ghost cells
  call ib_update_border(Nxib,'+','ym')
  call ib_update_border(Nyib,'-','ym')
  call ib_update_border(Nzib,'-','ym')

  return
end subroutine ib_get_normal


! ================================ !
! Momentum correction for IB cells !
! ================================ !
subroutine ib_correct_momentum
  use ib
  use velocity
  use memory
  implicit none
  
  integer :: i,j,k
  real(WP) :: myrho,my_visc
  real(WP), dimension(3) :: velib

  ! If not used, return
  if (.not.use_ib) return

  call ib_set_velocity

  ! Zero source terms
  VFx_sc = 0.0_WP
  VFy_sc = 0.0_WP
  VFz_sc = 0.0_WP

  if(correct_momentum_cells) then
     ! check to see if we are correcting based on volume fraction or pressure
     if(MVF_clip.gt.0.0_WP) then
        ! Loop over domain
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 ! Update uncorrected U cells
                 if (VFu(i,j,k).le.MVF_clip) then
                    myrho = sum(interp_sc_x(i,j,:)*RHOmid(i-st2:i+st1,j,k))
                    if(SAu(i,j,k).gt.epsilon(1.0_WP).and.VFu(i,j,k).gt.epsilon(1.0_WP)) then
                       my_visc = sum(interp_sc_x(i,j,:)*VISC(i-st2:i+st1,j,k))
                       VFx_sc(i,j,k) = my_visc*SAu(i,j,k)*(rhoU(i,j,k)/myrho-U_ib(i,j,k))/VHu(i,j,k)
                    end if
                    ! Set new velocity
                    rhoU(i,j,k) = myrho*U_ib(i,j,k)

                 end if

                 ! Update uncorrected V cells
                 if (VFv(i,j,k).le.MVF_clip) then
                    myrho = sum(interp_sc_y(i,j,:)*RHOmid(i,j-st2:j+st1,k))
                    if(SAv(i,j,k).gt.epsilon(1.0_WP).and.VFv(i,j,k).gt.epsilon(1.0_WP)) then
                       my_visc=sum(interp_sc_y(i,j,:)*VISC(i,j-st2:j+st1,k))
                       VFy_sc(i,j,k)=my_visc*SAv(i,j,k)*(rhoV(i,j,k)/myrho-V_ib(i,j,k))/VHv(i,j,k)
                    end if
                    ! Set new velocity
                    rhoV(i,j,k) = myrho*V_ib(i,j,k)
                 end if

                 ! Update uncorrected W cells
                 if (VFw(i,j,k).le.MVF_clip) then
                    myrho = sum(interp_sc_z(i,j,:)*RHOmid(i,j,k-st2:k+st1))
                    if (SAw(i,j,k).gt.epsilon(1.0_WP).and.VFw(i,j,k).gt.epsilon(1.0_WP)) then
                       my_visc=sum(interp_sc_z(i,j,:)*VISC(i,j,k-st2:k+st1))
                       VFz_sc(i,j,k)=my_visc*SAw(i,j,k)*(rhoW(i,j,k)/myrho-W_ib(i,j,k))/VHw(i,j,k)
                    end if
                    ! Set new velocity
                    rhoW(i,j,k) = myrho*W_ib(i,j,k)
                 end if

              end do
           end do
        end do
     else
        ! Loop over domain
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_

                 ! Update uncorrected U cells
                 if (AFpx(i,j,k).le.0.0_WP) then
                    myrho = sum(interp_sc_x(i,j,:)*RHOmid(i-st2:i+st1,j,k))
                    if(SAu(i,j,k).gt.0.0_WP.and.VFu(i,j,k).gt.0.0_WP) then
                       my_visc = sum(interp_sc_x(i,j,:)*VISC(i-st2:i+st1,j,k))
                       VFx_sc(i,j,k) = my_visc*SAu(i,j,k)*(rhoU(i,j,k)/myrho-U_ib(i,j,k))/VHu(i,j,k)
                    end if
                    ! Set new velocity
                    rhoU(i,j,k) = myrho*U_ib(i,j,k)

                 end if

                 ! Update uncorrected V cells
                 if (AFpy(i,j,k).le.0.0_WP) then
                    myrho = sum(interp_sc_y(i,j,:)*RHOmid(i,j-st2:j+st1,k))
                    if(SAv(i,j,k).gt.0.0_WP.and.VFv(i,j,k).gt.0.0_WP) then
                       my_visc=sum(interp_sc_y(i,j,:)*VISC(i,j-st2:j+st1,k))
                       VFy_sc(i,j,k)=my_visc*SAv(i,j,k)*(rhoV(i,j,k)/myrho-V_ib(i,j,k))/VHv(i,j,k)
                    end if
                    ! Set new velocity
                    rhoV(i,j,k) = myrho*V_ib(i,j,k)
                 end if

                 ! Update uncorrected W cells
                 if (AFpz(i,j,k).le.0.0_WP) then
                    myrho = sum(interp_sc_z(i,j,:)*RHOmid(i,j,k-st2:k+st1))
                    if (SAw(i,j,k).gt.0.0_WP.and.VFw(i,j,k).gt.0.0_WP) then
                       my_visc=sum(interp_sc_z(i,j,:)*VISC(i,j,k-st2:k+st1))
                       VFz_sc(i,j,k)=my_visc*SAw(i,j,k)*(rhoW(i,j,k)/myrho-W_ib(i,j,k))/VHw(i,j,k)
                    end if
                    ! Set new velocity
                    rhoW(i,j,k) = myrho*W_ib(i,j,k)
                 end if

              end do
           end do
        end do
     end if
  else
     ! Loop over domain
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_

              ! Update interior U cells
              if (VFu(i,j,k).le.epsilon(1.0_WP)) then
                 myrho = sum(interp_sc_x(i,j,:)*RHOmid(i-st2:i+st1,j,k))
                 ! Set new velocity
                 rhoU(i,j,k) = myrho*U_ib(i,j,k)
              end if

              ! Update interior V cells
              if (VFv(i,j,k).le.epsilon(1.0_WP)) then
                 myrho = sum(interp_sc_y(i,j,:)*RHOmid(i,j-st2:j+st1,k))
                 ! Set new velocity
                 rhoV(i,j,k) = myrho*V_ib(i,j,k)

              end if

              ! Update interior W cells
              if (VFw(i,j,k).le.epsilon(1.0_WP)) then
                 myrho = sum(interp_sc_z(i,j,:)*RHOmid(i,j,k-st2:k+st1))
                 ! Set new velocity
                 rhoW(i,j,k) = myrho*W_ib(i,j,k)
              end if

           end do
        end do
     end do

  end if
  return
end subroutine ib_correct_momentum

subroutine ib_monitor
  use time_info
  use data
  use ib
  use parallel
  implicit none

  real(WP), dimension(8) :: fcm_array
  real(WP),dimension(3) :: force_visc,force_pres
  if(.not.use_ib) return

  fcm_array = 0.0_WP
  if(use_fcm_pressure) then
     call parallel_max(real(fcm_p_maxtag,WP),fcm_array(1))
     call parallel_sum(real(fcm_p_count,WP) ,fcm_array(2))
  end if
  if(use_fcm_momentum) then
     call parallel_max(maxval(FCM_viscu(imin_:imax_,jmin_:jmax_,kmin_:kmax_,:)),fcm_array(3))
     call parallel_sum(real(count(FCM_viscu(imin_:imax_,jmin_:jmax_,kmin_:kmax_,:) > 0.0_WP  ),WP),fcm_array(4))
     call parallel_max(maxval(FCM_viscv(imin_:imax_,jmin_:jmax_,kmin_:kmax_,:)),fcm_array(5))
     call parallel_sum(real(count(FCM_viscv(imin_:imax_,jmin_:jmax_,kmin_:kmax_,:) > 0.0_WP  ),WP),fcm_array(6))
     call parallel_max(maxval(FCM_viscw(imin_:imax_,jmin_:jmax_,kmin_:kmax_,:)),fcm_array(7))
     call parallel_sum(real(count(FCM_viscw(imin_:imax_,jmin_:jmax_,kmin_:kmax_,:) > 0.0_WP  ),WP),fcm_array(8))
  end if

  if(use_fcm_pressure .or. use_fcm_momentum) then
     call monitor_select_file('ib_fcm')
     call monitor_set_array_values(fcm_array)
  end if

  ! force on ib
  call parallel_sum(sum(VFx(imin_:imax_,jmin_:jmax_,kmin_:kmax_)),force_visc(1))
  call parallel_sum(sum(VFy(imin_:imax_,jmin_:jmax_,kmin_:kmax_)),force_visc(2))
  call parallel_sum(sum(VFz(imin_:imax_,jmin_:jmax_,kmin_:kmax_)),force_visc(3))
  call parallel_sum(sum(PFx(imin_:imax_,jmin_:jmax_,kmin_:kmax_)),force_pres(1))
  call parallel_sum(sum(PFy(imin_:imax_,jmin_:jmax_,kmin_:kmax_)),force_pres(2))
  call parallel_sum(sum(PFz(imin_:imax_,jmin_:jmax_,kmin_:kmax_)),force_pres(3))


  call monitor_select_file('ib_force')
  call monitor_set_single_value(1,force_visc(1))
  call monitor_set_single_value(2,force_visc(2))
  call monitor_set_single_value(3,force_visc(3))
  call monitor_set_single_value(4,force_pres(1))
  call monitor_set_single_value(5,force_pres(2))
  call monitor_set_single_value(6,force_pres(3))
  call monitor_set_single_value(7,force_visc(1)+force_pres(1))
  call monitor_set_single_value(8,force_visc(2)+force_pres(2))
  call monitor_set_single_value(9,force_visc(3)+force_pres(3))
  
  return
end subroutine ib_monitor

subroutine extend_cent(cent)
  use ib
  implicit none
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3) :: cent

  if (xper.eq.1.and.iproc.eq. 1 ) cent(imino :imin-1,:,:,1)=cent(imino :imin-1,:,:,1)-xL
  if (xper.eq.1.and.iproc.eq.npx) cent(imax+1:imaxo ,:,:,1)=cent(imax+1:imaxo ,:,:,1)+xL
  if (yper.eq.1.and.jproc.eq. 1 ) cent(:,jmino :jmin-1,:,2)=cent(:,jmino :jmin-1,:,2)-yL
  if (yper.eq.1.and.jproc.eq.npy) cent(:,jmax+1:jmaxo ,:,2)=cent(:,jmax+1:jmaxo ,:,2)+yL
  if (zper.eq.1.and.kproc.eq. 1 ) cent(:,:,kmino :kmin-1,3)=cent(:,:,kmino :kmin-1,3)-zL
  if (zper.eq.1.and.kproc.eq.npz) cent(:,:,kmax+1:kmaxo ,3)=cent(:,:,kmax+1:kmaxo ,3)+zL
end subroutine extend_cent


! ========================= !
! PDE-based extension in IB !
! ========================= !
subroutine multiphase_ib_extend(A,xdir,ydir,zdir,src,nit)
  use ib
  use memory
  implicit none
  
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: A,xdir,ydir,zdir,src
  integer :: i,j,k,n,nit
  real(WP) :: gradx,grady,gradz
  
  do n=1,nit
     
     ! Loop over domain and populate cell in IB
     tmp1=A
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ! Update only inside IBs
              if (vol(i,j,k).eq.0.0_WP.and.Gib(i,j,k).lt.0.0_WP) then
                 ! Get upwinded gradient of A
                 gradx=0.5_WP*(xdir(i,j,k)+abs(xdir(i,j,k)))*(A(i  ,j,k)-A(i-1,j,k))/(xm(i  )-xm(i-1))&
                      +0.5_WP*(xdir(i,j,k)-abs(xdir(i,j,k)))*(A(i+1,j,k)-A(i  ,j,k))/(xm(i+1)-xm(i  ))
                 grady=0.5_WP*(ydir(i,j,k)+abs(ydir(i,j,k)))*(A(i,j  ,k)-A(i,j-1,k))/(ym(j  )-ym(j-1))&
                      +0.5_WP*(ydir(i,j,k)-abs(ydir(i,j,k)))*(A(i,j+1,k)-A(i,j  ,k))/(ym(j+1)-ym(j  ))
                 gradz=0.5_WP*(zdir(i,j,k)+abs(zdir(i,j,k)))*(A(i,j,k  )-A(i,j,k-1))/(zm(k  )-zm(k-1))&
                      +0.5_WP*(zdir(i,j,k)-abs(zdir(i,j,k)))*(A(i,j,k+1)-A(i,j,k  ))/(zm(k+1)-zm(k  ))
                 ! Update A field
                 tmp1(i,j,k)=A(i,j,k)-0.5_WP*min_meshsize*(gradx+grady+gradz-src(i,j,k))
              end if
           end do
        end do
     end do
     A=tmp1
     
     ! Communicate field
     call boundary_update_border(A,'+','ym')
     call boundary_neumann_alldir(A)
     
  end do
  
  return
end subroutine multiphase_ib_extend
