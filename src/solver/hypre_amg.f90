module hypre_amg
  use pressure
  implicit none
  
  ! Number of cells 
  integer :: npcells
  integer :: npcells_
  integer, dimension(:), allocatable :: npcells_proc
  
  ! Unique index for all pressure points
  ! -1 : point is not used
  ! >0 : normal point
  integer, dimension(:,:,:), allocatable :: p_index
  
  ! Index of first and last local cell 
  integer :: p_min,p_max
  
  ! Arrays NECESSARY to pass arg to HYPRE
  ! Dont change that
  integer :: nrows, ncols
  integer,  dimension(:), allocatable :: cols
  integer,  dimension(:), allocatable :: rows
  real(WP), dimension(:), allocatable :: values

  ! HYPRE objects
  integer(kind=8), parameter :: HYPRE_PARCSR = 5555
  integer(kind=8) :: matrix
  integer(kind=8) :: rhs,sol
  integer(kind=8) :: solver
  integer(kind=8) :: par_matrix, par_rhs, par_sol
  
end module hypre_amg


! =========================================== !
! Prepare the global index of pressure points !
! Following ex5.c from HYPRE examples         !
! =========================================== !
subroutine hypre_amg_prepare
  use hypre_amg
  use metric_velocity_conv
  use parallel
  use masks
  implicit none
  integer  :: i,j,k,ierr
  integer  :: count
  
  ! Count number of local/global pressure cells
  npcells_ = 0
  do k = kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if(mask(i,j).eq.0 .and. sum(lap(i,j,k,:,0)).ne.0.0_WP) npcells_ = npcells_ + 1
        end do
     end do
  end do
  call parallel_sum(npcells_,npcells)
  
  ! Create an array with nbr cells per cpu
  allocate(npcells_proc(0:nproc))
  call MPI_allgather(npcells_,1,MPI_INTEGER,npcells_proc(1:nproc),1,MPI_INTEGER,comm,ierr)
  npcells_proc(0) = 0
  do i=1,nproc
     npcells_proc(i) = npcells_proc(i) + npcells_proc(i-1)
  end do
  
  ! Create a single index for all pressure points - default -1
  allocate(p_index(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  p_index = -1
  
  ! Start with an offset
  count = npcells_proc(irank-1)-1
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if(mask(i,j).eq.0 .and. sum(lap(i,j,k,:,0)).ne.0.0_WP) then 
              count = count + 1
              p_index(i,j,k) = count
           end if
        end do
     end do
  end do
  
  ! Take care of periodicity and domain decomposition
  call boundary_update_border_int(p_index,'+','ym')
  
  ! Get min/max
  p_max = -1
  p_min = maxval(p_index)
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (p_index(i,j,k).ne.-1) then
              p_min = min(p_min,p_index(i,j,k))
              p_max = max(p_max,p_index(i,j,k))
           end if
        end do
     end do
  end do
  
  return
end subroutine hypre_amg_prepare


! ================================== !
! Initialize the HYPRE AMG operators !
! ================================== !
subroutine hypre_amg_init_operator
  use hypre_amg
  use parallel
  use metric_velocity_conv
  implicit none
  
  integer :: ierr
  
  par_matrix = 0
  par_rhs = 0
  par_sol = 0
  
  ! Prepare the HYPRE AMG variables
  call hypre_amg_prepare
  
  ! Create the matrix in HYPRE
  call HYPRE_IJMatrixCreate(comm, p_min, p_max, p_min, p_max, matrix, ierr)
  call HYPRE_IJMatrixSetObjectType(matrix, HYPRE_PARCSR, ierr)
  call HYPRE_IJMatrixInitialize(matrix, ierr)
  
  ! Prepare RHS
  call HYPRE_IJVectorCreate(comm, p_min, p_max, rhs, ierr)
  call HYPRE_IJVectorSetObjectType(rhs, HYPRE_PARCSR, ierr)
  call HYPRE_IJVectorInitialize(rhs, ierr)
  call HYPRE_IJVectorGetObject(rhs, par_rhs, ierr)
  
  ! Create solution vector
  call HYPRE_IJVectorCreate(comm, p_min, p_max, sol, ierr)
  call HYPRE_IJVectorSetObjectType(sol, HYPRE_PARCSR, ierr)
  call HYPRE_IJVectorInitialize(sol, ierr)
  call HYPRE_IJVectorGetObject(sol, par_sol, ierr)
  
  ! Allocate work arrays
  allocate(rows  (npcells_))
  allocate(values(npcells_))
  
  return
end subroutine hypre_amg_init_operator


! ========================= !
! Initialize the AMG solver !
! ========================= !
subroutine hypre_amg_init_solver
  use hypre_amg
  use parallel
  use metric_velocity_conv
  implicit none
  
  integer :: ierr
  
  ! Create the solver
  solver = 0
  call HYPRE_BoomerAMGCreate(solver, ierr)
  
  ! Set some parameters
  call HYPRE_BoomerAMGSetPrintLevel (solver, 0, ierr)      ! print solve info + parameters
  call HYPRE_BoomerAMGSetInterpType (solver, 0, ierr)      ! interpolation
  call HYPRE_BoomerAMGSetCycleType  (solver, 1, ierr)      ! type of cycles (V:1, W:2)
  call HYPRE_BoomerAMGSetCoarsenType(solver, 6, ierr)      ! Falgout coarsening
  call HYPRE_BoomerAMGSetMeasureType(solver, 1, ierr)      ! global measure
  call HYPRE_BoomerAMGSetRelaxType  (solver, 6, ierr)      ! hybrid symmetric Gauss-Seidel
  call HYPRE_BoomerAMGSetNumSweeps  (solver, 1, ierr)      ! Sweeeps on each level
  call HYPRE_BoomerAMGSetMaxLevels  (solver, 20,ierr)      ! maximum number of levels
  call HYPRE_BoomerAMGSetMaxIter(solver, max_iter, ierr)   ! maximum nbr of iter
  call HYPRE_BoomerAMGSetTol(solver, cvg, ierr)            ! conv. tolerance
  
  return
end subroutine hypre_amg_init_solver
  

! ============================= !
! Update the HYPRE AMG operator !
! ============================= !
subroutine hypre_amg_update_operator
  use hypre_amg
  use parallel
  use metric_velocity_conv
  use fourier
  implicit none
  
  integer :: i,j,k,st,ierr
  integer  :: ind,tmpi,nst
  real(WP) :: tmpr
  integer,  dimension(:), allocatable :: cols_
  integer,  dimension(:), allocatable :: rows_
  real(WP), dimension(:), allocatable :: values_
    
  par_matrix = 0
  
  ! Allocate the temporary arrays
  allocate(rows_  (1))
  allocate(cols_  (3*2*stcp+1))
  allocate(values_(3*2*stcp+1))
  
  ! Fill up the matrix, one row at a time
  nrows = 1

  ! Fill up the matrix coefficients
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           
           if (p_index(i,j,k).ne.-1) then 
              nst = 0
              
              ! Diagonal
              nst = nst + 1
              cols_  (nst) = p_index(i,j,k)
              values_(nst) = sum(lap(i,j,k,:,0))
              
              do st = -stcp,+stcp
                 if (st.eq.0) cycle
                 
                 ! Left-Right
                 if (nx.ne.1 .and. .not.(pressure_fft.and.fft_x)) then
                    if(p_index(i+st,j,k).ne.-1) then                     
                       nst = nst + 1
                       cols_  (nst) = p_index(i+st,j,k)
                       values_(nst) = lap(i,j,k,1,st)
                    end if
                 end if
                 
                 ! Top-Bottom
                 if (ny.ne.1 .and. .not.(pressure_fft.and.fft_y)) then
                    if (p_index(i,j+st,k).ne.-1) then
                       nst = nst + 1
                       cols_  (nst) = p_index(i,j+st,k)
                       values_(nst) = lap(i,j,k,2,st)
                    end if
                 end if
                 
                 ! Front-Back
                 if (nz.ne.1 .and. .not.(pressure_fft.and.fft_z)) then
                    if (p_index(i,j,k+st).ne.-1) then
                       nst = nst + 1
                       cols_  (nst) = p_index(i,j,k+st)
                       values_(nst) = lap(i,j,k,3,st)
                    end if
                 end if
                 
              end do
              
              ! Sort the points
              do st=1,nst
                 ind = st + minloc(cols_(st:nst),1) - 1
                 tmpr = values_(st)
                 values_(st) = values_(ind)
                 values_(ind) = tmpr
                 tmpi = cols_(st)
                 cols_(st) = cols_(ind)
                 cols_(ind) = tmpi
              end do
              
              ncols = nst
              rows_ = p_index(i,j,k)
              call HYPRE_IJMatrixSetValues(matrix, nrows, ncols, rows_, &
                   cols_, values_, ierr)

           end if
        end do
     end do
  end do
  
  ! Assemble the matrix
  call HYPRE_IJMatrixAssemble(matrix, ierr)
  call HYPRE_IJMatrixGetObject(matrix, par_matrix, ierr)
  
  ! Deallocate temporary arrays
  deallocate(rows_)
  deallocate(cols_)
  deallocate(values_)
  
  return
end subroutine hypre_amg_update_operator


! ================================ !
! Prepare the solver for HYPRE AMG !
! ================================ !
subroutine hypre_amg_solver_update
  use hypre_amg
  implicit none
  integer :: ierr
  
  ! Update the operator
  call hypre_amg_update_operator
  
  ! Now setup
  call HYPRE_BoomerAMGSetup(solver, par_matrix, par_rhs, par_sol, ierr)
  
  return
end subroutine hypre_amg_solver_update


! ========================= !
! Transfer the RHS to HYPRE !
! ========================= !
subroutine hypre_amg_rhs_transfer
  use hypre_amg
  use metric_velocity_conv
  use masks
  implicit none
  integer :: i,j,k,ierr
  integer :: count
  
  ! Set the RHS
  count = 0
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (mask(i,j).eq.0 .and. sum(lap(i,j,k,:,0)).ne.0.0_WP) then
              count = count + 1
              rows  (count) = p_index(i,j,k)
              values(count) = RP(i,j,k)
           end if
        end do
     end do
  end do
  call HYPRE_IJVectorSetValues(rhs, npcells_, rows, values, ierr)
  call HYPRE_IJVectorAssemble (rhs, ierr)
  
  return
end subroutine hypre_amg_rhs_transfer


! ======================== !
! Transfer the IG to HYPRE !
! ======================== !
subroutine hypre_amg_set_ig
  use hypre_amg
  use metric_velocity_conv
  use masks
  implicit none
  integer :: i,j,k,ierr
  integer :: count
  
  ! Set the initial guess
  count = 0
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (mask(i,j).eq.0 .and. sum(lap(i,j,k,:,0)).ne.0.0_WP) then
              count = count + 1
              rows  (count) = p_index(i,j,k)
              values(count) = DP(i,j,k)
           end if
        end do
     end do
  end do
  call HYPRE_IJVectorSetValues(sol, npcells_, rows, values, ierr)
  call HYPRE_IJVectorAssemble (sol, ierr)
  
  return
end subroutine hypre_amg_set_ig


! ================================ !
! Solve the problem with HYPRE AMG !
! ================================ !
subroutine hypre_amg_solve
  use hypre_amg
  use time_info
  implicit none
  integer :: ierr
  
  ! Prepare the solver
  if (pressure_cst_lap.eq.2 .or. &
      pressure_cst_lap.eq.1.and.niter.eq.1) call hypre_amg_solver_update
  
  ! Now solve
  call HYPRE_BoomerAMGSolve(solver, par_matrix, par_rhs, par_sol, ierr)

  ! Run info - needed logging turned on
  call HYPRE_BoomerAMGGetNumIterations(solver, it_p, ierr)
  call HYPRE_BoomerAMGGetFinalReltvRes(solver, max_resP, ierr)
  
  return
end subroutine hypre_amg_solve


! ================================ !
! Transfer the solution from HYPRE !
! ================================ !
subroutine hypre_amg_sol_transfer
  use hypre_amg
  use metric_velocity_conv
  use masks
  implicit none
  integer :: i,j,k,ierr
  integer :: count
  
  call HYPRE_IJVectorGetValues(sol, npcells_, rows, values, ierr)
  count = 0
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (mask(i,j).eq.0 .and. sum(lap(i,j,k,:,0)).ne.0.0_WP) then
              count = count + 1
              DP(i,j,k) = values(count)
           end if
        end do
     end do
  end do
  
  return
end subroutine hypre_amg_sol_transfer
