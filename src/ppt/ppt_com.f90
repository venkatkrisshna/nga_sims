! ===================================== !
! Various tracer communication routines !
! ===================================== !
module ppt_com
  use ppt
  use ppt_mod
  use parallel
  implicit none
  
  ! MPI structure
  integer :: MPI_TRACER
  integer :: SIZE_MPI_TRACER
  
end module ppt_com


! ============================== !
! Tracer interprocessor exchange !
! ============================== !
subroutine ppt_communication
  use ppt_com
  implicit none
  
  integer, dimension(nproc) :: who_send,who_recv,counter
  integer :: i,nb_recv,nb_send,ntracer_old
  integer :: rank_send,rank_recv,tracer_rank,ierr,status(MPI_STATUS_SIZE)
  type(tracer_type), dimension(:,:), allocatable :: buf_send
  
  ! Recycle
  call ppt_recycle
  
  ! Prepare information about who sends what to whom
  who_send=0
  do i=1,ntracer_
     call ppt_proc(tracer(i)%i,tracer(i)%j,tracer(i)%k,tracer_rank)
     who_send(tracer_rank)=who_send(tracer_rank)+1
  end do
  ! Remove the diagonal
  who_send(irank)=0

  ! Prepare information about who receives what from whom
  do rank_recv=1,nproc
     call MPI_gather(who_send(rank_recv),1,MPI_INTEGER,who_recv,1, &
          MPI_INTEGER,rank_recv-1,comm,ierr)
  end do

  ! Prepare the buffers
  nb_send=maxval(who_send)
  nb_recv=sum(who_recv)
  
  ! Allocate buffers to send tracers
  allocate(buf_send(nproc,nb_send))
  
  ! Find and pack the tracers to be sent
  ! Prepare the counter
  counter(:)=0
  do i=1,ntracer_
     ! Get the cpu
     call ppt_proc(tracer(i)%i,tracer(i)%j,tracer(i)%k,tracer_rank)
     if (tracer_rank .NE. irank) then
        ! Prepare for sending
        counter(tracer_rank)=counter(tracer_rank)+1
        buf_send(tracer_rank,counter(tracer_rank))=tracer(i)
        ! Need to remove the tracer
        tracer(i)%stop = 1
     end if
  end do
  
  ! Everybody resizes
  ntracer_old = ntracer_
  call ppt_resize(ntracer_+nb_recv)
  
  ! We just loop through the CPUs, pack tracers in buf_send, send, unpack
  do rank_send=1,nproc
     if (irank.eq.rank_send) then
        ! I'm the sender of tracers
        do rank_recv=1,nproc
           if(who_send(rank_recv).gt.0) then
              call MPI_send(buf_send(rank_recv,:),who_send(rank_recv), &
                   MPI_TRACER,rank_recv-1,0,comm,ierr)
           end if
        end do
     else
        ! I'm not the sender, I receive
        if (who_recv(rank_send).gt.0) then
           call MPI_recv(tracer(ntracer_old+1:ntracer_old+who_recv(rank_send)),who_recv(rank_send), &
                MPI_TRACER,rank_send-1,0,comm,status,ierr)
           ntracer_old = ntracer_old+who_recv(rank_send)
        end if
     end if
  end do
  
  ! Done, deallocate
  deallocate(buf_send)
  
  ! Recycle
  call ppt_recycle
  
  return
end subroutine ppt_communication


!!$! ============================== !
!!$! MPI Tracer datatype creation   !
!!$! Old MPI-1.2 standard - doesn't !
!!$! account for 64 bits machines   !
!!$! ============================== !
!!$subroutine ppt_mpi_prepare
!!$  use ppt_com
!!$  implicit none
!!$  
!!$  integer, dimension(9) :: types,lengths,displacement
!!$  integer :: base,ierr
!!$  
!!$  ! Create the MPI structure to send tracers
!!$  types(1:3) = MPI_REAL_WP
!!$  types(4:8) = MPI_INTEGER
!!$  types(9)    = MPI_UB
!!$  lengths(:)   = 1
!!$
!!$  ! Hard-code displacement here
!!$  displacement( 1)=  0
!!$  displacement( 2)=  8
!!$  displacement( 3)= 16
!!$  displacement( 4)= 24
!!$  displacement( 5)= 28
!!$  displacement( 6)= 32
!!$  displacement( 7)= 36
!!$  displacement( 8)= 40
!!$  displacement( 9)= 44
!!$  
!!$  ! Finalize by creating and commiting the new type
!!$  call mpi_type_create_struct(9,lengths,displacement,types,MPI_TRACER,ierr)
!!$  call MPI_Type_commit(MPI_TRACER,ierr)
!!$  
!!$  ! If problem, say it
!!$  if (ierr.ne.0) call die("Problem with MPI_TRACER")
!!$  
!!$  ! Get the size of this type
!!$  call MPI_type_size(MPI_TRACER,SIZE_MPI_TRACER,ierr)
!!$  
!!$  return
!!$end subroutine ppt_mpi_prepare

! ============================= !
! MPI tracere datatype creation !
! New MPI-2 standard - accounts !
! for 64 bits machines          !
! ============================= !
subroutine ppt_mpi_prepare
  use ppt_com
  implicit none
  
  integer(KIND=MPI_ADDRESS_KIND), dimension(10) :: displacement
  integer, dimension(10) :: types,lengths
  integer(KIND=MPI_ADDRESS_KIND) :: base
  integer :: ierr
  
  types(1:4) = MPI_REAL_WP
  types(5:9) = MPI_INTEGER
  types(10)    = MPI_UB
  lengths(:)   = 1
  
  ! Count the displacement for this structure
  call MPI_get_address(tracer(1),         base,             ierr)
  ! Position - cartesian
  call MPI_get_address(tracer(1)%x,       displacement(1),  ierr)
  call MPI_get_address(tracer(1)%y,       displacement(2),  ierr)
  call MPI_get_address(tracer(1)%z,       displacement(3),  ierr)
  ! Time injected
  call MPI_get_address(tracer(1)%t0,      displacement(4),  ierr)
  ! Position - indices
  call MPI_get_address(tracer(1)%i,       displacement(5), ierr)
  call MPI_get_address(tracer(1)%j,       displacement(6), ierr)
  call MPI_get_address(tracer(1)%k,       displacement(7), ierr)
  ! Tracer id
  call MPI_get_address(tracer(1)%id,      displacement(8), ierr)
  ! Alive/dead flag
  call MPI_get_address(tracer(1)%stop,    displacement(9), ierr)
  call MPI_get_address(tracer(2),         displacement(10), ierr)
  displacement=displacement-base
  
  ! Finalize by creating and commiting the new type
  call MPI_Type_create_struct(10,lengths,displacement,types,MPI_TRACER,ierr)
  call MPI_Type_commit(MPI_TRACER,ierr)
  
  ! If problem, say it
  if (ierr.ne.0) call die("Problem with MPI_TRACER")
  
  ! Get the size of this type
  call MPI_type_size(MPI_TRACER,SIZE_MPI_TRACER,ierr)
  
  return
end subroutine ppt_mpi_prepare


! ===================================== !
! Tracer communication for collisions !
! ===================================== !
subroutine ppt_communication_gp
  use ppt_com
  implicit none
    
  ! Clean ghost tracers structure
  call ppt_resize_gp(0)
  ntracer_gp_=0
  
  ! Communicate
  call ppt_communication_border_x(1)
  call ppt_communication_border_y(1)
  call ppt_communication_border_z(1)
  
  return
end subroutine ppt_communication_gp


! =================================== !
! Tracer communication in x direction !
! =================================== !
subroutine ppt_communication_border_x(no)
  use ppt_com
  use parallel
  implicit none
  
  ! Input parameters
  integer, intent(in) :: no
  
  ! Local variables
  type(tracer_type), dimension(:), pointer :: tosend
  type(tracer_type), dimension(:), pointer :: torecv
  integer :: nsend,nrecv
  integer :: istatus(MPI_STATUS_SIZE)
  integer :: icount,isource,idest
  integer :: i,ierr,n
  
  ! Count number of tracers to send left
  nsend=0
  do n=1,ntracer_
     if (tracer(n)%i.lt.imin_+no) nsend=nsend+1
  end do
  
  ! Allocate send buffer
  allocate(tosend(nsend))
  
  ! Copy tracers in buffer
  nsend=0
  do n=1,ntracer_
     if (tracer(n)%i.lt.imin_+no) then
        nsend=nsend+1
        tosend(nsend)=tracer(n)
        if (xper.eq.1 .and. tosend(nsend)%i.lt.imin+no) then
           tosend(nsend)%x=tosend(nsend)%x+xL
           tosend(nsend)%i=tosend(nsend)%i+nx
        end if
     end if
  end do
 
  ! Communicate sizes
  nrecv=0
  call MPI_CART_SHIFT(comm,0,-1,isource,idest,ierr)
  call MPI_SENDRECV(nsend,1,MPI_INTEGER,idest,0,nrecv,1,MPI_INTEGER,isource,0,comm,istatus,ierr)
  
  ! Allocate recv buffer
  allocate(torecv(nrecv))

  ! Communicate
  call MPI_SENDRECV(tosend,nsend,MPI_TRACER,idest,0,torecv,nrecv,MPI_TRACER,isource,0,comm,istatus,ierr)
     
  ! Add to ghost tracers
  if (nrecv.gt.0) then
     call ppt_resize_gp(ntracer_gp_+nrecv)
     tracer_gp(ntracer_gp_+1:ntracer_gp_+nrecv)=torecv(1:nrecv)
     ntracer_gp_=ntracer_gp_+nrecv
  end if

  ! Count number of tracers to send right
  nsend=0
  do n=1,ntracer_
     if (tracer(n)%i.gt.imax_-no) nsend=nsend+1
  end do

  ! Allocate send buffer
  if (associated(tosend)) deallocate(tosend)
  allocate(tosend(nsend))

  ! Copy tracers in buffer
  nsend=0
  do n=1,ntracer_
     if (tracer(n)%i.gt.imax_-no) then
        nsend=nsend+1
        tosend(nsend)=tracer(n)
        if (xper.eq.1 .and. tosend(nsend)%i.gt.imax-no) then
           tosend(nsend)%x=tosend(nsend)%x-xL
           tosend(nsend)%i=tosend(nsend)%i-nx
        end if
     end if
  end do

  ! Communicate sizes
  nrecv=0
  call MPI_CART_SHIFT(comm,0,+1,isource,idest,ierr)
  call MPI_SENDRECV(nsend,1,MPI_INTEGER,idest,2,nrecv,1,MPI_INTEGER,isource,2,comm,istatus,ierr)

  ! Allocate recv buffer
  deallocate(torecv)
  allocate(torecv(nrecv))
 
  ! Communicate
  call MPI_SENDRECV(tosend,nsend,MPI_TRACER,idest,0,torecv,nrecv,MPI_TRACER,isource,0,comm,istatus,ierr)

  ! Add to ghost tracers
  if (nrecv.gt.0) then
     call ppt_resize_gp(ntracer_gp_+nrecv)
     tracer_gp(ntracer_gp_+1:ntracer_gp_+nrecv)=torecv(1:nrecv)
     ntracer_gp_=ntracer_gp_+nrecv
  end if

  ! Deallocate
  deallocate(tosend);nullify(tosend)
  deallocate(torecv);nullify(torecv)
  
  return
end subroutine ppt_communication_border_x


! =================================== !
! Tracer communication in y direction !
! =================================== !
subroutine ppt_communication_border_y(no)
  use ppt_com
  use parallel
  implicit none
  
  ! Input parameters
  integer, intent(in) :: no
  
  ! Local variables
  type(tracer_type), dimension(:), pointer :: tosend
  type(tracer_type), dimension(:), pointer :: torecv
  integer :: nsend,nrecv
  integer :: istatus(MPI_STATUS_SIZE)
  integer :: icount,isource,idest
  integer :: i,ierr,n
  
  ! Count number of tracers to send left
  nsend=0
  do n=1,ntracer_
     if (tracer(n)%j.lt.jmin_+no) nsend=nsend+1
  end do
  do n=1,ntracer_gp_
     if (tracer_gp(n)%j.lt.jmin_+no) nsend=nsend+1
  end do

  ! Allocate send buffer
  allocate(tosend(nsend))
  
  ! Copy tracers in buffer
  nsend=0
  do n=1,ntracer_
     if (tracer(n)%j.lt.jmin_+no) then
        nsend=nsend+1
        tosend(nsend)=tracer(n)
        if (yper.eq.1 .and. tosend(nsend)%j.lt.jmin+no) then
           tosend(nsend)%y=tosend(nsend)%y+yL
           tosend(nsend)%j=tosend(nsend)%j+ny
        end if
     end if
  end do
  do n=1,ntracer_gp_
     if (tracer_gp(n)%j.lt.jmin_+no) then
        nsend=nsend+1
        tosend(nsend)=tracer_gp(n)
        if (yper.eq.1 .and. tosend(nsend)%j.lt.jmin+no) then
           tosend(nsend)%y=tosend(nsend)%y+yL
           tosend(nsend)%j=tosend(nsend)%j+ny
        end if
     end if
  end do
  
  ! Communicate sizes
  nrecv=0
  call MPI_CART_SHIFT(comm,1,-1,isource,idest,ierr)
  call MPI_SENDRECV(nsend,1,MPI_INTEGER,idest,4,nrecv,1,MPI_INTEGER,isource,4,comm,istatus,ierr)
  
  ! Allocate recv buffer
  allocate(torecv(nrecv))


  ! Communicate
  call MPI_SENDRECV(tosend,nsend,MPI_TRACER,idest,0,torecv,nrecv,MPI_TRACER,isource,0,comm,istatus,ierr)

  ! Add to ghost tracers
  call ppt_resize_gp(ntracer_gp_+nrecv)
  if (nrecv.gt.0) tracer_gp(ntracer_gp_+1:ntracer_gp_+nrecv)=torecv(1:nrecv)
  ntracer_gp_=ntracer_gp_+nrecv
  
  ! Count number of tracers to send right
  nsend=0
  do n=1,ntracer_
     if (tracer(n)%j.gt.jmax_-no) nsend=nsend+1
  end do
  do n=1,ntracer_gp_-nrecv
     if (tracer_gp(n)%j.gt.jmax_-no) nsend=nsend+1
  end do
  
  ! Allocate send buffer
  deallocate(tosend)
  allocate(tosend(nsend))
  
  ! Copy tracers in buffer
  nsend=0
  do n=1,ntracer_
     if (tracer(n)%j.gt.jmax_-no) then
        nsend=nsend+1
        tosend(nsend)=tracer(n)
        if (yper.eq.1 .and. tosend(nsend)%j.gt.jmax-no) then
           tosend(nsend)%y=tosend(nsend)%y-yL
           tosend(nsend)%j=tosend(nsend)%j-ny
        end if
     end if
  end do
  do n=1,ntracer_gp_-nrecv
     if (tracer_gp(n)%j.gt.jmax_-no) then
        nsend=nsend+1
        tosend(nsend)=tracer_gp(n)
        if (yper.eq.1 .and. tosend(nsend)%j.gt.jmax-no) then
           tosend(nsend)%y=tosend(nsend)%y-yL
           tosend(nsend)%j=tosend(nsend)%j-ny
        end if
     end if
  end do
  
  ! Communicate sizes
  nrecv=0
  call MPI_CART_SHIFT(comm,1,+1,isource,idest,ierr)
  call MPI_SENDRECV(nsend,1,MPI_INTEGER,idest,6,nrecv,1,MPI_INTEGER,isource,6,comm,istatus,ierr)
  
  ! Allocate recv buffer
  deallocate(torecv)
  allocate(torecv(nrecv))

  ! Communicate     
  call MPI_SENDRECV(tosend,nsend,MPI_TRACER,idest,0,torecv,nrecv,MPI_TRACER,isource,0,comm,istatus,ierr)

  ! Add to ghost tracers
  call ppt_resize_gp(ntracer_gp_+nrecv)
  if (nrecv.gt.0) tracer_gp(ntracer_gp_+1:ntracer_gp_+nrecv)=torecv(1:nrecv)
  ntracer_gp_=ntracer_gp_+nrecv
  
  ! Deallocate
  deallocate(tosend);nullify(tosend)
  deallocate(torecv);nullify(torecv)
  
  return
end subroutine ppt_communication_border_y


! =================================== !
! Tracer communication in z direction !
! =================================== !
subroutine ppt_communication_border_z(no)
  use ppt_com
  use parallel
  implicit none
  
  ! Input parameters
  integer, intent(in) :: no

  ! Local variables
  type(tracer_type), dimension(:), pointer :: tosend
  type(tracer_type), dimension(:), pointer :: torecv
  integer :: nsend,nrecv
  integer :: istatus(MPI_STATUS_SIZE)
  integer :: icount,isource,idest
  integer :: i,ierr,n
  
  ! Count number of tracers to send left
  nsend=0
  do n=1,ntracer_
     if (tracer(n)%k.lt.kmin_+no) nsend=nsend+1
  end do
  do n=1,ntracer_gp_
     if (tracer_gp(n)%k.lt.kmin_+no) nsend=nsend+1
  end do

  ! Allocate send buffer
  allocate(tosend(nsend))
  
  ! Copy tracers in buffer
  nsend=0
  do n=1,ntracer_
     if (tracer(n)%k.lt.kmin_+no) then
        nsend=nsend+1
        tosend(nsend)=tracer(n)
        if (zper.eq.1 .and. tosend(nsend)%k.lt.kmin+no) then
           tosend(nsend)%z=tosend(nsend)%z+zL
           tosend(nsend)%k=tosend(nsend)%k+nz
        end if
     end if
  end do
  do n=1,ntracer_gp_
     if (tracer_gp(n)%k.lt.kmin_+no) then
        nsend=nsend+1
        tosend(nsend)=tracer_gp(n)
        if (zper.eq.1 .and. tosend(nsend)%k.lt.kmin+no) then
           tosend(nsend)%z=tosend(nsend)%z+zL
           tosend(nsend)%k=tosend(nsend)%k+nz
        end if
     end if
  end do
  
  ! Communicate sizes
  nrecv=0
  call MPI_CART_SHIFT(comm,2,-1,isource,idest,ierr)
  call MPI_SENDRECV(nsend,1,MPI_INTEGER,idest,8,nrecv,1,MPI_INTEGER,isource,8,comm,istatus,ierr)
  
  ! Allocate recv buffer
  allocate(torecv(nrecv))

  ! Communicate     
  call MPI_SENDRECV(tosend,nsend,MPI_TRACER,idest,0,torecv,nrecv,MPI_TRACER,isource,0,comm,istatus,ierr)

  ! Add to ghost tracers
  call ppt_resize_gp(ntracer_gp_+nrecv)
  if (nrecv.gt.0) tracer_gp(ntracer_gp_+1:ntracer_gp_+nrecv)=torecv(1:nrecv)
  ntracer_gp_=ntracer_gp_+nrecv
  
  ! Count number of tracers to send right
  nsend=0
  do n=1,ntracer_
     if (tracer(n)%k.gt.kmax_-no) nsend=nsend+1
  end do
  do n=1,ntracer_gp_-nrecv
     if (tracer_gp(n)%k.gt.kmax_-no) nsend=nsend+1
  end do

  ! Allocate send buffer
  deallocate(tosend)
  allocate(tosend(nsend))
  
  ! Copy tracers in buffer
  nsend=0
  do n=1,ntracer_
     if (tracer(n)%k.gt.kmax_-no) then
        nsend=nsend+1
        tosend(nsend)=tracer(n)
        if (zper.eq.1 .and. tosend(nsend)%k.gt.kmax-no) then
           tosend(nsend)%z=tosend(nsend)%z-zL
           tosend(nsend)%k=tosend(nsend)%k-nz
        end if
     end if
  end do
  do n=1,ntracer_gp_-nrecv
     if (tracer_gp(n)%k.gt.kmax_-no) then
        nsend=nsend+1
        tosend(nsend)=tracer_gp(n)
        if (zper.eq.1 .and. tosend(nsend)%k.gt.kmax-no) then
           tosend(nsend)%z=tosend(nsend)%z-zL
           tosend(nsend)%k=tosend(nsend)%k-nz
        end if
     end if
  end do
  
  ! Communicate sizes
  nrecv=0
  call MPI_CART_SHIFT(comm,2,+1,isource,idest,ierr)
  call MPI_SENDRECV(nsend,1,MPI_INTEGER,idest,10,nrecv,1,MPI_INTEGER,isource,10,comm,istatus,ierr)

  ! Allocate recv buffer
  deallocate(torecv)
  allocate(torecv(nrecv))

  ! Communicate
  call MPI_SENDRECV(tosend,nsend,MPI_TRACER,idest,0,torecv,nrecv,MPI_TRACER,isource,0,comm,istatus,ierr)

  ! Add to ghost tracers
  call ppt_resize_gp(ntracer_gp_+nrecv)
  if (nrecv.gt.0) tracer_gp(ntracer_gp_+1:ntracer_gp_+nrecv)=torecv(1:nrecv)
  ntracer_gp_=ntracer_gp_+nrecv
  
  ! Deallocate
  deallocate(tosend);nullify(tosend)
  deallocate(torecv);nullify(torecv)
  
  return
end subroutine ppt_communication_border_z
