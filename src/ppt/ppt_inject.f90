! ================ !
! Tracer injection !
! ================ !
module ppt_inject
  use ppt
  implicit none
  
  ! Injection parameters
  character(len=str_medium) :: tracer_injection
  
  ! Bulk uniform injection
  integer :: ninject,inject_save
  real(WP) :: inject_freq

contains
  
end module ppt_inject


! ============================ !
! Initialize injection routine !
! ============================ !
subroutine ppt_inject_init
  use ppt_inject
  use parser
  use parallel
  use time_info
  implicit none
  
  integer :: i
  
  ! Injection parameters
  call parser_read('Tracer injection',tracer_injection,'none')
  select case (trim(tracer_injection))
  case ('none')
     ! Nothing to do
  case ('bulk')
     ! Injection frequency
     call parser_read('Tracer frequency',inject_freq)
     ! Number of tracers to inject
     call parser_read('Tracers per inject',ninject)
     inject_save = int(time/inject_freq)
  case default
     call die('Unknown tracer injection type.')
  end select
  
  return
end subroutine ppt_inject_init

! ========================= !
! Add new tracers uniformly !
! ========================= !
subroutine ppt_inject_bulk
  use ppt_inject
  use time_info
  use parallel
  use ib
  implicit none
  
  integer :: n,nadd,count,ntracer0_,ibuf
  integer :: maxid_,maxid

  ! Initial number of tracers added
  ntracer_in = 0

  ! Add tracers if needed
  if (int(time/inject_freq).NE.inject_save) then
     inject_save = int(time/inject_freq)

     ! Initial number of tracers
     ntracer0_ = ntracer_

     ! Determine id to assign to tracer
     maxid_=0
     do n=1,ntracer_
        maxid_=max(maxid_,tracer(n)%id)
     end do
     call parallel_max(maxid_,maxid)

     do while (ntracer_in .lt. ninject)

        ! Root process creates temporary tracer
        if (irank.eq.iroot) then
           ! Increment counter
           count = ntracer0_ + 1
           ! Create space for new tracer
           call ppt_resize(count)
           ! Assign time at injection
           tracer(count)%t0 = time
           ! Assign id to tracer
           tracer(count)%id = int(maxid) + ntracer_in + 1
           ! Give a position at the injector to the tracer
           call ppt_get_position(tracer(count)%x,tracer(count)%y,tracer(count)%z)
           ! Localize the tracer
           call ppt_locate(tracer(count)%x,tracer(count)%y,tracer(count)%z,tracer(count)%i,tracer(count)%j,tracer(count)%k)
           ! Make it an "official" tracer
           tracer(count)%stop=0
        end if

        ! Communicate tracers
        call ppt_communication

        ! Loop through newly created particles
        nadd=0
        do n=ntracer0_+1,ntracer_
           ! Check if in bound
           if ((vol(tracer(n)%i,tracer(n)%j,tracer(n)%k).le.0.0_WP).or.(use_ib.and.&
                get_Gib((/tracer(n)%x,tracer(n)%y,tracer(n)%z/),tracer(n)%i,tracer(n)%j,tracer(n)%k).le.0.0_WP)) then
              ! Remove tracer
              tracer(n)%stop=1
           else
              ! Increment the counter
              nadd=nadd+1
           end if
        end do
           
        ! Clean up tracers
        call ppt_recycle
        
        ! Update initial ntracer
        ntracer0_=ntracer_

        ! Total number of tracers injected
        call parallel_sum(nadd,ibuf)
        ntracer_in = ntracer_in + ibuf

     end do

  end if
  
contains
  
  ! Position for bulk injection of tracers
  subroutine ppt_get_position(xp,yp,zp)
    use ppt_inject
    use random
    implicit none
    
    real(WP), intent(out) :: xp,yp,zp
    real(WP) :: rand
    integer :: ip,jp,kp
    
    ! Random position (account for volume fraction)
    xp = x(imin)
    if (ny.eq.1) then
       yp = ym(jmin_)
    else
       call random_number(rand)
       yp = y(jmin)+rand*yL
    end if
    if (nz.eq.1) then
       zp = zm(kmin_)
    else
       call random_number(rand)
       zp = z(kmin)+rand*zL
    end if
    ! Localize the tracer
    call ppt_locate(xp,yp,zp,ip,jp,kp)
    
    return
  end subroutine ppt_get_position
  
end subroutine ppt_inject_bulk


! ================== !
! Inject new tracers !
! ================== !
subroutine ppt_inject_step
  use ppt_inject
  implicit none
  
  ! Choose injection type
  select case (trim(tracer_injection))
  case ('none')
     return
  case ('bulk')
     call ppt_inject_bulk
  end select
  
  return
end subroutine ppt_inject_step
