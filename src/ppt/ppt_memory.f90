! ============================================= !
! Memory routines for passive particle tracking !
! Provides a smarter handling of the memory     !
! - limits the number of de/allocations         !
! - handles fully dynamic size                  !
! ============================================= !
module ppt_memory
  use geometry
  use partition
  use parallel
  use ppt
  implicit none
  !
end module ppt_memory


! ===================================== !
! Extremely basic sorting routine       !
! Should be replaced by heap/quick sort !
! ===================================== !
subroutine ppt_recycle
  use ppt_memory
  implicit none
  
  integer :: i,new_size
  
  ! Compact real tracers at the beginning of the array
  new_size = 0
  if (associated(tracer)) then
     do i=1,size(tracer,dim=1)
        if (tracer(i)%stop.eq.0) then
           new_size = new_size + 1
           if (i .ne. new_size) then
              tracer(new_size)=tracer(i)
              tracer(i)%stop = 1
           end if
        end if
     end do
  end if
  
  ! Resize array to new_size
  call ppt_resize(new_size)

  ! Update sizes
  call ppt_update_size(new_size)
  
 return
end subroutine ppt_recycle


! ======================================= !
! Update the number of tracers everywhere !
! ======================================= !
subroutine ppt_update_size(n)
  use ppt_memory
  implicit none
  
  integer, intent(in) :: n
  integer :: ierr
  
  ! Update sizes
  ntracer_ = n
  call MPI_allgather(ntracer_,1,MPI_INTEGER,ntracer_proc,1,MPI_INTEGER,comm,ierr)
  ntracer = sum(ntracer_proc)
  
  return
end subroutine ppt_update_size


! =========================================== !
! Resize the storage array to an optimal size !
! in the sense of size and frequency of       !
! allocation/deallocation                     !
! =========================================== !
subroutine ppt_resize(n)
  use ppt_memory
  implicit none
  
  integer, intent(in) :: n
  type(tracer_type), dimension(:), pointer :: tracer_temp
  integer :: n_now,n_new,i
  real(WP), parameter :: coeff_up   = 1.3_WP
  real(WP), parameter :: coeff_down = 0.7_WP
  real(WP) :: up,down
  
  ! Resize tracer array to size n
  if (.NOT.associated(tracer)) then
     ! tracer is of size 0
     if (n.eq.0) then
        ! Nothing to do, that's what we want
     else
        ! Allocate directly of size n
        allocate(tracer(n))
        tracer(1:n)%stop=1
     end if
  else if (n.eq.0) then
     ! tracer is associated, be we want to empty it
     deallocate(tracer)
     nullify(tracer)
  else
     ! Update non zero size to another non zero size
     n_now = size(tracer,dim=1)
     up  =real(n_now,WP)*coeff_up
     down=real(n_now,WP)*coeff_down
     if (n.gt.n_now) then
        ! Increase from n_now to n_new
        n_new = max(n,int(up))
        allocate(tracer_temp(n_new))
        do i=1,n_now
           tracer_temp(i) = tracer(i)
        end do
        deallocate(tracer)
        nullify(tracer)
        tracer => tracer_temp
        tracer(n_now+1:n_new)%stop=1
     else if (n.lt.int(down)) then
        ! Decrease from n_now to n_new
        allocate(tracer_temp(n))
        do i=1,n
           tracer_temp(i) = tracer(i)
        end do
        deallocate(tracer)
        nullify(tracer)
        tracer => tracer_temp
     end if
  end if
  
  return
end subroutine ppt_resize


! ======================================= !
! Resizing routine for ghost tracer array !
! ======================================= !
subroutine ppt_resize_gp(n)
  use ppt_memory
  implicit none
  
  integer, intent(in) :: n
  type(tracer_type), dimension(:), pointer :: tracer_temp
  integer :: n_now,n_new,i
  real(WP), parameter :: coeff_up   = 1.3_WP
  real(WP), parameter :: coeff_down = 0.7_WP
  real(WP) :: up,down
  
  ! Resize tracer array to size n
  if (.NOT.associated(tracer_gp)) then
     ! tracer is of size 0
     if (n.eq.0) then
        ! Nothing to do, that's what we want
     else
        ! Allocate directly of size n
        allocate(tracer_gp(n))
        tracer_gp(1:n)%stop=1
     end if
  else if (n.eq.0) then
     ! tracer is associated, be we want to empty it
     deallocate(tracer_gp)
     nullify(tracer_gp)
  else
     ! Update non zero size to another non zero size
     n_now = size(tracer_gp,dim=1)
     up  =real(n_now,WP)*coeff_up
     down=real(n_now,WP)*coeff_down
     if (n.gt.n_now) then
        ! Increase from n_now to n_new
        n_new = max(n,int(up))
        allocate(tracer_temp(n_new))
        do i=1,n_now
           tracer_temp(i) = tracer_gp(i)
        end do
        deallocate(tracer_gp)
        nullify(tracer_gp)
        tracer_gp => tracer_temp
        tracer_gp(n_now+1:n_new)%stop=1
     else if (n.lt.int(down)) then
        ! Decrease from n_now to n_new
        allocate(tracer_temp(n))
        do i=1,n
           tracer_temp(i) = tracer_gp(i)
        end do
        deallocate(tracer_gp)
        nullify(tracer_gp)
        tracer_gp => tracer_temp
     end if
  end if
  
  return
end subroutine ppt_resize_gp
! ------------------------------------------ !

