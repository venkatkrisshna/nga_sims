module ppt_mod
  use precision
  implicit none
  
  ! Particle type
  type tracer_type
     ! Position (Cartesian)
     real(WP) :: x
     real(WP) :: y
     real(WP) :: z
     ! Time injected
     real(WP) :: t0
     ! Position (mesh)
     integer :: i
     integer :: j
     integer :: k
     ! Tracer ID
     integer :: id
     ! Control parameter
     integer :: stop
  end type tracer_type
  
  ! CHANGE HERE
  integer :: tracer_kind=52
  
  ! Tracer array and sizes
  type(tracer_type), dimension(:), pointer :: tracer
  integer :: ntracer_
  integer :: ntracer
  integer, dimension(:), allocatable :: ntracer_proc
  
  ! Particle array and sizes for com
  type(tracer_type), dimension(:), pointer :: tracer_gp
  integer :: ntracer_gp_
  
end module ppt_mod
