! ========================================== !
! Tracer I/O routines                        !
! Contains both serial and parallel versions !
! ========================================== !
module ppt_io
  use parallel
  use string
  use ppt
  use ppt_com
  use time_info
  implicit none
  
  ! I/O stuff
  integer(kind=MPI_OFFSET_KIND) :: HEADER_OFFSET
  integer :: tracer_last
  !tracer_last = int(tracer_time/tracer_freq)
  
  ! Filename
  character(len=str_medium) :: tracer_file_init
  character(len=str_medium) :: tracer_file
  
  ! Serial vs parallel TRACER I/O
  logical :: tracer_use_pIO
  
  ! Overwrite flag
  logical :: overwrite
  
end module ppt_io


! ========================= !
! Tracer I/O initialization !
! ========================= !
subroutine ppt_io_init
  use ppt_io
  use parser
  implicit none
    
  ! Read filenames
  call parser_read('Tracer file to read',tracer_file_init)
  call parser_read('Tracer file to write',tracer_file)
  
  ! Read how to do I/O
  call parser_read('Use parallel tracer I/O',tracer_use_pIO,use_pIO)
  
  ! Overwrite flag
  call parser_read('Data overwrite',overwrite,.true.)
  
  return
end subroutine ppt_io_init


! ======================= !
! Test if we need to save !
! the tracer data file    !
! ======================= !
subroutine ppt_write(flag)
  use ppt_io
  use time_info
  use data
  implicit none
  logical, intent(in) :: flag
  
  ! Check wether spray is used
  if (.not.use_tracer) return
  
  if (int(time/data_freq).ne.data_last .or. flag) then
     call ppt_write_full3D
  end if
  
  return
end subroutine ppt_write


! ================= !
! Write tracer file !
! ================= !
subroutine ppt_write_full3D
  use ppt_io
  implicit none
  
  ! Check wether spray is used
  if (.not.use_tracer) return
  
  ! Call right IO routine
  if (tracer_use_pIO) then
     call ppt_write_full3D_parallel
  else
     call ppt_write_full3D_serial
  end if
  
  return
end subroutine ppt_write_full3D


! ============================ !
! Write tracer file - parallel !
! ============================ !
subroutine ppt_write_full3D_parallel
  use ppt_io
  implicit none
  
  integer :: ierr,file,info,i
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer(kind=MPI_OFFSET_KIND) :: offset,ntracer_MOK
  character(len=str_medium) :: filename,buffer
  logical :: file_is_there
  
  ! Get the name
  filename=trim(mpiiofs)//trim(tracer_file)
  
  ! Add time info to the file name
  if (.not.overwrite) then
     write(buffer,'(ES12.3)') time
     filename = trim(adjustl(filename))//'_'//trim(adjustl(buffer))
  end if
  
  ! Open the file to write
  inquire(file=filename,exist=file_is_there)
  if (file_is_there .and. irank.eq.iroot) call MPI_file_delete(filename,info,ierr)
  call MPI_file_open(comm,filename,IOR(MPI_MODE_CREATE,MPI_MODE_WRONLY),mpi_info,file,ierr)
  
  ! Write header
  if (irank.eq.iroot) then
     call MPI_FILE_WRITE(file,ntracer,1,MPI_INTEGER,status,ierr)
     call MPI_FILE_WRITE(file,SIZE_MPI_TRACER,1,MPI_INTEGER,status,ierr)
     call MPI_FILE_WRITE(file,dt,1,MPI_REAL_WP,status,ierr)
     call MPI_FILE_WRITE(file,time,1,MPI_REAL_WP,status,ierr)
  end if
  
  ! Compute the offset and write
  ntracer_MOK=0
  do i=1,irank-1
     ntracer_MOK=ntracer_MOK+int(ntracer_proc(i),MPI_OFFSET_KIND)
  end do
  offset = int(SIZE_MPI_TRACER,MPI_OFFSET_KIND)*ntracer_MOK+HEADER_OFFSET
  if (ntracer_.gt.0) call MPI_file_write_at(file,offset,tracer,ntracer_,MPI_tracer,status,ierr)
  
  ! Close the file
  call MPI_file_close(file,ierr)
  
  ! Filename and info on the screen
  call monitor_log("TRACER_DATA WRITTEN")
  
  return
end subroutine ppt_write_full3D_parallel


! ========================== !
! Write tracer file - serial !
! ========================== !
subroutine ppt_write_full3D_serial
  use ppt_io
  implicit none
  
  integer :: i,ierr,file,tracer_size
  character(len=str_medium) :: filename,dirname,buffer
  
  ! Get the name of the file to write to
  dirname=trim(tracer_file)
  
  ! Add time info to the file name
  if (.not.overwrite) then
     write(buffer,'(ES12.3)') time
     dirname = trim(adjustl(dirname))//'_'//trim(adjustl(buffer))
  end if
  
  ! Add serial tag to dirname
  dirname = trim(adjustl(dirname))//'_serial'

  ! Create directory
  if (irank.eq.iroot) then
     call CREATE_FOLDER(trim(dirname))
  end if
  call MPI_BARRIER(comm,ierr)

  ! Loop over processors, only one writes at a time
  do i=1,nproc
     if (i.eq.irank) then
        
        ! Create filename
        filename = trim(adjustl(dirname)) // "/tracer."
        write(filename(len_trim(filename)+1:len_trim(filename)+6),'(i6.6)') irank
        
        ! Open the file to write
        call BINARY_FILE_OPEN(file,trim(adjustl(filename)),"w",ierr)
        
        ! Write header corresponding to proc
        tracer_size = 0
        if (ntracer_.gt.0) tracer_size = sizeof(tracer(1))
        call BINARY_FILE_WRITE(file,ntracer_,1,kind(ntracer_),ierr)                                      
        call BINARY_FILE_WRITE(file,tracer_size,1,kind(tracer_size),ierr)                              
        call BINARY_FILE_WRITE(file,dt,1,kind(dt),ierr)                                            
        call BINARY_FILE_WRITE(file,time,1,kind(time),ierr) 
        
        ! Write local tracers
        if (ntracer_.gt.0) call BINARY_FILE_WRITE(file,tracer,ntracer_,tracer_size,ierr)
        
        ! Close the file 
        call BINARY_FILE_CLOSE(file,ierr)
     end if

     ! Synchronize                                                                             
     call MPI_BARRIER(comm,ierr)
  end do

  ! Log
  if (irank.eq.iroot) call monitor_log("3D TRACER WRITTEN (SERIAL)")
  
  return
end subroutine ppt_write_full3D_serial


! ================ !
! Read tracer file !
! ================ !
subroutine ppt_read
  use ppt_io
  implicit none
  
  ! Call right IO routine
  if (tracer_use_pIO) then
     call ppt_read_parallel
  else
     call ppt_read_serial
  end if
  
  return
end subroutine ppt_read


! =========================== !
! Read tracer file - parallel !
! =========================== !
subroutine ppt_read_parallel
  use ppt_io
  implicit none
  
  integer, dimension(:,:), allocatable :: ppp
  integer :: ierr,file,i,j,ntracer_add,ibuf
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer(kind=MPI_OFFSET_KIND) :: offset
  real(WP) :: dt_read,time_read
  integer :: tracer_size
  integer :: nchunk,count
  character(len=str_medium) :: filename
  logical :: file_is_there
  integer, parameter :: chunk_size=1000
  
  ! Define the HEADER size
  HEADER_OFFSET = kind(ntracer_add)+kind(tracer_size)+kind(dt)+kind(time)
  
  ! Open the file
  inquire(file=trim(tracer_file_init),exist=file_is_there)
  if (.not.file_is_there) return
  filename= trim(mpiiofs) // trim(tracer_file_init)
  call MPI_file_open(comm,filename,MPI_MODE_RDONLY,mpi_info,file,ierr)
  if (ierr .ne. 0) return
  
  ! Get the number of tracers from the size of the file
  !call MPI_file_get_size(file,SIZE_FILE,ierr)
  !ntracer_add=SIZE_FILE/SIZE_MPI_TRACER
  
  ! Read header
  call MPI_FILE_READ_ALL(file,ntracer_add,1,MPI_INTEGER,status,ierr)
  call MPI_FILE_READ_ALL(file,tracer_size,1,MPI_INTEGER,status,ierr)
  call MPI_FILE_READ_ALL(file,dt_read,1,MPI_REAL_WP,status,ierr)
  call MPI_FILE_READ_ALL(file,time_read,1,MPI_REAL_WP,status,ierr)
  if (tracer_size.ne.SIZE_MPI_TRACER) call die("Tracer type unreadable")
  
  ! Share this among the processors
  nchunk=int(ntracer_add/(nproc*chunk_size))+1
  allocate(ppp(nproc,nchunk))
  ppp(:,:) = int(ntracer_add/(nproc*nchunk))
  count = 0
  out:do j=1,nchunk
     do i=1,nproc
        count = count + 1
        if (count.gt.mod(ntracer_add,nproc*nchunk)) exit out
        ppp(i,j) = ppp(i,j) + 1
     end do
  end do out
  
  ! Read by chunk
  do j=1,nchunk
     
     ! Find offset
     ibuf = sum(ppp(1:irank-1,:))+sum(ppp(irank,1:j-1))
     offset = int(SIZE_MPI_TRACER,MPI_OFFSET_KIND)*int(ibuf,MPI_OFFSET_KIND)+HEADER_OFFSET
     
     ! Resize tracer array
     call ppt_resize(ntracer_+ppp(irank,j))
     
     ! Read this file
     call MPI_file_read_at(file,offset,tracer(ntracer_+1:ntracer_+ppp(irank,j)),ppp(irank,j),MPI_tracer,status,ierr)
  
     ! Most general case: relocate every droplet
     do i=ntracer_+1,ntracer_+ppp(irank,j)
        call ppt_locate(tracer(i)%x,tracer(i)%y,tracer(i)%z,tracer(i)%i,tracer(i)%j,tracer(i)%k)
     end do
     
     ! Exchange all that
     call ppt_communication
     
     ! Monitoring
     if (irank.eq.iroot) print*,'Reading',100.0_WP*real(j,WP)/real(nchunk,WP),'% complete'
     
  end do
  
  ! Close the file
  call MPI_file_close(file,ierr)
  
  return
end subroutine ppt_read_parallel


! ========================= !
! Read tracer file - serial !
! ========================= !
subroutine ppt_read_serial
  use ppt_io
  implicit none
  
  integer :: ierr,file,i,j,tracer_size
  real(WP) :: dt_read,time_read
  character(len=str_medium) :: filename,buffer
  logical :: file_is_there

  ! Check if there is a file to read                  
  filename = trim(adjustl(tracer_file_init)) // '/tracer.000001'
  inquire(file=filename,exist=file_is_there)
  if (.not.file_is_there) return

  ! Each processor reads its own tracer file, one at a time
  do i=1,nproc
     if (i.eq.irank) then

        print *, 'processor ',irank,' reading'

        ! Set filename 
        write(buffer,'(i6.6)') irank
        filename = trim(adjustl(tracer_file_init)) // '/tracer.' // trim(adjustl(buffer))

        ! Open file        
        call BINARY_FILE_OPEN(file,trim(filename),"r",ierr)
        
        ! Read local number of tracers
        call binary_file_read(file,ntracer_,1,kind(ntracer_),ierr)
        call binary_file_read(file,tracer_size,1,kind(tracer_size),ierr)                                      
        call binary_file_read(file,dt_read,1,kind(dt_read),ierr)                                          
        call binary_file_read(file,time_read,1,kind(time_read),ierr)   
        
        ! Resize local tracer array
        call ppt_resize(ntracer_)
        
        ! Read tracers
        if (ntracer_>0) call binary_file_read(file,tracer(1:ntracer_),ntracer_,tracer_size,ierr)
        
        ! Re-locate
        do j=1,ntracer_
           call ppt_locate(tracer(j)%x,tracer(j)%y,tracer(j)%z,tracer(j)%i,tracer(j)%j,tracer(j)%k)
        end do

        ! Close the file
        call binary_file_close(file,ierr)
     end if
     
     ! Communicate (should not be needed)
     call ppt_communication
     
     ! Synchronize 
     call MPI_BARRIER(comm,ierr)
  end do

  return
end subroutine ppt_read_serial
