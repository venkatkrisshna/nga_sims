! ========================================= !
! Localization routines for spray tracking  !
! Performs a bisection for the x y z arrays !
! ========================================= !
module ppt_locator
  use geometry
  use partition
  use parallel
  use structure
  implicit none
  !
end module ppt_locator


! ====================================== !
! Simple localization routine            !
! Takes in (xp,yp,zp) and (ip,jp,kp)_old !
! and returns (ip,jp,kp)                 !
! ====================================== !
subroutine ppt_localize(ip_old,jp_old,kp_old,ip,jp,kp,xp,yp,zp)
  use ppt_locator
  implicit none
  
  integer,  intent(in)  :: ip_old,jp_old,kp_old
  real(WP), intent(in)  :: xp,yp,zp
  integer,  intent(out) :: ip,jp,kp
  
  ! Find right i index
  ip=ip_old
  do while (xp-x(ip  ).lt.0.0_WP)
     ip=ip-1
  end do
  do while (x(ip+1)-xp.le.0.0_WP)
     ip=ip+1
  end do
  
  ! Find right j index
  jp=jp_old
  do while (yp-y(jp  ).lt.0.0_WP)
     jp=jp-1
  end do
  do while (y(jp+1)-yp.le.0.0_WP)
     jp=jp+1
  end do
  
  ! Find right k index
  kp=kp_old
  do while (zp-z(kp  ).lt.0.0_WP)
     kp=kp-1
  end do
  do while (z(kp+1)-zp.le.0.0_WP)
     kp=kp+1
  end do
  
  return
end subroutine ppt_localize


! ============================================ !
! Processor localization routine               !
! Takes in (ip,jp,kp) and returns irank of the !
! owner of the drop. Assumes the tracer is     !
! inside the actual domain.                    !
! ============================================ !
subroutine ppt_proc(ip,jp,kp,proc)
  use ppt_locator
  implicit none
  
  integer, intent(in) :: ip,jp,kp
  integer, intent(out) :: proc
  integer :: ib,jb,kb
  
  ! Get the block that owns the tracer
  do ib=ibmin,ibmax
     if (ib1(ib).LE.ip .AND. ib2(ib).GE.ip) exit
  end do
  do jb=jbmin,jbmax
     if (jb1(jb).LE.jp .AND. jb2(jb).GE.jp) exit
  end do
  do kb=kbmin,kbmax
     if (kb1(kb).LE.kp .AND. kb2(kb).GE.kp) exit
  end do
  
  ! Get the processor from the indices
  proc=bcpu(ib,jb,kb)
  
  return
end subroutine ppt_proc


! ============================================== !
! Actual localization routine                    !
! Takes in (xp,yp,zp) and returns (ip,jp,kp)     !
! Returns imin-1 if outside (<x(imin)) (left)    !
! Returns imax+1 if outside (>x(imax+1)) (right) !
! ============================================== !
subroutine ppt_locate(xp,yp,zp,ip,jp,kp)
  use ppt_locator
  implicit none
  
  real(WP), intent(in) :: xp,yp,zp
  integer, intent(out) :: ip,jp,kp
  
  ! Localize along x
  call pbisection(xp,ip,x(imin:imax+1),imin,imax+1)
  
  ! Localize along y
  call pbisection(yp,jp,y(jmin:jmax+1),jmin,jmax+1)
  
  ! Localize along z
  call pbisection(zp,kp,z(kmin:kmax+1),kmin,kmax+1)

  return
end subroutine ppt_locate


! ================================================== !
! Bisection routine                                  !
! Gets an array and its size as well as a position x !
! Returns the index between istart-1 and iend        !
! xarray(iloc)<=xloc<xarray(iloc+1)                  !
! Assuming xarray is monotonically increasing        !
! ================================================== !
subroutine pbisection(xloc,iloc,xarray,istart,iend)
  use precision
  implicit none
  
  real(WP), intent(in) :: xloc
  integer, intent(out) :: iloc
  integer, intent(in) :: istart
  integer, intent(in) :: iend
  real(WP), dimension(istart:iend), intent(in) :: xarray
  
  integer :: il,im,iu
  
  ! Initialize lower and upper limits
  il=istart-1
  iu=iend
  
  ! While not done
  do while (iu-il.gt.1)
     ! Compute a mid-point
     im=(iu+il)/2
     ! Replace lower of upper limit as appropriate
     if (xloc.ge.xarray(im)) then
        il=im
     else
        iu=im
     end if
  end do
  
  ! Finalize the output
  if (xloc.eq.xarray(istart)) then
     iloc=istart
  elseif (xloc.ge.xarray(iend)) then
     iloc=iend
  else
     iloc=il
  end if
  
  return
end subroutine pbisection
