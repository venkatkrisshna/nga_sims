! ========================= !
! Passive particle tracking !
! ========================= !
module ppt
  use precision
  use geometry
  use partition
  use parallel
  use parser
  use ppt_mod
  use data
  implicit none
  
  ! Monitoring info
  integer  :: ntracer_in,ntracer_out
    
end module ppt


! ========================== !
! Initialize tracer routines !
! ========================== !
subroutine ppt_init
  use ppt
  use parser
  implicit none
  
  ! Check whether tracers are used
  call parser_read('Use tracer',use_tracer,.false.)
  if (.not.use_tracer) return
  
  ! Create & start the timer
  call timing_create('tracer')
  call timing_start ('tracer')

  ! Allocate arrays
  allocate(ntracer_proc(nproc))
  
  ! Initialize MPI
  call ppt_resize(2)
  call ppt_mpi_prepare ! Old MPI-1.2 standard
  !call ppt_mpi_prepare2 ! New MPI-2 standard to account for 64 bits machines
  call ppt_resize(0)
  
  ! Read the particle file
  call ppt_io_init
  call ppt_read

  ! Initialize TRACER injection
  call ppt_inject_init
  
  ! Monitor tracers
  call monitor_create_file_step('tracer',3)
  call monitor_set_header(1,'Ntracer','i')
  call monitor_set_header(2,'Ntracer in','i')
  call monitor_set_header(3,'Ntracer out','i')
  
  ! Stop a timer
  call timing_stop('tracer')
  
  return
end subroutine ppt_init


! ============================== !
! Advance the particle equations !
! ============================== !
subroutine ppt_step
  use ppt
  implicit none

  integer :: i
  
  ! Check whether tracer is used
  if (.not.use_tracer) return
  
  ! Start a timer
  call timing_start('tracer')
  
  ! Zero counter for tracers leaving the domain
  ntracer_out=0
  
  ! Add new tracers to the computation
  call ppt_inject_step

  ! Advance the equations
  do i=1,ntracer_
     call ppt_solve(i)
  end do
  
  ! Communicate tracers
  call ppt_communication
  
  ! Stop a timer
  call timing_stop('tracer')
  
  return
end subroutine ppt_step


! ============================ !
! Tracer solver: integrates in !
! time the tracer position     !
! ============================ !
subroutine ppt_solve(id)
  use ppt
  implicit none
  
  integer, intent(in) :: id
  
  ! Solve the equations
  call ppt_solver_solve(id)
  
  ! Correct the position to take into account periodicity
  ! Z periodicity
  if (zper.eq.1) tracer(id)%z=z(kmin)+modulo(tracer(id)%z-z(kmin),z(kmax+1)-z(kmin))
  ! Y periodicity
  if (yper.eq.1) tracer(id)%y=y(jmin)+modulo(tracer(id)%y-y(jmin),y(jmax+1)-y(jmin))
  ! X periodicity
  if (xper.eq.1) tracer(id)%x=x(imin)+modulo(tracer(id)%x-x(imin),x(imax+1)-x(imin))
  
  ! Relocalize
  call ppt_localize(tracer(id)%i,tracer(id)%j,tracer(id)%k,tracer(id)%i,tracer(id)%j,tracer(id)%k,tracer(id)%x,tracer(id)%y,tracer(id)%z)
  
  return
end subroutine ppt_solve


! ================= !
! Tracer monitoring !
! ================= !
subroutine ppt_monitor
  use ppt
  implicit none
  
  integer :: ibuf
  
  ! Check whether tracer is used
  if (.not.use_tracer) return
  
  ! Exit condition
  call parallel_sum(ntracer_out,ibuf); ntracer_out=ibuf
  
  ! Transfer to monitor
  call monitor_select_file('tracer')
  call monitor_set_single_value(1,real(ntracer,WP))
  call monitor_set_single_value(2,real(ntracer_in,WP))
  call monitor_set_single_value(3,real(ntracer_out,WP))
  
  return
end subroutine ppt_monitor


! ==================== !
! Last call : save all !
! ==================== !
subroutine ppt_finalize
  use ppt
  implicit none
  
  ! Check wether tracer is used
  if (.not.use_tracer) return
  
  ! Dump all the droplets to a file for restart
  call ppt_write_full3D
  
  return
end subroutine ppt_finalize
