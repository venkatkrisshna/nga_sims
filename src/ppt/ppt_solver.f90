! ================================ !
! Passive particle tracking solver !
! ================================ !
module ppt_solver
  use ppt
  use masks
  implicit none
  
  ! Local copy of current particle
  type(tracer_type) :: myt
  
  ! Gas phase properties at tracer position
  real(WP) :: ug,vg,wg
  
  ! Jacobian
  real(WP) :: dxdt,dydt,dzdt
  
end module ppt_solver


! ===================================== !
! Time solver - advance - wall - locate !
! 2nd order RK                          !
! ===================================== !
subroutine ppt_solver_solve(myi)
  use ppt_solver
  use time_info
  implicit none
  
  integer, intent(in) :: myi
  
  ! Create local copy of particle
  myt=tracer(myi)
  
  ! Advance - Euler prediction
  call ppt_solver_rhs
  myt%x  = tracer(myi)%x + 0.5_WP*dxdt*dt
  myt%y  = tracer(myi)%y + 0.5_WP*dydt*dt
  myt%z  = tracer(myi)%z + 0.5_WP*dzdt*dt
  
  ! Correct - Midpoint rule
  call ppt_solver_rhs
  myt%x  = tracer(myi)%x + dxdt*dt
  myt%y  = tracer(myi)%y + dydt*dt
  myt%z  = tracer(myi)%z + dzdt*dt
  
  ! Top and bottom conditions
  if (xper.eq.0) then
     ! Exit condition at top
     if (myt%x.gt.x(imax+1)) then
        myt%stop=1
        ntracer_out=ntracer_out+1
     end if
  end if
  
  ! Relocalize
  call ppt_localize(tracer(myi)%i,tracer(myi)%j,tracer(myi)%k,&
                          myt%i,      myt%j,      myt%k,&
                          myt%x,      myt%y,      myt%z)
  
  ! Copy back to tracer
  tracer(myi)=myt
  
  return
end subroutine ppt_solver_solve


! =============== !
! RHS computation !
! =============== !
subroutine ppt_solver_rhs
  use ppt_solver
  implicit none
  
  ! Interpolate the gas phase info at the droplet location
  call ppt_solver_interpolate
  
  ! Return rhs
  dxdt = ug
  dydt = vg
  dzdt = wg
    
  return
end subroutine ppt_solver_rhs


! ======================== !
! Gas phase interpolations !
! ======================== !
subroutine ppt_solver_interpolate
  use ppt_solver
  use interpolate
  implicit none
    
  integer :: sx,sy,sz
  integer :: i1,j1,k1
  integer :: i2,j2,k2
  real(WP) :: wx1,wx2,wy1,wy2,wz1,wz2
  real(WP), dimension(2,2,2) :: wwd,wwn
  real(WP), parameter :: eps=1.0e-9_WP
  
  ! Get interpolation cells w/r to centroids
  sx = -1
  sy = -1
  sz = -1
  if (myt%x.ge.xm(myt%i)) sx = 0
  if (myt%y.ge.ym(myt%j)) sy = 0
  if (myt%z.ge.zm(myt%k)) sz = 0
  
  ! Get the first interpolation point
  i1=myt%i+sx; i2=i1+1
  j1=myt%j+sy; j2=j1+1
  k1=myt%k+sz; k2=k1+1
  
  ! Compute the linear interpolation coefficients
  wx1 = (xm(i2)-myt%x )/(xm(i2)-xm(i1))
  wx2 = (myt%x -xm(i1))/(xm(i2)-xm(i1))
  wy1 = (ym(j2)-myt%y )/(ym(j2)-ym(j1))
  wy2 = (myt%y -ym(j1))/(ym(j2)-ym(j1))
  wz1 = (zm(k2)-myt%z )/(zm(k2)-zm(k1))
  wz2 = (myt%z -zm(k1))/(zm(k2)-zm(k1))
  
  ! Combine the interpolation coefficients to form a tri-linear interpolation
  wwd(1,1,1)=wx1*wy1*wz1
  wwd(2,1,1)=wx2*wy1*wz1
  wwd(1,2,1)=wx1*wy2*wz1
  wwd(2,2,1)=wx2*wy2*wz1
  wwd(1,1,2)=wx1*wy1*wz2
  wwd(2,1,2)=wx2*wy1*wz2
  wwd(1,2,2)=wx1*wy2*wz2
  wwd(2,2,2)=wx2*wy2*wz2
  
  ! Correct for walls with Dirichlet condition
  if (mask(i1,j1).eq.1) wwd(1,1,:) = 0.0_WP
  if (mask(i1,j2).eq.1) wwd(1,2,:) = 0.0_WP
  if (mask(i2,j1).eq.1) wwd(2,1,:) = 0.0_WP
  if (mask(i2,j2).eq.1) wwd(2,2,:) = 0.0_WP
  
  ! Correct for walls with Neumann condition
  wwn = wwd/sum(wwd)
  
  ! Velocities
  ug=sum(wwd*Ui(i1:i2,j1:j2,k1:k2))
  vg=sum(wwd*Vi(i1:i2,j1:j2,k1:k2))
  wg=sum(wwd*Wi(i1:i2,j1:j2,k1:k2))

  return
end subroutine ppt_solver_interpolate
