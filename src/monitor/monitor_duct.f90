module monitor_duct
  use precision
  use geometry
  use partition
  use borders
  implicit none
  
  ! Values to monitor
  real(WP) :: wmean,RHOmean,Cfz
  
end module monitor_duct


! ========================== !
! Initialize the duct module !
! ========================== !
subroutine monitor_duct_init
  use monitor_duct
  implicit none
  
  ! Create a file to monitor at each timestep
  call monitor_create_file_step('duct',3)
  call monitor_set_header(1,'RHOmean','r')
  call monitor_set_header(2,'Wmean','r')
  call monitor_set_header(3,'Cfz','r')
  
  return
end subroutine monitor_duct_init


! ========================================== !
! Compute the quantities relevant to monitor !
! ========================================== !
subroutine monitor_duct_compute
  use monitor_duct
  use parallel
  use data
  use metric_generic
  use metric_velocity_visc
  use masks
  implicit none
  
  integer :: i,j,k
  real(WP) :: mybuf,myA,surf
  
  ! Compute mean density
  mybuf=0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           mybuf=mybuf+vol(i,j,k)*RHO(i,j,k)
        end do
     end do
  end do
  call parallel_sum(mybuf,RHOmean)
  RHOmean=RHOmean/vol_total
  
  ! Compute mass flow rate between the walls
  mybuf=0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           mybuf=mybuf+vol(i,j,k)*rhoW(i,j,k)
        end do
     end do
  end do
  call parallel_sum(mybuf,Wmean)
  Wmean=Wmean/(vol_total*RHOmean)
  
  ! Compute friction force on the walls
  mybuf=0.0_WP
  myA=0.0_WP
  ! X- wall
  if (iproc.eq.1) then
     i=imin+1
     do k=kmin_,kmax_
        do j=max(jmin_,jmin+1),min(jmax_,jmax-1)
           myA=myA+dz*dy(j)
           mybuf=mybuf+dz*dy(j)*sum(interp_sc_xz(i,j,:,:)*VISC(i-st2:i+st1,j,k-st2:k+st1))*sum(grad_w_x(i,j,k,:)*W(i-stv2:i+stv1,j,k))
        end do
     end do
  end if
  ! X+ wall
  if (iproc.eq.npx) then
     i=imax
     do k=kmin_,kmax_
        do j=max(jmin_,jmin+1),min(jmax_,jmax-1)
           myA=myA+dz*dy(j)
           mybuf=mybuf+dz*dy(j)*sum(interp_sc_xz(i,j,:,:)*VISC(i-st2:i+st1,j,k-st2:k+st1))*sum(grad_w_x(i,j,k,:)*W(i-stv2:i+stv1,j,k))
        end do
     end do
  end if
  ! Y- wall
  if (jproc.eq.1) then
     j=jmin+1
     do k=kmin_,kmax_
        do i=max(imin_,imin+1),min(imax_,imax-1)
           myA=myA+dz*dx(i)
           mybuf=mybuf+dz*dx(i)*sum(interp_sc_yz(i,j,:,:)*VISC(i,j-st2:j+st1,k-st2:k+st1))*sum(grad_w_y(i,j,k,:)*W(i,j-stv2:j+stv1,k))
        end do
     end do
  end if
  ! Y+ wall
  if (jproc.eq.npy) then
     j=jmax
     do k=kmin_,kmax_
        do i=max(imin_,imin+1),min(imax_,imax-1)
           myA=myA+dz*dx(i)
           mybuf=mybuf+dz*dx(i)*sum(interp_sc_yz(i,j,:,:)*VISC(i,j-st2:j+st1,k-st2:k+st1))*sum(grad_w_y(i,j,k,:)*W(i,j-stv2:j+stv1,k))
        end do
     end do
  end if
  ! Gather data
  call parallel_sum(myA,surf)
  call parallel_sum(mybuf,Cfz)
  if (Wmean.eq.0.0_WP) then
     Cfz=0.0_WP
  else
     Cfz=2.0_WP*Cfz/(RHOmean*Wmean**2)/surf
  end if
  
  ! Transfer values to monitor
  call monitor_select_file('duct')
  call monitor_set_single_value(1,RHOmean)
  call monitor_set_single_value(2,Wmean)
  call monitor_set_single_value(3,Cfz)
  
  return
end subroutine monitor_duct_compute

