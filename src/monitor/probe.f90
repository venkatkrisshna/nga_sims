module probe
  use precision
  use geometry
  use partition
  implicit none
  
  ! Number of probes
  integer :: nprobes
  
  ! Probe locations
  real(WP), dimension(:), allocatable :: probe_x,probe_y,probe_z
  logical,  dimension(:), allocatable :: probe_local
  
  ! Interpolation
  real(WP), dimension(:), allocatable :: interp_x,interp_xm
  real(WP), dimension(:), allocatable :: interp_y,interp_ym
  real(WP), dimension(:), allocatable :: interp_z,interp_zm
  integer,  dimension(:), allocatable :: index_x,index_xm
  integer,  dimension(:), allocatable :: index_y,index_ym
  integer,  dimension(:), allocatable :: index_z,index_zm
  
  ! Values to monitor
  real(WP), dimension(:), allocatable :: mval

end module probe


! ===================== !
! Initialize the probes !
! ===================== !
subroutine probe_init
  use probe
  use parser
  use data
  implicit none
  integer :: nargs,n,i,j,k,isc
  logical :: isdef
  real(WP), dimension(:,:), allocatable :: list

  ! Any probes ?
  call parser_is_defined('Probe locations',isdef)
  if (isdef) then
     call parser_getsize('Probe locations',nargs)
     if (mod(nargs,3).ne.0) then
        call die('probe_init: incorrect number of coordinates for probes locations')
     else
        nprobes = nargs/3
     end if
  else
     nprobes = 0
     return
  end if
  
  ! Allocate arrays
  allocate(list(3,nprobes))
  allocate(probe_x(nprobes))
  allocate(probe_y(nprobes))
  allocate(probe_z(nprobes))
  allocate(probe_local(nprobes))
  allocate(interp_x (nprobes))
  allocate(interp_xm(nprobes))
  allocate(interp_y (nprobes))
  allocate(interp_ym(nprobes))
  allocate(interp_z (nprobes))
  allocate(interp_zm(nprobes))
  allocate(index_x (nprobes))
  allocate(index_xm(nprobes))
  allocate(index_y (nprobes))
  allocate(index_ym(nprobes))
  allocate(index_z (nprobes))
  allocate(index_zm(nprobes))

  ! Read the locations
  call parser_read('Probe locations',list)
  probe_x = list(1,:)
  probe_y = list(2,:)
  probe_z = list(3,:)
  
  ! Get interpolation
  do n=1,nprobes
     if (probe_x(n).lt.x(imin) .or. probe_x(n).ge.x(imax+1)) &
          call die('probe_init: probe x location not in domain')
     if (probe_y(n).lt.y(jmin) .or. probe_y(n).ge.y(jmax+1)) &
          call die('probe_init: probe y location not in domain')
     if (probe_z(n).lt.z(kmin) .or. probe_z(n).ge.z(kmax+1)) &
          call die('probe_init: probe z location not in domain')
     
     if ( probe_x(n).lt.x(imin_) .or. probe_x(n).ge.x(imax_+1) .or. &
          probe_y(n).lt.y(jmin_) .or. probe_y(n).ge.y(jmax_+1) .or. &
          probe_z(n).lt.z(kmin_) .or. probe_z(n).ge.z(kmax_+1)) then
        probe_local(n) = .false.
     else
        probe_local(n) = .true.

        ! Interpolation in x
        i = imin_
        do while(x(i+1).le.probe_x(n)) 
           i = i+1
        end do
        index_x(n)  = i
        interp_x(n) = (probe_x(n)-x(i))/(x(i+1)-x(i))
        i = imin_
        do while(xm(i+1).le.probe_x(n)) 
           i = i+1
        end do
        index_xm(n)  = i
        interp_xm(n) = (probe_x(n)-xm(i))/(xm(i+1)-xm(i))
        
        ! Interpolation in y
        j = jmin_
        do while(y(j+1).le.probe_y(n)) 
           j = j+1
        end do
        index_y(n)  = j
        interp_y(n) = (probe_y(n)-y(j))/(y(j+1)-y(j))
        j = jmin_
        do while(ym(j+1).le.probe_y(n)) 
           j = j+1
        end do
        index_ym(n)  = j
        interp_ym(n) = (probe_y(n)-ym(j))/(ym(j+1)-ym(j))
        
        ! Interpolation in z
        k = kmin_
        do while(z(k+1).le.probe_z(n)) 
           k = k+1
        end do
        index_z(n)  = k
        interp_z(n) = (probe_z(n)-z(k))/(z(k+1)-z(k))
        k = kmin_
        do while(zm(k+1).le.probe_z(n)) 
           k = k+1
        end do
        index_zm(n)  = k
        interp_zm(n) = (probe_z(n)-zm(k))/(zm(k+1)-zm(k))
     end if
  end do
  
  ! Create file to monitor
  call monitor_create_file_step('probes',(4+nscalar)*nprobes)
  do n=1,nprobes
     call monitor_set_header(1+(n-1)*(4+nscalar),'U','r')
     call monitor_set_header(2+(n-1)*(4+nscalar),'V','r')
     call monitor_set_header(3+(n-1)*(4+nscalar),'W','r')
     call monitor_set_header(4+(n-1)*(4+nscalar),'P','r')
     do isc=1,nscalar
        call monitor_set_header(4+isc+(n-1)*(4+nscalar),trim(SC_name(isc)),'r')
     end do
  end do
  allocate(mval((4+nscalar)*nprobes))

  return
end subroutine probe_init


! ================== !
! Monitor the probes !
! ================== !
subroutine probe_monitor
  use probe
  use data
  implicit none
  integer  :: n,isc,i,j,k
  real(WP) :: wx1,wy1,wz1,wx2,wy2,wz2,tmp
  
  ! Nothing to do if no probes
  if (nprobes.eq.0) return
  
  ! Compute local values at the probes
  mval = 0.0_WP
  do n=1,nprobes
     if (probe_local(n)) then
        ! Velocity - U
        i = index_x (n); wx2 = interp_x (n); wx1 = 1.0_WP-wx2
        j = index_ym(n); wy2 = interp_ym(n); wy1 = 1.0_WP-wy2
        k = index_zm(n); wz2 = interp_zm(n); wz1 = 1.0_WP-wz2
        mval(1+(n-1)*(4+nscalar)) = &
             + wx1 * ( wy1 * (wz1*U(i  ,j  ,k)+wz2*U(i  ,j  ,k+1)) &
                     + wy2 * (wz1*U(i  ,j+1,k)+wz2*U(i  ,j+1,k+1)) )&
             + wx2 * ( wy1 * (wz1*U(i+1,j  ,k)+wz2*U(i+1,j  ,k+1)) &
                     + wy2 * (wz1*U(i+1,j+1,k)+wz2*U(i+1,j+1,k+1)) )
        ! Velocity - V
        i = index_xm(n); wx2 = interp_xm(n); wx1 = 1.0_WP-wx2
        j = index_y (n); wy2 = interp_y (n); wy1 = 1.0_WP-wy2
        k = index_zm(n); wz2 = interp_zm(n); wz1 = 1.0_WP-wz2
        mval(2+(n-1)*(4+nscalar)) = &
             + wx1 * ( wy1 * (wz1*V(i  ,j  ,k)+wz2*V(i  ,j  ,k+1)) &
                     + wy2 * (wz1*V(i  ,j+1,k)+wz2*V(i  ,j+1,k+1)) )&
             + wx2 * ( wy1 * (wz1*V(i+1,j  ,k)+wz2*V(i+1,j  ,k+1)) &
                     + wy2 * (wz1*V(i+1,j+1,k)+wz2*V(i+1,j+1,k+1)) )
        ! Velocity - W
        i = index_xm(n); wx2 = interp_xm(n); wx1 = 1.0_WP-wx2
        j = index_ym(n); wy2 = interp_ym(n); wy1 = 1.0_WP-wy2
        k = index_z (n); wz2 = interp_z (n); wz1 = 1.0_WP-wz2
        mval(3+(n-1)*(4+nscalar)) = &
             + wx1 * ( wy1 * (wz1*W(i  ,j  ,k)+wz2*W(i  ,j  ,k+1)) &
                     + wy2 * (wz1*W(i  ,j+1,k)+wz2*W(i  ,j+1,k+1)) )&
             + wx2 * ( wy1 * (wz1*W(i+1,j  ,k)+wz2*W(i+1,j  ,k+1)) &
                     + wy2 * (wz1*W(i+1,j+1,k)+wz2*W(i+1,j+1,k+1)) )
        ! Pressure
        i = index_xm(n); wx2 = interp_xm(n); wx1 = 1.0_WP-wx2
        j = index_ym(n); wy2 = interp_ym(n); wy1 = 1.0_WP-wy2
        k = index_zm(n); wz2 = interp_zm(n); wz1 = 1.0_WP-wz2
        mval(4+(n-1)*(4+nscalar)) = &
             + wx1 * ( wy1 * (wz1*P(i  ,j  ,k)+wz2*P(i  ,j  ,k+1)) &
                     + wy2 * (wz1*P(i  ,j+1,k)+wz2*P(i  ,j+1,k+1)) )&
             + wx2 * ( wy1 * (wz1*P(i+1,j  ,k)+wz2*P(i+1,j  ,k+1)) &
                     + wy2 * (wz1*P(i+1,j+1,k)+wz2*P(i+1,j+1,k+1)) )
        ! Scalars
        i = index_xm(n); wx2 = interp_xm(n); wx1 = 1.0_WP-wx2
        j = index_ym(n); wy2 = interp_ym(n); wy1 = 1.0_WP-wy2
        k = index_zm(n); wz2 = interp_zm(n); wz1 = 1.0_WP-wz2
        do isc=1,nscalar
           mval(4+isc+(n-1)*(4+nscalar)) = &
                + wx1 * ( wy1 * (wz1*SC(i  ,j  ,k,isc)+wz2*SC(i  ,j  ,k+1,isc)) &
                        + wy2 * (wz1*SC(i  ,j+1,k,isc)+wz2*SC(i  ,j+1,k+1,isc)) )&
                + wx2 * ( wy1 * (wz1*SC(i+1,j  ,k,isc)+wz2*SC(i+1,j  ,k+1,isc)) &
                        + wy2 * (wz1*SC(i+1,j+1,k,isc)+wz2*SC(i+1,j+1,k+1,isc)) )
        end do
     end if
  end do
  
  ! Get the global value
  do n=1,(4+nscalar)*nprobes
     call parallel_sum(mval(n),tmp)
     mval(n) = tmp
  end do
  
  ! Transfer to monitor
  call monitor_select_file('probes')
  call monitor_set_array_values(mval)

  return
end subroutine probe_monitor

