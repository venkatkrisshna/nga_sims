! ============================= !
! Direct solver for BBMG solver !
! ============================= !
module bbmg_direct
  use bbmg_operator
  implicit none
  
  ! Problem size
  integer :: np
  
  ! Vectors
  real(WP), dimension(:), allocatable :: sol
  real(WP), dimension(:), allocatable :: rhs,myrhs
  
  ! Operator
  real(WP), dimension(:,:), allocatable :: OP,myOP
  
end module bbmg_direct


! =================================== !
! Initialization of the direct solver !
! =================================== !
subroutine bbmg_direct_init
  use bbmg_direct
  implicit none
  
  ! Set problem size to last grid size
  np=lvl(nlvl)%ncell
  
  ! Allocate data
  allocate(sol(np))
  allocate(rhs(np))
  allocate(myrhs(np))
  allocate(OP(np,np))
  allocate(myOP(np,np))
  
  return
end subroutine bbmg_direct_init


! ========================================= !
! Transfer coarse operator to direct solver !
! ========================================= !
subroutine bbmg_direct_operator
  use bbmg_direct
  implicit none
  
  integer :: i1,j1,k1,ind1
  integer :: i2,j2,k2,ind2
  integer :: si,sj,sk,ierr
  
  ! Zero OP
  myOP=0.0_WP
  
  ! Loop over coarse domain
  do k1=lvl(nlvl)%kmin_,lvl(nlvl)%kmax_
     do j1=lvl(nlvl)%jmin_,lvl(nlvl)%jmax_
        do i1=lvl(nlvl)%imin_,lvl(nlvl)%imax_
           
           ! Use lexicographic ordering
           ind1=i1+(j1-1)*lvl(nlvl)%nx+(k1-1)*lvl(nlvl)%nx*lvl(nlvl)%ny
           
           ! Loop over operator stencil
           kloop: do sk=-1,+1
              
              ! Update index
              k2=k1+sk
              ! Handle periodicity
              if (k2.eq.lvl(nlvl)%kmaxo) then
                 if (zper.ne.1) cycle kloop
                 k2=lvl(nlvl)%kmin
              end if
              if (k2.eq.lvl(nlvl)%kmino) then
                 if (zper.ne.1) cycle kloop
                 k2=lvl(nlvl)%kmax
              end if
              
              jloop: do sj=-1,+1
                 
                 ! Update index
                 j2=j1+sj
                 ! Handle periodicity
                 if (j2.eq.lvl(nlvl)%jmaxo) then
                    if (yper.ne.1) cycle jloop
                    j2=lvl(nlvl)%jmin
                 end if
                 if (j2.eq.lvl(nlvl)%jmino) then
                    if (yper.ne.1) cycle jloop
                    j2=lvl(nlvl)%jmax
                 end if
                 
                 iloop: do si=-1,+1
                    
                    ! Update index
                    i2=i1+si
                    ! Handle periodicity
                    if (i2.eq.lvl(nlvl)%imaxo) then
                       if (xper.ne.1) cycle iloop
                       i2=lvl(nlvl)%imin
                    end if
                    if (i2.eq.lvl(nlvl)%imino) then
                       if (xper.ne.1) cycle iloop
                       i2=lvl(nlvl)%imax
                    end if
                    
                    ! Use lexicographic ordering
                    ind2=i2+(j2-1)*lvl(nlvl)%nx+(k2-1)*lvl(nlvl)%nx*lvl(nlvl)%ny
                    
                    ! Set OP entry
                    myOP(ind1,ind2)=myOP(ind1,ind2)+lvl(nlvl)%lap(i1,j1,k1,si,sj,sk)
                    
                 end do iloop
              end do jloop
           end do kloop
           
        end do
     end do
  end do
  
  ! Gather operator
  call MPI_allreduce(myOP,OP,np*np,MPI_REAL_WP,MPI_SUM,comm,ierr)
  
  return
end subroutine bbmg_direct_operator


! ================= !
! Run direct solver !
! ================= !
subroutine bbmg_direct_solve
  use bbmg_direct
  use math
  implicit none
  
  integer :: i,j,k,ind,ierr
  real(WP) :: buf
  
  ! Zero RHS
  myrhs=0.0_WP
  
  ! Loop over coarse domain
  do k=lvl(nlvl)%kmin_,lvl(nlvl)%kmax_
     do j=lvl(nlvl)%jmin_,lvl(nlvl)%jmax_
        do i=lvl(nlvl)%imin_,lvl(nlvl)%imax_
           
           ! Use lexicographic ordering
           ind=i+(j-1)*lvl(nlvl)%nx+(k-1)*lvl(nlvl)%nx*lvl(nlvl)%ny
           
           ! Set RHS entry
           myrhs(ind)=lvl(nlvl)%f(i,j,k)
           
        end do
     end do
  end do
  
  !if (irank.eq.1) print*,'SUM(RHS)=',sum(myrhs)
  
  ! Gather RHS on all processors
  call MPI_allreduce(myrhs,rhs,np,MPI_REAL_WP,MPI_SUM,comm,ierr)
  
  ! Run direct solver
  myOP=OP
  call solve_linear_system_safe(myOP,rhs,sol,np)
  
  ! Prevent drift
  buf=sum(sol)
  sol=sol-buf
  
  ! Scatter solution
  do k=lvl(nlvl)%kmin_,lvl(nlvl)%kmax_
     do j=lvl(nlvl)%jmin_,lvl(nlvl)%jmax_
        do i=lvl(nlvl)%imin_,lvl(nlvl)%imax_
           
           ! Use lexicographic ordering
           ind=i+(j-1)*lvl(nlvl)%nx+(k-1)*lvl(nlvl)%nx*lvl(nlvl)%ny
           
           ! Put solution back
           lvl(nlvl)%v(i,j,k)=sol(ind)
           
        end do
     end do
  end do
  
  ! Communicate solution
  call bbmg_com_update(lvl(nlvl)%v,nlvl)
  
  return
end subroutine bbmg_direct_solve
