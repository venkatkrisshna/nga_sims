! ========================================== !
! Multi-level grid hierarchy for BBMG solver !
! ========================================== !
module bbmg_grid
  use bbmg
  implicit none
  
  ! Number of levels per direction
  integer :: nlvl_x,nlvl_y,nlvl_z
  
  ! Overlap size
  integer :: nov
  
contains
  
  ! Division of an integer by 2
  function div(ind)
    implicit none
    integer :: div
    integer, intent(in) :: ind
    div=ind/2+mod(ind,2)
  end function div
  
end module bbmg_grid


! ================================ !
! Initialization of grid hierarchy !
! ================================ !
subroutine bbmg_grid_init
  use bbmg_grid
  use parser
  implicit none
  
  integer :: i,n1,n2,n3
  
  ! Assume 27pts Laplacien
  nov=1
  
  ! Compute maximum number of levels per direction
  nlvl_x=1
  do while (nx.gt.2**(nlvl_x-1))
     nlvl_x=nlvl_x+1
  end do
  nlvl_y=1
  do while (ny.gt.2**(nlvl_y-1))
     nlvl_y=nlvl_y+1
  end do
  nlvl_z=1
  do while (nz.gt.2**(nlvl_z-1))
     nlvl_z=nlvl_z+1
  end do
  
  ! Compute maximum number of levels
  n1=nlvl_x;n2=nlvl_y;n3=nlvl_z
  if (nlvl_x.eq.1) n1=huge(nlvl_x)
  if (nlvl_y.eq.1) n2=huge(nlvl_y)
  if (nlvl_z.eq.1) n3=huge(nlvl_z)
  nlvl=min(n1,n2,n3)
  if (nlvl_x.eq.1.and.nlvl_y.eq.1.and.nlvl_z.eq.1) nlvl=1
  
  ! Allocate lvl structure
  allocate(lvl(nlvl))
  
  ! Generate hierarchy of grids in x
  lvl(1)%nx   =nx
  lvl(1)%imin =imin -nover
  lvl(1)%imax =imax -nover
  lvl(1)%imin_=imin_-nover
  lvl(1)%imax_=imax_-nover
  lvl(1)%imino =lvl(1)%imin -nov
  lvl(1)%imaxo =lvl(1)%imax +nov
  lvl(1)%imino_=lvl(1)%imin_-nov
  lvl(1)%imaxo_=lvl(1)%imax_+nov
  lvl(1)%nx_ =lvl(1)%imax_-lvl(1)%imin_+1
  lvl(1)%nxo =lvl(1)%nx +2*nov
  lvl(1)%nxo_=lvl(1)%nx_+2*nov
  do i=2,nlvl
     ! Divide indices and sizes by two
     lvl(i)%nx   =div(lvl(i-1)%nx     )
     lvl(i)%imin =div(lvl(i-1)%imin +1)
     lvl(i)%imax =div(lvl(i-1)%imax   )
     lvl(i)%imin_=div(lvl(i-1)%imin_+1)
     lvl(i)%imax_=div(lvl(i-1)%imax_  )
     ! Set secondary info
     lvl(i)%imino =lvl(i)%imin -nov
     lvl(i)%imaxo =lvl(i)%imax +nov
     lvl(i)%imino_=lvl(i)%imin_-nov
     lvl(i)%imaxo_=lvl(i)%imax_+nov
     lvl(i)%nx_ =lvl(i)%imax_-lvl(i)%imin_+1
     lvl(i)%nxo =lvl(i)%nx +2*nov
     lvl(i)%nxo_=lvl(i)%nx_+2*nov
  end do
  
  ! Generate hierarchy of grids in y
  lvl(1)%ny   =ny
  lvl(1)%jmin =jmin -nover
  lvl(1)%jmax =jmax -nover
  lvl(1)%jmin_=jmin_-nover
  lvl(1)%jmax_=jmax_-nover
  lvl(1)%jmino =lvl(1)%jmin -nov
  lvl(1)%jmaxo =lvl(1)%jmax +nov
  lvl(1)%jmino_=lvl(1)%jmin_-nov
  lvl(1)%jmaxo_=lvl(1)%jmax_+nov
  lvl(1)%ny_ =lvl(1)%jmax_-lvl(1)%jmin_+1
  lvl(1)%nyo =lvl(1)%ny +2*nov
  lvl(1)%nyo_=lvl(1)%ny_+2*nov
  do i=2,nlvl
     ! Divide indices and sizes by two
     lvl(i)%ny   =div(lvl(i-1)%ny     )
     lvl(i)%jmin =div(lvl(i-1)%jmin +1)
     lvl(i)%jmax =div(lvl(i-1)%jmax   )
     lvl(i)%jmin_=div(lvl(i-1)%jmin_+1)
     lvl(i)%jmax_=div(lvl(i-1)%jmax_  )
     ! Set secondary info
     lvl(i)%jmino =lvl(i)%jmin -nov
     lvl(i)%jmaxo =lvl(i)%jmax +nov
     lvl(i)%jmino_=lvl(i)%jmin_-nov
     lvl(i)%jmaxo_=lvl(i)%jmax_+nov
     lvl(i)%ny_ =lvl(i)%jmax_-lvl(i)%jmin_+1
     lvl(i)%nyo =lvl(i)%ny +2*nov
     lvl(i)%nyo_=lvl(i)%ny_+2*nov
  end do
  
  ! Generate hierarchy of grids in z
  lvl(1)%nz   =nz
  lvl(1)%kmin =kmin -nover
  lvl(1)%kmax =kmax -nover
  lvl(1)%kmin_=kmin_-nover
  lvl(1)%kmax_=kmax_-nover
  lvl(1)%kmino =lvl(1)%kmin -nov
  lvl(1)%kmaxo =lvl(1)%kmax +nov
  lvl(1)%kmino_=lvl(1)%kmin_-nov
  lvl(1)%kmaxo_=lvl(1)%kmax_+nov
  lvl(1)%nz_ =lvl(1)%kmax_-lvl(1)%kmin_+1
  lvl(1)%nzo =lvl(1)%nz +2*nov
  lvl(1)%nzo_=lvl(1)%nz_+2*nov
  do i=2,nlvl
     ! Divide indices and sizes by two
     lvl(i)%nz   =div(lvl(i-1)%nz     )
     lvl(i)%kmin =div(lvl(i-1)%kmin +1)
     lvl(i)%kmax =div(lvl(i-1)%kmax   )
     lvl(i)%kmin_=div(lvl(i-1)%kmin_+1)
     lvl(i)%kmax_=div(lvl(i-1)%kmax_  )
     ! Set secondary info
     lvl(i)%kmino =lvl(i)%kmin -nov
     lvl(i)%kmaxo =lvl(i)%kmax +nov
     lvl(i)%kmino_=lvl(i)%kmin_-nov
     lvl(i)%kmaxo_=lvl(i)%kmax_+nov
     lvl(i)%nz_ =lvl(i)%kmax_-lvl(i)%kmin_+1
     lvl(i)%nzo =lvl(i)%nz +2*nov
     lvl(i)%nzo_=lvl(i)%nz_+2*nov
  end do
  
  ! Count unknowns per level
  do i=1,nlvl
     lvl(i)%ncell  =max(lvl(i)%nx  ,0)*max(lvl(i)%ny  ,0)*max(lvl(i)%nz  ,0)
     lvl(i)%ncello =max(lvl(i)%nxo ,0)*max(lvl(i)%nyo ,0)*max(lvl(i)%nzo ,0)
     lvl(i)%ncell_ =max(lvl(i)%nx_ ,0)*max(lvl(i)%ny_ ,0)*max(lvl(i)%nz_ ,0)
     lvl(i)%ncello_=max(lvl(i)%nxo_,0)*max(lvl(i)%nyo_,0)*max(lvl(i)%nzo_,0)
  end do
  
  ! Find direct solve level
  lvl_loop: do i=1,nlvl
     if (lvl(i)%ncell.le.ncell_max) then
        nlvl=i
        exit lvl_loop
     end if
  end do lvl_loop
  
!!$  ! ========= START DEBUGGING ==================================================
!!$  if (irank.eq.1) then
!!$     
!!$     do i=1,nlvl
!!$        
!!$        ! Level
!!$        print*,'Level',i
!!$        
!!$        ! Index range
!!$        print*,'Index range - i',lvl(i)%imin,lvl(i)%imax
!!$        print*,'Index range - j',lvl(i)%jmin,lvl(i)%jmax
!!$        print*,'Index range - k',lvl(i)%kmin,lvl(i)%kmax
!!$        
!!$        ! Global sizes
!!$        print*,'Global size - i',lvl(i)%nx,lvl(i)%nxo
!!$        print*,'Global size - j',lvl(i)%ny,lvl(i)%nyo
!!$        print*,'Global size - k',lvl(i)%nz,lvl(i)%nzo
!!$        print*,'Global size - ijk',lvl(i)%ncell,lvl(i)%ncello
!!$        
!!$     end do
!!$     
!!$  end if
!!$  
!!$  ! Local sizes
!!$  do i=1,nlvl
!!$     if (irank.eq.1) print*,'Level',i
!!$     do n1=1,nproc
!!$        if (n1.eq.irank) then
!!$           print*,'Processor rank','[',irank,']'
!!$           print*,'Processor cart loc',iproc,jproc,kproc
!!$           print*,'Local size - i',lvl(i)%imin_,lvl(i)%imax_
!!$           print*,'Local size - j',lvl(i)%jmin_,lvl(i)%jmax_
!!$           print*,'Local size - k',lvl(i)%kmin_,lvl(i)%kmax_
!!$        end if
!!$        call MPI_BARRIER(comm,n2)
!!$     end do
!!$  end do
!!$  ! ========= END DEBUGGING ====================================================
  
  return
end subroutine bbmg_grid_init
