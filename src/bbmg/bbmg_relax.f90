! ================================== !
! Relaxation routine for BBMG solver !
! ================================== !
module bbmg_relax
  use bbmg_operator
  implicit none
  
end module bbmg_relax


! =============================== !
! 8 color Gauss-Seidel relaxation !
! =============================== !
subroutine bbmg_relax_gs(A,B,n,dir)
  use bbmg_operator
  implicit none
  
  integer, intent(in) :: n
  real(WP), dimension(lvl(n)%imino_:lvl(n)%imaxo_,lvl(n)%jmino_:lvl(n)%jmaxo_,lvl(n)%kmino_:lvl(n)%kmaxo_) :: A
  real(WP), dimension(lvl(n)%imino_:lvl(n)%imaxo_,lvl(n)%jmino_:lvl(n)%jmaxo_,lvl(n)%kmino_:lvl(n)%kmaxo_) :: B
  integer, intent(in) :: dir
  integer :: col,colmin,colmax,coldir
  integer :: i,j,k
  
  ! Choose sweep direction
  select case (dir)
  case (+1)
     ! Positive sweep
     colmin=1
     colmax=8
     coldir=+1
  case (-1)
     ! Negative sweep
     colmin=8
     colmax=1
     coldir=-1
  case default
     call die('BBMG - GS sweep direction unknown.')
  end select
  
  ! Loop over colors
  do col=colmin,colmax,coldir
     
     ! Loop over domain
     do k=lvl(n)%kmin_+mod((col-1)/4,2),lvl(n)%kmax_,2
        do j=lvl(n)%jmin_+mod((col-1)/2,2),lvl(n)%jmax_,2
           do i=lvl(n)%imin_+mod((col-1)/1,2),lvl(n)%imax_,2
              ! Gauss-Seidel step
              A(i,j,k) = 0.0_WP
              A(i,j,k) = sum(lvl(n)%lap(i,j,k,:,:,:)*A(i-1:i+1,j-1:j+1,k-1:k+1))
              A(i,j,k) = (B(i,j,k)-A(i,j,k))/(lvl(n)%lap(i,j,k,0,0,0)+epsilon(1.0_WP))
           end do
        end do
     end do
     
     ! Communicate solution
     call bbmg_com_update(A,n)
     
  end do
  
  return
end subroutine bbmg_relax_gs
