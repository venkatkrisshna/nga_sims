! =================================== !
! Parallel Black-Box Multigrid solver !
! ----------------------------------- !
!  - self-contained, highly portable  !
!  - assumes a cartesian data layout  !
!  - restriction & prolongation       !
! based on Laplacian operator only    !
!  - automatic handling of wall/IB    !
! =================================== !
module bbmg
  use geometry
  use parallel
  use metric_velocity_conv
  use masks
  use partition
  implicit none

  ! Normalization logical
  logical :: bbmg_normalize
    
  ! MG parameters
  integer :: MGcst
  integer :: MGit,MGcycle,MGrelax1,MGrelax2
  integer :: ncell_max
  real(WP) :: MGcvg
  
  ! Number of levels
  integer :: nlvl
  
  ! Level structure definition
  type lvl_type
     
     ! Global sizes
     integer :: ncell,ncello
     integer :: nx ,imin ,imax ,nxo ,imino ,imaxo
     integer :: ny ,jmin ,jmax ,nyo ,jmino ,jmaxo
     integer :: nz ,kmin ,kmax ,nzo ,kmino ,kmaxo
     
     ! Local sizes
     integer :: ncell_,ncello_
     integer :: nx_,imin_,imax_,nxo_,imino_,imaxo_
     integer :: ny_,jmin_,jmax_,nyo_,jmino_,jmaxo_
     integer :: nz_,kmin_,kmax_,nzo_,kmino_,kmaxo_
     
     ! Operators
     real(WP), dimension(:,:,:,:,:,:), allocatable :: c2f
     real(WP), dimension(:,:,:,:,:,:), allocatable :: f2c
     real(WP), dimension(:,:,:,:,:,:), allocatable :: lap
     real(WP), dimension(:,:,:,:,:,:), allocatable :: lapc2f
     
     ! Vectors
     real(WP), dimension(:,:,:), allocatable :: v
     real(WP), dimension(:,:,:), allocatable :: f
     real(WP), dimension(:,:,:), allocatable :: r
     
     ! Parallel information
     logical, dimension(:), allocatable :: send_xm,send_xp
     logical, dimension(:), allocatable :: send_ym,send_yp
     logical, dimension(:), allocatable :: send_zm,send_zp
     integer :: nsend_xm,nsend_xp
     integer :: nsend_ym,nsend_yp
     integer :: nsend_zm,nsend_zp
     integer :: recv_xm,recv_xp
     integer :: recv_ym,recv_yp
     integer :: recv_zm,recv_zp
     
  end type lvl_type
  
  ! Level data
  type(lvl_type), dimension(:), allocatable :: lvl
  
end module bbmg


! ==================================== !
! Initialization of the BBMG algorithm !
! ==================================== !
subroutine bbmg_init
  use bbmg
  use parser
  use pressure
  implicit none

  ! Set normalization logical
  bbmg_normalize=.false.
  
  ! Set MG parameters
  MGcst    =pressure_cst_lap
  MGit     =max_iter
  MGcvg    =cvg
  MGcycle  =1 ! Perform V-cycle
  MGrelax1 =1 ! 1 positive sweep of RBGS down
  MGrelax2 =1 ! 1 negative sweep of RBGS up
  ncell_max=0 ! 128
  
  ! Initialize grid hierarchy
  call bbmg_grid_init
  
  ! Initialize multi-level operators
  call bbmg_operator_init
  
  ! Initialize communication routines
  call bbmg_com_init
  
  ! Initialize direct solver
  call bbmg_direct_init
  
  ! Prepare operators if constant operator
  if (MGcst.eq.0) call bbmg_operator_compute
  
  return
end subroutine bbmg_init


! ===================== !
! BBMG recursive solver !
! ===================== !
subroutine bbmg_solve
  use bbmg
  use pressure
  use time_info
  implicit none
  
  integer :: iter
  integer :: i,j,k
  integer :: fi,fj,fk
  real(WP) :: normres,normres0
  
  ! If variable operator, recompute hierarchy
  if (MGcst.eq.2 .or. &
      MGcst.eq.1.and.niter.eq.1) call bbmg_operator_compute
  
  ! Transfer RHS
  lvl(1)%f=0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           fi=i-nover;fj=j-nover;fk=k-nover
           lvl(1)%f(fi,fj,fk)=RP(i,j,k)
        end do
     end do
  end do
  call bbmg_com_update(lvl(1)%f,1)
  
  ! Transfer initial guess
  lvl(1)%v=0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           fi=i-nover;fj=j-nover;fk=k-nover
           lvl(1)%v(fi,fj,fk)=DP(i,j,k)
        end do
     end do
  end do
  call bbmg_com_update(lvl(1)%v,1)
  
  ! Store initial residual
  call bbmg_operator_residual(1)
  call parallel_max(maxval(abs(lvl(1)%r(lvl(1)%imin_:lvl(1)%imax_,&
                                        lvl(1)%jmin_:lvl(1)%jmax_,&
                                        lvl(1)%kmin_:lvl(1)%kmax_))),normres0)
  if (normres0.eq.0.0_WP) return
  
  ! Loop over cycles
  iterloop: do iter=1,MGit
     
     ! Perform a MG cycle
     call bbmg_cycle(1)
     
     ! Check convergence
     call bbmg_operator_residual(1)
     call parallel_max(maxval(abs(lvl(1)%r(lvl(1)%imin_:lvl(1)%imax_,&
                                           lvl(1)%jmin_:lvl(1)%jmax_,&
                                           lvl(1)%kmin_:lvl(1)%kmax_))),normres)
     normres=normres/normres0
     !if (irank.eq.1) print*,'[',iter,'] ----->',normres
     if (normres.le.MGcvg) exit iterloop
     
  end do iterloop
  
  ! Return solution
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           fi=i-nover;fj=j-nover;fk=k-nover
           DP(i,j,k)=lvl(1)%v(fi,fj,fk)
        end do
     end do
  end do
  
  ! Return monitoring values
  it_p=iter
  max_resP=normres
  
  return
end subroutine bbmg_solve


! =========================== !
! BBMG - recursive MG cycling !
! =========================== !
recursive subroutine bbmg_cycle(n)
  use bbmg
  implicit none
  
  integer, intent(in) :: n
  integer :: it,it1,it2,dir
  
  ! Zero error
  if (n.gt.1) lvl(n)%v=0.0_WP
  
  ! Treat last level
  if (n.eq.nlvl) then
     dir=1
     do it1=1,MGrelax1+MGrelax2
        call bbmg_relax_gs(lvl(n)%v,lvl(n)%f,n,dir)
        dir=-dir
     end do
     !call bbmg_direct_solve
     return
  end if
  
  ! First relaxation
  dir=1
  do it1=1,MGrelax1
     call bbmg_relax_gs(lvl(n)%v,lvl(n)%f,n,dir)
     dir=-dir
  end do
  
  ! Compute residual
  call bbmg_operator_residual(n)
  
  !if (irank.eq.1) print*,'Level [',n,'] -> SUM(RES)=',sum(lvl(n)%r(lvl(n)%imin_:lvl(n)%imax_,lvl(n)%jmin_:lvl(n)%jmax_,lvl(n)%kmin_:lvl(n)%kmax_))
  
  ! Restrict residual
  call bbmg_operator_f2c(lvl(n)%r,n,lvl(n+1)%f,n+1)
  
  !if (irank.eq.1) print*,'Level [',n+1,'] -> SUM(RHS)=',sum(lvl(n+1)%f(lvl(n+1)%imin_:lvl(n+1)%imax_,lvl(n+1)%jmin_:lvl(n+1)%jmax_,lvl(n+1)%kmin_:lvl(n+1)%kmax_))
  
  ! Cycle
  do it=1,MGcycle
     call bbmg_cycle(n+1)
  end do
  
  ! Prolongate solution
  call bbmg_operator_c2f(lvl(n+1)%v,n+1,lvl(n)%r,n)
  
  ! Correct solution
  lvl(n)%v=lvl(n)%v+lvl(n)%r
  
  ! Second relaxation
  do it2=1,MGrelax2
     call bbmg_relax_gs(lvl(n)%v,lvl(n)%f,n,dir)
     dir=-dir
  end do
  
  return
end subroutine bbmg_cycle
