! ===================================== !
! Multi-level operators for BBMG solver !
! ===================================== !
module bbmg_operator
  use bbmg
  use bbmg_grid
  implicit none
  
contains
  
  ! ====================================================== !
  ! Parity of point i shifted by n to ensure 1 <= i <= n   !
  ! pmod(i,n) returns the parity of the rest of the        !
  ! integer division of i by n  such that 1.ge.result.le.n !
  ! ====================================================== !
  function pmodx(i,n)
    integer, intent(in) :: i,n
    integer :: pmodx
    if (xper.eq.1) then
       pmodx=1-mod(mod(i+lvl(n)%nx-1,lvl(n)%nx)+1,2)
    else
       pmodx=1-mod(i,2)
    end if
  end function pmodx
  
  function pmody(i,n)
    integer, intent(in) :: i,n
    integer :: pmody
    if (yper.eq.1) then
       pmody=1-mod(mod(i+lvl(n)%ny-1,lvl(n)%ny)+1,2)
    else
       pmody=1-mod(i,2)
    end if
  end function pmody
  
  function pmodz(i,n)
    integer, intent(in) :: i,n
    integer :: pmodz
    if (zper.eq.1) then
       pmodz=1-mod(mod(i+lvl(n)%nz-1,lvl(n)%nz)+1,2)
    else
       pmodz=1-mod(i,2)
    end if
  end function pmodz
  
end module bbmg_operator


! ======================================= !
! Initialization of multi-level operators !
! ======================================= !
subroutine bbmg_operator_init
  use bbmg_operator
  implicit none
  
  integer :: n
  
  ! Allocate operators
  do n=1,nlvl
     
     ! Handle empty processors
     if (lvl(n)%ncello_.eq.0) cycle
     
     ! Prolongation operator (from lvl n+1 to lvl n)
     if (n.lt.nlvl) then
        allocate(lvl(n)%c2f(lvl(n)%imino_:lvl(n)%imaxo_,&
                            lvl(n)%jmino_:lvl(n)%jmaxo_,&
                            lvl(n)%kmino_:lvl(n)%kmaxo_,0:1,0:1,0:1))
        lvl(n)%c2f=0.0_WP
     end if
     
     ! Restriction operator (from lvl n-1 to lvl n)
     if (n.gt.1) then
        allocate(lvl(n)%f2c(lvl(n)%imino_:lvl(n)%imaxo_,&
                            lvl(n)%jmino_:lvl(n)%jmaxo_,&
                            lvl(n)%kmino_:lvl(n)%kmaxo_,-1:1,-1:1,-1:1))
        lvl(n)%f2c=0.0_WP
     end if
     
     ! Laplacian operator
     allocate(lvl(n)%lap(lvl(n)%imino_:lvl(n)%imaxo_,&
                         lvl(n)%jmino_:lvl(n)%jmaxo_,&
                         lvl(n)%kmino_:lvl(n)%kmaxo_,-1:1,-1:1,-1:1))
     lvl(n)%lap=0.0_WP
     
     ! Laplacian*Prolongation operator (from lvl n+1 to lvl n)
     if (n.lt.nlvl) then
        allocate(lvl(n)%lapc2f(lvl(n)%imino_:lvl(n)%imaxo_,&
                               lvl(n)%jmino_:lvl(n)%jmaxo_,&
                               lvl(n)%kmino_:lvl(n)%kmaxo_,-1:1,-1:1,-1:1))
        lvl(n)%lapc2f=0.0_WP
     end if
     
     ! Allocate vectors
     allocate(lvl(n)%v(lvl(n)%imino_:lvl(n)%imaxo_,&
                       lvl(n)%jmino_:lvl(n)%jmaxo_,&
                       lvl(n)%kmino_:lvl(n)%kmaxo_))
     lvl(n)%v=0.0_WP
     
     allocate(lvl(n)%f(lvl(n)%imino_:lvl(n)%imaxo_,&
                       lvl(n)%jmino_:lvl(n)%jmaxo_,&
                       lvl(n)%kmino_:lvl(n)%kmaxo_))
     lvl(n)%f=0.0_WP
     
     allocate(lvl(n)%r(lvl(n)%imino_:lvl(n)%imaxo_,&
                       lvl(n)%jmino_:lvl(n)%jmaxo_,&
                       lvl(n)%kmino_:lvl(n)%kmaxo_))
     lvl(n)%r=0.0_WP
     
  end do
  
  return
end subroutine bbmg_operator_init


! ==================================== !
! Computation of multi-level operators !
! ==================================== !
subroutine bbmg_operator_compute
  use bbmg_operator
  implicit none
  
  integer :: i,j,k,n
  integer :: fi,fj,fk
  
  ! Transfer the Laplacian operator from metrics
  do k=kmin_-nov,kmax_+nov
     do j=jmin_-nov,jmax_+nov
        do i=imin_-nov,imax_+nov
           ! Shift by nover
           fi=i-nover;fj=j-nover;fk=k-nover
           ! Zero operator
           lvl(1)%lap(fi,fj,fk,:,:,:)=0.0_WP
           ! Tranfer diagonal
           lvl(1)%lap(fi,fj,fk,0,0,0)=sum(lap(i,j,k,:,0))
           ! West-East
           lvl(1)%lap(fi,fj,fk,-1,0,0)=lap(i,j,k,1,-1)
           lvl(1)%lap(fi,fj,fk,+1,0,0)=lap(i,j,k,1,+1)
           ! South-North
           lvl(1)%lap(fi,fj,fk,0,-1,0)=lap(i,j,k,2,-1)
           lvl(1)%lap(fi,fj,fk,0,+1,0)=lap(i,j,k,2,+1)
           ! Bottom-Top
           lvl(1)%lap(fi,fj,fk,0,0,-1)=lap(i,j,k,3,-1)
           lvl(1)%lap(fi,fj,fk,0,0,+1)=lap(i,j,k,3,+1)
        end do
     end do
  end do
  
  ! Loop over levels
  do n=2,nlvl
     
     ! Create prolongation operator
     call bbmg_operator_prolongation(n-1)
     
     ! Create restriction operator
     call bbmg_operator_restriction(n)
     
     ! Create Laplacian operator
     call bbmg_operator_laplacian(n)
     
  end do
  
  ! Prepare direct solver
  call bbmg_direct_operator
  
  return
end subroutine bbmg_operator_compute


! ==================================== !
! Computation of prolongation operator !
! ==================================== !
subroutine bbmg_operator_prolongation(n)
  use bbmg_operator
  implicit none
  
  integer, intent(in) :: n
  integer :: n1,n2,n3
  integer :: fi,fj,fk
  integer :: li,lj,lk
  integer :: pi,pj,pk
  integer :: si,sj,sk
  real(WP), dimension(-1:+1)       :: lap1d
  real(WP), dimension(-1:+1,-1:+1) :: lap2d
  real(WP), parameter :: ratio=10.0_WP
  
  ! Sweep 1 - identity and 1D interpolations
  do fk=lvl(n)%kmino_,lvl(n)%kmaxo_
     do fj=lvl(n)%jmino_,lvl(n)%jmaxo_
        do fi=lvl(n)%imino_,lvl(n)%imaxo_
           
           ! Check stencil size
           n1 = pmodx(fi,n)
           n2 = pmody(fj,n)
           n3 = pmodz(fk,n)
           
           ! Reset prolongation operator
           lvl(n)%c2f(fi,fj,fk,:,:,:)=0.0_WP
           
           ! Compute prolongation coefficients
           select case (n1+n2+n3)
           case (0)
              
              ! Fine and coarse locations coincide => Identity
              lvl(n)%c2f(fi,fj,fk,0,0,0)=1.0_WP
              
           case (1)
              
              ! Fine location lies on coarse x/y/z-line => Which one?
              if      (n1.eq.1) then
                 
                 ! Aggregate Laplacian
                 lap1d=0.0_WP
                 do lk=-1,+1
                    do lj=-1,+1
                       do li=-1,+1
                          if (ratio*abs(lvl(n)%lap(fi,fj,fk,li, 0, 0)).ge.&
                                    abs(lvl(n)%lap(fi,fj,fk,li,lj,lk))) then
                             lap1d(li)=lap1d(li)+lvl(n)%lap(fi,fj,fk,li,lj,lk)
                          else
                             lap1d( 0)=lap1d( 0)+lvl(n)%lap(fi,fj,fk,li,lj,lk)
                          end if
                       end do
                    end do
                 end do
                 lap1d(0)=sign(max(abs(lap1d(0)),epsilon(1.0_WP)),lap1d(0))
                 
                 ! Form x-interpolation
                 lvl(n)%c2f(fi,fj,fk,0,0,0)=-lap1d(-1)/lap1d(0)
                 lvl(n)%c2f(fi,fj,fk,1,0,0)=-lap1d(+1)/lap1d(0)
                 
              else if (n2.eq.1) then
                 
                 ! Aggregate Laplacian
                 lap1d=0.0_WP
                 do lk=-1,+1
                    do lj=-1,+1
                       do li=-1,+1
                          if (ratio*abs(lvl(n)%lap(fi,fj,fk, 0,lj, 0)).ge.&
                                    abs(lvl(n)%lap(fi,fj,fk,li,lj,lk))) then
                             lap1d(lj)=lap1d(lj)+lvl(n)%lap(fi,fj,fk,li,lj,lk)
                          else
                             lap1d( 0)=lap1d( 0)+lvl(n)%lap(fi,fj,fk,li,lj,lk)
                          end if
                       end do
                    end do
                 end do
                 lap1d(0)=sign(max(abs(lap1d(0)),epsilon(1.0_WP)),lap1d(0))
                 
                 ! Form y-interpolation
                 lvl(n)%c2f(fi,fj,fk,0,0,0)=-lap1d(-1)/lap1d(0)
                 lvl(n)%c2f(fi,fj,fk,0,1,0)=-lap1d(+1)/lap1d(0)
                 
              else if (n3.eq.1) then
                 
                 ! Aggregate Laplacian
                 lap1d=0.0_WP
                 do lk=-1,+1
                    do lj=-1,+1
                       do li=-1,+1
                          if (ratio*abs(lvl(n)%lap(fi,fj,fk, 0, 0,lk)).ge.&
                                    abs(lvl(n)%lap(fi,fj,fk,li,lj,lk))) then
                             lap1d(lk)=lap1d(lk)+lvl(n)%lap(fi,fj,fk,li,lj,lk)
                          else
                             lap1d( 0)=lap1d( 0)+lvl(n)%lap(fi,fj,fk,li,lj,lk)
                          end if
                       end do
                    end do
                 end do
                 lap1d(0)=sign(max(abs(lap1d(0)),epsilon(1.0_WP)),lap1d(0))
                 
                 ! Form z-interpolation
                 lvl(n)%c2f(fi,fj,fk,0,0,0)=-lap1d(-1)/lap1d(0)
                 lvl(n)%c2f(fi,fj,fk,0,0,1)=-lap1d(+1)/lap1d(0)
                 
              end if
              
           case default
              
              ! Do not treat face of cube centers yet
              cycle
              
           end select
           
        end do
     end do
  end do
  
  ! Sweep 2 - 2D face interpolations
  do fk=lvl(n)%kmino_,lvl(n)%kmaxo_
     do fj=lvl(n)%jmino_,lvl(n)%jmaxo_
        do fi=lvl(n)%imino_,lvl(n)%imaxo_
           
           ! Check stencil size
           n1 = pmodx(fi,n)
           n2 = pmody(fj,n)
           n3 = pmodz(fk,n)
           
           ! Compute prolongation coefficients
           select case (n1+n2+n3)
           case (2)
              
              ! Fine location lies on coarse xy/yz/zx-plane => Which one?
              if      (n1.eq.0) then
                 
                 ! yz-plane - skip if in y/z ghost cell
                 if ( fj.eq.lvl(n)%jmino_ .or. fj.eq.lvl(n)%jmaxo_ .or. &
                      fk.eq.lvl(n)%kmino_ .or. fk.eq.lvl(n)%kmaxo_ ) cycle
                 
                 ! Aggregate Laplacian
                 lap2d=0.0_WP
                 do lk=-1,+1
                    do lj=-1,+1
                       do li=-1,+1
                          if (ratio*abs(lvl(n)%lap(fi,fj,fk, 0,lj,lk)).ge.&
                                    abs(lvl(n)%lap(fi,fj,fk,li,lj,lk))) then
                             lap2d(lj,lk)=lap2d(lj,lk)+lvl(n)%lap(fi,fj,fk,li,lj,lk)
                          else
                             lap2d( 0, 0)=lap2d( 0, 0)+lvl(n)%lap(fi,fj,fk,li,lj,lk)
                          end if
                       end do
                    end do
                 end do
                 lap2d(0,0)=sign(max(abs(lap2d(0,0)),epsilon(1.0_WP)),lap2d(0,0))
                 
                 ! Loop over Laplacian stencil
                 do lk=-1,+1
                    do lj=-1,+1
                       
                       ! Skip middle of stencil
                       if (lj.eq.0 .and. lk.eq.0) cycle
                       
                       ! Loop over prolongation stencil
                       do pk=0,1-abs(lk)
                          do pj=0,1-abs(lj)
                             
                             sj=(lj+1)/2+pj
                             sk=(lk+1)/2+pk

                             lvl(n)%c2f(fi,fj,fk,0,sj,sk) = lvl(n)%c2f(fi,fj,fk,0,sj,sk) &
                                  - lap2d(lj,lk)/lap2d(0,0) &
                                  * lvl(n)%c2f(fi,fj+lj,fk+lk,0,pj,pk)
                             
                          end do
                       end do
                       
                    end do
                 end do
                 
              else if (n2.eq.0) then
                 
                 ! zx-plane - skip if in z/x ghost cell
                 if ( fk.eq.lvl(n)%kmino_ .or. fk.eq.lvl(n)%kmaxo_ .or. &
                      fi.eq.lvl(n)%imino_ .or. fi.eq.lvl(n)%imaxo_ ) cycle
                 
                 ! Aggregate Laplacian
                 lap2d=0.0_WP
                 do lk=-1,+1
                    do lj=-1,+1
                       do li=-1,+1
                          if (ratio*abs(lvl(n)%lap(fi,fj,fk,li, 0,lk)).ge.&
                                    abs(lvl(n)%lap(fi,fj,fk,li,lj,lk))) then
                             lap2d(li,lk)=lap2d(li,lk)+lvl(n)%lap(fi,fj,fk,li,lj,lk)
                          else
                             lap2d( 0, 0)=lap2d( 0, 0)+lvl(n)%lap(fi,fj,fk,li,lj,lk)
                          end if
                       end do
                    end do
                 end do
                 lap2d(0,0)=sign(max(abs(lap2d(0,0)),epsilon(1.0_WP)),lap2d(0,0))
                 
                 ! Loop over Laplacian stencil
                 do lk=-1,+1
                    do li=-1,+1
                       
                       ! Skip middle of stencil
                       if (li.eq.0 .and. lk.eq.0) cycle
                       
                       ! Loop over prolongation stencil
                       do pk=0,1-abs(lk)
                          do pi=0,1-abs(li)
                             
                             si=(li+1)/2+pi
                             sk=(lk+1)/2+pk

                             lvl(n)%c2f(fi,fj,fk,si,0,sk) = lvl(n)%c2f(fi,fj,fk,si,0,sk) &
                                  - lap2d(li,lk)/lap2d(0,0) &
                                  * lvl(n)%c2f(fi+li,fj,fk+lk,pi,0,pk)
                             
                          end do
                       end do
                       
                    end do
                 end do
                 
              else if (n3.eq.0) then
                 
                 ! xy-plane - skip if in x/y ghost cell
                 if ( fi.eq.lvl(n)%imino_ .or. fi.eq.lvl(n)%imaxo_ .or. &
                      fj.eq.lvl(n)%jmino_ .or. fj.eq.lvl(n)%jmaxo_ ) cycle
                 
                 ! Aggregate Laplacian
                 lap2d=0.0_WP
                 do lk=-1,+1
                    do lj=-1,+1
                       do li=-1,+1
                          if (ratio*abs(lvl(n)%lap(fi,fj,fk,li,lj, 0)).ge.&
                                    abs(lvl(n)%lap(fi,fj,fk,li,lj,lk))) then
                             lap2d(li,lj)=lap2d(li,lj)+lvl(n)%lap(fi,fj,fk,li,lj,lk)
                          else
                             lap2d( 0, 0)=lap2d( 0, 0)+lvl(n)%lap(fi,fj,fk,li,lj,lk)
                          end if
                       end do
                    end do
                 end do
                 lap2d(0,0)=sign(max(abs(lap2d(0,0)),epsilon(1.0_WP)),lap2d(0,0))
                 
                 ! Loop over Laplacian stencil
                 do lj=-1,+1
                    do li=-1,+1
                       
                       ! Skip middle of stencil
                       if (li.eq.0 .and. lj.eq.0) cycle
                       
                       ! Loop over prolongation stencil
                       do pj=0,1-abs(lj)
                          do pi=0,1-abs(li)
                             
                             si=(li+1)/2+pi
                             sj=(lj+1)/2+pj
                             
                             lvl(n)%c2f(fi,fj,fk,si,sj,0) = lvl(n)%c2f(fi,fj,fk,si,sj,0) &
                                  - lap2d(li,lj)/lap2d(0,0) &
                                  * lvl(n)%c2f(fi+li,fj+lj,fk,pi,pj,0)
                             
                          end do
                       end do
                       
                    end do
                 end do
                 
              end if
              
           case default
              
              ! Identity & line already treated
              ! Don't treat cube centers yet
              cycle
              
           end select
           
        end do
     end do
  end do
  
  ! Sweep 3 - 3D cube interpolation
  do fk=lvl(n)%kmin_,lvl(n)%kmax_
     do fj=lvl(n)%jmin_,lvl(n)%jmax_
        do fi=lvl(n)%imin_,lvl(n)%imax_
           
           ! Check stencil size
           n1 = pmodx(fi,n)
           n2 = pmody(fj,n)
           n3 = pmodz(fk,n)
           
           ! Compute prolongation coefficients
           select case (n1+n2+n3)
           case (3)
              
              ! Fine location lies on xyz-cube
              ! Loop over Laplacian stencil
              do lk=-1,+1
                 do lj=-1,+1
                    do li=-1,+1
                       
                       ! Skip middle of stencil
                       if (li.eq.0 .and. lj.eq.0 .and. lk.eq.0) cycle
                       
                       ! Loop over prolongation stencil
                       do pk=0,1-abs(lk)
                          do pj=0,1-abs(lj)
                             do pi=0,1-abs(li)
                                
                                si=(li+1)/2+pi
                                sj=(lj+1)/2+pj
                                sk=(lk+1)/2+pk
                                
                                lvl(n)%c2f(fi,fj,fk,si,sj,sk) = lvl(n)%c2f(fi,fj,fk,si,sj,sk) &
                                     - lvl(n)%lap(fi,fj,fk,li,lj,lk)/sign(max(abs(lvl(n)%lap(fi,fj,fk,0,0,0)),epsilon(1.0_WP)),lvl(n)%lap(fi,fj,fk,0,0,0)) &
                                     * lvl(n)%c2f(fi+li,fj+lj,fk+lk,pi,pj,pk)
                                
                             end do
                          end do
                       end do
                       
                    end do
                 end do
              end do
              
           case default
              
              ! All other cases have been treated already
              cycle
              
           end select
           
        end do
     end do
  end do

  ! Communicate operator
  call bbmg_com_update_op(lvl(n)%c2f,n,2)
  
  return
end subroutine bbmg_operator_prolongation


! =================================== !
! Computation of restriction operator !
! =================================== !
subroutine bbmg_operator_restriction(n)
  use bbmg_operator
  implicit none
  
  integer, intent(in) :: n
  integer :: fi,fj,fk
  integer :: ci,cj,ck
  integer :: ri,rj,rk
  integer :: pi,pj,pk
  real(WP) :: buf
  
  ! Zero restriction
  lvl(n)%f2c=0.0_WP
  
  ! Loop over coarse cells
  do ck=lvl(n)%kmin_,lvl(n)%kmax_
     do cj=lvl(n)%jmin_,lvl(n)%jmax_
        do ci=lvl(n)%imin_,lvl(n)%imax_
           
           ! Find corresponding fine cell
           fi=2*ci-1; fj=2*cj-1; fk=2*ck-1
           
           ! Loop over restriction stencil
           do rk=-pmodz(fk-1,n-1),+pmodz(fk+1,n-1)
              do rj=-pmody(fj-1,n-1),+pmody(fj+1,n-1)
                 do ri=-pmodx(fi-1,n-1),+pmodx(fi+1,n-1)
                    
                    pi=(1-ri)/2
                    pj=(1-rj)/2
                    pk=(1-rk)/2
                    
                    lvl(n)%f2c(ci,cj,ck,ri,rj,rk) = lvl(n-1)%c2f(fi+ri,fj+rj,fk+rk,pi,pj,pk)
                    
                 end do
              end do
           end do
           
           ! Normalize restriction - Fixes John's problems?
           if (bbmg_normalize) then
              buf = sum(lvl(n)%f2c(ci,cj,ck,:,:,:)) + epsilon(1.0_WP)
              lvl(n)%f2c(ci,cj,ck,:,:,:) = lvl(n)%f2c(ci,cj,ck,:,:,:) / buf
           end if
           
        end do
     end do
  end do
  
  ! Communicate operator
  call bbmg_com_update_op(lvl(n)%f2c,n,3)
  
  return
end subroutine bbmg_operator_restriction


! ================================= !
! Computation of Laplacian operator !
! ================================= !
subroutine bbmg_operator_laplacian(n)
  use bbmg_operator
  implicit none
  
  integer, intent(in) :: n
  integer :: fi,fj,fk
  integer :: ci,cj,ck
  integer :: ri,rj,rk
  integer :: li,lj,lk
  integer :: pi,pj,pk
  integer :: si,sj,sk
  
  ! Zero lapc2f
  lvl(n-1)%lapc2f=0.0_WP
  
  ! Loop over fine cells
  do fk=lvl(n-1)%kmin_,lvl(n-1)%kmax_
     do fj=lvl(n-1)%jmin_,lvl(n-1)%jmax_
        do fi=lvl(n-1)%imin_,lvl(n-1)%imax_
           
           ! Loop over laplacian stencil
           do lk=-1,+1
              do lj=-1,+1
                 do li=-1,+1
                    
                    ! Loop over prolongation stencil
                    do pk=0,pmodz(fk+lk,n-1)
                       do pj=0,pmody(fj+lj,n-1)
                          do pi=0,pmodx(fi+li,n-1)
                             
                             si = pi + (1-pmodx(fi,n-1))*min(li,0) + (1-pmodx(fi+li,n-1))*max(li,0)
                             sj = pj + (1-pmody(fj,n-1))*min(lj,0) + (1-pmody(fj+lj,n-1))*max(lj,0)
                             sk = pk + (1-pmodz(fk,n-1))*min(lk,0) + (1-pmodz(fk+lk,n-1))*max(lk,0)
                             
                             lvl(n-1)%lapc2f(fi,fj,fk,si,sj,sk) = lvl(n-1)%lapc2f(fi,fj,fk,si,sj,sk) &
                                  + lvl(n-1)%lap(fi,fj,fk,li,lj,lk) &
                                  * lvl(n-1)%c2f(fi+li,fj+lj,fk+lk,pi,pj,pk)
                             
                          end do
                       end do
                    end do
                    
                 end do
              end do
           end do
           
        end do
     end do
  end do
  
  ! Communicate operator
  call bbmg_com_update_op(lvl(n-1)%lapc2f,n-1,3)
  
  ! Zero Laplacian
  lvl(n)%lap=0.0_WP
  
  ! Loop over coarse cells
  do ck=lvl(n)%kmin_,lvl(n)%kmax_
     do cj=lvl(n)%jmin_,lvl(n)%jmax_
        do ci=lvl(n)%imin_,lvl(n)%imax_
           
           ! Find corresponding fine cell
           fi=2*ci-1; fj=2*cj-1; fk=2*ck-1
           
           ! Loop over restriction stencil
           do rk=-pmodz(fk-1,n-1),+pmodz(fk+1,n-1)
              do rj=-pmody(fj-1,n-1),+pmody(fj+1,n-1)
                 do ri=-pmodx(fi-1,n-1),+pmodx(fi+1,n-1)
                    
                    ! Loop over lapc2f stencil
                    do lk=-1+pmodz(fk+rk,n-1),+1
                       do lj=-1+pmody(fj+rj,n-1),+1
                          do li=-1+pmodx(fi+ri,n-1),+1
                             
                             si = (ri-1)/2+li
                             sj = (rj-1)/2+lj
                             sk = (rk-1)/2+lk
                             
                             lvl(n)%lap(ci,cj,ck,si,sj,sk) = lvl(n)%lap(ci,cj,ck,si,sj,sk) &
                                  + lvl(n)%f2c(ci,cj,ck,ri,rj,rk) &
                                  * lvl(n-1)%lapc2f(fi+ri,fj+rj,fk+rk,li,lj,lk)
                             
                          end do
                       end do
                    end do
                    
                 end do
              end do
           end do
           
        end do
     end do
  end do
  
  ! Communicate operator
  call bbmg_com_update_op(lvl(n)%lap,n,3)
  
  return
end subroutine bbmg_operator_laplacian


! ======================================== !
! Application of the prolongation operator !
! ======================================== !
subroutine bbmg_operator_c2f(Ac,nc,Af,nf)
  use bbmg_operator
  implicit none
  
  integer, intent(in) :: nc
  integer, intent(in) :: nf
  real(WP), dimension(lvl(nc)%imino_:lvl(nc)%imaxo_,lvl(nc)%jmino_:lvl(nc)%jmaxo_,lvl(nc)%kmino_:lvl(nc)%kmaxo_) :: Ac
  real(WP), dimension(lvl(nf)%imino_:lvl(nf)%imaxo_,lvl(nf)%jmino_:lvl(nf)%jmaxo_,lvl(nf)%kmino_:lvl(nf)%kmaxo_) :: Af
  integer :: fi,fj,fk
  integer :: ci,cj,ck
  
  ! Check levels
  if (nc.ne.nf+1) call die('BBMG: grid levels are incompatible with prolongation.')
  
  ! Apply prolongation operator
  do fk=lvl(nf)%kmin_,lvl(nf)%kmax_
     do fj=lvl(nf)%jmin_,lvl(nf)%jmax_
        do fi=lvl(nf)%imin_,lvl(nf)%imax_
           
           ! Find corresponding coarse cell
           ci=(fi+1)/2; cj=(fj+1)/2; ck=(fk+1)/2
           
           Af(fi,fj,fk) = sum(lvl(nf)%c2f(fi,fj,fk,:,:,:)*Ac(ci:ci+1,cj:cj+1,ck:ck+1))
           
        end do
     end do
  end do
  
  ! Communicate vector
  call bbmg_com_update(Af,nf)
  
  return
end subroutine bbmg_operator_c2f


! ======================================= !
! Application of the restriction operator !
! ======================================= !
subroutine bbmg_operator_f2c(Af,nf,Ac,nc)
  use bbmg_operator
  implicit none
  
  integer, intent(in) :: nf
  integer, intent(in) :: nc
  real(WP), dimension(lvl(nf)%imino_:lvl(nf)%imaxo_,lvl(nf)%jmino_:lvl(nf)%jmaxo_,lvl(nf)%kmino_:lvl(nf)%kmaxo_) :: Af
  real(WP), dimension(lvl(nc)%imino_:lvl(nc)%imaxo_,lvl(nc)%jmino_:lvl(nc)%jmaxo_,lvl(nc)%kmino_:lvl(nc)%kmaxo_) :: Ac
  integer :: fi,fj,fk
  integer :: ci,cj,ck
  
  ! Check levels
  if (nc.ne.nf+1) call die('BBMG: grid levels are incompatible with restriction.')
  
  ! Apply restriction operator
  do ck=lvl(nc)%kmin_,lvl(nc)%kmax_
     do cj=lvl(nc)%jmin_,lvl(nc)%jmax_
        do ci=lvl(nc)%imin_,lvl(nc)%imax_
           
           ! Find corresponding fine cell
           fi=2*ci-1; fj=2*cj-1; fk=2*ck-1
           
           Ac(ci,cj,ck) = sum(lvl(nc)%f2c(ci,cj,ck,:,:,:)*Af(fi-1:fi+1,fj-1:fj+1,fk-1:fk+1))
           
        end do
     end do
  end do
  
  ! Communicate vector
  call bbmg_com_update(Ac,nc)
  
  return
end subroutine bbmg_operator_f2c


! =========================== !
! Computation of the residual !
! =========================== !
subroutine bbmg_operator_residual(n)
  use bbmg_operator
  implicit none
  
  integer, intent(in) :: n
  integer :: i,j,k
  
  ! Compute residual
  do k=lvl(n)%kmin_,lvl(n)%kmax_
     do j=lvl(n)%jmin_,lvl(n)%jmax_
        do i=lvl(n)%imin_,lvl(n)%imax_
           
           lvl(n)%r(i,j,k) = lvl(n)%f(i,j,k) &
                - sum(lvl(n)%lap(i,j,k,:,:,:)*lvl(n)%v(i-1:i+1,j-1:j+1,k-1:k+1))
           
        end do
     end do
  end do
  
  ! Communicate vector
  call bbmg_com_update(lvl(n)%r,n)
  
  return
end subroutine bbmg_operator_residual
