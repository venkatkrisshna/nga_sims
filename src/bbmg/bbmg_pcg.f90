! =========================================== !
! Preconditioned Conjugate Gradient with      !
! Parallel Black-Box Multigrid preconditioner !
! =========================================== !
module bbmg_pcg
  use bbmg
  implicit none
  
  ! Work arrays
  real(WP), dimension(:,:,:), allocatable :: res,pp,zz,Ap
  
  ! Coefficients
  real(WP) :: alpha,beta,rho1,rho2
  
end module bbmg_pcg


! =================================== !
! Initialization of the PCG algorithm !
! =================================== !
subroutine bbmg_pcg_init
  use bbmg_pcg
  implicit none
  
  ! Allocation of work arrays
  allocate(res(imin_ :imax_ ,jmin_ :jmax_ ,kmin_ :kmax_ )); res=0.0_WP
  allocate(Ap (imin_ :imax_ ,jmin_ :jmax_ ,kmin_ :kmax_ )); Ap =0.0_WP
  allocate(pp (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); pp =0.0_WP
  allocate(zz (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); zz =0.0_WP
  
  ! Initialize BBMG preconditioned
  call bbmg_init
  
  return
end subroutine bbmg_pcg_init


! =============== !
! PCG-BBMG solver !
! =============== !
subroutine bbmg_pcg_solve
  use bbmg_pcg
  use pressure
  use time_info
  implicit none
  
  integer  :: idone,iter
  real(WP) :: normres,normres0
  
  ! If variable operator, recompute hierarchy
  if (MGcst.eq.2 .or. &
      MGcst.eq.1.and.niter.eq.1) call bbmg_operator_compute
  
  ! Loop initialization
  iter  = 0
  idone = 0
  
  ! res=RP-A.DP
  call bbmg_pcg_operator(DP,res)
  res=RP-res
  
  ! Check initial residual norm
  call parallel_max(maxval(abs(res)),normres0)
  normres=normres0
  if (normres0.eq.0.0_WP) idone = 1
  
  ! Conjugate gradient
  loop:do while(idone.ne.1)
     
     ! Increment loop counter
     iter=iter+1
     
     ! M.ww=res
     call bbmg_pcg_precond(res,zz)
     
     ! (ww.res)
     rho2=rho1
     call parallel_sum(sum(zz(imin_:imax_,jmin_:jmax_,kmin_:kmax_)*res),rho1)
     
     ! beta=rho1/rho2
     if (iter.eq.1) then
        beta=0.0_WP
     else
        beta=rho1/rho2
     end if
     
     ! pp=zz+beta.pp
     pp=zz+beta*pp
     
     ! Ap=A.pp
     call bbmg_pcg_operator(pp,Ap)
     
     ! alpha=rho1/(pp.Ap)
     call parallel_sum(sum(pp(imin_:imax_,jmin_:jmax_,kmin_:kmax_)*Ap),alpha)
     alpha=rho1/alpha
     
     ! DP=DP+alpha.pp
     DP=DP+alpha*pp
     
     ! res=res-alpha.Ap
     res=res-alpha*Ap
     
     ! Check if done
     call parallel_max(maxval(abs(res)),normres)
     normres=normres/normres0
     if (normres.le.MGcvg .or. iter.ge.MGit) idone=1
     
  end do loop
  
  ! Transfer to monitor
  max_resP = normres
  it_p  = iter
  
  return
end subroutine bbmg_pcg_solve


! =================== !
! BBMG preconditioner !
! =================== !
subroutine bbmg_pcg_precond(A,B)
  use bbmg_pcg
  implicit none
  
  real(WP), dimension(imin_ :imax_ ,jmin_ :jmax_ ,kmin_ :kmax_ ) :: A
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: B
  integer :: iter
  integer :: i,j,k
  integer :: fi,fj,fk
  
  ! Transfer RHS
  lvl(1)%f=0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           fi=i-nover;fj=j-nover;fk=k-nover
           lvl(1)%f(fi,fj,fk)=A(i,j,k)
        end do
     end do
  end do
  call bbmg_com_update(lvl(1)%f,1)
  
  ! Zero initial guess ?
  lvl(1)%v=0.0_WP
  
  ! Perform a MG cycle
  call bbmg_cycle(1)
  
  ! Return solution
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           fi=i-nover;fj=j-nover;fk=k-nover
           B(i,j,k)=lvl(1)%v(fi,fj,fk)
        end do
     end do
  end do
  
  return
end subroutine bbmg_pcg_precond


! =========================== !
! Application of the operator !
! =========================== !
subroutine bbmg_pcg_operator(A,B)
  use metric_velocity_conv
  implicit none
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: A
  real(WP), dimension(imin_ :imax_ ,jmin_ :jmax_ ,kmin_ :kmax_ ) :: B
  integer :: i,j,k
  
  ! Communicate A
  call boundary_update_border(A,'+','ym')
  
  ! Apply operator
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           B(i,j,k) = &
                sum(A(i-stcp:i+stcp,j,k)*lap(i,j,k,1,:)) + & ! X-direction
                sum(A(i,j-stcp:j+stcp,k)*lap(i,j,k,2,:)) + & ! Y-direction
                sum(A(i,j,k-stcp:k+stcp)*lap(i,j,k,3,:))     ! Z-direction
        end do
     end do
  end do
  
  return
end subroutine bbmg_pcg_operator
