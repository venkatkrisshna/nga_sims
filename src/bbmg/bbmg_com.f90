! ====================================== !
! Communication routines for BBMG solver !
! ====================================== !
module bbmg_com
  use bbmg
  use bbmg_grid
  implicit none
  
  ! Cartesian rank info
  integer, dimension(:,:,:), allocatable :: rank
  
end module bbmg_com


! ======================================== !
! Initialization of communication routines !
! ======================================== !
subroutine bbmg_com_init
  use bbmg_com
  implicit none
  
  integer :: ip,jp,kp,n,ierr
  integer :: my_min,my_max,hisrank
  integer, dimension(3) :: ploc
  integer, dimension(:), pointer :: ncello_rank
  integer, dimension(:), allocatable :: mino_,min_,max_,maxo_
  integer, dimension(:), pointer :: buf
  
  ! Prepare rank(iproc,jproc,kproc)
  allocate(rank(npx,npy,npz))
  do kp=1,npz
     do jp=1,npy
        do ip=1,npx
           ploc(1)=ip-1;ploc(2)=jp-1;ploc(3)=kp-1
           call MPI_CART_RANK(comm,ploc,rank(ip,jp,kp),ierr)
        end do
     end do
  end do
  rank=rank+1
  
  ! Allocate index arrays
  allocate(buf(nproc),mino_(nproc),min_(nproc),max_(nproc),maxo_(nproc))
  
  ! Allocate ncell arrays
  allocate(ncello_rank(nproc))
  
  ! For each level
  do n=1,nlvl
     
     ! Gather mesh sizes at this level
     ncello_rank=0;call MPI_allgather(lvl(n)%ncello_,1,MPI_INTEGER,ncello_rank,1,MPI_INTEGER,comm,ierr)
     
     ! X DIRECTION ----------------------------------------------------------------------------------
     
     ! Allocate send and receive arrays
     allocate(lvl(n)%send_xm(npx));lvl(n)%send_xm=.false.;lvl(n)%recv_xm=MPI_PROC_NULL
     allocate(lvl(n)%send_xp(npx));lvl(n)%send_xp=.false.;lvl(n)%recv_xp=MPI_PROC_NULL
     
     ! Gather indices
     buf=0;buf(irank)=lvl(n)%imino_;call MPI_allreduce(buf,mino_,nproc,MPI_INTEGER,MPI_SUM,comm,ierr)
     buf=0;buf(irank)=lvl(n)%imin_ ;call MPI_allreduce(buf,min_ ,nproc,MPI_INTEGER,MPI_SUM,comm,ierr)
     buf=0;buf(irank)=lvl(n)%imax_ ;call MPI_allreduce(buf,max_ ,nproc,MPI_INTEGER,MPI_SUM,comm,ierr)
     buf=0;buf(irank)=lvl(n)%imaxo_;call MPI_allreduce(buf,maxo_,nproc,MPI_INTEGER,MPI_SUM,comm,ierr)
     
     ! Check if I'm a sender
     if (lvl(n)%ncello_.gt.0.and.lvl(n)%imin_.le.lvl(n)%imax_) then
        ! Set indices
        my_min=lvl(n)%imin_
        my_max=lvl(n)%imax_
        ! Find receivers
        do ip=1,npx
           ! Get processor rank
           hisrank=rank(ip,jproc,kproc)
           ! Check if he can be a receiver
           if (ncello_rank(hisrank).le.0) cycle
           ! Test if he's a receiver - careful about periodicity
           if (xper.eq.1) then
              if (mod(maxo_(hisrank),lvl(n)%nx).eq.mod(my_min,lvl(n)%nx)) lvl(n)%send_xm(ip)=.true.
              if (mod(mino_(hisrank),lvl(n)%nx).eq.mod(my_max,lvl(n)%nx)) lvl(n)%send_xp(ip)=.true.
           else
              if (maxo_(hisrank).eq.my_min) lvl(n)%send_xm(ip)=.true.
              if (mino_(hisrank).eq.my_max) lvl(n)%send_xp(ip)=.true.
           end if
        end do
     end if
     
     ! Count number of send operations
     lvl(n)%nsend_xm=0;lvl(n)%nsend_xp=0
     do ip=1,npx
        if (lvl(n)%send_xm(ip)) lvl(n)%nsend_xm=lvl(n)%nsend_xm+1
        if (lvl(n)%send_xp(ip)) lvl(n)%nsend_xp=lvl(n)%nsend_xp+1
     end do
        
     ! Check if I'm a receiver
     if (lvl(n)%ncello_.gt.0) then
        ! Set indices
        my_max=lvl(n)%imaxo_
        my_min=lvl(n)%imino_
        ! Find sender
        do ip=1,npx
           ! Get processor rank
           hisrank=rank(ip,jproc,kproc)
           ! Check if he can be a sender
           if (ncello_rank(hisrank).le.0.or.min_(hisrank).gt.max_(hisrank)) cycle
           ! Test if he's the sender - careful about periodicity
           if (xper.eq.1) then
              if (mod(min_(hisrank),lvl(n)%nx).eq.mod(my_max,lvl(n)%nx)) lvl(n)%recv_xm=hisrank-1
              if (mod(max_(hisrank),lvl(n)%nx).eq.mod(my_min,lvl(n)%nx)) lvl(n)%recv_xp=hisrank-1
           else
              if (min_(hisrank).eq.my_max) lvl(n)%recv_xm=hisrank-1
              if (max_(hisrank).eq.my_min) lvl(n)%recv_xp=hisrank-1
           end if
        end do
     end if
     
     ! Y DIRECTION ----------------------------------------------------------------------------------
     
     ! Allocate send and receive arrays
     allocate(lvl(n)%send_ym(npy));lvl(n)%send_ym=.false.;lvl(n)%recv_ym=MPI_PROC_NULL
     allocate(lvl(n)%send_yp(npy));lvl(n)%send_yp=.false.;lvl(n)%recv_yp=MPI_PROC_NULL
     
     ! Gather indices
     buf=0;buf(irank)=lvl(n)%jmino_;call MPI_allreduce(buf,mino_,nproc,MPI_INTEGER,MPI_SUM,comm,ierr)
     buf=0;buf(irank)=lvl(n)%jmin_ ;call MPI_allreduce(buf,min_ ,nproc,MPI_INTEGER,MPI_SUM,comm,ierr)
     buf=0;buf(irank)=lvl(n)%jmax_ ;call MPI_allreduce(buf,max_ ,nproc,MPI_INTEGER,MPI_SUM,comm,ierr)
     buf=0;buf(irank)=lvl(n)%jmaxo_;call MPI_allreduce(buf,maxo_,nproc,MPI_INTEGER,MPI_SUM,comm,ierr)
     
     ! Check if I'm a sender
     if (lvl(n)%ncello_.gt.0.and.lvl(n)%jmin_.le.lvl(n)%jmax_) then
        ! Set indices
        my_min=lvl(n)%jmin_
        my_max=lvl(n)%jmax_
        ! Find receivers
        do jp=1,npy
           ! Get processor rank
           hisrank=rank(iproc,jp,kproc)
           ! Check if he can be a receiver
           if (ncello_rank(hisrank).le.0) cycle
           ! Test if he's a receiver - careful about periodicity
           if (yper.eq.1) then
              if (mod(maxo_(hisrank),lvl(n)%ny).eq.mod(my_min,lvl(n)%ny)) lvl(n)%send_ym(jp)=.true.
              if (mod(mino_(hisrank),lvl(n)%ny).eq.mod(my_max,lvl(n)%ny)) lvl(n)%send_yp(jp)=.true.
           else
              if (maxo_(hisrank).eq.my_min) lvl(n)%send_ym(jp)=.true.
              if (mino_(hisrank).eq.my_max) lvl(n)%send_yp(jp)=.true.
           end if
        end do
     end if
     
     ! Count number of send operations
     lvl(n)%nsend_ym=0;lvl(n)%nsend_yp=0
     do jp=1,npy
        if (lvl(n)%send_ym(jp)) lvl(n)%nsend_ym=lvl(n)%nsend_ym+1
        if (lvl(n)%send_yp(jp)) lvl(n)%nsend_yp=lvl(n)%nsend_yp+1
     end do
     
     ! Check if I'm a receiver
     if (lvl(n)%ncello_.gt.0) then
        ! Set indices
        my_max=lvl(n)%jmaxo_
        my_min=lvl(n)%jmino_
        ! Find sender
        do jp=1,npy
           ! Get processor rank
           hisrank=rank(iproc,jp,kproc)
           ! Check if he can be a sender
           if (ncello_rank(hisrank).le.0.or.min_(hisrank).gt.max_(hisrank)) cycle
           ! Test if he's the sender - careful about periodicity
           if (yper.eq.1) then
              if (mod(min_(hisrank),lvl(n)%ny).eq.mod(my_max,lvl(n)%ny)) lvl(n)%recv_ym=hisrank-1
              if (mod(max_(hisrank),lvl(n)%ny).eq.mod(my_min,lvl(n)%ny)) lvl(n)%recv_yp=hisrank-1
           else
              if (min_(hisrank).eq.my_max) lvl(n)%recv_ym=hisrank-1
              if (max_(hisrank).eq.my_min) lvl(n)%recv_yp=hisrank-1
           end if
        end do
     end if
     
     ! Z DIRECTION ----------------------------------------------------------------------------------
     
     ! Allocate send and receive arrays
     allocate(lvl(n)%send_zm(npz));lvl(n)%send_zm=.false.;lvl(n)%recv_zm=MPI_PROC_NULL
     allocate(lvl(n)%send_zp(npz));lvl(n)%send_zp=.false.;lvl(n)%recv_zp=MPI_PROC_NULL
     
     ! Gather indices
     buf=0;buf(irank)=lvl(n)%kmino_;call MPI_allreduce(buf,mino_,nproc,MPI_INTEGER,MPI_SUM,comm,ierr)
     buf=0;buf(irank)=lvl(n)%kmin_ ;call MPI_allreduce(buf,min_ ,nproc,MPI_INTEGER,MPI_SUM,comm,ierr)
     buf=0;buf(irank)=lvl(n)%kmax_ ;call MPI_allreduce(buf,max_ ,nproc,MPI_INTEGER,MPI_SUM,comm,ierr)
     buf=0;buf(irank)=lvl(n)%kmaxo_;call MPI_allreduce(buf,maxo_,nproc,MPI_INTEGER,MPI_SUM,comm,ierr)
     
     ! Check if I'm a sender
     if (lvl(n)%ncello_.gt.0.and.lvl(n)%kmin_.le.lvl(n)%kmax_) then
        ! Set indices
        my_min=lvl(n)%kmin_
        my_max=lvl(n)%kmax_
        ! Find receivers
        do kp=1,npz
           ! Get processor rank
           hisrank=rank(iproc,jproc,kp)
           ! Check if he can be a receiver
           if (ncello_rank(hisrank).le.0) cycle
           ! Test if he's a receiver - careful about periodicity
           if (zper.eq.1) then
              if (mod(maxo_(hisrank),lvl(n)%nz).eq.mod(my_min,lvl(n)%nz)) lvl(n)%send_zm(kp)=.true.
              if (mod(mino_(hisrank),lvl(n)%nz).eq.mod(my_max,lvl(n)%nz)) lvl(n)%send_zp(kp)=.true.
           else
              if (maxo_(hisrank).eq.my_min) lvl(n)%send_zm(kp)=.true.
              if (mino_(hisrank).eq.my_max) lvl(n)%send_zp(kp)=.true.
           end if
        end do
     end if
     
     ! Count number of send operations
     lvl(n)%nsend_zm=0;lvl(n)%nsend_zp=0
     do kp=1,npz
        if (lvl(n)%send_zm(kp)) lvl(n)%nsend_zm=lvl(n)%nsend_zm+1
        if (lvl(n)%send_zp(kp)) lvl(n)%nsend_zp=lvl(n)%nsend_zp+1
     end do
     
     ! Check if I'm a receiver
     if (lvl(n)%ncello_.gt.0) then
        ! Set indices
        my_max=lvl(n)%kmaxo_
        my_min=lvl(n)%kmino_
        ! Find sender
        do kp=1,npz
           ! Get processor rank
           hisrank=rank(iproc,jproc,kp)
           ! Check if he can be a sender
           if (ncello_rank(hisrank).le.0.or.min_(hisrank).gt.max_(hisrank)) cycle
           ! Test if he's the sender - careful about periodicity
           if (zper.eq.1) then
              if (mod(min_(hisrank),lvl(n)%nz).eq.mod(my_max,lvl(n)%nz)) lvl(n)%recv_zm=hisrank-1
              if (mod(max_(hisrank),lvl(n)%nz).eq.mod(my_min,lvl(n)%nz)) lvl(n)%recv_zp=hisrank-1
           else
              if (min_(hisrank).eq.my_max) lvl(n)%recv_zm=hisrank-1
              if (max_(hisrank).eq.my_min) lvl(n)%recv_zp=hisrank-1
           end if
        end do
     end if
     
!!$     ! ========= START DEBUGGING ==================================================
!!$     if (irank.eq.1) then
!!$        
!!$        ! Level
!!$        print*,'Level',n
!!$        
!!$        ! Global sizes
!!$        print*,'Global size - i',lvl(n)%imin,lvl(n)%imax
!!$        print*,'Global size - j',lvl(n)%jmin,lvl(n)%jmax
!!$        print*,'Global size - k',lvl(n)%kmin,lvl(n)%kmax
!!$        
!!$     end if
!!$     
!!$     ! Local sizes
!!$     do ip=1,nproc
!!$        if (ip.eq.irank) then
!!$           print*,'Processor rank','[',irank,']'
!!$           print*,'Processor cart loc',iproc,jproc,kproc
!!$           print*,'Local size - i',lvl(n)%imin_,lvl(n)%imax_
!!$           print*,'Local size - j',lvl(n)%jmin_,lvl(n)%jmax_
!!$           print*,'Local size - k',lvl(n)%kmin_,lvl(n)%kmax_
!!$        end if
!!$        call MPI_BARRIER(comm,ierr)
!!$     end do
!!$     
!!$     ! Communication data
!!$     do ip=1,nproc
!!$        if (ip.eq.irank) then
!!$           print*,'Processor rank','[',irank,']'
!!$           print*,'Sending to - xm',lvl(n)%send_xm
!!$           print*,'Sending to - xp',lvl(n)%send_xp
!!$           print*,'Sending to - ym',lvl(n)%send_ym
!!$           print*,'Sending to - yp',lvl(n)%send_yp
!!$           print*,'Sending to - zm',lvl(n)%send_zm
!!$           print*,'Sending to - zp',lvl(n)%send_zp
!!$           print*,'Receiving from - xm',lvl(n)%recv_xm
!!$           print*,'Receiving from - xp',lvl(n)%recv_xp
!!$           print*,'Receiving from - ym',lvl(n)%recv_ym
!!$           print*,'Receiving from - yp',lvl(n)%recv_yp
!!$           print*,'Receiving from - zm',lvl(n)%recv_zm
!!$           print*,'Receiving from - zp',lvl(n)%recv_zp
!!$        end if
!!$        call MPI_BARRIER(comm,ierr)
!!$     end do
!!$     ! ========= END DEBUGGING ====================================================
     
  end do
  
  ! Deallocate
  deallocate(buf);nullify(buf)
  deallocate(ncello_rank);nullify(ncello_rank)
  
  return
end subroutine bbmg_com_init


! ================================ !
! Communication routine at level n !
! ================================ !
subroutine bbmg_com_update(A,n)
  use bbmg_com
  implicit none
  
  integer :: n
  real(WP), dimension(lvl(n)%nxo_,lvl(n)%nyo_,lvl(n)%nzo_) :: A
  
  ! Communicate vector in 3D
  call bbmg_com_update_x(A,n,lvl(n)%nxo_,lvl(n)%nyo_,lvl(n)%nzo_,nov)
  call bbmg_com_update_y(A,n,lvl(n)%nxo_,lvl(n)%nyo_,lvl(n)%nzo_,nov)
  call bbmg_com_update_z(A,n,lvl(n)%nxo_,lvl(n)%nyo_,lvl(n)%nzo_,nov)
  
  return
end subroutine bbmg_com_update


! ================================ !
! Communication routine at level n !
! for a ((nop)**3) pts operator    !
! ================================ !
subroutine bbmg_com_update_op(A,n,nop)
  use bbmg_com
  implicit none
  
  integer :: n,nop
  real(WP), dimension(lvl(n)%nxo_,lvl(n)%nyo_,lvl(n)%nzo_,nop,nop,nop) :: A
  
  ! Communicate operator in 3D
  call bbmg_com_update_op_x(A,n,lvl(n)%nxo_,lvl(n)%nyo_,lvl(n)%nzo_,nov,nop)
  call bbmg_com_update_op_y(A,n,lvl(n)%nxo_,lvl(n)%nyo_,lvl(n)%nzo_,nov,nop)
  call bbmg_com_update_op_z(A,n,lvl(n)%nxo_,lvl(n)%nyo_,lvl(n)%nzo_,nov,nop)
  
  return
end subroutine bbmg_com_update_op


! ======================================= !
! Communication at level n in x direction !
! ======================================= !
subroutine bbmg_com_update_x(A,n,n1,n2,n3,no)
  use bbmg_com
  implicit none
  
  integer, intent(in) :: n,n1,n2,n3,no
  real(WP), dimension(n1,n2,n3), intent(inout) :: A
  real(WP), dimension(:,:,:), pointer :: buf1,buf2
  integer, dimension(:), pointer :: req
  integer :: icount,ierr,dest,ip,count
  integer, dimension(:,:), pointer :: req_stat
  
  ! Initialize buffer
  allocate(buf1(no,n2,n3))
  allocate(buf2(no,n2,n3))
  icount = no*n2*n3
  
  ! Initialize requests
  allocate(req_stat(MPI_STATUS_SIZE,max(lvl(n)%nsend_xm,lvl(n)%nsend_xp)+1))
  allocate(req(max(lvl(n)%nsend_xm,lvl(n)%nsend_xp)+1))
  
  ! MINUS DIRECTION
  ! Loop over potential destinations
  count=0
  do ip=1,npx
     ! Test if I need to send
     if (lvl(n)%send_xm(ip)) then
        ! Get processor rank
        dest=rank(ip,jproc,kproc)-1
        ! Copy left buffer
        buf1(:,:,:) = A(1+no:1+no+no-1,:,:)
        ! Send left buffer to destination
        count=count+1
        call MPI_ISEND(buf1,icount,MPI_REAL_WP,dest,0,comm,req(count),ierr)
     end if
  end do
  
  ! Receive from potential sender
  count=count+1
  call MPI_IRECV(buf2,icount,MPI_REAL_WP,lvl(n)%recv_xm,0,comm,req(count),ierr)
  
  ! Wait for completion
  call MPI_WAITALL(count,req(1:count),req_stat(:,1:count),ierr)
  
  ! Paste left buffer to right
  if (lvl(n)%recv_xm.NE.MPI_PROC_NULL) A(n1-no+1:n1,:,:) = buf2(:,:,:)
  
  ! PLUS DIRECTION
  ! Loop over potential destinations
  count=0
  do ip=1,npx
     ! Test if I need to send
     if (lvl(n)%send_xp(ip)) then
        ! Get processor rank
        dest=rank(ip,jproc,kproc)-1
        ! Copy right buffer
        buf1(:,:,:) = A(n1-no-no+1:n1-no,:,:)
        ! Send right buffer to destination
        count=count+1
        call MPI_ISEND(buf1,icount,MPI_REAL_WP,dest,0,comm,req(count),ierr)
     end if
  end do
  
  ! Receive from potential sender
  count=count+1
  call MPI_IRECV(buf2,icount,MPI_REAL_WP,lvl(n)%recv_xp,0,comm,req(count),ierr)
  
  ! Wait for completion
  call MPI_WAITALL(count,req(1:count),req_stat(:,1:count),ierr)
  
  ! Paste left buffer to right
  if (lvl(n)%recv_xp.NE.MPI_PROC_NULL) A(1:no,:,:) = buf2(:,:,:)
  
  ! Deallocate
  deallocate(buf1);nullify(buf1)
  deallocate(buf2);nullify(buf2)
  deallocate(req);nullify(req)
  deallocate(req_stat);nullify(req_stat)
  
  return
end subroutine bbmg_com_update_x


! ======================================= !
! Communication at level n in y direction !
! ======================================= !
subroutine bbmg_com_update_y(A,n,n1,n2,n3,no)
  use bbmg_com
  implicit none
  
  integer, intent(in) :: n,n1,n2,n3,no
  real(WP), dimension(n1,n2,n3), intent(inout) :: A
  real(WP), dimension(:,:,:), pointer :: buf1,buf2
  integer, dimension(:), pointer :: req
  integer :: icount,ierr,dest,jp,count
  integer, dimension(:,:), pointer :: req_stat
  
  ! Initialize buffer
  allocate(buf1(n1,no,n3))
  allocate(buf2(n1,no,n3))
  icount = n1*no*n3
  
  ! Initialize requests
  allocate(req_stat(MPI_STATUS_SIZE,max(lvl(n)%nsend_ym,lvl(n)%nsend_yp)+1))
  allocate(req(max(lvl(n)%nsend_ym,lvl(n)%nsend_yp)+1))
  
  ! MINUS DIRECTION
  ! Loop over potential destinations
  count=0
  do jp=1,npy
     ! Test if I need to send
     if (lvl(n)%send_ym(jp)) then
        ! Get processor rank
        dest=rank(iproc,jp,kproc)-1
        ! Copy left buffer
        buf1(:,:,:) = A(:,1+no:1+no+no-1,:)
        ! Send left buffer to destination
        count=count+1
        call MPI_ISEND(buf1,icount,MPI_REAL_WP,dest,0,comm,req(count),ierr)
     end if
  end do
  
  ! Receive from potential sender
  count=count+1
  call MPI_IRECV(buf2,icount,MPI_REAL_WP,lvl(n)%recv_ym,0,comm,req(count),ierr)
  
  ! Wait for completion
  call MPI_WAITALL(count,req(1:count),req_stat(:,1:count),ierr)
  
  ! Paste left buffer to right
  if (lvl(n)%recv_ym.NE.MPI_PROC_NULL) A(:,n2-no+1:n2,:) = buf2(:,:,:)
  
  ! PLUS DIRECTION
  ! Loop over potential destinations
  count=0
  do jp=1,npy
     ! Test if I need to send
     if (lvl(n)%send_yp(jp)) then
        ! Get processor rank
        dest=rank(iproc,jp,kproc)-1
        ! Copy right buffer
        buf1(:,:,:) = A(:,n2-no-no+1:n2-no,:)
        ! Send right buffer to destination
        count=count+1
        call MPI_ISEND(buf1,icount,MPI_REAL_WP,dest,0,comm,req(count),ierr)
     end if
  end do
  
  ! Receive from potential sender
  count=count+1
  call MPI_IRECV(buf2,icount,MPI_REAL_WP,lvl(n)%recv_yp,0,comm,req(count),ierr)
  
  ! Wait for completion
  call MPI_WAITALL(count,req(1:count),req_stat(:,1:count),ierr)
  
  ! Paste left buffer to right
  if (lvl(n)%recv_yp.NE.MPI_PROC_NULL) A(:,1:no,:) = buf2(:,:,:)
  
  ! Deallocate
  deallocate(buf1);nullify(buf1)
  deallocate(buf2);nullify(buf2)
  deallocate(req);nullify(req)
  deallocate(req_stat);nullify(req_stat)
  
  return
end subroutine bbmg_com_update_y


! ======================================= !
! Communication at level n in z direction !
! ======================================= !
subroutine bbmg_com_update_z(A,n,n1,n2,n3,no)
  use bbmg_com
  implicit none
  
  integer, intent(in) :: n,n1,n2,n3,no
  real(WP), dimension(n1,n2,n3), intent(inout) :: A
  real(WP), dimension(:,:,:), pointer :: buf1,buf2
  integer, dimension(:), pointer :: req
  integer :: icount,ierr,dest,kp,count
  integer, dimension(:,:), pointer :: req_stat
  
  ! Initialize buffer
  allocate(buf1(n1,n2,no))
  allocate(buf2(n1,n2,no))
  icount = n1*n2*no
  
  ! Initialize requests
  allocate(req_stat(MPI_STATUS_SIZE,max(lvl(n)%nsend_zm,lvl(n)%nsend_zp)+1))
  allocate(req(max(lvl(n)%nsend_zm,lvl(n)%nsend_zp)+1))
  
  ! MINUS DIRECTION
  ! Loop over potential destinations
  count=0
  do kp=1,npz
     ! Test if I need to send
     if (lvl(n)%send_zm(kp)) then
        ! Get processor rank
        dest=rank(iproc,jproc,kp)-1
        ! Copy left buffer
        buf1(:,:,:) = A(:,:,1+no:1+no+no-1)
        ! Send left buffer to destination
        count=count+1
        call MPI_ISEND(buf1,icount,MPI_REAL_WP,dest,0,comm,req(count),ierr)
     end if
  end do
  
  ! Receive from potential sender
  count=count+1
  call MPI_IRECV(buf2,icount,MPI_REAL_WP,lvl(n)%recv_zm,0,comm,req(count),ierr)
  
  ! Wait for completion
  call MPI_WAITALL(count,req(1:count),req_stat(:,1:count),ierr)
  
  ! Paste left buffer to right
  if (lvl(n)%recv_zm.NE.MPI_PROC_NULL) A(:,:,n3-no+1:n3) = buf2(:,:,:)
  
  ! PLUS DIRECTION
  ! Loop over potential destinations
  count=0
  do kp=1,npz
     ! Test if I need to send
     if (lvl(n)%send_zp(kp)) then
        ! Get processor rank
        dest=rank(iproc,jproc,kp)-1
        ! Copy right buffer
        buf1(:,:,:) = A(:,:,n3-no-no+1:n3-no)
        ! Send right buffer to destination
        count=count+1
        call MPI_ISEND(buf1,icount,MPI_REAL_WP,dest,0,comm,req(count),ierr)
     end if
  end do
  
  ! Receive from potential sender
  count=count+1
  call MPI_IRECV(buf2,icount,MPI_REAL_WP,lvl(n)%recv_zp,0,comm,req(count),ierr)
  
  ! Wait for completion
  call MPI_WAITALL(count,req(1:count),req_stat(:,1:count),ierr)
  
  ! Paste left buffer to right
  if (lvl(n)%recv_zp.NE.MPI_PROC_NULL) A(:,:,1:no) = buf2(:,:,:)
  
  ! Deallocate
  deallocate(buf1);nullify(buf1)
  deallocate(buf2);nullify(buf2)
  deallocate(req);nullify(req)
  deallocate(req_stat);nullify(req_stat)
  
  return
end subroutine bbmg_com_update_z


! ======================================= !
! Communication at level n in x direction !
! ======================================= !
subroutine bbmg_com_update_op_x(A,n,n1,n2,n3,no,nop)
  use bbmg_com
  implicit none
  
  integer, intent(in) :: n,n1,n2,n3,no,nop
  real(WP), dimension(n1,n2,n3,nop,nop,nop), intent(inout) :: A
  real(WP), dimension(:,:,:,:,:,:), pointer :: buf1,buf2
  integer, dimension(:), pointer :: req
  integer :: icount,ierr,dest,ip,count
  integer, dimension(:,:), pointer :: req_stat
  
  ! Initialize buffer
  allocate(buf1(no,n2,n3,nop,nop,nop))
  allocate(buf2(no,n2,n3,nop,nop,nop))
  icount = no*n2*n3*nop**3
  
  ! Initialize requests
  allocate(req_stat(MPI_STATUS_SIZE,max(lvl(n)%nsend_xm,lvl(n)%nsend_xp)+1))
  allocate(req(max(lvl(n)%nsend_xm,lvl(n)%nsend_xp)+1))
  
  ! MINUS DIRECTION
  ! Loop over potential destinations
  count=0
  do ip=1,npx
     ! Test if I need to send
     if (lvl(n)%send_xm(ip)) then
        ! Get processor rank
        dest=rank(ip,jproc,kproc)-1
        ! Copy left buffer
        buf1(:,:,:,:,:,:) = A(1+no:1+no+no-1,:,:,:,:,:)
        ! Send left buffer to destination
        count=count+1
        call MPI_ISEND(buf1,icount,MPI_REAL_WP,dest,0,comm,req(count),ierr)
     end if
  end do
  
  ! Receive from potential sender
  count=count+1
  call MPI_IRECV(buf2,icount,MPI_REAL_WP,lvl(n)%recv_xm,0,comm,req(count),ierr)
  
  ! Wait for completion
  call MPI_WAITALL(count,req(1:count),req_stat(:,1:count),ierr)
  
  ! Paste left buffer to right
  if (lvl(n)%recv_xm.NE.MPI_PROC_NULL) A(n1-no+1:n1,:,:,:,:,:) = buf2(:,:,:,:,:,:)
  
  ! PLUS DIRECTION
  ! Loop over potential destinations
  count=0
  do ip=1,npx
     ! Test if I need to send
     if (lvl(n)%send_xp(ip)) then
        ! Get processor rank
        dest=rank(ip,jproc,kproc)-1
        ! Copy right buffer
        buf1(:,:,:,:,:,:) = A(n1-no-no+1:n1-no,:,:,:,:,:)
        ! Send right buffer to destination
        count=count+1
        call MPI_ISEND(buf1,icount,MPI_REAL_WP,dest,0,comm,req(count),ierr)
     end if
  end do
  
  ! Receive from potential sender
  count=count+1
  call MPI_IRECV(buf2,icount,MPI_REAL_WP,lvl(n)%recv_xp,0,comm,req(count),ierr)
  
  ! Wait for completion
  call MPI_WAITALL(count,req(1:count),req_stat(:,1:count),ierr)
  
  ! Paste left buffer to right
  if (lvl(n)%recv_xp.NE.MPI_PROC_NULL) A(1:no,:,:,:,:,:) = buf2(:,:,:,:,:,:)
  
  ! Deallocate
  deallocate(buf1);nullify(buf1)
  deallocate(buf2);nullify(buf2)
  deallocate(req);nullify(req)
  deallocate(req_stat);nullify(req_stat)
  
  return
end subroutine bbmg_com_update_op_x


! ======================================= !
! Communication at level n in y direction !
! ======================================= !
subroutine bbmg_com_update_op_y(A,n,n1,n2,n3,no,nop)
  use bbmg_com
  implicit none
  
  integer, intent(in) :: n,n1,n2,n3,no,nop
  real(WP), dimension(n1,n2,n3,nop,nop,nop), intent(inout) :: A
  real(WP), dimension(:,:,:,:,:,:), pointer :: buf1,buf2
  integer, dimension(:), pointer :: req
  integer :: icount,ierr,dest,jp,count
  integer, dimension(:,:), pointer :: req_stat
  
  ! Initialize buffer
  allocate(buf1(n1,no,n3,nop,nop,nop))
  allocate(buf2(n1,no,n3,nop,nop,nop))
  icount = n1*no*n3*nop**3
  
  ! Initialize requests
  allocate(req_stat(MPI_STATUS_SIZE,max(lvl(n)%nsend_ym,lvl(n)%nsend_yp)+1))
  allocate(req(max(lvl(n)%nsend_ym,lvl(n)%nsend_yp)+1))
  
  ! MINUS DIRECTION
  ! Loop over potential destinations
  count=0
  do jp=1,npy
     ! Test if I need to send
     if (lvl(n)%send_ym(jp)) then
        ! Get processor rank
        dest=rank(iproc,jp,kproc)-1
        ! Copy left buffer
        buf1(:,:,:,:,:,:) = A(:,1+no:1+no+no-1,:,:,:,:)
        ! Send left buffer to destination
        count=count+1
        call MPI_ISEND(buf1,icount,MPI_REAL_WP,dest,0,comm,req(count),ierr)
     end if
  end do
  
  ! Receive from potential sender
  count=count+1
  call MPI_IRECV(buf2,icount,MPI_REAL_WP,lvl(n)%recv_ym,0,comm,req(count),ierr)
  
  ! Wait for completion
  call MPI_WAITALL(count,req(1:count),req_stat(:,1:count),ierr)
  
  ! Paste left buffer to right
  if (lvl(n)%recv_ym.NE.MPI_PROC_NULL) A(:,n2-no+1:n2,:,:,:,:) = buf2(:,:,:,:,:,:)
  
  ! PLUS DIRECTION
  ! Loop over potential destinations
  count=0
  do jp=1,npy
     ! Test if I need to send
     if (lvl(n)%send_yp(jp)) then
        ! Get processor rank
        dest=rank(iproc,jp,kproc)-1
        ! Copy right buffer
        buf1(:,:,:,:,:,:) = A(:,n2-no-no+1:n2-no,:,:,:,:)
        ! Send right buffer to destination
        count=count+1
        call MPI_ISEND(buf1,icount,MPI_REAL_WP,dest,0,comm,req(count),ierr)
     end if
  end do
  
  ! Receive from potential sender
  count=count+1
  call MPI_IRECV(buf2,icount,MPI_REAL_WP,lvl(n)%recv_yp,0,comm,req(count),ierr)
  
  ! Wait for completion
  call MPI_WAITALL(count,req(1:count),req_stat(:,1:count),ierr)
  
  ! Paste left buffer to right
  if (lvl(n)%recv_yp.NE.MPI_PROC_NULL) A(:,1:no,:,:,:,:) = buf2(:,:,:,:,:,:)
  
  ! Deallocate
  deallocate(buf1);nullify(buf1)
  deallocate(buf2);nullify(buf2)
  deallocate(req);nullify(req)
  deallocate(req_stat);nullify(req_stat)
  
  return
end subroutine bbmg_com_update_op_y


! ======================================= !
! Communication at level n in z direction !
! ======================================= !
subroutine bbmg_com_update_op_z(A,n,n1,n2,n3,no,nop)
  use bbmg_com
  implicit none
  
  integer, intent(in) :: n,n1,n2,n3,no,nop
  real(WP), dimension(n1,n2,n3,nop,nop,nop), intent(inout) :: A
  real(WP), dimension(:,:,:,:,:,:), pointer :: buf1,buf2
  integer, dimension(:), pointer :: req
  integer :: icount,ierr,dest,kp,count
  integer, dimension(:,:), pointer :: req_stat
  
  ! Initialize buffer
  allocate(buf1(n1,n2,no,nop,nop,nop))
  allocate(buf2(n1,n2,no,nop,nop,nop))
  icount = n1*n2*no*nop**3
  
  ! Initialize requests
  allocate(req_stat(MPI_STATUS_SIZE,max(lvl(n)%nsend_zm,lvl(n)%nsend_zp)+1))
  allocate(req(max(lvl(n)%nsend_zm,lvl(n)%nsend_zp)+1))
  
  ! MINUS DIRECTION
  ! Loop over potential destinations
  count=0
  do kp=1,npz
     ! Test if I need to send
     if (lvl(n)%send_zm(kp)) then
        ! Get processor rank
        dest=rank(iproc,jproc,kp)-1
        ! Copy left buffer
        buf1(:,:,:,:,:,:) = A(:,:,1+no:1+no+no-1,:,:,:)
        ! Send left buffer to destination
        count=count+1
        call MPI_ISEND(buf1,icount,MPI_REAL_WP,dest,0,comm,req(count),ierr)
     end if
  end do
  
  ! Receive from potential sender
  count=count+1
  call MPI_IRECV(buf2,icount,MPI_REAL_WP,lvl(n)%recv_zm,0,comm,req(count),ierr)
  
  ! Wait for completion
  call MPI_WAITALL(count,req(1:count),req_stat(:,1:count),ierr)
  
  ! Paste left buffer to right
  if (lvl(n)%recv_zm.NE.MPI_PROC_NULL) A(:,:,n3-no+1:n3,:,:,:) = buf2(:,:,:,:,:,:)
  
  ! PLUS DIRECTION
  ! Loop over potential destinations
  count=0
  do kp=1,npz
     ! Test if I need to send
     if (lvl(n)%send_zp(kp)) then
        ! Get processor rank
        dest=rank(iproc,jproc,kp)-1
        ! Copy right buffer
        buf1(:,:,:,:,:,:) = A(:,:,n3-no-no+1:n3-no,:,:,:)
        ! Send right buffer to destination
        count=count+1
        call MPI_ISEND(buf1,icount,MPI_REAL_WP,dest,0,comm,req(count),ierr)
     end if
  end do
  
  ! Receive from potential sender
  count=count+1
  call MPI_IRECV(buf2,icount,MPI_REAL_WP,lvl(n)%recv_zp,0,comm,req(count),ierr)
  
  ! Wait for completion
  call MPI_WAITALL(count,req(1:count),req_stat(:,1:count),ierr)
  
  ! Paste left buffer to right
  if (lvl(n)%recv_zp.NE.MPI_PROC_NULL) A(:,:,1:no,:,:,:) = buf2(:,:,:,:,:,:)
  
  ! Deallocate
  deallocate(buf1);nullify(buf1)
  deallocate(buf2);nullify(buf2)
  deallocate(req);nullify(req)
  deallocate(req_stat);nullify(req_stat)
  
  return
end subroutine bbmg_com_update_op_z
