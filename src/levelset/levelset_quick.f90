module levelset_quick
  use levelset
  use data
  use metric_generic
  implicit none
  
  ! Coefficients for the QUICK interpolation
  real(WP), dimension(:,:,:), allocatable :: quick_xp,quick_yp,quick_zp
  real(WP), dimension(:,:,:), allocatable :: quick_xm,quick_ym,quick_zm
  
end module levelset_quick

! =========================================== !
! Initialize the metrics for the QUICK scheme !
! =========================================== !
subroutine levelset_quick_init
  use levelset_quick
  use masks
  use math
  implicit none
  
  integer :: i,j,k
  
  ! Allocate the arrays
  allocate(quick_xp(imin_:imax_,jmin_:jmax_,-2:+1))
  allocate(quick_yp(imin_:imax_,jmin_:jmax_,-2:+1))
  allocate(quick_zp(imin_:imax_,jmin_:jmax_,-2:+1))
  allocate(quick_xm(imin_:imax_,jmin_:jmax_,-1:+2))
  allocate(quick_ym(imin_:imax_,jmin_:jmax_,-1:+2))
  allocate(quick_zm(imin_:imax_,jmin_:jmax_,-1:+2))
  
  ! Quick is 3rd order
  k = kmin_
  do j=jmin_,jmax_
     do i=imin_,imax_
        
        ! Get the coefficients for X differentiation
        call hofdd(4,xm(i-2:i+1),xm(i),quick_xp(i,j,-2:+1))
        call hofdd(4,xm(i-1:i+2),xm(i),quick_xm(i,j,-1:+2))
        
        ! Get the coefficients for Y differentiation
        call hofdd(4,ym(j-2:j+1),ym(j),quick_yp(i,j,-2:+1))
        call hofdd(4,ym(j-1:j+2),ym(j),quick_ym(i,j,-1:+2))
        
        ! Get the coefficients for Z differentiation
        call hofdd(4,zm(k-2:k+1),zm(k),quick_zp(i,j,-2:+1))
        call hofdd(4,zm(k-1:k+2),zm(k),quick_zm(i,j,-1:+2))
        
        ! Consider walls in X direction
        if (mask(i-2,j).eq.1 .or. mask(i-2,j).eq.3) then
           quick_xp(i,j,-1) = quick_xp(i,j,-1) + quick_xp(i,j,-2)
           quick_xp(i,j,-2) = 0.0_WP
        end if
        if (mask(i-1,j).eq.1 .or. mask(i-1,j).eq.3) then
           quick_xp(i,j, 0) = quick_xp(i,j, 0) + quick_xp(i,j,-1) + quick_xp(i,j,-2)
           quick_xp(i,j,-1) = 0.0_WP
           quick_xp(i,j,-2) = 0.0_WP
           quick_xm(i,j, 0) = quick_xm(i,j, 0) + quick_xm(i,j,-1)
           quick_xm(i,j,-1) = 0.0_WP
        end if
        if (mask(i+1,j).eq.1 .or. mask(i+1,j).eq.3) then
           quick_xp(i,j, 0) = quick_xp(i,j, 0) + quick_xp(i,j,+1)
           quick_xp(i,j,+1) = 0.0_WP 
           quick_xm(i,j, 0) = quick_xm(i,j, 0) + quick_xm(i,j,+1) + quick_xm(i,j,+2)
           quick_xm(i,j,+1) = 0.0_WP
           quick_xm(i,j,+2) = 0.0_WP
        end if
        if (mask(i+2,j).eq.1 .or. mask(i+2,j).eq.3) then
           quick_xm(i,j,+1) = quick_xm(i,j,+1) + quick_xm(i,j,+2)
           quick_xm(i,j,+2) = 0.0_WP
        end if
        
        ! Consider walls in Y direction
        if (mask(i,j-2).eq.1 .or. mask(i,j-2).eq.3) then
           quick_yp(i,j,-1) = quick_yp(i,j,-1) + quick_yp(i,j,-2)
           quick_yp(i,j,-2) = 0.0_WP
        end if
        if (mask(i,j-1).eq.1 .or. mask(i,j-1).eq.3) then
           quick_yp(i,j, 0) = quick_yp(i,j, 0) + quick_yp(i,j,-1) + quick_yp(i,j,-2)
           quick_yp(i,j,-1) = 0.0_WP
           quick_yp(i,j,-2) = 0.0_WP
           quick_ym(i,j, 0) = quick_ym(i,j, 0) + quick_ym(i,j,-1)
           quick_ym(i,j,-1) = 0.0_WP
        end if
        if (mask(i,j+1).eq.1 .or. mask(i,j+1).eq.3) then
           quick_yp(i,j, 0) = quick_yp(i,j, 0) + quick_yp(i,j,+1)
           quick_yp(i,j,+1) = 0.0_WP 
           quick_ym(i,j, 0) = quick_ym(i,j, 0) + quick_ym(i,j,+1) + quick_ym(i,j,+2)
           quick_ym(i,j,+1) = 0.0_WP
           quick_ym(i,j,+2) = 0.0_WP
        end if
        if (mask(i,j+2).eq.1 .or. mask(i,j+2).eq.3) then
           quick_ym(i,j,+1) = quick_ym(i,j,+1) + quick_ym(i,j,+2)
           quick_ym(i,j,+2) = 0.0_WP
        end if
        
        ! In the walls set to zero
        if (mask(i,j).eq.1 .or. mask(i,j).eq.3) then
           quick_xp(i,j,:) = 0.0_WP
           quick_xm(i,j,:) = 0.0_WP
           quick_yp(i,j,:) = 0.0_WP
           quick_ym(i,j,:) = 0.0_WP
           quick_zp(i,j,:) = 0.0_WP
           quick_zm(i,j,:) = 0.0_WP
        end if

     end do
  end do

  ! Enforce cylindrical coordinates, if used
  if (icyl.eq.1) then
     do j=jmin_,jmax_
        do i=imin_,imax_
           quick_zp(i,j,:) = quick_zp(i,j,:) * ymi(j)
           quick_zm(i,j,:) = quick_zm(i,j,:) * ymi(j)
        end do
     end do
  end if
  
  ! Boundary Conditions
  ! -------------------
  
  ! In x
  ! - Periodic
  !   -> nothing to be done
  ! - Not Periodic
  !   -> left/right : Neumann
  if (xper.ne.1 .and. iproc.eq.1) then
     quick_xp(imin  ,:, 0) = quick_xp(imin,:,0) + quick_xp(imin,:,-1) + quick_xp(imin,:,-2)
     quick_xp(imin  ,:,-1) = 0.0_WP
     quick_xp(imin  ,:,-2) = 0.0_WP
     quick_xm(imin  ,:, 0) = quick_xm(imin,:,0) + quick_xm(imin,:,-1)
     quick_xm(imin  ,:,-1) = 0.0_WP
     quick_xp(imin+1,:,-1) = quick_xp(imin+1,:,-1) + quick_xp(imin+1,:,-2)
     quick_xp(imin+1,:,-2) = 0.0_WP
  end if
  if (xper.ne.1 .and. iproc.eq.npx) then
     quick_xm(imax  ,:, 0) = quick_xm(imax,:,0) + quick_xm(imax,:,+1) + quick_xm(imax,:,+2)
     quick_xm(imax  ,:,+1) = 0.0_WP
     quick_xm(imax  ,:,+2) = 0.0_WP
     quick_xp(imax  ,:, 0) = quick_xp(imax,:,0) + quick_xp(imax,:,+1)
     quick_xp(imax  ,:,+1) = 0.0_WP
     quick_xm(imax-1,:,+1) = quick_xm(imax-1,:,+1) + quick_xm(imax-1,:,+2)
     quick_xm(imax-1,:,+2) = 0.0_WP
  end if
  
  ! In y
  ! - Periodic
  !   -> nothing to be done
  ! - Not Periodic
  !   -> up/down : Neumann
  if (yper.ne.1 .and. jproc.eq.1 .and. icyl.eq.0) then
     quick_yp(:,jmin, 0) = quick_yp(:,jmin,0) + quick_yp(:,jmin,-1) + quick_yp(:,jmin,-2)
     quick_yp(:,jmin,-1) = 0.0_WP
     quick_yp(:,jmin,-2) = 0.0_WP
     quick_ym(:,jmin, 0) = quick_ym(:,jmin,0) + quick_ym(:,jmin,-1)
     quick_ym(:,jmin,-1) = 0.0_WP
     quick_yp(:,jmin+1,-1) = quick_yp(:,jmin+1,-1) + quick_yp(:,jmin+1,-2)
     quick_yp(:,jmin+1,-2) = 0.0_WP
  end if
  if (yper.ne.1 .and. jproc.eq.npy) then
     quick_ym(:,jmax, 0) = quick_ym(:,jmax,0) + quick_ym(:,jmax,+1) + quick_ym(:,jmax,+2)
     quick_ym(:,jmax,+1) = 0.0_WP
     quick_ym(:,jmax,+2) = 0.0_WP
     quick_yp(:,jmax, 0) = quick_yp(:,jmax,0) + quick_yp(:,jmax,+1)
     quick_yp(:,jmax,+1) = 0.0_WP
     quick_ym(:,jmax-1,+1) = quick_ym(:,jmax-1,+1) + quick_ym(:,jmax-1,+2)
     quick_ym(:,jmax-1,+2) = 0.0_WP
  end if
  
  ! In z
  ! - Periodic
  !   -> nothing to be done
  
  return
end subroutine levelset_quick_init


! =========================================================== !
! Compute the residuals of the levelset equation              !
!                                                             !
! - velocity field @ n stored in Uold                         !
! - levelset field @ n stored in Gmid                         !
!                                                             !
! 3 working arrays of size (at least) (nx_+1)*(ny_+1)*(nz_+1) !
! =========================================================== !
subroutine levelset_quick_residual
  use levelset_quick
  implicit none
  
  integer  :: i,j,k
  real(WP) :: rhs,ui,vi,wi
  
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           
           ui = sum(interp_u_xm(i,j,:)*U(i-st1:i+st2,j,k))
           vi = sum(interp_v_ym(i,j,:)*V(i,j-st1:j+st2,k))
           wi = sum(interp_w_zm(i,j,:)*W(i,j,k-st1:k+st2))
           
           rhs= - 0.5_WP * (ui+abs(ui)) * sum(quick_xp(i,j,:)*LVLSET(i-2:i+1,j,k)) &
                - 0.5_WP * (ui-abs(ui)) * sum(quick_xm(i,j,:)*LVLSET(i-1:i+2,j,k)) &
                - 0.5_WP * (vi+abs(vi)) * sum(quick_yp(i,j,:)*LVLSET(i,j-2:j+1,k)) &
                - 0.5_WP * (vi-abs(vi)) * sum(quick_ym(i,j,:)*LVLSET(i,j-1:j+2,k)) &
                - 0.5_WP * (wi+abs(wi)) * sum(quick_zp(i,j,:)*LVLSET(i,j,k-2:k+1)) &
                - 0.5_WP * (wi-abs(wi)) * sum(quick_zm(i,j,:)*LVLSET(i,j,k-1:k+2))
           
           ResLVLSET(i,j,k) = -2.0_WP*LVLSET(i,j,k)+LVLSETold(i,j,k) + &
                ( LVLSETold(i,j,k) + dt*rhs ) + dt*srcLVLSET(i,j,k)
        end do
     end do
  end do
  
  return
end subroutine levelset_quick_residual


! ==================================== !
! Inverse the residual of the levelset !
! By using approximate factorization   !
! ==================================== !
subroutine levelset_quick_inverse
  use levelset_quick
  use parallel
  use memory
  use implicit
  implicit none
  integer  :: i,j,k
  real(WP) :: ui,vi,wi
  real(WP) :: conv1,conv2
  real(WP) :: dt2
  
  dt2 = dt/2.0_WP
  
  ! If purely explicit return
  if (.not.implicit_any) return
  
  ! X-direction
  if (implicit_x) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              
              ui = sum(interp_u_xm(i,j,:)*U(i-st1:i+st2,j,k))
              conv1 = 0.5_WP*(ui+abs(ui))
              conv2 = 0.5_WP*(ui-abs(ui))
              
              Ax(j,k,i,-2) = dt2 * conv1 * quick_xp(i,j,-2)
              
              Ax(j,k,i,-1) = dt2 *(conv1 * quick_xp(i,j,-1) + conv2 * quick_xm(i,j,-1))
              
              Ax(j,k,i, 0) = 1.0_WP + &
                             dt2 *(conv1 * quick_xp(i,j, 0) + conv2 * quick_xm(i,j,0 ))
              
              Ax(j,k,i,+1) = dt2 *(conv1 * quick_xp(i,j,+1) + conv2 * quick_xm(i,j,+1))
              
              Ax(j,k,i,+2) = dt2 * conv2 * quick_xm(i,j,+2)
              
              Rx(j,k,i) = ResLVLSET(i,j,k)
              
           end do
        end do
     end do
     call linear_solver_x(5)
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Rx(j,k,i) = ResLVLSET(i,j,k)
           end do
        end do
     end do
  end if
  
  ! Y-direction
  if (implicit_y) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              
              vi = sum(interp_v_ym(i,j,:)*V(i,j-st1:j+st2,k))
              conv1 = 0.5_WP*(vi+abs(vi))
              conv2 = 0.5_WP*(vi-abs(vi))
              
              Ay(i,k,j,-2) = dt2 * conv1 * quick_yp(i,j,-2)
              
              Ay(i,k,j,-1) = dt2 *(conv1 * quick_yp(i,j,-1) + conv2 * quick_ym(i,j,-1))
              
              Ay(i,k,j, 0) = 1.0_WP + &
                             dt2 *(conv1 * quick_yp(i,j, 0) + conv2 * quick_ym(i,j, 0)) 
              
              Ay(i,k,j,+1) = dt2 *(conv1 * quick_yp(i,j,+1) + conv2 * quick_ym(i,j,+1))
              
              Ay(i,k,j,+2) = dt2 * conv2 * quick_ym(i,j,+2)
              
              Ry(i,k,j) = Rx(j,k,i)
              
           end do
        end do
     end do
     if (icyl.eq.1 .and. jproc.eq.1) then
        do k=kmin_,kmax_
           Ay(:,k,jmin+1,-1) = Ay(:,k,jmin+1,-1) + Ay(:,k,jmin+1,-2)
           Ay(:,k,jmin+1,-2) = 0.0_WP
           Ay(:,k,jmin, 0) = Ay(:,k,jmin, 0) + Ay(:,k,jmin,-1) + Ay(:,k,jmin,-2)
           Ay(:,k,jmin,-1) = 0.0_WP
           Ay(:,k,jmin,-2) = 0.0_WP
        end do
     end if  
     call linear_solver_y(5)
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              Ry(i,k,j) = Rx(j,k,i)
           end do
        end do
     end do
  end if
  
  ! Z-direction
  if (implicit_z) then
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_

              wi = sum(interp_w_zm(i,j,:)*W(i,j,k-st1:k+st2))
              conv1 = 0.5_WP*(wi+abs(wi))
              conv2 = 0.5_WP*(wi+abs(wi))
              
              Az(i,j,k,-2) = dt2 * conv1 * quick_zp(i,j,-2)
              
              Az(i,j,k,-1) = dt2 *(conv1 * quick_zp(i,j,-1) + conv2 * quick_zm(i,j,-1))
              
              Az(i,j,k, 0) = 1.0_WP + &
                             dt2 *(conv1 * quick_zp(i,j, 0) + conv2 * quick_zm(i,j, 0))
                           
              Az(i,j,k,+1) = dt2 *(conv1 * quick_zp(i,j,+1) + conv2 * quick_zm(i,j,+1))
              
              Az(i,j,k,+2) = dt2 * conv2 * quick_zm(i,j,+2)
              
              Rz(i,j,k) = Ry(i,k,j)
              
           end do
        end do
     end do
     call linear_solver_z(5)
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ResLVLSET(i,j,k) = Rz(i,j,k)
           end do
        end do
     end do
  else
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ResLVLSET(i,j,k) = Ry(i,k,j)
           end do
        end do
     end do
  end if
  
  return
end subroutine levelset_quick_inverse
