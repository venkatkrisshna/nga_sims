!===============================================================================
!===============================================================================

module levelset_fmm
  use levelset
  use precision
  use partition
  implicit none
  
  ! Do we need to reinit?
  logical :: fmm_reinit
  
  integer, parameter :: fmm_far_plus           = 1, &
                        fmm_far_minus          = 2, &
                        fmm_close_plus         = 3, &
                        fmm_close_minus        = 4, &
                        fmm_accepted_plus      = 5, &
                        fmm_accepted_minus     = 6, &
                        fmm_tmp                = 7

  ! FMM data arrays
  real(WP), dimension(:,:,:), allocatable :: phi_fmm
  real(WP), dimension(:,:,:), allocatable :: phi_vel
  real(WP), dimension(:,:,:), allocatable :: phi_vel_init
  integer,  dimension(:,:,:), allocatable :: phi_flag
  integer,  dimension(:,:,:), allocatable :: vel_set
  
  real(WP), dimension(:,:,:), allocatable :: lvlset_normal_x_old
  real(WP), dimension(:,:,:), allocatable :: lvlset_normal_y_old
  real(WP), dimension(:,:,:), allocatable :: lvlset_normal_z_old

  ! FMM parallel passing info arrays
  integer :: rank_x_low
  integer :: rank_x_high
  integer :: rank_y_low
  integer :: rank_y_high
  integer :: rank_z_low
  integer :: rank_z_high
  
  integer  :: i_passlo, i_passhi
  integer  :: j_passlo, j_passhi
  integer  :: k_passlo, k_passhi
  
  ! MPI buffering...
  integer(1), dimension(:), allocatable :: mpi_buffer
  integer :: mpi_buffer_size
  
  integer, parameter :: local_counter_max = 50

  ! Number of nodes on each side of the front
  integer :: n_plus, n_minus
    
  ! Variables for calculating distances and velocities
  integer                  :: n_nbrs
  integer,  dimension(6)   :: index_nbrs
  real(WP), dimension(3,6) :: dx_nbrs
  real(WP), dimension(6)   :: phi_nbrs
  real(WP), dimension(6)   :: vel_nbrs
  real(WP)                 :: phi_me
  real(WP)                 :: vel_me

  integer :: close_plus_count
  integer :: close_minus_count

  integer, dimension(:,:), pointer :: close_plus_ijk
  integer, dimension(:,:), pointer :: close_minus_ijk
  integer, dimension(:,:), pointer :: close_ijk

  integer, dimension(:,:), pointer :: accepted_ijk

  ! I,J,K mins and maxes for close nodes
  integer :: imin_close, imax_close
  integer :: jmin_close, jmax_close
  integer :: kmin_close, kmax_close

contains

  !=============================================================================

  subroutine fmm_phi_calc(local_phi,radius)
    implicit none
    integer                  :: i,j
    real(WP), intent(out)    :: local_phi
    real(WP), intent(in)     :: radius
    real(WP), dimension(6)   :: s_local
    real(WP)                 :: root1
    integer                  :: local_1, local_2, local_3
    integer                  :: local_4, local_5, local_6, local_7

    ! Account for cylindrical coords
    dx_nbrs(3,:) = dx_nbrs(3,:) * radius

    ! Check for zeros
    j = n_nbrs
    do i = 1,j
       s_local(i) = sqrt(sum(dx_nbrs(1:3,i)**2))
       if (s_local(i).le.1.0e-10_WP) then
          local_phi = phi_nbrs(i)
          n_nbrs = 0
       end if
    end do

    ! How many neighbors can be used in the computation?
    select case (n_nbrs)

      case(0) 
        ! do nothing: local_phi already set

      case(1)
        local_phi = phi_nbrs(1) + sqrt(sum(dx_nbrs(1:3,1)**2))

      case(2)
        ! 2 possible cases - 1) the two nbrs either lie across 
        ! node from each other, or 2) they don't

        if (abs(index_nbrs(1)).eq.abs(index_nbrs(2))) then
           ! Nbrs on both sides of the node
           local_phi = phi_nbrs(1) + sqrt(sum(dx_nbrs(1:3,1)**2))
           local_phi = min(local_phi,phi_nbrs(2) + sqrt(sum(dx_nbrs(1:3,2)**2)))
        else
           ! Nbrs only on one side of the node
           call fmm_phi_calc_2D(phi_nbrs(1)   ,phi_nbrs(2)   ,   &
                                dx_nbrs(1:3,1),dx_nbrs(1:3,2),local_phi) 
        end if

      case(3)
        ! 2 possible cases - 1) any two of the nbrs lie across 
        ! node from each other, or 2) no two nbrs do

        ! First check for the crossing cases
        if     (abs(index_nbrs(1)).eq.abs(index_nbrs(2))) then
           ! Two cases
           ! #1 --------------------------------------------
           local_1 = 1
           local_2 = 3
           call fmm_phi_calc_2D(phi_nbrs(local_1)   ,phi_nbrs(local_2)   ,   &
                          dx_nbrs(1:3,local_1),dx_nbrs(1:3,local_2),local_phi) 
           
           ! #2 --------------------------------------------
           local_1 = 2
           local_2 = 3
           call fmm_phi_calc_2D(phi_nbrs(local_1)   ,phi_nbrs(local_2)   ,   &
                          dx_nbrs(1:3,local_1),dx_nbrs(1:3,local_2),root1) 
           local_phi =  min(local_phi,root1)

        elseif (abs(index_nbrs(2)).eq.abs(index_nbrs(3))) then
           ! Two cases
           ! #1 --------------------------------------------
           local_1 = 1
           local_2 = 2
           call fmm_phi_calc_2D(phi_nbrs(local_1)   ,phi_nbrs(local_2)   ,   &
                          dx_nbrs(1:3,local_1),dx_nbrs(1:3,local_2),local_phi) 
           
           ! #2 --------------------------------------------
           local_1 = 1
           local_2 = 3
           call fmm_phi_calc_2D(phi_nbrs(local_1)   ,phi_nbrs(local_2)   ,   &
                          dx_nbrs(1:3,local_1),dx_nbrs(1:3,local_2),root1) 
           local_phi =  min(local_phi,root1)

        else
           ! And now the non-crossed case: three nbrs all along diffrnt dirns
           call fmm_phi_calc_3D(phi_nbrs(1)   ,phi_nbrs(2)   ,phi_nbrs(3),    &
                                dx_nbrs(1:3,1),dx_nbrs(1:3,2),dx_nbrs(1:3,3), &
                                local_phi) 
        end if

      case(4)
        ! 2 possible cases: either 1) both sets of nbrs lie across node 
        ! from each other, or 2) only one set of nbrs does
        if (abs(index_nbrs(1)).eq.abs(index_nbrs(2))) then
           if (abs(index_nbrs(3)).eq.abs(index_nbrs(4))) then
              ! Try 4 different 2D cases
              ! #1 --------------------------------------------
              local_1 = 1
              local_2 = 3
              call fmm_phi_calc_2D(phi_nbrs(local_1)   ,phi_nbrs(local_2)   , &
                                   dx_nbrs(1:3,local_1),dx_nbrs(1:3,local_2), &
                                   local_phi) 
              
              ! #2 --------------------------------------------
              local_1 = 1
              local_2 = 4
              call fmm_phi_calc_2D(phi_nbrs(local_1)   ,phi_nbrs(local_2)   , &
                                   dx_nbrs(1:3,local_1),dx_nbrs(1:3,local_2), &
                                   root1) 
              local_phi =  min(local_phi,root1)
              
              ! #3 --------------------------------------------
              local_1 = 2
              local_2 = 3
              call fmm_phi_calc_2D(phi_nbrs(local_1)   ,phi_nbrs(local_2)   , &
                                   dx_nbrs(1:3,local_1),dx_nbrs(1:3,local_2), &
                                   root1) 
              local_phi =  min(local_phi,root1)
              
              ! #4 --------------------------------------------
              local_1 = 2
              local_2 = 4
              call fmm_phi_calc_2D(phi_nbrs(local_1)   ,phi_nbrs(local_2)   , &
                                   dx_nbrs(1:3,local_1),dx_nbrs(1:3,local_2), &
                                   root1) 
              local_phi =  min(local_phi,root1)
              
           else
              ! Try 2 different 3D cases
              ! #1 --------------------------------------------
              local_1 = 1
              local_2 = 3
              local_3 = 4
              call fmm_phi_calc_3D(phi_nbrs(local_1)   ,phi_nbrs(local_2),    &
                                   phi_nbrs(local_3)   ,dx_nbrs(1:3,local_1), &
                                   dx_nbrs(1:3,local_2),dx_nbrs(1:3,local_3), &
                                   local_phi) 
              
              ! #2 --------------------------------------------
              local_1 = 2
              local_2 = 3
              local_3 = 4
              call fmm_phi_calc_3D(phi_nbrs(local_1)   ,phi_nbrs(local_2),    &
                                   phi_nbrs(local_3)   ,dx_nbrs(1:3,local_1), &
                                   dx_nbrs(1:3,local_2),dx_nbrs(1:3,local_3), &
                                   root1) 
              local_phi =  min(local_phi,root1)

           end if

        elseif (abs(index_nbrs(2)).eq.abs(index_nbrs(3))) then
           ! Try 2 different 3D cases
           ! #1 --------------------------------------------
           local_1 = 1
           local_2 = 2
           local_3 = 4
           call fmm_phi_calc_3D(phi_nbrs(local_1)   ,phi_nbrs(local_2),    &
                                phi_nbrs(local_3)   ,dx_nbrs(1:3,local_1), &
                                dx_nbrs(1:3,local_2),dx_nbrs(1:3,local_3), &
                                local_phi) 
             
           ! #2 --------------------------------------------
           local_1 = 1
           local_2 = 3
           local_3 = 4
           call fmm_phi_calc_3D(phi_nbrs(local_1)   ,phi_nbrs(local_2),    &
                                phi_nbrs(local_3)   ,dx_nbrs(1:3,local_1), &
                                dx_nbrs(1:3,local_2),dx_nbrs(1:3,local_3), &
                                root1) 
           local_phi =  min(local_phi,root1)

        else 
           ! Try 2 different 3D cases
           ! #1 --------------------------------------------
           local_1 = 1
           local_2 = 2
           local_3 = 3
           call fmm_phi_calc_3D(phi_nbrs(local_1)   ,phi_nbrs(local_2),    &
                                phi_nbrs(local_3)   ,dx_nbrs(1:3,local_1), &
                                dx_nbrs(1:3,local_2),dx_nbrs(1:3,local_3), &
                                local_phi) 
             
           ! #2 --------------------------------------------
           local_1 = 1
           local_2 = 2
           local_3 = 4
           call fmm_phi_calc_3D(phi_nbrs(local_1)   ,phi_nbrs(local_2),    &
                                phi_nbrs(local_3)   ,dx_nbrs(1:3,local_1), &
                                dx_nbrs(1:3,local_2),dx_nbrs(1:3,local_3), &
                                root1) 
           local_phi =  min(local_phi,root1)
        
        end if
      
      case(5)
        ! Only 1 case - test using the 4 possible planes and take smallest soln
        ! Renumber so that the 5th node has no one across from him
        if (abs(index_nbrs(1)).ne.abs(index_nbrs(2))) then
           local_1 = 1

           local_4 = 2
           local_5 = 3
           local_6 = 4
           local_7 = 5
        elseif (abs(index_nbrs(3)).ne.abs(index_nbrs(4))) then
           local_1 = 3
         
           local_4 = 1
           local_5 = 2
           local_6 = 4
           local_7 = 5
        else 
           local_1 = 5

           local_4 = 1
           local_5 = 2
           local_6 = 3
           local_7 = 4
        end if
           
        local_2 = local_4
        local_3 = local_6
        call fmm_phi_calc_3D(phi_nbrs(local_1)   ,phi_nbrs(local_2),    &
                             phi_nbrs(local_3)   ,dx_nbrs(1:3,local_1), &
                             dx_nbrs(1:3,local_2),dx_nbrs(1:3,local_3), &
                             local_phi) 
             
        local_2 = local_4
        local_3 = local_7
        call fmm_phi_calc_3D(phi_nbrs(local_1)   ,phi_nbrs(local_2),    &
                             phi_nbrs(local_3)   ,dx_nbrs(1:3,local_1), &
                             dx_nbrs(1:3,local_2),dx_nbrs(1:3,local_3), &
                             root1) 
        local_phi =  min(local_phi,root1)
        
        local_2 = local_5
        local_3 = local_6
        call fmm_phi_calc_3D(phi_nbrs(local_1)   ,phi_nbrs(local_2),    &
                             phi_nbrs(local_3)   ,dx_nbrs(1:3,local_1), &
                             dx_nbrs(1:3,local_2),dx_nbrs(1:3,local_3), &
                             root1) 
        local_phi =  min(local_phi,root1)
        
        local_2 = local_5
        local_3 = local_7
        call fmm_phi_calc_3D(phi_nbrs(local_1)   ,phi_nbrs(local_2),    &
                             phi_nbrs(local_3)   ,dx_nbrs(1:3,local_1), &
                             dx_nbrs(1:3,local_2),dx_nbrs(1:3,local_3), &
                             root1) 
        local_phi =  min(local_phi,root1)
        
      case(6)
        ! Only 1 case - test using the 8 possible planes and take smallest soln
        local_1 = 1
        local_2 = 3
        local_3 = 5
        call fmm_phi_calc_3D(phi_nbrs(local_1)   ,phi_nbrs(local_2),    &
                             phi_nbrs(local_3)   ,dx_nbrs(1:3,local_1), &
                             dx_nbrs(1:3,local_2),dx_nbrs(1:3,local_3), &
                             local_phi) 
             
        local_1 = 1
        local_2 = 3
        local_3 = 6
        call fmm_phi_calc_3D(phi_nbrs(local_1)   ,phi_nbrs(local_2),    &
                             phi_nbrs(local_3)   ,dx_nbrs(1:3,local_1), &
                             dx_nbrs(1:3,local_2),dx_nbrs(1:3,local_3), &
                             root1) 
        local_phi =  min(local_phi,root1)
        
        local_1 = 1
        local_2 = 4
        local_3 = 5
        call fmm_phi_calc_3D(phi_nbrs(local_1)   ,phi_nbrs(local_2),    &
                             phi_nbrs(local_3)   ,dx_nbrs(1:3,local_1), &
                             dx_nbrs(1:3,local_2),dx_nbrs(1:3,local_3), &
                             root1) 
        local_phi =  min(local_phi,root1)
        
        local_1 = 1
        local_2 = 4
        local_3 = 6
        call fmm_phi_calc_3D(phi_nbrs(local_1)   ,phi_nbrs(local_2),    &
                             phi_nbrs(local_3)   ,dx_nbrs(1:3,local_1), &
                             dx_nbrs(1:3,local_2),dx_nbrs(1:3,local_3), &
                             root1) 
        local_phi =  min(local_phi,root1)
        
        local_1 = 2
        local_2 = 3
        local_3 = 5
        call fmm_phi_calc_3D(phi_nbrs(local_1)   ,phi_nbrs(local_2),    &
                             phi_nbrs(local_3)   ,dx_nbrs(1:3,local_1), &
                             dx_nbrs(1:3,local_2),dx_nbrs(1:3,local_3), &
                             root1) 
        local_phi =  min(local_phi,root1)
        
        local_1 = 2
        local_2 = 4
        local_3 = 5
        call fmm_phi_calc_3D(phi_nbrs(local_1)   ,phi_nbrs(local_2),    &
                             phi_nbrs(local_3)   ,dx_nbrs(1:3,local_1), &
                             dx_nbrs(1:3,local_2),dx_nbrs(1:3,local_3), &
                             root1) 
        local_phi =  min(local_phi,root1)
        
        local_1 = 2
        local_2 = 3
        local_3 = 6
        call fmm_phi_calc_3D(phi_nbrs(local_1)   ,phi_nbrs(local_2),    &
                             phi_nbrs(local_3)   ,dx_nbrs(1:3,local_1), &
                             dx_nbrs(1:3,local_2),dx_nbrs(1:3,local_3), &
                             root1) 
        local_phi =  min(local_phi,root1)

        local_1 = 2
        local_2 = 4
        local_3 = 6
        call fmm_phi_calc_3D(phi_nbrs(local_1)   ,phi_nbrs(local_2),    &
                             phi_nbrs(local_3)   ,dx_nbrs(1:3,local_1), &
                             dx_nbrs(1:3,local_2),dx_nbrs(1:3,local_3), &
                             root1) 
        local_phi =  min(local_phi,root1)

      case default
        call die('Error: n_nbrs out of range')

    end select

    return
  end subroutine fmm_phi_calc

  !=============================================================================
  
  subroutine fmm_phi_calc_2D(G1,G2,dx1,dx2,G_out)
    implicit none
    real(WP), intent(in)               :: G1,G2
    real(WP), dimension(3), intent(in) :: dx1,dx2
    real(WP), intent(out)              :: G_out
    real(WP)                           :: a,b,c,d1,d2,root1,root2
    real(WP)                           :: d1i,d2i
           
    ! Square of the distance between nodes
    d1 = sum(dx1(1:3)**2)
    d2 = sum(dx2(1:3)**2)

    ! Take the inverse
    d1i = 1.0_WP/d1
    d2i = 1.0_WP/d2

    ! Value of G given by root of quadratic eqn
    a = d1i + d2i
    b = -2.0_WP*G1*d1i -2.0_WP*G2*d2i
    c = (G1**2)*d1i + (G2**2)*d2i - 1.0_WP

    root1 = (-b + sqrt(max(b**2 - 4.0_WP*a*c,0.0_WP)))/(2.0_WP*a)
    root2 = (-b - sqrt(max(b**2 - 4.0_WP*a*c,0.0_WP)))/(2.0_WP*a)

    ! Have to be careful about selecting these roots...
    ! Don't want negative values
    if (root1.le.0.0_WP) root1 = huge(1.0_WP)
    if (root2.le.0.0_WP) root2 = huge(1.0_WP)
    ! Also don't want values smaller than our original two G values
    if (root1.le.G1)     root1 = huge(1.0_WP)
    if (root1.le.G2)     root1 = huge(1.0_WP)
    if (root2.le.G1)     root2 = huge(1.0_WP)
    if (root2.le.G2)     root2 = huge(1.0_WP)
    
    G_out =  min(root1,root2)
    !G_out =  max(0.0_WP,max(root1,root2))

    return
  end subroutine fmm_phi_calc_2D
  
  !=============================================================================
  
  subroutine fmm_phi_calc_3D(G1,G2,G3,dx1,dx2,dx3,G_out)
    implicit none
    real(WP), intent(in)               :: G1,G2,G3
    real(WP), dimension(3), intent(in) :: dx1,dx2,dx3
    real(WP), intent(out)              :: G_out
    real(WP)                           :: a,b,c,d1,d2,d3,root1,root2
           
    ! Square of the distance between nodes
    d1 = sum(dx1(1:3)**2)
    d2 = sum(dx2(1:3)**2)
    d3 = sum(dx3(1:3)**2)
    
    ! Take the inverse
    d1 = 1.0_WP/d1
    d2 = 1.0_WP/d2
    d3 = 1.0_WP/d3

    ! Value of G given by root of quadratic eqn
    a = d1 + d2 + d3
    b = -2.0_WP*G1*d1 -2.0_WP*G2*d2 -2.0_WP*G3*d3
    c = (G1**2)*d1 + (G2**2)*d2 + (G3**2)*d3 - 1.0_WP

    root1 = (-b + sqrt(max(b**2 - 4.0_WP*a*c,0.0_WP)))/(2.0_WP*a)
    root2 = (-b - sqrt(max(b**2 - 4.0_WP*a*c,0.0_WP)))/(2.0_WP*a)
    
    ! Have to be careful about selecting these roots...
    ! Don't want negative values
    if (root1.le.0.0_WP) root1 = huge(1.0_WP)
    if (root2.le.0.0_WP) root2 = huge(1.0_WP)
    ! Also don't want values smaller than our original two G values
    if (root1.le.G1)     root1 = huge(1.0_WP)
    if (root1.le.G2)     root1 = huge(1.0_WP)
    if (root1.le.G3)     root1 = huge(1.0_WP)
    if (root2.le.G1)     root2 = huge(1.0_WP)
    if (root2.le.G2)     root2 = huge(1.0_WP)
    if (root2.le.G3)     root2 = huge(1.0_WP)
    
    G_out =  min(root1,root2)
    !G_out =  max(0.0_WP,max(root1,root2))
    
    return
  end subroutine fmm_phi_calc_3D
  
  !=============================================================================

  subroutine fmm_vel_calc(local_vel,radius)
    implicit none
    integer                  :: i,j
    real(WP), intent(out)    :: local_vel
    real(WP), intent(in)     :: radius
    real(WP), dimension(6)   :: s_local
    real(WP)                 :: d1,d2,d3
    real(WP)                 :: d4,d5,d6
    real(WP)                 :: dG1,dG2,dG3
    real(WP)                 :: vel1,vel2,vel3
    
    ! Account for cylindrical coords
    dx_nbrs(3,:) = dx_nbrs(3,:) * radius

    ! Check for zeros
    j = n_nbrs
    do i = 1,j
       s_local(1) = sqrt(sum(dx_nbrs(1:3,i)**2))
       if (s_local(1).le.1.0e-10_WP) then
          local_vel = vel_nbrs(i)
          !write(*,*) 'setting n_nbrs = 0'
          n_nbrs = 0
       end if
    end do

    ! How many neighbors can be used in the computation?
    select case (n_nbrs)
      case(0) 
        ! do nothing: local_phi already set
      case(1)
        local_vel = vel_nbrs(1)
      case(2)
        ! 2 possible cases - 1) the two nbrs either lie across 
        ! node from each other, or 2) they don't
        if (abs(index_nbrs(1)).eq.abs(index_nbrs(2))) then
           ! Nbrs on both sides of the node
           d1  = sqrt(sum(dx_nbrs(1:3,1)**2))
           d2  = sqrt(sum(dx_nbrs(1:3,2)**2))
           if (d1.lt.d2) then
              local_vel = vel_nbrs(1)
              ! Make sure the sign of the velocity hasn't changed - this
              ! is important when two fronts are near each other
              if (local_vel.ne.sign(local_vel,vel_me)) local_vel = vel_nbrs(2)
           else
              local_vel = vel_nbrs(2)
              if (local_vel.ne.sign(local_vel,vel_me)) local_vel = vel_nbrs(1)
           end if
        else
           ! Nbrs only on one side of the node
           d1   = sqrt(sum(dx_nbrs(1:3,1)**2))
           d2   = sqrt(sum(dx_nbrs(1:3,2)**2))
           dG1  = abs(phi_me - phi_nbrs(1))
           dG2  = abs(phi_me - phi_nbrs(2))
           vel1 = vel_nbrs(1)
           vel2 = vel_nbrs(2)
           call fmm_vel_calc_2D(vel1,vel2,d1,d2,dG1,dG2,local_vel)
        end if
      case(3)
        ! 2 possible cases - 1) any two of the nbrs lie across 
        ! node from each other, or 2) no two nbrs do
        ! First check for the crossing cases
        if     (abs(index_nbrs(1)).eq.abs(index_nbrs(2))) then
           d1  = sqrt(sum(dx_nbrs(1:3,1)**2))
           d2  = sqrt(sum(dx_nbrs(1:3,2)**2))
           d3  = sqrt(sum(dx_nbrs(1:3,3)**2))
           if ((d1.le.d2) .and. (vel_nbrs(1).eq.sign(vel_nbrs(1),vel_me))) then
              dG1  = abs(phi_me - phi_nbrs(1))
              vel1 = vel_nbrs(1)
              d1   = d1 
           elseif (vel_nbrs(2).eq.sign(vel_nbrs(2),vel_me)) then
              dG1  = abs(phi_me - phi_nbrs(2))
              vel1 = vel_nbrs(2)
              d1   = d2
           else
              dG1  = abs(phi_me - phi_nbrs(1))
              vel1 = vel_nbrs(1)
              d1   = d1 
           end if
           d2   = d3
           dG2  = abs(phi_me - phi_nbrs(3))
           vel2 = vel_nbrs(3)
           call fmm_vel_calc_2D(vel1,vel2,d1,d2,dG1,dG2,local_vel)
        elseif (abs(index_nbrs(2)).eq.abs(index_nbrs(3))) then
           d1  = sqrt(sum(dx_nbrs(1:3,1)**2))
           d2  = sqrt(sum(dx_nbrs(1:3,2)**2))
           d3  = sqrt(sum(dx_nbrs(1:3,3)**2))
           
           dG1  = abs(phi_me - phi_nbrs(1))
           vel1 = vel_nbrs(1)
           if ((d2.le.d3) .and. (vel_nbrs(2).eq.sign(vel_nbrs(2),vel_me)) ) then
              dG2  = abs(phi_me - phi_nbrs(2))
              vel2 = vel_nbrs(2)
              d2   = d2 
           elseif (vel_nbrs(3).eq.sign(vel_nbrs(3),vel_me)) then
              dG2  = abs(phi_me - phi_nbrs(3))
              vel2 = vel_nbrs(3)
              d2   = d3
           else
              dG2  = abs(phi_me - phi_nbrs(2))
              vel2 = vel_nbrs(2)
              d2   = d2 
           end if
           call fmm_vel_calc_2D(vel1,vel2,d1,d2,dG1,dG2,local_vel)
        else
           ! And now the non-crossed case: three nbrs all along diffrnt dirns
           d1  = sqrt(sum(dx_nbrs(1:3,1)**2))
           d2  = sqrt(sum(dx_nbrs(1:3,2)**2))
           d3  = sqrt(sum(dx_nbrs(1:3,3)**2))
           dG1 = abs(phi_me - phi_nbrs(1))
           dG2 = abs(phi_me - phi_nbrs(2))
           dG3 = abs(phi_me - phi_nbrs(3))
           call fmm_vel_calc_3D(vel_nbrs(1),vel_nbrs(2),vel_nbrs(3),d1,d2,d3, &
                                dG1,dG2,dG3,local_vel)
        end if
      case(4)
        ! 2 possible cases: either 1) both sets of nbrs lie across node 
        ! from each other, or 2) only one set of nbrs does
        if (abs(index_nbrs(1)).eq.abs(index_nbrs(2))) then
           if (abs(index_nbrs(3)).eq.abs(index_nbrs(4))) then
              d1  = sqrt(sum(dx_nbrs(1:3,1)**2))
              d2  = sqrt(sum(dx_nbrs(1:3,2)**2))
              d3  = sqrt(sum(dx_nbrs(1:3,3)**2))
              d4  = sqrt(sum(dx_nbrs(1:3,4)**2))
              if ((d1.le.d2) .and. (vel_nbrs(1).eq.sign(vel_nbrs(1),vel_me)) ) then
                 d1   = d1
                 dG1  = abs(phi_me - phi_nbrs(1))
                 vel1 = vel_nbrs(1)
              elseif (vel_nbrs(2).eq.sign(vel_nbrs(2),vel_me)) then
                 d1   = d2
                 dG1  = abs(phi_me - phi_nbrs(2))
                 vel1 = vel_nbrs(2)
              else
                 d1   = d1
                 dG1  = abs(phi_me - phi_nbrs(1))
                 vel1 = vel_nbrs(1)
              end if
              if ( (d3.le.d4) .and. (vel_nbrs(3).eq.sign(vel_nbrs(3),vel_me)) ) then
                 d2   = d3
                 dG2  = abs(phi_me - phi_nbrs(3))
                 vel2 = vel_nbrs(3)
              elseif (vel_nbrs(4).eq.sign(vel_nbrs(4),vel_me)) then
                 d2   = d4
                 dG2  = abs(phi_me - phi_nbrs(4))
                 vel2 = vel_nbrs(4)
              else
                 d2   = d3
                 dG2  = abs(phi_me - phi_nbrs(3))
                 vel2 = vel_nbrs(3)
              end if
              call fmm_vel_calc_2D(vel1,vel2,d1,d2,dG1,dG2,local_vel)
           else
              d1  = sqrt(sum(dx_nbrs(1:3,1)**2))
              d2  = sqrt(sum(dx_nbrs(1:3,2)**2))
              d3  = sqrt(sum(dx_nbrs(1:3,3)**2))
              d4  = sqrt(sum(dx_nbrs(1:3,4)**2))
              if ((d1.le.d2) .and. (vel_nbrs(1).eq.sign(vel_nbrs(1),vel_me)) ) then
                 d1          = d1
                 dG1         = abs(phi_me - phi_nbrs(1))
                 vel_nbrs(1) = vel_nbrs(1)
              elseif (vel_nbrs(2).eq.sign(vel_nbrs(2),vel_me) ) then
                 d1          = d2
                 dG1         = abs(phi_me - phi_nbrs(2))
                 vel_nbrs(1) = vel_nbrs(2)
              else
                 d1          = d1
                 dG1         = abs(phi_me - phi_nbrs(1))
                 vel_nbrs(1) = vel_nbrs(1)
              end if
              d2 = d3
              d3 = d4
              dG2  = abs(phi_me - phi_nbrs(3))
              dG3  = abs(phi_me - phi_nbrs(4))
              vel2 = vel_nbrs(3)
              vel3 = vel_nbrs(4)
              call fmm_vel_calc_3D(vel1,vel2,vel3,d1,d2,d3, &
                                dG1,dG2,dG3,local_vel)
           end if
        elseif (abs(index_nbrs(2)).eq.abs(index_nbrs(3))) then
           d1  = sqrt(sum(dx_nbrs(1:3,1)**2))
           d2  = sqrt(sum(dx_nbrs(1:3,2)**2))
           d3  = sqrt(sum(dx_nbrs(1:3,3)**2))
           d4  = sqrt(sum(dx_nbrs(1:3,4)**2))
           if ((d2.le.d3) .and. (vel_nbrs(2).eq.sign(vel_nbrs(2),vel_me)) ) then
              d2   = d2
              dG2  = abs(phi_me - phi_nbrs(2))
              vel2 = vel_nbrs(2)
           elseif (vel_nbrs(3).eq.sign(vel_nbrs(3),vel_me) ) then
              d2   = d3
              dG2  = abs(phi_me - phi_nbrs(3))
              vel2 = vel_nbrs(3)
           else
              d2   = d2
              dG2  = abs(phi_me - phi_nbrs(2))
              vel2 = vel_nbrs(2)
           end if
           d1   = d1
           dG1  = abs(phi_me - phi_nbrs(1))
           vel1 = vel_nbrs(1)
           d3   = d4
           dG3  = abs(phi_me - phi_nbrs(4))
           vel3 = vel_nbrs(4)
           call fmm_vel_calc_3D(vel1,vel2,vel3,d1,d2,d3, &
                                dG1,dG2,dG3,local_vel)
        else 
           d1  = sqrt(sum(dx_nbrs(1:3,1)**2))
           d2  = sqrt(sum(dx_nbrs(1:3,2)**2))
           d3  = sqrt(sum(dx_nbrs(1:3,3)**2))
           d4  = sqrt(sum(dx_nbrs(1:3,4)**2))
           d1   = d1
           dG1  = abs(phi_me - phi_nbrs(1))
           vel1 = vel_nbrs(1)
           d2   = d2
           dG2  = abs(phi_me - phi_nbrs(2))
           vel2 = vel_nbrs(2)
           if ((d3.le.d4) .and. (vel_nbrs(3).eq.sign(vel_nbrs(3),vel_me)) ) then
              d3   = d3
              dG3  = abs(phi_me - phi_nbrs(3))
              vel3 = vel_nbrs(3)
           elseif (vel_nbrs(4).eq.sign(vel_nbrs(4),vel_me)) then
              d3   = d4
              dG3  = abs(phi_me - phi_nbrs(4))
              vel3 = vel_nbrs(4)
           else 
              d3   = d3
              dG3  = abs(phi_me - phi_nbrs(3))
              vel3 = vel_nbrs(3)
           end if
           call fmm_vel_calc_3D(vel1,vel2,vel3,d1,d2,d3, &
                                dG1,dG2,dG3,local_vel)
        end if
      case(5)
        ! Only 1 case - test using the 4 possible planes and take smallest soln
        ! Renumber so that the 5th node has no one across from him
        if (abs(index_nbrs(1)).ne.abs(index_nbrs(2))) then
           d1  = sqrt(sum(dx_nbrs(1:3,1)**2))
           d2  = sqrt(sum(dx_nbrs(1:3,2)**2))
           d3  = sqrt(sum(dx_nbrs(1:3,3)**2))
           d4  = sqrt(sum(dx_nbrs(1:3,4)**2))
           d5  = sqrt(sum(dx_nbrs(1:3,5)**2))
           
           dG1 = abs(phi_me - phi_nbrs(1))
           vel1 = vel_nbrs(1)
           if (d2.le.d3) then
              d2   = d2
              dG2  = abs(phi_me - phi_nbrs(2))
              vel2 = vel_nbrs(2)
           else
              d2   = d3
              dG2  = abs(phi_me - phi_nbrs(3))
              vel2 = vel_nbrs(3)
           end if
           if (d4.le.d5) then
              d3   = d4
              dG3  = abs(phi_me - phi_nbrs(4))
              vel3 = vel_nbrs(4)
           else
              d3   = d5
              dG3  = abs(phi_me - phi_nbrs(5))
              vel3 = vel_nbrs(5)
           end if
           call fmm_vel_calc_3D(vel1,vel2,vel3,d1,d2,d3, &
                                dG1,dG2,dG3,local_vel)
        elseif (abs(index_nbrs(3)).ne.abs(index_nbrs(4))) then
           d1  = sqrt(sum(dx_nbrs(1:3,1)**2))
           d2  = sqrt(sum(dx_nbrs(1:3,2)**2))
           d3  = sqrt(sum(dx_nbrs(1:3,3)**2))
           d4  = sqrt(sum(dx_nbrs(1:3,4)**2))
           d5  = sqrt(sum(dx_nbrs(1:3,5)**2))
           if (d1.le.d2) then
              d1   = d1
              dG1  = abs(phi_me - phi_nbrs(1))
              vel1 = vel_nbrs(1)
           else
              d1   = d2
              dG1  = abs(phi_me - phi_nbrs(2))
              vel1 = vel_nbrs(2)
           end if

           d2   = d3
           dG2  = abs(phi_me - phi_nbrs(3))
           vel2 = vel_nbrs(3)
           
           if (d4.le.d5) then
              d3   = d4
              dG3  = abs(phi_me - phi_nbrs(4))
              vel3 = vel_nbrs(4)
           else
              d3   = d5
              dG3  = abs(phi_me - phi_nbrs(5))
              vel3 = vel_nbrs(5)
           end if
           call fmm_vel_calc_3D(vel1,vel2,vel3,d1,d2,d3, &
                                dG1,dG2,dG3,local_vel)
        else 
           d1  = sqrt(sum(dx_nbrs(1:3,1)**2))
           d2  = sqrt(sum(dx_nbrs(1:3,2)**2))
           d3  = sqrt(sum(dx_nbrs(1:3,3)**2))
           d4  = sqrt(sum(dx_nbrs(1:3,4)**2))
           d5  = sqrt(sum(dx_nbrs(1:3,5)**2))
           if (d1.le.d2) then
              d1   = d1
              dG1  = abs(phi_me - phi_nbrs(1))
              vel1 = vel_nbrs(1)
           else
              d1   = d2
              dG1  = abs(phi_me - phi_nbrs(2))
              vel1 = vel_nbrs(2)
           end if
           if (d3.le.d4) then
              d2   = d3
              dG2  = abs(phi_me - phi_nbrs(3))
              vel2 = vel_nbrs(3)
           else
              d2   = d4
              dG2  = abs(phi_me - phi_nbrs(4))
              vel2 = vel_nbrs(4)
           end if
           d3   = d5
           dG3  = abs(phi_me - phi_nbrs(5))
           vel3 = vel_nbrs(5)
           
           call fmm_vel_calc_3D(vel1,vel2,vel3,d1,d2,d3, &
                                dG1,dG2,dG3,local_vel)
        end if
      case(6)
           d1  = sqrt(sum(dx_nbrs(1:3,1)**2))
           d2  = sqrt(sum(dx_nbrs(1:3,2)**2))
           d3  = sqrt(sum(dx_nbrs(1:3,3)**2))
           d4  = sqrt(sum(dx_nbrs(1:3,4)**2))
           d5  = sqrt(sum(dx_nbrs(1:3,5)**2))
           d6  = sqrt(sum(dx_nbrs(1:3,6)**2))
           if (d1.le.d2) then
              d1   = d1
              dG1  = abs(phi_me - phi_nbrs(1))
              vel1 = vel_nbrs(1)
           else
              d1   = d2
              dG1  = abs(phi_me - phi_nbrs(2))
              vel1 = vel_nbrs(2)
           end if
           if (d3.le.d4) then
              d2   = d3
              dG2  = abs(phi_me - phi_nbrs(3))
              vel2 = vel_nbrs(3)
           else
              d2   = d4
              dG2  = abs(phi_me - phi_nbrs(4))
              vel2 = vel_nbrs(4)
           end if
           if (d5.le.d6) then
              d3   = d5
              dG3  = abs(phi_me - phi_nbrs(5))
              vel3 = vel_nbrs(5)
           else
              d3   = d6
              dG3  = abs(phi_me - phi_nbrs(6))
              vel3 = vel_nbrs(6)
           end if
           call fmm_vel_calc_3D(vel1,vel2,vel3,d1,d2,d3, &
                                dG1,dG2,dG3,local_vel)
      case default
        call die('Error: n_nbrs out of range')
    end select

    return
  end subroutine fmm_vel_calc
  
  !=============================================================================
  
  subroutine fmm_vel_calc_2D(vel1,vel2,d1,d2,dG1,dG2,vel_out)
    implicit none
    real(WP), intent(in)               :: vel1,vel2
    real(WP), intent(in)               :: d1,d2
    real(WP), intent(in)               :: dG1,dG2
    real(WP)                           :: d1i,d2i
    real(WP), intent(out)              :: vel_out
    real(WP)                           :: numer, denom
    
    ! Extended speed fncn should satisfy Grad(Speed)*Grad(G) = 0.
    ! To make this happen, use the discrete eqn on pg. 13 of Adalsteinsson and
    ! Sethian, "Fast Construction of Extension Velocities", JCP vol 148, 1999
    ! as a basis, and modify for the general case when h is not constant.
           
    ! Take the square of the inverse of the distance between nodes
    d1i = 1.0_WP/(d1**2)
    d2i = 1.0_WP/(d2**2)

    ! Compute the speed
    numer = (vel1*d1i*dG1 + vel2*d2i*dG2) 
    denom = (d1i*max(dG1,1.0e-12_WP) + d2i*dG2)
    vel_out = numer / denom

    return
  end subroutine fmm_vel_calc_2D
  
  !=============================================================================
  
  subroutine fmm_vel_calc_3D(vel1,vel2,vel3,d1,d2,d3,dG1,dG2,dG3,vel_out)
    implicit none
    real(WP), intent(in)               :: vel1,vel2,vel3
    real(WP), intent(in)               :: d1,d2,d3
    real(WP), intent(in)               :: dG1,dG2,dG3
    real(WP)                           :: d1i,d2i,d3i
    real(WP), intent(out)              :: vel_out
    real(WP)                           :: numer, denom
    
    ! Extended speed fncn should satisfy Grad(Speed)*Grad(G) = 0.
    ! To make this happen, use the discrete eqn on pg. 13 of Adalsteinsson and
    ! Sethian, "Fast Construction of Extension Velocities", JCP vol 148, 1999
    ! as a basis, and modify for the general case when h is not constant.
    
    ! Take the square of the inverse of the distance between nodes
    d1i = 1.0_WP/(d1**2)
    d2i = 1.0_WP/(d2**2)
    d3i = 1.0_WP/(d3**2)
    
    ! Compute the speed
    numer = (vel1*d1i*dG1 + vel2*d2i*dG2 + vel3*d3i*dG3)
    denom = (d1i*max(dG1,1.0e-12_WP) + d2i*dG2 + d3i*dG3)

    vel_out = numer / denom
           
    return
  end subroutine fmm_vel_calc_3D
  
  !=============================================================================

end module levelset_fmm

!===============================================================================
!===============================================================================

subroutine levelset_fmm_init
  use levelset
  use levelset_fmm
  use levelset_heapsort
  use parallel
  use geometry
  use parser
  implicit none
  integer :: isource
  integer :: idest
  integer :: ierr

  call parser_read('FMM reinit',fmm_reinit,.true.)
  
  ! Exit if not used
  if (.not.fmm_reinit) return

  allocate(lvlset_normal_x_old(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(lvlset_normal_y_old(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(lvlset_normal_z_old(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  
  allocate(phi_fmm     (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(phi_vel     (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(phi_vel_init(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(phi_flag    (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(vel_set    (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  
  allocate(close_plus_ijk (nx_*ny_*nz_,3))
  allocate(close_minus_ijk(nx_*ny_*nz_,3))

  ! Determine the ranks of the procs that this proc should send to  
  call MPI_CART_SHIFT(comm,0,-1,isource,idest,ierr)
  rank_x_low  = idest
  call MPI_CART_SHIFT(comm,0,+1,isource,idest,ierr)
  rank_x_high = idest
  call MPI_CART_SHIFT(comm,1,-1,isource,idest,ierr)
  rank_y_low  = idest
  call MPI_CART_SHIFT(comm,1,+1,isource,idest,ierr)
  rank_y_high = idest
  call MPI_CART_SHIFT(comm,2,-1,isource,idest,ierr)
  rank_z_low  = idest
  call MPI_CART_SHIFT(comm,2,+1,isource,idest,ierr)
  rank_z_high = idest
  
  ! Set bounds on what nodes to send to other procs
  if (nx.gt.1) then
     i_passlo = imin_+1
     i_passhi = imax_-1
  else
     i_passlo = imin_
     i_passhi = imax_
  end if
  if (ny.gt.1) then
     j_passlo = jmin_+1
     j_passhi = jmax_-1
  else
     j_passlo = jmin_
     j_passhi = jmax_
  end if
  if (nz.gt.1) then
     k_passlo = kmin_+1
     k_passhi = kmax_-1
  else
     k_passlo = kmin_
     k_passhi = kmax_
  end if

  ! Set bounds for the ghost nodes to consider given problem dimensions
  if (nx.gt.1) then
     imin_close = imin_-1
     imax_close = imax_+1
  else
     imin_close = imin_
     imax_close = imax_
  end if
  if (ny.gt.1) then
     jmin_close = jmin_-1
     jmax_close = jmax_+1
  else
     jmin_close = jmin_
     jmax_close = jmax_
  end if
  if (nz.gt.1) then
     kmin_close = kmin_-1
     kmax_close = kmax_+1
  else
     kmin_close = kmin_
     kmax_close = kmax_
  end if

  ! Attach an mpi buffer for parallelization
  mpi_buffer_size = 8*10*max(local_counter_max,20)
  allocate(mpi_buffer(mpi_buffer_size))
  call MPI_BUFFER_ATTACH(mpi_buffer, mpi_buffer_size, ierr)

  return
end subroutine levelset_fmm_init

!===============================================================================
!===============================================================================

subroutine levelset_fmm_reinit
  use data
  use levelset_marker
  use levelset_fmm
  use levelset_heapsort
  use parallel
  use masks
  use metric_generic
  implicit none

  integer  :: i,j,k
  integer  :: n,nn,nnn,m
  integer  :: ii,jj,kk
  integer  :: iii,jjj,kkk
  integer  :: kPi
  integer  :: i0,j0,k0
  integer  :: iter
  integer  :: iheap
  integer  :: fmm_close, fmm_accepted, fmm_far
  integer  :: local_index
  integer  :: close_count
  integer  :: n_already_close, n_new_close
  integer, dimension(6) :: already_close, new_close

  integer  :: n_accepted
  logical  :: local_done
  logical  :: global_done
  integer  :: local_counter

  real(WP) :: local_phi, this_phi, this_vel
  real(WP) :: ui,vi,wi
  real(WP) :: local_dx
  real(WP) :: radius

  real(WP) :: cutoff_outer
  real(WP) :: cutoff_inner

  integer  :: my_ibuf(3), ibuf(3), ierr

  logical, external :: fmm_parallel_recv
  
  ! Exit if not used
  if (.not.fmm_reinit) return
  
  ! Recompute normals
  call levelset_normals

  ! Set up the field from which the velocity to be extended will be taken
  phi_vel_init = 0.0_WP
  phi_vel      = 0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ui = sum(interp_u_xm(i,j,:)*U(i-st1:i+st2,j,k)) 
           vi = sum(interp_v_ym(i,j,:)*V(i,j-st1:j+st2,k)) 
           wi = sum(interp_w_zm(i,j,:)*W(i,j,k-st1:k+st2))

           phi_vel_init(i,j,k)= ui*lvlset_normal_x(i,j,k) + &
                                vi*lvlset_normal_y(i,j,k) + &
                                wi*lvlset_normal_z(i,j,k) + &
                                srclvlset(i,j,k)
        end do
     end do
  end do
  call levelset_apply_bc(phi_vel_init,'+','ym')

  n_plus = 0
  n_minus = 0
  phi_flag = 0

  ! First tag all nodes and make plus and minus counts
  do k = kmino_,kmaxo_
     do j = jmino_,jmaxo_
        do i = imino_,imaxo_
           if (LVLSET_band(i,j,k).eq.0) cycle   
           if (mask(i,j).eq.1 .or. mask(i,j).eq.3) cycle

           if (LVLSET(i,j,k).gt.0) then
              n_plus = n_plus + 1
              phi_flag(i,j,k) = fmm_far_plus
           else
              n_minus = n_minus + 1
              phi_flag(i,j,k) = fmm_far_minus
           end if
        
        end do
     end do
  end do

  close_plus_count = 0
  close_minus_count = 0
  ! Set up list of initially close nodes
  do k=kmin_close,kmax_close
     do j=jmin_close,jmax_close
        do i=imin_close,imax_close
           if (LVLSET_band(i,j,k).eq.0) cycle   
           if (mask(i,j).eq.1 .or. mask(i,j).eq.3) cycle   
           
           do n = 1,6
              select case(n)
                case(1)
                  ii=i-1; jj=j; kk=k
                case(2)
                  ii=i+1; jj=j; kk=k
                case(3)
                  ii=i; jj=j-1; kk=k
                case(4)
                  ii=i; jj=j+1; kk=k
                case(5)
                  ii=i; jj=j; kk=k-1
                case(6)
                  ii=i; jj=j; kk=k+1
                case default
                  ! should never happen
              end select

              if (LVLSET(i,j,k)*LVLSET(ii,jj,kk).le.0.0_WP) then  
                 if (LVLSET(i,j,k).le.0.0_WP) then
                    if (phi_flag(i,j,k).ne.fmm_close_minus) then
                       phi_flag(i,j,k) = fmm_close_minus
                       close_minus_count = close_minus_count + 1
                       close_minus_ijk(close_minus_count,1) = i
                       close_minus_ijk(close_minus_count,2) = j
                       close_minus_ijk(close_minus_count,3) = k
                    end if
                 else
                    if (phi_flag(i,j,k).ne.fmm_close_plus) then
                       phi_flag(i,j,k) = fmm_close_plus
                       close_plus_count = close_plus_count + 1
                       close_plus_ijk(close_plus_count,1) = i
                       close_plus_ijk(close_plus_count,2) = j
                       close_plus_ijk(close_plus_count,3) = k
                    end if
                 end if             
              end if            

           end do 

        end do
     end do
  end do

  ! Allocate a heap for sorting purposes
  if (allocated(heap)) deallocate(heap)
  allocate(heap(max(n_minus,n_plus)))
  
  if (associated(accepted_ijk)) deallocate(accepted_ijk)
  allocate(accepted_ijk(max(n_minus,n_plus),3))
  
  ! Set up the temp level set field variable
  phi_fmm = lvlset_cutoff

  n_heap = 0
  if (allocated(rheap_index)) deallocate(rheap_index)
  allocate(rheap_index(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))

  ! First positive, then negative side of the front
  do iter=1,2

     ! use the ibuf to store:
     ! 1: count of received messages
     ! 2: count of sent messages
     ! 3: our current n_heap
     my_ibuf(1:2) = 0   ! clear the message counters

     if (iter .eq. 1) then
        fmm_accepted    = fmm_accepted_plus
        fmm_close       = fmm_close_plus
        fmm_far         = fmm_far_plus
        close_count     = close_plus_count
        close_ijk       => close_plus_ijk
     else
        fmm_accepted    = fmm_accepted_minus
        fmm_close       = fmm_close_minus
        fmm_far         = fmm_far_minus
        close_count     = close_minus_count
        close_ijk       => close_minus_ijk
     end if

     ! Set up the initial close nodes list
     do nn = 1,close_count
        i = close_ijk(nn,1) 
        j = close_ijk(nn,2) 
        k = close_ijk(nn,3)

        ! Double check
        if (phi_flag(i,j,k).eq.fmm_close) then            
           n_nbrs = 0
           ! Count the number of neighbors       
           do n = 1,6
              select case(n)
                case(1)
                  ii=i-1; jj=j; kk=k
                  local_index = -1
                case(2)
                  ii=i+1; jj=j; kk=k
                  local_index = +1
                case(3)
                  ii=i; jj=j-1; kk=k
                  local_index = -2
                case(4)
                  ii=i; jj=j+1; kk=k
                  local_index = +2
                case(5)
                  ii=i; jj=j; kk=k-1
                  local_index = -3
                case(6)
                  ii=i; jj=j; kk=k+1
                  local_index = +3
                case default
                  ! should never happen
              end select
              if (LVLSET(i,j,k)*LVLSET(ii,jj,kk).le.0.0_WP) then
                 if (LVLSET(ii,jj,kk).eq.LVLSET(i,j,k)) cycle
                 n_nbrs = n_nbrs + 1
                 index_nbrs(n_nbrs) = local_index
                 dx_nbrs(1,n_nbrs) = abs(-LVLSET(i,j,k) * (xm(ii)-xm(i)) / &
                                     (LVLSET(ii,jj,kk) - LVLSET(i,j,k)))
                 dx_nbrs(2,n_nbrs) = abs(-LVLSET(i,j,k) * (ym(jj)-ym(j)) / &
                                     (LVLSET(ii,jj,kk) - LVLSET(i,j,k)))
                 dx_nbrs(3,n_nbrs) = abs(-LVLSET(i,j,k) * (zm(kk)-zm(k)) / &
                                     (LVLSET(ii,jj,kk) - LVLSET(i,j,k)))
              end if
           end do
           ! Set the radius in case of cyl coords
           if (icyl.eq.1) then 
              radius = abs(ym(j))
           else
              radius = 1.0_WP
           end if
           if (n_nbrs .gt. 0) then
              !phi_nbrs(:) = 0.0_WP
              !call fmm_phi_calc(local_phi,radius)            
              !phi_fmm(i,j,k) = min(phi_fmm(i,j,k),local_phi)
              phi_fmm(i,j,k) = abs(LVLSET(i,j,k))
              if (j.le.jmin+1) then
                 phi_nbrs(:) = 0.0_WP
                 call fmm_phi_calc(local_phi,radius)            
                 phi_fmm(i,j,k) = min(phi_fmm(i,j,k),local_phi)
              end if
           end if
           n_heap             = n_heap + 1
           heap(n_heap)%G     = phi_fmm(i,j,k)
           heap(n_heap)%i     = i
           heap(n_heap)%j     = j
           heap(n_heap)%k     = k
           rheap_index(i,j,k) = n_heap
        else 
           write(*,*) 'Error: Not-close node found in initial close list'
        end if

     end do
     
     call fmm_heapify(n_heap)
     n_accepted = 0
     global_done = .FALSE.
     global_loop: do while (.not.global_done)

        local_counter = 0
        local_done = .FALSE.     

        local_loop: do while (.not.local_done)

           ! =================================================
           ! start of parallel message processing
           ! =================================================
       
           ! check for new message from other processors
           do while (fmm_parallel_recv(i,j,k,this_phi,this_vel))

              ! Set a bias towards the communicated value
              !if (this_phi.le.(phi_fmm(i,j,k)+2.0E-12_WP)) then 
              !   this_phi = max(0.0_WP,this_phi-4.0E-12_WP)
              !end if
              !write(*,*) 'recvd, this_phi = ',this_phi
              !write(*,*) 'i,j,k = ',i,j,k

              !if ((max(0.0_WP,this_phi-4.0E-12_WP)).lt.phi_fmm(i,j,k)) then 
              if ((max(0.0_WP,this_phi+1.0E-12_WP)).lt.phi_fmm(i,j,k)) then 

                 ! check if this value is less than the currently 
                 ! accepted value, and if it is, then roll back.
                 do while (n_accepted.gt.0)
                    ii = accepted_ijk(n_accepted,1)
                    jj = accepted_ijk(n_accepted,2)
                    kk = accepted_ijk(n_accepted,3)

                    ! check...
                    if (phi_flag(ii,jj,kk).ne.fmm_accepted) then
                       write(*,*) 'Error: expected an accepted value.'
                    end if
                 
                    ! check if we have rolled back enough...
                    if (this_phi.gt.max(phi_fmm(ii,jj,kk)-1.0E-12_WP,0.0_WP)) then
                       if (ii.ge.imin_ .and. ii.le.imax_) then
                          if (jj.ge.jmin_ .and. jj.le.jmax_) then
                             if (kk.ge.kmin_ .and. kk.le.kmax_) then
                                exit
                             end if
                          end if
                       end if
                    end if

                    phi_flag(ii,jj,kk) = fmm_close
                    n_heap = n_heap + 1
                    heap(n_heap)%G = phi_fmm(ii,jj,kk)
                    heap(n_heap)%i = ii
                    heap(n_heap)%j = jj
                    heap(n_heap)%k = kk
                    rheap_index(ii,jj,kk) = n_heap
                    call fmm_sift_newval_down(n_heap)
                    ! and reduce the accepted list...
                    n_accepted = n_accepted - 1

                 end do

                 ! now take the sent value...
                 ! and take action - we must be either far or close. If
                 ! we were accepted, we should have been rolled back...
                 if (phi_flag(i,j,k).eq.fmm_close) then

                    phi_fmm(i,j,k) = this_phi
                    phi_vel(i,j,k) = this_vel

                    iheap = rheap_index(i,j,k)
                    ! check...
                    if (heap(iheap)%G.lt.(this_phi-4.0E-12_WP)) then
                       write(*,*) 'Error: must always be greater.'
                       write(*,*) '     heap(iheap)%G = ',heap(iheap)%G
                       write(*,*) '     this_phi      = ',this_phi
                    end if                              
                    ! set and resort...
                    heap(iheap)%G = this_phi
                    call fmm_sift_newval_down(n_heap)

                 elseif (phi_flag(i,j,k).eq.fmm_far) then

                    phi_fmm(i,j,k) = this_phi
                    phi_vel(i,j,k) = this_vel

                    ! add as close...
                    phi_flag(i,j,k) = fmm_close
                    n_heap = n_heap + 1
                    heap(n_heap)%G = phi_fmm(i,j,k)
                    heap(n_heap)%i = i
                    heap(n_heap)%j = j
                    heap(n_heap)%k = k
                    rheap_index(i,j,k) = n_heap
                    call fmm_sift_newval_down(n_heap)

                 else
                 
                    !write(*,*) 'Error: unexpected flag in fmm:',phi_flag(i,j,k)
                    !write(*,*) '   irank = ',irank
                    !write(*,*) '   unexpected flag at i,j,k = ',i,j,k
                    !write(*,*) '                   ii,jj,kk = ',ii,jj,kk
                    !write(*,*) '   phi_fmm(i,j,k) = ',phi_fmm(i,j,k)
                    !write(*,*) '   phi_fmm(ii,jj,kk) = ',phi_fmm(ii,jj,kk)
                    !write(*,*) '   this_phi = ',this_phi
                    !write(*,*) '   n_accepted = ',n_accepted
                    
                 end if
               
              end if
              
              ! add one to recv message counter...
              my_ibuf(1) = my_ibuf(1) + 1

           end do
           !write(*,*) 'done with message receiving, n_acc = ',n_accepted
           !write(*,*) '   my_ibuf(1) = ',my_ibuf(1)

           ! =================================================
           ! end of parallel message processing
           ! =================================================

           if (n_heap.gt.0) then

              i = heap(1)%i 
              j = heap(1)%j 
              k = heap(1)%k

              if (phi_flag(i,j,k).ne.fmm_close) call die('wrong node put into heap')

              phi_flag(i,j,k) = fmm_accepted
              local_counter = local_counter + 1

              n_accepted = n_accepted + 1
              accepted_ijk(n_accepted,1) = i
              accepted_ijk(n_accepted,2) = j
              accepted_ijk(n_accepted,3) = k

              ! Now delete the heap root
              call fmm_delete_root(n_heap)
              
              ! Do the velocity extension and add nbrs to the heap
              n_already_close = 0
              n_new_close = 0
              n_nbrs = 0
              do nn = 1,6
                 select case(nn)
                   case(1)
                     ii=max(imin_close,i-1); jj=j; kk=k
                     if (i.eq.ii) cycle
                     local_index = -1
                   case(2)
                     ii=min(imax_close,i+1); jj=j; kk=k
                     if (i.eq.ii) cycle
                     local_index = +1
                   case(3)
                     ii=i; jj=max(jmin_close,j-1); kk=k
                     if (j.eq.jj) cycle
                     local_index = -2
                   case(4)
                     ii=i; jj=min(jmax_close,j+1); kk=k
                     if (j.eq.jj) cycle
                     local_index = +2
                   case(5)
                     ii=i; jj=j; kk=max(kmin_close,k-1)
                     if (k.eq.kk) cycle
                     local_index = -3
                   case(6)
                     ii=i; jj=j; kk=min(kmax_close,k+1)
                     if (k.eq.kk) cycle
                     local_index = +3
                   case default
                     ! should never happen
                 end select

                 ! Don't add nodes that are outside the bands
                 if (LVLSET_band(ii,jj,kk).eq.0) cycle
                 if (mask(ii,jj).eq.1 .or. mask(ii,jj).eq.3) cycle
                 
                 ! Count the nodes to be used in extending the distance function
                 if (phi_flag(ii,jj,kk).eq.fmm_close) then
                    n_already_close = n_already_close + 1
                    already_close(n_already_close) = nn
                    phi_flag(ii,jj,kk) = fmm_tmp ! to prevent double counting
                 elseif (phi_flag(ii,jj,kk).eq.fmm_far) then
                    n_new_close = n_new_close + 1
                    new_close(n_new_close) = nn
                    phi_flag(ii,jj,kk) = fmm_tmp
                 end if
   
                 ! Set up values for velocity extension
                 if ((LVLSET(i,j,k)*LVLSET(ii,jj,kk)).le.0.0_WP) then
                    if (LVLSET(i,j,k).eq.LVLSET(ii,jj,kk)) cycle
                    ! An intersection
                    n_nbrs = n_nbrs + 1
                    phi_nbrs(n_nbrs) = 0.0_WP
                    index_nbrs(n_nbrs) = local_index
                    dx_nbrs(1,n_nbrs) = abs(-LVLSET(i,j,k) * (xm(ii)-xm(i)) / &
                                        (LVLSET(ii,jj,kk) - LVLSET(i,j,k)))
                    dx_nbrs(2,n_nbrs) = abs(-LVLSET(i,j,k) * (ym(jj)-ym(j)) / &
                                        (LVLSET(ii,jj,kk) - LVLSET(i,j,k)))
                    dx_nbrs(3,n_nbrs) = abs(-LVLSET(i,j,k) * (zm(kk)-zm(k)) / &
                                        (LVLSET(ii,jj,kk) - LVLSET(i,j,k)))
   
                    ! Interpolate to get the velocity
                    if    (abs(local_index).eq.1) then
                       local_dx = abs(xm(ii) - xm(i))
                    elseif(abs(local_index).eq.2) then
                       local_dx = abs(ym(jj) - ym(j))
                    else
                       local_dx = abs(dz)
                    end if
                    vel_nbrs(n_nbrs) = &
                          (phi_vel_init(ii,jj,kk)-phi_vel_init(i,j,k)) / & 
                          local_dx * phi_fmm(i,j,k) + phi_vel_init(i,j,k)
                    
                 elseif (phi_flag(ii,jj,kk).eq.fmm_accepted) then
                    ! An accepted nbr
                    n_nbrs = n_nbrs + 1
                    phi_nbrs(n_nbrs)   = phi_fmm(ii,jj,kk)
                    index_nbrs(n_nbrs) = local_index
                    dx_nbrs(1,n_nbrs)  = abs(xm(ii)-xm(i))
                    dx_nbrs(2,n_nbrs)  = abs(ym(jj)-ym(j))
                    dx_nbrs(3,n_nbrs)  = abs(zm(kk)-zm(k))
                    vel_nbrs(n_nbrs)   = phi_vel(ii,jj,kk)
                 end if
   
              end do

              ! Work on the physical domain
              if (i.ge.imin_ .and. i.le.imax_ .and. &
                  j.ge.jmin_ .and. j.le.jmax_ .and. &
                  k.ge.kmin_ .and. k.le.kmax_ ) then
              
                 ! Extend the propagation speed -------------------------
                 if (n_nbrs.gt.0) then
                    ! Set the radius in case of cyl coords
                    if (icyl.eq.1) then 
                       radius = abs(ym(jj))
                    else
                       radius = 1.0_WP
                    end if
                    phi_me = phi_fmm(i,j,k)
                    vel_me = phi_vel_init(i,j,k)
                    call fmm_vel_calc(local_phi,radius)
                    phi_vel(i,j,k) = local_phi
                 end if
           
                 ! If this is a boundary node, let other processors 
                 ! know the node has been accepted
                 if (rank_x_low.ge.0) then 
                    if (i.lt.i_passlo) then
                       if (xper.eq.1 .and. iproc.eq.1) then
                          i0 = i+nx
                       else
                          i0 = i
                       end if
                       call fmm_parallel_send(rank_x_low,i0,j,k,phi_fmm(i,j,k),phi_vel(i,j,k))
                       my_ibuf(2) = my_ibuf(2) + 1
                    end if
                 end if
                 if (rank_x_high.ge.0) then
                    if (i.gt.i_passhi) then
                       if (xper.eq.1 .and. iproc.eq.npx) then
                          i0 = i-nx
                       else
                          i0 = i
                       end if
                       call fmm_parallel_send(rank_x_high,i0,j,k,phi_fmm(i,j,k),phi_vel(i,j,k))
                       my_ibuf(2) = my_ibuf(2) + 1
                    end if
                 end if
                 if (rank_y_low.ge.0) then
                    if (j.lt.j_passlo) then
                       if (yper.eq.1 .and. jproc.eq.1) then
                          j0 = j+ny
                       else
                          j0 = j
                       end if
                       call fmm_parallel_send(rank_y_low,i,j0,k,phi_fmm(i,j,k),phi_vel(i,j,k))
                       my_ibuf(2) = my_ibuf(2) + 1
                    end if
                 end if
                 if (rank_y_high.ge.0) then
                    if (j.gt.j_passhi) then
                       if (yper.eq.1 .and. jproc.eq.npy) then
                          j0 = j-ny
                       else
                          j0 = j
                       end if
                       call fmm_parallel_send(rank_y_high,i,j0,k,phi_fmm(i,j,k),phi_vel(i,j,k))
                       my_ibuf(2) = my_ibuf(2) + 1
                    end if
                 end if
                 if (rank_z_low.ge.0) then
                    if (k.lt.k_passlo) then
                       if (zper.eq.1 .and. kproc.eq.1) then
                          k0 = k+nz
                       else
                          k0 = k
                       end if
                       call fmm_parallel_send(rank_z_low,i,j,k0,phi_fmm(i,j,k),phi_vel(i,j,k))
                       my_ibuf(2) = my_ibuf(2) + 1
                    end if       
                 end if
                 if (rank_z_high.ge.0) then
                    if (k.gt.k_passhi) then
                       if (zper.eq.1 .and. kproc.eq.npz) then
                          k0 = k-nz
                       else
                          k0 = k
                       end if
                       call fmm_parallel_send(rank_z_high,i,j,k0,phi_fmm(i,j,k),phi_vel(i,j,k))
                       my_ibuf(2) = my_ibuf(2) + 1
                    end if
                 end if
                 ! Account for the centerline
                 if (icyl.eq.1 .and. jproc.eq.1) then
                    if (j.eq.jmin) then
                       ! Adjust the j,k indices for transferring
                       kPi = k+nz/2
                       if (kPi.gt.kmax) kPi = kPi-nz
                       ! Send to myself
                       call fmm_parallel_send(irank-1,i,jmin-1,kPi,phi_fmm(i,j,k),phi_vel(i,j,k))
                       my_ibuf(2) = my_ibuf(2) + 1
                    end if
                 end if

              end if
       
              ! Process already close nodes --------------------------
              do nn = 1,n_already_close
                 m = already_close(nn)  
                 select case(m)
                   case(1)
                     ii=max(imin_close,i-1); jj=j; kk=k
                     if (i.eq.ii) cycle
                     local_index = -1
                   case(2)
                     ii=min(imax_close,i+1); jj=j; kk=k
                     if (i.eq.ii) cycle
                     local_index = +1
                   case(3)
                     ii=i; jj=max(jmin_close,j-1); kk=k
                     if (j.eq.jj) cycle
                     local_index = -2
                   case(4)
                     ii=i; jj=min(jmax_close,j+1); kk=k
                     if (j.eq.jj) cycle
                     local_index = +2
                   case(5)
                     ii=i; jj=j; kk=max(kmin_close,k-1)
                     if (k.eq.kk) cycle
                     local_index = -3
                   case(6)
                     ii=i; jj=j; kk=min(kmax_close,k+1)
                     if (k.eq.kk) cycle
                     local_index = +3
                   case default
                     ! should never happen
                 end select
                 
                 ! Change the flag
                 phi_flag(ii,jj,kk) = fmm_close
        
                 n_nbrs = 0
                 do nnn = 1,6
                    select case(nnn)
                      case(1)
                        iii=ii-1; jjj=jj; kkk=kk
                        local_index = -1
                      case(2)
                        iii=ii+1; jjj=jj; kkk=kk
                        local_index = +1
                      case(3)
                        iii=ii; jjj=jj-1; kkk=kk
                        local_index = -2
                      case(4)
                        iii=ii; jjj=jj+1; kkk=kk
                        local_index = +2
                      case(5)
                        iii=ii; jjj=jj; kkk=kk-1
                        local_index = -3
                      case(6)
                        iii=ii; jjj=jj; kkk=kk+1
                        local_index = +3
                      case default
                        ! should never happen
                    end select

                    if (LVLSET_band(iii,jjj,kkk).eq.0) cycle
                    if (mask(iii,jjj).eq.1 .or. mask(iii,jjj).eq.3) cycle

                    ! Check for nbrs
                    if ((LVLSET(ii,jj,kk)*LVLSET(iii,jjj,kkk)).le.0.0_WP) then
                       if (LVLSET(ii,jj,kk).eq.LVLSET(iii,jjj,kkk)) cycle
                       ! An intersection
                       n_nbrs = n_nbrs + 1
                       phi_nbrs(n_nbrs) = 0.0_WP
                       index_nbrs(n_nbrs) = local_index
                       dx_nbrs(1,n_nbrs) = abs(-LVLSET(ii,jj,kk) * (xm(iii)-xm(ii)) / &
                                           (LVLSET(iii,jjj,kkk) - LVLSET(ii,jj,kk)))
                       dx_nbrs(2,n_nbrs) = abs(-LVLSET(ii,jj,kk) * (ym(jjj)-ym(jj)) / &
                                           (LVLSET(iii,jjj,kkk) - LVLSET(ii,jj,kk)))
                       dx_nbrs(3,n_nbrs) = abs(-LVLSET(ii,jj,kk) * (zm(kkk)-zm(kk)) / &
                                           (LVLSET(iii,jjj,kkk) - LVLSET(ii,jj,kk)))
                    elseif (phi_flag(iii,jjj,kkk).eq.fmm_accepted) then
                       ! An accepted nbr
                       n_nbrs = n_nbrs + 1
                       phi_nbrs(n_nbrs) = phi_fmm(iii,jjj,kkk)
                       index_nbrs(n_nbrs) = local_index
                       dx_nbrs(1,n_nbrs) = abs(xm(iii)-xm(ii))
                       dx_nbrs(2,n_nbrs) = abs(ym(jjj)-ym(jj))
                       dx_nbrs(3,n_nbrs) = abs(zm(kkk)-zm(kk))
                    end if
                 end do
                 ! Set the radius in case of cyl coords
                 if (icyl.eq.1) then 
                    radius = abs(ym(jj))
                 else
                    radius = 1.0_WP
                 end if
                 ! Recompute phi
                 if (n_nbrs.gt.0) then
                    call fmm_phi_calc(local_phi,radius)
                    phi_fmm(ii,jj,kk) = min(phi_fmm(ii,jj,kk),local_phi)
                 end if
                 ! Modify the heap
                 m = rheap_index(ii,jj,kk)
                 if (phi_fmm(ii,jj,kk).gt.heap(m)%G) then
                    ! Check ...
                    if (phi_fmm(ii,jj,kk) .gt. heap(m)%G + 1.0E-12_WP) then
                       write(*,*) 'Error: dont expect significant phi increase'
                    end if
                    heap(m)%G = phi_fmm(ii,jj,kk)
                    call fmm_sift_minval_down(n_heap,m)
                 else
                    heap(m)%G = phi_fmm(ii,jj,kk)
                    call fmm_sift_newval_down(m)
                 end if
              end do  

              ! Process new close nodes ------------------------------
              do nn = 1,n_new_close
                 m = new_close(nn)  
                 select case(m)
                   case(1)
                     ii=max(imin_close,i-1); jj=j; kk=k
                     if (i.eq.ii) cycle
                     local_index = -1
                   case(2)
                     ii=min(imax_close,i+1); jj=j; kk=k
                     if (i.eq.ii) cycle
                     local_index = +1
                   case(3)
                     ii=i; jj=max(jmin_close,j-1); kk=k
                     if (j.eq.jj) cycle
                     local_index = -2
                   case(4)
                     ii=i; jj=min(jmax_close,j+1); kk=k
                     if (j.eq.jj) cycle
                     local_index = +2
                   case(5)
                     ii=i; jj=j; kk=max(kmin_close,k-1)
                     if (k.eq.kk) cycle
                     local_index = -3
                   case(6)
                     ii=i; jj=j; kk=min(kmax_close,k+1)
                     if (k.eq.kk) cycle
                     local_index = +3
                   case default
                     ! should never happen
                 end select

                 ! Change the flag
                 phi_flag(ii,jj,kk) = fmm_close
           
                 n_nbrs = 0
                 do nnn = 1,6
                    select case(nnn)
                      case(1)
                        iii=ii-1; jjj=jj; kkk=kk
                        local_index = -1
                      case(2)
                        iii=ii+1; jjj=jj; kkk=kk
                        local_index = +1
                      case(3)
                        iii=ii; jjj=jj-1; kkk=kk
                        local_index = -2
                      case(4)
                        iii=ii; jjj=jj+1; kkk=kk
                        local_index = +2
                      case(5)
                        iii=ii; jjj=jj; kkk=kk-1
                        local_index = -3
                      case(6)
                        iii=ii; jjj=jj; kkk=kk+1
                        local_index = +3
                      case default
                        ! should never happen
                    end select
                    if (LVLSET_band(iii,jjj,kkk).eq.0) cycle
                    if (mask(iii,jjj).eq.1 .or. mask(iii,jjj).eq.3) cycle
                    ! Check for nbrs
                    if ((LVLSET(ii,jj,kk)*LVLSET(iii,jjj,kkk)).le.0.0_WP) then
                       ! An intersection
                       n_nbrs = n_nbrs + 1
                       index_nbrs(n_nbrs) = local_index
                       phi_nbrs(n_nbrs) = 0.0_WP
                       dx_nbrs(1,n_nbrs) = abs(-LVLSET(ii,jj,kk) * (xm(iii)-xm(ii)) / &
                                           (LVLSET(iii,jjj,kkk) - LVLSET(ii,jj,kk)))
                       dx_nbrs(2,n_nbrs) = abs(-LVLSET(ii,jj,kk) * (ym(jjj)-ym(jj)) / &
                                           (LVLSET(iii,jjj,kkk) - LVLSET(ii,jj,kk)))
                       dx_nbrs(3,n_nbrs) = abs(-LVLSET(ii,jj,kk) * (zm(kkk)-zm(kk)) / &
                                           (LVLSET(iii,jjj,kkk) - LVLSET(ii,jj,kk)))
                    elseif (phi_flag(iii,jjj,kkk).eq.fmm_accepted) then
                       ! An accepted nbr
                       n_nbrs = n_nbrs + 1
                       index_nbrs(n_nbrs) = local_index
                       phi_nbrs(n_nbrs) = phi_fmm(iii,jjj,kkk)
                       dx_nbrs(1,n_nbrs) = abs(xm(iii)-xm(ii))
                       dx_nbrs(2,n_nbrs) = abs(ym(jjj)-ym(jj))
                       dx_nbrs(3,n_nbrs) = abs(zm(kkk)-zm(kk))
                    end if
                 end do
                 ! Set the radius in case of cyl coords
                 if (icyl.eq.1) then 
                    radius = abs(ym(jj))
                 else
                    radius = 1.0_WP
                 end if
                 ! Recompute phi
                 if (n_nbrs.gt.0) then
                    call fmm_phi_calc(local_phi,radius) 
                    phi_fmm(ii,jj,kk) = min(phi_fmm(ii,jj,kk),local_phi)
                 end if
                 ! Add to the heap
                 n_heap = n_heap + 1
                 heap(n_heap)%G = phi_fmm(ii,jj,kk)
                 heap(n_heap)%i = ii
                 heap(n_heap)%j = jj
                 heap(n_heap)%k = kk
                 rheap_index(ii,jj,kk) = n_heap
                 call fmm_sift_newval_down(n_heap)
              end do  
   
           end if   

           local_done = ((n_heap.eq.0).or.(local_counter.eq.local_counter_max))

        end do local_loop

        my_ibuf(3) = n_heap
        call MPI_ALLREDUCE(my_ibuf,ibuf,3,MPI_INTEGER,MPI_SUM,comm,ierr)
        global_done = ((ibuf(1).eq.ibuf(2)).and.(ibuf(3).eq.0))

     end do global_loop

  end do
  !call boundary_update_border_levelset_int(phi_flag,'+','ym')


  cutoff_outer = 0.80_WP*LVLSET_cutoff     
  cutoff_inner = 0.40_WP*LVLSET_cutoff  
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           if (abs(LVLSET_band(i,j,k)).ge.6) then
              phi_vel(i,j,k) = 0.0_WP
           else
              local_phi = abs(LVLSET(i,j,k))
              if (local_phi.gt.cutoff_inner) then
                 if (local_phi.le.cutoff_outer) then
                    phi_vel(i,j,k) = phi_vel(i,j,k) * &
                           (local_phi-cutoff_outer)**2.0_WP * & 
                           (2.0_WP*local_phi + cutoff_outer - 3.0_WP*cutoff_inner) / &
                           (cutoff_outer - cutoff_inner)**3.0_WP
                 else
                    phi_vel(i,j,k) = 0.0_WP
                 end if
              end if
           end if
        end do
     end do 
  end do
  
  call levelset_apply_bc(phi_vel,'+','ym')
  
  ! Now update the LVLSET variable
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           if (phi_fmm(i,j,k).ge.LVLSET_cutoff) phi_vel(i,j,k) = 0.0_WP
           !if (abs(LVLSET_band(i,j,k)).ge.3) then
           if (phi_flag(i,j,k).eq.fmm_accepted_plus) then
              LVLSET(i,j,k) = phi_fmm(i,j,k)
           elseif (phi_flag(i,j,k).eq.fmm_accepted_minus) then
              LVLSET(i,j,k) = -phi_fmm(i,j,k)
              !   phi_fmm(i,j,k) = -phi_fmm(i,j,k)
              !else
              !   phi_vel(i,j,k) = phi_vel_init(i,j,k)
           end if
           !end if
        end do
     end do
  end do
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           if (phi_flag(i,j,k).eq.fmm_accepted_minus) phi_fmm(i,j,k) = -phi_fmm(i,j,k)
        end do
     end do
  end do
  
  call levelset_apply_bc(LVLSET,'+','ym')
  call levelset_apply_bc(phi_vel,'+','ym')
  call levelset_apply_bc(phi_fmm,'+','ym')
  call boundary_update_border_int(phi_flag,'+','ym')
  
  return
end subroutine levelset_fmm_reinit

!===============================================================================
!===============================================================================

function fmm_parallel_recv(i,j,k,phi_value,vel_value) result(message)
!function fmm_parallel_recv(i,j,k,phi_value) result(message)
  use parallel
  implicit none
  integer, intent(out)   :: i,j,k
  real(WP), intent(out)  :: phi_value
  real(WP), intent(out)  :: vel_value
  !real(WP), dimension(4) :: val_recv
  real(WP), dimension(5) :: val_recv
  logical                :: message
  integer                :: status(MPI_STATUS_SIZE)
  integer                :: ierr,isource,imessage

  call MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, comm, imessage, status, ierr)
  if (ierr.ne.0) write(*,*) 'Problem in MPI_Iprobe'
  if (imessage.eq.0) then
     message = .FALSE.
  else
     message = .TRUE.
     isource = status(MPI_SOURCE)
     call MPI_Recv(val_recv,5,MPI_REAL_WP,isource,0,comm,status,ierr)
     !call MPI_Recv(val_recv,4,MPI_REAL_WP,isource,0,comm,status,ierr)
     i = nint(val_recv(1))
     j = nint(val_recv(2))
     k = nint(val_recv(3))
     phi_value = val_recv(4)
     vel_value = val_recv(5)
  end if

  return
end function fmm_parallel_recv

!===============================================================================
!===============================================================================

subroutine fmm_parallel_send(idest,i,j,k,phi_value,vel_value)
!subroutine fmm_parallel_send(idest,i,j,k,phi_value)
  use parallel
  implicit none
  integer, intent(in)   :: idest
  integer, intent(in)   :: i,j,k
  real(WP), intent(in)  :: phi_value
  real(WP), intent(in)  :: vel_value
  integer               :: ierr
  real(WP),dimension(5) :: buffer
  !real(WP),dimension(4) :: buffer

  buffer(1) = real(i,WP)
  buffer(2) = real(j,WP)
  buffer(3) = real(k,WP)
  buffer(4) = phi_value
  buffer(5) = vel_value

  ! use a buffered send...
  call MPI_Bsend(buffer,5,MPI_REAL_WP,idest,0,comm,ierr)
  !call MPI_Bsend(buffer,4,MPI_REAL_WP,idest,0,comm,ierr)
  if (ierr.ne.0) write(*,*) 'Problem with MPI_Bsend, irank = ',idest
  
  return
end subroutine fmm_parallel_send  

!===============================================================================
!===============================================================================

subroutine levelset_fmm_prestep
  use levelset_fmm
  use data
  implicit none
  
  ! Exit if not used
  if (.not.fmm_reinit) return
  
  ! Save the old normals
  lvlset_normal_x_old = lvlset_normal_x
  lvlset_normal_y_old = lvlset_normal_y
  lvlset_normal_z_old = lvlset_normal_z
  
  return
end subroutine levelset_fmm_prestep

