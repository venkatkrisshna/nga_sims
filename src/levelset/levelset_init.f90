subroutine levelset_field_init
  use data
  use levelset
  implicit none
  
  integer :: i,j,k
  real(WP) :: length
  
  ! Level set reset variables
  logical :: lvlset_reset
  character(len=str_medium) :: lvlset_geometry
  real(WP) :: loc(3)
  
  ! Level set reset variables
  call parser_read('Reset levelset',lvlset_reset,.false.)
  if (.not.lvlset_reset) return
  
  call parser_read('Level set geometry',lvlset_geometry)
  call parser_read('Level set location',loc)
  
  select case (trim(lvlset_geometry))
     
  case ('flat')  
     do k = kmino_,kmaxo_
        do j = jmino_,jmaxo_
           do i = imino_,imaxo_
              
              if     ((xm(i).le.loc(1)) .and. (ym(j).le.loc(2)) ) then
                 LVLSET(i,j,k) = loc(1) - xm(i)
              elseif ((xm(i).le.loc(1)) .and. (abs(ym(j)).gt.loc(2)) ) then
                 LVLSET(i,j,k) = loc(2) - abs(ym(j))
              elseif ((xm(i).gt.loc(1)) .and. (abs(ym(j)).le.loc(2)) ) then
                 LVLSET(i,j,k) = loc(1) - xm(i)
              else
                 LVLSET(i,j,k) = -sqrt((xm(i) -loc(1))**2.0 + (abs(ym(j))-loc(2))**2.0)
              end if
           end do
        end do
     end do
     
  case('sphere')
     if (icyl.ne.1) then
        do k = kmino_,kmaxo_
           do j = jmino_,jmaxo_
              do i = imino_,imaxo_
                 LVLSET(i,j,k) = loc(3) - sqrt( (xm(i)-loc(1))**2 + ym(j)**2 + zm(k)**2 )
                 
              end do
           end do
        end do
     else
        do k = kmino_,kmaxo_
           do j = jmino_,jmaxo_
              do i = imino_,imaxo_
                 LVLSET(i,j,k) = loc(3) - sqrt( &
                      (xm(i)-loc(1))**2 + &
                      (ym(j)*cos(zm(k))-loc(2))**2 + &
                      (ym(j)*sin(zm(k)))**2 )
              end do
           end do
        end do
     end if
     
  case ('cone')  
     do k = kmino_,kmaxo_
        do j = jmino_,jmaxo_
           do i = imino_,imaxo_
              if     (xm(i).le.loc(1)) then
                 LVLSET(i,j,k) = xm(i) - loc(1)
                 !elseif ((xm(i).gt.loc(1)).and.(abs(ym(j)).le.length)) then
              else
                 length = xm(i)*sin(loc(2))
                 LVLSET(i,j,k) = length-abs(ym(j))
              end if
           end do
        end do
     end do
     
  case default
     if (irank==iroot) then
        write(*,*) 'in levelset_init.f90 :'
        write(*,*) '     NOT A VALID INITIAL LEVELSET GEOMETRY'
     end if
     
  end select
  
  return
end subroutine levelset_field_init


!===============================================================================
!===============================================================================
subroutine levelset_volume
  use data
  use levelset
  use metric_generic
  implicit none
  
  integer :: i,j,k
  real(WP), parameter :: eps_vol = 1.0e-8_WP
  real(WP) :: Gm,Gdxi,Gdeta,Gdzeta
  real(WP) :: A,B,C,D,E
  real(WP), dimension(3) :: abs_gradG
  
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           
           ! Make G < 0
           Gm = -abs(LVLSET(i,j,k))
           
           ! Compute abs(grad(Gdist)*dx)
           abs_gradG(1) = dx(i)*abs(sum(grad_xm(i,j,:)*LVLSET(i-1:i+1,j,k)))
           abs_gradG(2) = dy(j)*abs(sum(grad_ym(i,j,:)*LVLSET(i,j-1:j+1,k)))
           if (icyl.eq.0) then
              abs_gradG(3) = dz      *abs(sum(grad_zm(i,j,:)*LVLSET(i,j,k-1:k+1)))
           else
              abs_gradG(3) = dz*ym(j)*abs(sum(grad_zm(i,j,:)*LVLSET(i,j,k-1:k+1)))
           end if
           
           ! Check dimensionality of the problem
           Gdxi   = maxval(abs_gradG(1:3))
           Gdzeta = minval(abs_gradG(1:3))
           Gdeta  = sum(abs_gradG(1:3)) - Gdxi - Gdzeta
           
           ! Compute coefficients
           A = max(Gm+0.5_WP*( Gdxi+Gdeta+Gdzeta),0.0_WP)
           B = max(Gm+0.5_WP*( Gdxi+Gdeta-Gdzeta),0.0_WP)
           C = max(Gm+0.5_WP*( Gdxi-Gdeta+Gdzeta),0.0_WP)
           D = max(Gm+0.5_WP*(-Gdxi+Gdeta+Gdzeta),0.0_WP)
           E = max(Gm+0.5_WP*( Gdxi-Gdeta-Gdzeta),0.0_WP)
           
           ! Compute volume fraction
           if (Gdxi.gt.eps_vol) then
              if (Gdeta.gt.eps_vol) then
                 if (Gdzeta.gt.eps_vol) then ! 3D
                    lvlset_vol(i,j,k) = (A**3-B**3-C**3-D**3+E**3)/(6.0_WP*Gdxi*Gdeta*Gdzeta)
                 else                        ! 2D
                    lvlset_vol(i,j,k) = (A**2-C**2)/(2.0_WP*Gdxi*Gdeta)
                 end if
              else                           ! 1D
                 lvlset_vol(i,j,k) = A/Gdxi
              end if
           else                              ! 0D
              if (Gm.ne.0.0_WP) then
                 lvlset_vol(i,j,k) = 0.0_WP
              else
                 lvlset_vol(i,j,k) = 0.5_WP
              end if
           end if
           
           ! Correct for sign of G
           !if (LVLSET(i,j,k).gt.0.0_WP) then
           !   lvlset_vol(i,j,k) = 1.0_WP-lvlset_vol(i,j,k)
           !end if
           if (LVLSET(i,j,k).lt.0.0_WP) then
              lvlset_vol(i,j,k) = 1.0_WP-lvlset_vol(i,j,k)
           end if
           
        end do
     end do
  end do
  
  ! Update boundaries
  call levelset_apply_bc(lvlset_vol,'+','ym')
  
  return
end subroutine levelset_volume
