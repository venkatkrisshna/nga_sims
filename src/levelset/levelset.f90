module levelset
  use precision
  use partition
  use geometry
  use time_info
  implicit none
  
  ! Solution at old time step : n
  real(WP), dimension(:,:,:), allocatable :: LVLSETold
  
  ! Source term between time n and n+1
  real(WP), dimension(:,:,:), allocatable :: srcLVLSET
  
  ! Residual between time n and n+1
  real(WP), dimension(:,:,:), allocatable :: ResLVLSET

  ! Level set scheme
  character(len=str_medium) :: lvlset_scheme

  ! # of reinitialization iterations
  integer :: reinit_freq

  ! Normal vector to the front
  real(WP), dimension(:,:,:), allocatable :: lvlset_normal_x
  real(WP), dimension(:,:,:), allocatable :: lvlset_normal_y
  real(WP), dimension(:,:,:), allocatable :: lvlset_normal_z
  real(WP), dimension(:,:,:), allocatable :: lvlset_kappa
  real(WP), dimension(:,:,:), allocatable :: lvlset_vol

  ! Values to monitor
  real(WP) :: levelset_int1
  real(WP) :: levelset_int2
  real(WP) :: max_resLVLSET
  
contains
  
  ! Apply boundary conditions on the levelset fields
  ! ------------------------------------------------
  subroutine levelset_apply_bc(A,sym,axis)
    implicit none
    real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: A
    character(len=*), intent(in) :: sym,axis
    
    ! Update Ghost Cells
    call boundary_update_border(A,sym,axis)
    
    ! Take care of non periodic BCs
    call boundary_neumann(A,'-xm')
    call boundary_neumann(A,'+xm')
    call boundary_neumann(A,'-ym')
    call boundary_neumann(A,'+ym')
    
    return
  end subroutine levelset_apply_bc
  
  ! Force values in the walls
  ! -------------------------
  subroutine levelset_update_walls(A)
    use masks
    implicit none
    real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: A
    integer :: i,j
    
    do j = jmin_,jmax_
       loop: do i = imin_,imax_
          if (mask(i,j).eq.1 .or. mask(i,j).eq.3) then
             if (mask(i-1,j).eq.0) then
                A(i,j,:) = A(i-1,j,:)
                cycle loop
             end if
             if (mask(i+1,j).eq.0) then
                A(i,j,:) = A(i+1,j,:)
                cycle loop
             end if
             if (mask(i,j-1).eq.0) then
                A(i,j,:) = A(i,j-1,:)
                cycle loop
             end if
             if (mask(i,j+1).eq.0) then
                A(i,j,:) = A(i,j+1,:)
                cycle loop
             end if
          end if
       end do loop
    end do
    
    return
  end subroutine levelset_update_walls
  
end module levelset


! ==================================================== !
! Initialize the scalar module                         !
!                                                      !
! -> allocate the arrays                               !
! -> update ghost cells                                !
! -> apply boundary conditions                         !
! -> run specific init                                 !
!                                                      !
! Before: LVLSET correct only inside the domain        !
!         -> imin_:imax_,jmin_:jmax_,kmin_:kmax_       !
! After : LVLSET correct everywhere                    !
!         -> imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_ !
! ==================================================== !
subroutine levelset_init
  use levelset
  use parser
  use data
  implicit none
  
  call parser_read('Use level set',use_lvlset,.false.)
  
  ! If no level set => exit
  if (.not.use_lvlset) return

  ! Create & Start the timer
  call timing_create('levelset')
  call timing_start ('levelset')
  
  ! Level set reset variables
  call levelset_field_init
  
  ! Apply boundary conditions
  call levelset_apply_bc(LVLSET,'+','ym')
  
  ! Allocate arrays for old solution
  allocate(LVLSETold      (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(srcLVLSET      (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(ResLVLSET      (imin_ :imax_ ,jmin_ :jmax_ ,kmin_ :kmax_ ))
  allocate(lvlset_normal_x(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(lvlset_normal_y(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(lvlset_normal_z(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(lvlset_kappa   (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(lvlset_vol     (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  
  ! Initialize the transport scheme
  call parser_read('Level set scheme',lvlset_scheme)
  select case (trim(lvlset_scheme))
  case ('houc')
     call levelset_houc_init
  case ('quick')
     call levelset_quick_init
  case default
     call die('levelset_init: unknown Level set scheme')
  end select
  
  ! Get info about how often to reinitialize
  call parser_read('Reinit frequency',reinit_freq)
  
  ! Initialize the different reinitializations
  call levelset_marker_init
  call levelset_fmm_init
  call levelset_pde_init
  
  ! Set the level set to a distance function
  call levelset_pde_prestep
  call levelset_pde_reinit
  call levelset_fmm_reinit
  call levelset_cutoff
  
  ! Initialize the burning velocity model
  call levelset_source_init
 
  ! Create new file to monitor at each iterations
  call monitor_create_file_step('levelset',2)
  call monitor_set_header(1,'Integral','r')
  call monitor_set_header(2,'Liquid volume','r')
  
  ! Create new file to monitor at each subiterations
  call monitor_create_file_iter('convergence_levelset',1)
  call monitor_set_header(1,'res_lvlset','r')
  
  ! Stop the timer
  call timing_stop('levelset')

  return
end subroutine levelset_init


! ===================================================== !
! PRE-TIMESTEP Routine                                  !
!                                                       !
! -> Set up the iterative process                       !
! -> Compute the source term for the level set equation !
! ===================================================== !
subroutine levelset_prestep
  use levelset
  use data
  implicit none
  
  ! If no level set => exit
  if (.not.use_lvlset) return

  ! Start the timer
  call timing_start('levelset')
  
  ! Save the old velocity
  LVLSETold = LVLSET
  
  ! Zero the source term
  srcLVLSET = 0.0_WP
  
  ! FMM specific prestep
  call levelset_fmm_prestep

  ! Stop the timer
  call timing_stop('levelset')
  
  return
end subroutine levelset_prestep


! ========================================================== !
! ADVANCE the solution                                       !
!   -> second order in time                                  !
!   -> variable accuracy in space                            !
!   -> explicit prediction                                   !
!   -> ADI to sequentially make implicit corrections         !
!                                                            !
! Z(n+3/2,k+1) = Z(n+1/2) + dt*F(0.5*(Z(n+3/2,k)+Z(n+1/2))   !
!                   + 0.5*dt*dF/dZ*(Z(n+3/2,k+1)-Z(n+3/2,k)) !
! n : time step                                              !
! k : inner loop iteration                                   !
! Velocity field used : best approximation for U(n+1)        !
! ========================================================== !
subroutine levelset_step
  use levelset
  use time_info
  use data
  implicit none
  integer :: i,j,k

  ! If no level set => exit
  if (.not.use_lvlset) return
  
  ! Start the timer
  call timing_start('levelset')
  
  ! Compute mid point
  LVLSET = 0.5_WP*(LVLSET+LVLSETold)
  
  ! Compute the source term, which is usually a burning velocity
  call levelset_source_compute
  
  ! Solve for the level set
  select case (trim(lvlset_scheme))
  case ('houc')
     call levelset_houc_residual
     ! explicit - no implicit !
  case ('quick')
     call levelset_quick_residual
     call levelset_quick_inverse
  end select
  
  ! Update the level set
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           LVLSET(i,j,k) = 2.0_WP*LVLSET(i,j,k)-LVLSETold(i,j,k) & 
                + ResLVLSET(i,j,k)
        end do
     end do
  end do
  call levelset_cutoff

  ! Update the physical boundaries
  call levelset_apply_bc(LVLSET,'+','ym')
  
  ! Update the level set in the walls
  call levelset_update_walls(LVLSET)
  
  ! Compute maximum of residuals
  call parallel_max(maxval(abs(resLVLSET)),max_resLVLSET)
  
  ! Transfer values to monitor
  call monitor_select_file('convergence_levelset')
  call monitor_set_single_value(1,max_resLVLSET)
  
  ! Stop the timer
  call timing_stop('levelset')
  
  return
end subroutine levelset_step


! ===================================================== !
! POST-TIMESTEP Routine                                 !
!                                                       !
! -> Set the level set to be a distance function        !
! ===================================================== !
subroutine levelset_poststep
  use levelset
  use time_info
  use data
  implicit none
  
  ! If no level set => exit
  if (.not.use_lvlset) return
  
  ! Start the timer
  call timing_start('levelset')
  
  ! Recompute the bands
  call levelset_set_bands
  
  ! Set the level set to be a distance function
  if (mod(ntime,reinit_freq).eq.0) then
     call levelset_pde_prestep
     call levelset_fmm_reinit
     call levelset_pde_reinit
  end if
  
  ! Stop the timer
  call timing_stop('levelset')
  
  return
end subroutine levelset_poststep


! ============================ !
! Monitor the level set solver !
! ============================ !
subroutine levelset_monitor
  use data
  use levelset
  implicit none

  integer :: i,j,k
  real(WP) :: tmp1
  real(WP) :: tmp2

  if (.not.use_lvlset) return
  
  ! Start the timer
  call timing_start('levelset')
  
  call levelset_volume
  
  ! Integrals of G
  tmp1 = 0.0_WP
  tmp2 = 0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           tmp1 = tmp1 + LVLSET(i,j,k)*vol(i,j,k)
           tmp2 = tmp2 + lvlset_vol(i,j,k)*vol(i,j,k)
        end do
     end do
  end do
  call parallel_sum(tmp1,levelset_int1)
  call parallel_sum(tmp2,levelset_int2)
  
  ! Transfer to monitor
  call monitor_select_file("levelset")
  call monitor_set_single_value(1,levelset_int1)
  call monitor_set_single_value(2,levelset_int2)
  
  ! Stop the timer
  call timing_stop('levelset')
  
  return
end subroutine levelset_monitor
