!===============================================================================
!===============================================================================

module levelset_heapsort
  use precision
  implicit none

  type heap_type
    real(WP) :: G         ! level set value
    integer  :: i,j,k     ! index
  end type heap_type
  
  type(heap_type), allocatable :: heap(:)   ! heap of close nodes
  
  integer, allocatable :: rheap_index(:,:,:) ! reverse heap index

  integer :: n_heap  ! size of the heap

  contains

  !=============================================================================

  subroutine fmm_heapify(array_size)
    implicit none

    integer, intent(in) :: array_size
    integer             :: i

    do i = floor(real(array_size,WP)/2.0_WP),1,-1
       call fmm_sift_minval_down(array_size,i)
    end do

    return
  end subroutine fmm_heapify

  !=============================================================================

  subroutine fmm_sift_minval_down(array_size,start)
    implicit none

    integer, intent(in) :: array_size, start
    integer             :: k, k_half, child
    type(heap_type)     :: heap_store

    ! This subroutine makes sure that the start value is smaller than
    ! all values between start*2 and the first non-decreasing value
    ! above start*2
  
    k          = start
    heap_store = heap(k)
    k_half     = floor(real(array_size,WP)/2.0_WP)

    do while (k .le. k_half)

       child = 2*k

       if (child .lt. array_size) then
          ! if child values in correct order, move to next child
          if (heap(child)%G .gt. heap(child+1)%G) child = child + 1
       end if

       ! if child value larger than start value, start value is min, so exit
       if (heap_store%G .le. heap(child)%G) exit

       ! otherwise child is the smaller value: swap with start value
       heap(k) = heap(child)
       rheap_index(heap(k)%i,heap(k)%j,heap(k)%k) = k
       k = child

    end do
  
    ! put the stored start value into whatever child node swapped with it
    heap(k) = heap_store
    rheap_index(heap(k)%i,heap(k)%j,heap(k)%k) = k

    return
  end subroutine fmm_sift_minval_down

  !=============================================================================

  subroutine fmm_sift_newval_down(start)
    implicit none
  
    integer, intent(in) :: start
    integer             :: k, parent
    type(heap_type)     :: heap_store

    k = start
    heap_store = heap(k)

    do while (k.gt.1) 
 
       parent = floor(real(k,WP)/2.0_WP)
  
       ! if parent is smaller than start value minval is already low, so exit
       if (heap(parent)%G.le.heap_store%G) exit

       ! otherwise, parent is the larger value: swap with start value
       heap(k) = heap(parent)
       rheap_index(heap(k)%i,heap(k)%j,heap(k)%k) = k
       k = parent

    end do

    ! put the stored start value into whatever parent node swapped with it
    heap(k) = heap_store
    rheap_index(heap(k)%i,heap(k)%j,heap(k)%k) = k

    return
  end subroutine fmm_sift_newval_down

  !=============================================================================

  subroutine fmm_delete_root(array_size)
    implicit none

    integer, intent(inout) :: array_size

    ! deletes the root, moves last elemt of heap to top and performs a downheap

    if (array_size .gt. 1) then
       heap(1) = heap(array_size)
       call fmm_sift_minval_down(array_size-1,1)
    end if

    array_size = array_size - 1

    return
  end subroutine fmm_delete_root

  !=============================================================================

end module levelset_heapsort
  
!===============================================================================
!===============================================================================
