module levelset_pde
  use levelset
  use data
  implicit none
  
  ! Do we need to reinit?
  logical :: pde_reinit

  real(WP), dimension(:,:,:,:), allocatable :: reinit_x,reinit_y,reinit_z
  real(WP), dimension(:,:)    , allocatable :: HJ_factor
  real(WP), dimension(:,:)    , allocatable :: RK_coeff
  real(WP), dimension(:,:,:,:), allocatable :: dGdt
  real(WP), dimension(:,:,:)  , allocatable :: G_0
  real(WP)                              :: dt_reinit
  integer                               :: RK_reinit_order

end module levelset_pde


! ========================================================== !
! Initialize the variables to solve the Hamilton-Jacobi PDE  !
! ========================================================== !
subroutine levelset_pde_init
  use levelset_pde
  use masks
  use parser
  implicit none
  real(WP) :: dt_test, CFL_reinit
  integer  :: i,j,k,ii,jj
  
  call parser_read('PDE reinit',pde_reinit,.false.)
  
  ! If reinit not used, exit
  if (.not.pde_reinit) return
  
  ! Compute the time step limitation for reinitialization
  CFL_reinit = 0.5_WP  
  dt_reinit = huge(1.0_WP)
  dt_test   = huge(1.0_WP)
  if (icyl.eq.1) then
     do k = kmin_,kmax_
        do j=max(jmin_,jmin+1),jmax_
        !do j=jmin_,jmax_
           do i=imin_,imax_
              dt_test = min(dt_test, CFL_reinit * & 
                              (dx(i)*dy(j)*ym(j)*dz)**(1.0_WP/3.0_WP))
              dt_test = min(dt_test, CFL_reinit*ym(j)*dz)
              dt_test = min(dt_test, CFL_reinit*dx(i))
              dt_test = min(dt_test, CFL_reinit*dy(j))
           end do
        end do
     end do
  else
     do k = kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              dt_test = min(dt_test,CFL_reinit * & 
                              (dx(i)*dy(j)*dz)**(1.0_WP/3.0_WP))
              dt_test = min(dt_test, CFL_reinit*dz   )
              dt_test = min(dt_test, CFL_reinit*dx(i))
              dt_test = min(dt_test, CFL_reinit*dy(j))
           end do
        end do
     end do
  end if
  call parallel_min(dt_test,dt_reinit)
  
  ! Set coefficients for Runge-Kutta reinitialization
  call parser_read('PDE RK order',RK_reinit_order)
  select case (RK_reinit_order)
   case(1)
      allocate(RK_coeff(0:0,1:1))
      !n_RK_steps = 1
      RK_coeff(0,1) = 1.0_WP
   case(2)
      allocate(RK_coeff(0:1,1:2))
      !n_RK_steps = 2
      RK_coeff(0,1) =  1.0_WP
      RK_coeff(0,2) = -0.5_WP
      RK_coeff(1,2) =  0.5_WP
   case(3)
      allocate(RK_coeff(0:2,1:3))
      !n_RK_steps = 3
      RK_coeff(0,1) =  1.00_WP
      RK_coeff(0,2) = -0.75_WP
      RK_coeff(1,2) =  0.25_WP
      RK_coeff(0,3) = -1.00_WP/12.00_WP
      RK_coeff(1,3) = -1.00_WP/12.00_WP
      RK_coeff(2,3) =  2.00_WP/3.00_WP
   case(4)
      allocate(RK_coeff(0:3,1:4))
      !n_RK_steps = 4
      RK_coeff(0,1) =  0.50_WP
      RK_coeff(0,2) = -0.50_WP
      RK_coeff(1,2) =  0.50_WP
      RK_coeff(0,3) =  0.00_WP
      RK_coeff(1,3) = -0.50_WP
      RK_coeff(2,3) =  1.00_WP
      RK_coeff(0,4) =  1.00_WP/6.00_WP
      RK_coeff(1,4) =  1.00_WP/3.00_WP
      RK_coeff(2,4) = -2.00_WP/3.00_WP
      RK_coeff(3,4) =  1.00_WP/6.00_WP
   case default
      call die('Unknown RK reinit order for level set')
  end select   

  ! Allocate the arrays - use the reinit scheme
  allocate(reinit_x (imin_:imax_,jmin_:jmax_,kmin_:kmax_,8))
  allocate(reinit_y (imin_:imax_,jmin_:jmax_,kmin_:kmax_,8))
  allocate(reinit_z (imin_:imax_,jmin_:jmax_,kmin_:kmax_,8))
  allocate(HJ_factor(imin_:imax_,jmin_:jmax_))
  allocate(dGdt     (imin_:imax_,jmin_:jmax_,kmin_:kmax_,RK_reinit_order))
  allocate(G_0      (imin_:imax_,jmin_:jmax_,kmin_:kmax_))

  ! Account for the walls, where the magnitude of the gradient
  ! may not be exactly 1
  HJ_factor = 1.0_WP
  do j=jmin_,jmax_
  do i=imin_,imax_
     do jj=j-1,j+1
     do ii=i-1,i+1
        if (mask(ii,jj).ne.0) then
           HJ_factor(i,j) = 0.0_WP
        end if
     end do
     end do
  end do
  end do
 
  return
end subroutine levelset_pde_init

!===============================================================================
!===============================================================================

subroutine levelset_pde_prestep
  use levelset_pde
  implicit none
  integer :: i,j,k
  
  ! Exit, if no reinit
  if (.not.pde_reinit) return
  
  do k = kmin_,kmax_
     do j = jmin_,jmax_
        do i = imin_,imax_
           G_0(i,j,k) = LVLSET(i,j,k)
        end do
     end do
  end do
  
  return
end subroutine levelset_pde_prestep


! ========================================================== !
! Set the coefficients for Hamilton-Jacobi operator          !
! ========================================================== !
subroutine levelset_reinit_weno3_coeff
  use levelset_pde
  implicit none
  integer  :: i,j,k
  real(WP) :: r_minus, r_plus, w_minus, w_plus
  real(WP), parameter :: epsilon = 1.0e-9_WP

  ! 3rd order WENO coefficients from the paper:
  ! 'Weighted ENO Schemes for Hamilton-Jacobi Equations',
  ! Guang-Shan Jiang and Danping Peng, 
  ! JCP, Vol. 21 No. 6, pp 2126-2143
  
  do k=kmin_,kmax_
  do j=jmin_,jmax_
  do i=imin_,imax_
           
     ! X DIRN ==================================================
     ! Direction x - left biased stencil
     r_minus = (epsilon + (LVLSET(i  ,j,k) - &
                    2.0_WP*LVLSET(i-1,j,k) + &
                           LVLSET(i-2,j,k) )**2.0_WP) / & 
               (epsilon + (LVLSET(i+1,j,k) - &
                    2.0_WP*LVLSET(i  ,j,k) + &
                           LVLSET(i-1,j,k) )**2.0_WP)
     w_minus = 1.0_WP/(1.0_WP+2.0_WP*r_minus**2.0_WP)
     reinit_x(i,j,k,1) = 0.5_WP*(dxmi(i-2)*w_minus)
     reinit_x(i,j,k,2) = 0.5_WP*(dxmi(i-1)*(-1.0_WP - 2.0_WP*w_minus) + &
                                 dxmi(i-2)*(-w_minus) ) 
     reinit_x(i,j,k,3) = 0.5_WP*(dxmi(i-1)*( 1.0_WP + 2.0_WP*w_minus) + &
                                 dxmi(i  )*(-1.0_WP + w_minus) )
     reinit_x(i,j,k,4) = 0.5_WP*(dxmi(i  )*( 1.0_WP - w_minus) ) 
                                
     ! Direction x - right biased stencil
     r_plus = (epsilon + (LVLSET(i+2,j,k) - &
                   2.0_WP*LVLSET(i+1,j,k) + &
                          LVLSET(i  ,j,k))**2.0_WP) / & 
              (epsilon + (LVLSET(i+1,j,k) - &
                   2.0_WP*LVLSET(i  ,j,k) + &
                          LVLSET(i-1,j,k))**2.0_WP)
     w_plus = 1.0_WP/(1.0_WP+2.0_WP*r_plus**2.0_WP)
     reinit_x(i,j,k,5) = 0.5_WP*(dxmi(i-1)*(-1.0_WP + w_plus))
     reinit_x(i,j,k,6) = 0.5_WP*(dxmi(i-1)*( 1.0_WP - w_plus) + &
                                 dxmi(i  )*(-1.0_WP - 2.0_WP*w_plus) ) 
     reinit_x(i,j,k,7) = 0.5_WP*(dxmi(i  )*( 1.0_WP + 2.0_WP*w_plus) + &
                                 dxmi(i+1)*( w_plus) )
     reinit_x(i,j,k,8) = 0.5_WP*(dxmi(i+1)*(-w_plus) ) 
     
     ! Y DIRN ==================================================
     ! Direction y - left biased stencil
     r_minus = (epsilon + (LVLSET(i,j  ,k) - &
                    2.0_WP*LVLSET(i,j-1,k) + &
                           LVLSET(i,j-2,k) )**2.0_WP) / & 
               (epsilon + (LVLSET(i,j+1,k) - &
                    2.0_WP*LVLSET(i,j  ,k) + &
                           LVLSET(i,j-1,k) )**2.0_WP)
     w_minus = 1.0_WP/(1.0_WP+2.0_WP*r_minus**2.0_WP)
     reinit_y(i,j,k,1) = 0.5_WP*(dymi(j-2)*w_minus)
     reinit_y(i,j,k,2) = 0.5_WP*(dymi(j-1)*(-1.0_WP - 2.0_WP*w_minus) + &
                                 dymi(j-2)*(-w_minus) ) 
     reinit_y(i,j,k,3) = 0.5_WP*(dymi(j-1)*( 1.0_WP + 2.0_WP*w_minus) + &
                                 dymi(j  )*(-1.0_WP + w_minus) )
     reinit_y(i,j,k,4) = 0.5_WP*(dymi(j  )*( 1.0_WP - w_minus) ) 
                                
     ! Direction y - right biased stencil
     r_plus = (epsilon + (LVLSET(i,j+2,k) - &
                   2.0_WP*LVLSET(i,j+1,k) + &
                          LVLSET(i,j  ,k))**2.0_WP) / & 
              (epsilon + (LVLSET(i,j+1,k) - &
                   2.0_WP*LVLSET(i,j  ,k) + &
                          LVLSET(i,j-1,k))**2.0_WP)
     w_plus = 1.0_WP/(1.0_WP+2.0_WP*r_plus**2.0_WP)
     reinit_y(i,j,k,5) = 0.5_WP*(dymi(j-1)*(-1.0_WP + w_plus))
     reinit_y(i,j,k,6) = 0.5_WP*(dymi(j-1)*( 1.0_WP - w_plus) + &
                                 dymi(j  )*(-1.0_WP - 2.0_WP*w_plus) ) 
     reinit_y(i,j,k,7) = 0.5_WP*(dymi(j  )*( 1.0_WP + 2.0_WP*w_plus) + &
                                 dymi(j+1)*( w_plus) )
     reinit_y(i,j,k,8) = 0.5_WP*(dymi(j+1)*(-w_plus) ) 
     
     ! Z DIRN ==================================================
     ! Direction z - left biased stencil
     r_minus = (epsilon + (LVLSET(i,j,k  ) - &
                    2.0_WP*LVLSET(i,j,k-1) + &
                           LVLSET(i,j,k-2) )**2.0_WP) / & 
               (epsilon + (LVLSET(i,j,k+1) - &
                    2.0_WP*LVLSET(i,j,k  ) + &
                           LVLSET(i,j,k-1) )**2.0_WP)
     w_minus = 1.0_WP/(1.0_WP+2.0_WP*r_minus**2.0_WP)
     reinit_z(i,j,k,1) = 0.5_WP*(dzi*w_minus)
     reinit_z(i,j,k,2) = 0.5_WP*(dzi*(-1.0_WP - 2.0_WP*w_minus) + &
                                 dzi*(-w_minus) ) 
     reinit_z(i,j,k,3) = 0.5_WP*(dzi*( 1.0_WP + 2.0_WP*w_minus) + &
                                 dzi*(-1.0_WP + w_minus) )
     reinit_z(i,j,k,4) = 0.5_WP*(dzi*( 1.0_WP - w_minus) ) 
                                
     ! Direction z - right biased stencil
     r_plus = (epsilon + (LVLSET(i,j,k+2) - &
                   2.0_WP*LVLSET(i,j,k+1) + &
                          LVLSET(i,j,k  ))**2.0_WP) / & 
              (epsilon + (LVLSET(i,j,k+1) - &
                   2.0_WP*LVLSET(i,j,k  ) + &
                          LVLSET(i,j,k-1))**2.0_WP)
     w_plus = 1.0_WP/(1.0_WP+2.0_WP*r_plus**2.0_WP)
     reinit_z(i,j,k,5) = 0.5_WP*(dzi*(-1.0_WP + w_plus))
     reinit_z(i,j,k,6) = 0.5_WP*(dzi*( 1.0_WP - w_plus) + &
                                 dzi*(-1.0_WP - 2.0_WP*w_plus) ) 
     reinit_z(i,j,k,7) = 0.5_WP*(dzi*( 1.0_WP + 2.0_WP*w_plus) + &
                                 dzi*( w_plus ))
     reinit_z(i,j,k,8) = 0.5_WP*(dzi*(-w_plus )) 

     if (icyl.eq.1) reinit_z(i,j,k,:) = reinit_z(i,j,k,:)*ymi(j)
     
  end do
  end do
  end do

  return
end subroutine levelset_reinit_weno3_coeff



! =========================================================== !
! Solve the Hamilton Jacobi Reinitialization equation         !
! =========================================================== !
subroutine levelset_pde_reinit
  use levelset_pde
  use levelset_marker
  implicit none
  integer             :: i,j,k,n,nn
  integer             :: jmin_loc
  real(WP)            :: G_plus, G_minus, G_sign
  real(WP)            :: dGdx_p, dGdx_m, dGdy_p, dGdy_m, dGdz_p, dGdz_m
  real(WP)            :: delta_val

  ! Solve the Hamilton-Jacobi PDE that will force the level set 
  ! field variable to be a distance function.
  ! The Godunov flux scheme used here is from page 2142 of
  ! 'Weighted ENO Schemes for Hamilton-Jacobi Equations',
  ! Guang-Shan Jiang and Danping Peng,
  ! JCP, Vol. 21 No. 6, pp 2126-2143
  
  ! Exit if not used
  if (.not.pde_reinit) return
  
  call levelset_reinit_weno3_coeff
  call levelset_centerline_average

  if (icyl.eq.1 .and. jproc.eq.1) then
     jmin_loc = jmin+1
  else
     jmin_loc = jmin_
  end if
  
  dGdt = 0.0_WP
  do nn = 1, RK_reinit_order
     do k=kmin_,kmax_
        do j=jmin_loc,jmax_
           do i=imin_,imax_   
              
              if (LVLSET_band(i,j,k).eq.0) cycle 
                  
              dGdx_m = sum(reinit_x(i,j,k,1:4)*LVLSET(i-2:i+1,j,k)) 
              dGdx_p = sum(reinit_x(i,j,k,5:8)*LVLSET(i-1:i+2,j,k)) 
              dGdy_m = sum(reinit_y(i,j,k,1:4)*LVLSET(i,j-2:j+1,k)) 
              dGdy_p = sum(reinit_y(i,j,k,5:8)*LVLSET(i,j-1:j+2,k))
              dGdz_m = sum(reinit_z(i,j,k,1:4)*LVLSET(i,j,k-2:k+1))
              dGdz_p = sum(reinit_z(i,j,k,5:8)*LVLSET(i,j,k-1:k+2))
   
              G_plus = sqrt(max(max(dGdx_m,0.0_WP)**2,min(dGdx_p,0.0_WP)**2) &
                          + max(max(dGdy_m,0.0_WP)**2,min(dGdy_p,0.0_WP)**2) &
                          + max(max(dGdz_m,0.0_WP)**2,min(dGdz_p,0.0_WP)**2))
                      
              G_minus =sqrt(max(min(dGdx_m,0.0_WP)**2,max(dGdx_p,0.0_WP)**2) &
                          + max(min(dGdy_m,0.0_WP)**2,max(dGdy_p,0.0_WP)**2) &
                          + max(min(dGdz_m,0.0_WP)**2,max(dGdz_p,0.0_WP)**2))
                          
              !if     (LVLSET_band(i,j,k).lt.0) then
              !   G_sign = -1.0_WP
              !elseif (LVLSET_band(i,j,k).gt.0) then
              !   G_sign =  1.0_WP
              !end if                                                  

              if (icyl.eq.1) then
                 delta_val = (dxm(i)*dym(j)*ym(j)*dz)**(1.0/3.0)
              else
                 delta_val = (dxm(i)*dym(j)      *dz)**(1.0/3.0)
              end if
              G_sign = G_0(i,j,k) / &
                       sqrt(G_0(i,j,k)**2 + delta_val**2 + 1.0e-9_WP)
                                    
              G_plus  = G_plus  - 1.0_WP
              G_minus = G_minus - 1.0_WP
                                       
              dGdt(i,j,k,nn) = -HJ_factor(i,j)*(max(G_sign,0.0_WP)*G_plus + &
                                                min(G_sign,0.0_WP)*G_minus )
                                                   
           end do
        end do
     end do
     
     do n = 0, nn-1
        do k=kmin_,kmax_
           do j=jmin_loc,jmax_
              do i=imin_,imax_
                 
                 LVLSET(i,j,k) = LVLSET(i,j,k) + dt_reinit * & 
                                                 RK_coeff(n,nn)* & 
                                                 dGdt(i,j,k,n+1)
              end do
           end do
        end do
     end do
     
     call levelset_apply_bc(LVLSET,'+','ym')
  end do

  call levelset_cutoff
  
  return
end subroutine levelset_pde_reinit

!===============================================================================
!===============================================================================

subroutine levelset_centerline_average
  use levelset_pde
  implicit none
  integer  :: i
  real(WP) :: tmp_sum

  if (icyl.ne.1) return
  
  ! Average the centerline values
  if (jproc.eq.1) then
     do i = imino_,imaxo_
        !tmp_sum = sum(LVLSET(i,jmin+1,kmin:kmax))
        tmp_sum = sum(LVLSET(i,jmin,kmin:kmax))
        tmp_sum = tmp_sum / real(kmax+1-kmin,WP)
        LVLSET(i,jmino:jmin,:) = tmp_sum
     end do
  end if

  return
end subroutine levelset_centerline_average

!===============================================================================
!===============================================================================
