module levelset_source
  use levelset
  implicit none
  
  ! Level set source model
  character(len=str_medium) :: lvlset_src

  ! Link to temporary arrays
  real(WP), dimension(:,:,:), pointer :: RHO_u  ! Unburned density
  real(WP), dimension(:,:,:), pointer :: SL     ! Burning velocity
  real(WP), dimension(:,:,:), pointer :: lF     ! Flame thickness

  ! Input specified parameters
  real(WP) :: SL_input
  real(WP) :: lF_input
  
!!$  real(WP), dimension(:,:,:), allocatable :: G_st
!!$  real(WP), dimension(:,:,:), allocatable :: FG_st  
!!$  
!!$  real(WP), dimension(:,:,:), allocatable :: sl_table
!!$  real(WP), dimension(:,:,:), allocatable :: lf_table  
!!$  
!!$  real(WP), dimension(:,:,:), allocatable :: C_st
!!$  real(WP), dimension(:,:,:), allocatable :: delta_f, delta_ff
!!$  
!!$  real(WP), dimension(:,:,:), allocatable :: dyn_alpha
!!$  
!!$  ! Dynamic burning velocity model variables
!!$  real(WP), dimension(:,:,:,:), allocatable :: lvlset_normal_f
!!$  real(WP), dimension(:,:,:),   allocatable :: lvlset_kappa_f
!!$  real(WP), dimension(:,:,:),   allocatable :: uprime_f, uprime_ff
!!$  real(WP), dimension(:,:,:),   allocatable :: Da_f, Da_ff
!!$  real(WP), dimension(:,:,:),   allocatable :: rho_rat_f, rho_rat_ff
!!$  real(WP), dimension(:,:,:),   allocatable :: AperV_f, AperV_ff
!!$  real(WP), dimension(:,:,:),   allocatable :: dyn_rhs, dyn_res
!!$  real(WP), dimension(:,:,:),   allocatable :: DIFF_avg
  
contains 
  
  ! Compute the unburned density, flame speed, and flame thickness
  ! GB: Don't know if it is the best place to put that...
  ! --------------------------------------------------------------
  subroutine levelset_flame_parameters
    use combustion
    implicit none
    
    select case(trim(chemistry))
    case ('none')
       ! Constant properties from input file
       RHO_u = rho_input(1)
       SL = SL_input
       lF = lF_input
       
    case ('premixed')
       ! Get everything from premixed table
       call premixed_chemtable_lookup('RHOu', RHO_u, &
            SC(:,:,:,isc_PROG), CVAR, SC(:,:,:,isc_ZMIX), nxo_*nyo_*nzo_)
       call premixed_chemtable_lookup('SL', SL, &
            SC(:,:,:,isc_PROG), CVAR, SC(:,:,:,isc_ZMIX), nxo_*nyo_*nzo_)
       call premixed_chemtable_lookup('lF', lF, &
            SC(:,:,:,isc_PROG), CVAR, SC(:,:,:,isc_ZMIX), nxo_*nyo_*nzo_)
       
    end select
    
    return
  end subroutine levelset_flame_parameters
  
!!$  function delta_st(G) result(d)
!!$    implicit none
!!$    real(WP) :: d, Pi
!!$    real(WP) :: delta_width
!!$    real(WP), intent(in) :: G
!!$    
!!$    delta_width = 0.005_WP
!!$    Pi = 3.14159_WP
!!$    
!!$    if (abs(G) >= delta_width) then
!!$       d = 0.0_WP
!!$    else
!!$       d = 0.5_WP/delta_width*(1.0_WP+cos(Pi*G/delta_width))
!!$    end if
!!$  end function delta_st
  
  ! Compute a smoothed heaviside function
  ! -------------------------------------
  function heaviside_st(G) result(H)
    use math
    implicit none 
    real(WP) :: H
    real(WP), intent(in) :: G    
    real(WP), parameter :: heaviside_width = 0.12_WP
    
    if (G <= -heaviside_width) then
       H = 0.0_WP
    elseif (G >= heaviside_width) then
       H = 1.0_WP
    else
       H = 0.5_WP + 0.5_WP/heaviside_width * G + 0.5_WP/Pi * sin(Pi/heaviside_width*G)
    end if
    H = H - 0.5_WP
    
  end function heaviside_st
  
end module levelset_source


! ================================= !
! Initialize the source term module !
! ================================= !
subroutine levelset_source_init
  use levelset_source
  use memory
  use parser
  implicit none
  
  ! Initialize the turbulent burning velocity model
  call parser_read('Level set source',lvlset_src,'none')
  if (trim(lvlset_src).eq.'none') return
 
  ! Choose which burning speed model to use
  select case (trim(lvlset_src))
     
  case ('laminar')
     ! nothing needs to be done here
     
  case ('turbulent')
     ! nothing needs to be done here
     
!!$  case ('dynamic')
!!$     allocate(G_st         (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
!!$     allocate(FG_st        (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))   
!!$     allocate(delta_f      (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
!!$     allocate(delta_ff     (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))  
!!$     allocate(C_st         (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
!!$     allocate(dyn_alpha    (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
!!$     
!!$     allocate(lvlset_normal_f(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3))
!!$     allocate(lvlset_kappa_f(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
!!$     allocate(uprime_f (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
!!$     allocate(uprime_ff(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
!!$     allocate(Da_f        (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
!!$     allocate(Da_ff       (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
!!$     allocate(AperV_f     (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
!!$     allocate(AperV_ff    (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
!!$     allocate(rho_rat_f   (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
!!$     allocate(rho_rat_ff  (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
!!$     allocate(dyn_rhs     (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
!!$     allocate(dyn_res     (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
!!$     allocate(DIFF_avg    (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
!!$     
!!$     !call levelset_extend_area_init
!!$     
!!$  case ('turbulent Lewis number dependent')
!!$     ! nothing needs to be done here
     
  case default
     call die('levelset_source_init: unknown burning speed model')        
  end select
  
  ! Link temporary arrays
  RHO_u => tmp1
  SL => tmp2
  lF => tmp3
  
  ! Read default values for Sl and lF
  call parser_read('Constant SL', SL_input, 0.0_WP)
  call parser_read('Constant lF', lF_input, 0.0_WP)
  
  return
end subroutine levelset_source_init


! =========================================== !
! Evaluate the source term for the G-equation !
! =========================================== !
subroutine levelset_source_compute
  use levelset_source
  use levelset_marker
  use data
  use masks
  use filter
  implicit none
  
  integer  :: i,j,k
  
  ! Peters model for ST
  ! Equ. 2.196, Page 133
  real(WP), parameter :: alpha = 0.780_WP
  real(WP), parameter :: beta  = 0.195_WP
  
  real(WP) :: uprime, Damk, ST, RHOmid
  real(WP) :: abs_G, cutoff_inner, cutoff_outer
  
!!$  ! For the non-unity Lewis number case
!!$  real(WP), external :: quadf
!!$  real(WP) :: Ma_length, Le_num, Ma_num, Ma_delta_integral
!!$  real(WP) :: left_bound, right_bound
!!$  real(WP) :: Ma_delta_sum
!!$  real(WP) :: Ma_delta_h
!!$  real(WP) :: function_value
!!$  real(WP) :: int_coord
!!$  integer  :: int_points

  ! Zero out the level set source
  srcLVLSET = 0.0_WP
  if (trim(lvlset_src).eq.'none') return
  
  ! Compute the interface normals and the curvature
  call levelset_normals
  call levelset_curvature
  
  ! Evaluate the unburned density, burning velocity, flame thickness
  call levelset_flame_parameters

  ! Choose which burning speed model to use
  select case (trim(lvlset_src))
     
  case ('laminar')
     
     do k = kmin_,kmax_
        do j = jmin_,jmax_
           do i = imin_,imax_
              if (LVLSET_band(i,j,k).eq.0) cycle
              if (mask(i,j).ne.0) cycle
              
              RHOmid = 0.5_WP*(RHO(i,j,k)+RHOold(i,j,k))
              srcLVLSET(i,j,k) = RHO_u(i,j,k) / RHOmid * SL(i,j,k)
           end do
        end do
     end do
     
  case ('turbulent')
     
     do k = kmin_,kmax_
        do j = jmin_,jmax_
           do i = imin_,imax_
              if (LVLSET_band(i,j,k).eq.0) cycle
              if (mask(i,j).ne.0) cycle
              
              uprime = (VISC(i,j,k)-VISCmol(i,j,k)) / delta_3D(i,j)
              
              Damk = SL(i,j,k) * delta_3D(i,j) / ( uprime * lF(i,j,k))
              ST = SL(i,j,k) + uprime * &
                   (-beta*Damk + sqrt( (beta*Damk)**2.0_WP + alpha*Damk))
              
              RHOmid = 0.5_WP*(RHO(i,j,k)+RHOold(i,j,k))
              srcLVLSET(i,j,k) = RHO_u(i,j,k) / RHOmid * max(ST,SL(i,j,k))! &
              !+ DIFF(i,j,k,1)/RHO(i,j,k)*lvlset_kappa(i,j,k)
           end do
        end do
     end do
     
!!$  case ('dynamic')
!!$     if (niter.eq.1) call dynamic_ST_model
!!$     do k = kmin_,kmax_
!!$        do j = jmin_,jmax_
!!$           do i = imin_,imax_
!!$              if (LVLSET_band(i,j,k).eq.0) cycle
!!$              if (mask(i,j).ne.0) cycle
!!$              
!!$              srcLVLSET(i,j,k) = RHO_u(i,j,k) / RHO(i,j,k) * SL(i,j,k) * C_st(i,j,k) &
!!$                   + DIFF(i,j,k,1)/RHO(i,j,k)*lvlset_kappa(i,j,k)
!!$           end do
!!$        end do
!!$     end do
     
!!$    case ('turbulent Lewis number dependent')
!!$       ! get the Markstein length
!!$       !call pressure_burning_velocity(SL, lF)
!!$       call parallel_max(lF,lF_phi)
!!$       int_points   = 128
!!$       left_bound   = 0.0_WP
!!$       right_bound  = Ma_delta
!!$       Ma_delta_h   = (right_bound-left_bound)/real(int_points,WP)
!!$       Ma_delta_sum = 0.0_WP
!!$       ! integrate to get the length
!!$       do i = 1,int_points
!!$          int_coord = left_bound + Ma_delta_h*(real(i-1,WP)+0.5_WP)
!!$          function_value = quadf(x)
!!$          Ma_delta_sum = Ma_delta_sum + function_value                 
!!$       end do
!!$       Ma_delta_integral = Ma_delta_h*Ma_delta_sum
!!$
!!$       Le_num=0.788_WP * (global_phi) + 0.212_WP
!!$
!!$       !Markstein number
!!$       Ma_num = (1.0_WP + Ma_delta)/(Ma_delta) * (log(1.0_WP + Ma_delta)) + &
!!$                 Ze_num*(Le_num  - 1.0_WP) / &
!!$                 max((2.0_WP*Le_num)*(Ma_delta_integral),1.0e-9_WP)
!!$                 
!!$       Ma_length = Ma_num * lF_phi                
!!$          
!!$       alpha = 4.0000_WP
!!$       beta  = 0.1950_WP
!!$
!!$       !call pressure_burning_velocity(SL, lF)
!!$       if (icyl.eq.1) then
!!$   
!!$          do k = kmin_,kmax_
!!$             do j = jmin_,jmax_
!!$                do i = imin_,imax_
!!$                   if (LVLSET_band(i,j,k).eq.0) cycle
!!$                   if (mask(i,j).ne.0) cycle
!!$                   if (SL.eq.0.0_WP) cycle
!!$             
!!$                   delta = min(min(dx(i),dy(j)),ym(j)*dz)
!!$                   Damk = SL * delta / (max(VISC(i,j,k)/delta,1.0e-9_WP) * lF)
!!$                   ST_over_SL = 1.0_WP + VISC(i,j,k)/delta/SL * &
!!$                      (-beta*Damk + sqrt( (beta*Damk)**2.0_WP + beta*alpha*Damk))
!!$             
!!$                   srcLVLSET(i,j,k) = SL * ST_over_SL * rho_unburned / RHO(i,j,k) + &
!!$                           SL * lF * lvlset_kappa(i,j,k) + &
!!$                          -SL * Ma_length * lvlset_kappa(i,j,k)
!!$
!!$                end do
!!$             end do
!!$          end do
!!$
!!$       else
!!$          
!!$          do k = kmin_,kmax_
!!$             do j = jmin_,jmax_
!!$                do i = imin_,imax_
!!$                   if (LVLSET_band(i,j,k).eq.0) cycle
!!$                   if (mask(i,j).ne.0) cycle
!!$                   if (SL.eq.0.0_WP) cycle
!!$             
!!$                   delta = min(min(dx(i),dy(j)),dz)
!!$                   Damk = SL * delta / (max(VISC(i,j,k)/delta,1.0e-9_WP) * lF)
!!$                   ST_over_SL = 1.0_WP + VISC(i,j,k)/delta/SL * &
!!$                      (-beta*Damk + sqrt( (beta*Damk)**2.0_WP + beta*alpha*Damk))
!!$             
!!$                   srcLVLSET(i,j,k) = SL * ST_over_SL * rho_unburned / RHO(i,j,k) + &
!!$                           SL * lF * lvlset_kappa(i,j,k) + &
!!$                          -SL * Ma_length * lvlset_kappa(i,j,k)
!!$
!!$                end do
!!$             end do
!!$          end do
!!$
!!$       end if
!!$    
  end select
  
  
  ! Introduce a smooth decay from SL to 0 away from the flame front
  ! Cubic function
!!$  cutoff_inner = 0.35_WP * LVLSET_cutoff
!!$  cutoff_outer = 0.65_WP * LVLSET_cutoff
  do k = kmin_,kmax_
     do j = jmin_,jmax_
        do i = imin_,imax_
           ! Only for points in the bands
           if (LVLSET_band(i,j,k).eq.0) cycle
           
           ! Zero source terms beyond cutoff
           abs_G = abs(LVLSET(i,j,k))
           if (abs_G.gt.0.99_WP*LVLSET_cutoff) srcLVLSET(i,j,k) = 0.0_WP
           
           ! Smooth decay
!!$           if (abs_G.gt.cutoff_inner) then
!!$              if (abs_G.le.cutoff_outer) then
!!$                 srcLVLSET(i,j,k) = srcLVLSET(i,j,k)*(abs_G-cutoff_outer)**2.0_WP * &
!!$                      (2.0_WP*abs_G+cutoff_outer-3.0_WP*cutoff_inner) / &
!!$                      (cutoff_outer-cutoff_inner)**3.0_WP
!!$              else
!!$                 srcLVLSET(i,j,k) = 0.0_WP
!!$              end if
!!$           end if
           
        end do
     end do
  end do
  
  return
end subroutine levelset_source_compute


! ================================= !
! Compute the flame surface normals !
! ================================= !
subroutine levelset_normals
  use levelset_marker
  use levelset_source
  use metric_generic
  use data
  implicit none
  integer  :: i,j,k
  real(WP) :: abs_G, cutoff_inner, cutoff_outer
  real(WP) :: lvlset_mag
  
  cutoff_inner = 0.25_WP * LVLSET_cutoff
  cutoff_outer = 0.55_WP * LVLSET_cutoff
  
  ! Use 2nd order central differencing to compute the normals
  lvlset_normal_x = 0.0_WP
  lvlset_normal_y = 0.0_WP
  lvlset_normal_z = 0.0_WP
  
  do k = kmin_,kmax_
     do j = jmin_,jmax_
        do i = imin_,imax_
           if (LVLSET_band(i,j,k).eq.0) cycle
           
           lvlset_normal_x(i,j,k) = sum(grad_xm(i,j,:)*LVLSET(i-1:i+1,j,k))      
           lvlset_normal_y(i,j,k) = sum(grad_ym(i,j,:)*LVLSET(i,j-1:j+1,k))
           lvlset_normal_z(i,j,k) = sum(grad_zm(i,j,:)*LVLSET(i,j,k-1:k+1))
           lvlset_mag = sqrt( &
                lvlset_normal_x(i,j,k)**2 + &
                lvlset_normal_y(i,j,k)**2 + &
                lvlset_normal_z(i,j,k)**2 )
           
           lvlset_normal_x(i,j,k) = lvlset_normal_x(i,j,k) / (lvlset_mag+epsilon(1.0_WP))
           lvlset_normal_y(i,j,k) = lvlset_normal_y(i,j,k) / (lvlset_mag+epsilon(1.0_WP))
           lvlset_normal_z(i,j,k) = lvlset_normal_z(i,j,k) / (lvlset_mag+epsilon(1.0_WP))
           
           abs_G = abs(LVLSET(i,j,k))
           if (abs_G.gt.cutoff_inner) then
              if (abs_G.le.cutoff_outer) then
                 lvlset_normal_x(i,j,k) = lvlset_normal_x(i,j,k) * &
                      (abs_G-cutoff_outer)**2.0_WP * &
                      (2.0_WP*abs_G+cutoff_outer-3.0_WP*cutoff_inner) / &
                      (cutoff_outer-cutoff_inner)**3.0_WP
                 lvlset_normal_y(i,j,k) = lvlset_normal_y(i,j,k) * &
                      (abs_G-cutoff_outer)**2.0_WP * &
                      (2.0_WP*abs_G+cutoff_outer-3.0_WP*cutoff_inner) / &
                      (cutoff_outer-cutoff_inner)**3.0_WP
                 lvlset_normal_z(i,j,k) = lvlset_normal_z(i,j,k) * &
                      (abs_G-cutoff_outer)**2.0_WP*&
                      (2.0_WP*abs_G+cutoff_outer-3.0_WP*cutoff_inner) / &
                      (cutoff_outer-cutoff_inner)**3.0_WP
              else 
                 lvlset_normal_x(i,j,k) = 0.0_WP
                 lvlset_normal_y(i,j,k) = 0.0_WP
                 lvlset_normal_z(i,j,k) = 0.0_WP
              end if
           end if
              
        end do
     end do
  end do
  
  ! Apply boundary conditions
  call levelset_apply_bc(lvlset_normal_x,'+','ym')
  call levelset_apply_bc(lvlset_normal_y,'-','ym')
  call levelset_apply_bc(lvlset_normal_z,'-','ym')
  
  return
end subroutine levelset_normals


! ================================= !
! Compute the flame front curvature !
! With Least Square Method          !
! ================================= !
subroutine levelset_curvature
  use levelset_marker
  use levelset_source
  use data
  use math
  use memory
  implicit none
  integer  :: i,j,k
  integer  :: ii,jj,kk
  real(WP) :: mindx
  
  real(WP), dimension(10,27) :: LS_A
  real(WP), dimension(10,10) :: A
  real(WP), dimension(10)    :: sol,b
  real(WP), dimension(27)    :: b1
  integer :: count
  real(WP) :: xloc, yloc, zloc
  
  lvlset_kappa = 0.0_WP
  
  ! Create a smooth heaviside function
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           tmp1(i,j,k) = heaviside_st(LVLSET(i,j,k))
        end do
     end do
  end do

  do k = kmin_,kmax_
     do j = jmin_,jmax_
        do i = imin_,imax_

           if (LVLSET_band(i,j,k).eq.0) cycle
           if (abs(LVLSET_band(i,j,k)).gt.3) cycle
           count = 0
           
           if (icyl.eq.1) then
              xloc = xm(i)
              yloc = ym(j)*cos(zm(k))
              zloc = ym(j)*sin(zm(k))
              do kk = k-1,k+1
                 do jj = j-1,j+1
                    do ii = i-1,i+1
                       ! Compute operator; convert to Cartesian
                       count = count + 1
                       ! G
                       LS_A(1,count)  = 1.0_WP
                       ! dG/dx
                       LS_A(2,count)  = xm(ii)-xloc
                       ! dG/dy
                       LS_A(3,count)  = ym(jj)*cos(zm(kk))-yloc
                       ! dG/dz
                       LS_A(4,count)  = ym(jj)*sin(zm(kk))-zloc
                       ! d2G/dx2
                       LS_A(5,count)  = 0.5_WP*(xm(ii)-xloc)**2
                       ! d2G/dy2
                       LS_A(6,count)  = 0.5_WP*(ym(jj)*cos(zm(kk))-yloc)**2
                       ! d2G/dz2
                       LS_A(7,count)  = 0.5_WP*(ym(jj)*sin(zm(kk))-zloc)**2
                       ! d2G/dxdy
                       LS_A(8,count)  = (xm(ii)-xloc)*(ym(jj)*cos(zm(kk))-yloc)
                       ! d2G/dxdz
                       LS_A(9,count)  = (xm(ii)-xloc)*(ym(jj)*sin(zm(kk))-zloc)
                       ! d2G/dydz
                       LS_A(10,count) = (ym(jj)*cos(zm(kk))-yloc) &
                                       *(ym(jj)*sin(zm(kk))-zloc)
                    end do
                 end do
              end do
           else
              do kk = k-1,k+1
                 do jj = j-1,j+1
                    do ii = i-1,i+1
                       ! Compute operator
                       count = count + 1
                       ! G
                       LS_A(1,count)  = 1.0_WP
                       ! dG/dx
                       LS_A(2,count)  = xm(ii)-xm(i)
                       ! dG/dy
                       LS_A(3,count)  = ym(jj)-ym(j)
                       ! dG/dz
                       LS_A(4,count)  = zm(kk)-zm(k)
                       ! d2G/dx2
                       LS_A(5,count)  = 0.5_WP*(xm(ii)-xm(i))**2
                       ! d2G/dy2
                       LS_A(6,count)  = 0.5_WP*(ym(jj)-ym(j))**2
                       ! d2G/dz2
                       LS_A(7,count)  = 0.5_WP*(zm(kk)-zm(k))**2
                       ! d2G/dxdy
                       LS_A(8,count)  = (xm(ii)-xm(i))*(ym(jj)-ym(j))
                       ! d2G/dxdz
                       LS_A(9,count)  = (xm(ii)-xm(i))*(zm(kk)-zm(k))
                       ! d2G/dydz
                       LS_A(10,count) = (ym(jj)-ym(j))*(zm(kk)-zm(k))
                    end do
                 end do
              end do
           end if
           
           ! Prepare the Least-square matrix
           do jj = 1,10
              do ii = 1,10
                 A(ii,jj) = sum(LS_A(ii,:)*LS_A(jj,:))
              end do
           end do

           ! Form local RHS
           count = 0
           do kk = k-1,k+1
              do jj = j-1,j+1
                 do ii = i-1,i+1
                    count = count + 1
                    b1(count) = tmp1(ii,jj,kk)
                 end do
              end do
           end do

           ! Form final RHS
           do ii = 1,10
              b(ii) = sum(LS_A(ii,:)*b1(:))
           end do

           ! Solve the system - Ax=b
           call solve_linear_system(A,b,sol,10)
           
           lvlset_kappa(i,j,k) = &
               ( + sol(2)**2*sol(6) + sol(2)**2*sol(7)  &
                 + sol(3)**2*sol(7) + sol(3)**2*sol(5)  &
                 + sol(4)**2*sol(5) + sol(4)**2*sol(6)  &
                 - 2.0_WP*sol(2)*sol(3)*sol(8)          &
                 - 2.0_WP*sol(2)*sol(4)*sol(9)          &
                 - 2.0_WP*sol(3)*sol(4)*sol(10) )       &
                 / ((sol(2)**2+sol(3)**2+sol(4)**2)**(1.5_WP)+epsilon(1.0_WP))
           
           ! Clip
           if (icyl.eq.0) then
              mindx = 0.50_WP/max(dx(i),dy(j),dz)
           else
              mindx = 0.50_WP/max(dx(i),dy(j),dz*ym(j))
           end if
           lvlset_kappa(i,j,k) = max(min(lvlset_kappa(i,j,k),mindx),-mindx)

        end do
     end do
  end do
  
  ! Apply boundary conditions
  call levelset_apply_bc(lvlset_kappa,'+','ym')

  return
end subroutine levelset_curvature

!===============================================================================
!===============================================================================

!!$subroutine levelset_filtered_geometry
!!$  use levelset_marker
!!$  use levelset_source
!!$  use metric_generic
!!$  use data
!!$  use math
!!$  implicit none
!!$  integer  :: i,j,k
!!$  integer  :: ii,jj,kk
!!$  real(WP) :: mindx, mag
!!$
!!$  real(WP), dimension(10,27) :: LS_A
!!$  real(WP), dimension(10,10) :: A
!!$  real(WP), dimension(10)    :: sol,b
!!$  real(WP), dimension(27)    :: b1
!!$  integer :: count,n
!!$  real(WP) :: xloc, yloc, zloc
!!$  
!!$  lvlset_kappa_f = 0.0_WP
!!$  lvlset_normal_f = 0.0_WP
!!$
!!$  do k=kmin_,kmax_
!!$     do j=jmin_,jmax_
!!$        do i=imin_,imax_
!!$
!!$           if (LVLSET_band(i,j,k).eq.0) cycle
!!$           if (abs(LVLSET_band(i,j,k)).gt.3) cycle
!!$           count = 0
!!$           
!!$           if (icyl.eq.1) then
!!$              xloc = xm(i)
!!$              yloc = ym(j)*cos(zm(k))
!!$              zloc = ym(j)*sin(zm(k))
!!$              do kk = k-1,k+1
!!$                 do jj = j-1,j+1
!!$                    do ii = i-1,i+1
!!$                       ! Compute operator; convert to Cartesian
!!$                       count = count + 1
!!$                       ! G
!!$                       LS_A(1,count)  = 1.0_WP
!!$                       ! dG/dx
!!$                       LS_A(2,count)  = xm(ii)-xloc
!!$                       ! dG/dy
!!$                       LS_A(3,count)  = ym(jj)*cos(zm(kk))-yloc
!!$                       ! dG/dz
!!$                       LS_A(4,count)  = ym(jj)*sin(zm(kk))-zloc
!!$                       ! d2G/dx2
!!$                       LS_A(5,count)  = 0.5_WP*(xm(ii)-xloc)**2
!!$                       ! d2G/dy2
!!$                       LS_A(6,count)  = 0.5_WP*(ym(jj)*cos(zm(kk))-yloc)**2
!!$                       ! d2G/dz2
!!$                       LS_A(7,count)  = 0.5_WP*(ym(jj)*sin(zm(kk))-zloc)**2
!!$                       ! d2G/dxdy
!!$                       LS_A(8,count)  = (xm(ii)-xloc)*(ym(jj)*cos(zm(kk))-yloc)
!!$                       ! d2G/dxdz
!!$                       LS_A(9,count)  = (xm(ii)-xloc)*(ym(jj)*sin(zm(kk))-zloc)
!!$                       ! d2G/dydz
!!$                       LS_A(10,count) = (ym(jj)*cos(zm(kk))-yloc) &
!!$                                       *(ym(jj)*sin(zm(kk))-zloc)
!!$                    end do
!!$                 end do
!!$              end do
!!$           else
!!$              do kk = k-1,k+1
!!$                 do jj = j-1,j+1
!!$                    do ii = i-1,i+1
!!$                       ! Compute operator
!!$                       count = count + 1
!!$                       ! G
!!$                       LS_A(1,count)  = 1.0_WP
!!$                       ! dG/dx
!!$                       LS_A(2,count)  = xm(ii)-xm(i)
!!$                       ! dG/dy
!!$                       LS_A(3,count)  = ym(jj)-ym(j)
!!$                       ! dG/dz
!!$                       LS_A(4,count)  = zm(kk)-zm(k)
!!$                       ! d2G/dx2
!!$                       LS_A(5,count)  = 0.5_WP*(xm(ii)-xm(i))**2
!!$                       ! d2G/dy2
!!$                       LS_A(6,count)  = 0.5_WP*(ym(jj)-ym(j))**2
!!$                       ! d2G/dz2
!!$                       LS_A(7,count)  = 0.5_WP*(zm(kk)-zm(k))**2
!!$                       ! d2G/dxdy
!!$                       LS_A(8,count)  = (xm(ii)-xm(i))*(ym(jj)-ym(j))
!!$                       ! d2G/dxdz
!!$                       LS_A(9,count)  = (xm(ii)-xm(i))*(zm(kk)-zm(k))
!!$                       ! d2G/dydz
!!$                       LS_A(10,count) = (ym(jj)-ym(j))*(zm(kk)-zm(k))
!!$                    end do
!!$                 end do
!!$              end do
!!$           end if
!!$
!!$           do jj = 1,10
!!$              do ii = 1,10
!!$                 A(ii,jj) = sum(LS_A(ii,:)*LS_A(jj,:))
!!$              end do
!!$           end do
!!$
!!$           ! Form local RHS
!!$           count = 0
!!$           do kk = k-1,k+1
!!$              do jj = j-1,j+1
!!$                 do ii = i-1,i+1
!!$                    count = count + 1
!!$                    b1(count) = FG_st(ii,jj,kk)
!!$                 end do
!!$              end do
!!$           end do
!!$
!!$           ! Form final RHS
!!$           do ii = 1,10
!!$              b(ii) = sum(LS_A(ii,:)*b1(:))
!!$           end do
!!$
!!$           ! Solve the system - Ax=b
!!$           n=10
!!$           call solve_linear_system(A,b,sol,n)
!!$
!!$           ! Get the curvature
!!$           lvlset_kappa_f(i,j,k) = &
!!$               ( + sol(2)**2*sol(6) + sol(2)**2*sol(7)  &
!!$                 + sol(3)**2*sol(7) + sol(3)**2*sol(5)  &
!!$                 + sol(4)**2*sol(5) + sol(4)**2*sol(6)  &
!!$                 - 2.0_WP*sol(2)*sol(3)*sol(8)          &
!!$                 - 2.0_WP*sol(2)*sol(4)*sol(9)          &
!!$                 - 2.0_WP*sol(3)*sol(4)*sol(10) )       &
!!$                 / ((sol(2)**2+sol(3)**2+sol(4)**2)**(1.5_WP)+epsilon(1.0_WP))
!!$
!!$           ! Get the normals
!!$           mag = max(sqrt(sol(2)**2+sol(3)**2+sol(4)**2),1.0E-9_WP)
!!$           if (icyl.eq.1) then
!!$              lvlset_normal_f(i,j,k,1) = sol(2)/mag
!!$              lvlset_normal_f(i,j,k,2) = ( sol(3)*cos(zm(kk))+sol(4)*sin(zm(kk)))/mag
!!$              lvlset_normal_f(i,j,k,3) = (-sol(3)*sin(zm(kk))+sol(4)*cos(zm(kk)))/mag
!!$           else
!!$              lvlset_normal_f(i,j,k,1) = sol(2)/mag
!!$              lvlset_normal_f(i,j,k,2) = sol(3)/mag
!!$              lvlset_normal_f(i,j,k,3) = sol(4)/mag
!!$           end if
!!$           
!!$           ! Clip
!!$           if (icyl.eq.0) then
!!$              mindx = 0.50_WP/max(dx(i),dy(j),dz)
!!$           else
!!$              mindx = 0.50_WP/max(dx(i),dy(j),dz*ym(j))
!!$           end if
!!$           lvlset_kappa_f(i,j,k) = max(min(lvlset_kappa_f(i,j,k),mindx),-mindx)
!!$
!!$           if (lvlset_kappa_f(i,j,k).gt.0.0_WP) then
!!$              lvlset_kappa_f(i,j,k)=min(lvlset_kappa_f(i,j,k),lvlset_kappa(i,j,k))
!!$           else
!!$              lvlset_kappa_f(i,j,k)=max(lvlset_kappa_f(i,j,k),lvlset_kappa(i,j,k))
!!$           end if
!!$
!!$        end do
!!$     end do
!!$  end do
!!$  call boundary_update_border_levelset(lvlset_kappa_f,'+','ym')
!!$  call boundary_levelset_neumann(lvlset_kappa_f,'-xm')
!!$  call boundary_levelset_neumann(lvlset_kappa_f,'+xm')
!!$  call boundary_levelset_neumann(lvlset_kappa_f,'-ym')
!!$  call boundary_levelset_neumann(lvlset_kappa_f,'+ym')
!!$  !call filter_global_3D(lvlset_kappa,lvlset_kappa_f,'+','n')
!!$
!!$  ! Clip at the centerline
!!$  !if (icyl.eq.1 .and. jproc.eq.1) lvlset_kappa_f(:,jmin:jmin_,:) = 0.0_WP
!!$  
!!$  call boundary_update_border_levelset(lvlset_normal_f(:,:,:,1),'+','ym')
!!$  call boundary_levelset_neumann(lvlset_normal_f(:,:,:,1),'-xm')
!!$  call boundary_levelset_neumann(lvlset_normal_f(:,:,:,1),'+xm')
!!$  call boundary_levelset_neumann(lvlset_normal_f(:,:,:,1),'-ym')
!!$  call boundary_levelset_neumann(lvlset_normal_f(:,:,:,1),'+ym')
!!$  
!!$  call boundary_update_border_levelset(lvlset_normal_f(:,:,:,2),'+','ym')
!!$  call boundary_levelset_neumann(lvlset_normal_f(:,:,:,2),'-xm')
!!$  call boundary_levelset_neumann(lvlset_normal_f(:,:,:,2),'+xm')
!!$  call boundary_levelset_neumann(lvlset_normal_f(:,:,:,2),'-ym')
!!$  call boundary_levelset_neumann(lvlset_normal_f(:,:,:,2),'+ym')
!!$  
!!$  call boundary_update_border_levelset(lvlset_normal_f(:,:,:,3),'+','ym')
!!$  call boundary_levelset_neumann(lvlset_normal_f(:,:,:,3),'-xm')
!!$  call boundary_levelset_neumann(lvlset_normal_f(:,:,:,3),'+xm')
!!$  call boundary_levelset_neumann(lvlset_normal_f(:,:,:,3),'-ym')
!!$  call boundary_levelset_neumann(lvlset_normal_f(:,:,:,3),'+ym')
!!$
!!$  return
!!$end subroutine levelset_filtered_geometry
!!$
!!$!===============================================================================
!!$!===============================================================================
!!$
!!$subroutine dynamic_ST_model
!!$  use levelset_source
!!$  use combustion
!!$  use levelset_marker
!!$  use filter
!!$  use data
!!$  use masks
!!$  use sgsmodel
!!$  use interpolate
!!$  use memory
!!$  implicit none
!!$  
!!$  real(WP), external :: SL_function
!!$  real(WP), external :: dSL_function
!!$
!!$  real(WP) :: diff_ratio
!!$      
!!$  integer  :: i,j,k,m
!!$  integer  :: counter
!!$  
!!$  real(WP) :: aa,bb,cc
!!$  real(WP) :: beta
!!$  real(WP) :: SL,lF
!!$  real(WP) :: dalpha, alpha
!!$  real(WP) :: rho_max, rho_min
!!$  integer  :: ilow, ihigh, jlow, jhigh
!!$
!!$  real(WP), dimension(-1:1,-1:1,-1:1) :: Af1,rhof1,Daf1,upf1
!!$    
!!$  ! Compute a heaviside function using G 
!!$  do k=kmin_,kmax_
!!$     do j=jmin_,jmax_
!!$        do i=imin_,imax_
!!$           G_st(i,j,k) = heaviside_st(LVLSET(i,j,k))
!!$        end do
!!$     end do
!!$  end do
!!$  call boundary_update_border(G_st,'+','ym')
!!$  call boundary_neumann(G_st,'-xm')
!!$  call boundary_neumann(G_st,'+xm')
!!$  call boundary_neumann(G_st,'-ym')
!!$  call boundary_neumann(G_st,'+ym')
!!$
!!$  ! Filter the heaviside function
!!$  call filter_global_3D(G_st,FG_st,'+','n')
!!$  call boundary_neumann(FG_st,'-xm')
!!$  call boundary_neumann(FG_st,'+xm')
!!$  call boundary_neumann(FG_st,'-ym')
!!$  call boundary_neumann(FG_st,'+ym')
!!$
!!$  ! Compute filtered curvature and normals
!!$  call levelset_filtered_geometry
!!$
!!$  ! Get the delta function describing the front
!!$  if (icyl.eq.1) then
!!$     call delta_compute_cyl
!!$  else
!!$     call delta_compute
!!$  end if
!!$
!!$  ! Get the local area per volume 
!!$  call area_per_volume
!!$  
!!$  ! Calculate the unfiltered and filtered kinetic energy
!!$  call interpolate_velocities
!!$  call filter_global_3D(Ui, FUi ,'+','d')
!!$  call filter_global_3D(Vi, FVi ,'-','d')
!!$  call filter_global_3D(Wi, FWi ,'-','d')
!!$  rho_max = maxval(RHO(:,:,:))
!!$  rho_min = minval(RHO(:,:,:))
!!$  call filter_global_3D(RHO,FRHO,'+','n')     
!!$  do k=kmin_,kmax_
!!$     do j=jmin_,jmax_
!!$        do i=imin_,imax_
!!$           FRHO(i,j,k) = max(min(FRHO(i,j,k),rho_max),rho_min)
!!$        end do
!!$     end do
!!$  end do
!!$  do k=kmin_,kmax_
!!$     do j=jmin_,jmax_
!!$        do i=imin_,imax_
!!$           G_st(i,j,k)  = 0.5_WP *  RHO(i,j,k) * ( Ui(i,j,k)**2.0_WP + &
!!$                                                   Vi(i,j,k)**2.0_WP + &
!!$                                                   Wi(i,j,k)**2.0_WP ) 
!!$           FG_st(i,j,k) = 0.5_WP * FRHO(i,j,k) * (FUi(i,j,k)**2.0_WP + &
!!$                                                  FVi(i,j,k)**2.0_WP + &
!!$                                                  FWi(i,j,k)**2.0_WP ) 
!!$        end do
!!$     end do
!!$  end do
!!$  call boundary_update_border(G_st ,'+','ym')
!!$  call boundary_update_border(FG_st,'+','ym')
!!$  
!!$  ! Compute uprime
!!$  do k=kmin_,kmax_
!!$     do j=jmin_,jmax_
!!$        do i=imin_,imax_
!!$           aa = sqrt(0.666_WP * G_st(i,j,k)  / RHO(i,j,k) )
!!$           bb = sqrt(0.666_WP * FG_st(i,j,k) / FRHO(i,j,k))
!!$           uprime_f(i,j,k)  = max(VISC(i,j,k)-VISCmol(i,j,k),0.0_WP) &
!!$                             / (0.2_WP*delta_3D(i,j)*RHO(i,j,k))
!!$           uprime_ff(i,j,k) = sqrt(uprime_f(i,j,k)**2 &
!!$                                   + 0.666_WP*max(aa-bb,0.0_WP))
!!$        end do
!!$     end do
!!$  end do
!!$  call boundary_update_border(uprime_f ,'+','ym')
!!$  call boundary_update_border(uprime_ff,'+','ym')
!!$  call filter_global_3D(uprime_f,FUi,'+','n')
!!$  uprime_f = FUi
!!$  call filter_global_3D(uprime_ff,FUi,'+','n')
!!$  uprime_ff = FUi
!!$
!!$  ! Average uprime in homogeneous direction
!!$  do i=imin_,imax_
!!$     do j=jmin_,jmax_
!!$        if (mask(i,j).ne.0) then
!!$           uprime_f(i,j,:) = 0.0_WP
!!$           uprime_ff(i,j,:) = 0.0_WP
!!$        else 
!!$           aa = sum(uprime_f(i,j,kmin_:kmax_))
!!$           call parallel_sum_dir(aa,bb,'z')
!!$           uprime_f(i,j,:) = bb / real(nz,WP)
!!$     
!!$           aa = sum(uprime_ff(i,j,kmin_:kmax_))
!!$           call parallel_sum_dir(aa,bb,'z')
!!$           uprime_ff(i,j,:) = bb / real(nz,WP)
!!$        end if
!!$     end do
!!$  end do
!!$  call boundary_update_border(uprime_f ,'+','ym')
!!$  call boundary_update_border(uprime_ff,'+','ym')
!!$  
!!$  ! Compute density ratios
!!$  do k=kmin_,kmax_
!!$     do j=jmin_,jmax_
!!$        do i=imin_,imax_
!!$           rho_rat_f(i,j,k)  = 1.0_WP / RHO(i,j,k)
!!$           rho_rat_ff(i,j,k) = 1.0_WP / FRHO(i,j,k)
!!$        end do
!!$     end do
!!$  end do
!!$  call boundary_update_border(rho_rat_f ,'+','ym')
!!$  call boundary_update_border(rho_rat_ff,'+','ym')
!!$  
!!$  ! Average density in homogeneous direction
!!$  do i=imin_,imax_
!!$     do j=jmin_,jmax_
!!$        aa = sum(rho_rat_f(i,j,kmin_:kmax_))
!!$        call parallel_sum_dir(aa,bb,'z')
!!$        rho_rat_f(i,j,:) = bb / real(nz,WP)
!!$     
!!$        aa = sum(rho_rat_ff(i,j,kmin_:kmax_))
!!$        call parallel_sum_dir(aa,bb,'z')
!!$        rho_rat_ff(i,j,:) = bb / real(nz,WP)
!!$     end do
!!$  end do
!!$  call boundary_update_border(rho_rat_f ,'+','ym')
!!$  call boundary_update_border(rho_rat_ff,'+','ym')
!!$
!!$  ! Compute Damkohler numbers
!!$  do k=kmin_,kmax_
!!$     do j=jmin_,jmax_
!!$        do i=imin_,imax_
!!$           !call laminar_burning_velocity(SC(i,j,k,isc_ZMIX), SL, lF)
!!$           Da_f(i,j,k)  = (SL       *delta_3D(i,j)) / &
!!$                          max(uprime_f(i,j,k)*lF,1.0E-10)
!!$           Da_f(i,j,k)  = min(Da_f(i,j,k),1.0E2_WP)
!!$           Da_f(i,j,k)  = max(Da_f(i,j,k),1.0E-2_WP)
!!$           Da_ff(i,j,k) = (SL*1.5_WP*delta_3D(i,j)) / &
!!$                          max(uprime_ff(i,j,k)*lF,1.0E-10)
!!$           Da_ff(i,j,k) = min(Da_ff(i,j,k),1.0E2_WP)
!!$           Da_ff(i,j,k) = max(Da_ff(i,j,k),1.0E-2_WP)
!!$
!!$           ! The larger scale Da # should always be smaller
!!$           Da_ff(i,j,k) = min(Da_ff(i,j,k),Da_f(i,j,k))
!!$        end do
!!$     end do
!!$  end do
!!$  call boundary_update_border(Da_f ,'+','ym')
!!$  call boundary_update_border(Da_ff,'+','ym')
!!$  
!!$  ! Average DIFF in homogeneous direction
!!$  do i=imin_,imax_
!!$     do j=jmin_,jmax_
!!$        aa = sum(DIFF(i,j,kmin_:kmax_,1))
!!$        bb = sum(RHO(i,j,kmin_:kmax_))
!!$        call parallel_sum_dir(aa,cc,'z')
!!$        call parallel_sum_dir(bb,aa,'z')
!!$        cc = cc/real(nz,WP)
!!$        aa = aa/real(nz,WP)
!!$        DIFF_avg(i,j,:) = cc / aa
!!$     end do
!!$  end do
!!$  call boundary_update_border(DIFF_avg,'+','ym')
!!$
!!$  ! Compute the dynamic coefficient everywhere
!!$!  call filter_global_3D(Ui, FUi ,'+','d')
!!$!  call filter_global_3D(Vi, FVi ,'-','d')
!!$!  call filter_global_3D(Wi, FWi ,'-','d')
!!$  G_st  = 0.0_WP
!!$  FG_st = 0.0_WP
!!$  beta = 0.1950_WP
!!$  do k=kmin_,kmax_
!!$     do j=jmin_,jmax_
!!$        do i=imin_,imax_
!!$           if (LVLSET_band(i,j,k).eq.0) cycle     
!!$           !call laminar_burning_velocity(SC(i,j,k,isc_ZMIX), SL, lF)
!!$           if (SL.eq.0.0_WP) cycle
!!$
!!$           aa = (DIFF_avg(i,j,k)*lvlset_kappa(i,j,k)) &
!!$               + rho_rat_f(i,j,k)*(SL-beta*uprime_f(i,j,k)*Da_f(i,j,k))
!!$           !aa = aa - Ui(i,j,k) * lvlset_normal(i,j,k,1) &
!!$           !        - Vi(i,j,k) * lvlset_normal(i,j,k,2) &
!!$           !        - Wi(i,j,k) * lvlset_normal(i,j,k,3)
!!$
!!$           G_st(i,j,k) = AperV_f(i,j,k) * aa
!!$
!!$           bb = (DIFF_avg(i,j,k) * lvlset_kappa_f(i,j,k)) &
!!$               + rho_rat_ff(i,j,k)*(SL-beta*uprime_ff(i,j,k)*Da_ff(i,j,k))
!!$           !bb = bb + FUi(i,j,k) * lvlset_normal_f(i,j,k,1) &
!!$           !        + FVi(i,j,k) * lvlset_normal_f(i,j,k,2) &
!!$           !        + FWi(i,j,k) * lvlset_normal_f(i,j,k,3)
!!$
!!$           FG_st(i,j,k) = AperV_ff(i,j,k) * bb
!!$
!!$        end do
!!$     end do
!!$  end do
!!$  call boundary_update_border( G_st,'+','ym')
!!$  call boundary_update_border(FG_st,'+','ym')
!!$
!!$  ! Test filter the non-coefficient dependent terms
!!$  call filter_global_3D(G_st, FUi ,'+','n')
!!$
!!$  ! Form the rhs of the dynamic equation to solve
!!$  dyn_rhs = 0.0_WP
!!$  do k=kmin_,kmax_
!!$     do j=jmin_,jmax_
!!$        do i=imin_,imax_
!!$           if (LVLSET_band(i,j,k).eq.0) cycle     
!!$           dyn_rhs(i,j,k) = FUi(i,j,k) - FG_st(i,j,k)
!!$        end do
!!$     end do
!!$  end do
!!$
!!$  C_st = 1.0_WP
!!$  dyn_alpha = 0.0_WP
!!$  dyn_res   = 0.0_WP
!!$  ! Solve for the dynamic coefficient
!!$  do k=kmin_,kmax_
!!$     do j=jmin_,jmax_
!!$        do i=imin_,imax_
!!$           if (LVLSET_band(i,j,k).eq.0) cycle     
!!$           if (abs(LVLSET_band(i,j,k)).gt.2) cycle     
!!$           if (AperV_ff(i,j,k).eq.0.0_WP) cycle
!!$           if (AperV_f (i,j,k).eq.0.0_WP) cycle
!!$
!!$           ! Set up arrays to pass
!!$           upf1(-1:1,-1:1,-1:1)  =  uprime_f(i-1:i+1,j-1:j+1,k-1:k+1)
!!$           Daf1(-1:1,-1:1,-1:1)  =      Da_f(i-1:i+1,j-1:j+1,k-1:k+1)
!!$           rhof1(-1:1,-1:1,-1:1) = rho_rat_f(i-1:i+1,j-1:j+1,k-1:k+1)
!!$           Af1(-1:1,-1:1,-1:1)   =   AperV_f(i-1:i+1,j-1:j+1,k-1:k+1)
!!$
!!$           counter = 0 
!!$           dyn_alpha(i,j,k) = 0.0_WP
!!$           dalpha = 1.0_WP
!!$           aa = SL_function(dyn_alpha(i,j,k),beta, &
!!$                            Af1,AperV_ff(i,j,k), &
!!$                            dyn_rhs(i,j,k), &
!!$                            Daf1,Da_ff(i,j,k), &
!!$                            upf1,uprime_ff(i,j,k), &
!!$                            rhof1,rho_rat_ff(i,j,k))
!!$           bb = dSL_function(dyn_alpha(i,j,k),beta, &
!!$                            Af1,AperV_ff(i,j,k), &
!!$                            Daf1,Da_ff(i,j,k), &
!!$                            upf1,uprime_ff(i,j,k), &
!!$                            rhof1,rho_rat_ff(i,j,k))
!!$
!!$           do while ((abs(dalpha).gt.1.0e-8_WP).and.(counter.lt.40))
!!$              if (abs(bb).ge.1.0e-10_WP) then
!!$                 dalpha = -aa/bb
!!$              else
!!$                 dalpha = 1.0E-9_WP
!!$              end if
!!$              dyn_alpha(i,j,k) = dyn_alpha(i,j,k) + dalpha
!!$              dyn_alpha(i,j,k) = max(dyn_alpha(i,j,k),1.0E-9_WP)
!!$              aa = SL_function(dyn_alpha(i,j,k),beta, &
!!$                               Af1,AperV_ff(i,j,k), &
!!$                               dyn_rhs(i,j,k), &
!!$                               Daf1,Da_ff(i,j,k), &
!!$                               upf1,uprime_ff(i,j,k), &
!!$                               rhof1,rho_rat_ff(i,j,k))
!!$              bb = dSL_function(dyn_alpha(i,j,k),beta, &
!!$                                Af1,AperV_ff(i,j,k), &
!!$                                Daf1,Da_ff(i,j,k), &
!!$                                upf1,uprime_ff(i,j,k), &
!!$                                rhof1,rho_rat_ff(i,j,k))
!!$              counter = counter + 1
!!$           end do
!!$           dyn_res(i,j,k) = aa
!!$
!!$        end do
!!$     end do
!!$  end do
!!$  call boundary_update_border(dyn_alpha,'+','ym')
!!$
!!$  ! Smooth the coefficient
!!$  call filter_global_3D(dyn_alpha, FUi ,'+','n')
!!$  dyn_alpha = FUi
!!$
!!$  ! Now compute ST / SL
!!$  do k=kmin_,kmax_
!!$     do j=jmin_,jmax_
!!$        do i=imin_,imax_
!!$           if (LVLSET_band(i,j,k).eq.0) cycle     
!!$           aa = max(dyn_alpha(i,j,k),0.0_WP)
!!$           aa = min(dyn_alpha(i,j,k),8.0_WP)
!!$           if (abs(LVLSET_band(i,j,k)).gt.6) aa = 0.0_WP
!!$           !call laminar_burning_velocity(SC(i,j,k,isc_ZMIX), SL, lF)
!!$           C_st(i,j,k) = 1.0_WP + uprime_f(i,j,k)/SL * (-beta*Da_f(i,j,k) &
!!$            + sqrt((beta*Da_f(i,j,k))**2+beta*aa*Da_f(i,j,k)))
!!$        end do
!!$     end do
!!$  end do
!!$   
!!$  write(*,*) 'maxval C_st = ',maxval(C_st)
!!$  write(*,*) 'minval C_st = ',minval(C_st)
!!$  
!!$  100 format(A12,ES12.3,A16,ES12.3,A16,ES12.3)
!!$  200 format(A12,ES12.3,A12,ES12.3)
!!$  300 format(A12,ES12.3)
!!$ 
!!$  return
!!$end subroutine dynamic_ST_model
!!$
!!$!===============================================================================
!!$!===============================================================================
!!$
!!$subroutine area_per_volume
!!$  use levelset_source
!!$  use levelset_marker
!!$  use masks
!!$  implicit none
!!$
!!$  integer  :: nf
!!$  integer  :: i,j,k
!!$  integer  :: ii,jj,kk
!!$  integer  :: count_1, count_2
!!$  integer  :: ilow,ihigh
!!$  integer  :: jlow,jhigh
!!$  integer  :: klow,khigh
!!$  real(WP) :: area_sum_1, area_sum_2, area_sum_3
!!$  real(WP) :: vol_sum_1, vol_sum_2
!!$
!!$  AperV_f  = 0.0_WP
!!$  AperV_ff = 0.0_WP
!!$  nf = 5
!!$  do k=kmin_,kmax_
!!$     do j=jmin_,jmax_
!!$        do i=imin_,imax_
!!$
!!$           if (LVLSET_band(i,j,k).eq.0) cycle    
!!$           if (abs(LVLSET_band(i,j,k)).gt.1) cycle   
!!$
!!$           AperV_f(i,j,k) = delta_f(i,j,k)*vol(i,j)
!!$           AperV_ff(i,j,k) = delta_ff(i,j,k)*vol(i,j) 
!!$
!!$           !area_sum_1 = 0.0_WP
!!$           !area_sum_2 = 0.0_WP
!!$           !vol_sum_1  = 1.0E-12_WP
!!$           !count_1 = 0
!!$           !count_2 = 0
!!$           !ilow = max(imino_,i-nf)
!!$           !jlow = max(jmino_,j-nf)
!!$           !klow = max(kmino_,k-nf)
!!$           !ihigh = min(imaxo_,i+nf)
!!$           !jhigh = min(jmaxo_,j+nf)
!!$           !khigh = min(kmaxo_,k+nf)
!!$           !do kk=klow,khigh
!!$           !   do jj=jlow,jhigh
!!$           !      do ii=ilow,ihigh
!!$           !         if (mask(ii,jj).ne.0) cycle
!!$           !         area_sum_1 = area_sum_1 +  delta_f(ii,jj,kk)*vol(ii,jj)
!!$           !         area_sum_2 = area_sum_2 + delta_ff(ii,jj,kk)*vol(ii,jj)
!!$           !         vol_sum_1 = vol_sum_1 + vol(ii,jj)
!!$           !      end do
!!$           !   end do
!!$           !end do
!!$           !
!!$           !AperV_f (i,j,k)  = max(area_sum_1,area_sum_2) / vol_sum_1
!!$           !AperV_ff(i,j,k)  = area_sum_2 / vol_sum_1
!!$
!!$        end do
!!$     end do
!!$  end do
!!$  call boundary_update_border(AperV_f ,'+','ym')
!!$  call boundary_update_border(AperV_ff,'+','ym')
!!$
!!$  write(*,*) 'Area 1 = ',sum(AperV_f(imin_:imax_,jmin_:jmax_,kmin_:kmax_))
!!$  write(*,*) 'Area 2 = ',sum(AperV_ff(imin_:imax_,jmin_:jmax_,kmin_:kmax_))
!!$       
!!$  !call levelset_extend_area
!!$  !do k=kmino_,kmaxo_
!!$  !   do j=jmino_,jmaxo_
!!$  !      do i=imino_,imaxo_
!!$  !         AperV_f(i,j,k) = max(AperV_f(i,j,k),AperV_ff(i,j,k))   
!!$  !      end do
!!$  !   end do
!!$  !end do
!!$
!!$  ! Average
!!$  do i=imin_,imax_
!!$     area_sum_1 = sum(AperV_f(i-nf:i+nf,jmin_:jmax_,kmin_:kmax_))
!!$     call parallel_sum_dir(area_sum_1,area_sum_2,'yz')
!!$     
!!$     area_sum_1 = sum(AperV_ff(i-nf:i+nf,jmin_:jmax_,kmin_:kmax_))
!!$     call parallel_sum_dir(area_sum_1,area_sum_3,'yz')
!!$     
!!$     area_sum_1 = nz*sum(vol(i-nf:i+nf,jmin_:jmax_))
!!$     call parallel_sum_dir(area_sum_1,vol_sum_1,'yz')
!!$     
!!$     delta_f(i,:,:) = max(area_sum_2,area_sum_3) / vol_sum_1
!!$     delta_ff(i,:,:) = area_sum_3 / vol_sum_1
!!$  end do
!!$  AperV_f  = delta_f
!!$  AperV_ff = delta_ff
!!$  call boundary_update_border(AperV_f ,'+','ym')
!!$  call boundary_update_border(AperV_ff,'+','ym')
!!$
!!$  return
!!$end subroutine area_per_volume
!!$
!!$!===============================================================================
!!$!===============================================================================
!!$
!!$function SL_function(alpha,beta,A1,A2,rhs,Da1,Da2,up1,up2,rho1,rho2) &
!!$         result(d)
!!$   use precision
!!$   implicit none
!!$   real(WP), intent(in) :: alpha,beta
!!$   real(WP), intent(in) :: A2, rhs, Da2, up2, rho2
!!$   real(WP), dimension(-1:1,-1:1,-1:1), intent(in) :: A1, Da1, up1, rho1
!!$   real(WP) :: d, d_tmp
!!$   real(WP) :: weight
!!$   real(WP) :: sqrt_arg
!!$   integer  :: case_tmp
!!$   integer  :: i,j,k
!!$   
!!$   sqrt_arg =(beta*Da2)**2+beta*alpha*Da2
!!$   d_tmp = -A2*rho2*up2*sqrt(sqrt_arg) + rhs
!!$
!!$   ! Include the test filtered terms - use a 1/6,2/3,1/6 filtering rule
!!$   do k=-1,1
!!$      do j=-1,1
!!$         do i=-1,1
!!$           case_tmp = abs(k) + abs(j) + abs(i)
!!$           if (case_tmp.eq.3) then
!!$              weight = 0.004629629_WP
!!$           elseif (case_tmp.eq.2) then
!!$              weight = 0.018518519_WP
!!$           elseif (case_tmp.eq.1) then
!!$              weight = 0.074074074_WP
!!$           else
!!$              weight = 0.296296296_WP
!!$           end if
!!$           sqrt_arg = (beta*Da1(i,j,k))**2+beta*alpha*Da1(i,j,k)
!!$           d_tmp = d_tmp + weight &
!!$                    *A1(i,j,k)*rho1(i,j,k)*up1(i,j,k)*sqrt(sqrt_arg)
!!$         end do
!!$      end do
!!$   end do
!!$
!!$   d = d_tmp
!!$
!!$end function SL_function
!!$
!!$!===============================================================================
!!$!===============================================================================
!!$
!!$function dSL_function(alpha,beta,A1,A2,Da1,Da2,up1,up2,rho1,rho2) &
!!$         result(d)
!!$   use precision
!!$   implicit none
!!$   real(WP), intent(in) :: alpha,beta
!!$   real(WP), intent(in) :: A2, Da2, up2, rho2
!!$   real(WP), dimension(-1:1,-1:1,-1:1), intent(in) :: A1, Da1, up1, rho1
!!$   real(WP) :: d, d_tmp
!!$   real(WP) :: weight
!!$   real(WP) :: sqrt_arg
!!$   integer  :: case_tmp
!!$   integer  :: i,j,k
!!$   
!!$   sqrt_arg = (beta*Da2)**2+beta*alpha*Da2
!!$   d_tmp = -0.5_WP*beta*Da2*A2*rho2*up2/sqrt(sqrt_arg)
!!$
!!$   ! Include the test filtered terms - use a 1/6,2/3,1/6 filtering rule
!!$   do k=-1,1
!!$      do j=-1,1
!!$         do i=-1,1
!!$           case_tmp = abs(k) + abs(j) + abs(i)
!!$           if (case_tmp.eq.3) then
!!$              weight = 0.004629629_WP
!!$           elseif (case_tmp.eq.2) then
!!$              weight = 0.018518519_WP
!!$           elseif (case_tmp.eq.1) then
!!$              weight = 0.074074074_WP
!!$           else
!!$              weight = 0.296296296_WP
!!$           end if
!!$           sqrt_arg = (beta*Da1(i,j,k))**2+beta*alpha*Da1(i,j,k)
!!$           d_tmp = d_tmp + weight*0.5_WP*beta*Da1(i,j,k) &
!!$                 *A1(i,j,k)*rho1(i,j,k)*up1(i,j,k)/sqrt(sqrt_arg)
!!$         end do
!!$      end do
!!$   end do
!!$
!!$   d = d_tmp
!!$
!!$end function dSL_function
!!$
!!$!===============================================================================
!!$!===============================================================================
!!$
!!$subroutine delta_compute
!!$   use levelset_source
!!$   use parallel
!!$   use data 
!!$   use metric_generic
!!$   implicit none
!!$   real(WP) :: dpx, dmx, dpy, dmy, dpz, dmz
!!$   real(WP) :: diff_x , diff_y , diff_z
!!$   real(WP) :: diff_xp, diff_yp, diff_zp
!!$   real(WP) :: diff_xm, diff_ym, diff_zm
!!$   real(WP) :: abs_lap
!!$   real(WP) :: eps
!!$   integer  :: i,j,k
!!$   
!!$   eps = 1.0e-9_WP
!!$   delta_f = 0.0_WP
!!$   delta_ff = 0.0_WP
!!$
!!$   ! Unfiltered delta function describing front
!!$   do k = kmin_,kmax_
!!$      do j = jmin_,jmax_
!!$         do i = imin_,imax_
!!$            diff_x  = sum(grad_xm(i,j,:)*G_st(i-1:i+1,j,k))
!!$            diff_y  = sum(grad_ym(i,j,:)*G_st(i,j-1:j+1,k))
!!$            diff_z  = sum(grad_zm(i,j,:)*G_st(i,j,k-1:k+1))
!!$            abs_lap = sqrt( diff_x**2 + diff_y**2 + diff_z**2 + eps)
!!$      
!!$            diff_xp = abs((G_st(i+1,j,k)-G_st(i,j,k))/(xm(i+1)-xm(i)))
!!$            diff_xm = abs((G_st(i,j,k)-G_st(i-1,j,k))/(xm(i)-xm(i-1)))
!!$      
!!$            diff_yp = abs((G_st(i,j+1,k)-G_st(i,j,k))/(ym(j+1)-ym(j)))
!!$            diff_ym = abs((G_st(i,j,k)-G_st(i,j-1,k))/(ym(j)-ym(j-1)))
!!$      
!!$            diff_zp = abs((G_st(i,j,k+1)-G_st(i,j,k))/(zm(k+1)-zm(k)))
!!$            diff_zm = abs((G_st(i,j,k)-G_st(i,j,k-1))/(zm(k)-zm(k-1)))
!!$
!!$            if (G_st(i+1,j,k)*G_st(i,j,k).le.0.0_WP) then
!!$               if (diff_xp.eq.0.0_WP) then 
!!$                  dpx = 0.0_WP 
!!$               else
!!$                  dpx = abs(G_st(i+1,j,k)*diff_x) / &
!!$                     ( (xm(i+1)-xm(i))**2 * diff_xp * abs_lap )
!!$               end if
!!$            else
!!$               dpx = 0.0_WP
!!$            end if
!!$
!!$            if (G_st(i-1,j,k)*G_st(i,j,k).lt.0.0_WP) then
!!$               if (diff_xm.eq.0.0_WP) then 
!!$                  dmx = 0.0_WP 
!!$               else
!!$                  dmx = abs(G_st(i-1,j,k)*diff_x) / &
!!$                     ( (xm(i)-xm(i-1))**2 * diff_xm * abs_lap )
!!$               end if
!!$            else
!!$               dmx = 0.0_WP
!!$            end if
!!$      
!!$            if (G_st(i,j+1,k)*G_st(i,j,k).le.0.0_WP) then
!!$               if (diff_yp.eq.0.0_WP) then 
!!$                  dpy = 0.0_WP 
!!$               else
!!$                  dpy = abs(G_st(i,j+1,k)*diff_y) / &
!!$                     ( (ym(j+1)-ym(j))**2 * diff_yp * abs_lap )
!!$               end if
!!$            else
!!$               dpy = 0.0_WP
!!$            end if
!!$
!!$            if (G_st(i,j-1,k)*G_st(i,j,k).lt.0.0_WP) then
!!$               if (diff_ym.eq.0.0_WP) then 
!!$                  dmy = 0.0_WP 
!!$               else
!!$                  dmy = abs(G_st(i,j-1,k)*diff_y) / &
!!$                     ( (ym(j)-ym(j-1))**2 * diff_ym * abs_lap )
!!$               end if
!!$            else
!!$               dmy = 0.0_WP
!!$            end if
!!$      
!!$            if (G_st(i,j,k+1)*G_st(i,j,k).le.0.0_WP) then
!!$               if (diff_zp.eq.0.0_WP) then 
!!$                  dpz = 0.0_WP 
!!$               else
!!$                  dpz = abs(G_st(i,j,k+1)*diff_z) / &
!!$                     ( (zm(k+1)-zm(k))**2 * diff_zp * abs_lap )
!!$               end if
!!$            else
!!$               dpz = 0.0_WP
!!$            end if
!!$
!!$            if (G_st(i,j,k-1)*G_st(i,j,k).lt.0.0_WP) then
!!$               if (diff_zm.eq.0.0_WP) then 
!!$                  dmz = 0.0_WP 
!!$               else
!!$                  dmz = abs(G_st(i,j,k-1)*diff_z) / &
!!$                     ( (zm(k)-zm(k-1))**2 * diff_zm * abs_lap )
!!$               end if
!!$            else
!!$               dmz = 0.0_WP
!!$            end if
!!$
!!$            delta_f(i,j,k) = dpx + dpy + dpz + dmx + dmy + dmz
!!$   
!!$         end do
!!$      end do
!!$   end do
!!$   call boundary_update_border(delta_f,'+','ym')
!!$   call boundary_neumann(delta_f,'-xm')
!!$   call boundary_neumann(delta_f,'+xm')
!!$   call boundary_neumann(delta_f,'-ym')
!!$   call boundary_neumann(delta_f,'+ym')
!!$
!!$   ! Filtered delta function describing front
!!$   do k = kmin_,kmax_
!!$      do j = jmin_,jmax_
!!$         do i = imin_,imax_
!!$            diff_x  = sum(grad_xm(i,j,:)*FG_st(i-1:i+1,j,k))
!!$            diff_y  = sum(grad_ym(i,j,:)*FG_st(i,j-1:j+1,k))
!!$            diff_z  = sum(grad_zm(i,j,:)*FG_st(i,j,k-1:k+1))
!!$            abs_lap = sqrt( diff_x**2 + diff_y**2 + diff_z**2 + eps)
!!$      
!!$            diff_xp = abs((FG_st(i+1,j,k)-FG_st(i,j,k))/(xm(i+1)-xm(i)))
!!$            diff_xm = abs((FG_st(i,j,k)-FG_st(i-1,j,k))/(xm(i)-xm(i-1)))
!!$      
!!$            diff_yp = abs((FG_st(i,j+1,k)-FG_st(i,j,k))/(ym(j+1)-ym(j)))
!!$            diff_ym = abs((FG_st(i,j,k)-FG_st(i,j-1,k))/(ym(j)-ym(j-1)))
!!$      
!!$            diff_zp = abs((FG_st(i,j,k+1)-FG_st(i,j,k))/(zm(k+1)-zm(k)))
!!$            diff_zm = abs((FG_st(i,j,k)-FG_st(i,j,k-1))/(zm(k)-zm(k-1)))
!!$
!!$            if (FG_st(i+1,j,k)*FG_st(i,j,k).le.0.0_WP) then
!!$               if (diff_xp.eq.0.0_WP) then 
!!$                  dpx = 0.0_WP 
!!$               else
!!$                  dpx = abs(FG_st(i+1,j,k)*diff_x) / &
!!$                     ( (xm(i+1)-xm(i))**2 * diff_xp * abs_lap )
!!$               end if
!!$            else
!!$               dpx = 0.0_WP
!!$            end if
!!$
!!$            if (FG_st(i-1,j,k)*FG_st(i,j,k).lt.0.0_WP) then
!!$               if (diff_xm.eq.0.0_WP) then 
!!$                  dmx = 0.0_WP 
!!$               else
!!$                  dmx = abs(FG_st(i-1,j,k)*diff_x) / &
!!$                     ( (xm(i)-xm(i-1))**2 * diff_xm * abs_lap )
!!$               end if
!!$            else
!!$               dmx = 0.0_WP
!!$            end if
!!$      
!!$            if (FG_st(i,j+1,k)*FG_st(i,j,k).le.0.0_WP) then
!!$               if (diff_yp.eq.0.0_WP) then 
!!$                  dpy = 0.0_WP 
!!$               else
!!$                  dpy = abs(FG_st(i,j+1,k)*diff_y) / &
!!$                     ( (ym(j+1)-ym(j))**2 * diff_yp * abs_lap )
!!$               end if
!!$            else
!!$               dpy = 0.0_WP
!!$            end if
!!$
!!$            if (FG_st(i,j-1,k)*FG_st(i,j,k).lt.0.0_WP) then
!!$               if (diff_ym.eq.0.0_WP) then 
!!$                  dmy = 0.0_WP 
!!$               else
!!$                  dmy = abs(FG_st(i,j-1,k)*diff_y) / &
!!$                     ( (ym(j)-ym(j-1))**2 * diff_ym * abs_lap )
!!$               end if
!!$            else
!!$               dmy = 0.0_WP
!!$            end if
!!$      
!!$            if (FG_st(i,j,k+1)*FG_st(i,j,k).le.0.0_WP) then
!!$               if (diff_zp.eq.0.0_WP) then 
!!$                  dpz = 0.0_WP 
!!$               else
!!$                  dpz = abs(FG_st(i,j,k+1)*diff_z) / &
!!$                     ( (zm(k+1)-zm(k))**2 * diff_zp * abs_lap )
!!$               end if
!!$            else
!!$               dpz = 0.0_WP
!!$            end if
!!$
!!$            if (FG_st(i,j,k-1)*FG_st(i,j,k).lt.0.0_WP) then
!!$               if (diff_zm.eq.0.0_WP) then 
!!$                  dmz = 0.0_WP 
!!$               else
!!$                  dmz = abs(FG_st(i,j,k-1)*diff_z) / &
!!$                     ( (zm(k)-zm(k-1))**2 * diff_zm * abs_lap )
!!$               end if
!!$            else
!!$               dmz = 0.0_WP
!!$            end if
!!$
!!$            delta_ff(i,j,k) = dpx + dpy + dpz + dmx + dmy + dmz
!!$   
!!$         end do
!!$      end do
!!$   end do
!!$   call boundary_update_border(delta_ff,'+','ym')
!!$   call boundary_neumann(delta_ff,'-xm')
!!$   call boundary_neumann(delta_ff,'+xm')
!!$   call boundary_neumann(delta_ff,'-ym')
!!$   call boundary_neumann(delta_ff,'+ym')
!!$
!!$   return
!!$end subroutine delta_compute
!!$
!!$!===============================================================================
!!$!===============================================================================
!!$
!!$subroutine delta_compute_cyl
!!$   use levelset_source
!!$   use parallel
!!$   use data 
!!$   use metric_generic
!!$   implicit none
!!$   real(WP) :: dpx, dmx, dpy, dmy, dpz, dmz
!!$   real(WP) :: diff_x , diff_y , diff_z
!!$   real(WP) :: diff_xp, diff_yp, diff_zp
!!$   real(WP) :: diff_xm, diff_ym, diff_zm
!!$   real(WP) :: abs_lap
!!$   real(WP) :: eps
!!$   integer  :: i,j,k
!!$   
!!$   eps = 1.0e-9_WP
!!$
!!$   ! Unfiltered delta function describing front
!!$   do k = kmin_,kmax_
!!$      do j = jmin_,jmax_
!!$         do i = imin_,imax_
!!$            diff_x  = sum(grad_xm(i,j,:)*G_st(i-1:i+1,j,k))
!!$            diff_y  = sum(grad_ym(i,j,:)*G_st(i,j-1:j+1,k))
!!$            diff_z  = sum(grad_zm(i,j,:)*G_st(i,j,k-1:k+1))
!!$            abs_lap = sqrt( diff_x**2 + diff_y**2 + diff_z**2 + eps)
!!$      
!!$            diff_xp = abs((G_st(i+1,j,k)-G_st(i,j,k))/(xm(i+1)-xm(i)))
!!$            diff_xm = abs((G_st(i,j,k)-G_st(i-1,j,k))/(xm(i)-xm(i-1)))
!!$      
!!$            diff_yp = abs((G_st(i,j+1,k)-G_st(i,j,k))/(ym(j+1)-ym(j)))
!!$            diff_ym = abs((G_st(i,j,k)-G_st(i,j-1,k))/(ym(j)-ym(j-1)))
!!$            
!!$            diff_zp = abs((G_st(i,j,k+1)-G_st(i,j,k))/(zm(k+1)-zm(k))*ymi(j))
!!$            diff_zm = abs((G_st(i,j,k)-G_st(i,j,k-1))/(zm(k)-zm(k-1))*ymi(j))
!!$
!!$            if (G_st(i+1,j,k)*G_st(i,j,k).le.0.0_WP) then
!!$               if (diff_xp.eq.0.0_WP) then 
!!$                  dpx = 0.0_WP 
!!$               else
!!$                  dpx = abs(G_st(i+1,j,k)*diff_x) / &
!!$                     ( (xm(i+1)-xm(i))**2 * diff_xp * abs_lap )
!!$               end if
!!$            else
!!$               dpx = 0.0_WP
!!$            end if
!!$
!!$            if (G_st(i-1,j,k)*G_st(i,j,k).lt.0.0_WP) then
!!$               if (diff_xm.eq.0.0_WP) then 
!!$                  dmx = 0.0_WP 
!!$               else
!!$                  dmx = abs(G_st(i-1,j,k)*diff_x) / &
!!$                     ( (xm(i)-xm(i-1))**2 * diff_xm * abs_lap )
!!$               end if
!!$            else
!!$               dmx = 0.0_WP
!!$            end if
!!$      
!!$            if (G_st(i,j+1,k)*G_st(i,j,k).le.0.0_WP) then
!!$               if (diff_yp.eq.0.0_WP) then 
!!$                  dpy = 0.0_WP 
!!$               else
!!$                  dpy = abs(G_st(i,j+1,k)*diff_y) / &
!!$                     ( (ym(j+1)-ym(j))**2 * diff_yp * abs_lap )
!!$               end if
!!$            else
!!$               dpy = 0.0_WP
!!$            end if
!!$
!!$            if (G_st(i,j-1,k)*G_st(i,j,k).lt.0.0_WP) then
!!$               if (diff_ym.eq.0.0_WP) then 
!!$                  dmy = 0.0_WP 
!!$               else
!!$                  dmy = abs(G_st(i,j-1,k)*diff_y) / &
!!$                     ( (ym(j)-ym(j-1))**2 * diff_ym * abs_lap )
!!$               end if
!!$            else
!!$               dmy = 0.0_WP
!!$            end if
!!$      
!!$            if (G_st(i,j,k+1)*G_st(i,j,k).le.0.0_WP) then
!!$               if (diff_zp.eq.0.0_WP) then 
!!$                  dpz = 0.0_WP 
!!$               else
!!$                  dpz = abs(G_st(i,j,k+1)*diff_z) / &
!!$                     ( ym(j)**2*(zm(k+1)-zm(k))**2 * diff_zp * abs_lap )
!!$               end if
!!$            else
!!$               dpz = 0.0_WP
!!$            end if
!!$
!!$            if (G_st(i,j,k-1)*G_st(i,j,k).lt.0.0_WP) then
!!$               if (diff_zm.eq.0.0_WP) then 
!!$                  dmz = 0.0_WP 
!!$               else
!!$                  dmz = abs(G_st(i,j,k-1)*diff_z) / &
!!$                     ( ym(j)**2*(zm(k)-zm(k-1))**2 * diff_zm * abs_lap )
!!$               end if
!!$            else
!!$               dmz = 0.0_WP
!!$            end if
!!$
!!$            delta_f(i,j,k) = dpx + dpy + dpz + dmx + dmy + dmz
!!$   
!!$         end do
!!$      end do
!!$   end do
!!$   call boundary_update_border(delta_f,'+','ym')
!!$   call boundary_neumann(delta_f,'-xm')
!!$   call boundary_neumann(delta_f,'+xm')
!!$   call boundary_neumann(delta_f,'-ym')
!!$   call boundary_neumann(delta_f,'+ym')
!!$
!!$   ! Filtered delta function describing front
!!$   do k = kmin_,kmax_
!!$      do j = jmin_,jmax_
!!$         do i = imin_,imax_
!!$            diff_x  = sum(grad_xm(i,j,:)*FG_st(i-1:i+1,j,k))
!!$            diff_y  = sum(grad_ym(i,j,:)*FG_st(i,j-1:j+1,k))
!!$            diff_z  = sum(grad_zm(i,j,:)*FG_st(i,j,k-1:k+1))
!!$            abs_lap = sqrt( diff_x**2 + diff_y**2 + diff_z**2 + eps)
!!$      
!!$            diff_xp = abs((FG_st(i+1,j,k)-FG_st(i,j,k))/(xm(i+1)-xm(i)))
!!$            diff_xm = abs((FG_st(i,j,k)-FG_st(i-1,j,k))/(xm(i)-xm(i-1)))
!!$      
!!$            diff_yp = abs((FG_st(i,j+1,k)-FG_st(i,j,k))/(ym(j+1)-ym(j)))
!!$            diff_ym = abs((FG_st(i,j,k)-FG_st(i,j-1,k))/(ym(j)-ym(j-1)))
!!$      
!!$            diff_zp = abs((FG_st(i,j,k+1)-FG_st(i,j,k))/(zm(k+1)-zm(k))*ymi(j))
!!$            diff_zm = abs((FG_st(i,j,k)-FG_st(i,j,k-1))/(zm(k)-zm(k-1))*ymi(j))
!!$
!!$            if (FG_st(i+1,j,k)*FG_st(i,j,k).le.0.0_WP) then
!!$               if (diff_xp.eq.0.0_WP) then 
!!$                  dpx = 0.0_WP 
!!$               else
!!$                  dpx = abs(FG_st(i+1,j,k)*diff_x) / &
!!$                     ( (xm(i+1)-xm(i))**2 * diff_xp * abs_lap )
!!$               end if
!!$            else
!!$               dpx = 0.0_WP
!!$            end if
!!$
!!$            if (FG_st(i-1,j,k)*FG_st(i,j,k).lt.0.0_WP) then
!!$               if (diff_xm.eq.0.0_WP) then 
!!$                  dmx = 0.0_WP 
!!$               else
!!$                  dmx = abs(FG_st(i-1,j,k)*diff_x) / &
!!$                     ( (xm(i)-xm(i-1))**2 * diff_xm * abs_lap )
!!$               end if
!!$            else
!!$               dmx = 0.0_WP
!!$            end if
!!$      
!!$            if (FG_st(i,j+1,k)*FG_st(i,j,k).le.0.0_WP) then
!!$               if (diff_yp.eq.0.0_WP) then 
!!$                  dpy = 0.0_WP 
!!$               else
!!$                  dpy = abs(FG_st(i,j+1,k)*diff_y) / &
!!$                     ( (ym(j+1)-ym(j))**2 * diff_yp * abs_lap )
!!$               end if
!!$            else
!!$               dpy = 0.0_WP
!!$            end if
!!$
!!$            if (FG_st(i,j-1,k)*FG_st(i,j,k).lt.0.0_WP) then
!!$               if (diff_ym.eq.0.0_WP) then 
!!$                  dmy = 0.0_WP 
!!$               else
!!$                  dmy = abs(FG_st(i,j-1,k)*diff_y) / &
!!$                     ( (ym(j)-ym(j-1))**2 * diff_ym * abs_lap )
!!$               end if
!!$            else
!!$               dmy = 0.0_WP
!!$            end if
!!$      
!!$            if (FG_st(i,j,k+1)*FG_st(i,j,k).le.0.0_WP) then
!!$               if (diff_zp.eq.0.0_WP) then 
!!$                  dpz = 0.0_WP 
!!$               else
!!$                  dpz = abs(FG_st(i,j,k+1)*diff_z) / &
!!$                     ( ym(j)**2*(zm(k+1)-zm(k))**2 * diff_zp * abs_lap )
!!$               end if
!!$            else
!!$               dpz = 0.0_WP
!!$            end if
!!$
!!$            if (FG_st(i,j,k-1)*FG_st(i,j,k).lt.0.0_WP) then
!!$               if (diff_zm.eq.0.0_WP) then 
!!$                  dmz = 0.0_WP 
!!$               else
!!$                  dmz = abs(FG_st(i,j,k-1)*diff_z) / &
!!$                     ( ym(j)**2*(zm(k)-zm(k-1))**2 * diff_zm * abs_lap )
!!$               end if
!!$            else
!!$               dmz = 0.0_WP
!!$            end if
!!$
!!$            delta_ff(i,j,k) = dpx + dpy + dpz + dmx + dmy + dmz
!!$   
!!$         end do
!!$      end do
!!$   end do
!!$   call boundary_update_border(delta_ff,'+','ym')
!!$   call boundary_neumann(delta_ff,'-xm')
!!$   call boundary_neumann(delta_ff,'+xm')
!!$   call boundary_neumann(delta_ff,'-ym')
!!$   call boundary_neumann(delta_ff,'+ym')
!!$
!!$   return
!!$end subroutine delta_compute_cyl
!!$
!!$!===============================================================================
!!$!===============================================================================
!!$
!!$function  quadf(x_local) result(d)
!!$  use precision
!!$  implicit none
!!$  real(WP), intent(in) :: x_local
!!$  real(WP) :: d
!!$  
!!$  d = (log(x_local+1.0_WP))/x_local
!!$  
!!$end function quadf
!!$
!!$!===============================================================================
!!$!===============================================================================
!!$               
!!$
