module levelset_houc
  use levelset
  use data
  use scalar_houc
  implicit none
  
  ! Empty module

end module levelset_houc

! =========================================== !
! Initialize the houc module                  !
! =========================================== !
subroutine levelset_houc_init
  use levelset_houc
  use masks
  use math
  implicit none
  
  ! Initialize the scalar metrics
  call scalar_houc_init
  
  return
end subroutine levelset_houc_init


! =========================================================== !
! Compute the residuals of the levelset equation              !
!                                                             !
! - velocity field @ n stored in Uold                         !
! - levelset field @ n stored in Gmid                         !
!                                                             !
! 3 working arrays of size (at least) (nx_+1)*(ny_+1)*(nz_+1) !
! =========================================================== !
subroutine levelset_houc_residual
  use levelset_houc
  use parallel
  use memory
  implicit none
  
  integer  :: i,j,k
  real(WP) :: rhs
  
  do k=kmin_-st1,kmax_+st2
     do j=jmin_-st1,jmax_+st2
        do i=imin_-st1,imax_+st2
           
           FX(i,j,k) = &
             -0.5_WP*(rhoU(i,j,k)+abs(rhoU(i,j,k)))*sum(houc_xp(i,j,:)*LVLSET(i-sthp1:i+sthp2,j,k)) &
             -0.5_WP*(rhoU(i,j,k)-abs(rhoU(i,j,k)))*sum(houc_xm(i,j,:)*LVLSET(i-sthm1:i+sthm2,j,k))

           FY(i,j,k) = &
             -0.5_WP*(rhoV(i,j,k)+abs(rhoV(i,j,k)))*sum(houc_yp(i,j,:)*LVLSET(i,j-sthp1:j+sthp2,k)) &
             -0.5_WP*(rhoV(i,j,k)-abs(rhoV(i,j,k)))*sum(houc_ym(i,j,:)*LVLSET(i,j-sthm1:j+sthm2,k))

           FZ(i,j,k) = &
             -0.5_WP*(rhoW(i,j,k)+abs(rhoW(i,j,k)))*sum(houc_zp(:)*LVLSET(i,j,k-sthp1:k+sthp2)) &
             -0.5_WP*(rhoW(i,j,k)-abs(rhoW(i,j,k)))*sum(houc_zm(:)*LVLSET(i,j,k-sthm1:k+sthm2))

        end do
     end do
  end do

  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_

           rhs = sum(div_u(i,j,k,:)*FX(i-st1:i+st2,j,k))+&
                 sum(div_v(i,j,k,:)*FY(i,j-st1:j+st2,k))+&
                 sum(div_w(i,j,k,:)*FZ(i,j,k-st1:k+st2))
           
           ResLVLSET(i,j,k) = -2.0_WP*LVLSET(i,j,k)+LVLSETold(i,j,k) + & 
                     (RHOold(i,j,k)*LVLSETold(i,j,k)+dt_old*rhs) / RHO(i,j,k) &
                     + dt*srcLVLSET(i,j,k)
                     
        end do
     end do
  end do
  
  return
end subroutine levelset_houc_residual
