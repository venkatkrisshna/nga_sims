module levelset_marker
  use levelset
  use data
  implicit none
  
  ! Absolute value of min/max levelset
  real(WP) :: LVLSET_cutoff
  
  ! Size of the band
  integer :: band_width
  
  ! Value of the band 
  integer, dimension(:,:,:), allocatable :: LVLSET_band
  
  ! Number of pints in each bands
  integer, dimension(:), allocatable :: minus_band_count
  integer, dimension(:), allocatable :: plus_band_count
 
  ! Lists of nodes in the plus and minus bands
  ! All points inside the domain +/-1 (min_-1 to max_+1)
  integer, dimension(:,:), allocatable :: minus_band_ijk
  integer, dimension(:,:), allocatable :: plus_band_ijk
  
end module levelset_marker


! ============================================ !
! Initialize the marker module                 !
! Used even if markers are not used for reinit !
! ============================================ !
subroutine levelset_marker_init
  use levelset_marker
  use parser
  implicit none
  
  ! Number of the bands
  call parser_read('Band width',band_width)
  ! Cutoff value for levelset
  call parser_read('Cutoff value',LVLSET_cutoff)
  
  ! Allocate the arrays
  allocate(LVLSET_band     (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(minus_band_ijk  (nxo_*nyo_*nzo_,3))
  allocate(plus_band_ijk   (nxo_*nyo_*nzo_,3))
  allocate(minus_band_count(band_width)) 
  allocate(plus_band_count (band_width))
  
  ! Recompute the bands
  call levelset_set_bands
  
  return
end subroutine levelset_marker_init


! ===================================== !
! Compute the band number of all points !
! ===================================== !
subroutine levelset_set_bands
  use levelset_marker
  use masks
  implicit none
  integer :: i,j,k,n
  integer :: ii,jj,kk
  integer :: plus_counter, minus_counter
  
  ! Set arrays to zero
  LVLSET_band      = 0
  plus_band_ijk    = 0
  minus_band_ijk   = 0  
  plus_band_count  = 0
  minus_band_count = 0
  
  ! Start counters at zero
  plus_counter     = 0
  minus_counter    = 0
  
  ! Find the front, and put surrounding nodes into the 1st band
  do k = kmin_-1,kmax_+1
     do j = jmin_-1,jmax_+1
        loop: do i = imin_-1,imax_+1
           
           if (mask(i,j).eq.1 .or. mask(i,j).eq.3) cycle loop
           
           do kk = max(k-1,kmin_-1),min(k+1,kmax_+1)
              do jj = max(j-1,jmin_-1),min(j+1,jmax_+1)
                 do ii = max(i-1,imin_-1),min(i+1,imax_+1)
                    
                    if (LVLSET(i,j,k)*LVLSET(ii,jj,kk).le.0.0_WP) then
                       
                       if ((LVLSET(i,j,k).le.0).and.(LVLSET_band(i,j,k).eq.0)) then 
                          minus_counter = minus_counter + 1
                          minus_band_ijk(minus_counter,1) = i
                          minus_band_ijk(minus_counter,2) = j
                          minus_band_ijk(minus_counter,3) = k
                          LVLSET_band(i,j,k) = -1
                       elseif ((LVLSET(i,j,k).gt.0).and.(LVLSET_band(i,j,k).eq.0)) then
                          plus_counter = plus_counter + 1
                          plus_band_ijk(plus_counter,1) = i
                          plus_band_ijk(plus_counter,2) = j
                          plus_band_ijk(plus_counter,3) = k
                          LVLSET_band(i,j,k) = 1
                       end if
                       
                       cycle loop
                    end if
                    
                 end do
              end do
           end do
           
        end do loop
     end do
  end do
  
  plus_band_count(1)  = plus_counter
  minus_band_count(1) = minus_counter
  call boundary_update_border_int(LVLSET_band,'+','ym')
  
  ! Grow all bands out from the front
  do n = 2,band_width
     call levelset_grow_band(n)
     call boundary_update_border_int(LVLSET_band,'+','ym')
  end do
  
  return
end subroutine levelset_set_bands



! ====================================== !
! Once the +1/-1 bands have been created !
! extend the bands to n                  !
! ====================================== !
subroutine levelset_grow_band(n)
  use levelset_marker
  use parallel
  use masks
  implicit none
  integer :: i,j,k,m
  integer,intent(in) :: n
  integer :: ii,jj,kk
  integer :: plus_counter, minus_counter
  integer :: plus_start, minus_start
  
  ! Reset counters
  plus_counter  = 0
  plus_start    = sum(plus_band_count(1:n-1))
  minus_counter = 0 
  minus_start   = sum(minus_band_count(1:n-1))
  
  ! Plus nodes
  do m = sum(plus_band_count(1:n-2))+1, sum(plus_band_count(1:n-1))
     
     i = plus_band_ijk(m,1)
     j = plus_band_ijk(m,2)
     k = plus_band_ijk(m,3)
     
     do kk = max(k-1,kmin_-1),min(k+1,kmax_+1)
        do jj = max(j-1,jmin_-1),min(j+1,jmax_+1)
           do ii = max(i-1,imin_-1),min(i+1,imax_+1)
              
              ! If masked, discard
              if (mask(ii,jj).eq.1 .or. mask(ii,jj).eq.3) cycle
              
              if ((LVLSET_band(ii,jj,kk).eq.0).and.(LVLSET(ii,jj,kk).gt.0.0_WP)) then
                 ! Put the point in the band
                 LVLSET_band(ii,jj,kk) = n
                 plus_counter = plus_counter + 1
                 plus_band_ijk(plus_start + plus_counter,1) = ii
                 plus_band_ijk(plus_start + plus_counter,2) = jj
                 plus_band_ijk(plus_start + plus_counter,3) = kk
              end if
              
           end do
        end do
     end do
  end do
  plus_band_count(n) = plus_counter
  
  ! Minus nodes
  do m = sum(minus_band_count(1:n-2))+1, sum(minus_band_count(1:n-1))
     
     i = minus_band_ijk(m,1)
     j = minus_band_ijk(m,2)
     k = minus_band_ijk(m,3)
     
     do kk = max(k-1,kmin_-1),min(k+1,kmax_+1)
        do jj = max(j-1,jmin_-1),min(j+1,jmax_+1)
           do ii = max(i-1,imin_-1),min(i+1,imax_+1)
              
              ! If masked, discard
              if (mask(ii,jj).eq.1 .or. mask(ii,jj).eq.3) cycle
              
              if ((LVLSET_band(ii,jj,kk).eq.0).and.(LVLSET(ii,jj,kk).le.0.0_WP)) then
                 ! Put the point in the band
                 LVLSET_band(ii,jj,kk) = -n
                 minus_counter = minus_counter + 1
                 minus_band_ijk(minus_start + minus_counter,1) = ii
                 minus_band_ijk(minus_start + minus_counter,2) = jj
                 minus_band_ijk(minus_start + minus_counter,3) = kk
              end if
              
           end do
        end do
     end do
  end do
  minus_band_count(n) = minus_counter
  
  return
end subroutine levelset_grow_band



! =============================== !
! Clip the values of the levelset !
! =============================== !
subroutine levelset_cutoff
  use levelset_marker
  implicit none
  integer :: i,j,k
  
  ! Clip the level set value 
  do k = kmino_,kmaxo_
     do j = jmino_,jmaxo_
        do i = imino_,imaxo_
           if (LVLSET_band(i,j,k).ne.0) then
              if (LVLSET(i,j,k).gt.  LVLSET_cutoff ) LVLSET(i,j,k) =  LVLSET_cutoff
              if (LVLSET(i,j,k).lt.(-LVLSET_cutoff)) LVLSET(i,j,k) = -LVLSET_cutoff 
           else
              if (LVLSET(i,j,k).ge.0.0_WP) LVLSET(i,j,k) =  LVLSET_cutoff
              if (LVLSET(i,j,k).lt.0.0_WP) LVLSET(i,j,k) = -LVLSET_cutoff 
           end if
        end do
     end do
  end do
  
  ! Update the physical boundaries
  call levelset_apply_bc(LVLSET,'+','ym')
  
  ! Update the level set in the walls
  call levelset_update_walls(LVLSET)
  
  return
end subroutine levelset_cutoff


