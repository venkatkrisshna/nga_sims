module diffusion
  use combustion
  implicit none
  
  ! Diffusion combustion model
  character(len=str_medium) :: diffModel
  
  ! Diffusion source term for PROG
  ! Saved for partially premixed combustion
  real(WP), dimension(:,:,:), allocatable :: C_DIFF_SRC
  
end module diffusion


! ============================ !
! Initialization of the module !
! ============================ !
subroutine diffusion_init
  use diffusion
  implicit none
  
  character(len=str_medium) :: filename
  
  ! Read in the chemtable and initialize it
  call parser_read('Diffusion chemtable',filename)
  call diffusion_chemtable_init(filename)
  
  ! Specific Initilization for the models
  select case(trim(diffModel))
  case ('FPVA')
     if ((isc_ZMIX.eq.0) .or. (isc_PROG.eq.0)) then
        call die('diffusion_init: FPVA model requires both scalars ZMIX and C ')
     end if
  case ('Steady Flamelet')
     if (isc_ZMIX.eq.0) then
        call die('diffusion_init: Steady Flamelet model requires the scalar ZMIX')
     end if
     ! Need to recompute the diffusivity, but inaccurate
     ! -> Missing eddy diffusivity
     ! -> diffusivity depends upon chi which depends upon diffusivity...
     call combustion_CHI
  case ('Burke-Schumann')
     if (isc_ZMIX.eq.0) then
        call die('diffusion_init: Burke-Schumann model requires ZMIX')
     end if
  case default
     call die('diffusion_init: Not yet implemented')
  end select
  
  ! Allocate arrays
  allocate(C_DIFF_SRC (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  
  return
end subroutine diffusion_init


! ============================================= !
! Compute the source term for progress variable !
! ============================================= !
subroutine diffusion_source_prog(SCmid,src)
  use diffusion
  use memory
  use time_info
  implicit none
  
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nscalar) :: SCmid
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nscalar) :: src
  integer  :: i,j,k
  
  if (trim(diffModel).eq.'FPVA') then
     ! Get source term for PROG from chemtable
     call diffusion_chemtable_lookup('SRC_PROG',C_DIFF_SRC, &
          SCmid(:,:,:,isc_ZMIX),ZVAR,SCmid(:,:,:,isc_PROG),nxo_*nyo_*nzo_)
     
     ! Evaluate source term for RHO*PROG
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              src(i,j,k,isc_PROG) = src(i,j,k,isc_PROG) + &
                   0.5_WP*(RHO(i,j,k)+RHOold(i,j,k)) * dt * C_DIFF_SRC(i,j,k)
           end do
        end do
     end do
  end if
  
  return
end subroutine diffusion_source_prog


! ========================== !
! Ignite the flow everywhere !
! ========================== !
subroutine diffusion_ignite
  use diffusion
  implicit none
  
  integer :: i,j,k
  real(WP) :: sc_min,sc_max
  
  ! Get the most burning flamelet
  if ((trim(diffModel).eq.'FPVA')) then
     do k=kmino_,kmaxo_
        do j=jmino_,jmaxo_
           do i=imino_,imaxo_
              call diffusion_chemtable_prog_minmax(SC(i,j,k,isc_ZMIX),ZVAR(i,j,k),sc_min,sc_max)
              SC(i,j,k,isc_PROG) = sc_max
           end do
        end do
     end do
  end if
  
  return
end subroutine diffusion_ignite


! ===================================== !
! Simplified diffusion chemtable lookup !
! ===================================== !
subroutine diffusion_lookup(tag,Ain)
  use diffusion
  use memory
  implicit none
  
  character(len=*), intent(in) :: tag
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: Ain
  
  select case(trim(diffModel))
  case ('FPVA')
     call diffusion_chemtable_lookup(tag,Ain,SC(:,:,:,isc_ZMIX),ZVAR,SC(:,:,:,isc_PROG),nxo_*nyo_*nzo_)
  case ('Steady Flamelet')
     call diffusion_chemtable_lookup(tag,Ain,SC(:,:,:,isc_ZMIX),ZVAR,CHI,nxo_*nyo_*nzo_)
  case ('Enthalpy Flamelet')
     call diffusion_chemtable_lookup(tag,Ain,SC(:,:,:,isc_ZMIX),ZVAR,SC(:,:,:,isc_ENTH),nxo_*nyo_*nzo_)
  case ('Burke-Schumann')
     tmp1 = 0.0_WP
     call diffusion_chemtable_lookup(tag,Ain,SC(:,:,:,isc_ZMIX),tmp1,tmp1,nxo_*nyo_*nzo_)
  case default
     call die('diffusion_lookup: diffModel not yet Implemented')
  end select
  
  return
end subroutine diffusion_lookup


