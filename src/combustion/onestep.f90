! Adapted from Comb. Flame 2006 - Fernandez-Galisteo, Sanchez, Linan
! "A simple one-step chemistry model for partially premixed hydrocarbon combustion"
module onestep
  use combustion
  use string
  implicit none
  
  ! Min/Max values for temperature
  real(WP), parameter :: Tmin =  298.0_WP
  real(WP), parameter :: Tmax = 3000.0_WP

  ! Thermo data
  real(WP), parameter :: R_cst = 8.314472_WP  ! [J/(mol.K)]
  
  ! Indices of the transported scalars
  integer :: isc_Yf,isc_Yo2,isc_Yh2o, isc_Yco2

   ! Stoichiometric coeff
  real(WP) :: nu_F,nu_O2,nu_H2O,nu_CO2

  ! Molecular weights
  real(WP) :: W_F
  real(WP), parameter :: W_O2  = 31.9988e-3_WP
  real(WP), parameter :: W_CO2 = 44.0095e-3_WP
  real(WP), parameter :: W_H2O = 18.0153e-3_WP
  real(WP), parameter :: W_N2  = 28.0135e-3_WP
  
  ! Computation of ZMIX
  real(WP) :: Yf_1,Yf_2,Yo_1,Yo_2,Yd_1,Yd_2
  real(WP) :: nu, Zst
  real(WP), dimension(:,:,:), allocatable :: Mix_Frac

  ! Flame index
  real(WP), dimension(:,:,:), allocatable :: ksi
  ! Chemical source term
  real(WP), dimension(:,:,:), allocatable :: Wchem
  
  ! DASSL variables
  real(WP) :: dassl_fraction
  integer, dimension(15) :: info
  integer :: idid
  real(WP), dimension(:), allocatable :: rtol
  real(WP), dimension(:), allocatable :: atol
  integer :: lrw,liw
  integer,  dimension(:), allocatable :: iwork
  real(WP), dimension(:), allocatable :: rwork
  real(WP) :: rpar
  integer  :: ipar
  integer  :: neq
  
  ! Radiation heat loss
  logical :: radiation
  
  ! Species parameters - GRI-MECH 3.0
  real(WP), dimension(6) :: ch_F   = (/ 1.65326226E+00, 1.00263099E-02,-3.31661238E-06, 5.36483138E-10,-3.14696758E-14,-1.00095936E+04 /)
  real(WP), dimension(6) :: cl_F   = (/ 5.14911468E+00,-1.36622009E-02, 4.91453921E-05,-4.84246767E-08, 1.66603441E-11,-1.02465983E+04 /)

  real(WP), dimension(6) :: ch_O2  = (/ 3.28253784E+00, 1.48308754E-03,-7.57966669E-07, 2.09470555E-10,-2.16717794E-14,-1.08845772E+03 /)
  real(WP), dimension(6) :: cl_O2  = (/ 3.78245636E+00,-2.99673416E-03, 9.84730201E-06,-9.68129509E-09, 3.24372837E-12,-1.06394356E+03 /)
  real(WP), dimension(6) :: ch_CO2 = (/ 3.85746029E+00, 4.41437026E-03,-2.21481404E-06, 5.23490188E-10,-4.72084164E-14,-4.87591660E+04 /)
  real(WP), dimension(6) :: cl_CO2 = (/ 2.35677352E+00, 8.98459677E-03,-7.12356269E-06, 2.45919022E-09,-1.43699548E-13,-4.83719697E+04 /)
  real(WP), dimension(6) :: ch_H2O = (/ 3.03399249E+00, 2.17691804E-03,-1.64072518E-07,-9.70419870E-11, 1.68200992E-14,-3.00042971E+04 /)
  real(WP), dimension(6) :: cl_H2O = (/ 4.19864056E+00,-2.03643410E-03, 6.52040211E-06,-5.48797062E-09, 1.77197817E-12,-3.02937267E+04 /)
  real(WP), dimension(6) :: ch_N2  = (/ 2.92664000E+00, 1.48797680E-03,-5.68476000E-07, 1.00970380E-10,-6.75335100E-15,-9.22797700E+02 /)
  real(WP), dimension(6) :: cl_N2  = (/ 3.29867700E+00, 1.40824040E-03,-3.96322200E-06, 5.64151500E-09,-2.44485400E-12,-1.02089990E+03 /)
  
  ! Heat Release precomputed
  real(WP), dimension(6) :: ch_Q
  real(WP), dimension(6) :: cl_Q
  
  
  ! Reaction constants - Methane
  ! Variable Heat Release
  real(WP), parameter :: aq = 0.21_WP
  ! Modified Arrhenius rate constant
  real(WP), parameter :: A_cst = 6.9e8_WP    ! [m^3/(mol.s)]
  real(WP), parameter :: Ta0 = 15.9e3_WP     ! [K]
  real(WP), parameter :: phi_lean = 0.64_WP
  real(WP), parameter :: phi_rich = 1.07_WP
  real(WP), parameter :: TaC_lean = 8.250_WP
  real(WP), parameter :: TaC_rich = 1.443_WP
  ! Flamability limit - Westbrook & Dryer CST 1981
  real(WP), parameter :: phi_min = 0.5_WP
  real(WP), parameter :: phi_max = 1.6_WP

  ! Reaction constants - Heptane - OD
  !real(WP), parameter :: Cp = 1200.0_WP         ! [J/(kg.K)]
  !real(WP), parameter :: A_cst = 1.2e9_WP       ! [m^3/(mol.s)]
  !real(WP), parameter :: Ta0 = 14.0e3_WP        ! [K]
  !real(WP), parameter :: phi_rich = 0.95_WP     ! [-]
  !real(WP), parameter :: Q0 = 4.0e6_WP          ! [J/mol]
  !real(WP), parameter :: aq = 0.18_WP           ! [-]
  ! Flamability limit
  !real(WP), parameter :: phi_min = 0.5_WP
  !real(WP), parameter :: phi_max = 4.5_WP
  
contains
  
  ! Read in the name of the fuel and evaluate critical parameters
  ! -------------------------------------------------------------
  subroutine onestep_setup_reaction
    use parser
    implicit none
    character(len=str_short) :: fuel
    integer :: iC,iH,nC,nH
    
    ! Get name of fuel 
    call parser_read('One-step fuel', fuel)
    
    ! Get formula
    iC = index(fuel,'C')
    iH = index(fuel,'H')
    if (iC.eq.0) then ! H2
       nC = 0
       nH = 2
    else
       if (iH.eq.iC+1) then ! CH4
          nC = 1
          nH = 4
       else
          read(fuel(iC+1:iH-1),'(i4)') nC
          read(fuel(iH+1:str_short),'(i4)') nH
       end if
    end if
    
    ! Stoichiometric coeff
    nu_F   = -1.0_WP
    nu_O2  = -nC-0.25_WP*nH
    nu_CO2 = nC
    nu_H2O = 0.5_WP*nH
    
    ! Molecular weight
    W_F = 12.0107e-3_WP*nC + 1.00794e-3_WP*nH
    
    ! Precompute heat release
    ch_Q = -R_cst * (nu_F*ch_F + nu_O2*ch_O2 + nu_CO2*ch_CO2 + nu_H2O*ch_H2O)
    cl_Q = -R_cst * (nu_F*cl_F + nu_O2*cl_O2 + nu_CO2*cl_CO2 + nu_H2O*cl_H2O)
    
    return
  end subroutine onestep_setup_reaction
  
  ! Compute heat release
  ! --------------------
  function Q(T_,phi)
    implicit none
    real(WP) :: Q
    real(WP), intent(in) :: T_,phi
    integer  :: i
    
    if (T_.gt.1000.0_WP) then
       Q = ch_Q(6)
       do i=1,5
          Q = Q + ch_Q(i) * T_**i/real(i,WP)
       end do
    else
       Q = cl_Q(6)
       do i=1,5
          Q = Q + cl_Q(i) * T_**i/real(i,WP)
       end do
    end if
    
    if (phi.gt.1.0_WP) Q = max(Q*(1.0_WP-aQ*(phi-1.0_WP)),0.0_WP)
    
    return
  end function Q
  
  ! Compute heat capacity
  ! ---------------------
  function Cp(Yf,Yo2,Yco2,Yh2o,Yn2,T_)
    implicit none
    real(WP) :: Cp
    real(WP), intent(in) :: Yf,Yo2,Yco2,Yh2o,Yn2,T_
    integer  :: i
    
    Cp = 0.0_WP
    
    if (T_.gt.1000.0_WP) then
       do i=1,5
          Cp = Cp + T_**(i-1) * ( ch_F(i)*Yf/W_F + ch_O2(i)*Yo2/W_O2 + ch_CO2(i)*Yco2/W_CO2 + ch_H2O(i)*Yh2o/W_H2O + ch_N2(i)*Yn2/W_N2 )
       end do
    else
       do i=1,5
          Cp = Cp + T_**(i-1) * ( cl_F(i)*Yf/W_F + cl_O2(i)*Yo2/W_O2 + cl_CO2(i)*Yco2/W_CO2 + cl_H2O(i)*Yh2o/W_H2O + cl_N2(i)*Yn2/W_N2 )
       end do
    end if
    
    Cp = Cp * R_cst
    
    return
  end function Cp
  
  ! Compute heat capacity (molar)
  ! -----------------------------
  function HH(Yf,Yo2,Yco2,Yh2o,Yn2,T_)
    implicit none
    real(WP) :: HH
    real(WP), intent(in) :: Yf,Yo2,Yco2,Yh2o,Yn2,T_
    integer  :: i
    
    if (T_.gt.1000.0_WP) then
       HH = ch_F(6)*Yf/W_F + ch_O2(6)*Yo2/W_O2 + ch_CO2(6)*Yco2/W_CO2 + ch_H2O(6)*Yh2o/W_H2O + ch_N2(6)*Yn2/W_N2
       do i=1,5
          HH = HH + T_**i/real(i,WP) * ( ch_F(i)*Yf/W_F + ch_O2(i)*Yo2/W_O2 + ch_CO2(i)*Yco2/W_CO2 + ch_H2O(i)*Yh2o/W_H2O + ch_N2(i)*Yn2/W_N2 )
       end do
    else
       HH = cl_F(6)*Yf/W_F + cl_O2(6)*Yo2/W_O2 + cl_CO2(6)*Yco2/W_CO2 + cl_H2O(6)*Yh2o/W_H2O + cl_N2(6)*Yn2/W_N2
       do i=1,5
          HH = HH + T_**i/real(i,WP) * ( cl_F(i)*Yf/W_F + cl_O2(i)*Yo2/W_O2 + cl_CO2(i)*Yco2/W_CO2 + cl_H2O(i)*Yh2o/W_H2O + cl_N2(i)*Yn2/W_N2 )
       end do
    end if
    
    HH = HH * R_cst
    
    return
  end function HH
  
  ! Compute activation energy
  ! -------------------------
  Function Ta(phi)
    implicit none
    real(WP) :: Ta
    real(WP), intent(in) :: phi
    
    if (phi.lt.phi_lean) then
       Ta = Ta0*(1.0_WP+TaC_lean*(phi-phi_lean)**2)
    else if (phi.gt.phi_rich) then
       Ta = Ta0*(1.0_WP+TaC_rich*(phi-phi_rich)**2)
    else
       Ta = Ta0
    end if
    
    ! Old from heptane - OD
    !if (phi_.gt.phi_rich) then
    !   Ta_cst = Ta0*(1.558_WP+phi_*(-1.171_WP+phi_*0.621_WP))
    !else
    !   Ta_cst = Ta0
    !end if
    
    return
  end Function Ta
  
  ! Compute the Right-Hand Side of the equation
  ! -> the chemical source term
  ! -------------------------------------------
  subroutine onestep_compute_rhs(sol,rhs)
    implicit none

    real(WP), dimension(neq) :: sol,rhs    
    real(WP) :: Yf,Yo2,Yco2,Yh2o,Yn2
    real(WP) :: T_,rho_,Wmol_
    real(WP) :: Z_,Yf_u,Yo_u,phi
    real(WP) :: src
    
    ! Prepare the variables
    Yf   = min(max(sol(1),0.0_WP),1.0_WP)
    Yo2  = min(max(sol(2),0.0_WP),1.0_WP)
    Yco2 = min(max(sol(3),0.0_WP),1.0_WP)
    Yh2o = min(max(sol(4),0.0_WP),1.0_WP)
    Yn2  = min(max(1.0_WP-Yf-Yo2-Yco2-Yh2o,0.0_WP),1.0_WP)
    T_   = min(max(sol(5),Tmin),Tmax)
    
    ! Compute equivalence ratio
    Z_   = (nu*(Yf-Yf_2)-(Yo2-Yo_2)) / (nu*(Yf_1-Yf_2)-(Yo_1-Yo_2))
    Z_   = min(max(Z_,0.0_WP),1.0_WP)
    Yf_u = Yf_1*Z_ + Yf_2*(1.0_WP-Z_)
    Yo_u = Yo_1*Z_ + Yo_2*(1.0_WP-Z_)
    phi  = nu*Yf_u/(Yo_u+epsilon(Yo_u))
    
    ! Compute the new density
    Wmol_ = 1.0_WP/(Yf/W_F+Yo2/W_O2+Yco2/W_CO2+Yh2o/W_H2O+Yn2/W_N2)
    rho_  = Pthermo*Wmol_/(R_cst*T_)
    
    ! Compute chemical source term
    src = (rho_*Yf/W_F) * (rho_*Yo2/W_O2) * A_cst*exp(-Ta(phi)/T_) / rho_
    
    ! Compute source term
    rhs(1) = src * nu_F  *W_F
    rhs(2) = src * nu_O2 *W_O2
    rhs(3) = src * nu_CO2*W_CO2
    rhs(4) = src * nu_H2O*W_H2O
    rhs(5) = src * Q(T_,phi)/Cp(Yf,Yo2,Yco2,Yh2o,Yn2,T_)
    
    return
  end subroutine onestep_compute_rhs
  
  
  ! Compute the residual of the system for one point
  ! ------------------------------------------------
  subroutine onestep_res(time,sol,dsol,res,ires,rpar,ipar)
    implicit none
    
    real(WP), intent(in)   :: time
    real(WP), dimension(neq) :: sol,dsol,res
    real(WP) :: ires
    real(WP) :: rpar
    integer  :: ipar
    
    ! Compute rhs
    call onestep_compute_rhs(sol,res)
    
    ! Compute : res=source-Y'
    res = res - dsol
    
    return
  end subroutine onestep_res
  
  
  ! Dummy function required by DASSL
  subroutine onestep_jac
  end subroutine onestep_jac

end module onestep


! ================================= !
! Initialize the One Step Chemistry !
! ================================= !
subroutine onestep_init
  use onestep
  use parser
  implicit none
  
  real(WP) :: tol
  integer  :: isc

  ! Create & Start the timer
  call timing_create('one-step')
  call timing_start ('one-step')

  ! Find the indices of the variables
  isc_Yf = 0
  isc_Yo2   = 0
  isc_Yco2  = 0
  isc_Yh2o  = 0
  do isc=1,nscalar
     select case(trim(SC_name(isc)))
     case ('Yf')
        isc_Yf = isc
     case ('Yo2')
        isc_Yo2 = isc
     case ('Yco2')
        isc_Yco2 = isc
     case ('Yh2o')
        isc_Yh2o = isc
     end select
  end do
  
  ! Check we have enough to use the model
  if ( (isc_Yf.eq.0)   .or. (isc_Yo2.eq.0)  .or. &
       (isc_Yco2.eq.0) .or. (isc_Yh2o.eq.0) .or. (isc_T .eq.0) ) then
     call die('onestep_init: one-step model requires Yf, Yo2, Yco2, Yh2o, and T')
  end if
  
  ! Read fuel and evaluate reaction properties
  call onestep_setup_reaction
  
  ! Boundary conditions to compute ZMIX
  call parser_read('Yf_1',Yf_1,1.0_WP)
  call parser_read('Yf_2',Yf_2,0.0_WP)
  call parser_read('Yo_1',Yo_1,0.0_WP)
  call parser_read('Yo_2',Yo_2,0.233_WP)
  Yd_1 = 1.0_WP-Yf_1-Yo_1
  Yd_2 = 1.0_WP-Yf_2-Yo_2
  nu  = nu_O2*W_O2/(nu_F*W_F)
  Zst = (nu*Yf_2-Yo_2) / (nu*(Yf_2-Yf_1)-(Yo_2-Yo_1))
  !print*,"Zst",Zst

  ! Check that we're DP
  if (WP.ne.kind(1.0_WP)) call die('onestep_init: DASSL is DP only')
  
  ! DASSL info
  info(1)  = 0 ! First call
  info(2)  = 1 ! Tolerances are arrays
  info(3)  = 0 ! Output when asked to only
  info(4)  = 0 ! Allow time overshoot
  info(5)  = 0 ! We do not provide an analytical Jacobian
  info(6)  = 0 ! Not a banded problem
  info(7)  = 0 ! Let DASSL decide the timestep
  info(8)  = 0 ! Let DASSL decide the initial timestep
  info(9)  = 0 ! Allow maximum order of 5
  info(10) = 1 ! Non-negativity constrain - not used (try it ?)
  info(11) = 0 ! Initial derivative are consistent
  info(12) = 0 ! Not used
  info(13) = 0 ! Not used
  info(14) = 0 ! Not used
  info(15) = 0 ! Not used
  
  ! Number of equations
  neq = 5
  
  ! Tolerances
  allocate(rtol(neq))
  allocate(atol(neq))
  call parser_read('One-step cvg.',tol)
  rtol = tol
  atol(1:4) = tol
  atol(5) = tol*1000.0_WP
  
  ! Work space
  liw = 20+neq
  lrw = 40+(4+5)*neq+neq*neq
  allocate(iwork(liw))
  allocate(rwork(lrw))
  
  ! Allocate Arrays
  allocate(Wchem   (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); Wchem = 0.0_WP
  allocate(Mix_Frac(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(ksi     (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  
  ! Radiation heat losses
  call parser_read('Radiation',radiation,.false.)
  
  ! Create monitor file
  call monitor_create_file_step('onestep',2)
  call monitor_set_header(1,'Pthermo','r')
  call monitor_set_header(2,'DASSL %','r')
  
  ! Stop the timer
  call timing_stop('one-step')

  return
end subroutine onestep_init


! ============================ !
! Compute the Mixture Fraction !
! ============================ !
subroutine onestep_get_mixfrac
  use onestep
  implicit none
  
  integer  :: i,j,k
  real(WP) :: Yf,Yo2
  
  ! Start the timer
  call timing_start('one-step')

  ! Compute the mixture fraction from the species
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           Yf  = SC(i,j,k,isc_Yf)
           Yo2 = SC(i,j,k,isc_Yo2)
           Mix_Frac(i,j,k) = (nu*(Yf-Yf_2)-(Yo2-Yo_2)) / (nu*(Yf_1-Yf_2)-(Yo_1-Yo_2))
        end do
     end do
  end do
  
  ! Stop the timer
  call timing_stop('one-step')

  return
end subroutine onestep_get_mixfrac


! =================================== !
! Compute the Scalar dissipation rate !
! =================================== !
subroutine onestep_get_chi
  use onestep
  use metric_generic
  implicit none
  
  integer :: i,j,k
  real(WP) :: Gx,Gy,Gz
  
  ! Start the timer
  call timing_start('one-step')

  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           Gx = sum(grad_xm(i,j,:)*Mix_Frac(i-stp:i+stp,j,k))
           Gy = sum(grad_ym(i,j,:)*Mix_Frac(i,j-stp:j+stp,k))
           Gz = sum(grad_zm(i,j,:)*Mix_Frac(i,j,k-stp:k+stp))
           
           CHI(i,j,k) = 2.0_WP*DIFF(i,j,k,isc_Yf)*(Gx**2+Gy**2+Gz**2)/RHO(i,j,k)
        end do
     end do
  end do
  
  ! Update ghost cells
  call boundary_update_border(CHI,'+','ym')
  
  ! Stop the timer
  call timing_stop('one-step')

  return
end subroutine onestep_get_chi


! ======================= !
! Compute the flame index !
! ======================= !
subroutine onestep_get_flame_index
  use onestep
  use metric_generic
  implicit none
  
  integer  :: i,j,k
  real(WP) :: Gfx,Gfy,Gfz
  real(WP) :: Gox,Goy,Goz
  real(WP) :: Tak,Wchem_max
  
  ! Start the timer
  call timing_start('one-step')

  ! Get maximum heat release
  call parallel_max(Wchem,Wchem_max)
  
  ! Compute the flame index
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Grad(Yf)
           Gfx = sum(grad_xm(i,j,:)*SC(i-stp:i+stp,j,k,isc_Yf))
           Gfy = sum(grad_ym(i,j,:)*SC(i,j-stp:j+stp,k,isc_Yf))
           Gfz = sum(grad_zm(i,j,:)*SC(i,j,k-stp:k+stp,isc_Yf))
           
           ! Grad(Yo2)
           Gox = sum(grad_xm(i,j,:)*SC(i-stp:i+stp,j,k,isc_Yo2))
           Goy = sum(grad_ym(i,j,:)*SC(i,j-stp:j+stp,k,isc_Yo2))
           Goz = sum(grad_zm(i,j,:)*SC(i,j,k-stp:k+stp,isc_Yo2))
           
           ! Takano flame index
           Tak = Gfx*Gox + Gfy*Goy + Gfz*Goz
           
           ! Normalized flame index
           if (Wchem(i,j,k) .ge. 1.0e-4_WP*Wchem_max .and. Tak.ne.0.0_WP) then
              ksi(i,j,k) = 0.5_WP*(1.0_WP+Tak/abs(Tak))
           else
              ksi(i,j,k) = -1.0_WP
           end if
        end do
     end do
  end do
  
  ! Update ghost cells
  call boundary_update_border(ksi,'+','ym')
  
  ! Stop the timer
  call timing_stop('one-step')

  return
end subroutine onestep_get_flame_index


! ============================================================ !
! Compute the new density from the mass fraction & Temperature !
! ============================================================ !
subroutine onestep_density
  use onestep
  use masks
  implicit none
  
  integer :: i,j,k
  real(WP) :: Yf,Yo2,Yco2,Yh2o,Yn2,T_
  
  ! Start the timer
  call timing_start('one-step')

  ! Compute the new density fron the equation of state
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           
           if (mask(i,j).eq.1) then
              RHO(i,j,k) = 1.0_WP
              
           else
              ! Clip for the evaluation of Molar Mass
              Yf   = min(max(SC(i,j,k,isc_Yf),   0.0_WP),max(Yf_1,Yf_2))
              Yo2  = min(max(SC(i,j,k,isc_Yo2),  0.0_WP),max(Yo_1,Yo_2))
              Yco2 = min(max(SC(i,j,k,isc_Yco2), 0.0_WP),1.0_WP)
              Yh2o = min(max(SC(i,j,k,isc_Yh2o), 0.0_WP),1.0_WP)
              Yn2  = min(max(1.0_WP-Yf-Yo2-Yco2-Yh2o,min(Yd_1,Yd_2)),max(Yd_1,Yd_2))
              T_   = min(max(SC(i,j,k,isc_T ),Tmin),Tmax)
              
              Wmol(i,j,k) = 1.0_WP/(Yf/W_F+Yo2/W_O2+Yco2/W_CO2+Yh2o/W_H2O+Yn2/W_N2)
              
              ! Compute new density 
              RHO(i,j,k) = Pthermo*Wmol(i,j,k)/(R_cst*T_)              
           end if
           
        end do
     end do
  end do
  
  ! Stop the timer
  call timing_stop('one-step')

  return
end subroutine onestep_density


! =============================================== !
! Compute the viscosity by using Sutherland's law !
! =============================================== !
subroutine onestep_viscosity
  use onestep
  implicit none

  ! Viscosity of Air
  real(WP), parameter :: visc0 = 1.7894e-5_WP
  real(WP), parameter :: Tref  = 273.11_WP
  real(WP), parameter :: Sref  = 110.56_WP
  
  integer  :: i,j,k
  real(WP) :: T_
  
  ! Start the timer
  call timing_start('one-step')
  
  do k = kmino_,kmaxo_
     do j = jmino_,jmaxo_
        do i = imino_,imaxo_
           
           T_ = min(max(SC(i,j,k,isc_T ),Tmin),Tmax)
           VISC(i,j,k) = visc0 * (T_/Tref)**1.5_WP * (Tref+Sref)/(T_+Sref)
           
        end do
     end do
  end do
  
  ! Stop the timer
  call timing_stop('one-step')
  
  return
end subroutine onestep_viscosity


! ================================================= !
! Compute the diffusivity by using Sutherland's law !
! ================================================= !
subroutine onestep_diffusivity
  use onestep
  implicit none

  ! Diffusivity is computed with a constant Prandtl number (0.7)
  real(WP), parameter :: diff0 = 2.5563e-5_WP
  real(WP), parameter :: Tref  = 273.11_WP
  real(WP), parameter :: Sref  = 110.56_WP
  
  integer  :: i,j,k
  real(WP) :: T_
  
  ! Start the timer
  call timing_start('one-step')

  do k = kmino_,kmaxo_
     do j = jmino_,jmaxo_
        do i = imino_,imaxo_
           
           T_ = min(max(SC(i,j,k,isc_T ),Tmin),Tmax)
           DIFF(i,j,k,:) = diff0 * (T_/Tref)**1.5_WP * (Tref+Sref)/(T_+Sref)
           
        end do
     end do
  end do
  
  ! Stop the timer
  call timing_stop('one-step')

  return
end subroutine onestep_diffusivity


! ================================================== !
! Compute the source terms due to a presure increase !
! ================================================== !
subroutine onestep_source_pressure(src)
  use onestep
  use masks
  implicit none

  integer :: i,j,k
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nscalar) :: src
  real(WP) :: Yf,Yo2,Yco2,Yh2o,Yn2,T_
  
  if (.not.constant_volume) return
  
  ! Start the timer
  call timing_start('one-step')

  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (mask(i,j).ne.1) then
              ! Get massfractions & temperature
              Yf   = min(max(SC(i,j,k,isc_Yf),  0.0_WP),max(Yf_1,Yf_2))
              Yo2  = min(max(SC(i,j,k,isc_Yo2), 0.0_WP),max(Yo_1,Yo_2))
              Yco2 = min(max(SC(i,j,k,isc_Yco2),0.0_WP),1.0_WP)
              Yh2o = min(max(SC(i,j,k,isc_Yh2o),0.0_WP),1.0_WP)
              Yn2  = min(max(1.0_WP-Yf-Yo2-Yco2-Yh2o,min(Yd_1,Yd_2)),max(Yd_1,Yd_2))
              T_ = min(max(SC(i,j,k,isc_T ),Tmin),Tmax)

              src(i,j,k,isc_T) = src(i,j,k,isc_T) + (Pthermo-Pthermo_old)/Cp(Yf,Yo2,Yco2,Yh2o,Yn2,T_)
           end if
        end do
     end do
  end do
  
  ! Stop the timer
  call timing_stop('one-step')

  return
end subroutine onestep_source_pressure


! ============================================= !
! Compute the source terms due to enthalpy flux !
! ============================================= !
subroutine onestep_source_entflux(src)
  use onestep
  use metric_generic
  use time_info
  implicit none

  integer :: i,j,k
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nscalar) :: src
  real(WP) :: T_,GT,Yf,Yo2,Yco2,Yh2o,Yn2
  real(WP), parameter :: hT = 1.0_WP
  real(WP) :: dCpdT
  
  ! Start the timer
  call timing_start('one-step')
  
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Get massfractions & temperature
           Yf   = min(max(SC(i,j,k,isc_Yf),  0.0_WP),max(Yf_1,Yf_2))
           Yo2  = min(max(SC(i,j,k,isc_Yo2), 0.0_WP),max(Yo_1,Yo_2))
           Yco2 = min(max(SC(i,j,k,isc_Yco2),0.0_WP),1.0_WP)
           Yh2o = min(max(SC(i,j,k,isc_Yh2o),0.0_WP),1.0_WP)
           Yn2  = min(max(1.0_WP-Yf-Yo2-Yco2-Yh2o,min(Yd_1,Yd_2)),max(Yd_1,Yd_2))
           T_   = min(max(SC(i,j,k,isc_T ),Tmin),Tmax)
           
           ! Compute gradient of temperature
           GT = sum(grad_xm(i,j,:)*SC(i-stp:i+stp,j,k,isc_T))**2 + &
                sum(grad_ym(i,j,:)*SC(i,j-stp:j+stp,k,isc_T))**2 + &
                sum(grad_zm(i,j,:)*SC(i,j,k-stp:k+stp,isc_T))**2
           
           dCpdT = (Cp(Yf,Yo2,Yco2,Yh2o,Yn2,T_+hT)/Cp(Yf,Yo2,Yco2,Yh2o,Yn2,T_)-1.0_WP)/hT
           
           src(i,j,k,isc_T) = src(i,j,k,isc_T) + DIFF(i,j,k,isc_T)* GT * dCpdT * dt
           
        end do
     end do
  end do
  
  ! Stop the timer
  call timing_stop('one-step')

  return
end subroutine onestep_source_entflux


! ======================================= !
! Compute the sink terms due to radiation !
! ======================================= !
subroutine onestep_source_radiation(src)
  use onestep
  use time_info
  use masks
  implicit none
  
  integer :: i,j,k
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nscalar) :: src
  real(WP), parameter :: sigma = 5.67e-8_WP
  real(WP) :: Yf,Yo2,Yco2,Yh2o,Yn2,Wmol_
  real(WP) :: T_,invT,Xco2,Xh2o,a_CO2,a_H2O
  
  if (.not.radiation) return
  
  ! Start the timer
  call timing_start('one-step')

  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           
           if (mask(i,j).ne.1) then
              ! Compute molecular weight
              Yf   = min(max(SC(i,j,k,isc_Yf),  0.0_WP),max(Yf_1,Yf_2))
              Yo2  = min(max(SC(i,j,k,isc_Yo2), 0.0_WP),max(Yo_1,Yo_2))
              Yco2 = min(max(SC(i,j,k,isc_Yco2),0.0_WP),1.0_WP)
              Yh2o = min(max(SC(i,j,k,isc_Yh2o),0.0_WP),1.0_WP)
              Yn2  = min(max(1.0_WP-Yf-Yo2-Yco2-Yh2o,min(Yd_1,Yd_2)),max(Yd_1,Yd_2))
              Wmol_ = 1.0_WP/(Yf/W_F+Yo2/W_O2+Yco2/W_CO2+Yh2o/W_H2O+Yn2/W_N2)

              ! Evaluate mole fractions of species
              Xco2 = Yco2*Wmol_/W_CO2
              Xh2o = Yh2o*Wmol_/W_H2O
              
              ! Evaluate the absorption coefficients
              ! Fits to RADCAL constants - TNF workshop
              T_ = min(max(SC(i,j,k,isc_T ),Tmin),Tmax)
              invT = 1000.0_WP/T_
              a_CO2 = 18.741 - 121.310*invT + 273.500*invT**2 - 194.050*invT**3 + 56.310*invT**4 - 5.8169*invT**5
              a_H2O = -0.23093 - 1.12390*invT + 9.41530*invT**2 - 2.99880*invT**3 + 0.51382*invT**4 - 1.8684e-5*invT**5
              
              ! Evaluate source term
              ! Optically thin assumption
              src(i,j,k,isc_T) = src(i,j,k,isc_T) - &
                   4.0_WP*sigma*T_**4 * Pthermo*1e-5_WP * (Xco2*a_CO2 + Xh2o*a_H2O) * dt/Cp(Yf,Yo2,Yco2,Yh2o,Yn2,T_)
           end if
        end do
     end do
  end do
  
  ! Stop the timer
  call timing_stop('one-step')
  
  return
end subroutine onestep_source_radiation


! ======================================= !
! Compute the source terms for combustion !
! ======================================= !
subroutine onestep_source_chemistry(src)
  use onestep
  use memory
  use time_info
  use masks
  use parallel
  implicit none
  
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nscalar) :: src
  real(WP), dimension(neq) :: solold,sol,dsol
  integer  :: i,j,k
  real(WP) :: t_start,t_chem
  real(WP) :: phi,Z_,Yf_u,Yo_u
  
  integer :: my_count,count
  my_count = 0
  
  ! Start the timer
  call timing_start('one-step')

  ! Chemical source terms
  ! ---------------------
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           
           ! --- Only if not masked cell ---
           if (mask(i,j).ne.1) then

              ! Set the Initial Conditions
              sol(1) = min(max(SC(i,j,k,isc_Yf),  0.0_WP),1.0_WP)
              sol(2) = min(max(SC(i,j,k,isc_Yo2), 0.0_WP),1.0_WP)
              sol(3) = min(max(SC(i,j,k,isc_Yco2),0.0_WP),1.0_WP)
              sol(4) = min(max(SC(i,j,k,isc_Yh2o),0.0_WP),1.0_WP)
              sol(5) = min(max(SC(i,j,k,isc_T),Tmin),Tmax)
              solold = sol
              
              ! Need to compute mixture fraction first
              ! phi is not conserved in a flame
              Z_   = (nu*(sol(1)-Yf_2)-(sol(2)-Yo_2)) / (nu*(Yf_1-Yf_2)-(Yo_1-Yo_2))
              Z_   = min(max(Z_,0.0_WP),1.0_WP)
              Yf_u = Yf_1*Z_ + Yf_2*(1.0_WP-Z_)
              Yo_u = Yo_1*Z_ + Yo_2*(1.0_WP-Z_)
              phi  = nu*Yf_u/(Yo_u+epsilon(Yo_u))
              
              ! Nothing to do if outside flamability limit
              if (phi.gt.phi_min .and. phi.lt.phi_max) then
                 
                 ! Compute a consistent yprime for DASSL
                 call onestep_compute_rhs(sol,dsol)
                 
                 ! Test if DASSL is necessary
                 t_chem = sol(5) / (abs(dsol(5))+epsilon(1.0_WP))
                 if (dt.gt.0.01_WP*t_chem) then
                    
                    my_count = my_count + 1
                    
                    ! Reset problem to new problem
                    info(1) = 0
                    t_start = 0.0_WP
                    
                    ! Solve
                    call DDASSL(onestep_res,neq,t_start,sol,dsol,dt,info, &
                         rtol,atol,idid,rwork,lrw,iwork,liw,rpar,ipar,onestep_jac)
                    
                    ! Get change over time
                    dsol = sol-solold
                 else
                    ! Explicit prediction
                    dsol = dsol*dt
                 end if
                 
                 ! Get back the source terms
                 src(i,j,k,isc_Yf)   = src(i,j,k,isc_Yf)   + dsol(1) * RHO(i,j,k)
                 src(i,j,k,isc_Yo2)  = src(i,j,k,isc_Yo2)  + dsol(2) * RHO(i,j,k)
                 src(i,j,k,isc_Yco2) = src(i,j,k,isc_Yco2) + dsol(3) * RHO(i,j,k)
                 src(i,j,k,isc_Yh2o) = src(i,j,k,isc_Yh2o) + dsol(4) * RHO(i,j,k)
                 src(i,j,k,isc_T )   = src(i,j,k,isc_T )   + dsol(5) * RHO(i,j,k)
              end if
           end if

        end do
     end do
  end do
  
  call parallel_sum(my_count,count)
  dassl_fraction=100.0_WP*real(count,WP)/real(nx*ny*nz,WP)
  
  ! Dump the quantities of interest in a monitor file
  call monitor_select_file('onestep')
  call monitor_set_single_value(1,Pthermo)
  call monitor_set_single_value(2,dassl_fraction)
  
  ! Get back Wchem
  Wchem = src(:,:,:,isc_Yf)/(nu_f*W_f*dt)
  
  ! Stop the timer
  call timing_stop('one-step')

  return
end subroutine onestep_source_chemistry


! ============================================================= !
! Check if the scalars are bounded within their physical limits !
! ============================================================= !
subroutine onestep_check_bounds(SC_,isc,bounded)
  use onestep
  implicit none
  
  real(WP), dimension(nscalar) :: SC_
  integer,  intent(in)  :: isc
  integer,  intent(inout) :: bounded
  
  ! No timer here, because too many calls

  ! If any of the mass fraction are outside the bounds then
  ! all the mass fractions used for onestep are out of bounds
  if (isc.eq.isc_Yf .or. isc.eq.isc_Yo2 .or. isc.eq.isc_Yco2 .or. isc.eq.isc_Yh2o .or. isc.eq.isc_T) then
     ! Lower bound
     if (SC_(isc_Yf)  .lt.-0.01_WP) bounded = 0
     if (SC_(isc_Yo2) .lt.-0.01_WP) bounded = 0
     if (SC_(isc_Yco2).lt.-0.01_WP) bounded = 0
     if (SC_(isc_Yh2o).lt.-0.01_WP) bounded = 0
     if (SC_(isc_T)   .lt.Tmin*0.95_WP) bounded = 0
     ! Upper bound
     if (SC_(isc_Yf)  .gt.1.01_WP*max(Yf_1,Yf_2)) bounded = 0
     if (SC_(isc_Yo2) .gt.1.01_WP*max(Yo_1,Yo_2)) bounded = 0
     if (SC_(isc_Yco2).gt.1.01_WP) bounded = 0
     if (SC_(isc_Yh2o).gt.1.01_WP) bounded = 0
     if (SC_(isc_T)   .gt.Tmax) bounded = 0
  end if
  
  return
end subroutine onestep_check_bounds
