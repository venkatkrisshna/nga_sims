module regime_index
  use precision
  use finitechem
  implicit none
  
  real(WP), dimension(:,:,:), allocatable :: LAMBDA
  real(WP), dimension(:,:,:), allocatable :: CHI_LAMBDA
  
  real(WP), dimension(:,:,:),   allocatable :: Y_O2
  real(WP), dimension(:,:,:,:), allocatable :: Y_O2_NORM
  real(WP), dimension(:,:,:),   allocatable :: Y_CH4
  real(WP), dimension(:,:,:,:), allocatable :: Y_CH4_NORM
  
  real(WP), dimension(:,:,:),   allocatable :: flame_index
  
  real(WP), dimension(:,:,:),   allocatable :: regime_fit_old
  
  real(WP) :: lean_flam_limit, rich_flam_limit
  
  ! Regime information
  real(WP), dimension(:,:,:), allocatable :: regime
  real(WP), dimension(:,:,:), allocatable :: regime_fit
  real(WP), dimension(:,:,:), allocatable :: C_DIFF_SRC

end module regime_index


! ============================================================= !
! Look in the chemtable for the variable named 'tag'            !
! with the value A1, A2, and A3 for the three mapping variables !
! ============================================================= !
subroutine mixed_chemtable_lookup(tag, R)
  use combustion
  use regime_index
  use data
  implicit none
  
  character(len=*), intent(in) :: tag
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_), intent(out) :: R
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: prem_val
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: diff_val
  
  real(WP) :: factor
  integer  :: i,j,k
  
  call premixed_chemtable_lookup(tag, prem_val, SC(:,:,:,isc_ZMIX), ZVAR, &
       SC(:,:,:,isc_PROG), nxo_*nyo_*nzo_)
  
  call chemtable_lookup(tag, diff_val, SC(:,:,:,isc_ZMIX), ZVAR, &
       SC(:,:,:,isc_PROG), nxo_*nyo_*nzo_)
  
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           
           if (regime_fit(i,j,k).le.0.0_WP) then
              factor = 0.0_WP
           else
              factor = regime_fit(i,j,k)
           end if
           R(i,j,k) = factor*prem_val(i,j,k) + (1.0_WP-factor)*diff_val(i,j,k)
           
        end do
     end do
  end do
  
  return
end subroutine mixed_chemtable_lookup


! ================================== !
! Initialize the regime index module !
! ================================== !
subroutine regime_index_init
  use regime_index
  use data
  implicit none
  
  ! Allocate temporary arrays
  allocate(Y_O2 (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(Y_CH4(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  
  allocate(Y_O2_NORM (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3))
  allocate(Y_CH4_NORM(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3))
  
  allocate(flame_index(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  
  allocate(regime_fit    (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(regime_fit_old(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  
  regime_fit = 0.0_WP
  regime_fit_old = 0.0_WP
  
  call parser_read('Lean flammability limit',lean_flam_limit)
  call parser_read('Rich flammability limit',rich_flam_limit)
  
  ! Compute the regime index
  call regime_index_compute
  
  return
end subroutine regime_index_init


! ======================================================== !
! Compute the flame index based on fuel/oxidizer gradients !
! ======================================================== !
subroutine regime_index_compute
  use regime_index
  use premixed_chemtable
  use data
  use combustion
  use memory
  use metric_generic
  implicit none

  real(WP) :: dot_prod
  integer  :: i,j,k
  
  ! Call chemtable for species
  call mixed_chemtable_lookup('Y_CH4',Y_CH4)
  call mixed_chemtable_lookup('Y_O2', Y_O2)
  
  ! Compute the flame index
  call compute_gradient(Y_CH4,Y_CH4_NORM)
  call compute_gradient(Y_O2 ,Y_O2_NORM )
  
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           dot_prod = sum(Y_CH4_NORM(i,j,k,:) * Y_O2_NORM(i,j,k,:))
           
           if (abs(dot_prod).ge.1.0E-5_WP) then
              flame_index(i,j,k) = 0.5_WP * ( 1.0_WP + dot_prod/(abs(dot_prod)+1.0E-8_WP) )
           else
              flame_index(i,j,k) = -1.0_WP
           end if
        end do
     end do
  end do
  
  ! Update the boundaries
  call boundary_update_border(flame_index(:,:,:),'+','ym')
  call boundary_neumann(flame_index(:,:,:),'-xm')
  call boundary_neumann(flame_index(:,:,:),'+xm')
  call boundary_neumann(flame_index(:,:,:),'-ym')
  call boundary_neumann(flame_index(:,:,:),'+ym')
  
  ! Smooth the flame index
  call regime_index_smooth
  
  return
end subroutine regime_index_compute


! ====================================== !
! Compute the gradient of a scalar field !
! ====================================== !
subroutine compute_gradient(A,Anorm)
  use metric_generic
  use data
  implicit none

  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_),   intent(in)  :: A
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3), intent(out) :: Anorm
  real(WP), dimension(3) :: Agrad
  real(WP) :: grad_mag
  integer  :: i,j,k,n
  
  ! Use 2nd order central differencing to compute the normals
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           
           Agrad(1) = sum(grad_xm(i,j,:)*A(i-1:i+1,j,k))      
           Agrad(2) = sum(grad_ym(i,j,:)*A(i,j-1:j+1,k))
           Agrad(3) = sum(grad_zm(i,j,:)*A(i,j,k-1:k+1))
           
           grad_mag = sqrt(sum( Agrad**2 ))
           
           if (grad_mag .ge. 1.0e-4_WP) then
              Anorm(i,j,k,:) = Agrad / grad_mag
           else
              Anorm(i,j,k,:) = 0.0_WP
           end if
           
        end do
     end do
  end do
  
  ! Update the boundaries
  do n=1,3
     call boundary_update_border(Anorm(:,:,:,n),'+','ym')
     call boundary_neumann(Anorm(:,:,:,n),'-xm')
     call boundary_neumann(Anorm(:,:,:,n),'+xm')
     call boundary_neumann(Anorm(:,:,:,n),'-ym')
     call boundary_neumann(Anorm(:,:,:,n),'+ym')
  end do
  
  return
end subroutine compute_gradient


! ======================= !
! Smooth the regime index !
! ======================= !
subroutine regime_index_smooth
  use regime_index
  use data
  use combustion
  use memory
  use masks
  use time_info
  implicit none

  integer  :: i,j,k, count, n
  real(WP) :: alpha

  regime_fit_old = regime_fit

  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (flame_index(i,j,k).le.0.0_WP) then
              regime_fit(i,j,k) = 0.0_WP
           else
              if ((SC(i,j,k,isc_ZMIX).ge.lean_flam_limit) & 
                 .and.(SC(i,j,k,isc_ZMIX).le.rich_flam_limit)) then
                 regime_fit(i,j,k) = flame_index(i,j,k)
              else
                 regime_fit(i,j,k) = 0.0_WP
              end if
           end if
        end do
     end do
  end do
  call boundary_update_border(regime_fit,'+','ym')

  n = 3
  do count = 1,n
     call filter_global_3D(regime_fit,tmp1,'+','n')
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              regime_fit(i,j,k) = tmp1(i,j,k)            
           end do
        end do
     end do
     call boundary_update_border(regime_fit,'+','ym')
     call boundary_neumann(regime_fit(:,:,:),'-xm')
     call boundary_neumann(regime_fit(:,:,:),'+xm')
     call boundary_neumann(regime_fit(:,:,:),'-ym')
     call boundary_neumann(regime_fit(:,:,:),'+ym')
  end do

  if (ntime.le.1) then
     alpha = 0.00_WP
  elseif (ntime.le.4) then
     alpha = 0.25_WP
  else
     alpha = 0.50_WP
  end if
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           regime_fit(i,j,k) =  (1.0_WP-alpha)*regime_fit(i,j,k) &
                              + (alpha       )*regime_fit_old(i,j,k)
        end do
     end do
  end do
  call boundary_update_border(regime_fit,'+','ym')
  call boundary_neumann(regime_fit(:,:,:),'-xm')
  call boundary_neumann(regime_fit(:,:,:),'+xm')
  call boundary_neumann(regime_fit(:,:,:),'-ym')
  call boundary_neumann(regime_fit(:,:,:),'+ym')

  return
end subroutine regime_index_smooth
