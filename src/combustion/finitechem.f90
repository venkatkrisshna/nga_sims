module finitechem
  use combustion
  use string
  use dvode_f90_m
  use time_info
  implicit none
  
  ! Clipping values for temperature
  real(WP), parameter :: Tmin=  50.0_WP
  real(WP), parameter :: Tmax=5000.0_WP
  
  ! Properties of the species - Cp [J/(mol.K)]
  real(WP), dimension(:), allocatable :: Wsp,Cpsp,hsp
    
  ! Thermo data - [J/(kmol.K)]
  real(WP), parameter :: Rcst=8.31434_WP
  
  ! Various indices and numbers
  integer :: Nreac ! Number of reactions
  integer :: Ntot  ! Number of species
  integer :: Nsp   ! Number of non steady-state species
  integer :: NT    ! Index of temperature
  integer :: isc_1 ! Index of first species scalar
  
  ! Chemical source term
  real(WP), dimension(:,:,:,:), allocatable :: SRCchem
  
  ! Diffusion fluxes
  real(WP), dimension(:,:,:,:), allocatable :: DFX,DFY,DFZ
  
  ! Cp of the mixture
  real(WP), dimension(:,:,:), allocatable :: Cpmix
  
  ! Heat release rate and its volumetric average
  real(WP), dimension(:,:,:), allocatable :: HR
  real(WP) :: HRavg
  
  ! DVODE variables
  type(vode_opts), save :: opts
  real(WP) :: rtol,atol,implicit_frac
    
  ! Variables used to calculate the viscosity, diffusivity, Lewis
  real(WP), dimension(:), allocatable :: Le
  logical :: use_lewis
      
  ! Chemistry CFL
  real(WP) :: CFLchem,CFLsp 
  
  ! Flame speed calculation 
  integer :: nf                                           ! Number of fuel species
  character(len=str_short), dimension(:), allocatable :: spf  ! Name of fuel species
  integer, dimension(:), allocatable :: ispf                  ! Indices of fuel species
  real(WP) :: srcF                                        ! Fuel source term
  real(WP) :: SLf                                         ! Flame speed
  
  ! Analytical jacobian
  logical :: use_jacanal

contains
  
  ! ================================================================== !
  ! Computes the chemical source term of the system (called by solver) !
  ! ================================================================== !
  subroutine finitechem_compute_rhs(n_,t_,sol,rhs)
    implicit none
    
    integer,  intent(in) :: n_
    real(WP), intent(in) :: t_
    real(WP), dimension(NT), intent(in)  :: sol
    real(WP), dimension(NT), intent(out) :: rhs
    real(WP) :: Cp_mix,Wmix,RHOmix
    real(WP), dimension(Ntot) :: conc
    real(WP), dimension(Nreac) :: myW,myK,myM
        
    ! Get W of mixture
    call finitechem_W(sol(1:Nsp),Wmix)

    ! Get Cp of mixture
    call finitechem_Cp(sol(1:Nsp),sol(NT),Cp_mix)
    
    ! Get RHO of mixture
    RHOmix=Pthermo*Wmix/(Rcst*sol(NT))
    
    ! Calculate concentrations of unsteady species
    conc(1:Nsp)=RHOmix*sol(1:Nsp)/Wsp(1:Nsp)
    
    ! --- Get thirdbodies --- !
    call thirdbodies( conc, myM )
    
    ! --- Rate coefficients --- !
    call ratecoefficients( myK, sol(NT), Pthermo, myM )
    
    ! --- Reaction rates --- !
    call reactionrates( myW, myK, conc, myM )
    
    ! --- Production rates --- !
    call prodrates(rhs,myW)
    
    ! Transform concentration into mass fraction
    rhs(1:Nsp)=rhs(1:Nsp)*Wsp(1:Nsp)/RHOmix
         
    ! Temperature rhs from change in concentration
    rhs(NT)=-sum(hsp(1:Nsp)*rhs(1:Nsp))/Cp_mix

    return
  end subroutine finitechem_compute_rhs

  ! ========================================================== !
  ! Computes the approximate analytical jacobian of the system !
  ! ========================================================== !
  subroutine finitechem_compute_jac(n_,t_,sol,ml,mu,jac,nrpd)
    implicit none

    ! Input
    integer :: n_, ml, mu, nrpd
    real(WP) :: t_
    real(WP), dimension(n_) :: sol,myPR
    real(WP), dimension(nrpd,n_) :: jac
    
    ! Local variables
    real(WP) :: Cp_mix,Wmix,RHOmix
    real(WP), dimension(Ntot) :: conc
    real(WP), dimension(Nreac) :: myW,myK,myM,dkdTcoeff

    ! Get W of mixture
    call finitechem_W(sol(1:Nsp),Wmix)

    ! Get Cp of mixture
    call finitechem_Cp(sol(1:Nsp),sol(NT),Cp_mix)

    ! Get RHO of mixture
    RHOmix=Pthermo*Wmix/(Rcst*sol(NT))
    
    ! Calculate concentrations of unsteady species
    conc(1:Nsp)=RHOmix*sol(1:Nsp)/Wsp(1:Nsp)

    ! --- Get thirdbodies --- !
    call thirdbodies( conc, myM )
    
    ! --- Rate coefficients --- !
    call ratecoefficients( myK, sol(NT), Pthermo, myM )
    
    ! --- Reaction rates --- !
    call reactionrates( myW, myK, conc, myM )
    
    ! --- Production rates --- !
    call prodrates(myPR,myW)
    myPR(1:Nsp)=myPR(1:Nsp)*Wsp(1:Nsp)/RHOmix

    ! Get analytical Jacobian from mechanism file  
    call get_jacobian(conc(1:Nsp),sol(NT),myPR,myW,myK,myM,RHOmix,CP_mix,hsp,cpsp,Wsp,Pthermo,jac)

    return
  end subroutine finitechem_compute_jac
  
end module finitechem

! ================================ !
! Initialize finite rate chemistry !
! Based on mechanism.f file        !
! ================================ !
subroutine finitechem_init
  use finitechem
  use parser
  use fileio
  implicit none
  
  integer :: i,j
  character(len=str_medium), dimension(:), allocatable :: spname
  character(len=str_medium) :: name
  real(WP) :: tmp
  
  ! Check we have enough to use the model
  if (isc_T   .eq.0) call die('finitechem_init: finite chem model requires T')
  if (isc_ENTH.eq.0) call die('finitechem_init: finite chem model requires H')
  
  ! Get number of species
  call getnspecies_all(Ntot)
  
  ! Get number of unsteady species
  call getnspecies(Nsp)
  
  ! Get species names
  allocate(spname(Ntot))
  call getspeciesnames(spname)
  
  ! Assumes all species are listed one after the other, in the same order as in mechanism.f
  isc_1=0
  do i=1,nscalar
     name=spname(1)
     if (SC_name(i).eq.trim(name(1:str_short))) isc_1=i
  end do
  if (isc_1.eq.0.and.Nsp.gt.0) call die('finitechem_init: species ' // trim(spname(1)) // 'not found')
  do i=1,Nsp
     name=spname(i)
     if (trim(SC_name(isc_1+i-1)).ne.trim(name(1:str_short))) &
          call die('finitechem_init: species ' // trim(spname(i)) // 'not found')
  end do
  
  ! Set temperature as Nsp+1 scalar
  NT=Nsp+1
   
  ! Get molecular masses of species
  allocate(Wsp(Ntot))
  call GETMOLARMASS(Wsp)
    
  ! Get number of reactions
  call GETNREACTIONS(Nreac)
  
  ! Allocate Cp for unsteady species
  allocate(Cpsp(Nsp))
  
  ! Allocate unsteady species enthalpy
  allocate(hsp(Nsp))
  
  ! Initialize transport properties
  call mechanism_init
  
  ! Allocate arrays
  allocate(HR(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); HR=0.0_WP
  allocate(Cpmix(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)); Cpmix=0.0_WP
  allocate(SRCchem(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,NT)); SRCchem=0.0_WP
  allocate(DFX(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,Nsp)); DFX=0.0_WP
  allocate(DFY(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,Nsp)); DFY=0.0_WP
  allocate(DFZ(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,Nsp)); DFZ=0.0_WP
  
  ! DVODE tolerances
  call parser_read('Finite chem acvg',atol,1.0e-15_WP)
  call parser_read('Finite chem rcvg',rtol,1.0e-10_WP)
  
  ! Allocate Lewis numbers
  allocate(Le(Nsp))
  
  ! Get Lewis numbers
  Le = 1.0_WP
  do i=1,Nsp
     call parser_read('Lewis '// SC_name(isc_1+i-1),Le(i),1.0_WP)
  end do
  call parser_read('Constant Lewis',use_lewis,.false.)
  
  ! --- Laminar flame speed calculation --- !
  if (simu_type .eq. 'flame speed') then
     ! Get fuel species and find corresponding indices
     call parser_getsize('Fuels',nf)
     allocate(spf(nf),ispf(nf))
     call parser_read('Fuels',spf(1:nf))
     ! Find indices of species relevant for initialization
     ispf = 0
     do i=1,Nsp
        do j=1,nf
           name = spf(j)
           if (SC_name(isc_1+i-1).eq.trim(name(1:str_short))) ispf(j) = i
        end do
     end do
     if (sum(ispf).eq.0) call die ('Please define appropriate fuel species')
     ! Create monitor file - Laminar flame speed
     srcF=0.0_WP
     call monitor_create_file_step('flame_speed',1)
     call monitor_set_header(1,'SL [cm/s]','r')
  end if
  
  ! Create monitor file - general
  call monitor_create_file_step('finite_chem',5)
  call monitor_set_header(1,'Pthermo','r')
  call monitor_set_header(2,'HRavg','r')
  call monitor_set_header(3,'Implicit %','r')
  call monitor_set_header(4,'CFLchem','r')
  call monitor_set_header(5,'CFLsp','r')
  
  ! Initialize monitor files
  call finitechem_monitor

  ! Use analytical jacobian or not
  call parser_read('Use analytical jac',use_jacanal,.false.)

  return
end subroutine finitechem_init


! ============================================================== !
! Compute the source terms for combustion at each sub-iterations !
! ============================================================== !
subroutine finitechem_source_mid(src)
  use finitechem
  implicit none  
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nscalar) :: src
  
  ! Chemical source terms
  call finitechem_source_chem_mid(src)
  
  ! Pressure source terms
  call finitechem_source_pres(src)
  
  ! Diffusive source terms
  call finitechem_source_diff(src)
  
  ! Enthalpy flux source terms
  call finitechem_source_enth(src)

  ! Update monitor
  call finitechem_monitor

  return
end subroutine finitechem_source_mid

! ================================================================================== !
! Compute the chemical source term before sub-iterations, do not multiply by density !
! ================================================================================== !
subroutine finitechem_source_chem
  use finitechem
  use masks
  implicit none
  
  real(WP), dimension(NT) :: solold,sol,dsol
  integer  :: i,j,k,istate,itask,n
  real(WP) :: tstop,tstart,tchem,myHRavg,mysrcF
  real(WP), dimension(22) :: rstats
  integer,  dimension(31) :: istats
  integer :: mycount,count
  real(WP), parameter :: Yshift=1.0e-6_WP
  
  ! Time integrate the chemical source term
  mysrcF = 0.0_WP
  SRCchem=0.0_WP
  mycount=0
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Not in walls
           if (vol(i,j,k).eq.0.0_WP) cycle
           ! Get initial field
           sol(1:Nsp)=max(SC(i,j,k,isc_1:isc_1+Nsp-1),0.0_WP)
           sol(NT)=SC(i,j,k,isc_T)
           solold=sol
           ! Evaluate local chemical time scale
           call finitechem_compute_rhs(NT,0.0_WP,sol,dsol)
           tchem=minval((sol+Yshift)/(abs(dsol)+epsilon(1.0_WP)))
           ! Assess need for DVODE
           if (dt.gt.0.1_WP*tchem) then
              ! Set values for DVODE
              itask=1; istate=1; tstart=0.0_WP; tstop=dt
              if (use_jacanal) then
                 opts=set_opts(dense_j=.true.,mxstep=500000,abserr=atol,relerr=rtol,tcrit=tstop,user_supplied_jacobian=.true.)
                 call dvode_f90(finitechem_compute_rhs,NT,sol,tstart,tstop,itask,istate,opts,j_fcn=finitechem_compute_jac)
              else
                 opts=set_opts(method_flag=22,mxstep=500000,abserr=atol,relerr=rtol,tcrit=tstop)
                 call dvode_f90(finitechem_compute_rhs,NT,sol,tstart,tstop,itask,istate,opts,j_fcn=finitechem_compute_jac)
              end if
              call get_stats(rstats,istats)
              call release_opts_arrays(opts)
              
              ! Error handling
              if (istate.ne.2) then
                 print *, '==============================='
                 print*, 'solold',solold
                 print *, '-------------------------------'
                 print*, 'sol',sol
                 print *, '-------------------------------'
                 print*, 'dsol',dsol
                 call die('finitechem_source: DVODE failed to converge.')
              end if
              ! Form source term
              SRCchem(i,j,k,:)=sol-solold
              ! Increment counter
              mycount=mycount+1
           else
              ! Explicit integration
              SRCchem(i,j,k,:)=dt*dsol
           end if
        end do
     end do
  end do

  ! Integrated fuel source term for flame speed
  if (simu_type.eq.'flame speed') then
     mysrcF=0.0_WP
     do n=1,nf
        mysrcF=mysrcF+sum(dx(imin_:imax_)*RHO(imin_:imax_,jmin_,kmin_)*SRCchem(imin_:imax_,jmin_,kmin_,ispf(n)))/dt
     end do
     call parallel_sum(mysrcF,srcF)
  end if
  
  ! Compute fraction of domain requiring implicit solver
  call parallel_sum(mycount,count)
  implicit_frac=100.0_WP*real(count,WP)/real(nx*ny*nz,WP)
  
  return
end subroutine finitechem_source_chem

! ================================================== !
! Multiply chemical source term with correct density !
! ================================================== !
subroutine finitechem_source_chem_mid(src)
  use finitechem
  use masks
  implicit none
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nscalar) :: src
  integer  :: i,j,k
  real(WP) :: myHRavg

  ! Add chemical source term to the scalar source
  HR=0.0_WP
  myHRavg=0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! Species source term
           src(i,j,k,isc_1:isc_1+Nsp-1)=src(i,j,k,isc_1:isc_1+Nsp-1)+SRCchem(i,j,k,1:Nsp)*RHO(i,j,k)
           ! Temperature source term
           src(i,j,k,isc_T)=src(i,j,k,isc_T)+SRCchem(i,j,k,NT)*RHO(i,j,k)
           ! Heat release
           call COMPTHERMODATA(hsp,Cpsp,SC(i,j,k,isc_T))
           HR(i,j,k)=-sum(hsp(1:Nsp)*SRCchem(i,j,k,1:Nsp))*RHO(i,j,k)/dt
           myHRavg=myHRavg+HR(i,j,k)*vol(i,j,k)
        end do
     end do
  end do

  ! Average heat release over the whole domain
  call parallel_sum(myHRavg,HRavg)
  HRavg=HRavg/vol_total

  return
end subroutine finitechem_source_chem_mid


! ================================ !
! Compute the pressure source term !
! ================================ !
subroutine finitechem_source_pres(src)
  use finitechem
  use masks
  implicit none
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nscalar) :: src
  integer :: i,j,k
  real(WP) :: Cp_mix,Tmix
  
  ! Compute pressure source term
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (mask(i,j).eq.1) cycle
           Tmix=min(max(SC(i,j,k,isc_T),Tmin),Tmax)
           call finitechem_Cp(SC(i,j,k,isc_1:isc_1+Nsp-1),Tmix,Cp_mix)
           src(i,j,k,isc_T   )=src(i,j,k,isc_T   )+(Pthermo-Pthermo_old)/Cp_mix
           src(i,j,k,isc_ENTH)=src(i,j,k,isc_ENTH)+(Pthermo-Pthermo_old)
        end do
     end do
  end do
  
  return
end subroutine finitechem_source_pres


! =========================== !
! Compute the diffusion terms !
! =========================== !
subroutine finitechem_source_diff(src)
  use finitechem
  use masks
  use metric_generic
  use memory
  implicit none
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nscalar) :: src
  integer :: i,j,k,n
  real(WP) :: Tmix
  
  ! Get Wmol and Cpmix fields
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           if (mask(i,j).ne.1) then
              Tmix=min(max(SC(i,j,k,isc_T),Tmin),Tmax)
              call finitechem_W(SC(i,j,k,isc_1:isc_1+Nsp-1),Wmol(i,j,k))
              call finitechem_Cp(SC(i,j,k,isc_1:isc_1+Nsp-1),Tmix,Cpmix(i,j,k))
           else
              Wmol(i,j,k)=1.0_WP
              Cpmix(i,j,k)=1.0_WP
           end if
        end do
     end do
  end do
  
  ! Form species diffusive fluxes
  do n=isc_1,isc_1+Nsp-1
     
     do k=kmin_-st1,kmax_+st2
        do j=jmin_-st1,jmax_+st2
           do i=imin_-st1,imax_+st2
              
              ! Molar diffusion correction - DIFF/Wmol*Yi*grad(Wmol)
              FX(i,j,k)=sum(interp_sc_x(i,j,:)*DIFF(i-st2:i+st1,j,k,n)/Wmol(i-st2:i+st1,j,k))*&
                   sum(interp_sc_x(i,j,:)*SC(i-st2:i+st1,j,k,n))*sum(grad_x(i,j,:)*Wmol(i-st2:i+st1,j,k))
              
              FY(i,j,k)=sum(interp_sc_y(i,j,:)*DIFF(i,j-st2:j+st1,k,n)/Wmol(i,j-st2:j+st1,k))*&
                   sum(interp_sc_y(i,j,:)*SC(i,j-st2:j+st1,k,n))*sum(grad_y(i,j,:)*Wmol(i,j-st2:j+st1,k))
              
              FZ(i,j,k)=sum(interp_sc_z(i,j,:)*DIFF(i,j,k-st2:k+st1,n)/Wmol(i,j,k-st2:k+st1))*&
                   sum(interp_sc_z(i,j,:)*SC(i,j,k-st2:k+st1,n))*sum(grad_z(i,j,:)*Wmol(i,j,k-st2:k+st1))
              
              ! Store full diffusive flux
              DFX(i,j,k,n-isc_1+1)=FX(i,j,k)+sum(interp_sc_x(i,j,:)*DIFF(i-st2:i+st1,j,k,n))*&
                   sum(grad_x(i,j,:)*SC(i-st2:i+st1,j,k,n))
              
              DFY(i,j,k,n-isc_1+1)=FY(i,j,k)+sum(interp_sc_y(i,j,:)*DIFF(i,j-st2:j+st1,k,n))*&
                   sum(grad_y(i,j,:)*SC(i,j-st2:j+st1,k,n))
              
              DFZ(i,j,k,n-isc_1+1)=FZ(i,j,k)+sum(interp_sc_z(i,j,:)*DIFF(i,j,k-st2:k+st1,n))*&
                   sum(grad_z(i,j,:)*SC(i,j,k-st2:k+st1,n))
              
           end do
        end do
     end do
     
     ! Update species source term
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              src(i,j,k,n)=src(i,j,k,n)+dt*(&
                   sum(div_u(i,j,k,:)*FX(i-st1:i+st2,j,k))+&
                   sum(div_v(i,j,k,:)*FY(i,j-st1:j+st2,k))+&
                   sum(div_w(i,j,k,:)*FZ(i,j,k-st1:k+st2)))
           end do
        end do
     end do
     
  end do
  
  ! Correct species diffusion to ensure sum(DFX)=0
  do k=kmin_-st1,kmax_+st2
     do j=jmin_-st1,jmax_+st2
        do i=imin_-st1,imax_+st2
           ! sum(DFX)
           tmp10(i,j,k)=sum(DFX(i,j,k,:))
           tmp11(i,j,k)=sum(DFY(i,j,k,:))
           tmp12(i,j,k)=sum(DFZ(i,j,k,:))
        end do
     end do
  end do
    
  do n=isc_1,isc_1+Nsp-1
     
     do k=kmin_-st1,kmax_+st2
        do j=jmin_-st1,jmax_+st2
           do i=imin_-st1,imax_+st2
              
              ! Diffusion correction: -Yi*sum(DFX)
              FX(i,j,k)=-sum(interp_sc_x(i,j,:)*SC(i-st2:i+st1,j,k,n))*tmp10(i,j,k)
              FY(i,j,k)=-sum(interp_sc_y(i,j,:)*SC(i,j-st2:j+st1,k,n))*tmp11(i,j,k)
              FZ(i,j,k)=-sum(interp_sc_z(i,j,:)*SC(i,j,k-st2:k+st1,n))*tmp12(i,j,k)
                            
              ! Update full diffusive flux
              DFX(i,j,k,n-isc_1+1)=DFX(i,j,k,n-isc_1+1)+FX(i,j,k)
              DFY(i,j,k,n-isc_1+1)=DFY(i,j,k,n-isc_1+1)+FY(i,j,k)
              DFZ(i,j,k,n-isc_1+1)=DFZ(i,j,k,n-isc_1+1)+FZ(i,j,k)
              
           end do
        end do
     end do
     
     ! Update species source term
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              src(i,j,k,n)=src(i,j,k,n)+dt*(&
                   sum(div_u(i,j,k,:)*FX(i-st1:i+st2,j,k))+&
                   sum(div_v(i,j,k,:)*FY(i,j-st1:j+st2,k))+&
                   sum(div_w(i,j,k,:)*FZ(i,j,k-st1:k+st2)))
           end do
        end do
     end do
     
  end do
  
  ! Form thermal diffusion correction - lambda/Cp^2*grad(Cpmix).grad(T)
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           src(i,j,k,isc_T)=src(i,j,k,isc_T)+dt*DIFF(i,j,k,isc_T)/Cpmix(i,j,k)*(&
                sum(grad_xm(i,j,:)*Cpmix(i-stp:i+stp,j,k))*sum(grad_xm(i,j,:)*SC(i-stp:i+stp,j,k,isc_T))+&
                sum(grad_ym(i,j,:)*Cpmix(i,j-stp:j+stp,k))*sum(grad_ym(i,j,:)*SC(i,j-stp:j+stp,k,isc_T))+&
                sum(grad_zm(i,j,:)*Cpmix(i,j,k-stp:k+stp))*sum(grad_zm(i,j,:)*SC(i,j,k-stp:k+stp,isc_T)))
        end do
     end do
  end do
  
  return
end subroutine finitechem_source_diff


! =========================== !
! Compute enthalpy flux terms !
! Cpmix was computed before   !
! =========================== !
subroutine finitechem_source_enth(src)
  use finitechem
  use masks
  use metric_generic
  use memory
  implicit none
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nscalar) :: src
  integer :: i,j,k,n
  real(WP) :: df1,df2,df3,Ttmp

  ! Form enthalpy flux
  FX=0.0_WP; FY=0.0_WP; FZ=0.0_WP
  do k=kmin_-st1,kmax_+st2
     do j=jmin_-st1,jmax_+st2
        do i=imin_-st1,imax_+st2
           
           ! Update hsp of species
           Ttmp=sum(interp_sc_x(i,j,:)*SC(i-st2:i+st1,j,k,isc_T))
           call compthermodata(hsp,Cpsp,Ttmp)
           
           ! Loop over species and compute enthalpy flux - sum(hi*(DF-lambda/Cp*grad(Yi)))
           do n=1,Nsp
              FX(i,j,k)=FX(i,j,k)+hsp(n)*(DFX(i,j,k,n)-&
                   sum(interp_sc_x(i,j,:)*DIFF(i-st2:i+st1,j,k,isc_T))*&
                   sum(grad_x(i,j,:)*SC(i-st2:i+st1,j,k,isc_1+n-1)))
           end do
           
           ! Update hsp of species
           Ttmp=sum(interp_sc_y(i,j,:)*SC(i,j-st2:j+st1,k,isc_T))
           call compthermodata(hsp,Cpsp,Ttmp)
           
           ! Loop over species and compute enthalpy flux - sum(hi*(DF-lambda/Cp*grad(Yi)))
           do n=1,Nsp
              FY(i,j,k)=FY(i,j,k)+hsp(n)*(DFY(i,j,k,n)-&
                   sum(interp_sc_y(i,j,:)*DIFF(i,j-st2:j+st1,k,isc_T))*&
                   sum(grad_y(i,j,:)*SC(i,j-st2:j+st1,k,isc_1+n-1)))
           end do
           
           ! Update hsp of species
           Ttmp=sum(interp_sc_z(i,j,:)*SC(i,j,k-st2:k+st1,isc_T))
           call compthermodata(hsp,Cpsp,Ttmp)
           
           ! Loop over species and compute enthalpy flux - sum(hi*(DF-lambda/Cp*grad(Yi)))
           do n=1,Nsp
              FZ(i,j,k)=FZ(i,j,k)+hsp(n)*(DFZ(i,j,k,n)-&
                   sum(interp_sc_z(i,j,:)*DIFF(i,j,k-st2:k+st1,isc_T))*&
                   sum(grad_z(i,j,:)*SC(i,j,k-st2:k+st1,isc_1+n-1)))
           end do
           
        end do
     end do
  end do
  
  ! Update temperature and enthalpy source terms
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           
           if (mask(i,j).eq.1) cycle
           
           ! Update Cp of species
           call compthermodata(hsp,Cpsp,SC(i,j,k,isc_T))
           
           ! Loop over species and compute temperature flux - sum(Cpsp*DF.grad(T))/Cpmix
           df1=0.0_WP; df2=0.0_WP; df3=0.0_WP
           do n=1,Nsp
              df1=df1+Cpsp(n)*sum(interp_sc_x(i,j,:)*DFX(i-st2:i+st1,j,k,n))
              df2=df2+Cpsp(n)*sum(interp_sc_y(i,j,:)*DFY(i,j-st2:j+st1,k,n))
              df3=df3+Cpsp(n)*sum(interp_sc_z(i,j,:)*DFZ(i,j,k-st2:k+st1,n))
           end do
                      
           ! Temperature source
           src(i,j,k,isc_T)=src(i,j,k,isc_T)+dt/Cpmix(i,j,k)*(&
                df1*sum(grad_xm(i,j,:)*SC(i-stp:i+stp,j,k,isc_T))+&
                df2*sum(grad_ym(i,j,:)*SC(i,j-stp:j+stp,k,isc_T))+&
                df3*sum(grad_zm(i,j,:)*SC(i,j,k-stp:k+stp,isc_T)))
           
           ! Enthalpy source
           src(i,j,k,isc_ENTH)=src(i,j,k,isc_ENTH)+dt*(&
                sum(div_u(i,j,k,:)*FX(i-st1:i+st2,j,k))+&
                sum(div_v(i,j,k,:)*FY(i,j-st1:j+st2,k))+&
                sum(div_w(i,j,k,:)*FZ(i,j,k-st1:k+st2)))
           
        end do
     end do
  end do
  
  return
end subroutine finitechem_source_enth


! ================================ !
! Finite rate chemistry monitoring !
! ================================ !
subroutine finitechem_monitor
  use finitechem
  use parallel
  use metric_generic
  use masks
  implicit none
  
  integer :: i,j,k,n
  real(WP) :: myCFLchem,myCFLsp
  
  ! Dump the quantities of interest in a monitor file
  call monitor_select_file('finite_chem')
  call monitor_set_single_value(1,Pthermo)
  call monitor_set_single_value(2,HRavg)
  call monitor_set_single_value(3,implicit_frac)
  
  ! Estimate chemical timestep
  myCFLchem=maxval(abs(SRCchem(imin_:imax_,jmin_:jmax_,kmin_:kmax_,NT))/SC(imin_:imax_,jmin_:jmax_,kmin_:kmax_,isc_T))
  myCFLsp=-huge(1.0_WP)
  do n=isc_1,isc_1+Nsp-1
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              if (SC(i,j,k,n).gt.0.0_WP) then
                 myCFLsp=max(myCFLsp,abs(SRCchem(i,j,k,n+1-isc_1)/SC(i,j,k,n)))
              end if
           end do
        end do
     end do
  end do
  call parallel_max(myCFLchem,CFLchem)
  call parallel_max(myCFLsp,CFLsp)
  call monitor_set_single_value(4,CFLchem)
  call monitor_set_single_value(5,CFLsp)
  
  ! Laminar flame speed
  if (simu_type .eq. 'flame speed') then
     call finitechem_flame_speed
     call monitor_select_file('flame_speed')
     call monitor_set_single_value(1,SLf)
  end if

  return
end subroutine finitechem_monitor


! ============================================================= !
! Check if the scalars are bounded within their physical limits !
! ============================================================= !
subroutine finitechem_check_bounds(SC_,isc,bounded)
  use finitechem
  implicit none
  
  real(WP), dimension(nscalar) :: SC_
  integer, intent(in)  :: isc
  integer, intent(out) :: bounded
  
  ! Assume boundedness
  bounded=1
  
  ! Different rules for temperature or species
  if (isc.eq.isc_T) then
     if (SC_(isc_T).lt.Tmin) bounded=0
     if (SC_(isc_T).gt.Tmax) bounded=0
  else
     if (SC_(isc).lt.0.0_WP) bounded=0
     if (SC_(isc).gt.1.0_WP) bounded=0
  end if
  
  return
end subroutine finitechem_check_bounds


! =================================== !
! Compute density from mass fractions !
! =================================== !
subroutine finitechem_density
  use finitechem
  use masks
  implicit none
  
  integer :: i,j,k
  real(WP):: Tmix,Wmix,u1,u2,fact
  real(WP), dimension(Nsp) :: SCmix

  ! Original version
  ! Compute the new density from the equation of state
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           if (mask(i,j).eq.1) then
              RHO(i,j,k)=1.0_WP
           else
              Tmix=min(max(SC(i,j,k,isc_T),Tmin),Tmax)
              call finitechem_W(SC(i,j,k,isc_1:isc_1+Nsp-1),Wmix)
              RHO(i,j,k)=Pthermo*Wmix/(Rcst*Tmix)
           end if
        end do
     end do
  end do
  call boundary_update_border(RHO,'+','ym')
  
  return
end subroutine finitechem_density


! =========================================== !
! Compute viscosity by fitting transport data !
! =========================================== !
subroutine finitechem_viscosity
  use finitechem
  use masks
  implicit none
  
  integer  :: i,j,k,sc1,sc2
  real(WP) :: Tmix,buf
  real(WP), dimension(Nsp) :: eta
  real(WP), dimension(Nsp,Nsp) :: phi

  ! Compute the new viscosity from Wilke's method
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           if (mask(i,j).eq.1) cycle
           
           ! Pure compounds viscosity
           Tmix=min(max(SC(i,j,k,isc_T),Tmin),Tmax)
        							
           call get_viscosity(eta,Tmix)

           ! Mixing coefficients
           do sc2=1,Nsp
              do sc1=1,Nsp
                 if (sc1.eq.sc2) then
                    phi(sc1,sc2)=1.0_WP
                 else
                    buf=sqrt(eta(sc1)/eta(sc2))*(Wsp(sc2)/Wsp(sc1))**0.25_WP
                    phi(sc1,sc2)=(1.0_WP+buf)**2/sqrt(8.0_WP+8.0_WP*Wsp(sc1)/Wsp(sc2))
                 end if
              end do
           end do

           ! Mixing rule
           VISC(i,j,k)=0.0_WP
           do sc1=1,Nsp
              if (SC(i,j,k,isc_1+sc1-1).le.0.0_WP) cycle
              buf=sum(SC(i,j,k,isc_1:isc_1+Nsp-1)*phi(sc1,:)/Wsp)
              VISC(i,j,k)=VISC(i,j,k)+SC(i,j,k,isc_1+sc1-1)*eta(sc1)/(Wsp(sc1)*buf)
           end do

        end do
     end do
  end do
  call boundary_update_border(VISC,'+','ym')
    
  return
end subroutine finitechem_viscosity


! ============================================= !
! Compute diffusivity by fitting transport data !
! ============================================= !
subroutine finitechem_diffusivity 
  use finitechem
  use masks
  implicit none

  integer  :: i,j,k,n,m,idiff
  real(WP) :: Wmix,Tmix,lambda
  real(WP) :: sum1,sum2,Cp_mix,sumY
  real(WP), dimension(Nsp) :: eta,cond,YoverW,Ys,sumDiff
  real(WP), dimension(Nsp,Nsp) :: invDij
  
  ! Compute the new diffusivity
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           if (mask(i,j).eq.1) cycle

           ! ---- Thermal diffusivity ---- !
           ! Mixture molar mass and temperature
           call finitechem_W(SC(i,j,k,isc_1:isc_1+Nsp-1),Wmix)
           Tmix=min(max(SC(i,j,k,isc_T),Tmin),Tmax)
           
           ! Individual compounds viscosity 
           call get_viscosity(eta,Tmix)

           ! Thermo properties
           call compthermodata(hsp,Cpsp,Tmix)

           ! Individual compounds viscosity 
           call get_conductivity(cond,Tmix,eta,Cpsp,Wsp)
           
           ! Mixture averaged thermal conductivity 
           sum1=Wmix*sum(SC(i,j,k,isc_1:isc_1+Nsp-1)/(cond*Wsp))
           sum2=Wmix*sum(SC(i,j,k,isc_1:isc_1+Nsp-1)*cond/Wsp)
           lambda=0.5_WP*(sum2+1.0_WP/sum1)
           
           ! Average Cp based on scalar field
           call finitechem_Cp(SC(i,j,k,isc_1:isc_1+Nsp-1),Tmix,Cp_mix)
           
           ! Thermal diffusivity for enthalpy
           DIFF(i,j,k,isc_ENTH)=lambda/Cp_mix
           
           ! Thermal diffusivity for temperature
           DIFF(i,j,k,isc_T)=lambda/Cp_mix
           
           ! ---- Species diffusivity ---- !
           if (use_lewis) then
              ! Lewis number approximation
              do n=1,Nsp
                 DIFF(i,j,k,isc_1+n-1)=lambda/(Cp_mix*Le(n))
              end do
           else
              ! Binary diffusion coefficients
              call get_invDij(invDij,Tmix,Pthermo)

              ! Constant terms
              sumY = 0.0_WP
              Ys = SC(i,j,k,isc_1:isc_1+Nsp-1)
              YOverW = Ys/Wsp
              sumY = sum(Ys)

              ! Compute mixture-average diffusion coefficient for each species
              do n=1,Nsp
                 ! Denominator
                 sumDiff(n) = sum(YOverW*invDij(n,:))
                 
                 if (sumDiff(n).gt.1.0e-15_WP) then
                    ! Diffusion is well defined
                    DIFF(i,j,k,isc_1+n-1) = (sumY-Ys(n)) / (Wmix*sumDiff(n));
                 else
                    ! Diffusion is ill defined
                    sumDiff(n) = sum(invDij(n,:)/Wsp)
                    DIFF(i,j,k,isc_1+n-1) = (real(Nsp-1,WP)) / (Wmix*sumDiff(n) )
                 end if
                 DIFF(i,j,k,isc_1+n-1) = RHO(i,j,k)*DIFF(i,j,k,isc_1+n-1)
              end do
              
           end if
           
           ! Set mixture fraction diffusivity equal to the thermal diffusivity
           if (isc_ZMIX.ne.0) DIFF(i,j,k,isc_ZMIX)=DIFF(i,j,k,isc_ENTH)
           
        end do
     end do
  end do
  
  do n=1,nscalar
     call boundary_update_border(DIFF(:,:,:,n),'+','ym')
  end do
  
  return
end subroutine finitechem_diffusivity


! =============================== !
! Compute scalar dissipation rate !
! =============================== !
subroutine finitechem_get_chi
  use finitechem
  implicit none
  
  ! Mixture Fraction CHI
  if (isc_ZMIX.eq.0) return
  call gradient_squared(SC(:,:,:,isc_ZMIX),CHI)
  CHI=2.0_WP*CHI*DIFF(:,:,:,isc_ZMIX)/RHO
  
  return
end subroutine finitechem_get_chi


! ========================================== !
! Compute average molecular weight (kg/kmol) !
! ========================================== !
subroutine finitechem_W(scalar,Wmix)
  use finitechem
  implicit none
  
  real(WP), dimension(Nsp), intent(in) :: scalar
  real(WP), intent(out) :: Wmix
  real(WP), dimension(Nsp) :: scalar_clip
  
  scalar_clip = min(max(scalar,0.0_WP),1.0_WP)
  Wmix = 1.0_WP/sum(scalar_clip/Wsp)
 
  return
end subroutine finitechem_W


! ================================================ !
! Compute the average Cp in the mixture (J/(kg.K)) !
! ================================================ !
subroutine finitechem_Cp(scalar,Tmix,Cp_mix)
  use finitechem
  implicit none
  real(WP), dimension(Nsp), intent(in) :: scalar
  real(WP), intent(in) :: Tmix
  real(WP), intent(out) :: Cp_mix
  
  call compthermodata(hsp,Cpsp,Tmix)
  Cp_mix=sum(scalar*Cpsp)
  
  return
end subroutine finitechem_Cp

! ======================= !
! Flame speed computation !
! ======================= !
subroutine finitechem_flame_speed
  use finitechem
  use masks
  use time_info
  implicit none
  integer  :: i,j
  real(WP) :: tmp,alpha
  real(WP) :: a11,a12,a22,y1,y2
  real(WP) :: rhomax,Yfmax,myYf
  
  ! Get max density
  rhomax = -huge(1.0_WP)
  do i=imin_,imax_
     if (mask(i,jmin_).eq.0) then
        if (RHO(i,jmin_,kmin_).gt.rhomax) rhomax = RHO(i,jmin_,kmin_)
     end if
  end do
  call parallel_max(rhomax,tmp); rhomax=tmp

  ! Get max fuel mass fraction
  Yfmax = -huge(1.0_WP)
  do i=imin_,imax_
     if (mask(i,jmin_).ne.0) cycle
     myYF = 0.0_WP
     do j=1,nf
        myYF = myYF + SC(i,jmin_,kmin_,isc_1+ispf(j)-1)
     end do
     if (myYF.gt.Yfmax) Yfmax = myYF
  end do
  call parallel_max( Yfmax, tmp); Yfmax =  tmp
  
  ! Compute flame speed
  SLf = -100.0_WP*srcF/(rhomax*Yfmax)
  
  return
end subroutine finitechem_flame_speed
