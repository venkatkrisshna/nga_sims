module combustion
  use precision
  use partition
  use data
  implicit none
  
  ! Chemistry model
  character(len=str_medium) :: chemistry
  
  ! Default values in the case constant properties
  real(WP), dimension(:), allocatable :: rho_input
  real(WP), dimension(:), allocatable :: visc_input
  real(WP), dimension(:), allocatable :: diff_input
  real(WP), dimension(:), allocatable :: T_input
  
  ! Density evaluation
  logical :: density_adv        ! Advance density inversion
  logical :: rescale_scalars    ! Rescale scalars to conserve RHO*SC
  integer :: nfilter            ! Number of filtering steps of dRHO
  
  ! Sum of dRHO for boundary
  real(WP) :: sum_dRHO
  
  ! Thermodynamic pressure (background pressure) => Pthermo(t) only
  real(WP) :: Pthermo,Pthermo_old
  
  ! Internal Energy - Spurious heat release
  real(WP) :: Eint
  
  ! For constant volume
  logical  :: constant_volume
  real(WP) :: RHO_0
  
  ! Simple links to transported scalars
  integer :: isc_ZMIX     ! Mixture Fraction
  integer :: isc_ZMIX2    ! Mixture Fraction Squared (for the variance)
  integer :: isc_ZVAR     ! Mixture Fraction Variance
  integer :: isc_PROG     ! Progress Variable
  integer :: isc_T        ! Temperature
  integer :: isc_ENTH     ! Enthalpy
  
  ! Molecular mass
  real(WP), dimension(:,:,:), allocatable :: Wmol
  ! Temperature
  real(WP), dimension(:,:,:), allocatable :: T
  ! Scalar Dissipation Rate
  real(WP), dimension(:,:,:), allocatable :: CHI
  ! Variance of mixture fraction
  real(WP), dimension(:,:,:), allocatable :: ZVAR
  ! Variance of progress variable
  real(WP), dimension(:,:,:), allocatable :: CVAR
  
  ! Values to monitor
  real(WP) :: min_T,max_T,min_rho,max_rho
  integer  :: clip_drho
  
contains

  ! Compute the variance of the mixture fraction
  ! --------------------------------------------
  subroutine combustion_ZVAR(SCnow)
    implicit none
    
    integer :: i,j,k
    real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nscalar) :: SCnow
    
    ! If no Mixture fraction return
    if (isc_ZMIX.eq.0) return
    
    ! Preset to zero
    ZVAR = 0.0_WP
    
    if (isc_ZVAR.ne.0) then
       ! If ZVAR is present, use it
       ZVAR(:,:,:) = SCnow(:,:,:,isc_ZVAR)
    else if (isc_ZMIX2.ne.0) then
       ! If Z^2 is present, use it to get ZVAR
       do k=kmino_,kmaxo_
          do j=jmino_,jmaxo_
             do i=imino_,imaxo_
                ZVAR(i,j,k) = SCnow(i,j,k,isc_ZMIX2) - SCnow(i,j,k,isc_ZMIX)**2
             end do
          end do
       end do
    else
       ! Otherwise compute with dynamic model - newest SC used ! CHANGE HERE
       call sgsmodel_ZVAR(ZVAR,isc_ZMIX)
    end if
    
    ! Clip the computed Variance
    ZVAR = max(0.0_WP,min(SCnow(:,:,:,isc_ZMIX)*(1.0_WP-SCnow(:,:,:,isc_ZMIX)), ZVAR))
    
    return
  end subroutine combustion_ZVAR
  
  ! Compute the scalar dissipation rate
  ! chi = 2.0 * (D+Dt) * |grad(Z)|^2 
  ! -----------------------------------
  subroutine combustion_CHI
    implicit none
    
    CHI = 0.0_WP
    
    ! Compute CHI from mixture fraction if we have it
    if (isc_ZMIX.ne.0) then
       call gradient_squared(SC(:,:,:,isc_ZMIX),CHI)
       CHI = 2.0_WP*CHI*DIFF(:,:,:,isc_ZMIX)/RHO
    else
       if (trim(chemistry).eq.'one-step') call onestep_get_chi
       if (trim(chemistry).eq.'finite chem') call finitechem_get_chi
    end if
    
    return
  end subroutine combustion_CHI
  
  ! Compute change of mass inside the domain
  ! ----------------------------------------
  subroutine combustion_sum_drho
    use parallel
    use masks
    implicit none
    real(WP) :: tmp
    integer :: i,j,k
    
    tmp = 0.0_WP
    do k=kmin_,kmax_
       do j=jmin_,jmax_
          do i=imin_,imax_
             if (mask(i,j).ne.1.and.mask(i,j).ne.3) then
                tmp = tmp + dRHO(i,j,k)*vol(i,j,k)
             end if
          end do
       end do
    end do
    
    call parallel_sum(tmp,sum_dRHO)
    
    return
  end subroutine combustion_sum_drho
  
  ! Compute mean density
  ! --------------------
  subroutine combustion_mean_density(RHOmean)
    use parallel
    use time_info
    implicit none
    
    real(WP), intent(out) :: RHOmean
    real(WP) :: my_RHOmean,evap_mass
    integer  :: i,j,k
    
    ! Recompute mean density
    my_RHOmean = 0.0_WP
    do k=kmin_,kmax_
       do j=jmin_,jmax_
          do i=imin_,imax_
             my_RHOmean = my_RHOmean + RHO(i,j,k)*vol(i,j,k)
          end do
       end do
    end do
    call parallel_sum(my_RHOmean,RHOmean)
    RHOmean = RHOmean/vol_total
    
    ! Particles
    !if (use_lpt .and. niter.eq.1) then
    !   call lpt_get_evap_mass(evap_mass)
    !   RHOmean = RHOmean + evap_mass/vol_total
    !end if
    
    return
  end subroutine combustion_mean_density
  
  ! Rescale density to ensure continuity
  ! ------------------------------------
  subroutine combustion_rescale_density
    use masks
    implicit none
    
    real(WP) :: RHOmean
    integer  :: i,j,k
    
    ! If not constant volume return
    if (.not.constant_volume .or. trim(chemistry).eq.'none') return
    
    ! Recompute mean density
    call combustion_mean_density(RHOmean)
    
    ! Update Pthermo
    Pthermo = Pthermo*RHO_0/RHOmean
    
    ! Update density
    do k=kmino_,kmaxo_
       do j=jmino_,jmaxo_
          do i=imino_,imaxo_
             if (mask(i,j).ne.1 .and. mask(i,j).ne.3 .and. vol(i,j,k).gt.0.0_WP) RHO(i,j,k) = RHO(i,j,k) * (RHO_0/RHOmean)
          end do
       end do
    end do
    
    return
  end subroutine combustion_rescale_density
  
  ! Solve for the internal energy
  ! -----------------------------
  subroutine combustion_internal_energy
    use metric_velocity_conv
    use time_info
    use parallel
    implicit none
    
    integer  :: i,j,k
    real(WP) :: rhs,tmp
    
    tmp = 0.0_WP
    do k=kmin_,kmax_
       do j=jmin_,jmax_
          do i=imin_,imax_
             tmp = tmp - vol(i,j,k)*P(i,j,k)*( &
                  + sum(divc_u(i,j,k,:)*U(i-stc1:i+stc2,j,k)) &
                  + sum(divc_v(i,j,k,:)*V(i,j-stc1:j+stc2,k)) &
                  + sum(divc_w(i,j,k,:)*W(i,j,k-stc1:k+stc2)) )
          end do
       end do
    end do
    call parallel_sum(tmp,rhs)
    Eint = Eint + dt_uvw*rhs
    
    return
  end subroutine combustion_internal_energy
  
  ! Interpolate the dRHO for large values of dRHO
  ! ---------------------------------------------
  subroutine combustion_adv_dRHO
    use parallel
    use masks
    implicit none
    
    integer :: i,j,k,mycount
    real(WP) :: tmp1,tmp2,sum1,sum2,sdt
    
    tmp1 = 0.0_WP
    tmp2 = 0.0_WP
    do k=kmin_,kmax_
       do j=jmin_,jmax_
          do i=imin_,imax_
             tmp1 = tmp1 + dRHO(i,j,k)
             tmp2 = tmp2 + dRHO(i,j,k)**2
          end do
       end do
    end do
    call parallel_sum(tmp1,sum1)
    call parallel_sum(tmp2,sum2)
    sum1 = sum1/real(nx*ny*nz,WP)
    sum2 = sum2/real(nx*ny*nz,WP)
    sdt = sqrt(sum2-sum1**2)
    
    mycount = 0
    do k=kmin_,kmax_
       do j=jmin_,jmax_
          do i=imin_,imax_
             if (mask(i,j).eq.0 .and. abs(dRHO(i,j,k)-sum1).gt.5.0_WP*sdt) then
                mycount = mycount + 1
                call filter_local_3D(dRHO(i-1:i+1,j-1:j+1,k-1:k+1),tmp1,i,j,'n')
                dRHO(i,j,k) = tmp1
             end if
          end do
       end do
    end do
    call parallel_sum(mycount,clip_drho)
    
    ! Update boundaries
    call boundary_update_border(dRHO,'+','ym')
    
    return
  end subroutine combustion_adv_dRHO
  
end module combustion


! ============================================================ !
! Initialize the combustion module                             !
!                                                              ! 
! -> initialize combustion model                               !
! -> allocate the arrays                                       !
! -> update ghost cells                                        !
! -> apply boundary conditions                                 !
!                                                              !
! Before: RHO or RHOold or dRHO correct only inside the domain !
!         -> imin_:imax_,jmin_:jmax_,kmin_:kmax_               !
! After : RHO and RHOold and dRHO correct everywhere           !
!         -> imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_         !
! ============================================================ !
subroutine combustion_init
  use combustion
  use memory
  use parser
  use borders
  implicit none
  integer :: isc,n
  
  ! Create & Start the timer
  call timing_create('combustion')
  call timing_start ('combustion')

  ! Allocate some properties arrays
  allocate(Wmol(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(T   (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(CHI (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(ZVAR(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(CVAR(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  CVAR = 0.0_WP
    
  ! Link the variables to the given scalars
  isc_ZMIX  = 0
  isc_ZMIX2 = 0
  isc_ZVAR  = 0
  isc_PROG  = 0
  isc_T     = 0
  isc_ENTH  = 0
  
  do isc=1,nscalar
     select case(trim(SC_name(isc)))
     case ('ZMIX')
        isc_ZMIX = isc
     case ('ZMIX^2')
        isc_ZMIX2 = isc
     case ('ZVAR')
        isc_ZVAR = isc
     case ('PROG')
        isc_PROG = isc
     case ('T')
        isc_T = isc
     case ('ENTHALPY')
        isc_ENTH = isc
     end select
  end do
  
  ! Prepare the arrays to store constant properties
  n = 1
  if (xper.eq.1) n = ninlet
  
  ! Allocate the arrays
  allocate(rho_input (n))
  allocate(visc_input(n))
  allocate(diff_input(n))
  allocate(T_input   (n))
  
  ! Initialize thermodynamic pressure
  call parser_read('Pressure',Pthermo,1.0133e5_WP)
  Pthermo_old = Pthermo
  
  ! Read the chemistry model from the input file
  call parser_read('Chemistry model',chemistry)
  select case(trim(chemistry))
     
  case ('none')
     ! Read default values
     call parser_read('Density',rho_input)
     call parser_read('Viscosity',visc_input)
     call parser_read('Diffusivity',diff_input)
     call parser_read('Temperature',T_input)
     ! Precompute density
     call combustion_density
     combust = .false.
     
  case ('diffusion')
     ! Initialize diffusion combustion
     call diffusion_init
     combust = .true.
     
  case ('premixed')
     ! Initialize premixed combustion
     call premixed_init 
     combust = .true.
     
  case ('one-step')
     ! Initialize one-step chemistry
     call onestep_init
     combust = .true.
     
  case ('finite chem')
     ! Initialize finite rate chemistry
     call finitechem_init
     ! Recompute density 
     call combustion_density
     combust = .true.
     
  case ('multiphase')
     if (use_multiphase) then
        ! Multiphase simulation
        combust = .false.
        ! Obtain density
        call multiphase_density
     else
        call die('combustion_init: multiphase model requires (use multiphase)=.true.')
     end if
     
  case default
     call die('combustion_init: unknown chemistry model')
  end select
  
  ! Read in additional parameters
  call parser_read('Rescale scalars',rescale_scalars,.false.)
  call parser_read('Adv. density inversion',density_adv,.false.)
  call parser_read('dRHO filtering',nfilter,0)
  
  ! Initialize the variance
  if (nscalar.gt.0) call combustion_ZVAR(SC)
  
  ! Compute viscosity and diffusivity
  call combustion_viscosity
  call combustion_diffusivity
  
  ! If the arrays were not present in the input file, update them
  if (.not. rho_present)  then
     call combustion_density
  else
     tmp1 = RHO
     call combustion_density
     RHO(imin_:imax_,jmin_:jmax_,kmin_:kmax_) = tmp1(imin_:imax_,jmin_:jmax_,kmin_:kmax_)
  end if
  if (.not. drho_present) dRHO = 0.0_WP

  ! Update all the borders
  call boundary_update_border(RHO ,'+','ym')
  call boundary_update_border(dRHO,'+','ym')
  
  ! Recompute the old density
  RHOold = RHO - dRHO
  
  ! Compute the initial temperature
  call combustion_temperature
  
  ! Detect if closed/constant volume
  if (xper.eq.1 .or. (ninlet.eq.0 .and. noutlet.eq.0)) then
     constant_volume = .true.
     call combustion_mean_density(RHO_0)
  else
     constant_volume = .false.
  end if
  
  if (combust) then
     ! Set internal energy to zero
     Eint = 0.0_WP
     
     ! Create a new file to monitor at each timesteps
     call monitor_create_file_step('combustion',6)
     call monitor_set_header (1,'min_T','r')
     call monitor_set_header (2,'max_T','r')
     call monitor_set_header (3,'min_rho','r')
     call monitor_set_header (4,'max_rho','r')
     call monitor_set_header (5,'Pthermo','r')
     call monitor_set_header (6,'Eint','r')
     
     ! Create a new file to monitor at each subiteration
     call monitor_create_file_iter('convergence_combustion',2)
     call monitor_set_header (1,'res_RHO','r')
     call monitor_set_header (2,'clip_dRHO','i')
  end if
  
  ! Stop a timer
  call timing_stop('combustion')
  
  return
end subroutine combustion_init


! ==================================================== !
! Precompute some quantities for the combustion module !
! ==================================================== !
subroutine combustion_prestep
  use combustion
  implicit none
  
  ! Start a timer
  call timing_start('combustion')
  
  ! Compute Viscosity and Diffusivity
  call combustion_viscosity
  call combustion_diffusivity
  
  if (nscalar.ne.0) then
     ! Compute ZVAR and CHI
     call combustion_ZVAR(SC)
     call combustion_CHI
  end if
  
  ! Save the old density
  RHOold = RHO
  
  ! Save the old background pressure
  Pthermo_old = Pthermo
  
  ! Stop a timer
  call timing_stop('combustion')

  return
end subroutine combustion_prestep


! =========================================== !
! Compute the main quantities for the solvers !
! And some monitoring informations            !
! =========================================== !
subroutine combustion_step
  use combustion
  use masks
  use parallel
  use memory
  implicit none
  integer  :: i,j,k
  real(WP) :: max_dRHO

  ! Start a timer
  call timing_start('combustion')
  
  ! Save current density
  tmp1 = RHO
  
  ! Compute the density
  call combustion_density
  
  ! Compute change in density
  dRHO = RHO - RHOold
  
  ! Filter density changes
  do i=1,nfilter
     call filter_global_3D(dRHO,dRHO,'+','n')
  end do
  if (nfilter.eq.-1) call combustion_adv_dRHO
  
  ! Set zero in the walls
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           if (vol(i,j,k).eq.0.0_WP) dRHO(i,j,k) = 0.0_WP
        end do
     end do
  end do
  
  ! Recompute new density
  RHO = RHOold + dRHO
  
  ! Rescale the pressure to ensure continuity
  call combustion_rescale_density
  
  ! Rescale the scalars
  if (nscalar.gt.0 .and. rescale_scalars) then
     do k=kmino_,kmaxo_
        do j=jmino_,jmaxo_
           do i=imino_,imaxo_
              SC(i,j,k,:) = SC(i,j,k,:) * tmp1(i,j,k)/RHO(i,j,k)
           end do
        end do
     end do
  end if
  
  ! Compute change in density
  dRHO = RHO - RHOold
  
  ! Compute change of RHO over iteration
  tmp1 = tmp1 - RHO
  
  ! Compute total change of mass in volume
  call combustion_sum_drho
  
  ! Compute temperature for output
  call combustion_temperature
  
  ! Set the monitor values
  if (combust) then
     call parallel_max(maxval(abs(tmp1)),max_dRHO)
     call monitor_select_file('convergence_combustion')
     call monitor_set_single_value(1,max_dRHO)
     call monitor_set_single_value(2,real(clip_dRHO,WP))
  end if
  
  ! Stop a timer
  call timing_stop('combustion')

  return
end subroutine combustion_step


! ================================================================== !
! Compute the chemical related source terms for the scalar equations !
! -> Computed as src = src(t=dt/2) from 0.5*(SC+SCold)               !
! ================================================================== !
subroutine combustion_source_scalar_mid(src)
  use combustion
  use sgsmodel
  use time_info
  use strainrate
  use filter
  use memory
  implicit none
  
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nscalar) :: src
  integer  :: i,j,k
  
  ! Start a timer
  call timing_start('combustion')
  
  ! Source term for Z^2 equation
  if (isc_ZMIX2.ne.0 .and. use_sgs) then
     call gradient_squared(SC(:,:,:,isc_ZMIX),tmp1)
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ! Dissipation term for Z^2: L(Z^2) = -rho*chi -rho*chi_sgs
              src(i,j,k,isc_ZMIX2) = src(i,j,k,isc_ZMIX2) &
                   -dt*2.0_WP*tmp1(i,j,k)*DIFFmol(i,j,k,isc_ZMIX) &
                   -dt*2.0_WP*0.55_WP*0.55_WP*S(i,j,k)*ZVAR(i,j,k)*(0.5_WP*(RHOold(i,j,k)+RHO(i,j,k)))
           end do
        end do
     end do
  end if
  
  ! Source term for ZVAR equation
  if (isc_ZVAR.ne.0 .and. use_sgs) then
     call gradient_squared(SC(:,:,:,isc_ZMIX),tmp1)
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              ! Should check that
              src(i,j,k,isc_ZVAR) = src(i,j,k,isc_ZVAR) &
                   - 2.0_WP*dt*DIFF(i,j,k,isc_ZMIX)*ZVAR(i,j,k)/delta_3D(i,j)**2 &
                   + 2.0_WP*dt*DIFF(i,j,k,isc_ZMIX)*tmp1(i,j,k)
           end do
        end do
     end do
  end if
  
  ! Source term for progress variable equation
  if (isc_PROG.ne.0) then
     
     ! Purely diffusion flames
     if (trim(chemistry).eq.'diffusion') then
        call diffusion_source_prog(src)
     end if
     
     ! Purely premixed flames
     if (trim(chemistry).eq.'premixed') then
        call premixed_source_prog(src)
     end if
  end if
  
  ! Source term for mixture fraction
  if (isc_ZMIX.ne.0) then
     ! Purely premixed flames
     if (trim(chemistry).eq.'premixed') then
        call premixed_source_zmix(src)
     end if
  end if
  
  ! Source term for temperature equation
  if (trim(chemistry).eq.'one-step') then
     call onestep_source_pressure (src)
     call onestep_source_entflux  (src)
     call onestep_source_radiation(src)
  end if
  
  ! Source term for the finite rate chemistry case
  if (trim(chemistry).eq.'finite chem') then
     call finitechem_source_mid(src)
  end if
  
  ! Stop a timer
  call timing_stop('combustion')

  return
end subroutine combustion_source_scalar_mid


! ================================================================== !
! Compute the chemical related source terms for the scalar equations !
! -> Computed as src = int(src,t=0..dt) from SCold                   !
! ================================================================== !
subroutine combustion_source_scalar(src)
  use combustion
  use sgsmodel
  use time_info
  implicit none
  
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nscalar) :: src
  
  ! Start a timer
  call timing_start('combustion')
  
  ! Source terms for one-step chemistry
  if (trim(chemistry).eq.'one-step') then
     call onestep_source_chemistry(src)  
  end if
  
  ! Source term for finite rate chemistry case
  if (trim(chemistry).eq.'finite chem') then
     call finitechem_source_chem
  end if
  
  ! Stop a timer
  call timing_stop('combustion')
  
  return
end subroutine combustion_source_scalar


! ========================================================= !
! Check if a quantity is bounded within its physical bounds !
! ========================================================= !
subroutine combustion_check_bounds(SC_,isc,bounded)
  use combustion
  implicit none
  
  real(WP), dimension(nscalar), intent(in) :: SC_
  integer,  intent(in)  :: isc
  integer,  intent(out) :: bounded
  real(WP) :: sc_min,sc_max

  ! No timer here, because too many calls
  
  ! ---- Simple bounds checking ----
  sc_min = -huge(1.0_WP)
  sc_max = +huge(1.0_WP)

  ! Mixture fraction - relaxed for fewer out off bounds
  if (isc.eq.isc_ZMIX) then
     sc_min = -0.01_WP !0.0_WP
     sc_max =  1.01_WP !1.0_WP
  end if
  ! Mixture fraction squared
  if (isc.eq.isc_ZMIX2) then
     sc_min = SC_(isc_ZMIX)**2
     sc_max = SC_(isc_ZMIX)
  end if
  ! Mixture fraction Variance
  if (isc.eq.isc_ZVAR) then
     sc_min = 0.0_WP
     sc_max = SC_(isc_ZMIX)*(1.0_WP-SC_(isc_ZMIX))
  end if
  ! Progress Variable
  if (isc.eq.isc_PROG) then
     if (trim(chemistry).eq.'diffusion') then
        call diffusion_chemtable_prog_minmax(SC_(isc_ZMIX),0.0_WP,sc_min,sc_max)
        sc_min = -0.01_WP
     elseif (trim(chemistry).eq.'premixed') then
        call premixed_chemtable_prog_minmax(SC_(isc_ZMIX),sc_min,sc_max)
        sc_min = sc_min - 0.01_WP*(sc_max-sc_min)
        sc_max = sc_max + 0.01_WP*(sc_max-sc_min)
     else
        sc_min = 0.0_WP
     end if
  end if
  
  ! Check the bounds
  bounded = 1
  if (SC_(isc).lt.sc_min) bounded = 0
  if (SC_(isc).gt.sc_max) bounded = 0
  
  ! ---- More advanced bounds checking ----
  if (trim(chemistry).eq.'one-step')    call onestep_check_bounds(SC_,isc,bounded)
  if (trim(chemistry).eq.'finite chem') call finitechem_check_bounds(SC_,isc,bounded)
  
  return
end subroutine combustion_check_bounds


! ============================== !
! Compute the Density everywhere !
! Including the ghost cells      !
! ============================== !
subroutine combustion_density
  use combustion
  use borders
  implicit none
  integer :: j,nflow
  
  select case(trim(chemistry))
  case ('none')
     ! Constant density from input file
     if (xper.eq.1) then
        do nflow=1,ninlet
           do j=max(inlet(nflow)%jmino,jmino_),min(inlet(nflow)%jmaxo,jmaxo_)
              RHO(:,j,:) = rho_input(nflow)
           end do
        end do
     else
        RHO = rho_input(1)
     end if
     
  case ('diffusion')
     ! Get density from diffusion table
     call diffusion_lookup('RHO',RHO)
     
  case ('premixed')
     ! Get density from premixed table
     call premixed_lookup('RHO', RHO)
     
  case ('multiphase')
     ! Get density from multiphase
     call multiphase_density
     
  case ('one-step')
     ! Get density from the equation of state
     call onestep_density
     
  case ('finite chem')
     ! Get density from the equation of state
     call finitechem_density  
     
  end select
  
  ! Take into account the effect of fluid
  ! volume fraction by rescaling density
  ! This treatment is strange, but correct
  ! We should rewrite using epsp instead of epsf...
  RHO = RHO*(epsf+poros-1.0_WP)
  
  return
end subroutine combustion_density


! ================================ !
! Compute the Viscosity everywhere !
! Including the ghost cells        !
! ================================ !
subroutine combustion_viscosity
  use combustion
  use borders
  implicit none
  integer :: j,nflow
  
  select case(trim(chemistry))
  case ('none')
     ! Constant viscosity from input file
     if (xper.eq.1) then
        do nflow=1,ninlet
           do j=max(inlet(nflow)%jmino,jmino_),min(inlet(nflow)%jmaxo,jmaxo_)
              VISC(:,j,:) = visc_input(nflow)
           end do
        end do
     else
        VISC = visc_input(1)
     end if
     
  case ('diffusion')
     ! Get viscosity from diffusion table
     call diffusion_lookup('VISC',VISC)
     
  case ('premixed')
     ! Get viscosity from premixed table
     call premixed_lookup('VISC',VISC)
     
  case ('multiphase')
     ! Get viscosity from multiphase
     call multiphase_viscosity
     
  case ('one-step')
     ! Viscosity from Sutherland's law
     call onestep_viscosity
     
  case ('finite chem')
     ! Get viscosity by fitting chemkin data
     call finitechem_viscosity          
     
  end select
  
  ! Store molecular viscosity
  VISCmol = VISC
  
  ! Effective viscosity
  if (use_lpt) VISC=VISC+VISCmol*(epsf**(-2.8_WP)-1.0_WP)
  
  return
end subroutine combustion_viscosity


! ================================== !
! Compute the Diffusivity everywhere !
! Including the ghost cells          !
! ================================== !
subroutine combustion_diffusivity
  use combustion
  use memory
  use borders
  implicit none
  integer :: isc,nflow
  integer :: i,j,k
  
  ! Return if no scalar
  if (nscalar.eq.0) return

  select case(trim(chemistry))
  case ('none')
     ! Constant diffusivity from input file
     if (xper.eq.1) then
        do nflow=1,ninlet
           do j=max(inlet(nflow)%jmino,jmino_),min(inlet(nflow)%jmaxo,jmaxo_)
              DIFF(:,j,:,:) = diff_input(nflow)
           end do
        end do
     else
        DIFF = diff_input(1)
     end if
     
  case ('diffusion')
     ! Get diffusivity from diffusion table
     call diffusion_lookup('DIFF',tmp1)
     
     ! Same diffusivity for all scalars
     do isc=1,nscalar
        DIFF(:,:,:,isc) = tmp1
     end do
     
  case ('premixed')
     ! Get diffusivity from premixed table
     call premixed_lookup('DIFF',tmp1)
     
     ! Same diffusivity for all scalars
     do isc=1,nscalar
        DIFF(:,:,:,isc) = tmp1
     end do
     
     ! Correct diffusivity for non-unity Lewis numbers
     call premixed_diffusivity_update
     
  case ('multiphase')
     ! Nothing for now
     
  case ('one-step')
     ! Diffusivity from constant Prandtl number
     call onestep_diffusivity

  case ('finite chem') 
     ! Get diffusivity by fitting chemkin data
     call finitechem_diffusivity  

  end select
  
  ! Store molecular diffusivity
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           DIFFmol(i,j,k,:) = DIFF(i,j,k,:)
        end do
     end do
  end do
  
  return
end subroutine combustion_diffusivity


! ===================================== !
! Compute the Temperature by everywhere !
! Including the ghost cells             !
! ===================================== !
subroutine combustion_temperature
  use combustion
  use borders
  implicit none
  integer :: j,nflow
  
  select case(trim(chemistry))
  case ('none')
     ! Constant temperature from input file
     if (xper.eq.1) then
        do nflow=1,ninlet
           do j=max(inlet(nflow)%jmino,jmino_),min(inlet(nflow)%jmaxo,jmaxo_)
              T(:,j,:) = T_input(nflow)
           end do
        end do
     else
        T = T_input(1)
     end if
     
  case ('diffusion')
     ! Get temperature from diffusion table
     call diffusion_lookup('T',T)
     
  case ('premixed')
     ! Get temperature from premixed table
     call premixed_lookup('T',T)
     
  case ('multiphase')
     ! Nothing for now
     
  case ('one-step')
     ! Get temperature from transported scalar
     T(:,:,:) = SC(:,:,:,isc_T)
     
  case ('finite chem')
     ! Get temperature from transported scalar
     T(:,:,:) = SC(:,:,:,isc_T)   
     
  end select
  
  return
end subroutine combustion_temperature


! ========================== !
! Ignite the flow everywhere !
! ========================== !
subroutine combustion_ignite
  use combustion
  implicit none
  
  ! Diffusion specific ignition
  if (trim(chemistry).eq.'diffusion') then
     call diffusion_ignite
  end if
  
  ! Premixed specific ignition
  if (trim(chemistry).eq.'premixed') then
     call premixed_ignite
  end if
  
  ! Recompute density
  call combustion_density
  
  return
end subroutine combustion_ignite


! ============================= !
! Monitor the combustion module !
! ============================= !
subroutine combustion_monitor
  use combustion
  use masks
  implicit none
  
  integer  :: i,j,k
  real(WP) :: min_rho_,max_rho_,min_T_,max_T_
  
  ! Nothing to do if constant density
  if (.not.combust) return
  
  ! Start a timer
  call timing_start('combustion')
  
  ! Min and max of rho and T
  min_rho_ = +huge(1.0_WP)
  max_rho_ = -huge(1.0_WP)
  min_T_   = +huge(1.0_WP)
  max_T_   = -huge(1.0_WP)

  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (mask(i,j).ne.1) then
              if (RHO(i,j,k).lt.min_rho_) min_rho_ = RHO(i,j,k)
              if (RHO(i,j,k).gt.max_rho_) max_rho_ = RHO(i,j,k)
              if (T(i,j,k)  .lt.min_T_)   min_T_   = T(i,j,k)
              if (T(i,j,k)  .gt.max_T_)   max_T_   = T(i,j,k)
           end if
        end do
     end do
  end do
  call parallel_min(min_rho_,min_rho)
  call parallel_max(max_rho_,max_rho)
  call parallel_min(min_T_,  min_T)
  call parallel_max(max_T_,  max_T)
  
  ! Compute internal energy
  call combustion_internal_energy
  
  ! Transfer values to monitor
  call monitor_select_file('combustion')
  call monitor_set_single_value(1,min_T)
  call monitor_set_single_value(2,max_T)
  call monitor_set_single_value(3,min_rho)
  call monitor_set_single_value(4,max_rho)
  call monitor_set_single_value(5,Pthermo)
  call monitor_set_single_value(6,Eint)
    
  ! Stop a timer
  call timing_stop('combustion')
  
  return
end subroutine combustion_monitor
