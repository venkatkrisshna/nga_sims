!===============================================================================
!===============================================================================

module constant_volume
   use combustion
   implicit none

   ! Thermo data
   real(WP), parameter :: R_cst    = 8314.0_WP      ! [J/(kmol.K)]    
   real(WP), parameter :: Cp       = 1200.0_WP      ! [J/(kg.K)]

   ! Volume data
   real(WP)            :: vol_0

   ! Equivalence ratio
   real(WP)            :: global_phi

   real(WP)            :: Ma_delta, Ze_num

end module constant_volume

!===============================================================================
!===============================================================================

subroutine constant_volume_init
  use constant_volume
  implicit none

  call parser_read('Equivalence ratio',global_phi)

  return
end subroutine constant_volume_init

!===============================================================================
!===============================================================================

subroutine get_RHO_0
   use constant_volume
   use masks
   implicit none
   real(WP) :: tmp, tmp_vol
   integer  :: i,j,k

   ! first compute the density
   !call premixed_density

   tmp = 0.0_WP
   tmp_vol = 0.0_WP
   do k = kmin_,kmax_
      do j = jmin_,jmax_
         do i = imin_,imax_
            if (mask(i,j).eq.0) then
               tmp = tmp + RHO(i,j,k)*vol(i,j) 
               tmp_vol = tmp_vol + vol(i,j)
            end if
         end do
      end do
   end do
   call parallel_sum(tmp,RHO_0)
   call parallel_sum(tmp_vol,vol_0)
   RHO_0 = RHO_0 / vol_0
   if (irank.eq.iroot) write(*,*) 'RHO_0 = ',RHO_0
   if (irank.eq.iroot) write(*,*) 'vol_0 = ',vol_0

   return
end subroutine get_RHO_0

!===============================================================================
!===============================================================================

subroutine update_pressure
   use constant_volume
   use masks
   implicit none
   integer  :: i,j,k
   real(WP) :: tmp
   real(WP) :: W_u, W_b, Wtot, WoT
   real(WP) :: p_u, p_b, T_max

   W_u = 24.2_WP  !kg/kmol
   W_b = 26.1_WP  !kg/kmol
   
   T_max = 839.37_WP+3057.2_WP*(global_phi)-1177.9_WP*(global_phi)**(2.0_WP)

   ! Get Pmean
   tmp = 0.0_WP
   do k=kmin_,kmax_
      do j=jmin_,jmax_
         do i=imin_,imax_
            if (mask(i,j).eq.0) then
               p_b = min(max((SC(i,j,k,isc_T)-300.0_WP)/T_max,0.0_WP),1.0_WP)
               p_u = 1.0_WP - p_b
               Wtot = W_u*p_u + W_b*p_b
               tmp = tmp + Wtot/SC(i,j,k,isc_T)*vol(i,j)
            end if
         end do
      end do
   end do
   call parallel_sum(tmp,WoT)
   WoT = WoT/vol_0
   Pthermo = RHO_0*R_cst/WoT
   !if (irank.eq.iroot) write(*,*) 'Pthermo = ',Pthermo/101325.00_WP
                                                                 
   return
end subroutine update_pressure

!===============================================================================
!===============================================================================

subroutine constant_vol_src_term(src_term)
   use constant_volume
   use time_info
   implicit none
   real(WP), intent(out) :: src_term(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_)
   integer  :: i,j,k

   do k = kmin_,kmax_
      do j = jmin_,jmax_
         do i = imin_,imax_
            src_term(i,j,k) = src_term(i,j,k) + (Pthermo-Pthermo_old)/ & 
                                                (Cp*dt) / RHO(i,j,k)
         end do
      end do
   end do
   
   return
end subroutine constant_vol_src_term

!===============================================================================
!===============================================================================

subroutine pressure_burning_velocity(SL_phi, lF_phi)
   use precision
   use premixed
   use constant_volume
   use masks
   implicit none
   real(WP),intent(out)  :: SL_phi, lF_phi
   real(WP)              :: T0, Told, T_b, T_u
   real(WP)              :: Y_fu
   integer               :: i,j,k
   
   ! "global_phi" is the equivalence ratio
   
   ! Told = E*ln(B/P)^(-1)
   Told = 10200.9_WP*log(30044.1_WP/(Pthermo/101325.0_WP))**(-1.0_WP)
   
   ! T0 = Told + 236*(phi-1)*ln(40)/ln(phi*100) 
   T0 = Told+380_WP*(global_phi-0.9_WP)*log(40.0_WP)/log(global_phi*100.0_WP)

   T_b = 839.37_WP+3057.2_WP*(global_phi)-1177.9_WP*(global_phi)**(2.0_WP)
   
   T_u = 2600.0_WP
   do k = kmin_,kmax_
   do j = jmin_,jmax_
   do i = imin_,imax_
      if (mask(i,j).eq.0) T_u = min(T_u,SC(i,j,k,isc_T))
   end do
   end do
   end do
   call parallel_min(T_u,Y_fu)
   T_u = Y_fu

   ! Bound it for the purpose of keeping the CFL reasonable
   T_u = min(T_u,0.85_WP*T0)
  
   Y_fu = 0.0286_WP*global_phi
   
   SL_phi = 1.29288e6_WP * Y_fu**1.08721_WP * exp(-2057.56_WP/T0) * T_u/T0 * & 
            ((T_b-T0)/(T_b-T_u))**3.5349_WP
   
   SL_phi = SL_phi / 100.0_WP

   ! Terms for the Markstein length in the burning velocity
   Ma_delta = max((T0 - T_u)/T_u,1.0e-2_WP)
   Ze_num = 4.0_WP * (T_b - T_u) / (T_b - T0)
            
   !flame thickness: lF ~ D / (rho_unburned*SL)
   lF_phi = max(maxval(DIFFmol),1.0e-7_WP) / SL_phi
   
   return 
end subroutine pressure_burning_velocity

!===============================================================================
!===============================================================================

subroutine density_constant_volume
   use combustion
   use constant_volume
   use masks
   implicit none
   integer :: i,j,k
   real(WP) :: W_u, W_b, P_u, P_b, Wtot
   real(WP) :: T_max

   W_u = 24.2_WP  !kg/kmol
   W_b = 26.1_WP  !kg/kmol        
   T_max = 839.37_WP+3057.2_WP*(global_phi)-1177.9_WP*(global_phi)**(2.0_WP)
  
   ! Use ideal gas law to compute density
   do k=kmino_,kmaxo_
   do j=jmino_,jmaxo_
   do i=imino_,imaxo_
      if (mask(i,j).eq.0) then
         p_b = min(max((SC(i,j,k,isc_T)-300.0_WP)/T_max,0.0_WP),1.0_WP)
         p_u = 1.0_WP - p_b
         Wtot = W_u*p_u + W_b*p_b
      
         SC(i,j,k,isc_T) = min(max(300.0_WP,SC(i,j,k,isc_T)),2200.0_WP)
      
         RHO(i,j,k) = Pthermo * Wtot / (SC(i,j,k,isc_T) * R_cst)
      end if
   end do
   end do
   end do

   call boundary_update_border(RHO,'+','ym')

   return
end subroutine density_constant_volume

!===============================================================================
!===============================================================================
