module newton
  use precision
  use combustion
  implicit none

  integer, parameter :: niter = 10
  real(WP), external :: diffusion_chemtable_lookup_rho_val,diffusion_chemtable_lookup_rho_der

contains
  
  ! Invert a linear system of 'size' eqs
  subroutine invert(rhs,jac,size)
    implicit none
    
    integer, intent(in) :: size
    real(WP), dimension(size) :: rhs
    real(WP), dimension(size,size) :: jac
    real(WP), dimension(size) :: sol

    select case(size)
    case (1)
       rhs = rhs(1) / jac(1,1)
    case (2)
       rhs = rhs / (jac(1,1)*jac(2,2)-jac(1,2)*jac(2,1)) 
       sol(1) = + jac(2,2)*rhs(1) - jac(1,2)*rhs(2)
       sol(2) = - jac(2,1)*rhs(1) + jac(1,1)*rhs(2)
       rhs = sol
    case (3)
       rhs(1) = rhs(1) / jac(1,1)
       jac(1,:) = jac(1,:) / jac(1,1)

       rhs(2) = rhs(2) - rhs(1)*jac(2,1)
       jac(2,:) = jac(2,:) - jac(1,:)*jac(2,1)
       rhs(3) = rhs(3) - rhs(1)*jac(3,1)
       jac(3,:) = jac(3,:) - jac(1,:)*jac(3,1)

       rhs(2) = rhs(2) / jac(2,2)
       jac(2,:) = jac(2,:) / jac(2,2)

       rhs(3) = rhs(3) - rhs(2)*jac(3,2)
       jac(3,:) = jac(3,:) - jac(2,:)*jac(3,2)

       sol(3) = rhs(3)/jac(3,3)
       sol(2) = rhs(2) - jac(2,3)*sol(3)
       sol(1) = rhs(1) - jac(1,2)*sol(2) - jac(1,3)*sol(3)

       rhs = sol
    case default
       call die('newton/invert: not yet implemented')
    end select

    return
  end subroutine invert

  ! ---------------------------------------------------------------------

  ! Invert the 1D nonlinear system
  ! -> ZMIX : rho increases with ZMIX
  ! With the value
  ! -> S3 : either CHI or PROG
  subroutine invert_ZMIX_inc(i,j,k,S3,rho1,der)
    implicit none

    real(WP), intent(in)  :: S3
    real(WP), intent(out) :: rho1,der
    integer,  intent(in)  :: i,j,k
    integer :: n
    real(WP) :: rhs(1),jac(1,1)
    real(WP) :: rhoZ,Z1
    
    ! Value we want to invert
    rhoZ = SC(i,j,k,isc_ZMIX)*RHO(i,j,k)
    
    ! Initial guess = old value
    Z1 = SC(i,j,k,isc_ZMIX)
    
    ! Clip the values
    Z1 = min(1.0_WP,max(0.0_WP,Z1))
    
    ! Get the guess density and the derivative
    rho1 = diffusion_chemtable_lookup_rho_val(Z1,ZVAR(i,j,k),S3)
    
    ! Newton method
    do n=1,niter
       rhs(1) = rho1*Z1 - rhoZ
       jac(1,1) = rho1 + Z1*diffusion_chemtable_lookup_rho_der(1)
       
       call invert(rhs,jac,1)
       Z1 = Z1 - rhs(1)
       
       Z1 = min(1.0_WP,max(0.0_WP,Z1))
       rho1 = diffusion_chemtable_lookup_rho_val(Z1,ZVAR(i,j,k),S3)
    end do

    ! Return the derivative
    der = diffusion_chemtable_lookup_rho_der(1)
    
    return
  end subroutine invert_ZMIX_inc
  

  ! Invert the nonlinear system
  ! -> ZMIX : rho decreases with ZMIX
  ! With the value
  ! -> S3 : either CHI or PROG
  subroutine invert_ZMIX_dec(i,j,k,S3,rho1,der)
    implicit none

    real(WP), intent(in)  :: S3
    real(WP), intent(out) :: rho1,der
    integer,  intent(in)  :: i,j,k
    integer :: n
    real(WP) :: rhs(1),jac(1,1)
    real(WP) :: rhoY,Y1
    
    ! Value we want to invert
    rhoY = (1.0_WP-SC(i,j,k,isc_ZMIX))*RHO(i,j,k)
    
    ! Initial guess = old value
    Y1 = 1.0_WP - SC(i,j,k,isc_ZMIX)
    
    ! Clip the values
    Y1 = min(1.0_WP,max(0.0_WP,Y1))
    
    ! Get the guess density and the derivative
    rho1 = diffusion_chemtable_lookup_rho_val(1.0_WP-Y1,ZVAR(i,j,k),S3)
    
    ! Newton method
    do n=1,niter
       rhs(1) = rho1*Y1 - rhoY
       jac(1,1) = rho1 - Y1*diffusion_chemtable_lookup_rho_der(1)
       
       call invert(rhs,jac,1)
       Y1 = Y1 - rhs(1)
       
       Y1 = min(1.0_WP,max(0.0_WP,Y1))
       rho1 = diffusion_chemtable_lookup_rho_val(1.0_WP-Y1,ZVAR(i,j,k),S3)
    end do
    
    ! Return the derivative
    der = diffusion_chemtable_lookup_rho_der(1)

    return
  end subroutine invert_ZMIX_dec
    

  ! ---------------------------------------------------------------------

  ! Invert the nonlinear system
  ! -> ZMIX : rho increases with ZMIX
  ! -> ZVAR : rho increases with ZVAR
  ! With the value
  ! -> S3 : either CHI or PROG
  subroutine invert_ZMIX_ZVAR_inc(i,j,k,S3,rho1,der)
    implicit none

    real(WP), intent(in)  :: S3
    real(WP), intent(out) :: rho1,der
    integer,  intent(in)  :: i,j,k
    integer :: n
    real(WP) :: rhs(2),jac(2,2)
    real(WP) :: rhoZ,Z1,rhoQ,Q1
    
    ! Value we want to invert
    rhoZ = SC(i,j,k,isc_ZMIX)*RHO(i,j,k)
    rhoQ = SC(i,j,k,isc_ZVAR)*RHO(i,j,k)
    
    ! Initial guess = old value
    Z1 = SC(i,j,k,isc_ZMIX)
    Q1 = SC(i,j,k,isc_ZVAR)
    
    ! Clip the values
    Z1 = min(1.0_WP,max(0.0_WP,Z1))
    Q1 = min(Z1*(1.0_WP-Z1),max(0.0_WP,Q1))
    
    ! Get the guess density and the derivative
    rho1 = diffusion_chemtable_lookup_rho_val(Z1,Q1,S3)
    
    ! Newton method
    do n=1,niter
       rhs(1) = rho1*Z1 - rhoZ
       rhs(2) = rho1*Q1 - rhoQ
       
       jac(1,1) = rho1 + Z1*diffusion_chemtable_lookup_rho_der(1)
       jac(1,2) =      + Z1*diffusion_chemtable_lookup_rho_der(2)
       
       jac(2,1) =      + Q1*diffusion_chemtable_lookup_rho_der(1)
       jac(2,2) = rho1 + Q1*diffusion_chemtable_lookup_rho_der(2)
       
       call invert(rhs,jac,2)
       Z1 = Z1 - rhs(1)
       Q1 = Q1 - rhs(2)
       
       Z1 = min(1.0_WP,max(0.0_WP,Z1))
       Q1 = min(Z1*(1.0_WP-Z1),max(0.0_WP,Q1))
       
       rho1 = diffusion_chemtable_lookup_rho_val(Z1,Q1,S3)
    end do
    
    ! Return the derivative
    der = diffusion_chemtable_lookup_rho_der(1)
    
    return
  end subroutine invert_ZMIX_ZVAR_inc
  

  ! Invert the nonlinear system
  ! -> ZMIX : rho decreases with ZMIX
  ! -> ZVAR : rho increases with ZVAR
  ! With the value
  ! -> S3 : either CHI or PROG
  subroutine invert_ZMIX_ZVAR_dec(i,j,k,S3,rho1,der)
    implicit none

    real(WP), intent(in)  :: S3
    real(WP), intent(out) :: rho1,der
    integer,  intent(in)  :: i,j,k
    integer :: n
    real(WP) :: rhs(2),jac(2,2)
    real(WP) :: rhoY,Y1,rhoQ,Q1
    
    ! Value we want to invert
    rhoY = (1.0_WP-SC(i,j,k,isc_ZMIX))*RHO(i,j,k)
    rhoQ = SC(i,j,k,isc_ZVAR)*RHO(i,j,k)
    
    ! Initial guess = old value
    Y1 = 1.0_WP - SC(i,j,k,isc_ZMIX)
    Q1 = SC(i,j,k,isc_ZVAR)
    
    ! Clip the values
    Y1 = min(1.0_WP,max(0.0_WP,Y1))
    Q1 = min(Y1*(1.0_WP-Y1),max(0.0_WP,Q1))
    
    ! Get the guess density and the derivative
    rho1 = diffusion_chemtable_lookup_rho_val(1.0_WP-Y1,Q1,S3)
    
    ! Newton method
    do n=1,niter
       rhs(1) = rho1*Y1 - rhoY
       rhs(2) = rho1*Q1 - rhoQ
       
       jac(1,1) = rho1 - Y1*diffusion_chemtable_lookup_rho_der(1)
       jac(1,2) =      + Y1*diffusion_chemtable_lookup_rho_der(2)
       
       jac(2,1) =      - Q1*diffusion_chemtable_lookup_rho_der(1)
       jac(2,2) = rho1 + Q1*diffusion_chemtable_lookup_rho_der(2)
       
       call invert(rhs,jac,2)
       Y1 = Y1 - rhs(1)
       Q1 = Q1 - rhs(2)
       
       Y1 = min(1.0_WP,max(0.0_WP,Y1))
       Q1 = min(Y1*(1.0_WP-Y1),max(0.0_WP,Q1))
       
       rho1 = diffusion_chemtable_lookup_rho_val(1.0_WP-Y1,Q1,S3)
    end do
    
    ! Return the derivative
    der = diffusion_chemtable_lookup_rho_der(1)

    return
  end subroutine invert_ZMIX_ZVAR_dec

  ! ---------------------------------------------------------------------
  
  ! Invert the 1D nonlinear system
  ! -> ZMIX : rho increases with ZMIX
  subroutine invert_ZMIX_only_inc(i,j,k,rho1,der)
    implicit none
    
    real(WP), intent(out) :: rho1,der
    integer,  intent(in)  :: i,j,k
    integer :: n
    real(WP) :: rhs(1),jac(1,1)
    real(WP) :: rhoZ,Z1
    
    ! Value we want to invert
    rhoZ = SC(i,j,k,isc_ZMIX)*RHO(i,j,k)
    
    ! Initial guess = old value
    Z1 = SC(i,j,k,isc_ZMIX)
    
    ! Clip the values
    Z1 = min(1.0_WP,max(0.0_WP,Z1))
    
    ! Get the guess density and the derivative
    rho1 = diffusion_chemtable_lookup_rho_val(Z1,0.0_WP,0.0_WP)
    
    ! Newton method
    do n=1,niter
       rhs(1) = rho1*Z1 - rhoZ
       jac(1,1) = rho1 + Z1*diffusion_chemtable_lookup_rho_der(1)
       
       call invert(rhs,jac,1)
       Z1 = Z1 - rhs(1)
       
       Z1 = min(1.0_WP,max(0.0_WP,Z1))
       rho1 = diffusion_chemtable_lookup_rho_val(Z1,0.0_WP,0.0_WP)
    end do
    
    ! Return the derivative
    der = diffusion_chemtable_lookup_rho_der(1)
    
    return
  end subroutine invert_ZMIX_only_inc
  

  ! Invert the nonlinear system
  ! -> ZMIX : rho decreases with ZMIX
  subroutine invert_ZMIX_only_dec(i,j,k,rho1,der)
    implicit none
    
    real(WP), intent(out) :: rho1,der
    integer,  intent(in)  :: i,j,k
    integer :: n
    real(WP) :: rhs(1),jac(1,1)
    real(WP) :: rhoY,Y1
    
    ! Value we want to invert
    rhoY = (1.0_WP-SC(i,j,k,isc_ZMIX))*RHO(i,j,k)
    
    ! Initial guess = old value
    Y1 = 1.0_WP - SC(i,j,k,isc_ZMIX)
    
    ! Clip the values
    Y1 = min(1.0_WP,max(0.0_WP,Y1))
    
    ! Get the guess density and the derivative
    rho1 = diffusion_chemtable_lookup_rho_val(1.0_WP-Y1,0.0_WP,0.0_WP)
    
    ! Newton method
    do n=1,niter
       rhs(1) = rho1*Y1 - rhoY
       jac(1,1) = rho1 - Y1*diffusion_chemtable_lookup_rho_der(1)
       
       call invert(rhs,jac,1)
       Y1 = Y1 - rhs(1)
       
       Y1 = min(1.0_WP,max(0.0_WP,Y1))
       rho1 = diffusion_chemtable_lookup_rho_val(1.0_WP-Y1,0.0_WP,0.0_WP)
    end do
    
    ! Return the derivative
    der = diffusion_chemtable_lookup_rho_der(1)
    
    return
  end subroutine invert_ZMIX_only_dec
    
end module newton


! =================================== !
! 'Invert' the chemtable using :      !
! -> ZMIX : mixture fraction          !
! =================================== !
subroutine newton_ZMIX(S3)
  use newton
  implicit none

  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: S3
  integer :: i,j,k
  real(WP) :: rho_old,rho_new,der
  real(WP) :: Z1

  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           ! Initial guess = old value
           Z1 = SC(i,j,k,isc_ZMIX)
           
           ! Clip the values
           Z1 = min(1.0_WP,max(0.0_WP,Z1))
           
           ! Get the guess density and save old density
           rho_old = RHO(i,j,k)
           rho_new = diffusion_chemtable_lookup_rho_val(Z1,ZVAR(i,j,k),S3(i,j,k))
           der     = diffusion_chemtable_lookup_rho_der(1)
           
           ! Apply newton on the right quantity
           if (der.gt.0.0_WP) then
              call invert_ZMIX_inc(i,j,k,S3(i,j,k),rho_new,der)
              if (der.lt.0.0_WP) call invert_ZMIX_dec(i,j,k,S3(i,j,k),rho_new,der)
           else
              call invert_ZMIX_dec(i,j,k,S3(i,j,k),rho_new,der)
              if (der.gt.0.0_WP) call invert_ZMIX_inc(i,j,k,S3(i,j,k),rho_new,der)
           end if
           
           ! Now get all the scalars from conservation of rho*sc
           RHO(i,j,k) = rho_new
           SC(i,j,k,:) = rho_old * SC(i,j,k,:) / rho_new
        end do
     end do
  end do

  return
end subroutine newton_ZMIX

! =================================== !
! 'Invert' the chemtable using :      !
! -> ZMIX : mixture fraction          !
! =================================== !
subroutine newton_ZMIX_only
  use newton
  implicit none
  
  integer :: i,j,k
  real(WP) :: rho_old,rho_new,der
  real(WP) :: Z1
  
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           ! Initial guess = old value
           Z1 = SC(i,j,k,isc_ZMIX)
           
           ! Clip the values
           Z1 = min(1.0_WP,max(0.0_WP,Z1))
           
           ! Get the guess density and save old density
           rho_old = RHO(i,j,k)
           rho_new = diffusion_chemtable_lookup_rho_val(Z1,0.0_WP,0.0_WP)
           der     = diffusion_chemtable_lookup_rho_der(1)
           
           ! Apply newton on the right quantity
           if (der.gt.0.0_WP) then
              call invert_ZMIX_only_inc(i,j,k,rho_new,der)
              if (der.lt.0.0_WP) call invert_ZMIX_only_dec(i,j,k,rho_new,der)
           else
              call invert_ZMIX_only_dec(i,j,k,rho_new,der)
              if (der.gt.0.0_WP) call invert_ZMIX_only_inc(i,j,k,rho_new,der)
           end if
           
           ! Now get all the scalars from conservation of rho*sc
           RHO(i,j,k) = rho_new
           SC(i,j,k,:) = rho_old * SC(i,j,k,:) / rho_new
        end do
     end do
  end do
  
  return
end subroutine newton_ZMIX_only


! =================================== !
! 'Invert' the chemtable using :      !
! -> ZMIX : mixture fraction          !
! -> ZVAR : mixture fraction variance !
! =================================== !
subroutine newton_ZMIX_ZVAR(S3)
  use newton
  implicit none

  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: S3
  integer :: i,j,k
  real(WP) :: rho_old,rho_new,der
  real(WP) :: Z1,Q1

  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           ! Initial guess = old value
           Z1 = SC(i,j,k,isc_ZMIX)
           Q1 = SC(i,j,k,isc_ZVAR)
           
           ! Clip the values
           Z1 = min(1.0_WP,max(0.0_WP,Z1))
           Q1 = min(Z1*(1.0_WP-Z1),max(0.0_WP,Q1))
           
           ! Get the guess density and save old density
           rho_old = RHO(i,j,k)
           rho_new = diffusion_chemtable_lookup_rho_val(Z1,Q1,S3(i,j,k))
           der     = diffusion_chemtable_lookup_rho_der(1)
           
           ! Apply newton on the right quantity
           if (der.gt.0.0_WP) then
              call invert_ZMIX_ZVAR_inc(i,j,k,S3(i,j,k),rho_new,der)
              if (der.lt.0.0_WP) call invert_ZMIX_ZVAR_dec(i,j,k,S3(i,j,k),rho_new,der)
           else
              call invert_ZMIX_ZVAR_dec(i,j,k,S3(i,j,k),rho_new,der)
              if (der.gt.0.0_WP) call invert_ZMIX_ZVAR_inc(i,j,k,S3(i,j,k),rho_new,der)
           end if
           
           ! Now get all the scalars from conservation of rho*sc
           RHO(i,j,k) = rho_new
           SC(i,j,k,:) = rho_old * SC(i,j,k,:) / rho_new
        end do
     end do
  end do

  return
end subroutine newton_ZMIX_ZVAR

