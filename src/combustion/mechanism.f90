! -------------- !
! Empty template !
! -------------- !
module mechanism
  use precision
  use string
  implicit none

  ! Number of species, reactions, and thirdbodies
  integer, parameter :: nspec = 4
  integer, parameter :: nreac = 1
  integer, parameter :: nthird = 0

  ! Gas phase species
  integer, parameter :: gN2 = 1
  integer, parameter :: gVOL = 2
  integer, parameter :: gGAS = 3
  integer, parameter :: gHC = 4

  ! Transport parameters and function
  real(WP), dimension(nspec) :: koveps,mucoeff
  real(WP), dimension(nspec,nspec) :: Dcoeffs,Ocoeffs

  real(WP) :: kval

  ! --- Declarations - Rate coefficients --- !

contains

  ! ============================================ !
  ! Function for pressure dependent coefficients !
  ! ============================================ !
  real(WP) function getlindratecoeff (Tloc,pressure,k0,kinf,fc,concin)
      implicit none

    real(WP) ::  Tloc,pressure,k0,kinf,fc
    real(WP), parameter :: R = 8.31434
    real(WP) :: ntmp,ccoeff,dcoeff,lgknull
    real(WP) :: f
    real(WP) :: conc, concin

    if (concin.gt.0.0_WP) then
      conc = concin
    else
      conc = pressure / ( R * Tloc )
    end if
    ntmp = 0.75 - 1.27 * dlog10( fc )

    ccoeff = - 0.4 - 0.67 * dlog10( fc )
    dcoeff = 0.14
    k0 = k0 * conc / max(kinf, 1.0e-60_WP)
    lgknull = dlog10(k0)
    f = (lgknull+ccoeff)/(ntmp-dcoeff*(lgknull+ccoeff))
    f = fc**(1.0_WP / ( f * f + 1.0_WP ))
    getlindratecoeff = kinf * f * k0 / ( 1.0 + k0 )

  end function getlindratecoeff

  ! ================================================ !
  ! Compute omegamu needed for transport calculation !
  ! ================================================ !
  real(WP) function omegamu(T_)
    implicit none
    real(WP), intent(in) :: T_
    real(WP), parameter :: m1=3.3530622607_WP
    real(WP), parameter :: m2=2.53272006_WP
    real(WP), parameter :: m3=2.9024238575_WP
    real(WP), parameter :: m4=0.11186138893_WP
    real(WP), parameter :: m5=0.8662326188_WP  ! = -0.1337673812 + 1.0
    real(WP), parameter :: m6=1.3913958626_WP
    real(WP), parameter :: m7=3.158490576_WP
    real(WP), parameter :: m8=0.18973411754_WP
    real(WP), parameter :: m9=0.00018682962894_WP
    omegamu=(m1+T_*(m2+T_*(m3+T_*m4)))/(m5+T_*(m6+T_*(m7+T_*(m8+T_*m9))))
  end function omegamu

  ! ================================================ !
  ! Compute omegamu needed for transport calculation !
  ! ================================================ !
  real(WP) function omegaD(T_)
    implicit none
    real(WP), intent(in) :: T_
    real(WP), parameter :: m1=6.8728271691_WP
    real(WP), parameter :: m2 = 9.4122316321_WP
    real(WP), parameter :: m3 = 7.7442359037_WP
    real(WP), parameter :: m4 = 0.23424661229_WP
    real(WP), parameter :: m5 = 1.45337701568_WP	! = 1.0 + 0.45337701568 
    real(WP), parameter :: m6 = 5.2269794238_WP
    real(WP), parameter :: m7 = 9.7108519575_WP
    real(WP), parameter :: m8 = 0.46539437353_WP
    real(WP), parameter :: m9 = 0.00041908394781_WP
    omegaD=(m1+T_*(m2+T_*(m3+T_*m4)))/(m5+T_*(m6+T_*(m7+T_*(m8+T_*m9))));
  end function omegaD

end module mechanism

! ================ !
! Third bodies !
! ================ !
subroutine thirdbodies ( c, M )
  use mechanism
  implicit none

  real(WP), dimension(nspec) :: c
  real(WP), dimension(nthird) :: M

  M = 0.0_WP

  return
end subroutine thirdbodies

! ================= !
! Rate coefficients !
! ================= !
subroutine ratecoefficients ( k, Tloc, pressure, M )
  use mechanism
  implicit none

  real(WP), dimension(nreac) :: k
  real(WP), dimension(nthird) :: M
  real(WP) :: Tloc,lnT,RT,pressure

  ! Logarithm of temperature, R*T
  lnT = log(Tloc)
  RT = 8.314_WP*Tloc

  k(1) = kval

  return
end subroutine ratecoefficients

! ============== !
! Reaction rates !
! ============== !
subroutine reactionrates ( w, k, c, M, gamma )
  use mechanism
  implicit none

  real(WP), dimension(nreac) :: k, w
  real(WP), dimension(nspec) :: c
  real(WP), dimension(nthird) :: M
  real(WP) :: gamma

  w(1) = k(1) * gamma * c(gVOL)

  return
end subroutine reactionrates

! ================ !
! Production rates !
! ================ !
subroutine prodrates ( cdot, w)
  use mechanism
  implicit none

  ! --- Declarations - Miscellaneous --- !
  real(WP), dimension(nspec) :: cdot
  real(WP), dimension(nreac) :: w

  ! --- Production rates --- !
  cdot(gN2) = 0.0_WP
  cdot(gVOL) = -w(1)
  cdot(gGAS) = 0.175_WP * w(1)
  cdot(gHC) = 1.575_WP * w(1)
  
  return
end subroutine prodrates

! =========================== !
!  Parameters initialization  !
! =========================== !
subroutine mechanism_init 
  use mechanism
  use parser
  implicit none

  call parser_read('kval',kval)

  call get_kovereps
  call get_mucoeff
  
  return
end subroutine mechanism_init

! ============ !
!  Molar mass  !
! ============ !
subroutine getmolarmass( mm )
  use mechanism
  implicit none

  real(WP), dimension(nspec) :: mm

  mm(gN2) = 0.028_WP
  mm(gVOL) = 0.1498_WP
  mm(gGAS) = 0.028_WP
  mm(gHC) = 0.092_WP
  
  return
end subroutine getmolarmass

! =============== !
!  Species names  !
! =============== !
subroutine getspeciesnames( names )
  use mechanism
  implicit none

  character(len=str_medium), dimension(nspec) :: names

  names(gN2) = 'N2'
  names(gVOL) = 'VOL'
  names(gGAS) = 'GAS'
  names(gHC) = 'HC'

  return
end subroutine getspeciesnames

! ================= !
! Number of species !
! ================= !
subroutine getnspecies_all( nspecies )
  use mechanism
  implicit none

  integer :: nspecies

  nspecies = nspec

  return
end subroutine getnspecies_all

! =================== !
! Number of reactions !
! =================== !
subroutine getnreactions( nreactions )
  use mechanism
  implicit none

  integer ::  nreactions

  nreactions = nreac

  return
end subroutine getnreactions
! ================================== !
! Number of non-steady-state species !
! ================================== !
subroutine getnspecies(nspecies_nons)
  use mechanism
  implicit none

  integer :: nspecies_nons
  nspecies_NONS = nspec

  return
end subroutine getnspecies


! ===================== !
! Cp and H computations !
! ===================== !
subroutine compthermodata(H,Cp,T)
  use mechanism

  implicit none

  real(WP), dimension(nspec) :: H,Cp
  real(WP) :: T
  
  if (T.gt.1000.000) then
     
    H(gN2) = 296.716630977873 * ( T * ((2.92664000e+00_WP) + T * ((0.0007439884_WP) + T * ((-1.89492e-07_WP) + T * ((2.5242595e-11_WP) + T * ((-1.3506702e-15_WP)))))) + (-9.22797700e+02_WP))
    Cp(gN2) = 296.716630977873 * ((2.92664000e+00_WP) + T * ((1.48797680e-03_WP) + T * ((-5.68476000e-07_WP) + T * ((1.00970380e-10_WP) + T * (-6.75335100e-15_WP))))) 

    H(gVOL) = 64.870010299304 * ( T * ((1.76826275e+00_WP) + T * ((0.0344571753_WP) + T * ((-1.38107392e-05_WP) + T * ((2.947857725e-09_WP) + T * ((-2.57194122e-13_WP)))))) + (1.45412795e+04_WP))
    Cp(gVOL) = 64.870010299304 * ((1.76826275e+00_WP) + T * ((6.89143506e-02_WP) + T * ((-4.14322176e-05_WP) + T * ((1.17914309e-08_WP) + T * (-1.28597061e-12_WP)))))
    
    H(gHC) = 90.2381314172835 * ( T * ((-1.01117220e+00_WP) + T * ((0.0292650956_WP) + T * ((-1.15865023e-05_WP) + T * ((2.4554524825e-09_WP) + T * ((-2.1336174e-13_WP)))))) + (3.99363395e+03_WP))
    Cp(gHC) = 90.2381314172835 * ((-1.01117220e+00_WP) + T * ((5.85301912e-02_WP) + T * ((-3.47595069e-05_WP) + T * ((9.82180993e-09_WP) + T * (-1.06680870e-12_WP)))))

    H(gGAS) = 296.822563370225 * ( T * ((3.02507800e+00_WP) + T * ((0.0007213445_WP) + T * ((-1.87694266666667e-07_WP) + T * ((2.5464525e-11_WP) + T * ((-1.3821904e-15_WP)))))) + (-1.42683500e+04_WP))
    Cp(gGAS) = 296.822563370225 * ((3.02507800e+00_WP) + T * ((1.44268900e-03_WP) + T * ((-5.63082800e-07_WP) + T * ((1.01858100e-10_WP) + T * (-6.91095200e-15_WP)))))

  else
     
    H(gN2) = 296.716630977873 * ( T * ((3.29867700e+00_WP) + T * ((0.0007041202_WP) + T * ((-1.321074e-06_WP) + T * ((1.41037875e-09_WP) + T * ((-4.889708e-13_WP)))))) + (-1.02089990e+03_WP))
    Cp(gN2) = 296.716630977873 * ((3.29867700e+00_WP) + T * ((1.40824040e-03_WP) + T * ((-3.96322200e-06_WP) + T * ((5.64151500e-09_WP) + T * (-2.44485400e-12_WP)))))

    H(gVOL) = 64.870010299304 * ( T * ((-8.72434585e+00_WP) + T * ((0.052688004_WP) + T * ((-2.67236896666667e-05_WP) + T * ((5.46364935e-09_WP) + T * ((2.84133212e-13_WP)))))) + (1.66588912e+04_WP))
    Cp(gVOL) = 64.870010299304 * ((-8.72434585e+00_WP) + T * ((1.05376008e-01_WP) + T * ((-8.01710690e-05_WP) + T * ((2.18545974e-08_WP) + T * (1.42066606e-12_WP)))))

    H(gHC) = 90.2381314172835 * ( T * ((-4.54072038e+00_WP) + T * ((0.03427135725_WP) + T * ((-1.19037674666667e-05_WP) + T * ((-1.048494105e-09_WP) + T * ((1.48355959e-12_WP)))))) + (4.64121087e+03_WP))
    Cp(gHC) = 90.2381314172835 * ((-4.54072038e+00_WP) + T * ((6.85427145e-02_WP) + T * ((-3.57113024e-05_WP) + T * ((-4.19397642e-09_WP) + T * (7.41779795e-12_WP)))))

    H(gGAS) = 296.822563370225 * ( T * ((3.26245200e+00_WP) + T * ((0.0007559705_WP) + T * ((-1.29391833333333e-06_WP) + T * ((1.395486e-09_WP) + T * ((-4.949902e-13_WP)))))) + (-1.43105400e+04_WP))
    Cp(gGAS) = 296.822563370225 * ((3.26245200e+00_WP) + T * ((1.51194100e-03_WP) + T * ((-3.88175500e-06_WP) + T * ((5.58194400e-09_WP) + T * (-2.47495100e-12_WP)))))

  end if

  return
end subroutine compthermodata

! ================================== !
! k over epsilon from transport file !
! ================================== !
subroutine get_kovereps
  use mechanism
  implicit none

  koveps(gN2) = 0.0102532554085922_WP
  koveps(gVOL) = 0.00158629441624365_WP
  koveps(gGAS) = 0.0101936799184506_WP
  koveps(gHC) = 0.00201897839693115_WP

  return
end subroutine get_kovereps

! ================================ !
! Omega_coeffs (k over epsilon^2)  !
! ================================ !
subroutine get_Ocoeff
  use mechanism
  implicit none

  Ocoeffs(gN2,gN2) = 0.0102532554085922_WP
  Ocoeffs(gN2,gVOL) = 0.0040329495165412_WP
  Ocoeffs(gN2,gGAS) = 0.0102234242676958_WP
  Ocoeffs(gN2,gHC) = 0.00454984627961926_WP

  Ocoeffs(gVOL,gN2) = 0.00498682606180831_WP
  Ocoeffs(gVOL,gVOL) = 0.00158629441624365_WP
  Ocoeffs(gVOL,gHC) = 0.00178960726349902_WP
  Ocoeffs(gVOL,gGAS) = 0.00402121592750417_WP

  Ocoeffs(gGAS,gN2) = 0.0102234242676958_WP
  Ocoeffs(gGAS,gGAS) = 0.0101936799184506_WP
  Ocoeffs(gGAS,gHC) = 0.00453660881502721_WP
  Ocoeffs(gGAS,gVOL) = 0.00402121592750417_WP

  Ocoeffs(gHC,gN2) = 0.00454984627961926_WP
  Ocoeffs(gHC,gGAS) = 0.00453660881502721_WP
  Ocoeffs(gHC,gHC) = 0.00201897839693115_WP
  Ocoeffs(gHC,gVOL) =  0.00178960726349902_WP

  return
end subroutine get_Ocoeff

! ========================== !
! Constant part of viscosity !
! ========================== !
subroutine get_mucoeff
  use mechanism
  implicit none

  mucoeff(gN2) = 1.07775110505506e-06_WP
  mucoeff(gVOL) = 7.91311609587584e-07_WP 
  mucoeff(gHC) = 7.94245481527314e-07_WP
  mucoeff(gGAS) = 1.06050394031311e-06_WP

  return
end subroutine get_mucoeff

! ======================== !
! Pure component viscosity !
! ======================== !
subroutine get_viscosity( mu, T )
  use mechanism
  implicit none

  ! Viscosity array 
  real(WP), dimension(nspec) :: mu
  ! Temperature 
  real(WP) :: T
  integer :: i

  do i=1,nspec
     mu(i) = mucoeff(i)*sqrt(T)/omegamu(T*koveps(i))
  end do
  
  return
end subroutine get_viscosity

! =========================== !
! Pure component conductivity !
! =========================== !
subroutine get_conductivity( lambda, T, mu, Cp, W )
  use mechanism
  implicit none

  ! Conductivity array
  real(WP), dimension(nspec) :: lambda
  ! Temperature
  real(WP) :: T
  ! Viscosity and co.
  real(WP), dimension(nspec) :: mu, Cp, W

  lambda = mu*(Cp+1.2_WP*8.314_WP/W)

  return
end subroutine get_conductivity

! ============================================== !
! Constant part of binary diffusion coefficients !
! ============================================== !
subroutine get_Dcoeff
  use mechanism
  implicit none

  Dcoeffs(gN2,gN2) = 0.000383765760376417_WP
  Dcoeffs(gN2,gVOL) = 0.000163554588634937_WP
  Dcoeffs(gN2,gGAS) = 0.00038074458458987_WP
  Dcoeffs(gN2,gHC) = 0.000187874627328682_WP

  Dcoeffs(gVOL,gN2) = 0.000163554588634937_WP
  Dcoeffs(gVOL,gVOL) = 6.16023617323557e-05_WP
  Dcoeffs(gVOL,gHC) = 7.31552067728711e-05_WP
  Dcoeffs(gVOL,gGAS) = 0.000162614805155135_WP

  Dcoeffs(gGAS,gN2) = 0.00038074458458987_WP
  Dcoeffs(gGAS,gGAS) = 0.000377759205307997_WP
  Dcoeffs(gGAS,gHC) = 0.00018673407353191_WP 
  Dcoeffs(gGAS,gVOL) = 0.000162614805155135_WP

  Dcoeffs(gHC,gN2) = 0.000187874627328682_WP
  Dcoeffs(gHC,gGAS) = 0.00018673407353191_WP
  Dcoeffs(gHC,gHC) = 8.60103479877196e-05_WP
  Dcoeffs(gHC,gVOL) = 7.31552067728711e-05_WP 

  return
end subroutine get_Dcoeff

! ======================================== !
! Inverse of binary diffusion coefficients !
! ======================================== !
subroutine get_invDij( invDij, T, P )
  use mechanism
  implicit none

  ! Diffusion coefficients array 
  real(WP), dimension(nspec,nspec) :: invDij
  ! Temperature 
  real(WP) :: T
  ! Pressure 
  real(WP) :: P
  ! Mix 
  real(WP) :: TPterm
  integer :: i,j

  ! Pressure and temperature dependent term
  TPterm = P/(T*sqrt(T))
  do i=1,nspec
    do j=1,i-1
      invDij(i,j) = TPterm*omegaD(T*Ocoeffs(i,j))/Dcoeffs(i,j)
      invDij(j,i) = invDij(i,j)
    end do
    invDij(i,i) = 0.0_WP
  end do

  return
end subroutine get_invDij

! ---------------------------------------------------------------- !
! =================== !
! Analytical jacobian !
! =================== !
subroutine get_jacobian( c, Tloc, pr, w, k, M, rho, cpmix, h, cp, mm, pressure, jac)
  use mechanism
  implicit none

  real(WP), dimension(nspec) :: c,pr,h,mm,cp
  real(WP), dimension(nreac) :: k,wc,w
  real(WP), dimension(nspec+1,nspec+1) :: jac
  real(WP), dimension(0) :: M
  real(WP) :: Tloc,cpmix,dtmp,rho,pressure
  integer :: i

  return
end subroutine get_jacobian
