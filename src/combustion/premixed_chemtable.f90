module premixed_chemtable
  use string
  use precision
  implicit none

  ! Combustion model
  character(len=str_medium) :: PremixedcombModel

  ! Mapping variables of the chemtable
  integer :: n1,n2,n3
  real(WP) :: x1min, x1max, x2min, x2max, x3min, x3max
  real(WP), dimension(:), allocatable :: x1,x2,x3

  ! Number of variables tabulated
  integer :: nvar
  character(len=str_medium), dimension(:), allocatable :: chem_name

  ! Arrays of mapped variables
  real(WP), dimension(:,:,:,:), allocatable :: premixed_table
  
  ! Array of mask
  integer, dimension(:,:,:), allocatable :: premixed_chem_mask
  
  ! Arrays of max/min of PROG as a function of A3
  real(WP), dimension(:), allocatable :: prog_min
  real(WP), dimension(:), allocatable :: prog_max

  ! Store the values for interpolation for speedup in newton
  integer :: index_rho
  integer :: i1, i2, i3
  real(WP) :: w11, w12, w21, w22, w31, w32

end module premixed_chemtable


! ===================== !
! Read in the chemtable !
! ===================== !
subroutine premixed_chemtable_init(filename)
  use premixed_chemtable
  use data
  use parser
  use parallel
  implicit none

  character(len=str_medium), intent(inout) :: filename
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer  :: ierr, var, iunit
  
  ! Open the chemtable file
  filename = trim(mpiiofs) // trim(filename)
  call MPI_FILE_OPEN(MPI_COMM_WORLD,filename,MPI_MODE_RDONLY,mpi_info,iunit,ierr)
  if (ierr .ne. 0) call die("premixed_chemtable_init: error opening the chemtable")

  ! Read the headers
  call MPI_FILE_READ_ALL(iunit,n1,1,MPI_INTEGER,status,ierr)
  call MPI_FILE_READ_ALL(iunit,n2,1,MPI_INTEGER,status,ierr)
  call MPI_FILE_READ_ALL(iunit,n3,1,MPI_INTEGER,status,ierr)
  call MPI_FILE_READ_ALL(iunit,nvar,1,MPI_INTEGER,status,ierr)

  ! Allocate the corresponding arrays
  allocate (x1(n1), x2(n2), x3(n3))
  allocate (chem_name(nvar))
  allocate (premixed_table(n1,n2,n3,nvar))
  allocate (premixed_chem_mask(n1,n2,n3))
  allocate (prog_min(n3))
  allocate (prog_max(n3))
  prog_min=+huge(1.0_WP)
  prog_max=-huge(1.0_WP)
  
  ! Read the mapping variables
  call MPI_FILE_READ_ALL(iunit,x1,n1,MPI_REAL_WP,status,ierr)
  call MPI_FILE_READ_ALL(iunit,x2,n2,MPI_REAL_WP,status,ierr)
  call MPI_FILE_READ_ALL(iunit,x3,n3,MPI_REAL_WP,status,ierr)

  ! Read the chem_mask
  call MPI_FILE_READ_ALL(iunit,premixed_chem_mask,n1*n2*n3,MPI_INTEGER,status,ierr)
  
  ! Read the combustion model used
  call MPI_FILE_READ_ALL(iunit,PremixedcombModel,str_medium,MPI_CHARACTER,status,ierr)

  ! Read the names of the mapped variables
  do var=1,nvar
     call MPI_FILE_READ_ALL(iunit,chem_name(var),str_medium,MPI_CHARACTER,status,ierr)
  end do

  ! Read the mapped variables
  do var=1,nvar
     call MPI_FILE_READ_ALL(iunit,premixed_table(:,:,:,var),n1*n2*n3,MPI_REAL_WP,status,ierr)
     ! Get the min/max of the table
     if (trim(chem_name(var)).eq.'PROG') then
        do i1=1,n1
           do i2=1,n2
              do i3=1,n3
                 prog_min(i3) = min(premixed_table(i1,i2,i3,var),prog_min(i3))
                 prog_max(i3) = max(premixed_table(i1,i2,i3,var),prog_max(i3))
              end do
           end do
        end do
     end if
     ! Store index for density
     if (trim(chem_name(var)).eq.'RHO') index_rho = var
  end do
  
  ! Get some properties of the mapping
  x1min = minval(x1)
  x1max = maxval(x1)
  x2min = minval(x2)
  x2max = maxval(x2)
  x3min = minval(x3)
  x3max = maxval(x3)
  
  ! Close the file
  call MPI_FILE_CLOSE(iunit,ierr)
  
  return
end subroutine premixed_chemtable_init


! ============================================================= !
! Look in the chemtable for the variable named 'tag'            !
! with the value A1, A2, and A3 for the three mapping variables !
! ============================================================= !
subroutine premixed_chemtable_lookup(tag, R, A1, A2, A3, n)
  use premixed_chemtable
  implicit none

  integer, intent(in) :: n
  character(len=*), intent(in) :: tag
  real(WP), dimension(n), intent(in)  :: A1, A2, A3
  real(WP), dimension(n), intent(out) :: R

  integer :: var, i, j

  ! Get the index of the variable
  do var=1, nvar
     if (trim(chem_name(var)).eq.trim(tag)) exit
  end do
  if (var.gt.nvar) then
     call die('premixed chemtable_lookup: unknown variable : '//trim(tag))
  end if

  ! Trilinear interpolation
  do i=1,n

     ! First direction
     if (A1(i).lt.x1min) then
        i1 = 1
        w11 = 1.0_WP
     else if (A1(i).ge.x1max) then
        i1 = n1-1
        w11 = 0.0_WP
     else
        loop1:do j=1,n1-1
           if (A1(i).lt.x1(j+1)) then
              i1 = j
              exit loop1
           end if
        end do loop1
        w11 = (x1(i1+1)-A1(i))/(x1(i1+1)-x1(i1))
     end if
     w12 = 1.0_WP - w11

     ! Second direction
     if (A2(i).lt.x2min) then
        i2 = 1
        w21 = 1.0_WP
     else if (A2(i).ge.x2max) then
        i2 = n2-1
        w21 = 0.0_WP
     else
        loop2:do j=1,n2-1
           if (A2(i).lt.x2(j+1)) then
              i2 = j
              exit loop2
           end if
        end do loop2
        w21 = (x2(i2+1)-A2(i))/(x2(i2+1)-x2(i2))
     end if
     w22 = 1.0_WP - w21

     ! Third direction
     if (A3(i).lt.x3min) then
        i3 = 1
        w31 = 1.0_WP
     else if (A3(i).ge.x3max) then
        i3 = n3-1
        w31 = 0.0_WP
     else
        loop3:do j=1,n3-1
           if (A3(i).lt.x3(j+1)) then
              i3 = j
              exit loop3
           end if
        end do loop3
        w31 = (x3(i3+1)-A3(i))/(x3(i3+1)-x3(i3))
     end if
     w32 = 1.0_WP - w31
     ! Interpolation
     R(i) =  w31*( w21*( w11*premixed_table(i1  ,i2  ,i3  ,var)     &
                        +w12*premixed_table(i1+1,i2  ,i3  ,var) )   &
                  +w22*( w11*premixed_table(i1  ,i2+1,i3  ,var)     &
                        +w12*premixed_table(i1+1,i2+1,i3  ,var) ) ) &
            +w32*( w21*( w11*premixed_table(i1  ,i2  ,i3+1,var)     &
                        +w12*premixed_table(i1+1,i2  ,i3+1,var) )   &
                  +w22*( w11*premixed_table(i1  ,i2+1,i3+1,var)     &
                        +w12*premixed_table(i1+1,i2+1,i3+1,var) ) )
  end do

  return
end subroutine premixed_chemtable_lookup


! ============================================================= !
! Look in the chemtable for the density                         !
! with the value A1, A2, and A3 for the three mapping variables !
! ============================================================= !
function premixed_chemtable_lookup_rho(A1, A2, A3)
  use premixed_chemtable
  implicit none

  real(WP) :: premixed_chemtable_lookup_rho
  real(WP), intent(in)  :: A1, A2, A3

  integer :: var, j

  ! The variable is rho
  var = index_rho

  ! First direction
  if (A1.lt.x1min) then
     i1 = 1
     w11 = 1.0_WP
  else if (A1.ge.x1max) then
     i1 = n1-1
     w11 = 0.0_WP
  else
     loop1:do j=1,n1-1
        if (A1.lt.x1(j+1)) then
           i1 = j
           exit loop1
        end if
     end do loop1
     w11 = (x1(i1+1)-A1)/(x1(i1+1)-x1(i1))
  end if
  w12 = 1.0_WP - w11
  
  ! Second direction
  if (A2.lt.x2min) then
     i2 = 1
     w21 = 1.0_WP
  else if (A2.ge.x2max) then
     i2 = n2-1
     w21 = 0.0_WP
  else
     loop2:do j=1,n2-1
        if (A2.lt.x2(j+1)) then
           i2 = j
           exit loop2
        end if
     end do loop2
     w21 = (x2(i2+1)-A2)/(x2(i2+1)-x2(i2))
  end if
  w22 = 1.0_WP - w21
  
  ! Third direction
  if (A3.lt.x3min) then
     i3 = 1
     w31 = 1.0_WP
  else if (A3.ge.x3max) then
     i3 = n3-1
     w31 = 0.0_WP
  else
     loop3:do j=1,n3-1
        if (A3.lt.x3(j+1)) then
           i3 = j
           exit loop3
        end if
     end do loop3
     w31 = (x3(i3+1)-A3)/(x3(i3+1)-x3(i3))
  end if
  w32 = 1.0_WP - w31
  
  ! Interpolation
  premixed_chemtable_lookup_rho =  &
      w31*( w21*( w11*premixed_table(i1  ,i2  ,i3  ,var)     &
                 +w12*premixed_table(i1+1,i2  ,i3  ,var) )   &
           +w22*( w11*premixed_table(i1  ,i2+1,i3  ,var)     &
                 +w12*premixed_table(i1+1,i2+1,i3  ,var) ) ) &
     +w32*( w21*( w11*premixed_table(i1  ,i2  ,i3+1,var)     &
                 +w12*premixed_table(i1+1,i2  ,i3+1,var) )   &
           +w22*( w11*premixed_table(i1  ,i2+1,i3+1,var)     &
                 +w12*premixed_table(i1+1,i2+1,i3+1,var) ) )

  return
end function premixed_chemtable_lookup_rho


! =========================================================== !
! Return the interpolated min/max at the given location in Z1 !
! =========================================================== !
subroutine premixed_chemtable_prog_minmax(A3,sc_min,sc_max)
  use premixed_chemtable
  implicit none

  real(WP), intent(in)  :: A3
  real(WP), intent(out) :: sc_min,sc_max

  integer :: j

  ! Third direction
  if (A3.lt.x3min) then
     i3 = 1
     w31 = 1.0_WP
  else if (A3.ge.x3max) then
     i3 = n3-1
     w31 = 0.0_WP
  else
     loop1:do j=1,n1-1
        if (A3.lt.x3(j+1)) then
           i3 = j
           exit loop1
        end if
     end do loop1
     w31 = (x3(i3+1)-A3)/(x3(i3+1)-x3(i3))
  end if
  w32 = 1.0_WP - w31
  
  ! Return the interpolated max
  sc_min = w31*prog_min(i3) + w32*prog_min(i3+1)
  sc_max = w31*prog_max(i3) + w32*prog_max(i3+1)
  
  return
end subroutine premixed_chemtable_prog_minmax


! =============================================== !
! Find the maximum of a variable in the chemtable !
! =============================================== !
subroutine premixed_chemtable_lookup_max(tag, R)
  use premixed_chemtable
  implicit none

  character(len=*), intent(in) :: tag
  real(WP), intent(out) :: R

  integer :: var

  ! Get the index of the variable
  do var=1, nvar
     if (trim(chem_name(var)).eq.trim(tag)) exit
  end do
  if (var.gt.nvar) then
     call die('premixed chemtable_lookup: unknown variable : '//trim(tag))
  end if

  ! Return the max
  R = maxval(premixed_table(:,:,:,var))

  return
end subroutine premixed_chemtable_lookup_max


! =============================================== !
! Find the minimum of a variable in the chemtable !
! =============================================== !
subroutine premixed_chemtable_lookup_min(tag, R)
  use premixed_chemtable
  implicit none

  character(len=*), intent(in) :: tag
  real(WP), intent(out) :: R

  integer :: var

  ! Get the index of the variable
  do var=1, nvar
     if (trim(chem_name(var)).eq.trim(tag)) exit
  end do
  if (var.gt.nvar) then
     call die('premixed chemtable_lookup: unknown variable : '//trim(tag))
  end if

  ! Return the max
  R = minval(premixed_table(:,:,:,var))

  return
end subroutine premixed_chemtable_lookup_min
