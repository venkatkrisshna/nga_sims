module premixed
  use combustion
  implicit none
  
  ! Saved array for Ensight
  real(WP), dimension(:,:,:), allocatable :: c_from_g
  
  ! Arrays linked to temporary arrays
  real(WP), dimension(:,:,:), pointer :: flame_width   ! Flame parameters: lF
  real(WP), dimension(:,:,:), pointer :: c_max         ! Maximum progress variance
  real(WP), dimension(:,:,:), pointer :: YF            ! Mass fraction of fuel
  
  ! Premixed source term for PROG
  ! Saved for partially premixed combustion
  real(WP), dimension(:,:,:), allocatable :: C_PREM_SRC
  
  ! Fuel Lewis number
  real(WP) :: Le_F
  
end module premixed


! ========================================= !
! Initialize the premixed combustion module !
! ========================================= !
subroutine premixed_init
  use premixed
  use parser
  use memory
  implicit none  
  character(len=str_medium) :: filename
  
  ! Read in the premixed chemtable and initialize it
  call parser_read('Premixed chemtable',filename)
  call premixed_chemtable_init(filename)
  
  ! Read fuel Lewis number
  call parser_read('Fuel Lewis number', Le_F, 1.0_WP)
  
  ! Allocate arrays
  allocate(c_from_g   (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(C_PREM_SRC (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  
  ! Link array to temporary arrays
  ! CAREFUL : FX,FY,FZ = tmp4,5,6
  flame_width => tmp1
  c_max => tmp2
  YF => tmp3
  
  return
end subroutine premixed_init


! ============================================= !
! Compute the source term for progress variable !
! Consistent with levelset formulation          !
! ============================================= !
subroutine premixed_source_prog(src)
  use premixed
  use memory
  use time_info
  use math
  implicit none
  
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nscalar) :: src
  real(WP) :: filt_width, lF, dist, erf
  real(WP) :: aa, bb, cc
  real(WP) :: c_min, c_lvlset, psi
  integer  :: i,j,k
  
  ! Default null variance
  CVAR = 0.0_WP
  
  ! Get the flame thickness
  call premixed_chemtable_lookup('lF', flame_width,&
       SC(:,:,:,isc_PROG), CVAR, SC(:,:,:,isc_ZMIX), nxo_*nyo_*nzo_)
  
  ! Calculate a level set consistent progress variable
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           
           ! Get min/max of progress variable
           call premixed_chemtable_prog_minmax(SC(i,j,k,isc_ZMIX),c_min,c_max(i,j,k))
           
           ! Linear profile from 0 to 1 between -lF and +lF
           ! Filtered by a gaussian
           ! -> mean: dist
           ! -> rms : filt_width / sqrt(2)
           filt_width = sqrt(dx(i)**2+dy(j)**2) / sqrt(6.0_WP)
           lF   = 0.5_WP*flame_width(i,j,k)
           dist = LVLSET(i,j,k)
           
           aa = 0.25_WP * (1.0_WP + dist/lF) * & 
                (erf( (+lF-dist) / filt_width ) &
                -erf( (-lF-dist) / filt_width ) )
           
           bb = 0.25_WP * filt_width / (lF*sqrt(Pi)) * &
                (exp(-(lF+dist)**2/filt_width**2) &
                -exp(-(lF-dist)**2/filt_width**2) )
           
           cc = 0.5_WP*(1.0_WP - erf( (lF-dist) / filt_width) ) 
           
           ! Ensures that G=0 corresponds to C=c_max/2
           c_lvlset = aa + bb + cc
           c_lvlset = c_lvlset*c_max(i,j,k)
           
           ! Create a progress variable from the levelset
           psi = exp(-dist**2/(7.2_WP*filt_width**2))
           c_from_g(i,j,k) = SC(i,j,k,isc_PROG) + psi*(c_lvlset-SC(i,j,k,isc_PROG))
           
        end do
     end do
  end do
  
  ! Compute source term from table
  call premixed_chemtable_lookup('SRC_PROG', C_PREM_SRC, &
       c_from_g, CVAR, SC(:,:,:,isc_ZMIX), nxo_*nyo_*nzo_)
  
  ! Evaluate source term for RHO*PROG
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           if (SC(i,j,k,isc_PROG).gt.c_max(i,j,k)) C_PREM_SRC(i,j,k) = 0.0_WP
           
           src(i,j,k,isc_PROG) = src(i,j,k,isc_PROG) + &
                dt * RHO(i,j,k) * C_PREM_SRC(i,j,k)
        end do
     end do
  end do
  
  return
end subroutine premixed_source_prog


! ============================================ !
! Compute the source term for mixture fraction !
! Non-unity Lewis number effects               !
! ============================================ !
subroutine premixed_source_zmix(src)
  use premixed
  use memory
  use time_info
  use metric_generic
  implicit none
  
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,nscalar) :: src
  integer  :: i,j,k
  real(WP) :: factor
  
  ! Unity Lewis number, returns
  if (Le_F.eq.1.0_WP) return
  
  factor = dt * (1.0/Le_F - 1.0_WP)
  
  ! Compute the Fuel mass fraction
  ! YF = ZMIX - W_Fuel/W_PROG * PROG
  ! -> Only cross diffusion (PROG)
  ! -> Additional diffusion (ZMIX) treated separately
  YF = - 2.0_WP/18.0_WP * SC(:,:,:,isc_PROG)
  
  ! Clip Fuel mass fraction to physical values
  YF = min(max(YF,-SC(:,:,:,isc_ZMIX)),0.0_WP)
  
  ! Compute the fluxes
  do k=kmin_-st1,kmax_+st2
     do j=jmin_-st1,jmax_+st2
        do i=imin_-st1,imax_+st2
           
           FX(i,j,k) = &
                + sum(interp_sc_x(i,j,:)*DIFFmol(i-st2:i+st1,j,k,isc_PROG)) * &
                  sum(grad_x(i,j,:)*YF(i-st2:i+st1,j,k))
              
           FY(i,j,k) = &
                + sum(interp_sc_y(i,j,:)*DIFFmol(i,j-st2:j+st1,k,isc_PROG)) * &
                  sum(grad_y(i,j,:)*YF(i,j-st2:j+st1,k))
           
           FZ(i,j,k) = &
                + sum(interp_sc_z(i,j,:)*DIFFmol(i,j,k-st2:k+st1,isc_PROG)) * &
                  sum(grad_z(i,j,:)*YF(i,j,k-st2:k+st1))
           
        end do
     end do
  end do
  
  ! Compute the source term
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           
           src(i,j,k,isc_ZMIX) = factor * ( &
                +sum(div_u(i,j,k,:)*FX(i-st1:i+st2,j,k)) &
                +sum(div_v(i,j,k,:)*FY(i,j-st1:j+st2,k)) &
                +sum(div_w(i,j,k,:)*FZ(i,j,k-st1:k+st2)) )
           
        end do
     end do
  end do
  
!!$  ! For cylindrical, nothing at the axis
!!$  if (icyl.eq.1 .and. jproc.eq.1) then
!!$     j = jmin
!!$     do k=kmin_,kmax_
!!$        do i=imin_,imax_
!!$           
!!$           src(i,j,k,isc_ZMIX) = factor * ( &
!!$                +sum(div_u(i,j,k,:)*FX(i-st1:i+st2,j,k)) &
!!$                +sum(div_v(i,j,k,:)*FY(i,j-st1:j+st2,k)) )
!!$           
!!$        end do
!!$     end do
!!$  end if
  
  return
end subroutine premixed_source_zmix


! ============================================ !
! Compute the source term for mixture fraction !
! Non-unity Lewis number effects               !
! ============================================ !
subroutine premixed_diffusivity_update
  use premixed
  implicit none
  integer  :: i,j,k
  
  ! Unity Lewis number, returns
  if (Le_F.eq.1.0_WP) return
  
  ! Increase diffusivity to account for non-unity Lewis numbers
  ! -> kSecond term of the Fuel mass fraction
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           DIFF(i,j,k,isc_ZMIX) = DIFF(i,j,k,isc_ZMIX) / Le_F
        end do
     end do
  end do
  
  return
end subroutine premixed_diffusivity_update


! ========================== !
! Ignite the flow everywhere !
! ========================== !
subroutine premixed_ignite
  use premixed
  implicit none
  
  integer :: i,j,k
  real(WP) :: sc_min,sc_max
  
  ! For premixed flame force min/max progress variable
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           !SC(i,j,k,isc_ZMIX) = 0.0101
           call premixed_chemtable_prog_minmax(SC(i,j,k,isc_ZMIX),sc_min,sc_max)
           if (LVLSET(i,j,k).gt.0.0_WP) then
              SC(i,j,k,isc_PROG) = min(SC(i,j,k,isc_PROG),sc_max)
           else
              !SC(i,j,k,isc_PROG) = sc_min
           end if
        end do
     end do
  end do
  
  return
end subroutine premixed_ignite


! ==================================== !
! Simplified premixed chemtable lookup !
! ==================================== !
subroutine premixed_lookup(tag,Ain)
  use premixed
  implicit none
  
  character(len=*), intent(in) :: tag
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: Ain
  
  call premixed_chemtable_lookup(tag, Ain, &
       SC(:,:,:,isc_PROG), CVAR, SC(:,:,:,isc_ZMIX), nxo_*nyo_*nzo_)
  
  return
end subroutine premixed_lookup


