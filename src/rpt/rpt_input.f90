module rpt_input
  use parser
  use string
  use math
  implicit none
  
  ! Files info
  character(len=str_medium) :: part_file_init
  character(len=str_medium) :: part_file
  
  ! Particle density
  real(WP) :: rho_part
  
  ! Solver parameters
  integer :: rpt_substeps
  
  ! Collision parameters
  real(WP) :: e_n,e_w,dt_col
  
  ! Gravity
  logical :: gravity_is_defined
  real(WP), dimension(3) :: gravity

  ! Are particles static
  logical :: is_static
  
end module rpt_input

! Read the input file
subroutine rpt_input_read
  use rpt_input
  use geometry
  use ib
  implicit none
  
  ! Part files
  call parser_read('Part file to read',part_file_init)
  call parser_read('Part file to write',part_file)

  ! Gravity
  call parser_is_defined('Gravity',gravity_is_defined)
  if (gravity_is_defined) then
     call parser_read('Gravity',gravity)
  else
     gravity = 0.0_WP
  end if
  
  ! Read particle density
  call parser_read('Particle density',rho_part)
  
  ! Collision
  call parser_read('Restitution coeff',e_n)
  call parser_read('Wall restitution',e_w,0.8_WP)
  call parser_read('Collision time',dt_col)
  
  ! Solver parameters
  call parser_read('Number of substeps',rpt_substeps)
  
  ! Are particles static
  call parser_read('Static particles',is_static,.false.)
  if (.not.is_static) ibmove=.true.
  
  return
end subroutine rpt_input_read
