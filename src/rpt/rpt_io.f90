module rpt_io
  
  use rpt_com
  use rpt
  use parser
  use parallel

  implicit none

end module rpt_io

! Read particle file
subroutine rpt_read
  use rpt_io
  use data
  use time_info
  implicit none
  
  integer :: ierr,file
  integer, dimension(MPI_STATUS_SIZE) :: status
  real(WP) :: time_read
  integer :: part_size
  character(len=str_medium) :: filename
  logical :: file_is_there
  
  ! Open the file
  inquire(file=trim(part_file_init),exist=file_is_there)
  if (.not.file_is_there) return
  filename= trim(mpiiofs) // trim(part_file_init)
  call MPI_file_open(comm,filename,MPI_MODE_RDONLY,mpi_info,file,ierr)
  if (ierr .ne. 0) return
  
  ! Read number of particles
  call MPI_file_read_all(file,npart,1,MPI_INTEGER,status,ierr)
  call MPI_file_read_all(file,part_size,1,MPI_INTEGER,status,ierr)
  call MPI_file_read_all(file,time_read,1,MPI_REAL_WP,status,ierr)
  if (part_size.ne.SIZE_MPI_PARTICLE) call die("Particle type unreadable")
  
  ! Allocate memory for particles
  allocate(part(npart))
  
  ! Read this file
  call MPI_file_read_all(file,part(1:npart),npart,MPI_particle,status,ierr)
  
  ! Close the file
  call MPI_file_close(file,ierr)
  
  return
end subroutine rpt_read

! Write particle file
subroutine rpt_write(flag)
  use rpt_io
  use data
  use time_info
  implicit none
  
  integer :: ierr,file,part_size
  logical :: overwrite
  integer, dimension(MPI_STATUS_SIZE) :: status
  logical, intent(in) :: flag
  character(len=str_medium) :: filename,buffer
 
  ! Check wether spray is used
  if (.not.use_rpt) return
  
  ! If not iroot, return
  if (irank.ne.iroot) return
  
  ! Check if we need to dump particle data
  if (int(time/data_freq).ne.data_last .or. flag) then
     
     ! Get the name of the file to write to
     call parser_read('Part file to write',filename)
     filename = trim(filename)

     ! Add time info to the file name
     call parser_read('Data overwrite',overwrite,.true.)
     if (.not.overwrite) then
        write(buffer,'(ES12.3)') time
        filename = trim(adjustl(filename))//'_'//trim(adjustl(buffer))
     end if
  
     ! Open the file to write
     call BINARY_FILE_OPEN(file,trim(adjustl(filename)),"w",ierr)

     ! Write header corresponding to proc
     part_size = 0
     if (npart.gt.0) part_size = sizeof(part(1))
     call BINARY_FILE_WRITE(file,npart,1,kind(npart),status,ierr)                                      
     call BINARY_FILE_WRITE(file,part_size,1,kind(part_size),status,ierr)                              
     call BINARY_FILE_WRITE(file,time,1,kind(time),status,ierr) 
     
     ! Write local particles
     if (npart>0) call BINARY_FILE_WRITE(file,part,npart,part_size,ierr)
     
     ! Close the file 
     call BINARY_FILE_CLOSE(file,ierr)
     
     ! Log
     call monitor_log("BINARY PART FILE WRITTEN")
     
  end if
  
  return
end subroutine rpt_write

! Ascii routine
subroutine rpt_write_asci(flag)
  use rpt_io
  use time_info
  use data
  use fileio
  implicit none

  logical, intent(in) :: flag
  character(len=str_medium) :: filename,buffer
  integer :: n,iunit,ierr
  logical :: overwrite
 
  ! Check wether spray is used
  if (.not.use_rpt) return
  
  ! If not iroot, return
  if (irank.ne.iroot) return
  
  ! Check if we need to dump particle data
  if (int(time/data_freq).ne.data_last .or. flag) then
     
     ! Get the name of the file to write to
     call parser_read('Part file to write',filename)
     filename = trim(filename)
     
     ! Add time info to the file name
     call parser_read('Data overwrite',overwrite,.true.)
     if (.not.overwrite) then
        write(buffer,'(ES12.3)') time
        filename = trim(adjustl(filename))//'_'//trim(adjustl(buffer))
     end if
     
     ! Open the file to write
     iunit = iopen()
     open(iunit,file=trim(adjustl(filename)),form="formatted",iostat=ierr,status="REPLACE")
     
     ! Write data
     ! Number of particles
     write(iunit,'(i12)') npart
     ! location and velocity
     do n=1,npart
        write(iunit,'(i12,7(ES12.5))') n,part(n)%fcol,part(n)%x,part(n)%y,part(n)%z,part(n)%u,part(n)%v,part(n)%w
     end do
     
     ! close file
     close(iclose(iunit))

     ! Log
     call monitor_log("ASCII PART FILE WRITTEN")
     
  end if

  return
end subroutine rpt_write_asci
