! ========================== !
! Resolved particle tracking !
! ========================== !
module rpt
  use rpt_mod
  use rpt_input
  use precision
  use geometry
  use data

  ! Maximum particle diameter
  real(WP) :: dmax

  ! Particles close to processor domain
  integer, dimension(:), allocatable :: part_in
  integer :: np_in
  
  ! Characteristic mesh size
  real(WP) :: mesh_size
    
end module rpt

! ============================================== !
! Initialization for resolved particles routines !
! ============================================== !
subroutine rpt_init
  use rpt
  use parser
  implicit none

  character(len=str_medium) :: buf
  integer :: n
  
  ! Check whether spray is used
  call parser_read('IB geometry',buf)
  select case (trim(buf))
  case ('rpt')
     use_rpt = .true.
  case default
     use_rpt = .false.
  end select

  ! If no rpt, return
  if (.not.use_rpt) return
  
  ! Create & start the timer
  call timing_create('rpt')
  call timing_start ('rpt')
  
  ! Initialize MPI
  allocate(part(2))
  call rpt_mpi_prepare ! Old MPI-1.2 standard
  !call rpt_mpi_prepare2 ! New MPI-2 standard to account for 64 bits machines
  deallocate(part)
  nullify(part)
  
  ! Set characteristic mesh size
  mesh_size=max(maxval(dx),maxval(dy),dz)
  
  ! Read the input parameters
  call rpt_input_read
  
  ! Read the init particle file
  call rpt_read
  
  ! Get maximum diameter
  dmax = -huge(1.0_WP)
  do n=1,npart
     dmax = max(dmax,part(n)%d)
  end do
    
  ! Allocate array of particles relevant to current proc
  allocate(part_in(npart))

  ! Initialize close particles array
  call rpt_get_close_part
  
  ! Monitor the particles
  call monitor_create_file_step('rpt',7)
  call monitor_set_header(1,'np'  ,'i')
  call monitor_set_header(2,'<U>' ,'r')
  call monitor_set_header(3,'<V>' ,'r')
  call monitor_set_header(4,'<W>' ,'r')
  call monitor_set_header(5,'<Fx>','r')
  call monitor_set_header(6,'<Fy>','r')
  call monitor_set_header(7,'<Fz>','r')
  
  ! Stop a timer
  call timing_stop('rpt')
  
  return
end subroutine rpt_init

! =========================== !
! Advance the spray equations !
! =========================== !
subroutine rpt_step
  use rpt
  use parser
  use parallel
  use time_info
  implicit none
  
  ! Check whether spray is used
  if (.not.use_rpt) return
  
  ! Start a timer
  call timing_start('rpt')
  
  ! Update close particles array
  call rpt_get_close_part
  
  ! Update drag
  call rpt_drag_update
  
  ! Advance particle position
  call rpt_advance_particle
  
  ! Perform I/O
  call rpt_write(.false.)
  
  ! Stop a timer
  call timing_stop('rpt')
  
  return
end subroutine rpt_step

! ======================== !
! Compute collision forces !
! ======================== !
subroutine rpt_collision_force
  use rpt
  use math
  implicit none

  integer :: i,j
  real(WP) :: mi,mj,mij,dij,delta_ij,k_n,eta_n,r_influ
  real(WP), dimension(3) :: pos,vel,fcol,nij,vij 
  
  ! Zero collision force
  do i=1,npart
     part(i)%fcol = 0.0_WP
  end do
  
  ! Loop over particles
  do i=1,npart

     ! Particle-wall collision
     if (yper.eq.0) then
        ! Lower wall
        ! Distance between i and j
        pos(1) = 0.0_WP
        pos(2) = part(i)%y-y(2)
        pos(3) = 0.0_WP
        call rpt_per_dist(pos(1),pos(2),pos(3))
        dij = sqrt(sum(pos**2))
        
        ! Velocity
        vel(1) = part(i)%u
        vel(2) = part(i)%v
        vel(3) = part(i)%w
        
        ! Distance of influence
        r_influ = mesh_size
        !r_influ = 0.075_WP*0.5_WP*(part(i)%d+part(j)%d)
        delta_ij = 0.5_WP*part(i)%d + r_influ - dij
        
        ! If they are, add collision force to drag force
        if (delta_ij.gt.0.0_WP) then
           
           ! Reduced mass
           mi = rho_part*(pi/6.0_WP)*part(i)%d**3
           mij = mi
           
           ! Collision parameters
           k_n = mij/dt_col**2*(pi**2+log(e_w)**2)
           eta_n = (-2.0_WP*log(e_w)*sqrt(mij*k_n))/(sqrt(Pi**2+log(e_w)**2))
           
           ! Normal vector and velocity
           nij = pos/dij
           vij = sum(vel*nij) * nij
           
           ! Collision force
           fcol = (k_n*delta_ij*nij - eta_n*vij)
           
           ! Update drag force
           part(i)%fcol = part(i)%fcol + fcol
           
        end if

        ! Upper wall
        ! Distance between i and j
        pos(1) = 0.0_WP
        pos(2) = part(i)%y-y(ny)
        pos(3) = 0.0_WP
        call rpt_per_dist(pos(1),pos(2),pos(3))
        dij = sqrt(sum(pos**2))
        
        ! Velocity
        vel(1) = part(i)%u
        vel(2) = part(i)%v
        vel(3) = part(i)%w
        
        ! Distance of influence
        r_influ = mesh_size
        !r_influ = 0.075_WP*0.5_WP*(part(i)%d+part(j)%d)
        delta_ij = 0.5_WP*part(i)%d + r_influ - dij
        
        ! If they are, add collision force to drag force
        if (delta_ij.gt.0.0_WP) then
           
           ! Reduced mass
           mi = rho_part*(pi/6.0_WP)*part(i)%d**3
           mij = mi
           
           ! Collision parameters
           k_n = mij/dt_col**2*(pi**2+log(e_w)**2)
           eta_n = (-2.0_WP*log(e_w)*sqrt(mij*k_n))/(sqrt(Pi**2+log(e_w)**2))
           
           ! Normal vector and velocity
           nij = pos/dij
           vij = sum(vel*nij) * nij
           
           ! Collision force
           fcol = (k_n*delta_ij*nij - eta_n*vij)
           
           ! Update drag force
           part(i)%fcol = part(i)%fcol + fcol
           
        end if
     end if
     
     ! Particle-particle collision
     do j=i+1,npart
        
        ! Distance between i and j
        pos(1) = part(i)%x-part(j)%x
        pos(2) = part(i)%y-part(j)%y
        pos(3) = part(i)%z-part(j)%z
        call rpt_per_dist(pos(1),pos(2),pos(3))
        dij = sqrt(sum(pos**2))
        
        ! Velocity
        vel(1) = part(i)%u-part(j)%u
        vel(2) = part(i)%v-part(j)%v
        vel(3) = part(i)%w-part(j)%w
        
        ! Distance of influence
        r_influ = 2.0_WP*mesh_size
        !r_influ = 0.075_WP*0.5_WP*(part(i)%d+part(j)%d)
        delta_ij = 0.5_WP*(part(i)%d + part(j)%d) + r_influ - dij
        
        ! If they are, add collision force to drag force
        if (delta_ij.gt.0.0_WP) then
           
           ! Reduced mass
           mi = rho_part*(pi/6.0_WP)*part(i)%d**3
           mj = rho_part*(pi/6.0_WP)*part(j)%d**3
           mij = 1.0_WP/(1.0_WP/mi + 1.0_WP/mj)
           
           ! Collision parameters
           k_n = mij/dt_col**2*(pi**2+log(e_n)**2)
           eta_n = (-2.0_WP*log(e_n)*sqrt(mij*k_n))/(sqrt(Pi**2+log(e_n)**2))
           
           ! Normal vector and velocity
           nij = pos/dij
           vij = sum(vel*nij) * nij
           
           ! Collision force
           fcol = (k_n*delta_ij*nij - eta_n*vij)
           
           ! Update drag force
           part(i)%fcol = part(i)%fcol + fcol
           part(j)%fcol = part(j)%fcol - fcol
           
        end if
     end do
  end do
  
  return
end subroutine rpt_collision_force

! ======================== !
! Update particle position !
! ======================== !
subroutine rpt_advance_particle
  use rpt
  use math
  use simulation
  use parallel
  implicit none
  
  integer :: n,i
  real(WP) :: tstep
  real(WP) :: mass
  real(WP), dimension(3) :: force
  
  if (is_static) return
  
  ! Timestep for particle advancement
  tstep=dt_uvw/real(rpt_substeps,WP)
  
  ! Advance particle equation
  do i=1,rpt_substeps
     
     ! Compute collision force
     call rpt_collision_force
     
     ! Update particle location
     do n=1,npart
        
        ! Create mass
        mass = pi/6.0_WP*part(n)%d**3 * rho_part
        
        ! Create full force
        force = (part(n)%fcol + mass*gravity + part(n)%drag_p + part(n)%drag_v)/mass
        
        ! Position
        part(n)%x = part(n)%x + part(n)%u*tstep + 0.5_WP*force(1) * tstep**2
        part(n)%y = part(n)%y + part(n)%v*tstep + 0.5_WP*force(2) * tstep**2
        part(n)%z = part(n)%z + part(n)%w*tstep + 0.5_WP*force(3) * tstep**2
        
        ! Velocity
        part(n)%u = part(n)%u + force(1) * tstep
        part(n)%v = part(n)%v + force(2) * tstep
        part(n)%w = part(n)%w + force(3) * tstep
        
        ! Deal with periodicity
        call rpt_per_pos(part(n)%x,part(n)%y,part(n)%z)
        
     end do
     
  end do
  
  return
end subroutine rpt_advance_particle

! =========================================== !
! Find particles relevant to processor        !
! Note: could be used for geometry update too !
! =========================================== !
subroutine rpt_get_close_part
  use rpt
  implicit none

  integer :: n
  real(WP) :: px,py,pz,pxp,pyp,pzp,pxm,pym,pzm
  real(WP) :: x1,x2,y1,y2,z1,z2

!!$  ! Loop over particles and choose the ones close to proc
!!$  np_in = 0
!!$  part_in = 0
!!$  do n=1,npart
!!$
!!$     ! Particle position
!!$     px = part(n)%x
!!$     py = part(n)%y
!!$     pz = part(n)%z
!!$
!!$     ! Periodicity in + direction
!!$     pxp = part(n)%x+xL
!!$     pyp = part(n)%y+yL
!!$     pzp = part(n)%z+zL
!!$
!!$     ! Periodicity in - direction
!!$     pxm = part(n)%x-xL
!!$     pym = part(n)%y-yL
!!$     pzm = part(n)%z-zL
!!$
!!$     ! Domain boundaries
!!$     x1 = x(imino_)-dmax/2.0_WP
!!$     x2 = x(imaxo_)+dmax/2.0_WP
!!$     y1 = y(jmino_)-dmax/2.0_WP
!!$     y2 = y(jmaxo_)+dmax/2.0_WP
!!$     z1 = z(kmino_)-dmax/2.0_WP
!!$     z2 = z(kmaxo_)+dmax/2.0_WP
!!$     
!!$     ! Add particle to inside list
!!$     if ( ((px.gt.x1.and.px.lt.x2).or.(pxp.gt.x1.and.pxp.lt.x2).or.(pxm.gt.x1.and.pxm.lt.x2)) .and. &
!!$          ((py.gt.y1.and.py.lt.y2).or.(pyp.gt.y1.and.pyp.lt.y2).or.(pym.gt.y1.and.pym.lt.y2)) .and. &
!!$          ((pz.gt.z1.and.pz.lt.z2).or.(pzp.gt.z1.and.pzp.lt.z2).or.(pzm.gt.z1.and.pzm.lt.z2)) ) then
!!$        
!!$        np_in = np_in + 1
!!$        part_in(np_in) = n
!!$        
!!$     end if
!!$     
!!$  end do
  
  ! In case there's no close particle, revert to full list
  !if (np_in.eq.0) then
     do n=1,npart
        part_in(n)=n
     end do
     np_in=npart
  !end if
  
  return
end subroutine rpt_get_close_part

! ======================================================= !
! Interaction routine returning the distance function Gib !
! ======================================================= !
subroutine rpt_get_distance(Gib)
  use rpt
  use parallel
  use math
  implicit none

  integer :: i,j,k,n
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_), intent(out) :: Gib
  real(WP) :: distx,disty,distz,rad,dist
  
  ! Loop over all cells in processor
  do k=kmino_,kmaxo_
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           
           ! Initialize distance       
           dist = huge(1.0_WP)
           
           ! Loop over close particles
           do n=1,np_in
              
              ! Get distance to particle center
              distx = xm(i)-part(part_in(n))%x 
              disty = ym(j)-part(part_in(n))%y
              distz = zm(k)-part(part_in(n))%z
              rad   = part(part_in(n))%d/2.0_WP
              
              ! Apply periodicity
              call rpt_per_dist(distx,disty,distz)
              
              ! Get signed distance to particle interface and compare to current
              dist=min(dist,sqrt(distx**2+disty**2+distz**2)-rad)
              
           end do
           
           ! Store min distance in Gib
           Gib(i,j,k) = dist
           
        end do
     end do
  end do
  
  return
end subroutine rpt_get_distance
  
! =================================================== !
! Interaction routine returning the particle velocity !
! =================================================== !
subroutine rpt_get_velocity(ig,jg,kg,Uib,Vib,Wib)
  use rpt
  use parallel
  use math
  implicit none

  integer, intent(in) :: ig,jg,kg
  real(WP), intent(out) :: Uib,Vib,Wib
  integer :: n,n1,n2
  real(WP) :: dist1,dist2,new_dist
  real(WP) :: distx,disty,distz,rad
  
  ! Initialize distance
  dist1 = huge(1.0_WP)
  dist2 = huge(1.0_WP)
  
  ! Loop over close particles
  do n=1,np_in
     
     ! Get distance to particle center
     distx = xm(ig)-part(part_in(n))%x 
     disty = ym(jg)-part(part_in(n))%y
     distz = zm(kg)-part(part_in(n))%z
     rad   = part(part_in(n))%d/2.0_WP
          
     ! Apply periodicity
     call rpt_per_dist(distx,disty,distz)
     
     ! Get signed distance to particle interface and compare to current
     new_dist = sqrt(distx**2+disty**2+distz**2)-rad
     if (new_dist.lt.dist1) then
        dist2=dist1
        n2=n1
        dist1=new_dist
        n1=part_in(n)
     else if (new_dist.lt.dist2) then
        dist2=new_dist
        n2=part_in(n)
     end if
     
  end do
  
  ! Set velocity
  if (abs(dist1-dist2).lt.mesh_size) then
     Uib = 0.5_WP*(part(n1)%u+part(n2)%u)
     Vib = 0.5_WP*(part(n1)%v+part(n2)%v)
     Wib = 0.5_WP*(part(n1)%w+part(n2)%w)
  else
     Uib = part(n1)%u
     Vib = part(n1)%v
     Wib = part(n1)%w
  end if
  
  return
end subroutine rpt_get_velocity

! ==================== !
! Last call : save all !
! ==================== !
subroutine rpt_finalize
  use rpt
  implicit none
  
  ! Check wether rpt is used
  if (.not.use_rpt) return
  
  ! Dump all the particles to a file for restart
  call rpt_write(.true.)
  
  return
end subroutine rpt_finalize

! ========================================== !
! Take care of periodicity based on distance !
! ========================================== ! 
subroutine rpt_per_dist(posx, posy, posz)
  use geometry
  implicit none
  real(WP) :: posx,posy,posz
  
  if (abs(posx).gt.abs(posx-xL).and.xper.eq.1) posx=posx-xL
  if (abs(posx).gt.abs(posx+xL).and.xper.eq.1) posx=posx+xL
  if (abs(posy).gt.abs(posy-yL).and.yper.eq.1) posy=posy-yL
  if (abs(posy).gt.abs(posy+yL).and.yper.eq.1) posy=posy+yL
  if (abs(posz).gt.abs(posz-zL).and.zper.eq.1) posz=posz-zL
  if (abs(posz).gt.abs(posz+zL).and.zper.eq.1) posz=posz+zL
  
  return
end subroutine rpt_per_dist

! ========================================== !
! Take care of periodicity based on position !
! ========================================== ! 
subroutine rpt_per_pos(posx, posy, posz)
  use geometry
  implicit none
  real(WP) :: posx,posy,posz
  
  if (xper.eq.1.and.posx.ge.x(imax+1)) posx=posx-xL
  if (yper.eq.1.and.posy.ge.y(jmax+1)) posy=posy-yL
  if (zper.eq.1.and.posz.ge.z(kmax+1)) posz=posz-zL
  if (xper.eq.1.and.posx.lt.x(imin))   posx=posx+xL
  if (yper.eq.1.and.posy.lt.y(jmin))   posy=posy+yL
  if (zper.eq.1.and.posz.lt.z(kmin))   posz=posz+zL
  
  return
end subroutine rpt_per_pos


! ===================================== !
! Monitoring of the first two particles !
! ===================================== !
subroutine rpt_monitor
  use rpt
  implicit none
  
  integer :: i
  real(WP) :: meanU ,meanV ,meanW
  real(WP) :: meanFx,meanFy,meanFz
  
  ! Check wether rpt is used
  if (.not.use_rpt) return
  
  ! Requires at least one particle
  if (npart.lt.1) return
  
  ! Compute mean quantities
  meanU =0.0_WP; meanV =0.0_WP; meanW =0.0_WP
  meanFx=0.0_WP; meanFy=0.0_WP; meanFz=0.0_WP
  do i=1,npart
     meanU =meanU +part(i)%u
     meanV =meanV +part(i)%v
     meanW =meanW +part(i)%w
     meanFx=meanFx+part(i)%drag_p(1)+part(i)%drag_v(1)
     meanFy=meanFy+part(i)%drag_p(2)+part(i)%drag_v(2)
     meanFz=meanFz+part(i)%drag_p(3)+part(i)%drag_v(3)
  end do
  meanU =meanU /real(npart,WP)
  meanV =meanV /real(npart,WP)
  meanW =meanW /real(npart,WP)
  meanFx=meanFx/real(npart,WP)
  meanFy=meanFy/real(npart,WP)
  meanFz=meanFz/real(npart,WP)
  
  ! Transfer data to monitor
  call monitor_select_file('rpt')
  call monitor_set_single_value(1,real(npart,WP))
  call monitor_set_single_value(2,meanU)
  call monitor_set_single_value(3,meanV)
  call monitor_set_single_value(4,meanW)
  call monitor_set_single_value(5,meanFx)
  call monitor_set_single_value(6,meanFy)
  call monitor_set_single_value(7,meanFz)
  
  return
end subroutine rpt_monitor
