module rpt_drag
  use precision
  use rpt
  implicit none
  
end module rpt_drag

! =============================== !
! Compute drag force of particles !
! =============================== !
subroutine rpt_drag_update
  use rpt_drag
  use ib
  implicit none
  
  ! Still to do
  ! partager si plusieurs particles dans la meme cellule
  ! trouver critere pour que 2 particles intersectent la meme cellule
  
  integer :: i,j,k,n
  integer :: my_n,my_nx,my_ny,my_nz
  real(WP) :: distx,disty,distz,rad,dist
  real(WP) :: my_dist,my_distx,my_disty,my_distz
  real(WP), dimension(3) :: drag
  
  ! Zero drag force
  do i=1,npart
     part(i)%drag_v = 0.0_WP
     part(i)%drag_p = 0.0_WP
  end do
  
  ! Loop over all cells in processor
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           
           ! ------------------------------------------------- !
           ! Pressure and Viscous drag force - Stored at faces !
           ! ------------------------------------------------- !
           if (PFx(i,j,k).ne.0.0_WP.or.PFy(i,j,k).ne.0.0_WP.or.PFz(i,j,k).ne.0.0_WP.or.&
               VFx(i,j,k).ne.0.0_WP.or.VFy(i,j,k).ne.0.0_WP.or.VFz(i,j,k).ne.0.0_WP) then
              
              ! Initialize distance       
              my_distx = huge(1.0_WP)
              my_disty = huge(1.0_WP)
              my_distz = huge(1.0_WP)
              my_nx = 0
              my_ny = 0
              my_nz = 0
              
              ! Loop over close particles
              do n=1,np_in
                 
                 ! Particle radius
                 rad   = part(part_in(n))%d/2.0_WP    
                 
                 ! --- x --- !
                 ! Get distance to particle faces
                 distx = x(i) -part(part_in(n))%x 
                 disty = ym(j)-part(part_in(n))%y
                 distz = zm(k)-part(part_in(n))%z
                 
                 ! Apply periodicity
                 call rpt_per_dist(distx,disty,distz)
                 
                 ! Get distance to particle interface and compare to current
                 dist = abs(sqrt(distx**2+disty**2+distz**2)-rad)
                 if (dist.lt.my_distx) then
                    my_distx = dist
                    my_nx = part_in(n)
                 end if
                 
                 ! --- y --- !
                 ! Get distance to particle faces
                 distx = xm(i)-part(part_in(n))%x 
                 disty = y(j) -part(part_in(n))%y
                 distz = zm(k)-part(part_in(n))%z
                 
                 ! Apply periodicity
                 call rpt_per_dist(distx,disty,distz)
                 
                 ! Get distance to particle interface and compare to current
                 dist = abs(sqrt(distx**2+disty**2+distz**2)-rad)
                 if (dist.lt.my_disty) then
                    my_disty = dist
                    my_ny = part_in(n)
                 end if
                 
                 ! --- z --- !
                 ! Get distance to particle faces
                 distx = xm(i)-part(part_in(n))%x 
                 disty = ym(j)-part(part_in(n))%y
                 distz = z(k) -part(part_in(n))%z
                 
                 ! Apply periodicity
                 call rpt_per_dist(distx,disty,distz)
                 
                 ! Get distance to particle interface and compare to current
                 dist = abs(sqrt(distx**2+disty**2+distz**2)-rad)
                 if (dist.lt.my_distz) then
                    my_distz = dist
                    my_nz = part_in(n)
                 end if
                 
              end do
              
              ! Store pressure force element in right particle
              part(my_nx)%drag_p(1)=part(my_nx)%drag_p(1)+PFx(i,j,k)
              part(my_ny)%drag_p(2)=part(my_ny)%drag_p(2)+PFy(i,j,k)
              part(my_nz)%drag_p(3)=part(my_nz)%drag_p(3)+PFz(i,j,k)
              
              ! Store viscous force element in right particle
              part(my_nx)%drag_v(1)=part(my_nx)%drag_v(1)+VFx(i,j,k)
              part(my_ny)%drag_v(2)=part(my_ny)%drag_v(2)+VFy(i,j,k)
              part(my_nz)%drag_v(3)=part(my_nz)%drag_v(3)+VFz(i,j,k)
              
           end if
           
        end do
     end do
  end do
  
  ! Add contributions from different processors and update total force on particle
  do n=1,npart
     drag=part(n)%drag_p; call parallel_sum(drag,part(n)%drag_p)
     drag=part(n)%drag_v; call parallel_sum(drag,part(n)%drag_v)
  end do
  
  return
end subroutine rpt_drag_update
