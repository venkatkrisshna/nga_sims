module rpt_com
  use rpt
  use parallel
  implicit none
  
  ! MPI structure
  integer :: MPI_PARTICLE
  integer :: SIZE_MPI_PARTICLE

end module rpt_com
  
! Old MPI-1.2 standard that doesn't account for 64 bits machines
subroutine rpt_mpi_prepare
  use rpt_com
  implicit none
  
  integer, dimension(17) :: types,lengths,displacement
  integer :: base,ierr,i
  
  ! Create the MPI structure to send particles
  types( 1:16) = MPI_REAL_WP
  types(17)    = MPI_UB
  lengths(:)   = 1
  
  !! Count the displacement for this structure
  !call MPI_address(part(1),           base,             ierr)
  !! Position - cartesian
  !call MPI_address(part(1)%x,         displacement(1),  ierr)
  !call MPI_address(part(1)%y,         displacement(2),  ierr)
  !call MPI_address(part(1)%z,         displacement(3),  ierr)
  !! Velocity - cartesian
  !call MPI_address(part(1)%u,         displacement(4),  ierr)
  !call MPI_address(part(1)%v,         displacement(5),  ierr)
  !call MPI_address(part(1)%w,         displacement(6),  ierr)
  !! Radius
  !call MPI_address(part(1)%d,         displacement(7),  ierr)
  !! Total force
  !call MPI_address(part(1)%f(1),      displacement(8), ierr)
  !call MPI_address(part(1)%f(2),      displacement(9), ierr)
  !call MPI_address(part(1)%f(3),      displacement(10), ierr)
  !! Pressure Drag 
  !call MPI_address(part(1)%drag_v(1), displacement(11), ierr)
  !call MPI_address(part(1)%drag_v(2), displacement(12), ierr)
  !call MPI_address(part(1)%drag_v(3), displacement(13), ierr)
  !! Viscous Drag
  !call MPI_address(part(1)%drag_p(1), displacement(14), ierr)
  !call MPI_address(part(1)%drag_p(2), displacement(15), ierr)
  !call MPI_address(part(1)%drag_p(3), displacement(16), ierr)
  !! Next
  !call MPI_address(part(2),           displacement(17), ierr)
  !displacement=displacement-base

  !if (irank.eq.iroot) then
  !   write(*,*) "displacement( 1)=",displacement( 1)
  !   write(*,*) "displacement( 2)=",displacement( 2)
  !   write(*,*) "displacement( 3)=",displacement( 3)
  !   write(*,*) "displacement( 4)=",displacement( 4)
  !   write(*,*) "displacement( 5)=",displacement( 5)
  !   write(*,*) "displacement( 6)=",displacement( 6)
  !   write(*,*) "displacement( 7)=",displacement( 7)
  !   write(*,*) "displacement( 8)=",displacement( 8)
  !   write(*,*) "displacement( 9)=",displacement( 9)
  !   write(*,*) "displacement(10)=",displacement(10)
  !   write(*,*) "displacement(11)=",displacement(11)
  !   write(*,*) "displacement(12)=",displacement(12)
  !   write(*,*) "displacement(13)=",displacement(13)
  !   write(*,*) "displacement(14)=",displacement(14)
  !   write(*,*) "displacement(15)=",displacement(15)
  !   write(*,*) "displacement(16)=",displacement(16)
  !   write(*,*) "displacement(17)=",displacement(17)
  !end if

  displacement( 1)=           0
  displacement( 2)=           8
  displacement( 3)=          16
  displacement( 4)=          24
  displacement( 5)=          32
  displacement( 6)=          40
  displacement( 7)=          48
  displacement( 8)=          56
  displacement( 9)=          64
  displacement(10)=          72
  displacement(11)=          80
  displacement(12)=          88
  displacement(13)=          96
  displacement(14)=         104 
  displacement(15)=         112
  displacement(16)=         120
  displacement(17)=         128
      
  ! Finalize by creating and commiting the new type
  call mpi_type_create_struct(17,lengths,displacement,types,MPI_PARTICLE,ierr)
  call MPI_Type_commit(MPI_PARTICLE,ierr)
  
  ! If problem, say it
  if (ierr==1) call die("Problem with MPI_PARTICLE")
  
  ! Get the size of this type
  call MPI_type_size(MPI_PARTICLE,SIZE_MPI_PARTICLE,ierr)
  
  return
end subroutine rpt_mpi_prepare

!!$subroutine rpt_mpi_prepare2 : NOT UP-TO-DATE!!!
!!$  use rpt_com
!!$  implicit none
!!$  
!!$  integer(KIND=MPI_ADDRESS_KIND), dimension(11) :: displacement
!!$  integer, dimension(11) :: types,lengths
!!$  integer(KIND=MPI_ADDRESS_KIND) :: base
!!$  integer :: ierr
!!$  
!!$  ! Create the MPI structure to send particles
!!$  types( 1:10) = MPI_REAL_WP
!!$  types(11)    = MPI_UB
!!$  lengths(:)   = 1
!!$  
!!$  ! Count the displacement for this structure
!!$  call MPI_get_address(part(1),         base,             ierr)
!!$  ! Position - cartesian
!!$  call MPI_get_address(part(1)%x,       displacement(1),  ierr)
!!$  call MPI_get_address(part(1)%y,       displacement(2),  ierr)
!!$  call MPI_get_address(part(1)%z,       displacement(3),  ierr)
!!$  ! Velocity - cartesian
!!$  call MPI_get_address(part(1)%u,       displacement(4),  ierr)
!!$  call MPI_get_address(part(1)%v,       displacement(5),  ierr)
!!$  call MPI_get_address(part(1)%w,       displacement(6),  ierr)
!!$  ! Radius
!!$  call MPI_get_address(part(1)%r,       displacement(7),  ierr)
!!$  ! Drag force
!!$  call MPI_get_address(part(1)%drag(1), displacement(8), ierr)
!!$  call MPI_get_address(part(1)%drag(2), displacement(9), ierr)
!!$  call MPI_get_address(part(1)%drag(3), displacement(10), ierr)
!!$  ! Next
!!$  call MPI_get_address(part(2),         displacement(11), ierr)
!!$  displacement=displacement-base
!!$  
!!$  ! Finalize by creating and commiting the new type
!!$  call MPI_Type_create_struct(11,lengths,displacement,types,MPI_PARTICLE,ierr)
!!$  call MPI_Type_commit(MPI_PARTICLE,ierr)
!!$  
!!$  ! If problem, say it
!!$  if (ierr==1) call die("Problem with MPI_PARTICLE")
!!$  
!!$  ! Get the size of this type
!!$  call MPI_type_size(MPI_PARTICLE,SIZE_MPI_PARTICLE,ierr)
!!$  
!!$  return
!!$end subroutine rpt_mpi_prepare2

