module rpt_mod
  use precision
  implicit none

  ! Particle type
  type part_type
     ! Position (Cartesian)
     real(WP) :: x
     real(WP) :: y
     real(WP) :: z
     ! Velocity (Cartesian)
     real(WP) :: u
     real(WP) :: v
     real(WP) :: w
     ! Diameter
     real(WP) :: d     
     ! Collision forces
     real(WP), dimension(3) :: fcol
     ! Drag forces
     real(WP), dimension(3) :: drag_v
     real(WP), dimension(3) :: drag_p
  end type part_type
  
  ! Particle array and sizes
  type(part_type), dimension(:), pointer :: part
  integer :: npart
  
end module rpt_mod
