module sgs_adhoc2
  use sgsmodel
  implicit none
  
  real(WP) :: fdiff
  
end module sgs_adhoc2


! ============================== !
! Initialize the Adhoc2 SGS model !
! ============================== !
subroutine sgs_adhoc2_init
  use sgs_adhoc2
  use parser
  implicit none
  
  real(WP) :: sig1,sig2,C
  real(WP) :: delta
      
  ! Read the filter size
  call parser_read('SGS Ad-Hoc2 filter size',delta,3.0_WP*min_meshsize)  
  
  ! Compute filter properties
  C=1.0_WP/(2.0_WP*sqrt(2.0_WP*log(2.0_WP)))
  sig1=min_meshsize*C
  sig2=delta*C
  fdiff=0.5_WP*(4.0_WP*sig2**2-sig1**2)
  
  return
end subroutine sgs_adhoc2_init


! ============================================== !
! Average LM and MM over time for Eddy Viscosity !
! ============================================== !
subroutine sgs_adhoc2_average_VISC
  use sgs_adhoc2
  implicit none
  
  ! The filter operation requires values in the ghost cells
  call boundary_update_border(LM,'ym','+')
  call boundary_update_border(MM,'ym','+')
  
  call sgs_adhoc2_smooth(LM)
  call sgs_adhoc2_smooth(MM)
    
  return
end subroutine sgs_adhoc2_average_VISC


! ================================================ !
! Average LM and MM over time for Eddy Diffusivity !
! ================================================ !
subroutine sgs_adhoc2_average_DIFF
  use sgs_adhoc2
  implicit none
  
  ! The filter operation requires values in the ghost cells
  call boundary_update_border(LM,'ym','+')
  call boundary_update_border(MM,'ym','+')
  
  call sgs_adhoc2_smooth(LM)
  call sgs_adhoc2_smooth(MM)
  
  return
end subroutine sgs_adhoc2_average_DIFF


! =============================================== !
! Average LM and MM over time for Scalar Variance !
! =============================================== !
subroutine sgs_adhoc2_average_ZVAR
  use sgs_adhoc2
  implicit none
    
  ! The filter operation requires values in the ghost cells
  call boundary_update_border(LM,'ym','+')
  call boundary_update_border(MM,'ym','+')
  
  call sgs_adhoc2_smooth(LM)
  call sgs_adhoc2_smooth(MM)
  
  return
end subroutine sgs_adhoc2_average_ZVAR


! =================== !
! Laplacian Smoothing !
! =================== !
subroutine sgs_adhoc2_smooth(A)
  use sgs_adhoc2
  use memory
  use metric_generic
  implicit none
  
  integer :: i,j,k
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_) :: A
  
  ! Inverse in X-direction
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           Ax(j,k,i,-1) = - div_u(i,j,k,0) * fdiff * grad_x(i,j,-1)
           Ax(j,k,i, 0) = 1.0_WP - (div_u(i,j,k,0) * fdiff * grad_x(i,j,0) &
                                  + div_u(i,j,k,1) * fdiff * grad_x(i+1,j,-1))
           Ax(j,k,i,+1) = - div_u(i,j,k,1) * fdiff * grad_x(i+1,j, 0)
           Rx(j,k,i) = A(i,j,k)
        end do
     end do
  end do
  call linear_solver_x(3)
  
  ! Inverse in Y-direction
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           Ay(i,k,j,-1) = - div_v(i,j,k,0) * fdiff * grad_y(i,j,-1)
           Ay(i,k,j, 0) = 1.0_WP - (div_v(i,j,k,0)* fdiff * grad_y(i,j,0) &
                                  + div_v(i,j,k,1)* fdiff * grad_y(i,j+1,-1))
           Ay(i,k,j,+1) = - div_v(i,j,k,1) * fdiff * grad_y(i,j+1,0)
           Ry(i,k,j) = Rx(j,k,i)
        end do
     end do
  end do
  call linear_solver_y(3)
  
  ! Inverse in Z-direction
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           Az(i,j,k,-1) = - div_w(i,j,k,0) * fdiff * grad_z(i,j,-1)
           Az(i,j,k, 0) = 1.0_WP - (div_w(i,j,k,0) * fdiff * grad_z(i,j,0) &
                                  + div_w(i,j,k,1) * fdiff * grad_z(i,j,-1))
           Az(i,j,k,+1) = - div_w(i,j,k,1) * fdiff * grad_z(i,j,0)
           Rz(i,j,k) = Ry(i,k,j)
        end do
     end do
  end do
  call linear_solver_z(3)
  
  ! Update A
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           A(i,j,k)=Rz(i,j,k)
        end do
     end do
  end do
  
  ! Communicate
  call boundary_update_border(A,'ym','+')
  
  return
end subroutine sgs_adhoc2_smooth
