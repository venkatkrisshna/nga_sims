! ==================================== !
! Transpose module for real numbers    !
! Parallel implementation assuming one !
! non-decomposed direction at least    !
! ==================================== !
module transpose
  use precision
  use config
  use partition
  use metric_generic
  implicit none

  ! Transpose partition - X
  integer, dimension(:), allocatable :: imin_x,imax_x
  integer, dimension(:), allocatable :: jmin_x,jmax_x
  integer, dimension(:), allocatable :: kmin_x,kmax_x
  integer, dimension(:), allocatable :: nx_x,ny_x,nz_x
  real(WP), dimension(:,:,:,:), allocatable :: sendbuf_x,recvbuf_x
  integer :: sendcount_x,recvcount_x
  character(len=str_medium) :: idir_x

  ! Transpose partition - Y
  integer, dimension(:), allocatable :: imin_y,imax_y
  integer, dimension(:), allocatable :: jmin_y,jmax_y
  integer, dimension(:), allocatable :: kmin_y,kmax_y
  integer, dimension(:), allocatable :: nx_y,ny_y,nz_y
  real(WP), dimension(:,:,:,:), allocatable :: sendbuf_y,recvbuf_y
  integer :: sendcount_y,recvcount_y
  character(len=str_medium) :: idir_y

  ! Transpose partition - Z
  integer, dimension(:), allocatable :: imin_z,imax_z
  integer, dimension(:), allocatable :: jmin_z,jmax_z
  integer, dimension(:), allocatable :: kmin_z,kmax_z
  integer, dimension(:), allocatable :: nx_z,ny_z,nz_z
  real(WP), dimension(:,:,:,:), allocatable :: sendbuf_z,recvbuf_z
  integer :: sendcount_z,recvcount_z
  character(len=str_medium) :: idir_z

  ! Only initialize once
  logical :: init_x=.false.
  logical :: init_y=.false.
  logical :: init_z=.false.

end module transpose


! =============================== !
! Initialize the transpose module !
! =============================== !
subroutine transpose_init(dir)
  use transpose
  use parser
  use parallel
  implicit none

  integer :: ip,jp,kp,ierr
  integer :: q,r
  character(len=*) :: dir

  ! Check if already initialized
  if (trim(adjustl(dir)).eq.'x' .and. init_x) return
  if (trim(adjustl(dir)).eq.'y' .and. init_y) return
  if (trim(adjustl(dir)).eq.'z' .and. init_z) return
  if (trim(adjustl(dir)).eq.'x') init_x=.true.
  if (trim(adjustl(dir)).eq.'y') init_y=.true.
  if (trim(adjustl(dir)).eq.'z') init_z=.true.

  ! Initialize direction for transpose
  select case (trim(adjustl(dir)))
  case ('x')
     ! Determine initial non-decomposed direction used in transpose
     if (npx.eq.1) then
        idir_x="xdir"
     else if (npy.eq.1 .and. ny.gt.1) then
        idir_x="ydir"
     else if (npz.eq.1 .and. nz.gt.1) then
        idir_x="zdir"
     else if (npy.eq.1) then
        idir_x="ydir"
     else if (npz.eq.1) then
        idir_x="zdir"
     else
        call die('transpose_init: at least one direction non-decomposed required')
     end if

     ! Allocate global partitions
     allocate(nx_x(npx))
     allocate(ny_x(npx))
     allocate(nz_x(npx))
     allocate(imin_x(npx))
     allocate(imax_x(npx))
     allocate(jmin_x(npx))
     allocate(jmax_x(npx))
     allocate(kmin_x(npx))
     allocate(kmax_x(npx))
     
     ! Partition
     select case (trim(idir_x))
     case ('xdir')
        
        ! No transpose required
        
        ! Local partition
        nx_x = nx_
        ny_x = ny_
        nz_x = nz_
        imin_x = imin_
        imax_x = imax_
        jmin_x = jmin_
        jmax_x = jmax_
        kmin_x = kmin_
        kmax_x = kmax_
        
     case ('ydir')
        
        ! Store old local indices from each processor
        call MPI_AllGather(imin_,1,MPI_INTEGER,imin_x,1,MPI_INTEGER,comm_x,ierr)
        call MPI_AllGather(imax_,1,MPI_INTEGER,imax_x,1,MPI_INTEGER,comm_x,ierr)
        nx_x=imax_x-imin_x+1
        
        ! Partition new local indices
        do ip=1,npx
           q = ny/npx
           r = mod(ny,npx)
           if (ip<=r) then
              ny_x(ip)   = q+1
              jmin_x(ip) = jmin + (ip-1)*(q+1)
           else
              ny_x(ip)   = q
              jmin_x(ip) = jmin + r*(q+1) + (ip-r-1)*q
           end if
           jmax_x(ip) = jmin_x(ip) + ny_x(ip) - 1
        end do
        nz_x = nz_
        kmin_x = kmin_
        kmax_x = kmax_
        
        ! Variables for AllToAll communication
        sendcount_x = maxval(nx_x)*maxval(ny_x)*nz_
        recvcount_x = maxval(nx_x)*maxval(ny_x)*nz_
        allocate(sendbuf_x(maxval(nx_x),maxval(ny_x),kmin_:kmax_,npx))
        allocate(recvbuf_x(maxval(nx_x),maxval(ny_x),kmin_:kmax_,npx))
        
        ! Pad buffers with zeros
        sendbuf_x=0.0_WP
        recvbuf_x=0.0_WP
        
     case ('zdir')
        
        ! Store old local indices from each processor
        call MPI_AllGather(imin_,1,MPI_INTEGER,imin_x,1,MPI_INTEGER,comm_x,ierr)
        call MPI_AllGather(imax_,1,MPI_INTEGER,imax_x,1,MPI_INTEGER,comm_x,ierr)
        nx_x=imax_x-imin_x+1
        
        ! Partition new local indices
        do ip=1,npx
           q = nz/npx
           r = mod(nz,npx)
           if (ip<=r) then
              nz_x(ip)   = q+1
              kmin_x(ip) = kmin + (ip-1)*(q+1)
           else
              nz_x(ip)   = q
              kmin_x(ip) = kmin + r*(q+1) + (ip-r-1)*q
           end if
           kmax_x(ip) = kmin_x(ip) + nz_x(ip) - 1
        end do
        ny_x = ny_
        jmin_x = jmin_
        jmax_x = jmax_
        
        ! Variables for AllToAll communication
        sendcount_x = maxval(nx_x)*ny_*maxval(nz_x)
        recvcount_x = maxval(nx_x)*ny_*maxval(nz_x)
        allocate(sendbuf_x(maxval(nx_x),jmin_:jmax_,maxval(nz_x),npx))
        allocate(recvbuf_x(maxval(nx_x),jmin_:jmax_,maxval(nz_x),npx))
        
        ! Pad buffers with zeros
        sendbuf_x=0.0_WP
        recvbuf_x=0.0_WP
        
     end select
     
  case ('y')
     ! Determine initial non-decomposed direction used in transpose
     if (npy.eq.1) then
        idir_y="ydir"
     else if (npx.eq.1 .and. nx.gt.1) then
        idir_y="xdir"
     else if (npz.eq.1 .and. nz.gt.1) then
        idir_y="zdir"
     else if (npx.eq.1) then
        idir_y="xdir"
     else if (npz.eq.1) then
        idir_y="zdir"
     else
        call die('transpose_init: at least one direction non-decomposed required')
     end if

     ! Allocate global partitions
     allocate(nx_y(npy))
     allocate(ny_y(npy))
     allocate(nz_y(npy))
     allocate(imin_y(npy))
     allocate(imax_y(npy))
     allocate(jmin_y(npy))
     allocate(jmax_y(npy))
     allocate(kmin_y(npy))
     allocate(kmax_y(npy))

     ! Partition
     select case (trim(idir_y))
     case ('xdir')
        
        ! Store old local indices from each processor
        call MPI_AllGather(jmin_,1,MPI_INTEGER,jmin_y,1,MPI_INTEGER,comm_y,ierr)
        call MPI_AllGather(jmax_,1,MPI_INTEGER,jmax_y,1,MPI_INTEGER,comm_y,ierr)
        ny_y=jmax_y-jmin_y+1
        
        ! Partition new local indices
        do jp=1,npy
           q = nx/npy
           r = mod(nx,npy)
           if (jp<=r) then
              nx_y(jp)   = q+1
              imin_y(jp) = imin + (jp-1)*(q+1)
           else
              nx_y(jp)   = q
              imin_y(jp) = imin + r*(q+1) + (jp-r-1)*q
           end if
           imax_y(jp) = imin_y(jp) + nx_y(jp) - 1
        end do
        nz_y = nz_
        kmin_y = kmin_
        kmax_y = kmax_
        
        ! Variables for AllToAll communication
        sendcount_y = maxval(nx_y)*maxval(ny_y)*nz_
        recvcount_y = maxval(nx_y)*maxval(ny_y)*nz_
        allocate(sendbuf_y(maxval(nx_y),maxval(ny_y),kmin_:kmax_,npy))
        allocate(recvbuf_y(maxval(nx_y),maxval(ny_y),kmin_:kmax_,npy))
        
        ! Pad buffers with zeros
        sendbuf_y=0.0_WP
        recvbuf_y=0.0_WP
        
     case ('ydir')
        
        ! No transpose required
        
        ! Local partition
        nx_y = nx_
        ny_y = ny_
        nz_y = nz_
        imin_y = imin_
        imax_y = imax_
        jmin_y = jmin_
        jmax_y = jmax_
        kmin_y = kmin_
        kmax_y = kmax_
        
     case ('zdir')
        
        ! Store old local indices from each processor
        call MPI_AllGather(jmin_,1,MPI_INTEGER,jmin_y,1,MPI_INTEGER,comm_y,ierr)
        call MPI_AllGather(jmax_,1,MPI_INTEGER,jmax_y,1,MPI_INTEGER,comm_y,ierr)
        ny_y=jmax_y-jmin_y+1
        
        ! Partition new local indices
        do jp=1,npy
           q = nz/npy
           r = mod(nz,npy)
           if (jp<=r) then
              nz_y(jp)   = q+1
              kmin_y(jp) = kmin + (jp-1)*(q+1)
           else
              nz_y(jp)   = q
              kmin_y(jp) = kmin + r*(q+1) + (jp-r-1)*q
           end if
           kmax_y(jp) = kmin_y(jp) + nz_y(jp) - 1
        end do
        nx_y = nx_
        imin_y = imin_
        imax_y = imax_
        
        ! Variables for AllToAll communication
        sendcount_y = nx_*maxval(ny_y)*maxval(nz_y)
        recvcount_y = nx_*maxval(ny_y)*maxval(nz_y)
        allocate(sendbuf_y(imin_:imax_,maxval(ny_y),maxval(nz_y),npy))
        allocate(recvbuf_y(imin_:imax_,maxval(ny_y),maxval(nz_y),npy))
        
        ! Pad buffers with zeros
        sendbuf_y=0.0_WP
        recvbuf_y=0.0_WP
        
     end select
     
  case ('z')
     ! Determine initial non-decomposed direction used in transpose
     if (npz.eq.1) then
        idir_z="zdir"
     else if (npy.eq.1 .and. ny.gt.1) then
        idir_z="ydir"
     else if (npx.eq.1 .and. nx.gt.1) then
        idir_z="xdir"
     else if (npy.eq.1) then
        idir_z="ydir"
     else if (npx.eq.1) then
        idir_z="xdir"
     else
        call die('transpose_init: at least one direction non-decomposed required')
     end if

     ! Allocate global partitions
     allocate(nx_z(npz))
     allocate(ny_z(npz))
     allocate(nz_z(npz))
     allocate(imin_z(npz))
     allocate(imax_z(npz))
     allocate(jmin_z(npz))
     allocate(jmax_z(npz))
     allocate(kmin_z(npz))
     allocate(kmax_z(npz))

     ! Partition
     select case (trim(idir_z))
     case ('xdir')
        
        ! Store old local indices from each processor
        call MPI_AllGather(kmin_,1,MPI_INTEGER,kmin_z,1,MPI_INTEGER,comm_z,ierr)
        call MPI_AllGather(kmax_,1,MPI_INTEGER,kmax_z,1,MPI_INTEGER,comm_z,ierr)
        nz_z=kmax_z-kmin_z+1
        
        ! Partition new local indices
        do kp=1,npz
           q = nx/npz
           r = mod(nx,npz)
           if (kp<=r) then
              nx_z(kp)   = q+1
              imin_z(kp) = imin + (kp-1)*(q+1)
           else
              nx_z(kp)   = q
              imin_z(kp) = imin + r*(q+1) + (kp-r-1)*q
           end if
           imax_z(kp) = imin_z(kp) + nx_z(kp) - 1
        end do
        ny_z = ny_
        jmin_z = jmin_
        jmax_z = jmax_
        
        ! Variables for AllToAll communication
        sendcount_z = maxval(nx_z)*ny_*maxval(nz_z)
        recvcount_z = maxval(nx_z)*ny_*maxval(nz_z)
        allocate(sendbuf_z(maxval(nx_z),jmin_:jmax_,maxval(nz_z),npz))
        allocate(recvbuf_z(maxval(nx_z),jmin_:jmax_,maxval(nz_z),npz))
        
        ! Pad buffers with zeros
        sendbuf_z=0.0_WP
        recvbuf_z=0.0_WP
        
     case ('ydir')
        
        ! Store old local indices from each processor
        call MPI_AllGather(kmin_,1,MPI_INTEGER,kmin_z,1,MPI_INTEGER,comm_z,ierr)
        call MPI_AllGather(kmax_,1,MPI_INTEGER,kmax_z,1,MPI_INTEGER,comm_z,ierr)
        nz_z=kmax_z-kmin_z+1

        ! Partition new local indices
        do kp=1,npz
           q = ny/npz
           r = mod(ny,npz)
           if (kp<=r) then
              ny_z(kp)   = q+1
              jmin_z(kp) = jmin + (kp-1)*(q+1)
           else
              ny_z(kp)   = q
              jmin_z(kp) = jmin + r*(q+1) + (kp-r-1)*q
           end if
           jmax_z(kp) = jmin_z(kp) + ny_z(kp) - 1
        end do
        nx_z = nx_
        imin_z = imin_
        imax_z = imax_
        
        ! Variables for AllToAll communication
        sendcount_z = nx_*maxval(ny_z)*maxval(nz_z)
        recvcount_z = nx_*maxval(ny_z)*maxval(nz_z)
        allocate(sendbuf_z(imin_:imax_,maxval(ny_z),maxval(nz_z),npz))
        allocate(recvbuf_z(imin_:imax_,maxval(ny_z),maxval(nz_z),npz))
     
        ! Pad buffers with zeros
        sendbuf_z=0.0_WP
        recvbuf_z=0.0_WP
        
     case ('zdir')
        
        ! No transpose required
        
        ! Local partition
        nx_z = nx_
        ny_z = ny_
        nz_z = nz_
        imin_z = imin_
        imax_z = imax_
        jmin_z = jmin_
        jmax_z = jmax_
        kmin_z = kmin_
        kmax_z = kmax_
        
     end select
     
  case default
     call die('transpose_init: unknown direction')
  end select
  
  return
end subroutine transpose_init


! ========================== !
! Transpose A ==> At so      !
! contiguous in X direction  !
! ========================== !
subroutine transpose_x(A,At)
  use transpose
  use parallel
  implicit none

  real(WP), dimension(imin_:imax_,jmin_:jmax_,kmin_:kmax_), intent(in) :: A
  real(WP), dimension(imin:imax,jmin_x(iproc):jmax_x(iproc),kmin_x(iproc):kmax_x(iproc)), intent(out) :: At
  integer :: i,j,k,ip,ii,jj,kk,ierr
     
  select case (trim(idir_x))
  case ('xdir')
     
     ! No transpose required
     At = A
     
  case ('ydir')
     
     ! Transpose x==>y
     do ip=1,npx
        do k=kmin_,kmax_
           do j=jmin_x(ip),jmax_x(ip)
              do i=imin_,imax_
                 jj=j-jmin_x(ip)+1
                 ii=i-imin_+1
                 sendbuf_x(ii,jj,k,ip) = A(i,j,k)
              end do
           end do
        end do
     end do
     
     call MPI_AllToAll(sendbuf_x,sendcount_x,MPI_REAL_WP,&
          recvbuf_x,recvcount_x,MPI_REAL_WP,comm_x,ierr)
     
     do ip=1,npx
        do k=kmin_,kmax_
           do j=jmin_x(iproc),jmax_x(iproc)
              do i=imin_x(ip),imax_x(ip)
                 jj=j-jmin_x(iproc)+1
                 ii=i-imin_x(ip)+1
                 At(i,j,k)=recvbuf_x(ii,jj,k,ip)
              end do
           end do
        end do
     end do
     
  case ('zdir')
     
     ! Transpose x==>z
     do ip=1,npx
        do k=kmin_x(ip),kmax_x(ip)
           do j=jmin_,jmax_
              do i=imin_,imax_
                 kk=k-kmin_x(ip)+1
                 ii=i-imin_+1
                 sendbuf_x(ii,j,kk,ip) = A(i,j,k)
              end do
           end do
        end do
     end do
     
     call MPI_AllToAll(sendbuf_x,sendcount_x,MPI_REAL_WP,&
          recvbuf_x,recvcount_x,MPI_REAL_WP,comm_x,ierr)
     
     do ip=1,npx
        do k=kmin_x(iproc),kmax_x(iproc)
           do j=jmin_,jmax_
              do i=imin_x(ip),imax_x(ip)
                 kk=k-kmin_x(iproc)+1
                 ii=i-imin_x(ip)+1
                 At(i,j,k)=recvbuf_x(ii,j,kk,ip)
              end do
           end do
        end do
     end do
     
  end select

  return
end subroutine transpose_x


! ========================== !
! Transpose A ==> At so      !
! contiguous in Y direction  !
! ========================== !
subroutine transpose_y(A,At)
  use transpose
  use parallel
  implicit none

  real(WP), dimension(imin_:imax_,jmin_:jmax_,kmin_:kmax_), intent(in) :: A
  real(WP), dimension(imin_y(jproc):imax_y(jproc),jmin:jmax,kmin_y(jproc):kmax_y(jproc)), intent(out) :: At
  integer :: i,j,k,jp,ii,jj,kk,ierr

  select case (trim(idir_y))
  case ('xdir')
     
     ! Transpose y==>x
     do jp=1,npy
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_y(jp),imax_y(jp)
                 ii=i-imin_y(jp)+1
                 jj=j-jmin_+1
                 sendbuf_y(ii,jj,k,jp) = A(i,j,k)
              end do
           end do
        end do
     end do
     
     call MPI_AllToAll(sendbuf_y,sendcount_y,MPI_REAL_WP,&
          recvbuf_y,recvcount_y,MPI_REAL_WP,comm_y,ierr)
     
     do jp=1,npy
        do k=kmin_,kmax_
           do j=jmin_y(jp),jmax_y(jp)
              do i=imin_y(jproc),imax_y(jproc)
                 ii=i-imin_y(jproc)+1
                 jj=j-jmin_y(jp)+1
                 At(i,j,k)=recvbuf_y(ii,jj,k,jp)
              end do
           end do
        end do
     end do
     
  case ('ydir')
     
     ! No transpose required
     At = A
     
  case ('zdir')
     
     ! Transpose y==>z
     do jp=1,npy
        do k=kmin_y(jp),kmax_y(jp)
           do j=jmin_,jmax_
              do i=imin_,imax_
                 kk=k-kmin_y(jp)+1
                 jj=j-jmin_+1
                 sendbuf_y(i,jj,kk,jp) = A(i,j,k)
              end do
           end do
        end do
     end do
     
     call MPI_AllToAll(sendbuf_y,sendcount_y,MPI_REAL_WP,&
          recvbuf_y,recvcount_y,MPI_REAL_WP,comm_y,ierr)
     
     do jp=1,npy
        do k=kmin_y(jproc),kmax_y(jproc)
           do j=jmin_y(jp),jmax_y(jp)
              do i=imin_,imax_
                 kk=k-kmin_y(jproc)+1
                 jj=j-jmin_y(jp)+1
                 At(i,j,k)=recvbuf_y(i,jj,kk,jp)
              end do
           end do
        end do
     end do
        
  end select

  return
end subroutine transpose_y
 

! ========================== !
! Transpose A ==> At so      !
! contiguous in Z direction  !
! ========================== !
subroutine transpose_z(A,At)
  use transpose
  use parallel
  implicit none

  real(WP), dimension(imin_:imax_,jmin_:jmax_,kmin_:kmax_), intent(in) :: A
  real(WP), dimension(imin_z(kproc):imax_z(kproc),jmin_z(kproc):jmax_z(kproc),kmin:kmax), intent(out) :: At
  integer :: i,j,k,kp,ii,jj,kk,ierr

  select case (trim(idir_z))
  case ('xdir')
        
     ! Transpose z==>x
     do kp=1,npz
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_z(kp),imax_z(kp)
                 ii=i-imin_z(kp)+1
                 kk=k-kmin_+1
                 sendbuf_z(ii,j,kk,kp) = A(i,j,k)
              end do
           end do
        end do
     end do
     
     call MPI_AllToAll(sendbuf_z,sendcount_z,MPI_REAL_WP,&
          recvbuf_z,recvcount_z,MPI_REAL_WP,comm_z,ierr)
     
     do kp=1,npz
        do k=kmin_z(kp),kmax_z(kp)
           do j=jmin_,jmax_
              do i=imin_z(kproc),imax_z(kproc)
                 ii=i-imin_z(kproc)+1
                 kk=k-kmin_z(kp)+1
                 At(i,j,k)=recvbuf_z(ii,j,kk,kp)
              end do
           end do
        end do
     end do
     
  case ('ydir')
     
      ! Transpose z==>y
     do kp=1,npz
        do k=kmin_,kmax_
           do j=jmin_z(kp),jmax_z(kp)
              do i=imin_,imax_
                 jj=j-jmin_z(kp)+1
                 kk=k-kmin_+1
                 sendbuf_z(i,jj,kk,kp) = A(i,j,k)
              end do
           end do
        end do
     end do

     call MPI_AllToAll(sendbuf_z,sendcount_z,MPI_REAL_WP,&
          recvbuf_z,recvcount_z,MPI_REAL_WP,comm_z,ierr)

     do kp=1,npz
        do k=kmin_z(kp),kmax_z(kp)
           do j=jmin_z(kproc),jmax_z(kproc)
              do i=imin_,imax_
                 jj=j-jmin_z(kproc)+1
                 kk=k-kmin_z(kp)+1
                 At(i,j,k)=recvbuf_z(i,jj,kk,kp)
              end do
           end do
        end do
     end do
     
  case ('zdir')
     
     ! No transpose required
     At = A
     
  end select

  return
end subroutine transpose_z


! ========================!
! Back transpose At ==> A !
! from X direction        !
! ======================= !
subroutine btranspose_x(At,A)
  use transpose
  use parallel
  implicit none

  real(WP), dimension(imin:imax,jmin_x(iproc):jmax_x(iproc),kmin_x(iproc):kmax_x(iproc)), intent(in) :: At
  real(WP), dimension(imin_:imax_,jmin_:jmax_,kmin_:kmax_), intent(out) :: A
  integer :: i,j,k,ip,ii,jj,kk,ierr
     
  select case (trim(idir_x))
  case ('xdir')
     
     ! No transpose required
     A = At
     
  case ('ydir')
     
     ! Transpose y==>x
     do ip=1,npx
        do k=kmin_,kmax_
           do j=jmin_x(iproc),jmax_x(iproc)
              do i=imin_x(ip),imax_x(ip)
                 jj=j-jmin_x(iproc)+1
                 ii=i-imin_x(ip)+1
                 sendbuf_x(ii,jj,k,ip) = At(i,j,k)
              end do
           end do
        end do
     end do
     
     call MPI_AllToAll(sendbuf_x,sendcount_x,MPI_REAL_WP,&
          recvbuf_x,recvcount_x,MPI_REAL_WP,comm_x,ierr)
     
     do ip=1,npx
        do k=kmin_,kmax_
           do j=jmin_x(ip),jmax_x(ip)
              do i=imin_x(iproc),imax_x(iproc)
                 jj=j-jmin_x(ip)+1
                 ii=i-imin_x(iproc)+1
                 A(i,j,k)=recvbuf_x(ii,jj,k,ip)
              end do
           end do
        end do
     end do
     
  case ('zdir')
     
     ! Transpose z==>x
     do ip=1,npx
        do k=kmin_x(iproc),kmax_x(iproc)
           do j=jmin_,jmax_
              do i=imin_x(ip),imax_x(ip)
                 kk=k-kmin_x(iproc)+1
                 ii=i-imin_x(ip)+1
                 sendbuf_x(ii,j,kk,ip) = At(i,j,k)
              end do
           end do
        end do
     end do
     
     call MPI_AllToAll(sendbuf_x,sendcount_x,MPI_REAL_WP,&
          recvbuf_x,recvcount_x,MPI_REAL_WP,comm_x,ierr)
     
     do ip=1,npx
        do k=kmin_x(ip),kmax_x(ip)
           do j=jmin_,jmax_
              do i=imin_x(iproc),imax_x(iproc)
                 kk=k-kmin_x(ip)+1
                 ii=i-imin_x(iproc)+1
                 A(i,j,k)=recvbuf_x(ii,j,kk,ip)
              end do
           end do
        end do
     end do
     
  end select
  
  return
end subroutine btranspose_x


! ========================!
! Back transpose At ==> A !
! from Y direction        !
! ======================= !
subroutine btranspose_y(At,A)
  use transpose
  use parallel
  implicit none

  real(WP), dimension(imin_y(jproc):imax_y(jproc),jmin:jmax,kmin_y(jproc):kmax_y(jproc)), intent(in) :: At
  real(WP), dimension(imin_:imax_,jmin_:jmax_,kmin_:kmax_), intent(out) :: A
  integer :: i,j,k,jp,ii,jj,kk,ierr

  select case (trim(idir_y))
  case ('xdir')
        
     ! Transpose x==>y
     do jp=1,npy
        do k=kmin_,kmax_
           do j=jmin_y(jp),jmax_y(jp)
              do i=imin_y(jproc),imax_y(jproc)
                 ii=i-imin_y(jproc)+1
                 jj=j-jmin_y(jp)+1
                 sendbuf_y(ii,jj,k,jp) = At(i,j,k)
              end do
           end do
        end do
     end do
     
     call MPI_AllToAll(sendbuf_y,sendcount_y,MPI_REAL_WP,&
          recvbuf_y,recvcount_y,MPI_REAL_WP,comm_y,ierr)
     
     do jp=1,npy
        do k=kmin_,kmax_
           do j=jmin_y(jproc),jmax_y(jproc)
              do i=imin_y(jp),imax_y(jp)
                 ii=i-imin_y(jp)+1
                 jj=j-jmin_y(jproc)+1
                 A(i,j,k)=recvbuf_y(ii,jj,k,jp)
              end do
           end do
        end do
     end do
     
  case ('ydir')
     
     ! No transpose required
     A = At
     
  case ('zdir')
     
     ! Transpose z==>y
     do jp=1,npy
        do k=kmin_y(jproc),kmax_y(jproc)
           do j=jmin_y(jp),jmax_y(jp)
              do i=imin_,imax_
                 kk=k-kmin_y(jproc)+1
                 jj=j-jmin_y(jp)+1
                 sendbuf_y(i,jj,kk,jp) = At(i,j,k)
              end do
           end do
        end do
     end do
        
     call MPI_AllToAll(sendbuf_y,sendcount_y,MPI_REAL_WP,&
          recvbuf_y,recvcount_y,MPI_REAL_WP,comm_y,ierr)
     
     do jp=1,npy
        do k=kmin_y(jp),kmax_y(jp)
           do j=jmin_y(jproc),jmax_y(jproc)
              do i=imin_,imax_
                 kk=k-kmin_y(jp)+1
                 jj=j-jmin_y(jproc)+1
                 A(i,j,k)=recvbuf_y(i,jj,kk,jp)
              end do
           end do
        end do
     end do
        
  end select
  
  return
end subroutine btranspose_y

! ========================!
! Back transpose At ==> A !
! from Z direction        !
! ======================= !
subroutine btranspose_z(At,A)
  use transpose
  use parallel
  implicit none

  real(WP), dimension(imin_z(kproc):imax_z(kproc),jmin_z(kproc):jmax_z(kproc),kmin:kmax), intent(in) :: At
  real(WP), dimension(imin_:imax_,jmin_:jmax_,kmin_:kmax_), intent(out) :: A
  integer :: i,j,k,kp,ii,jj,kk,ierr

  select case (trim(idir_z))
  case ('xdir')
     
     ! Transpose x==>z
     do kp=1,npz
        do k=kmin_z(kp),kmax_z(kp)
           do j=jmin_,jmax_
              do i=imin_z(kproc),imax_z(kproc)
                 ii=i-imin_z(kproc)+1
                 kk=k-kmin_z(kp)+1
                 sendbuf_z(ii,j,kk,kp) = At(i,j,k)
              end do
           end do
        end do
     end do
     
     call MPI_AllToAll(sendbuf_z,sendcount_z,MPI_REAL_WP,&
          recvbuf_z,recvcount_z,MPI_REAL_WP,comm_z,ierr)
     
     do kp=1,npz
        do k=kmin_z(kproc),kmax_z(kproc)
           do j=jmin_,jmax_
              do i=imin_z(kp),imax_z(kp)
                 ii=i-imin_z(kp)+1
                 kk=k-kmin_z(kproc)+1
                 A(i,j,k)=recvbuf_z(ii,j,kk,kp)
              end do
           end do
        end do
     end do
     
  case ('ydir')
     
     ! Transpose y==>z
     do kp=1,npz
        do k=kmin_z(kp),kmax_z(kp)
           do j=jmin_z(kproc),jmax_z(kproc)
              do i=imin_,imax_
                 jj=j-jmin_z(kproc)+1
                 kk=k-kmin_z(kp)+1
                 sendbuf_z(i,jj,kk,kp) = At(i,j,k)
              end do
           end do
        end do
     end do
        
     call MPI_AllToAll(sendbuf_z,sendcount_z,MPI_REAL_WP,&
          recvbuf_z,recvcount_z,MPI_REAL_WP,comm_z,ierr)
     
     do kp=1,npz
        do k=kmin_z(kproc),kmax_z(kproc)
           do j=jmin_z(kp),jmax_z(kp)
              do i=imin_,imax_
                 jj=j-jmin_z(kp)+1
                 kk=k-kmin_z(kproc)+1
                 A(i,j,k)=recvbuf_z(i,jj,kk,kp)
              end do
           end do
        end do
     end do
     
  case ('zdir')
     
     ! No transpose required
     A = At
     
  end select
  
  return
end subroutine btranspose_z
