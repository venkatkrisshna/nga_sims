! ====================== !
! Interpolation routines !
! ====================== !
module interpolate
  use geometry
  use partition
  implicit none
  
  ! Cell centered velocities
  real(WP), dimension(:,:,:), allocatable :: Ui
  real(WP), dimension(:,:,:), allocatable :: Vi
  real(WP), dimension(:,:,:), allocatable :: Wi
  
end module interpolate


subroutine interpolate_init
  use interpolate
  implicit none
  
  allocate(Ui(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(Vi(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(Wi(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  Ui=0.0_WP
  Vi=0.0_WP
  Wi=0.0_WP
  
  return
end subroutine interpolate_init


subroutine interpolate_velocities
  use data
  use interpolate
  use metric_generic
  implicit none

  integer :: i,j,k
  
  ! Interpolate to cell centers
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           Ui(i,j,k) = sum(interp_u_xm(i,j,:) * U(i-st1:i+st2,j,k))
           Vi(i,j,k) = sum(interp_v_ym(i,j,:) * V(i,j-st1:j+st2,k))
           Wi(i,j,k) = sum(interp_w_zm(i,j,:) * W(i,j,k-st1:k+st2))
        end do
     end do
  end do
  
  ! Update ghost cells
  call boundary_update_border(Ui,'+','ym')
  call boundary_update_border(Vi,'-','ym')
  call boundary_update_border(Wi,'-','ym')
    
  ! Update geometric ghost cells
  call boundary_neumann(Ui,'+ym')
  call boundary_neumann(Ui,'-ym')
  call boundary_neumann(Ui,'+xm')
  call boundary_neumann(Ui,'-xm')
  call boundary_neumann(Vi,'+ym')
  call boundary_neumann(Vi,'-ym')
  call boundary_neumann(Vi,'+xm')
  call boundary_neumann(Vi,'-xm')
  call boundary_neumann(Wi,'+ym')
  call boundary_neumann(Wi,'-ym')
  call boundary_neumann(Wi,'+xm')
  call boundary_neumann(Wi,'-xm')
  
  return
end subroutine interpolate_velocities
