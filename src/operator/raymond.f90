! =================================== !
! Module implementing Raymond filters !
! =================================== !
module raymond
  use precision
  use geometry
  use partition
  implicit none
  
  ! Is Raymond being used?
  logical :: use_raymond=.false.

  ! Conservative 2nd order operator for the filter
  complex(WP), dimension(:,:,:,:), allocatable :: OP_x, OP_y, OP_z

  ! Coefficients for filter
  complex(WP), dimension(:,:,:), allocatable :: B_xr, B_yr, B_zr
    
end module 


! ====================== !
! Filters initialization !
! ====================== !
subroutine raymond_init
  use raymond
  use parser
  use parallel
  implicit none
  
  ! Initialize only once
  if (use_raymond) return
  
  ! Allocate operator
  allocate(OP_x(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,-1:1))
  allocate(OP_y(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,-1:1))
  allocate(OP_z(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,-1:1))

  allocate(B_xr(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(B_yr(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(B_zr(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))

  B_xr = 0.0_WP
  B_yr = 0.0_WP
  B_zr = 0.0_WP
 
  ! End initialization
  use_raymond = .true.
  
  return
end subroutine raymond_init

! ================== !
! Filter application !
! ================== !
subroutine raymond_filter(A,A_f,twoP,lc,sym,axis)
  use raymond
  use memory
  use parallel
  use math
  implicit none

  real(WP),dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_),intent(in) ::A
  real(WP),dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_),intent(out)::A_f
  integer, intent(in) :: twoP
  real(WP), intent(in) :: lc
  character(len=*), intent(in) :: sym, axis

  integer :: i,j,k,p,loop
  complex(WP), dimension(-1:1) :: foo
  complex(WP) ::  alpha

  ! Check the filter order
  if (mod(twoP,2).ne.0) call die('raymond_filter: filter order must be even')
  p=twoP/2

  ! Update the laplacian
  call raymond_operator(p,lc)

  ! Setup looping
  cRz = dcmplx(A(imin_:imax_,jmin_:jmax_,kmin_:kmax_))

  do loop=0,p-1

     ! Calculate the appropiate root of -1
     alpha = exp(dcmplx(0,-(Pi+loop*twoPi)/real(p,WP)))

     ! Filter in X-direction
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              foo(-1:1) = -alpha*OP_x(i,j,k,-1:1)
              foo(0) = foo(0) + dcmplx(1.0_WP)
              cAx(j,k,i,-1:1) = foo
              cRx(j,k,i) = cRz(i,j,k)
           end do
        end do
     end do
     if (nx.gt.1) then
        call clinear_solver_x(3)
     end if
     
     ! Filter in Y-direction
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              foo(-1:1) = -alpha*OP_y(i,j,k,-1:1)
              foo(0) = foo(0) + dcmplx(1.0_WP)
              cAy(i,k,j,-1:1) = foo
              cRy(i,k,j) = cRx(j,k,i)
           end do
        end do
     end do
     if (ny.gt.1) then
        call clinear_solver_y(3)
     end if
     
     ! Filter in Z-direction
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              foo(-1:1) = -alpha*OP_z(i,j,k,-1:1)
              foo(0) = foo(0) + dcmplx(1.0_WP)
              cAz(i,j,k,-1:1) = foo
              cRz(i,j,k) = cRy(i,k,j)
           end do
        end do
     end do
     if (nz.gt.1) then
        call clinear_solver_z(3)
     end if
     
     ! Update borders
     tmp10(imin_:imax_,jmin_:jmax_,kmin_:kmax_) = dreal(cRz)
     tmp11(imin_:imax_,jmin_:jmax_,kmin_:kmax_) = dimag(cRz)
     call boundary_update_border(tmp10,sym,axis)
     call boundary_update_border(tmp11,sym,axis)
     cRz = dcmplx(tmp10(imin_:imax_,jmin_:jmax_,kmin_:kmax_), &
                  tmp11(imin_:imax_,jmin_:jmax_,kmin_:kmax_))
  end do
  
  ! Update A
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           A_f(i,j,k)=dreal(cRz(i,j,k))
        end do
     end do
  end do
  
  ! Ghost Cells
  call boundary_update_border(A_f,sym,axis)
  
  return
end subroutine raymond_filter

! ========================================== !
! Create the conservative 2nd order operator !
! ========================================== !
subroutine raymond_operator(p,lc)
  use raymond
  use math
  use metric_generic
  use config
  use memory
  implicit none
  
  integer, intent(in) :: p
  real(WP), intent(in) :: lc

  ! Cutoff frequency
  real(WP) :: kc

  ! Filter coefficient
  complex(WP) :: beta, Bm, Bp
  real(WP) :: a0,am1,ap1

  complex(WP) :: ii
  integer :: i,j,k

  kc=Pi/lc

  ii = dcmplx(0.0_WP,1.0_WP)

  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
          ! Filter coefficient, X
          am1 = div_u(i,j,k,0)*grad_x(i  ,j,-1)
          a0  = div_u(i,j,k,0)*grad_x(i  ,j, 0) &
              + div_u(i,j,k,1)*grad_x(i+1,j,-1)
          ap1 = div_u(i,j,k,1)*grad_x(i+1,j, 0)
          ! Check for Neuman Conditions
          if(abs(am1).lt.epsilon(1.0_WP) .and. abs(ap1).lt.epsilon(1.0_WP)) then
            B_xr(i,j,k) = 0.0_WP
          else
            beta = dcmplx(1.0_WP)/(a0+am1*exp(-ii*kc*dxm(i))+ap1*exp(ii*kc*dxm(i+1)))**p
            B_xr(i,j,k) = exp(log(beta)/real(p,WP))
          end if

          ! Filter coefficient, Y
          am1 = div_v(i,j,k,0)*grad_y(i  ,j,-1)
          a0  = div_v(i,j,k,0)*grad_y(i  ,j, 0) &
              + div_v(i,j,k,1)*grad_y(i,j+1,-1)
          ap1 = div_v(i,j,k,1)*grad_y(i,j+1, 0)
          ! Check for Neuman Conditions
          if(abs(am1).lt.epsilon(1.0_WP) .and. abs(ap1).lt.epsilon(1.0_WP)) then
            B_yr(i,j,k) = 0.0_WP
          else
            beta = dcmplx(1.0_WP)/(a0+am1*exp(-ii*kc*dym(j))+ap1*exp(ii*kc*dym(j+1)))**p
            B_yr(i,j,k) = exp(log(beta)/real(p,WP))
          end if

          ! Filter coefficient, Z
          am1 = div_w(i,j,k,0)*grad_z(i  ,j,-1)
          a0  = div_w(i,j,k,0)*grad_z(i  ,j, 0) &
              + div_w(i,j,k,1)*grad_z(i  ,j,-1)
          ap1 = div_w(i,j,k,1)*grad_z(i  ,j, 0)
          ! Check for Neuman Conditions
          if(abs(am1).lt.epsilon(1.0_WP) .and. abs(ap1).lt.epsilon(1.0_WP)) then
            B_zr(i,j,k) = 0.0_WP
          else
            beta = dcmplx(1.0_WP)/(a0+am1*exp(-ii*kc*dz)+ap1*exp(ii*kc*dz))**p
            B_zr(i,j,k) = exp(log(beta)/real(p,WP))
          end if
        end do
    end do
  end do

  ! Update Ghost Cells
  tmp10 = dreal(B_xr)
  tmp11 = dimag(B_xr)
  call boundary_update_border(tmp10,'+','ym')
  call boundary_update_border(tmp11,'+','ym')
  B_xr = dcmplx(tmp10, tmp11)
  tmp10 = dreal(B_yr)
  tmp11 = dimag(B_yr)
  call boundary_update_border(tmp10,'-','y')
  call boundary_update_border(tmp11,'-','y')
  B_yr = dcmplx(tmp10, tmp11)
  tmp10 = dreal(B_zr)
  tmp11 = dimag(B_zr)
  call boundary_update_border(tmp10,'-','ym')
  call boundary_update_border(tmp11,'-','ym')
  B_zr = dcmplx(tmp10, tmp11)
  ! Correct boundaries if not periodic
  tmp10 = dreal(B_xr)
  tmp11 = dimag(B_xr)
  call boundary_neumann_alldir(tmp10)
  call boundary_neumann_alldir(tmp11)
  B_xr = dcmplx(tmp10, tmp11)
  tmp10 = dreal(B_yr)
  tmp11 = dimag(B_yr)
  call boundary_neumann_alldir(tmp10)
  call boundary_neumann_alldir(tmp11)
  B_yr = dcmplx(tmp10, tmp11)
  tmp10 = dreal(B_zr)
  tmp11 = dimag(B_zr)
  call boundary_neumann_alldir(tmp10)
  call boundary_neumann_alldir(tmp11)
  B_zr = dcmplx(tmp10, tmp11)
  
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
          ! Interpolate B to face
          Bm = sum(interp_sc_x(i,j,:)*B_xr(i-st2:i+st1,j,k))
          Bp = sum(interp_sc_x(i+1,j,:)*B_xr(i+1-st2:i+1+st1,j,k))
          ! Build the operator
          OP_x(i,j,k,-1) = div_u(i,j,k,0)*Bm*grad_x(i  ,j,-1)
          OP_x(i,j,k, 0) = div_u(i,j,k,0)*Bm*grad_x(i  ,j, 0) &
                         + div_u(i,j,k,1)*Bp*grad_x(i+1,j,-1)
          OP_x(i,j,k,+1) = div_u(i,j,k,1)*Bp*grad_x(i+1,j, 0)

          ! Interpolate B to face
          Bm = sum(interp_sc_y(i,j,:)*B_yr(i,j-st2:j+st1,k))
          Bp = sum(interp_sc_y(i,j,:)*B_yr(i,j+1-st2:j+1+st1,k))
          ! Build the operator
          OP_y(i,j,k,-1) = div_v(i,j,k,0)*Bm*grad_y(i,j  ,-1)
          OP_y(i,j,k, 0) = div_v(i,j,k,0)*Bm*grad_y(i,j  , 0) &
                         + div_v(i,j,k,1)*Bp*grad_y(i,j+1,-1)
          OP_y(i,j,k,+1) = div_v(i,j,k,1)*Bp*grad_y(i,j+1, 0)

          ! Interpolate B to face
          Bm = sum(interp_sc_z(i,j,:)*B_zr(i,j,k-st2:k+st1))
          Bp = sum(interp_sc_z(i,j,:)*B_zr(i,j,k+1-st2:k+1+st1))
          ! Build the operator
          OP_z(i,j,k,-1) = div_w(i,j,k,0)*Bm*grad_z(i,j,-1)
          OP_z(i,j,k, 0) = div_w(i,j,k,0)*Bm*grad_z(i,j, 0) &
                         + div_w(i,j,k,1)*Bp*grad_z(i,j,-1)
          OP_z(i,j,k,+1) = div_w(i,j,k,1)*Bp*grad_z(i,j, 0)

        end do
    end do
  end do

  return
end subroutine raymond_operator
