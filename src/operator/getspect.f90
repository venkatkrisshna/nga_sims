! ========================================= !
! Power Spectral Density calculation module !
! ========================================= !
module getspect
  use precision
  use partition
  use geometry
  use parallel
  implicit none
  
  real(WP), dimension(:,:), allocatable :: spect
  integer(KIND=8) :: plan
  integer :: N_spect
  complex(WP), dimension(:,:,:), allocatable :: dft
  
end module getspect


! ============================================== !
! Initialization of spectrum calculation routine !
! ============================================== !
subroutine getspect_init
  use getspect
  use math
  use parser
  integer :: ik
  real(WP) :: dk
  
  ! Initialize complex Fourier transform - should be done elsewhere?
  call cfourier_init

  ! Get the increment. Assumes same size in all directions 
  dk=twoPi/xL

  ! Now the number of waves depends only on nx,ny,nz
  N_spect = max(nx,max(ny,nz))
  
  ! Allocate the array; 1-> Wavenumber in x, 2-> Energy, 3-> Dissipation
  allocate(spect(3,N_spect+1)); spect=0.0_WP
  do ik=1,N_spect+1
     spect(1,ik)=dk*real(ik-1,WP)
  end do
  
  ! Allocate work array
  allocate(dft(imin_:imax_,jmin_:jmax_,kmin_:kmax_))
  
  return
end subroutine getspect_init


! ================================ !
! Compute the spectrum of a scalar !
! ================================ !
subroutine getspect_scalar(A)
  use getspect
  use math
  implicit none
  
  real(WP),dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_),intent(in):: A
  real(WP), dimension(N_spect+1):: S,D
  real(WP) :: dk,kc,kx,ky,kz,kk
  real(WP) :: eps,Ek
  integer :: i,j,k,ik,i_k,j_k,k_k
  
  ! Initialize variables
  S = 0.0_WP
  D = 0.0_WP
  
  ! Perform FFT in each Fourier direction
  dft = dcmplx(A(imin_:imax_,jmin_:jmax_,kmin_:kmax_))
  call cfourier_transform(dft)
  dft = dft/(nx*ny*nz)
  
  ! Account for aliasing (cutoff)
  dk=twoPi/xL
  kc=real(N_spect,WP)*Pi/xL
  eps=kc/1000000.0_WP
  
  ! Form wavevector, bin it, compute Power Spectrum
  do k=kmin_,kmax_
     k_k=k-kmin+1
     do j=jmin_,jmax_
        j_k=j-jmin+1
        do i=imin_,imax_
           i_k=i-imin+1
           ! Wavenumbers
           kx=real(i_k-1,WP)*dk
           if (i_k.gt.(nx/2+1)) kx=-real(nx-i_k+1,WP)*dk
           ky=real(j_k-1,WP)*dk
           if (j_k.gt.(ny/2+1)) ky=-real(ny-j_k+1,WP)*dk
           kz=real(k_k-1,WP)*dk
           if (k_k.gt.(nz/2+1)) kz=-real(nz-k_k+1,WP)*dk
           kk=sqrt(kx**2+ky**2+kz**2)
           ! Spectrum
           ik=1+idint(kk/dk+0.5_WP)
           if ((kk.gt.eps).and.(kk.le.kc)) then
              Ek=0.5_WP*real(dft(i,j,k)*conjg(dft(i,j,k)))
              S(ik)=S(ik)+Ek
              D(ik)=D(ik)+2.0_WP*kk**2*Ek
           end if
        end do
     end do
  end do
  
  ! Sum and share with neighbors
  call parallel_sum(S,spect(2,:))
  call parallel_sum(D,spect(3,:))
  
  return
end subroutine getspect_scalar

! ================================ !
! Compute the spectrum of a vector !
! ================================ !
subroutine getspect_vector(A,B,C)
  use getspect
  use data
  implicit none
  
  real(WP),dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_),intent(in):: A
  real(WP),dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_),intent(in):: B
  real(WP),dimension(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_),intent(in):: C

  real(WP),dimension(N_spect+1):: S,D

  call getspect_scalar(A)
  S=spect(2,:)
  D=spect(3,:)
  call getspect_scalar(B)
  S=S+spect(2,:)
  D=D+spect(3,:)
  call getspect_scalar(C)
  S=S+spect(2,:)
  D=D+spect(3,:)

  spect(2,:)=S
  spect(3,:)=D

  return
end subroutine getspect_vector
