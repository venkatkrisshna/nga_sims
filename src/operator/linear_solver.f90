! ========================================================= !
! Real linear solvers: 3D drivers for the polydiag routines !
! ========================================================= !
module linear_solver
  ! Dummy module
end module linear_solver


! ================ !
! Real solver in x !
! ================ !
subroutine linear_solver_x(nd)
  use precision
  use memory
  use partition
  implicit none
  
  ! Number of diagonals
  integer, intent(in) :: nd
  
  ! Choose based on number of diagonals
  select case(nd)
  case(3)
     call tridiagonal(&
          Ax(jmin_,kmin_,imin_,-1),&
          Ax(jmin_,kmin_,imin_, 0),&
          Ax(jmin_,kmin_,imin_,+1),&
          Rx,nx_,ny_*nz_,'x',stackmem(1,1),stackmem(1,2))
  case(5)
     call pentadiagonal(&
          Ax(jmin_,kmin_,imin_,-2),&
          Ax(jmin_,kmin_,imin_,-1),&
          Ax(jmin_,kmin_,imin_, 0),&
          Ax(jmin_,kmin_,imin_,+1),&
          Ax(jmin_,kmin_,imin_,+2),&
          Rx,nx_,ny_*nz_,'x',stackmem(1,1),stackmem(1,3))
  case default
     call polydiagonal((nd-1)/2,Ax(jmin_,kmin_,imin_,-(nd-1)/2),Rx,nx_,ny_*nz_,'x',stackmem)
  end select
  
  return
end subroutine linear_solver_x


! ================ !
! Real solver in y !
! ================ !
subroutine linear_solver_y(nd)
  use precision
  use memory
  use partition
  implicit none
  
  ! Number of diagonals
  integer, intent(in) :: nd
  
  ! Choose based on number of diagonals
  select case(nd)
  case(3)
     call tridiagonal(&
          Ay(imin_,kmin_,jmin_,-1),&
          Ay(imin_,kmin_,jmin_, 0),&
          Ay(imin_,kmin_,jmin_,+1),&
          Ry,ny_,nx_*nz_,'y',stackmem(1,1),stackmem(1,2))
  case(5)
     call pentadiagonal(&
          Ay(imin_,kmin_,jmin_,-2),&
          Ay(imin_,kmin_,jmin_,-1),&
          Ay(imin_,kmin_,jmin_, 0),&
          Ay(imin_,kmin_,jmin_,+1),&
          Ay(imin_,kmin_,jmin_,+2),&
          Ry,ny_,nx_*nz_,'y',stackmem(1,1),stackmem(1,3))
  case default
     call polydiagonal((nd-1)/2,Ay(imin_,kmin_,jmin_,-(nd-1)/2),Ry,ny_,nx_*nz_,'y',stackmem)
  end select
  
  return
end subroutine linear_solver_y


! ================ !
! Real solver in z !
! ================ !
subroutine linear_solver_z(nd)
  use precision
  use memory
  use partition
  implicit none
  
  ! Number of diagonals
  integer, intent(in) :: nd
  
  ! Choose based on number of diagonals
  select case(nd)
  case(3)
     call tridiagonal(&
          Az(imin_,jmin_,kmin_,-1),&
          Az(imin_,jmin_,kmin_, 0),&
          Az(imin_,jmin_,kmin_,+1),&
          Rz,nz_,ny_*nx_,'z',stackmem(1,1),stackmem(1,2))
  case(5)
     call pentadiagonal(&
          Az(imin_,jmin_,kmin_,-2),&
          Az(imin_,jmin_,kmin_,-1),&
          Az(imin_,jmin_,kmin_, 0),&
          Az(imin_,jmin_,kmin_,+1),&
          Az(imin_,jmin_,kmin_,+2),&
          Rz,nz_,ny_*nx_,'z',stackmem(1,1),stackmem(1,3))
  case default
     call polydiagonal((nd-1)/2,Az(imin_,jmin_,kmin_,-(nd-1)/2),Rz,nz_,ny_*nx_,'z',stackmem)
  end select
  
  return
end subroutine linear_solver_z
