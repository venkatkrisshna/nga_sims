module cfourier
  use precision
  use config
  use partition
  use metric_generic
  implicit none
  include 'fftw3.f'

  ! FFT directions
  logical :: fft_x
  logical :: fft_y
  logical :: fft_z

  ! FFTW variables
  complex(WP), dimension(:), allocatable :: in_x,out_x
  complex(WP), dimension(:), allocatable :: in_y,out_y
  complex(WP), dimension(:), allocatable :: in_z,out_z
  complex(WP), dimension(:,:,:), allocatable :: ctranspose_fftx
  complex(WP), dimension(:,:,:), allocatable :: ctranspose_ffty
  complex(WP), dimension(:,:,:), allocatable :: ctranspose_fftz
  integer(KIND=8) :: fplan_x,bplan_x
  integer(KIND=8) :: fplan_y,bplan_y
  integer(KIND=8) :: fplan_z,bplan_z

  ! Oddball
  logical :: oddball

end module cfourier


! ============================== !
! Initialize the cfourier module !
! ============================== !
subroutine cfourier_init
  use cfourier
  use ctranspose
  use parallel
  implicit none

  ! Check directions
  fft_x = .false.
  fft_y = .false.
  fft_z = .false.
  if (xper.eq.1 .and. nx.ne.1) fft_x = .true.
  if (yper.eq.1 .and. ny.ne.1) fft_y = .true.
  if (zper.eq.1 .and. nz.ne.1) fft_z = .true.
  
  ! Test if not even number of points
  if (fft_x .and. mod(nx,2).ne.0) call die('cfourier_init: fft only with even number of points')
  if (fft_y .and. mod(ny,2).ne.0) call die('cfourier_init: fft only with even number of points')
  if (fft_z .and. mod(nz,2).ne.0) call die('cfourier_init: fft only with even number of points')

  ! Initialize Fourier directions
  if (fft_x) then
     ! Initialize ctranspose
     call ctranspose_init('x')
     
     ! Create plan - X
     allocate(in_x (nx))
     allocate(out_x(nx))
     call dfftw_plan_dft_1d(fplan_x,nx,in_x,out_x,FFTW_FORWARD,FFTW_MEASURE)
     call dfftw_plan_dft_1d(bplan_x,nx,in_x,out_x,FFTW_BACKWARD,FFTW_MEASURE)

     ! Allocate array for ctransposed data
     allocate(ctranspose_fftx(imin:imax,jmin_x(iproc):jmax_x(iproc),kmin_x(iproc):kmax_x(iproc)))
  end if

  if (fft_y) then
     ! Initialize ctranspose
     call ctranspose_init('y')
     
     ! Create plan - Y
     allocate(in_y (ny))
     allocate(out_y(ny))
     call dfftw_plan_dft_1d(fplan_y,ny,in_y,out_y,FFTW_FORWARD,FFTW_MEASURE)
     call dfftw_plan_dft_1d(bplan_y,ny,in_y,out_y,FFTW_BACKWARD,FFTW_MEASURE)

     ! Allocate array for ctransposed data
     allocate(ctranspose_ffty(imin_y(jproc):imax_y(jproc),jmin:jmax,kmin_y(jproc):kmax_y(jproc)))
  end if

  if (fft_z) then
     ! Initialize ctranspose
     call ctranspose_init('z')
     
     ! Create plan - Z
     allocate(in_z (nz))
     allocate(out_z(nz))
     call dfftw_plan_dft_1d(fplan_z,nz,in_z,out_z,FFTW_FORWARD,FFTW_MEASURE)
     call dfftw_plan_dft_1d(bplan_z,nz,in_z,out_z,FFTW_BACKWARD,FFTW_MEASURE)

     ! Allocate array for ctransposed data
     allocate(ctranspose_fftz(imin_z(kproc):imax_z(kproc),jmin_z(kproc):jmax_z(kproc),kmin:kmax))
  end if
  
  ! Oddball
  oddball = .false.
  if ( (fft_x .or. nx.eq.1) .and. &
       (fft_y .or. ny.eq.1) .and. &
       (fft_z .or. nz.eq.1) .and. &
       iproc.eq.1 .and. jproc.eq.1 .and. kproc.eq.1) oddball = .true.
  
  return
end subroutine cfourier_init


! =========================== !
! Transpose A and perform FFT !
! =========================== !
subroutine cfourier_transform(A)
  use cfourier
  use ctranspose
  use parallel
  implicit none

  complex(WP), dimension(imin_:imax_,jmin_:jmax_,kmin_:kmax_), intent(inout) :: A
  integer :: i,j,k
  
  ! Perform FFT in each Fourier direction
  if (fft_x) then
     ! Transpose in X
     call ctranspose_x(A,ctranspose_fftx)
     
     ! Forward transform - X
     do k=kmin_x(iproc),kmax_x(iproc)
        do j=jmin_x(iproc),jmax_x(iproc)
           in_x = ctranspose_fftx(:,j,k)
           call dfftw_execute(fplan_x)
           ctranspose_fftx(:,j,k) = out_x
        end do
     end do
     
     ! Transpose back
     call bctranspose_x(ctranspose_fftx,A)
  end if

  if (fft_y) then
     ! Transpose in y
     call ctranspose_y(A,ctranspose_ffty)
     
     ! Forward transform - Y
     do k=kmin_y(jproc),kmax_y(jproc)
        do i=imin_y(jproc),imax_y(jproc)
           in_y = ctranspose_ffty(i,:,k)
           call dfftw_execute(fplan_y)
           ctranspose_ffty(i,:,k) = out_y
        end do
     end do
     
     ! Transpose back
     call bctranspose_y(ctranspose_ffty,A)
  end if

  if (fft_z) then
     ! Transpose in z
     call ctranspose_z(A,ctranspose_fftz)
     
     ! Forward transform - Z
     do j=jmin_z(kproc),jmax_z(kproc)
        do i=imin_z(kproc),imax_z(kproc)
           in_z = ctranspose_fftz(i,j,:)
           call dfftw_execute(fplan_z)
           ctranspose_fftz(i,j,:) = out_z
        end do
     end do
     
     ! Transpose back
     call bctranspose_z(ctranspose_fftz,A)
  end if
  
  ! Oddball
  if (oddball) A(imin_,jmin_,kmin_) = 0.0_WP
  
  return
end subroutine cfourier_transform


! ================================== !
! FFT -> complex and ctranspose back !
! ================================== !
subroutine cfourier_inverse(A)
  use cfourier
  use ctranspose
  use parallel
  implicit none

  complex(WP), dimension(imin_:imax_,jmin_:jmax_,kmin_:kmax_), intent(inout) :: A
  integer :: i,j,k
  
  ! Transpose RHS and perform FFT
  if (fft_x) then
     ! Transpose in X
     call ctranspose_x(A,ctranspose_fftx)
     
     ! Inverse transform
     do k=kmin_x(iproc),kmax_x(iproc)
        do j=jmin_x(iproc),jmax_x(iproc)
           in_x = ctranspose_fftx(:,j,k)
           call dfftw_execute(bplan_x)
           ctranspose_fftx(:,j,k) = out_x/real(nx,WP)
        end do
     end do

     ! Transpose back
     call bctranspose_x(ctranspose_fftx,A)
  end if

  if (fft_y) then
     ! Transpose in Y
     call ctranspose_y(A,ctranspose_ffty)
     
     ! Inverse transform - Y
     do k=kmin_y(jproc),kmax_y(jproc)
        do i=imin_y(jproc),imax_y(jproc)
           in_y = ctranspose_ffty(i,:,k)
           call dfftw_execute(bplan_y)
           ctranspose_ffty(i,:,k) = out_y/real(ny,WP)
        end do
     end do

     ! Transpose back
     call bctranspose_y(ctranspose_ffty,A)
  end if

  if (fft_z) then
     ! Transpose in Z
     call ctranspose_z(A,ctranspose_fftz)
     
     ! Inverse transform - Z
     do j=jmin_z(kproc),jmax_z(kproc)
        do i=imin_z(kproc),imax_z(kproc)
           in_z = ctranspose_fftz(i,j,:)
           call dfftw_execute(bplan_z)
           ctranspose_fftz(i,j,:) = out_z/real(nz,WP)
        end do
     end do
     
     ! Transpose back
     call bctranspose_z(ctranspose_fftz,A)
  end if
  
  return
end subroutine cfourier_inverse
