module fourier
  use precision
  use config
  use partition
  use metric_generic
  use mesh_size
  implicit none
  include 'fftw3.f'

  ! FFT directions
  logical :: fft_x,fct_x
  logical :: fft_y,fct_y
  logical :: fft_z,fct_z

  ! FFTW variables
  real(WP), dimension(:), allocatable :: in_x,out_x
  real(WP), dimension(:), allocatable :: in_y,out_y
  real(WP), dimension(:), allocatable :: in_z,out_z
  real(WP), dimension(:,:,:), allocatable :: transpose_fftx
  real(WP), dimension(:,:,:), allocatable :: transpose_ffty
  real(WP), dimension(:,:,:), allocatable :: transpose_fftz
  integer(KIND=8) :: fplan_x,bplan_x
  integer(KIND=8) :: fplan_y,bplan_y
  integer(KIND=8) :: fplan_z,bplan_z

  ! Oddball
  logical :: oddball

end module fourier


! ============================= !
! Initialize the fourier module !
! ============================= !
subroutine fourier_init
  use fourier
  use transpose
  use parallel
  implicit none

  ! Check directions
  fft_x = .false.
  fft_y = .false.
  fft_z = .false.
  if (xper.eq.1 .and. nx.ne.1) fft_x = .true.
  if (yper.eq.1 .and. ny.ne.1) fft_y = .true.
  if (zper.eq.1 .and. nz.ne.1) fft_z = .true.
  fct_x = .false.
  fct_y = .false.
  fct_z = .false.
  ! Might still not be working - need to further debug?
  !if (xper.eq.0 .and. uniform_x) fct_x = .true.
  !if (yper.eq.0 .and. uniform_y) fct_y = .true.
  !if (zper.eq.0 .and. uniform_z) fct_z = .true.
  
  ! Test if not even number of points
  if (fft_x .and. mod(nx,2).ne.0) call die('fourier_init: fft only with even number of points')
  if (fft_y .and. mod(ny,2).ne.0) call die('fourier_init: fft only with even number of points')
  if (fft_z .and. mod(nz,2).ne.0) call die('fourier_init: fft only with even number of points')
    
  ! Initialize Fourier directions
  if (fft_x) then
     ! Initialize transpose
     call transpose_init('x')
     ! Create plan - X
     allocate(in_x (nx))
     allocate(out_x(nx))
     call dfftw_plan_r2r_1d(fplan_x,nx,in_x,out_x,FFTW_R2HC,FFTW_MEASURE)
     call dfftw_plan_r2r_1d(bplan_x,nx,in_x,out_x,FFTW_HC2R,FFTW_MEASURE)
     ! Allocate array for transposed data
     allocate(transpose_fftx(imin:imax,jmin_x(iproc):jmax_x(iproc),kmin_x(iproc):kmax_x(iproc)))
  else if (fct_x) then
     ! Initialize transpose
     call transpose_init('x')
     ! Create plan - X
     allocate(in_x (nx))
     allocate(out_x(nx))
     call dfftw_plan_r2r_1d(fplan_x,nx,in_x,out_x,FFTW_REDFT10,FFTW_MEASURE)
     call dfftw_plan_r2r_1d(bplan_x,nx,in_x,out_x,FFTW_REDFT01,FFTW_MEASURE)
     ! Allocate array for transposed data
     allocate(transpose_fftx(imin:imax,jmin_x(iproc):jmax_x(iproc),kmin_x(iproc):kmax_x(iproc)))
  end if

  if (fft_y) then
     ! Initialize transpose
     call transpose_init('y')
     ! Create plan - Y
     allocate(in_y (ny))
     allocate(out_y(ny))
     call dfftw_plan_r2r_1d(fplan_y,ny,in_y,out_y,FFTW_R2HC,FFTW_MEASURE)
     call dfftw_plan_r2r_1d(bplan_y,ny,in_y,out_y,FFTW_HC2R,FFTW_MEASURE)
     ! Allocate array for transposed data
     allocate(transpose_ffty(imin_y(jproc):imax_y(jproc),jmin:jmax,kmin_y(jproc):kmax_y(jproc)))
  else if (fct_y) then
     ! Initialize transpose
     call transpose_init('y')
     ! Create plan - Y
     allocate(in_y (ny))
     allocate(out_y(ny))
     call dfftw_plan_r2r_1d(fplan_y,ny,in_y,out_y,FFTW_REDFT10,FFTW_MEASURE)
     call dfftw_plan_r2r_1d(bplan_y,ny,in_y,out_y,FFTW_REDFT01,FFTW_MEASURE)
     ! Allocate array for transposed data
     allocate(transpose_ffty(imin_y(jproc):imax_y(jproc),jmin:jmax,kmin_y(jproc):kmax_y(jproc)))
  end if
  
  if (fft_z) then
     ! Initialize transpose
     call transpose_init('z')
     ! Create plan - Z
     allocate(in_z (nz))
     allocate(out_z(nz))
     call dfftw_plan_r2r_1d(fplan_z,nz,in_z,out_z,FFTW_R2HC,FFTW_MEASURE)
     call dfftw_plan_r2r_1d(bplan_z,nz,in_z,out_z,FFTW_HC2R,FFTW_MEASURE)
     ! Allocate array for transposed data
     allocate(transpose_fftz(imin_z(kproc):imax_z(kproc),jmin_z(kproc):jmax_z(kproc),kmin:kmax))
  else if (fct_z) then
     ! Initialize transpose
     call transpose_init('z')
     ! Create plan - Z
     allocate(in_z (nz))
     allocate(out_z(nz))
     call dfftw_plan_r2r_1d(fplan_z,nz,in_z,out_z,FFTW_REDFT10,FFTW_MEASURE)
     call dfftw_plan_r2r_1d(bplan_z,nz,in_z,out_z,FFTW_REDFT01,FFTW_MEASURE)
     ! Allocate array for transposed data
     allocate(transpose_fftz(imin_z(kproc):imax_z(kproc),jmin_z(kproc):jmax_z(kproc),kmin:kmax))
  end if
  
  ! Oddball
  oddball = .false.
  if ( (fft_x .or. fct_x .or. nx.eq.1) .and. &
       (fft_y .or. fct_y .or. ny.eq.1) .and. &
       (fft_z .or. fct_z .or. nz.eq.1) .and. &
       iproc.eq.1 .and. jproc.eq.1 .and. kproc.eq.1) oddball = .true.
  
  return
end subroutine fourier_init


! =========================== !
! Transpose A and perform FFT !
! =========================== !
subroutine fourier_transform(A)
  use fourier
  use transpose
  use parallel
  implicit none

  real(WP), dimension(imin_:imax_,jmin_:jmax_,kmin_:kmax_), intent(inout) :: A
  integer :: i,j,k
  
  ! Perform transform in each Fourier direction
  if (fft_x.or.fct_x) then
     ! Transpose in X
     call transpose_x(A,transpose_fftx)
     
     ! Forward transform - X
     do k=kmin_x(iproc),kmax_x(iproc)
        do j=jmin_x(iproc),jmax_x(iproc)
           in_x = transpose_fftx(:,j,k)
           call dfftw_execute(fplan_x)
           transpose_fftx(:,j,k) = out_x
        end do
     end do
     
     ! Transpose back
     call btranspose_x(transpose_fftx,A)
  end if

  if (fft_y.or.fct_y) then
     ! Transpose in y
     call transpose_y(A,transpose_ffty)
     
     ! Forward transform - Y
     do k=kmin_y(jproc),kmax_y(jproc)
        do i=imin_y(jproc),imax_y(jproc)
           in_y = transpose_ffty(i,:,k)
           call dfftw_execute(fplan_y)
           transpose_ffty(i,:,k) = out_y
        end do
     end do
     
     ! Transpose back
     call btranspose_y(transpose_ffty,A)
  end if

  if (fft_z.or.fct_z) then
     ! Transpose in z
     call transpose_z(A,transpose_fftz)
     
     ! Forward transform - Z
     do j=jmin_z(kproc),jmax_z(kproc)
        do i=imin_z(kproc),imax_z(kproc)
           in_z = transpose_fftz(i,j,:)
           call dfftw_execute(fplan_z)
           transpose_fftz(i,j,:) = out_z
        end do
     end do
     
     ! Transpose back
     call btranspose_z(transpose_fftz,A)
  end if
  
  ! Oddball
  if (oddball) A(imin_,jmin_,kmin_) = 0.0_WP
  
  return
end subroutine fourier_transform


! ============================== !
! FFT -> real and transpose back !
! ============================== !
subroutine fourier_inverse(A)
  use fourier
  use transpose
  use parallel
  implicit none

  real(WP), dimension(imin_:imax_,jmin_:jmax_,kmin_:kmax_), intent(inout) :: A
  integer :: i,j,k
  
  ! Transpose RHS and perform transform
  if (fft_x) then
     ! Transpose in X
     call transpose_x(A,transpose_fftx)
     ! Inverse transform
     do k=kmin_x(iproc),kmax_x(iproc)
        do j=jmin_x(iproc),jmax_x(iproc)
           in_x = transpose_fftx(:,j,k)
           call dfftw_execute(bplan_x)
           transpose_fftx(:,j,k) = out_x/real(nx,WP)
        end do
     end do
     ! Transpose back
     call btranspose_x(transpose_fftx,A)
  else if (fct_x) then
     ! Transpose in X
     call transpose_x(A,transpose_fftx)
     ! Inverse transform
     do k=kmin_x(iproc),kmax_x(iproc)
        do j=jmin_x(iproc),jmax_x(iproc)
           in_x = transpose_fftx(:,j,k)
           call dfftw_execute(bplan_x)
           transpose_fftx(:,j,k) = out_x/real(2.0_WP*nx,WP)
        end do
     end do
     ! Transpose back
     call btranspose_x(transpose_fftx,A)
  end if

  if (fft_y) then
     ! Transpose in Y
     call transpose_y(A,transpose_ffty)
     ! Inverse transform - Y
     do k=kmin_y(jproc),kmax_y(jproc)
        do i=imin_y(jproc),imax_y(jproc)
           in_y = transpose_ffty(i,:,k)
           call dfftw_execute(bplan_y)
           transpose_ffty(i,:,k) = out_y/real(ny,WP)
        end do
     end do
     ! Transpose back
     call btranspose_y(transpose_ffty,A)
  else if (fct_y) then
     ! Transpose in Y
     call transpose_y(A,transpose_ffty)
     ! Inverse transform - Y
     do k=kmin_y(jproc),kmax_y(jproc)
        do i=imin_y(jproc),imax_y(jproc)
           in_y = transpose_ffty(i,:,k)
           call dfftw_execute(bplan_y)
           transpose_ffty(i,:,k) = out_y/real(2.0_WP*ny,WP)
        end do
     end do
     ! Transpose back
     call btranspose_y(transpose_ffty,A)
  end if

  if (fft_z) then
     ! Transpose in Z
     call transpose_z(A,transpose_fftz)
     ! Inverse transform - Z
     do j=jmin_z(kproc),jmax_z(kproc)
        do i=imin_z(kproc),imax_z(kproc)
           in_z = transpose_fftz(i,j,:)
           call dfftw_execute(bplan_z)
           transpose_fftz(i,j,:) = out_z/real(nz,WP)
        end do
     end do
     ! Transpose back
     call btranspose_z(transpose_fftz,A)
  else if (fft_z) then
     ! Transpose in Z
     call transpose_z(A,transpose_fftz)
     ! Inverse transform - Z
     do j=jmin_z(kproc),jmax_z(kproc)
        do i=imin_z(kproc),imax_z(kproc)
           in_z = transpose_fftz(i,j,:)
           call dfftw_execute(bplan_z)
           transpose_fftz(i,j,:) = out_z/real(2.0_WP*nz,WP)
        end do
     end do
     ! Transpose back
     call btranspose_z(transpose_fftz,A)
  end if
  
  return
end subroutine fourier_inverse
