! ============================================================ !
! Complex linear solvers: 3D drivers for the polydiag routines !
! ============================================================ !
module clinear_solver
  ! Dummy module
end module clinear_solver


! =================== !
! Complex solver in x !
! =================== !
subroutine clinear_solver_x(nd)
  use precision
  use memory
  use partition
  implicit none
  
  ! Number of diagonals
  integer, intent(in) :: nd
  
  ! Choose based on number of diagonals
  select case(nd)
  case(3)
     call ctridiagonal(&
          cAx(jmin_,kmin_,imin_,-1),&
          cAx(jmin_,kmin_,imin_, 0),&
          cAx(jmin_,kmin_,imin_,+1),&
          cRx,nx_,ny_*nz_,'x',cstackmem(1,1),cstackmem(1,2))
  case(5)
     call cpentadiagonal(&
          cAx(jmin_,kmin_,imin_,-2),&
          cAx(jmin_,kmin_,imin_,-1),&
          cAx(jmin_,kmin_,imin_, 0),&
          cAx(jmin_,kmin_,imin_,+1),&
          cAx(jmin_,kmin_,imin_,+2),&
          cRx,nx_,ny_*nz_,'x',cstackmem(1,1),cstackmem(1,3))
  case default
     call cpolydiagonal((nd-1)/2,cAx(jmin_,kmin_,imin_,-(nd-1)/2) &
                      ,cRx,nx_,ny_*nz_,'x',cstackmem)
  end select
  
  return
end subroutine clinear_solver_x


! =================== !
! Complex solver in y !
! =================== !
subroutine clinear_solver_y(nd)
  use precision
  use memory
  use partition
  implicit none
  
  ! Number of diagonals
  integer, intent(in) :: nd
  
  ! Choose based on number of diagonals
  select case(nd)
  case(3)
     call ctridiagonal(&
          cAy(imin_,kmin_,jmin_,-1),&
          cAy(imin_,kmin_,jmin_, 0),&
          cAy(imin_,kmin_,jmin_,+1),&
          cRy,ny_,nx_*nz_,'y',cstackmem(1,1),cstackmem(1,2))
  case(5)
     call cpentadiagonal(&
          cAy(imin_,kmin_,jmin_,-2),&
          cAy(imin_,kmin_,jmin_,-1),&
          cAy(imin_,kmin_,jmin_, 0),&
          cAy(imin_,kmin_,jmin_,+1),&
          cAy(imin_,kmin_,jmin_,+2),&
          cRy,ny_,nx_*nz_,'y',cstackmem(1,1),cstackmem(1,3))
  case default
     call cpolydiagonal((nd-1)/2,cAy(imin_,kmin_,jmin_,-(nd-1)/2), &
                              cRy,ny_,nx_*nz_,'y',cstackmem)
  end select
  
  return
end subroutine clinear_solver_y


! =================== !
! Complex solver in z !
! =================== !
subroutine clinear_solver_z(nd)
  use precision
  use memory
  use partition
  implicit none
  
  ! Number of diagonals
  integer, intent(in) :: nd
  
  ! Choose based on number of diagonals
  select case(nd)
  case(3)
     call ctridiagonal(&
          cAz(imin_,jmin_,kmin_,-1),&
          cAz(imin_,jmin_,kmin_, 0),&
          cAz(imin_,jmin_,kmin_,+1),&
          cRz,nz_,ny_*nx_,'z',cstackmem(1,1),cstackmem(1,2))
  case(5)
     call cpentadiagonal(&
          cAz(imin_,jmin_,kmin_,-2),&
          cAz(imin_,jmin_,kmin_,-1),&
          cAz(imin_,jmin_,kmin_, 0),&
          cAz(imin_,jmin_,kmin_,+1),&
          cAz(imin_,jmin_,kmin_,+2),&
          cRz,nz_,ny_*nx_,'z',cstackmem(1,1),cstackmem(1,3))
  case default
     call cpolydiagonal((nd-1)/2,cAz(imin_,jmin_,kmin_,-(nd-1)/2), &
                          cRz,nz_,ny_*nx_,'z',cstackmem)
  end select
  
  return
end subroutine clinear_solver_z
