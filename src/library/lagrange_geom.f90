module lagrange_geom
  use math

  real(WP), parameter :: r16 = 1.0_WP/6.0_WP
  real(WP), parameter :: r13 = 1.0_WP/3.0_WP
  integer, parameter :: max_ltris = 12

  !==== A Hexahedron *** lookup table based on this vertex ordering ***
  !          
  !       vertices
  !       
  !      7---------8          
  !     /|        /|      
  !    / |       / |      
  !   /  |      /  |      
  !  5---3-----6---4      
  !  |  /      |  /       
  !  | /       | /        
  !  |/        |/        
  !  1---------2          
  !
  !=====

  !===== lookup table for hex vertices to tet vertices
  ! -  note that all vertices are in low-to-high order, needed to enure
  ! -  triangles extruded from IB surface match with those of Cartesian edges
  integer, parameter, dimension(4,6) :: hex_to_tet = reshape(  &
       (/ &
       1, 2, 4, 8, &        ! tet : 1 - right-handed
       1, 2, 6, 8, &        ! tet : 2 - left -handed
       1, 3, 7, 8, &        ! tet : 3 - right
       1, 3, 4, 8, &        ! tet : 4 - left
       1, 5, 6, 8, &        ! tet : 5 - right
       1, 5, 7, 8  &        ! tet : 6 - left
       /), (/4,6/) )

  !==== Hex face *** lookup table based on this vertex ordering ***
  !          
  !       vertices
  !       
  !      3---------4          
  !      |         |      
  !      |         |      
  !      |         |      
  !      1---------2      
  !
  !=====

  !===== lookup table for rec vertices to tri vertices
  !  - order vertices from low-to-hi for consistency
  integer, parameter, dimension(3,2) :: rec_to_tri = reshape(  &
       (/ &
       1, 2, 4, &        ! tri : 1
       1, 3, 4  &        ! tri : 2
       /), (/3,2/) )

  ! ==== A triangle (from cutting a rectangle or hex face) :
  !    
  !    - vertex labels (vx), edge labels (ex) and contribution to case number [1,2,4]
  !
  !              v1  -  +1
  !             /  \
  !            /    \
  !           e2     e1
  !          /        \
  !         /          \
  !  +4 - v3-----e3-----v2  - +2
  !
  ! =====

  ! ==== lookup table for case to number intersected edges 
  integer, parameter, dimension(0:7) :: tri_nedge = (/ &
       0, 2, 2, 2, 2, 2, 2, 0/)
  !    0, 1, 2, 3, 4, 5, 6, 7

  ! ==== lookup table for edge to vertices
  integer, parameter, dimension(2,3) :: tri_edge_to_vertex = reshape( &
       (/ &
       1, 2, &               ! edge : 1
       1, 3, &               ! edge : 2
       2, 3  &               ! edge : 3
       /), (/2,3/) )    

  !==== lookup table for case to intersected edges :
  !     0 is a sentinel value indicating no more edges
  !     ordering of the edges important for area calculations
  integer, parameter, dimension(2,0:7) :: tri_case_to_edge = reshape( &
       (/ &
       0, 0, &      ! case : 0
       1, 2, &      ! case : 1
       1, 3, &      ! case : 2
       2, 3, &      ! case : 3
       2, 3, &      ! case : 4
       1, 3, &      ! case : 5
       1, 2, &      ! case : 6
       0, 0  &      ! case : 7
       /) , (/2,8/) )

  !==== lookup table (tri_verts,subtri,case) vertices with G > 0 :
  !     0 is a sentinel value
  !     ordering is important for decomposing into tris
  integer, parameter, dimension(3,2,0:7) :: tri_verts_to_subtri = reshape( &
       (/ &
       0, 0, 0, &      ! case : 0 - subtri : 1
       0, 0, 0, &      ! case : 0 - subtri : 2
       1, 0, 0, &      ! case : 1 - subtri : 1
       0, 0, 0, &      ! case : 1 - subtri : 2
       2, 0, 0, &      ! case : 2 - subtri : 1
       0, 0, 0, &      ! case : 2 - subtri : 2
       1, 2, 0, &      ! case : 3 - subtri : 1
       1, 0, 0, &      ! case : 3 - subtri : 2
       3, 0, 0, &      ! case : 4 - subtri : 1
       0, 0, 0, &      ! case : 4 - subtri : 2
       1, 3, 0, &      ! case : 5 - subtri : 1
       3, 0, 0, &      ! case : 5 - subtri : 2
       2, 3, 0, &      ! case : 6 - subtri : 1
       3, 0, 0, &      ! case : 6 - subtri : 2
       1, 2, 3, &      ! case : 7 - subtri : 1
       0, 0, 0  &      ! case : 7 - subtri : 2
       /), (/3,2,8/) )

  !==== number of vertices with G > 0 (non-zero entries above) per case
  integer, parameter, dimension(2,0:7) :: tri_nverts = reshape( (/ &
       0, 0, &      ! case : 0 - subtris 1, 2
       1, 0, &      ! case : 1
       1, 0, &      ! case : 2
       2, 1, &      ! case : 3
       1, 0, &      ! case : 4
       2, 1, &      ! case : 5
       2, 1, &      ! case : 6
       3, 0  &      ! case : 7
       /), (/2,8/) )
  ! ==== number of sub triangles
  integer, parameter, dimension(0:7) :: nsubtris = &
       (/ 0, 1, 1, 2, 1, 2, 2 ,1 /)
 
  ! ==== lookup table (intp,itri,case) for intersection points to subtri
  integer, parameter, dimension(2,2,0:7) :: tri_intp_to_subtri = reshape( (/ &
       0, 0, &   ! case 0 - subtri : 1
       0, 0, &   ! case 0 - subtri : 2
       1, 2, &   ! case 1 - subtri : 1
       0, 0, &   ! case 1 - subtri : 2
       1, 2, &   ! case 2 - subtri : 1
       0, 0, &   ! case 2 - subtri : 2
       2, 0, &   ! case 3 - subtri : 1
       1, 2, &   ! case 3 - subtri : 2
       1, 2, &   ! case 4 - subtri : 1
       0, 0, &   ! case 4 - subtri : 2
       1, 0, &   ! case 5 - subtri : 1
       1, 2, &   ! case 5 - subtri : 2
       1, 0, &   ! case 6 - subtri : 1
       1, 2, &   ! case 6 - subtri : 2
       0, 0, &   ! case 7 - subtri : 1
       0, 0  &   ! case 7 - subtri : 2
       /), (/2,2,8/))

  !==== number of intp (non-zero entries above) per subtri - case
  integer, parameter, dimension(2,0:7) :: tri_nintp = reshape( (/ &
       0, 0, &      ! case : 0 - subtris 1, 2
       2, 0, &      ! case : 1
       2, 0, &      ! case : 2
       1, 2, &      ! case : 3
       2, 0, &      ! case : 4
       1, 2, &      ! case : 5
       1, 2, &      ! case : 6
       0, 0  &      ! case : 7
       /), (/2,8/) )

  ! ==== lookup table for where (in subtri) to put tri%vertex points
  !      such that a triangle order of source-balanced-sink is kept
  integer, parameter, dimension(2,2,0:7) :: subtri_intp_order = reshape( (/ & 
       0, 0, &   ! case 0 - subtri : 1
       0, 0, &   ! case 0 - subtri : 2
       2, 3, &   ! case 1 - subtri : 1  (balanced-sink)
       0, 0, &   ! case 1 - subtri : 2
       1, 3, &   ! case 2 - subtri : 1  (source-sink)
       0, 0, &   ! case 2 - subtri : 2
       3, 0, &   ! case 3 - subtri : 1  (sink)
       2, 3, &   ! case 3 - subtri : 2  (balanced-sink)
       1, 2, &   ! case 4 - subtri : 1  (source-balanced)
       0, 0, &   ! case 4 - subtri : 2
       2, 0, &   ! case 5 - subtri : 1  (balanced)
       1, 2, &   ! case 5 - subtri : 2  (source-balanced)
       1, 0, &   ! case 6 - subtri : 1  (source)
       1, 2, &   ! case 6 - subtri : 2  (source-balanced)
       0, 0, &   ! case 7 - subtri : 1
       0, 0  &   ! case 7 - subtri : 2
       /), (/2,2,8/))

  integer, parameter, dimension(0:7) :: subtri_hand_flip = (/ &
       0, 0, 0,-1, 0, 0,-1, 0 /)
  !    0, 1, 2, 3, 4, 5, 6, 7
  

  type tri
     real(WP), dimension(3,3) :: vertex
     real(WP), dimension(3) :: G
     ! intersection points (1 tuple per edge)
     real(WP), dimension(3,2) :: intp
     ! intersected area with G > 0
     real(WP) :: cut_area
     real(WP) :: iso_length
     ! fluid area weighted centroid
     real(WP), dimension(3) :: cut_wcent
     integer :: case
  end type tri

  type simple_tri
     real(WP), dimension(3,3) :: vertex
     real(WP) :: area
     real(WP), dimension(3) :: cent,normal
  end type simple_tri

  type lagrange_tri
     real(WP), dimension(3,3) :: vertex
     integer , dimension(  3) :: is_ib
     logical , dimension(  3) :: masked
     integer :: hand  ! 1 for right-handed, 2 for left-handed
  end type lagrange_tri
 

  !==== A Tetrahedron: 
  !
  !      -vertex labels (vx), edge labels (ex) and contribution to case number [1,2,4,8]
  !
  !              v1  - +1
  !             /|\
  !            / | \
  !           e3 |  e1
  !          /   e2  \ 
  !         /    |    \
  !  +8 - v4--e5-|-----v2  - +2
  !         \    |    /
  !          \   |   /
  !           e6 |  e4
  !            \ | /
  !             \|/
  !              v3 - + 4
  !======  


  ! ==== lookup table for edge to vertices
  integer, parameter, dimension(2,6) :: edge_to_vertex = reshape( &
       (/ &
       1, 2, &               ! edge : 1
       1, 3, &               ! edge : 2
       1, 4, &               ! edge : 3
       2, 3, &               ! edge : 4
       2, 4, &               ! edge : 5
       3, 4  &               ! edge : 6
       /), (/2,6/) )    


  !==== lookup table for case to intersected edges :
  !     0 is a sentinel value indicating no more edges
  !     ordering of the edges for each case are important for area/volume calcs
  integer, parameter, dimension(4,0:15) :: case_to_edge = reshape( &
       (/ &
       0, 0, 0, 0, &      ! case : 0
       1, 2, 3, 0, &      ! case : 1
       1, 4, 5, 0, &      ! case : 2
       2, 3, 4, 5, &      ! case : 3
       2, 4, 6, 0, &      ! case : 4
       1, 3, 4, 6, &      ! case : 5
       1, 2, 5, 6, &      ! case : 6
       3, 5, 6, 0, &      ! case : 7
       3, 5, 6, 0, &      ! case : 8
       1, 2, 5, 6, &      ! case : 9
       1, 3, 4, 6, &      ! case : 10
       2, 4, 6, 0, &      ! case : 11
       2, 3, 4, 5, &      ! case : 12
       1, 4, 5, 0, &      ! case : 13
       1, 2, 3, 0, &      ! case : 14
       0, 0, 0, 0  &      ! case : 15
       /) , (/4,16/) )

  !==== lookup table for triangle handedness flip based on tet hand
  integer, parameter, dimension(2,0:15) :: tet_hand_tri_hand = reshape (&
       (/ &            !    tet hand
       0, 0,  & ! case 0  : RH,LH  
       2, 1,  & ! case 1  : RH,LH
       1, 2,  & ! case 2  : RH,LH
       2, 1,  & ! case 3  : RH,LH
       2, 1,  & ! case 4  : RH,LH
       1, 2,  & ! case 5  : RH,LH
       1, 2,  & ! case 6  : RH,LH
       2, 1,  & ! case 7  : RH,LH
       1, 2,  & ! case 8  : RH,LH
       2, 1,  & ! case 9  : RH,LH
       2, 1,  & ! case 10 : RH,LH
       1, 2,  & ! case 11 : RH,LH
       1, 2,  & ! case 12 : RH,LH
       2, 1,  & ! case 13 : RH,LH
       1, 2,  & ! case 14 : RH,LH
       0, 0   & ! case 15 : RH,LH
       /), (/2,16/) )

  !==== signed volume corrections (itet,tri%hand)
  real(WP), parameter, dimension(3,2) :: hand_to_vol = reshape (&
       (/ &
       -1.0_WP,+1.0_WP,-1.0_WP, & ! right-handed-tri, tets 1-3
       +1.0_WP,-1.0_WP,+1.0_WP  & ! left -handed-tri, tets 1-3
       /), (/3,2/))

  !==== number of sub-triangles which make up iso-surface per case
  integer, parameter, dimension(0:15) :: iso_nsubtris = (/ &
       0, 1, 1, 2, 1, 2, 2, 1, 1, 2, 2, 1, 2, 1, 1, 0 /) 
  !    0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15

  !==== number of sub-tets to decompose tet
  integer, parameter, dimension(0:15) :: nsubtets = (/ &
       0, 1, 1, 3, 1, 3, 3, 3, 1, 3, 3, 3, 3, 3, 3, 0 /)
  !    0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15

  !==== lookup table to select appropriate intp for source-balance-sink order
  !     (intp coordinate, sub-triangle, tet-case)
  integer, parameter, dimension(3,2,0:15) :: tet_intp_to_subtri = reshape ( &
       (/ &
       0, 0, 0, &       ! case : 0  - subtri : 1
       0, 0, 0, &       ! case : 0  - subtri : 2
       1, 2, 3, &       ! case : 1  - subtri : 1
       0, 0, 0, &       ! case : 1  - subtri : 2
       1, 2, 3, &       ! case : 2  - subtri : 1
       0, 0, 0, &       ! case : 2  - subtri : 2
       1, 2, 4, &       ! case : 3  - subtri : 1
       1, 3, 4, &       ! case : 3  - subtri : 2
       1, 2, 3, &       ! case : 4  - subtri : 1
       0, 0, 0, &       ! case : 4  - subtri : 2
       1, 2, 4, &       ! case : 5  - subtri : 1
       1, 3, 4, &       ! case : 5  - subtri : 2
       1, 2, 4, &       ! case : 6  - subtri : 1
       1, 3, 4, &       ! case : 6  - subtri : 2
       1, 2, 3, &       ! case : 7  - subtri : 1
       0, 0, 0, &       ! case : 7  - subtri : 2
       1, 2, 3, &       ! case : 8  - subtri : 1
       0, 0, 0, &       ! case : 8  - subtri : 2
       1, 2, 4, &       ! case : 9  - subtri : 1
       1, 3, 4, &       ! case : 9  - subtri : 2
       1, 2, 4, &       ! case : 10 - subtri : 1
       1, 3, 4, &       ! case : 10 - subtri : 2
       1, 2, 3, &       ! case : 11 - subtri : 1
       0, 0, 0, &       ! case : 11 - subtri : 2
       1, 2, 4, &       ! case : 12 - subtri : 1
       1, 3, 4, &       ! case : 12 - subtri : 2
       1, 2, 3, &       ! case : 13 - subtri : 1
       0, 0, 0, &       ! case : 13 - subtri : 2
       1, 2, 3, &       ! case : 14 - subtri : 1
       0, 0, 0, &       ! case : 14 - subtri : 2
       0, 0, 0, &       ! case : 15 - subtri : 1
       0, 0, 0  &       ! case : 15 - subtri : 2
       /), (/3,2,16/))

  !==== lookup table for the vertices of tet to subtet
  !       (vert coordinate,subtet,case)
  integer, parameter, dimension(3,3,0:15) :: tet_verts_to_subtet = reshape ( &
       (/ &
       0, 0, 0, &  ! case : 0  - subtet 1
       0, 0, 0, &  ! case : 0  - subtet 2
       0, 0, 0, &  ! case : 0  - subtet 3
       1, 0, 0, &  ! case : 1  - subtet 1
       0, 0, 0, &  ! case : 1  - subtet 2
       0, 0, 0, &  ! case : 1  - subtet 3
       2, 0, 0, &  ! case : 2  - subtet 1
       0, 0, 0, &  ! case : 2  - subtet 2
       0, 0, 0, &  ! case : 2  - subtet 3
       1, 2, 0, &  ! case : 3  - subtet 1
       1, 0, 0, &  ! case : 3  - subtet 2
       1, 0, 0, &  ! case : 3  - subtet 3
       3, 0, 0, &  ! case : 4  - subtet 1
       0, 0, 0, &  ! case : 4  - subtet 2
       0, 0, 0, &  ! case : 4  - subtet 3
       1, 0, 0, &  ! case : 5  - subtet 1
       1, 3, 0, &  ! case : 5  - subtet 2
       3, 0, 0, &  ! case : 5  - subtet 3
       3, 0, 0, &  ! case : 6  - subtet 1
       2, 0, 0, &  ! case : 6  - subtet 2
       2, 3, 0, &  ! case : 6  - subtet 3
       1, 2, 3, &  ! case : 7  - subtet 1
       1, 2, 0, &  ! case : 7  - subtet 2
       1, 0, 0, &  ! case : 7  - subtet 3
       4, 0, 0, &  ! case : 8  - subtet 1
       0, 0, 0, &  ! case : 8  - subtet 2
       0, 0, 0, &  ! case : 8  - subtet 3
       1, 4, 0, &  ! case : 9  - subtet 1
       4, 0, 0, &  ! case : 9  - subtet 2
       4, 0, 0, &  ! case : 9  - subtet 3
       2, 4, 0, &  ! case : 10 - subtet 1
       4, 0, 0, &  ! case : 10 - subtet 2
       4, 0, 0, &  ! case : 10 - subtet 3
       4, 0, 0, &  ! case : 11 - subtet 1
       1, 2, 4, &  ! case : 11 - subtet 2
       1, 4, 0, &  ! case : 11 - subtet 3
       4, 0, 0, &  ! case : 12 - subtet 1
       4, 0, 0, &  ! case : 12 - subtet 2
       3, 4, 0, &  ! case : 12 - subtet 3
       1, 3, 4, &  ! case : 13 - subtet 1
       3, 4, 0, &  ! case : 13 - subtet 2
       4, 0, 0, &  ! case : 13 - subtet 3
       2, 3, 4, &  ! case : 14 - subtet 1
       3, 4, 0, &  ! case : 14 - subtet 2
       4, 0, 0, &  ! case : 14 - subtet 3
       0, 0, 0, &  ! case : 15 - subtet 1
       0, 0, 0, &  ! case : 15 - subtet 2
       0, 0, 0  &  ! case : 15 - subtet 3
       /), (/3,3,16/))

  !==== lookup table for tet intersection points to subtets
  !      (intp index, subtet, case)
  integer, parameter, dimension(3,3,0:15) :: tet_intp_to_subtet = reshape ( &
       (/ &
       0, 0, 0, &  ! case 0  : subtet 1
       0, 0, 0, &  ! case 0  : subtet 2
       0, 0, 0, &  ! case 0  : subtet 3
       1, 2, 3, &  ! case 1  : subtet 1
       0, 0, 0, &  ! case 1  : subtet 2
       0, 0, 0, &  ! case 1  : subtet 3
       1, 2, 3, &  ! case 2  : subtet 1
       0, 0, 0, &  ! case 2  : subtet 2
       0, 0, 0, &  ! case 2  : subtet 3
       3, 4, 0, &  ! case 3  : subtet 1
       1, 3, 4, &  ! case 3  : subtet 2
       1, 2, 4, &  ! case 3  : subtet 3
       1, 2, 3, &  ! case 4  : subtet 1
       0, 0, 0, &  ! case 4  : subtet 2
       0, 0, 0, &  ! case 4  : subtet 3
       1, 2, 4, &  ! case 5  : subtet 1
       1, 4, 0, &  ! case 5  : subtet 2
       1, 3, 4, &  ! case 5  : subtet 3
       1, 2, 4, &  ! case 6  : subtet 1
       1, 3, 4, &  ! case 6  : subtet 2
       1, 4, 0, &  ! case 6  : subtet 3
       3, 0, 0, &  ! case 7  : subtet 1
       2, 3, 0, &  ! case 7  : subtet 2
       1, 2, 3, &  ! case 7  : subtet 3
       1, 2, 3, &  ! case 8  : subtet 1
       0, 0, 0, &  ! case 8  : subtet 2
       0, 0, 0, &  ! case 8  : subtet 3
       1, 2, 0, &  ! case 9  : subtet 1
       1, 2, 4, &  ! case 9  : subtet 2
       1, 3, 4, &  ! case 9  : subtet 3
       1, 3, 0, &  ! case 10 : subtet 1
       1, 3, 4, &  ! case 10 : subtet 2
       1, 2, 4, &  ! case 10 : subtet 3
       1, 2, 3, &  ! case 11 : subtet 1
       2, 0, 0, &  ! case 11 : subtet 2
       1, 2, 0, &  ! case 11 : subtet 3
       1, 2, 4, &  ! case 12 : subtet 1
       1, 3, 4, &  ! case 12 : subtet 2
       1, 3, 0, &  ! case 12 : subtet 3
       1, 0, 0, &  ! case 13 : subtet 1
       1, 2, 0, &  ! case 13 : subtet 2
       1, 2, 3, &  ! case 13 : subtet 3
       1, 0, 0, &  ! case 14 : subtet 1
       1, 2, 0, &  ! case 14 : subtet 2
       1, 2, 3, &  ! case 14 : subtet 3
       0, 0, 0, &  ! case 15 : subtet 1
       0, 0, 0, &  ! case 15 : subtet 2
       0, 0, 0  &  ! case 15 : subtet 3
       /), (/3,3,16/))

  !==== lookup table for order of tet intersection points in subtets
  !      (intp index, subtet, case)
  integer, parameter, dimension(3,3,0:15) :: subtet_intp_order = reshape ( &
       (/ &
       0, 0, 0, &  ! case 0  : subtet 1
       0, 0, 0, &  ! case 0  : subtet 2
       0, 0, 0, &  ! case 0  : subtet 3
       2, 3, 4, &  ! case 1  : subtet 1  (bm-bp-sink)
       0, 0, 0, &  ! case 1  : subtet 2
       0, 0, 0, &  ! case 1  : subtet 3
       1, 3, 4, &  ! case 2  : subtet 1  (source-bp-sink)
       0, 0, 0, &  ! case 2  : subtet 2
       0, 0, 0, &  ! case 2  : subtet 3
       3, 4, 0, &  ! case 3  : subtet 1  (bp-sink)
       2, 3, 4, &  ! case 3  : subtet 2  (bm-bp-sink)
       2, 3, 4, &  ! case 3  : subtet 3  (bm-bp-sink)
       1, 2, 4, &  ! case 4  : subtet 1
       0, 0, 0, &  ! case 4  : subtet 2
       0, 0, 0, &  ! case 4  : subtet 3
       2, 3, 4, &  ! case 5  : subtet 1
       2, 4, 0, &  ! case 5  : subtet 2
       1, 2, 4, &  ! case 5  : subtet 3
       1, 2, 4, &  ! case 6  : subtet 1
       1, 3, 4, &  ! case 6  : subtet 2
       1, 4, 0, &  ! case 6  : subtet 3
       4, 0, 0, &  ! case 7  : subtet 1
       3, 4, 0, &  ! case 7  : subtet 2
       2, 3, 4, &  ! case 7  : subtet 3
       1, 2, 3, &  ! case 8  : subtet 1
       0, 0, 0, &  ! case 8  : subtet 2
       0, 0, 0, &  ! case 8  : subtet 3
       2, 3, 0, &  ! case 9  : subtet 1
       1, 2, 3, &  ! case 9  : subtet 2
       1, 2, 3, &  ! case 9  : subtet 3
       1, 3, 0, &  ! case 10 : subtet 1
       1, 2, 3, &  ! case 10 : subtet 2
       1, 2, 3, &  ! case 10 : subtet 3
       1, 2, 3, &  ! case 11 : subtet 1
       3, 0, 0, &  ! case 11 : subtet 2
       2, 3, 0, &  ! case 11 : subtet 3
       1, 2, 3, &  ! case 12 : subtet 1
       1, 2, 3, &  ! case 12 : subtet 2
       1, 2, 0, &  ! case 12 : subtet 3
       2, 0, 0, &  ! case 13 : subtet 1  (bm)
       1, 2, 0, &  ! case 13 : subtet 2  (source-bm)
       1, 2, 3, &  ! case 13 : subtet 3  (source-bm-bp)
       1, 0, 0, &  ! case 14 : subtet 1
       1, 2, 0, &  ! case 14 : subtet 2
       1, 2, 3, &  ! case 14 : subtet 3
       0, 0, 0, &  ! case 15 : subtet 1
       0, 0, 0, &  ! case 15 : subtet 2
       0, 0, 0  &  ! case 15 : subtet 3
       /), (/3,3,16/))

  !==== number of intersected edges (non-zero entries above) per case
  integer, parameter, dimension(0:15) :: nedges = (/ &
       0, 3, 3, 4, 3, 4, 4, 3, 3, 4, 4, 3, 4, 3, 3, 0 /) 
  !    0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15

  !==== number of vertices with G > 0 per subtet per case
  integer, parameter, dimension(3,0:15) :: nverts = reshape ( &
       (/ &
       0, 0, 0, &  ! case : 0  - subtets 1-3
       1, 0, 0, &  ! case : 1
       1, 0, 0, &  ! case : 2
       2, 1, 1, &  ! case : 3
       1, 0, 0, &  ! case : 4
       1, 2, 1, &  ! case : 5
       1, 1, 2, &  ! case : 6
       3, 2, 1, &  ! case : 7
       1, 0, 0, &  ! case : 8
       2, 1, 1, &  ! case : 9
       2, 1, 1, &  ! case : 10
       1, 3, 2, &  ! case : 11
       1, 1, 2, &  ! case : 12
       3, 2, 1, &  ! case : 13
       3, 2, 1, &  ! case : 14
       0, 0, 0  &  ! case : 15
       /), (/3,16/))

  type tet
     real(WP), dimension(3,4) :: vertex
     real(WP), dimension(4) :: G
     ! intersection points (max of 4 needed)
     real(WP), dimension(3,4) :: intp
     ! === properties with cut_ refer to volume with G > 0
     real(WP) :: cut_vol
     ! fluid volume weighted centroid
     real(WP), dimension(3) :: cut_wcent
     ! === properties of G=0 iso-surface
     real(WP) :: iso_area
     ! iso-area weighted centroid and normal
     real(WP), dimension(3) :: iso_wcent,iso_wnormal
     integer :: case,hand
  end type tet

  type simple_tet
     real(WP), dimension(3,4) :: vertex
     real(WP) :: vol
     real(WP), dimension(3) :: cent
  end type simple_tet

  type face_tet
     real(WP), dimension(3,4) :: vertex
     real(WP), dimension(3,4) :: intp
     real(WP), dimension(  4) :: G
     integer,  dimension(  3) :: ijk
     integer  :: dir
     logical  :: first
     real(WP) :: vol
  end type face_tet

  type(tet) :: tet_gl
  type(tri) :: tri_gl,subtri_gl
  ! face triangle can be split into 2 sub-triangles
  type(simple_tri),dimension(2) :: subtris_gl
  ! each cut tet may be split into 3 sub-tets
  type(simple_tet),dimension(3) :: subtets_gl

contains

  !##################################################
  !##################################################
  ! begin geomtry related routines

  function magnitude(v1) result(M)
    implicit none
    real(WP), dimension(3), intent(in) :: v1
    real(WP) :: M

    M = sqrt(dot_product(v1,v1))
  end function magnitude

  ! vertex is an array of vertices for the polygon(hedra)
  ! vertex(:,i) contains the x,y,z coordinates of vertex 'i'
  function tet_volume(vertex) result(V)
    implicit none
    real(WP),dimension(:,:), intent(in) :: vertex
    real(WP) :: V

    V = abs(signed_tet_volume(vertex))

  end function tet_volume

  ! vertex is an array of vertices for the polygon(hedra)
  ! vertex(:,i) contains the x,y,z coordinates of vertex 'i'
  function signed_tet_volume(vertex) result(V)
    implicit none
    real(WP),dimension(:,:), intent(in) :: vertex
    real(WP) :: V

    V = r16*dot_product(vertex(:,2)-vertex(:,1),&
         cross_product(vertex(:,3)-vertex(:,1),vertex(:,4)-vertex(:,1)))

  end function signed_tet_volume

  ! vertex is an array of vertices for the triangle/tetrahedra
  ! vertex(:,i) contains the x,y,z coordinates of vertex 'i'
  function poly_centroid(vertex) result(centroid)
    implicit none
    real(WP), dimension(:,:), intent(in) :: vertex
    real(WP), dimension(3) :: centroid
    !----------------------------------------

    centroid = sum(vertex,DIM=2)/real(size(vertex,DIM=2),WP)

  end function poly_centroid

  ! v1,v2,v3 are the three vertices of the triangle (any order) 
  function triangle_area(vertex) result(A)
    implicit none
    real(WP), dimension(:,:), intent(in) :: vertex
    real(WP) :: A

    A = 0.5_WP*magnitude(cross_product((vertex(:,3)-vertex(:,1)),&
         (vertex(:,3)-vertex(:,2))))
  end function triangle_area

  !compute the normal of a triangle using (v2-v1 X v3-v2)
  function triangle_normal(vertex) result(n)
    implicit none
    real(WP), dimension(:,:), intent(in) :: vertex
    real(WP), dimension(3) :: n

    n = normalize(cross_product(vertex(:,2)-vertex(:,1),&
         vertex(:,3)-vertex(:,2)))

  end function triangle_normal

  ! end geometry related routines
  !##################################################
  !##################################################

  subroutine HexToTets(hex_vertex,hex_G,ntet) 
    implicit none
    real(WP), dimension(3,8),intent(in) :: hex_vertex
    real(WP), dimension(8), intent(in) :: hex_G
    integer, intent(in) :: ntet

    tet_gl%vertex = hex_vertex(:,hex_to_tet(:,ntet))
    tet_gl%G = hex_G(hex_to_tet(:,ntet))
    tet_gl%hand = 2-mod(ntet,2) ! 1 for RH tets, 2 for LH tets
  end subroutine HexToTets

  ! compute case of tet based on G
  function tet_case(G) result(case)
    implicit none
    real(WP), dimension(4), intent(in) :: G
    integer :: case

    ! sum over vertices
    case = int(0.5_WP+sign(0.51_WP,G(1)))+&
         2*int(0.5_WP+sign(0.51_WP,G(2)))+&
         4*int(0.5_WP+sign(0.51_WP,G(3)))+&
         8*int(0.5_WP+sign(0.51_WP,G(4)))
  end function tet_case

  ! ======================================================================
  ! compute intersection points of a tet via linear interpolation
  !  tet is defined by:
  !  -  case
  !  -  vertices
  !  -  G - values
  function tet_intp_lerp(case,verts,G) result(intp)
    implicit none
    integer, intent(in) :: case
    real(WP), dimension(3,4), intent(in) :: verts
    real(WP), dimension(  4), intent(in) :: G
    real(WP), dimension(3,4) :: intp
    ! -------------------------------
    integer :: i,v1,v2

    ! loop over edges
    do i=1,nedges(case)
       !estimate intersection point
       v1 = edge_to_vertex(1,case_to_edge(i,case))
       v2 = edge_to_vertex(2,case_to_edge(i,case))
       intp(1:3,i) = verts(:,v1)- G(v1)*(verts(:,v2)-verts(:,v1))/safe_epsilon(G(v2)-G(v1))
    end do

  end function tet_intp_lerp

  subroutine RecToTris(rec_vertex,rec_G,ntri)
    implicit none
    real(WP), dimension(3,4),intent(in) :: rec_vertex
    real(WP), dimension(4), intent(in) :: rec_G
    integer, intent(in) :: ntri

    tri_gl%vertex = rec_vertex(:,rec_to_tri(:,ntri))
    tri_gl%G = rec_G(rec_to_tri(:,ntri))

  end subroutine RecToTris

  function tri_case(G) result(case)
    implicit none
    real(WP), dimension(3), intent(in) :: G
    integer :: case
    ! sum over vertices
    case = int(0.5_WP+sign(0.51_WP,G(1)))+&
         2*int(0.5_WP+sign(0.51_WP,G(2)))+&
         4*int(0.5_WP+sign(0.51_WP,G(3)))
  end function tri_case

  subroutine triIntersectionPtsLinear()
    implicit none

    real(WP), dimension(3) :: p1,p2
    real(WP) :: m
    integer :: i,iedge

    ! loop over edges
    do i=1,tri_nedge(tri_gl%case)
       !estimate intersection point
       iedge = tri_case_to_edge(i,tri_gl%case)
       p1 = tri_gl%vertex(:,tri_edge_to_vertex(1,iedge))
       p2 = tri_gl%vertex(:,tri_edge_to_vertex(2,iedge))
       m = -tri_gl%G(tri_edge_to_vertex(1,iedge))/(tri_gl%G(tri_edge_to_vertex(2,iedge))-tri_gl%G(tri_edge_to_vertex(1,iedge)))
       tri_gl%intp(1:3,i) = p1 + m *(p2-p1)
    end do

  end subroutine triIntersectionPtsLinear

  subroutine subtriIntersectionPtsLinear()
    implicit none

    real(WP), dimension(3) :: p1,p2
    real(WP) :: m
    integer :: i,iedge

    ! loop over edges
    do i=1,tri_nedge(subtri_gl%case)
       !estimate intersection point
       iedge = tri_case_to_edge(i,subtri_gl%case)
       p1 = subtri_gl%vertex(:,tri_edge_to_vertex(1,iedge))
       p2 = subtri_gl%vertex(:,tri_edge_to_vertex(2,iedge))
       m = -subtri_gl%G(tri_edge_to_vertex(1,iedge))/ &
            (subtri_gl%G(tri_edge_to_vertex(2,iedge))-subtri_gl%G(tri_edge_to_vertex(1,iedge)))
       subtri_gl%intp(1:3,i) = p1 + m *(p2-p1)
    end do

  end subroutine subtriIntersectionPtsLinear

  !##################################################
  !##################################################
  ! at this point case and interections points have been set
  ! so compute surface areas, volumes, and centroids
  
  !==================================================
  ! compute iso-surface geometry - uses tet_gl
  !==================================================
  subroutine tet_iso_geometry()
    implicit none
    integer :: ntris,i,hand

    ! number of triangles to decompose IB
    ntris = iso_nsubtris(tet_gl%case)
    do i=1,ntris
       ! copy triangle vertices
       subtris_gl(i)%vertex = &
            tet_gl%intp(:,tet_intp_to_subtri(:,i,tet_gl%case))
       ! compute geometric properties
       subtris_gl(i)%area = triangle_area(subtris_gl(i)%vertex)
       subtris_gl(i)%cent = poly_centroid(subtris_gl(i)%vertex)
       ! triangle-hand
       hand = tet_hand_tri_hand(i+(tet_gl%hand-1)*(3-2*i),tet_gl%case)
       subtris_gl(i)%normal = real(3-2*hand,WP)*triangle_normal(subtris_gl(i)%vertex)
    end do
    ! sum up iso-surface properties
    tet_gl%iso_area=0.0_WP; tet_gl%iso_wcent=0.0_WP; tet_gl%iso_wnormal=0.0_WP
    do i=1,ntris
       tet_gl%iso_area = tet_gl%iso_area + subtris_gl(i)%area
       tet_gl%iso_wcent = tet_gl%iso_wcent + &
            subtris_gl(i)%area*subtris_gl(i)%cent
       tet_gl%iso_wnormal = tet_gl%iso_wnormal + &
            subtris_gl(i)%area*subtris_gl(i)%normal
    end do
  end subroutine tet_iso_geometry

  ! split tet into subtets
  subroutine subtets_from_tet(case,verts,intp)
    implicit none
    integer, intent(in) :: case
    real(WP), dimension(3,4), intent(in) :: verts,intp
    integer :: np,nv,i

    ! loop over subtets and create from verts and intp
    do i=1,nsubtets(case)
       ! number of original tet vertices in sub-tet
       nv = nverts(i,case)
       ! copy tet vertices
       subtets_gl(i)%vertex(:,tet_verts_to_subtet(1:nv,i,case)) = &
            verts(:,tet_verts_to_subtet(1:nv,i,case))
       ! copy tet intersection points
       np = 4-nv
       subtets_gl(i)%vertex(:,subtet_intp_order(1:np,i,case)) = &
            intp(:,tet_intp_to_subtet(1:np,i,case))
       subtets_gl(i)%vol = tet_volume(subtets_gl(i)%vertex)
       subtets_gl(i)%cent = poly_centroid(subtets_gl(i)%vertex)
    end do
  end subroutine subtets_from_tet

  ! geometry of the cut volumes
  subroutine tet_cut_geometry()
    implicit none
    integer :: i

    ! form subtets
    call subtets_from_tet(tet_gl%case,tet_gl%vertex,tet_gl%intp)

    ! sum up volume properties
    tet_gl%cut_vol=0.0_WP;tet_gl%cut_wcent=0.0_WP
    do i = 1,nsubtets(tet_gl%case)
       tet_gl%cut_vol = tet_gl%cut_vol + subtets_gl(i)%vol
       tet_gl%cut_wcent= tet_gl%cut_wcent+ subtets_gl(i)%vol*subtets_gl(i)%cent
    end do

  end subroutine tet_cut_geometry
  
  subroutine facetetIntersectionData(ftet,tets,cc,ntets)
    implicit none
    type(face_tet), intent(in) :: ftet
    type(face_tet), dimension(3), intent(out) :: tets
    integer, intent(in) :: cc
    integer, intent(out):: ntets
    integer :: np,nv,i

    ! number of subtets
    ntets = nsubtets(cc)
    do i=1,ntets
       ! number of original tet vertices in sub-tet
       nv = nverts(i,cc)
       ! copy tet vertices
       tets(i)%vertex(:,tet_verts_to_subtet(1:nv,i,cc)) = &
            ftet%vertex(:,tet_verts_to_subtet(1:nv,i,cc))
       ! copy tet intersection points
       np = 4-nv
       tets(i)%vertex(:,subtet_intp_order(1:np,i,cc)) = &
            ftet%intp(:,tet_intp_to_subtet(1:np,i,cc))
       tets(i)%vol = tet_volume(tets(i)%vertex)
       ! copy ijk coords
       tets(i)%ijk  = ftet%ijk
       tets(i)%dir = ftet%dir
       tets(i)%first = .false.
    end do

  end subroutine facetetIntersectionData

  subroutine triIntersectionData()
    implicit none
    integer :: ntris,itri,nv,np

    ! number of sub-triangles
    ntris = nsubtris(tri_gl%case)
    do itri=1,ntris
       ! number of original triangle vertices in subtri
       nv = tri_nverts(itri,tri_gl%case)
       ! copy triangle vertices
       subtris_gl(itri)%vertex(:,tri_verts_to_subtri(1:nv,itri,tri_gl%case)) = &
            tri_gl%vertex(:,tri_verts_to_subtri(1:nv,itri,tri_gl%case))
       ! copy triangle intersection points
       np = tri_nintp(itri,tri_gl%case)
       subtris_gl(itri)%vertex(:,subtri_intp_order(1:np,itri,tri_gl%case)) = &
            tri_gl%intp(:,tri_intp_to_subtri(1:np,itri,tri_gl%case))
       subtris_gl(itri)%area = triangle_area(subtris_gl(itri)%vertex)
       subtris_gl(itri)%cent = poly_centroid(subtris_gl(itri)%vertex)
    end do

    ! sum up area,centroids
    tri_gl%cut_area = 0.0_WP; tri_gl%cut_wcent = 0.0_WP
    do itri=1,ntris
       tri_gl%cut_area = tri_gl%cut_area + subtris_gl(itri)%area
       tri_gl%cut_wcent = tri_gl%cut_wcent + subtris_gl(itri)%area*subtris_gl(itri)%cent
    end do
    ! length
    tri_gl%iso_length = sqrt(sum( (tri_gl%intp(:,1)-tri_gl%intp(:,2))**2 ))
  end subroutine triIntersectionData

  !##################################################
  !##################################################

  subroutine splitTetLinear()
    implicit none

    ! first determine case
    tet_gl%case= tet_case(tet_gl%G)
    if (tet_gl%case == 0) then
       ! all nodes are negative
       tet_gl%cut_vol= 0.0_WP
       tet_gl%cut_wcent=0.0_WP
       ! no surface area here
       tet_gl%iso_area=0.0_WP
       tet_gl%iso_wcent=0.0_WP
    else if(tet_gl%case == 15) then
       ! all nodes are positive
       tet_gl%cut_vol=tet_volume(tet_gl%vertex)
       tet_gl%cut_wcent=tet_gl%cut_vol*poly_centroid(tet_gl%vertex)
       ! no surface area here
       tet_gl%iso_area=0.0_WP
       tet_gl%iso_wcent=0.0_WP
    else
       ! determine intersection points
       tet_gl%intp = tet_intp_lerp(tet_gl%case,tet_gl%vertex,tet_gl%G)
       ! compute iso-surface geometry
       call tet_iso_geometry()
       ! compute geometry of cut-volume
       call tet_cut_geometry()
    end if

  end subroutine splitTetLinear

  subroutine splitTriLinear()
    implicit none

    ! first determine case
    tri_gl%case=tri_case(tri_gl%G)
    if (tri_gl%case == 0) then
       ! all nodes are negative
       tri_gl%cut_area= 0.0_WP
       tri_gl%cut_wcent=0.0_WP
       tri_gl%iso_length=0.0_WP
    else if(tri_gl%case == 7) then
       ! all nodes are positive
       tri_gl%cut_area=triangle_area(tri_gl%vertex)
       tri_gl%cut_wcent=tri_gl%cut_area*poly_centroid(tri_gl%vertex)
       tri_gl%iso_length=0.0_WP
    else
       ! determine intersection points
       call triIntersectionPtsLinear()
       call triIntersectionData()
    end if

  end subroutine splitTriLinear

end module lagrange_geom
!##################################################
!##################################################

subroutine marching_tets(hex_vertex,G,vol,v_cent,area,a_cent,normal)
  use lagrange_geom
  implicit none
  real(WP), dimension(3,8), intent(in) :: hex_vertex
  real(WP), dimension(8  ), intent(in) :: G
  real(WP), intent(out) :: vol,area
  real(WP), intent(out),dimension(3) :: v_cent,a_cent,normal

  !--------------------------------------------------
  integer :: i,cubeCase
  real(WP), dimension(3) :: c_dxyz

  ! initialize case
  cubeCase = sum(int(0.5_WP+sign(0.51_WP,G)))
  ! initialize volume/area
  vol = 0.0_WP; v_cent = 0.0_WP; 
  area = 0.0_WP; a_cent = 0.0_WP; normal=0.0_WP

  if(cubeCase == 0) then
     ! no positive G values
     return
  end if

  if(cubeCase == 8) then
     ! all positive G values
     c_dxyz = hex_vertex(:,8)-hex_vertex(:,1)
     vol = product(c_dxyz)
     v_cent = poly_centroid(hex_vertex)
     return
  end if

  ! cube is intersected by G=0

  ! split cube into tets 
  do i=1,6
     call HexToTets(hex_vertex,G,i)
     call splitTetLinear()
     
     ! sum up volume and areas
     vol = vol + tet_gl%cut_vol
     v_cent = v_cent + tet_gl%cut_wcent

     area = area + tet_gl%iso_area
     a_cent = a_cent + tet_gl%iso_wcent
     normal = normal + tet_gl%iso_wnormal
  end do

  if(vol > 0.0_WP) v_cent = v_cent/vol
  if(area> 0.0_WP) then
     a_cent = a_cent/area
     normal = normalize(normal/area)
  end if

  return
end subroutine marching_tets

subroutine marching_simple_tets(hex_vertex,G,vol,vcent)
  use lagrange_geom
  implicit none
  real(WP), dimension(3,8), intent(in) :: hex_vertex
  real(WP), dimension(8  ), intent(in) :: G
  real(WP), intent(out) :: vol
  real(WP), dimension(3), intent(out) :: vcent

  !--------------------------------------------------
  integer :: i,cubeCase
  real(WP) :: tmp

  ! initialize case
  cubeCase = sum(int(0.5_WP+sign(0.51_WP,G)))
  ! initialize volume/area
  vol = 0.0_WP; vcent = 0.0_WP

  if(cubeCase == 0) then
     ! no positive G values
     return
  end if

  if(cubeCase == 8) then
     ! all positive G values
     vol = product(hex_vertex(:,8)-hex_vertex(:,1))
     vcent = poly_centroid(hex_vertex)
     return
  end if

  ! cube is intersected by G=0, split into tets
  do i=1,6
     call HexToTets(hex_vertex,G,i)
     tet_gl%case = tet_case(tet_gl%G)
     select case (tet_gl%case)
        case (15) ! all G > 0
           tmp = tet_volume(tet_gl%vertex)
           vol = vol + tmp
           vcent = vcent + tmp*poly_centroid(tet_gl%vertex) 
        case (1:14) ! tet intersected by G=0
           ! determine intersection points
           tet_gl%intp = tet_intp_lerp(tet_gl%case,tet_gl%vertex,tet_gl%G)
           ! compute cut volume
           call tet_cut_geometry()
           vol = vol + tet_gl%cut_vol
           vcent = vcent + tet_gl%cut_wcent
     end select
  end do
  vcent = vcent/(vol+epsilon(1.0_WP))

  return
end subroutine marching_simple_tets

! return the triangles which make up the ib-surface
subroutine ib_surf_triangles(hex_vertex,G,face_tris,nn)
  use lagrange_geom
  implicit none
  real(WP), dimension(3,8), intent(in) :: hex_vertex
  real(WP), dimension(8  ), intent(in) :: G
  type(lagrange_tri), dimension(max_ltris), intent(out) :: face_tris
  integer, intent(out) :: nn

  !--------------------------------------------------
  integer :: i,itri,ntris

  nn=0
  ! split cube into tets 
  do i=1,6
     call HexToTets(hex_vertex,G,i)
     tet_gl%case = tet_case(tet_gl%G)
     if(tet_gl%case.gt.0.and.tet_gl%case.lt.15) then
        tet_gl%intp = tet_intp_lerp(tet_gl%case,tet_gl%vertex,tet_gl%G)
        ! build up lagrange subtriangles
        ntris = iso_nsubtris(tet_gl%case)
        do itri=1,ntris
           nn = nn+1
           ! copy triangle vertices
           face_tris(nn)%vertex = &
                tet_gl%intp(:,tet_intp_to_subtri(:,itri,tet_gl%case))
           ! all vertices are ib
           face_tris(nn)%is_ib = 1
           ! triangle-hand
           face_tris(nn)%hand = tet_hand_tri_hand(itri+(tet_gl%hand-1)*(3-2*itri),tet_gl%case)
        end do
     end if

  end do

  return
end subroutine ib_surf_triangles

! ========================================
! Form an extruded face via face_tri and project_tri
! Split face into 3 tets and compute signed volume
! ========================================
subroutine cut_extruded_triangle(pverts,face_tri,face_tets,fi,fj,fk)
  use lagrange_geom
  implicit none
  real(WP), dimension(3,3), intent(in) :: pverts
  type(lagrange_tri), intent(in) :: face_tri
  type(face_tet), dimension(3), intent(out) :: face_tets
  integer, intent(in) :: fi,fj,fk
  ! -----------------
  integer :: c,it,np,nv

  ! -- ordering for the subtets is the same as case 7
  ! with pverts as intp - reuse tables
  c = 7
  do it=1,3
     ! number of original tet vertices in sub-tet
     nv = nverts(it,c)
     ! copy face_tri vertices
     face_tets(it)%vertex(:,tet_verts_to_subtet(1:nv,it,c)) = &
          face_tri%vertex(:,tet_verts_to_subtet(1:nv,it,c))
     ! copy projected tri vertices intersection points
     np = 4-nv
     face_tets(it)%vertex(:,subtet_intp_order(1:np,it,c)) = &
          pverts(:,tet_intp_to_subtet(1:np,it,c))
     ! assign and ijk - used in plane-cutting/localization routines
     face_tets(it)%ijk = (/fi,fj,fk/)
     ! compute signed volume and correct 
     face_tets(it)%vol =  hand_to_vol(it,face_tri%hand) * &
          signed_tet_volume(face_tets(it)%vertex)
     ! initialize cutting direction
     face_tets(it)%dir = 1
     face_tets(it)%first = .true.
  end do

end subroutine cut_extruded_triangle

!=====================================
! Cut a tet by a plane and form (+)/(-) tets
! based on which side of plane they fall on
!====================================
subroutine cut_tet_by_plane(ftet,ptets,mtets,nptets,nmtets,plane_loc,plane_dir,inc,ijk_clip)
  use lagrange_geom
  implicit none
  type(face_tet), intent(inout) :: ftet  ! input face_tet
  type(face_tet), dimension(3), intent(out):: ptets,mtets ! +/- tets
  integer, intent(out) :: nptets,nmtets ! number of +/- tets
  real(WP), intent(in) :: plane_loc
  integer, intent(in) :: plane_dir,inc
  integer, dimension(2), intent(in) :: ijk_clip
  !------------------------
  integer :: case,i

  ! compute G/case based on plane distance
  ftet%G = ftet%vertex(plane_dir,:) - plane_loc
  case = tet_case(ftet%G)
  ! check for trivial cases
  if(case==0) then
     ! tet on only minus side
     nptets=0; nmtets=1
     mtets(1) = ftet
     ! ensure volume is positive
     mtets(1)%vol = abs(ftet%vol)
     mtets(1)%first = .false.
  elseif(case==15) then
     ! tet only on plus side
     nptets=1; nmtets=0
     ptets(1) = ftet
     ptets(1)%vol = abs(ftet%vol)
     ptets(1)%first = .false.
  else
     ! tet cut by plane 
     ftet%intp = tet_intp_lerp(case,ftet%vertex,ftet%G)
     call facetetIntersectionData(ftet,ptets,case,nptets)
     call facetetIntersectionData(ftet,mtets,15-case,nmtets)
  end if

  ! increment vertices of tets depending of direction of cutting
  if(inc.eq.1) then
     do i=1,nptets
        if(ptets(i)%ijk(plane_dir).eq.ijk_clip(2)) then
           ! done clipping
           ptets(i)%dir = ftet%dir+1
           ptets(i)%first = .true.
        else
           ptets(i)%ijk(plane_dir) = ptets(i)%ijk(plane_dir)+inc
        end if
     end do
     if(.not.ftet%first) then
        ! mark (-) tets as done by increasing dir
        do i=1,nmtets
           mtets(i)%dir = ftet%dir+1
           mtets(i)%first = .true.
        end do
     end if
  else ! inc.eq.-1
     do i=1,nmtets
        if(mtets(i)%ijk(plane_dir) .eq. ijk_clip(1)) then
           ! done clipping
           mtets(i)%dir = ftet%dir+1
           mtets(i)%first = .true.
        else
           mtets(i)%ijk(plane_dir) = mtets(i)%ijk(plane_dir)+inc
        end if
     end do
     if(.not.ftet%first) then
        ! mark (+) tets as done
        do i=1,nptets
           ptets(i)%dir = ftet%dir+1
           ptets(i)%first = .true.
        end do
     end if
  end if

end subroutine cut_tet_by_plane

!=====================================
! Cut a tet by old G-field and form (+)/(-) tets
! based on which side of plane they fall on
!====================================
subroutine cut_tet_by_oldG(ftet,ptets,nptets)
  use lagrange_geom
  implicit none
  type(face_tet), intent(inout) :: ftet  ! input face_tet
  type(face_tet), dimension(3), intent(out):: ptets ! + tets
  integer, intent(out) :: nptets ! number of +/i tets
  !------------------------
  integer :: case,i
  
  ! compute case based on plane distance
  case = tet_case(ftet%G)
  ! check for trivial cases
  if(case==0) then
     ! tet on only minus side
     nptets=0
     return
  elseif(case==15) then
     ! tet only on plus side
     nptets=1
     ptets(1) = ftet
     return
  else
     ! tet cut by IB
     ftet%intp = tet_intp_lerp(case,ftet%vertex,ftet%G)
     call facetetIntersectionData(ftet,ptets,case,nptets)
     ! correct change to 'first' and
     ! make sign of volume same as original tet
     do i=1,nptets
        ptets(i)%first = .true.
        ptets(i)%vol = sign(ptets(i)%vol,ftet%vol)
     end do
     
  end if
  
end subroutine cut_tet_by_oldG

subroutine cut_tet_by_plic(ftet,ptets,mtets,nptets,nmtets)
  use lagrange_geom
  implicit none
  type(face_tet), intent(inout) :: ftet
  type(face_tet), dimension(3), intent(out):: ptets,mtets ! +/- tets
  integer, intent(out) :: nptets,nmtets ! number of +/- tets  
  integer :: case

  !--------------------------------------------------
  ! compute G/case based on plane distance
  case = tet_case(ftet%G)
  ! check for trivial cases
  if(case==0) then
     ! tet on only minus side
     nptets=0; nmtets=1
     mtets(1) = ftet
     ! ensure volume is positive
     mtets(1)%vol = abs(ftet%vol)
     mtets(1)%first = .false.
  elseif(case==15) then
     ! tet only on plus side
     nptets=1; nmtets=0
     ptets(1) = ftet
     ptets(1)%vol = abs(ftet%vol)
     ptets(1)%first = .false.
  else
     ! tet cut by plane 
     ftet%intp = tet_intp_lerp(case,ftet%vertex,ftet%G)
     call facetetIntersectionData(ftet,ptets,case,nptets)
     call facetetIntersectionData(ftet,mtets,15-case,nmtets)
  end if

  return
end subroutine cut_tet_by_plic

! === 2D cutting
subroutine marching_tris(rec_vertex,G,length,area,a_cent)
  use lagrange_geom
  implicit none
  real(WP), dimension(3,4), intent(in) :: rec_vertex
  real(WP), dimension(4  ), intent(in) :: G
  real(WP), intent(out) :: area,length
  real(WP), intent(out),dimension(3) :: a_cent

  !--------------------------------------------------
  integer :: i, recCase
  ! initialize case
  recCase = 0
  ! initialize area
  area = 0.0_WP; a_cent = 0.0_WP; length = 0.0_WP

  do i=1,4
     recCase = recCase + int(0.5_WP+sign(0.51_WP,G(i)))
  end do

  if(recCase == 0) then
     ! no positive G values
     return
  end if

  if(recCase == 4) then
     ! all positive G values
     area = maxval(rec_vertex(:,2)-rec_vertex(:,1))*&
          maxval(rec_vertex(:,3)-rec_vertex(:,1))
     a_cent = poly_centroid(rec_vertex)
     return
  end if

  ! rec is intersected by G=0

  ! split rec into tris
  do i=1,2
     call RecToTris(rec_vertex,G,i)
     call splitTriLinear()
     ! sum up areas
     area = area + tri_gl%cut_area
     a_cent = a_cent + tri_gl%cut_wcent
     length = length + tri_gl%iso_length
  end do

  if(area> 0.0_WP) a_cent = a_cent/area

  return
end subroutine marching_tris

! ===============================
! take a face described by rec_vertex and split into triangles
! which are then cut by IB (into more triangles)
subroutine face_triangles(rec_vertex,G,base_hand,face_tris,nn,masked)
  use lagrange_geom
  implicit none
  real(WP), dimension(3,4), intent(in) :: rec_vertex
  real(WP), dimension(4  ), intent(in) :: G
  integer, intent(in)  :: base_hand
  type(lagrange_tri), dimension(max_ltris), intent(out) :: face_tris
  integer, intent(out) :: nn
  logical, dimension(4), intent(in) :: masked

  !--------------------------------------------------
  integer :: i,itri,nv,np,hand

  ! split rec into tris
  nn = 0
  do i=1,2
     call RecToTris(rec_vertex,G,i)
     tri_gl%case=tri_case(tri_gl%G)
     if(tri_gl%case.eq.0) cycle

     hand = base_hand+(i-1)*(3-2*base_hand)

     if(tri_gl%case.eq.7) then
        nn = nn + 1
        face_tris(nn)%vertex = tri_gl%vertex
        face_tris(nn)%is_ib = 0
        face_tris(nn)%hand = hand
        face_tris(nn)%masked = masked(rec_to_tri(:,i))
     else
        call triIntersectionPtsLinear()
        ! split tri into subtris
        do itri=1,nsubtris(tri_gl%case)
           nn = nn +1
           ! number of original triangle vertices in subtri
           nv = tri_nverts(itri,tri_gl%case)
           ! copy triangle vertices
           face_tris(nn)%vertex(:,tri_verts_to_subtri(1:nv,itri,tri_gl%case)) = &
                tri_gl%vertex(:,tri_verts_to_subtri(1:nv,itri,tri_gl%case))
           ! copy triangle intersection points
           np = tri_nintp(itri,tri_gl%case)
           face_tris(nn)%vertex(:,subtri_intp_order(1:np,itri,tri_gl%case)) = &
                tri_gl%intp(:,tri_intp_to_subtri(1:np,itri,tri_gl%case))
           face_tris(nn)%is_ib = 0
           face_tris(nn)%is_ib(subtri_intp_order(1:np,itri,tri_gl%case)) = 1
           ! compute handedness (only second subtri flips handedness)
           face_tris(nn)%hand = hand + &
                (1-itri)*subtri_hand_flip(tri_gl%case)*(3-2*hand)
           ! propagate masks - not correct...
           face_tris(nn)%masked = .false.
        end do
     end if
  end do


  return
end subroutine face_triangles
