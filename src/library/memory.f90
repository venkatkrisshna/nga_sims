module memory
  use precision
  implicit none
  
  ! Arrays to be used for computation in solver:
  ! -> velocity
  ! -> scalar_quick
  ! -> SGS_lagrangian
  ! -> combustion
  real(WP), dimension(:,:,:), allocatable, target :: tmp1
  real(WP), dimension(:,:,:), allocatable, target :: tmp2
  real(WP), dimension(:,:,:), allocatable, target :: tmp3
  real(WP), dimension(:,:,:), allocatable, target :: tmp4
  real(WP), dimension(:,:,:), allocatable, target :: tmp5
  real(WP), dimension(:,:,:), allocatable, target :: tmp6
  real(WP), dimension(:,:,:), allocatable, target :: tmp7
  real(WP), dimension(:,:,:), allocatable, target :: tmp8
  real(WP), dimension(:,:,:), allocatable, target :: tmp9
  real(WP), dimension(:,:,:), allocatable, target :: tmp10
  real(WP), dimension(:,:,:), allocatable, target :: tmp11
  real(WP), dimension(:,:,:), allocatable, target :: tmp12
  real(WP), dimension(:,:,:), allocatable, target :: tmp13
  real(WP), dimension(:,:,:), allocatable, target :: tmp14
  real(WP), dimension(:,:,:), allocatable, target :: tmp15
  real(WP), dimension(:,:,:), allocatable, target :: tmp16

  ! 4D arrays for subcells (s,i,j,k)
  real(WP), dimension(:,:,:,:), allocatable, target :: tmp4d1
  real(WP), dimension(:,:,:,:), allocatable, target :: tmp4d2
  real(WP), dimension(:,:,:,:), allocatable, target :: tmp4d3
  real(WP), dimension(:,:,:,:), allocatable, target :: tmp4d4
  real(WP), dimension(:,:,:,:), allocatable, target :: tmp4d5
  real(WP), dimension(:,:,:,:), allocatable, target :: tmp4d6
  real(WP), dimension(:,:,:,:), allocatable, target :: tmp4d7
  real(WP), dimension(:,:,:,:), allocatable, target :: tmp4d8
  
  ! The following arrays are just allocatable to the previous arrays
  real(WP), dimension(:,:,:), pointer :: FX
  real(WP), dimension(:,:,:), pointer :: FY
  real(WP), dimension(:,:,:), pointer :: FZ
  real(WP), dimension(:,:,:), pointer :: rhoUi
  real(WP), dimension(:,:,:), pointer :: rhoVi
  real(WP), dimension(:,:,:), pointer :: rhoWi
  real(WP), dimension(:,:,:), pointer :: Fcyl
  real(WP), dimension(:,:,:), pointer :: Fcylv
  
  ! For the ADI
  real(WP), dimension(:,:,:),   allocatable :: Rx,Ry,Rz
  real(WP), dimension(:,:,:,:), allocatable :: Ax,Ay,Az
  complex(WP), dimension(:,:,:),   allocatable :: cRx,cRy,cRz
  complex(WP), dimension(:,:,:,:), allocatable :: cAx,cAy,cAz
  
  ! Work array for multidiagonal solvers
  real(WP), dimension(:,:), allocatable :: stackmem
  complex(WP), dimension(:,:), allocatable :: cstackmem

  
  ! Temporary integer array
  integer, dimension(:,:,:), allocatable :: tmpi1

  ! ================================= !
  ! Reallocate array with more memory !
  ! ================================= !
  interface reallocate
     module procedure reallocate_int_1d
  end interface reallocate
  
contains  
    subroutine reallocate_int_1D(A,n_inc)
      implicit none
      integer, dimension(:), allocatable, intent(inout) :: A ! array
      integer, intent(in) :: n_inc              ! number of elements to add to A
      integer, dimension(:), allocatable :: Atmp
      integer :: n

      n=size(A)
      ! Save data
      allocate(Atmp(n))
      Atmp=A
      ! Reallocate A
      deallocate(A)
      allocate(A(n+n_inc))
      ! Transfer back to A
      A(1:n)=Atmp

      return
    end subroutine reallocate_int_1D 
    
    subroutine reallocate_real_1D(A,n_inc)
      implicit none
      real(WP), dimension(:), allocatable, intent(inout) :: A ! array
      integer, intent(in) :: n_inc              ! number of elements to add to A
      real(WP), dimension(:), allocatable :: Atmp
      integer :: n

      n=size(A)
      ! Save data
      allocate(Atmp(n))
      Atmp=A
      ! Reallocate A
      deallocate(A)
      allocate(A(n+n_inc))
      ! Transfer back to A
      A(1:n)=Atmp

      return
    end subroutine reallocate_real_1D 
  
end module memory


subroutine memory_init(i1,i2,j1,j2,k1,k2,n)
  use memory
  implicit none
  integer, intent(in) :: i1,i2,j1,j2,k1,k2,n
  
  ! Allocate the main temporary arrays
  allocate(tmp1 (i1:i2,j1:j2,k1:k2))
  allocate(tmp2 (i1:i2,j1:j2,k1:k2))
  allocate(tmp3 (i1:i2,j1:j2,k1:k2))
  allocate(tmp4 (i1:i2,j1:j2,k1:k2))
  allocate(tmp5 (i1:i2,j1:j2,k1:k2))
  allocate(tmp6 (i1:i2,j1:j2,k1:k2))
  allocate(tmp7 (i1:i2,j1:j2,k1:k2))
  allocate(tmp8 (i1:i2,j1:j2,k1:k2))
  allocate(tmp9 (i1:i2,j1:j2,k1:k2))
  allocate(tmp10(i1:i2,j1:j2,k1:k2))
  allocate(tmp11(i1:i2,j1:j2,k1:k2))
  allocate(tmp12(i1:i2,j1:j2,k1:k2))
  allocate(tmp13(i1:i2,j1:j2,k1:k2))
  allocate(tmp14(i1:i2,j1:j2,k1:k2))
  allocate(tmp15(i1:i2,j1:j2,k1:k2))
  allocate(tmp16(i1:i2,j1:j2,k1:k2))

  ! Allocate 4d arrays
  allocate(tmp4d1(8,i1:i2,j1:j2,k1:k2))
  allocate(tmp4d2(8,i1:i2,j1:j2,k1:k2))
  allocate(tmp4d3(8,i1:i2,j1:j2,k1:k2))
  allocate(tmp4d4(8,i1:i2,j1:j2,k1:k2))
  allocate(tmp4d5(8,i1:i2,j1:j2,k1:k2))
  allocate(tmp4d6(8,i1:i2,j1:j2,k1:k2))
  allocate(tmp4d7(8,i1:i2,j1:j2,k1:k2))
  allocate(tmp4d8(8,i1:i2,j1:j2,k1:k2))

  ! Allocate integer arrays
  allocate(tmpi1(i1:i2,j1:j2,k1:k2))
  
  ! Allocate arrays for tri/penta-diagonal solvers
  allocate(stackmem((i2-i1)*(j2-j1)*(k2-k1),2*n+1))
  allocate(cstackmem((i2-i1)*(j2-j1)*(k2-k1),2*n+1))
  
  ! For the approximate factorization
  allocate(Ax(j1+n:j2-n,k1+n:k2-n,i1+n:i2-n,-n:+n))
  allocate(Ay(i1+n:i2-n,k1+n:k2-n,j1+n:j2-n,-n:+n))
  allocate(Az(i1+n:i2-n,j1+n:j2-n,k1+n:k2-n,-n:+n))
  allocate(cAx(j1+n:j2-n,k1+n:k2-n,i1+n:i2-n,-n:+n))
  allocate(cAy(i1+n:i2-n,k1+n:k2-n,j1+n:j2-n,-n:+n))
  allocate(cAz(i1+n:i2-n,j1+n:j2-n,k1+n:k2-n,-n:+n))
  
  allocate(Rx(j1+n:j2-n,k1+n:k2-n,i1+n:i2-n))
  allocate(Ry(i1+n:i2-n,k1+n:k2-n,j1+n:j2-n))
  allocate(Rz(i1+n:i2-n,j1+n:j2-n,k1+n:k2-n))
  allocate(cRx(j1+n:j2-n,k1+n:k2-n,i1+n:i2-n))
  allocate(cRy(i1+n:i2-n,k1+n:k2-n,j1+n:j2-n))
  allocate(cRz(i1+n:i2-n,j1+n:j2-n,k1+n:k2-n))
  
  ! Create the link for some nicer named allocatables
  rhoUi   => tmp1
  rhoVi   => tmp2
  rhoWi   => tmp3
  FX      => tmp4
  FY      => tmp5
  FZ      => tmp6
  Fcyl    => tmp7
  Fcylv   => tmp8
  
  return
end subroutine memory_init

