module ib_velocity
use math
use precision
implicit none
real(WP) :: G_radius, G_radius1,G_center(3), G_radius_motion, G_radius_motion_z
real(WP) :: mms_mu, mms_rho, mms_scd
contains
real(WP) function ibfile_velocity_U(xyz,inTime)
implicit none
real(WP), dimension(3), intent(in) :: xyz
real(WP), intent(in) :: inTime
ibfile_velocity_U = 0.0_WP
end function
real(WP) function ibfile_velocity_Gib(xyz,inTime)
implicit none
real(WP), dimension(3), intent(in) :: xyz
real(WP), intent(in) :: inTime
ibfile_velocity_Gib = G_radius - sqrt((-G_center(2) + xyz(2))**2)
end function
real(WP) function ibfile_velocity_W(xyz,inTime)
implicit none
real(WP), dimension(3), intent(in) :: xyz
real(WP), intent(in) :: inTime
ibfile_velocity_W = 0.0_WP
end function
real(WP) function ibfile_velocity_V(xyz,inTime)
implicit none
real(WP), dimension(3), intent(in) :: xyz
real(WP), intent(in) :: inTime
ibfile_velocity_V = 0.0_WP
end function
end module ib_velocity
subroutine ib_velocity_init
use ib_velocity
use parser
implicit none
call parser_read('Initial center x',G_center(1),0.0_WP)
call parser_read('Initial center y',G_center(2),0.0_WP)
call parser_read('Initial center z',G_center(3),0.0_WP)
call parser_read('Particle diameter', G_radius, 0.0_WP)
G_radius = 0.5_WP*G_radius
call parser_read('IB rad0', G_radius, G_radius)
call parser_read('IB rad1', G_radius1, 0.0_WP)
call parser_read('IB radius motion', G_radius_motion, 0.0_WP)
end subroutine ib_velocity_init
