! ========================== !
! Legendre polynomials in 3D !
! ========================== !
module legendre
  use precision
  use polynomial
  
contains
  
  ! ======================================================== !
  ! Determine number of degrees of freedom for order p basis !
  ! ndof=product{p+k,k=1..dim}/dim                           !
  ! ======================================================== !
  function legendre_ndof(p) result(ndof)
    implicit none
    integer, intent(in)  :: p
    integer, parameter   :: dim=3
    integer :: ndof,i
    ndof=1
    do i=1,dim
       ndof=ndof*(p+i)
    end do
    ndof=ndof/factorial_nm(dim,1)
  end function legendre_ndof
  
end module legendre


! ======================================================== !
! Generate scaled Legendre polynomials up to order p in 3D !
! ======================================================== !
subroutine legendre_generate(p,ndof,basis,phi,dphi,ddphi)
  use legendre
  implicit none
  
  integer, intent(in) :: p,ndof
  integer, dimension(ndof,3), intent(out) :: basis
  type(poly), dimension(0:p), intent(out) :: phi,dphi,ddphi
  integer :: i,j,k,count,pp
  type(poly) :: polytmp
  
  ! Generate 3D basis info
  basis=0
  count=0
  do pp=0,p
     do i=pp,0,-1
        do j=pp-i,0,-1
           k=pp-i-j
           count=count+1
           basis(count,1)=i
           basis(count,2)=j
           basis(count,3)=k
        end do
     end do
  end do
  
  ! Clean up basis function
  do i=0,p
     nullify(  phi(i)%a);nullify(  phi(i)%p)
     nullify( dphi(i)%a);nullify( dphi(i)%p)
     nullify(ddphi(i)%a);nullify(ddphi(i)%p)
  end do
  
  ! Form phi polynomials: phi_i(x)=i!/(2i)! d^i/dx^i{(x^2-1)^i}
  do i=0,p
     
     ! Allocate temporary polynomial for (x^2-1)^i
     polytmp%n=i+1
     allocate(polytmp%a(polytmp%n))
     allocate(polytmp%p(polytmp%n))
     
     ! Compute (x^2-1)^i using binomial theorem
     ! (x+y)^i = sum{C(i,k)*x^(i-k)*y^k,k=0..i}
     ! where C(i,k) is the binomial coefficient
     do k=0,i
        polytmp%a(k+1)=real(binomial(i,k)*(-1)**k,WP) 
        polytmp%p(k+1)=2*(i-k)
     end do
     
     ! Take ith derivative of (x^2-1)^i
     call derivative_n(polytmp,phi(i),i) 
     
     ! Multiply by i!/(2i)! to form phi
     do k=1,phi(i)%n
        phi(i)%a(k)=phi(i)%a(k)/real(factorial_nm(2*i,i+1),WP)
     end do
     
     ! Compute derivative of phi in dphi
     call derivative_n(phi(i),dphi(i),1)
     
     ! Compute derivative of dphi in ddphi
     call derivative_n(dphi(i),ddphi(i),1)
     
     ! Deallocate temporary polynomial
     polytmp%n=0
     deallocate(polytmp%a)
     deallocate(polytmp%p)
     
  end do
  
  return
end subroutine legendre_generate
