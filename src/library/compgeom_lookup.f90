module compgeom_lookup
  use precision
  implicit none
  
  ! Look-up tables for multiphase cutting routines
  ! -----------------------------------------------------------------------

  ! Number of new vertices on cut plane 
  integer, dimension(8) :: cutTri_nvert
  data cutTri_nvert(1:8) / 0, 2, 2, 2, 2, 2, 2, 0/
  ! Number of resulting tris on positive side 
  integer, dimension(8) :: cutTri_np  
  data cutTri_np(1:8)   / 0, 1, 1, 2, 1, 2, 2, 1/
   ! Number of resulting tris on negative side
  integer, dimension(8) :: cutTri_nn  
  data cutTri_nn(1:8)   / 1, 2, 2, 1, 2, 1, 1, 0/
  ! First point on intersection 
  integer, dimension(2,8) :: cutTri_v1
  data cutTri_v1(1:2,1) / -1,-1/
  data cutTri_v1(1:2,2) /  1, 1/
  data cutTri_v1(1:2,3) /  2, 2/
  data cutTri_v1(1:2,4) /  3, 3/
  data cutTri_v1(1:2,5) /  3, 3/ 
  data cutTri_v1(1:2,6) /  2, 2/
  data cutTri_v1(1:2,7) /  1, 1/
  data cutTri_v1(1:2,8) / -1,-1/
  ! Second point on intersection 
  integer, dimension(2,8) :: cutTri_v2
  data cutTri_v2(1:2,1) / -1,-1/
  data cutTri_v2(1:2,2) /  2, 3/
  data cutTri_v2(1:2,3) /  1, 3/
  data cutTri_v2(1:2,4) /  1, 2/
  data cutTri_v2(1:2,5) /  1, 2/ 
  data cutTri_v2(1:2,6) /  1, 3/
  data cutTri_v2(1:2,7) /  2, 3/
  data cutTri_v2(1:2,8) / -1,-1/

  ! Vertices in each tri
  integer, dimension(3,3,8) :: cutTri_v
  data cutTri_v(1:3,1:3,1) / 1, 2, 3, -1,-1,-1, -1,-1,-1/
  data cutTri_v(1:3,1:3,2) / 1, 4, 5,  2, 5, 4,  2, 3, 5/
  data cutTri_v(1:3,1:3,3) / 2, 5, 4,  1, 4, 5,  1, 5, 3/
  data cutTri_v(1:3,1:3,4) / 1, 2, 5,  1, 5, 4,  3, 4, 5/
  data cutTri_v(1:3,1:3,5) / 3, 4, 5,  1, 2, 5,  1, 5, 4/
  data cutTri_v(1:3,1:3,6) / 1, 4, 5,  1, 5, 3,  2, 5, 4/
  data cutTri_v(1:3,1:3,7) / 2, 3, 5,  2, 5, 4,  1, 4, 5/
  data cutTri_v(1:3,1:3,8) / 1, 2, 3, -1,-1,-1, -1,-1,-1/


  ! Subcell x,y,z location compared to cell center
  ! e.g. x_sub=xm(i)+subx(s)*dx(i) is the x-coord of the s subcell
  real(WP), dimension(8) :: subx,suby,subz 
  data subx(1:8) / -0.25_WP,  0.25_WP, -0.25_WP,  0.25_WP, -0.25_WP,  0.25_WP, -0.25_WP,  0.25_WP /
  data suby(1:8) / -0.25_WP, -0.25_WP,  0.25_WP,  0.25_WP, -0.25_WP, -0.25_WP,  0.25_WP,  0.25_WP /
  data subz(1:8) / -0.25_WP, -0.25_WP, -0.25_WP, -0.25_WP,  0.25_WP,  0.25_WP,  0.25_WP,  0.25_WP /

  ! Subcell neighbors
  integer, dimension(8) :: sc_sx,sc_sy,sc_sz
  integer, dimension(8) :: sc_im,sc_jm,sc_km
  integer, dimension(8) :: sc_ip,sc_jp,sc_kp
  data sc_sx / 2, 1, 4, 3, 6, 5, 8, 7 / 
  data sc_sy / 3, 4, 1, 2, 7, 8, 5, 6 /
  data sc_sz / 5, 6, 7, 8, 1, 2, 3, 4 /
  data sc_im /-1, 0,-1, 0,-1, 0,-1, 0 /
  data sc_jm /-1,-1, 0, 0,-1,-1, 0, 0 /
  data sc_km /-1,-1,-1,-1, 0, 0, 0, 0 /
  data sc_ip / 0, 1, 0, 1, 0, 1, 0, 1 /
  data sc_jp / 0, 0, 1, 1, 0, 0, 1, 1 /
  data sc_kp / 0, 0, 0, 0, 1, 1, 1, 1 /

  ! Cell vertices to edges
  integer, dimension(2,12) :: verts2edge
  data verts2edge(:, 1) / 1, 2 /
  data verts2edge(:, 2) / 1, 3 /
  data verts2edge(:, 3) / 1, 5 /
  data verts2edge(:, 4) / 2, 4 /
  data verts2edge(:, 5) / 2, 6 /
  data verts2edge(:, 6) / 3, 4 /
  data verts2edge(:, 7) / 3, 7 /
  data verts2edge(:, 8) / 4, 8 /
  data verts2edge(:, 9) / 5, 6 /
  data verts2edge(:,10) / 5, 7 /
  data verts2edge(:,11) / 6, 8 /
  data verts2edge(:,12) / 7, 8 /

  ! Cell face subfaces to cell subcell fluxes
  integer, dimension(4) :: subface2subcell_x ,subface2subcell_y ,subface2subcell_z
  integer, dimension(4) :: subface2subcell_xm,subface2subcell_ym,subface2subcell_zm
  data subface2subcell_x (1:4) / 1,3,5,7 /
  data subface2subcell_xm(1:4) / 2,4,6,8 /
  data subface2subcell_y (1:4) / 1,2,5,6 /
  data subface2subcell_ym(1:4) / 3,4,7,8 /
  data subface2subcell_z (1:4) / 1,2,3,4 /
  data subface2subcell_zm(1:4) / 5,6,7,8 /

  ! Cell subfaces to U cell fluxes
  integer, dimension(4) :: subface2ucell_xm,subface2ucell_y ,subface2ucell_z
  ! Fluxes at x, ym, and zm are within the U cell
  data subface2ucell_xm / 0, 0, 0, 0 /
  data subface2ucell_y  / 0, 1, 0, 1 /
  data subface2ucell_z  / 0, 1, 0, 1 /
  ! Cell subfaces to V cell fluxes
  integer, dimension(4) :: subface2vcell_x ,subface2vcell_ym,subface2vcell_z
  ! Fluxes at xm, y, and zm are within the V cell
  data subface2vcell_x  / 0, 1, 0, 1 /
  data subface2vcell_ym / 0, 0, 0, 0 /
  data subface2vcell_z  / 0, 0, 1, 1 /
  ! Cell subfaces to W cell fluxes
  integer, dimension(4) :: subface2wcell_x ,subface2wcell_y ,subface2wcell_zm
  ! Fluxes at xm, ym, and z are within the W cell
  data subface2wcell_x  / 0, 0, 1, 1 /
  data subface2wcell_y  / 0, 0, 1, 1 /
  data subface2wcell_zm / 0, 0, 0, 0 /

  ! Subcell fluxes to momentum and pressure cell fluxes
  integer, dimension(4) :: subcell2flux_Ux_s,subcell2flux_Vx_s,subcell2flux_Wx_s,subcell2flux_Px_s
  integer, dimension(4) :: subcell2flux_Uy_s,subcell2flux_Vy_s,subcell2flux_Wy_s,subcell2flux_Py_s
  integer, dimension(4) :: subcell2flux_Uz_s,subcell2flux_Vz_s,subcell2flux_Wz_s,subcell2flux_Pz_s
  integer, dimension(4) :: subcell2flux_Ux_i,subcell2flux_Vx_j,subcell2flux_Wx_k
  integer, dimension(4) :: subcell2flux_Uy_i,subcell2flux_Vy_j,subcell2flux_Wy_k
  integer, dimension(4) :: subcell2flux_Uz_i,subcell2flux_Vz_j,subcell2flux_Wz_k
  data subcell2flux_Ux_s / 2,4,6,8 /; data subcell2flux_Ux_i / 0, 0, 0, 0 /
  data subcell2flux_Uy_s / 1,2,5,6 /; data subcell2flux_Uy_i / 0,-1, 0,-1 /
  data subcell2flux_Uz_s / 1,2,3,4 /; data subcell2flux_Uz_i / 0,-1, 0,-1 /
  data subcell2flux_Vx_s / 1,3,5,7 /; data subcell2flux_Vx_j / 0,-1, 0,-1 /
  data subcell2flux_Vy_s / 3,4,7,8 /; data subcell2flux_Vy_j / 0, 0, 0, 0 /
  data subcell2flux_Vz_s / 1,2,3,4 /; data subcell2flux_Vz_j / 0, 0,-1,-1 /
  data subcell2flux_Wx_s / 1,3,5,7 /; data subcell2flux_Wx_k / 0, 0,-1,-1 /
  data subcell2flux_Wy_s / 1,2,5,6 /; data subcell2flux_Wy_k / 0, 0,-1,-1 /
  data subcell2flux_Wz_s / 5,6,7,8 /; data subcell2flux_Wz_k / 0, 0, 0, 0 / 
  data subcell2flux_Px_s / 1,3,5,7 /;
  data subcell2flux_Py_s / 1,2,5,6 /;
  data subcell2flux_Pz_s / 1,2,3,4 /;

  ! Subcell to momentum cells (index shift associated with subcells s=1-8)
  integer, dimension(8) :: subcell2Ucell_i,subcell2Vcell_j,subcell2Wcell_k
  data subcell2Ucell_i / 0,-1, 0,-1, 0,-1, 0,-1 /
  data subcell2Vcell_j / 0, 0,-1,-1, 0, 0,-1,-1 /
  data subcell2Wcell_k / 0, 0, 0, 0,-1,-1,-1,-1 /
 
  ! Points on flux volume to 5 tets
  integer, dimension(4,7,4) :: pts2tets
  data pts2tets(1,1,1:4) / 1,  4,  5, 13 /  ! Face 1
  data pts2tets(1,2,1:4) / 1, 13, 11, 10 /
  data pts2tets(1,3,1:4) / 1,  2, 11,  5 /
  data pts2tets(1,4,1:4) / 1,  5, 11, 13 /
  data pts2tets(1,5,1:4) / 5, 11, 13, 14 /
  data pts2tets(1,6,1:4) / 1,  2,  5, 19 /  ! correction 1
  data pts2tets(1,7,1:4) / 1,  5,  4, 19 /  ! correction 2
  
  data pts2tets(2,1,1:4) / 2,  5,  3, 11 /  ! Face 2
  data pts2tets(2,2,1:4) / 3,  5,  6, 15 /
  data pts2tets(2,3,1:4) / 3,  5, 15, 11 /
  data pts2tets(2,4,1:4) / 3, 12, 11, 15 /
  data pts2tets(2,5,1:4) / 5, 15, 11, 14 / 
  data pts2tets(2,6,1:4) / 2,  3,  5, 20 /  ! correction 1
  data pts2tets(2,7,1:4) / 3,  6,  5, 20 /  ! correction 2

  data pts2tets(3,1,1:4) / 4,  7,  5, 13 /  ! Face 3
  data pts2tets(3,2,1:4) / 7, 17, 13, 16 /
  data pts2tets(3,3,1:4) / 5,  7,  8, 17 /
  data pts2tets(3,4,1:4) / 5,  7, 17, 13 /
  data pts2tets(3,5,1:4) / 5, 13, 17, 14 / 
  data pts2tets(3,6,1:4) / 4,  5,  7, 21 /  ! correction 1
  data pts2tets(3,7,1:4) / 5,  8,  7, 21 /  ! correction 2

  data pts2tets(4,1,1:4) / 5,  6, 15,  9 /  ! Face 4
  data pts2tets(4,2,1:4) / 8,  9,  5, 17 /
  data pts2tets(4,3,1:4) / 9, 18, 15, 17 /
  data pts2tets(4,4,1:4) / 5,  9, 15, 17 /
  data pts2tets(4,5,1:4) / 5, 17, 15, 14 /
  data pts2tets(4,6,1:4) / 5,  6,  9, 22 /  ! correction 1
  data pts2tets(4,7,1:4) / 5,  9,  8, 22 /  ! correction 2

  ! Points on flux volume faces where correction tets are added
  integer, dimension(4,4) :: correction_pts
  data correction_pts(:,1) / 1, 2, 4, 5 /
  data correction_pts(:,2) / 5, 2, 6, 3 /
  data correction_pts(:,3) / 7, 4, 8, 5 /
  data correction_pts(:,4) / 5, 6, 8, 9 /

  ! Sign of each direction
  real(WP), dimension(3) :: dirSign
  data dirSign(:) / 1.0_WP, -1.0_WP, 1.0_WP/
  
  ! Vertices on cell to 5 tets
  integer, dimension(4,5) :: verts2tets
  data verts2tets(:,1) / 1, 4, 3, 7 /
  data verts2tets(:,2) / 1, 2, 4, 6 /
  data verts2tets(:,3) / 1, 5, 6, 7 /
  data verts2tets(:,4) / 1, 6, 4, 7 /
  data verts2tets(:,5) / 4, 7, 6, 8 /

  ! Vertices on cell face to 2 tris
  integer, dimension(3,2) :: verts2tris
  data verts2tris(:,1) / 1, 2, 4 /
  data verts2tris(:,2) / 1, 4, 3 /

  ! Number of new vertices on cut plane
  integer, dimension(    16) :: cut_nvert
  data cut_nvert(1:16) / 0, 3, 3, 4, 3, 4, 4, 3, 3, 4, 4, 3, 4, 3, 3, 0/
  ! Number of resulting tets
  integer, dimension(    16) :: cut_ntets
  data cut_ntets(1:16) / 1, 4, 4, 6, 4, 6, 6, 4, 4, 6, 6, 4, 6, 4, 4, 1/

  ! First point on intersection 
  integer, dimension(4,  16) :: cut_v1
  data cut_v1(1:4, 1) /-1,-1,-1,-1/
  data cut_v1(1:4, 2) / 1, 1, 1,-1/
  data cut_v1(1:4, 3) / 2, 2, 2,-1/
  data cut_v1(1:4, 4) / 1, 2, 1, 2/
  data cut_v1(1:4, 5) / 3, 3, 3,-1/
  data cut_v1(1:4, 6) / 1, 3, 1, 3/
  data cut_v1(1:4, 7) / 2, 3, 2, 3/
  data cut_v1(1:4, 8) / 4, 4, 4,-1/
  data cut_v1(1:4, 9) / 4, 4, 4,-1/
  data cut_v1(1:4,10) / 1, 4, 1, 4/
  data cut_v1(1:4,11) / 2, 4, 2, 4/
  data cut_v1(1:4,12) / 3, 3, 3,-1/
  data cut_v1(1:4,13) / 3, 4, 3, 4/
  data cut_v1(1:4,14) / 2, 2, 2,-1/
  data cut_v1(1:4,15) / 1, 1, 1,-1/
  data cut_v1(1:4,16) /-1,-1,-1,-1/

  ! Second point on intersection 
  integer, dimension(4,  16) :: cut_v2
  data cut_v2(1:4, 1) /-1,-1,-1,-1/
  data cut_v2(1:4, 2) / 2, 3, 4,-1/
  data cut_v2(1:4, 3) / 3, 4, 1,-1/
  data cut_v2(1:4, 4) / 4, 4, 3, 3/
  data cut_v2(1:4, 5) / 4, 1, 2,-1/
  data cut_v2(1:4, 6) / 4, 4, 2, 2/
  data cut_v2(1:4, 7) / 4, 4, 1, 1/
  data cut_v2(1:4, 8) / 1, 2, 3,-1/
  data cut_v2(1:4, 9) / 1, 2, 3,-1/
  data cut_v2(1:4,10) / 3, 3, 2, 2/
  data cut_v2(1:4,11) / 3, 3, 1, 1/
  data cut_v2(1:4,12) / 4, 1, 2,-1/
  data cut_v2(1:4,13) / 2, 2, 1, 1/
  data cut_v2(1:4,14) / 3, 4, 1,-1/
  data cut_v2(1:4,15) / 2, 3, 4,-1/
  data cut_v2(1:4,16) /-1,-1,-1,-1/

  ! Side of cut plane (used to update i,j,k)
  integer, dimension(6,  16) :: cut_side
  data cut_side(1:6, 1) / 1,-1,-1,-1,-1,-1/
  data cut_side(1:6, 2) / 2, 1, 1, 1,-1,-1/
  data cut_side(1:6, 3) / 2, 1, 1, 1,-1,-1/
  data cut_side(1:6, 4) / 2, 2, 2, 1, 1, 1/
  data cut_side(1:6, 5) / 2, 1, 1, 1,-1,-1/
  data cut_side(1:6, 6) / 2, 2, 2, 1, 1, 1/
  data cut_side(1:6, 7) / 2, 2, 2, 1, 1, 1/
  data cut_side(1:6, 8) / 2, 2, 2, 1,-1,-1/
  data cut_side(1:6, 9) / 2, 1, 1, 1,-1,-1/
  data cut_side(1:6,10) / 2, 2, 2, 1, 1, 1/
  data cut_side(1:6,11) / 2, 2, 2, 1, 1, 1/
  data cut_side(1:6,12) / 2, 2, 2, 1,-1,-1/
  data cut_side(1:6,13) / 2, 2, 2, 1, 1, 1/
  data cut_side(1:6,14) / 2, 2, 2, 1,-1,-1/
  data cut_side(1:6,15) / 2, 2, 2, 1,-1,-1/
  data cut_side(1:6,16) / 2,-1,-1,-1,-1,-1/

  ! Vertices in each tet
  integer, dimension(4,6,16) :: cut_vtet
  data cut_vtet(1:4,1:6, 1) / 1, 2, 3, 4, -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1/
  data cut_vtet(1:4,1:6, 2) / 5, 7, 6, 1,  6, 2, 3, 4,  4, 2, 5, 6,  5, 6, 7, 4, -1,-1,-1,-1, -1,-1,-1,-1/
  data cut_vtet(1:4,1:6, 3) / 7, 5, 6, 2,  1, 3, 4, 6,  1, 5, 3, 6,  5, 7, 6, 1, -1,-1,-1,-1, -1,-1,-1,-1/
  data cut_vtet(1:4,1:6, 4) / 5, 8, 6, 2,  5, 7, 8, 1,  5, 1, 8, 2,  5, 6, 8, 4,  5, 8, 7, 3,  5, 8, 3, 4/
  data cut_vtet(1:4,1:6, 5) / 6, 5, 7, 3,  2, 1, 4, 6,  6, 5, 4, 2,  6, 7, 5, 2, -1,-1,-1,-1, -1,-1,-1,-1/
  data cut_vtet(1:4,1:6, 6) / 5, 6, 8, 3,  5, 8, 7, 1,  5, 8, 1, 3,  5, 8, 6, 4,  5, 7, 8, 2,  5, 8, 4, 2/
  data cut_vtet(1:4,1:6, 7) / 8, 6, 5, 3,  5, 7, 8, 2,  8, 5, 2, 3,  8, 5, 6, 4,  5, 8, 7, 1,  5, 8, 1, 4/
  data cut_vtet(1:4,1:6, 8) / 1, 2, 3, 7,  1, 2, 7, 6,  5, 7, 6, 1,  5, 6, 7, 4, -1,-1,-1,-1, -1,-1,-1,-1/
  data cut_vtet(1:4,1:6, 9) / 5, 6, 7, 4,  1, 2, 3, 6,  5, 1, 3, 6,  5, 7, 6, 3, -1,-1,-1,-1, -1,-1,-1,-1/
  data cut_vtet(1:4,1:6,10) / 5, 8, 6, 4,  5, 7, 8, 1,  5, 8, 4, 1,  5, 6, 8, 3,  5, 8, 7, 2,  5, 8, 2, 3/
  data cut_vtet(1:4,1:6,11) / 8, 5, 6, 4,  5, 8, 7, 2,  8, 2, 5, 4,  8, 6, 5, 3,  5, 7, 8, 1,  5, 8, 3, 1/
  data cut_vtet(1:4,1:6,12) / 1, 4, 2, 7,  4, 1, 6, 7,  6, 7, 5, 4,  6, 5, 7, 3, -1,-1,-1,-1, -1,-1,-1,-1/
  data cut_vtet(1:4,1:6,13) / 8, 6, 5, 4,  5, 7, 8, 3,  8, 4, 5, 3,  8, 5, 6, 2,  5, 8, 7, 1,  5, 8, 1, 2/
  data cut_vtet(1:4,1:6,14) / 3, 4, 1, 7,  7, 6, 3, 4,  7, 6, 5, 3,  7, 5, 6, 2, -1,-1,-1,-1, -1,-1,-1,-1/
  data cut_vtet(1:4,1:6,15) / 7, 4, 2, 3,  2, 3, 6, 7,  5, 6, 7, 2,  5, 7, 6, 1, -1,-1,-1,-1, -1,-1,-1,-1/
  data cut_vtet(1:4,1:6,16) / 1, 2, 3, 4, -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1/

  ! Number of triangles on cut plane
  integer, dimension(    16) :: cut_ntris
  data cut_ntris(1:16) / 0, 1, 1, 2, 1, 2, 2, 1, 1, 2, 2, 1, 2, 1, 1, 0/

  ! Vertices in each tri on cut plane
  integer, dimension(3,2,16) :: cut_vtri
  data cut_vtri(1:3,1:2, 1) /-1,-1,-1, -1,-1,-1/
  data cut_vtri(1:3,1:2, 2) / 5, 7, 6, -1,-1,-1/
  data cut_vtri(1:3,1:2, 3) / 5, 6, 7, -1,-1,-1/
  data cut_vtri(1:3,1:2, 4) / 5, 8, 6,  5, 7, 8/
  data cut_vtri(1:3,1:2, 5) / 5, 7, 6, -1,-1,-1/
  data cut_vtri(1:3,1:2, 6) / 5, 6, 8,  5, 8, 7/
  data cut_vtri(1:3,1:2, 7) / 5, 8, 6,  5, 7, 8/
  data cut_vtri(1:3,1:2, 8) / 5, 7, 6, -1,-1,-1/
  data cut_vtri(1:3,1:2, 9) / 5, 6, 7, -1,-1,-1/
  data cut_vtri(1:3,1:2,10) / 5, 8, 6,  5, 7, 8/
  data cut_vtri(1:3,1:2,11) / 5, 6, 8,  5, 8, 7/
  data cut_vtri(1:3,1:2,12) / 5, 6, 7, -1,-1,-1/
  data cut_vtri(1:3,1:2,13) / 5, 8, 6,  5, 7, 8/
  data cut_vtri(1:3,1:2,14) / 5, 7, 6, -1,-1,-1/
  data cut_vtri(1:3,1:2,15) / 5, 6, 7, -1,-1,-1/
  data cut_vtri(1:3,1:2,16) /-1,-1,-1, -1,-1,-1/
  
  ! Index of first positive tet = # tets - # negative tets + 1
  integer, dimension(    16) :: cut_nntet
  data cut_nntet(1:16) / 1, 2, 2, 4, 2, 4, 4, 4, 2, 4, 4, 4, 4, 4, 4, 2/
  
  ! Relation of subcell to velocity cells
  integer, dimension(8) :: sub2u,sub2v,sub2w
  data sub2u / 0, 1, 0, 1, 0, 1, 0, 1/
  data sub2v / 0, 0, 1, 1, 0, 0, 1, 1/
  data sub2w / 0, 0, 0, 0, 1, 1, 1, 1/

  ! Relation of velocity cells to subcells
  integer, dimension(8) :: u2sub_i, v2sub_j, w2sub_k
  data u2sub_i / 0,-1, 0,-1, 0,-1,0,-1/
  data v2sub_j / 0, 0,-1,-1, 0, 0,-1,-1/
  data w2sub_k / 0, 0, 0, 0,-1,-1,-1,-1/

  ! Matrix to compute divg-free sub-cell velocities
  real(WP), dimension(12,36) :: divgfree_M
data  divgfree_M(:,1) / 0.2916666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.0416666666666667_WP, 0.2916666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.0416666666666667_WP, 0.2916666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.0416666666666667_WP / 
data  divgfree_M(:,2) / 0.4166666666666667_WP, -0.1666666666666667_WP, -0.1666666666666667_WP, -0.0833333333333333_WP, -0.2083333333333333_WP, 0.2083333333333333_WP, -0.0416666666666667_WP, 0.0416666666666667_WP, -0.2083333333333333_WP, 0.2083333333333333_WP, -0.0416666666666667_WP, 0.0416666666666667_WP / 
data  divgfree_M(:,3) / 0.0833333333333333_WP, 0.2916666666666667_WP, 0.0416666666666667_WP, 0.0833333333333333_WP, -0.2916666666666667_WP, -0.0833333333333333_WP, -0.0833333333333333_WP, -0.0416666666666667_WP, 0.0833333333333333_WP, 0.0416666666666667_WP, 0.2916666666666667_WP, 0.0833333333333333_WP / 
data  divgfree_M(:,4) / -0.1666666666666667_WP, 0.4166666666666667_WP, -0.0833333333333333_WP, -0.1666666666666667_WP, 0.2083333333333333_WP, -0.2083333333333333_WP, 0.0416666666666667_WP, -0.0416666666666667_WP, -0.0416666666666667_WP, 0.0416666666666667_WP, -0.2083333333333333_WP, 0.2083333333333333_WP / 
data  divgfree_M(:,5) / 0.0833333333333333_WP, 0.0416666666666667_WP, 0.2916666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.0416666666666667_WP, 0.2916666666666667_WP, 0.0833333333333333_WP, -0.2916666666666667_WP, -0.0833333333333333_WP, -0.0833333333333333_WP, -0.0416666666666667_WP / 
data  divgfree_M(:,6) / -0.1666666666666667_WP, -0.0833333333333333_WP, 0.4166666666666667_WP, -0.1666666666666667_WP, -0.0416666666666667_WP, 0.0416666666666667_WP, -0.2083333333333333_WP, 0.2083333333333333_WP, 0.2083333333333333_WP, -0.2083333333333333_WP, 0.0416666666666667_WP, -0.0416666666666667_WP / 
data  divgfree_M(:,7) / 0.0416666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.2916666666666667_WP, -0.0833333333333333_WP, -0.0416666666666667_WP, -0.2916666666666667_WP, -0.0833333333333333_WP, -0.0833333333333333_WP, -0.0416666666666667_WP, -0.2916666666666667_WP, -0.0833333333333333_WP / 
data  divgfree_M(:,8) / -0.0833333333333333_WP, -0.1666666666666667_WP, -0.1666666666666667_WP, 0.4166666666666667_WP, 0.0416666666666667_WP, -0.0416666666666667_WP, 0.2083333333333333_WP, -0.2083333333333333_WP, 0.0416666666666667_WP, -0.0416666666666667_WP, 0.2083333333333333_WP, -0.2083333333333333_WP / 
data  divgfree_M(:,9) / 0.2916666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.0416666666666667_WP, -0.0833333333333333_WP, -0.2916666666666667_WP, -0.0416666666666667_WP, -0.0833333333333333_WP, -0.0833333333333333_WP, -0.2916666666666667_WP, -0.0416666666666667_WP, -0.0833333333333333_WP / 
data  divgfree_M(:,10) / 0.0833333333333333_WP, 0.2916666666666667_WP, 0.0416666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.2916666666666667_WP, 0.0416666666666667_WP, 0.0833333333333333_WP, -0.0416666666666667_WP, -0.0833333333333333_WP, -0.0833333333333333_WP, -0.2916666666666667_WP / 
data  divgfree_M(:,11) / 0.0833333333333333_WP, 0.0416666666666667_WP, 0.2916666666666667_WP, 0.0833333333333333_WP, -0.0416666666666667_WP, -0.0833333333333333_WP, -0.0833333333333333_WP, -0.2916666666666667_WP, 0.0833333333333333_WP, 0.2916666666666667_WP, 0.0416666666666667_WP, 0.0833333333333333_WP / 
data  divgfree_M(:,12) / 0.0416666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.2916666666666667_WP, 0.0416666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.2916666666666667_WP, 0.0416666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.2916666666666667_WP / 
data  divgfree_M(:,13) / 0.2916666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.0416666666666667_WP, 0.2916666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.0416666666666667_WP, 0.2916666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.0416666666666667_WP / 
data  divgfree_M(:,14) / -0.2916666666666667_WP, -0.0833333333333333_WP, -0.0833333333333333_WP, -0.0416666666666667_WP, 0.0833333333333333_WP, 0.2916666666666667_WP, 0.0416666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.2916666666666667_WP, 0.0416666666666667_WP, 0.0833333333333333_WP / 
data  divgfree_M(:,15) / -0.2083333333333333_WP, 0.2083333333333333_WP, -0.0416666666666667_WP, 0.0416666666666667_WP, 0.4166666666666667_WP, -0.1666666666666667_WP, -0.1666666666666667_WP, -0.0833333333333333_WP, -0.2083333333333333_WP, -0.0416666666666667_WP, 0.2083333333333333_WP, 0.0416666666666667_WP / 
data  divgfree_M(:,16) / 0.2083333333333333_WP, -0.2083333333333333_WP, 0.0416666666666667_WP, -0.0416666666666667_WP, -0.1666666666666667_WP, 0.4166666666666667_WP, -0.0833333333333333_WP, -0.1666666666666667_WP, -0.0416666666666667_WP, -0.2083333333333333_WP, 0.0416666666666667_WP, 0.2083333333333333_WP / 
data  divgfree_M(:,17) / 0.0833333333333333_WP, 0.0416666666666667_WP, 0.2916666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.0416666666666667_WP, 0.2916666666666667_WP, 0.0833333333333333_WP, -0.2916666666666667_WP, -0.0833333333333333_WP, -0.0833333333333333_WP, -0.0416666666666667_WP / 
data  divgfree_M(:,18) / -0.0833333333333333_WP, -0.0416666666666667_WP, -0.2916666666666667_WP, -0.0833333333333333_WP, 0.0416666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.2916666666666667_WP, -0.0833333333333333_WP, -0.2916666666666667_WP, -0.0416666666666667_WP, -0.0833333333333333_WP / 
data  divgfree_M(:,19) / -0.0416666666666667_WP, 0.0416666666666667_WP, -0.2083333333333333_WP, 0.2083333333333333_WP, -0.1666666666666667_WP, -0.0833333333333333_WP, 0.4166666666666667_WP, -0.1666666666666667_WP, 0.2083333333333333_WP, 0.0416666666666667_WP, -0.2083333333333333_WP, -0.0416666666666667_WP / 
data  divgfree_M(:,20) / 0.0416666666666667_WP, -0.0416666666666667_WP, 0.2083333333333333_WP, -0.2083333333333333_WP, -0.0833333333333333_WP, -0.1666666666666667_WP, -0.1666666666666667_WP, 0.4166666666666667_WP, 0.0416666666666667_WP, 0.2083333333333333_WP, -0.0416666666666667_WP, -0.2083333333333333_WP / 
data  divgfree_M(:,21) / -0.0833333333333333_WP, -0.2916666666666667_WP, -0.0416666666666667_WP, -0.0833333333333333_WP, 0.2916666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.0416666666666667_WP, -0.0833333333333333_WP, -0.0416666666666667_WP, -0.2916666666666667_WP, -0.0833333333333333_WP / 
data  divgfree_M(:,22) / 0.0833333333333333_WP, 0.2916666666666667_WP, 0.0416666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.2916666666666667_WP, 0.0416666666666667_WP, 0.0833333333333333_WP, -0.0416666666666667_WP, -0.0833333333333333_WP, -0.0833333333333333_WP, -0.2916666666666667_WP / 
data  divgfree_M(:,23) / -0.0416666666666667_WP, -0.0833333333333333_WP, -0.0833333333333333_WP, -0.2916666666666667_WP, 0.0833333333333333_WP, 0.0416666666666667_WP, 0.2916666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.0416666666666667_WP, 0.2916666666666667_WP, 0.0833333333333333_WP / 
data  divgfree_M(:,24) / 0.0416666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.2916666666666667_WP, 0.0416666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.2916666666666667_WP, 0.0416666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.2916666666666667_WP / 
data  divgfree_M(:,25) / 0.2916666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.0416666666666667_WP, 0.2916666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.0416666666666667_WP, 0.2916666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.0416666666666667_WP / 
data  divgfree_M(:,26) / -0.2916666666666667_WP, -0.0833333333333333_WP, -0.0833333333333333_WP, -0.0416666666666667_WP, 0.0833333333333333_WP, 0.2916666666666667_WP, 0.0416666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.2916666666666667_WP, 0.0416666666666667_WP, 0.0833333333333333_WP / 
data  divgfree_M(:,27) / 0.0833333333333333_WP, 0.2916666666666667_WP, 0.0416666666666667_WP, 0.0833333333333333_WP, -0.2916666666666667_WP, -0.0833333333333333_WP, -0.0833333333333333_WP, -0.0416666666666667_WP, 0.0833333333333333_WP, 0.0416666666666667_WP, 0.2916666666666667_WP, 0.0833333333333333_WP / 
data  divgfree_M(:,28) / -0.0833333333333333_WP, -0.2916666666666667_WP, -0.0416666666666667_WP, -0.0833333333333333_WP, -0.0833333333333333_WP, -0.2916666666666667_WP, -0.0416666666666667_WP, -0.0833333333333333_WP, 0.0416666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.2916666666666667_WP / 
data  divgfree_M(:,29) / -0.2083333333333333_WP, -0.0416666666666667_WP, 0.2083333333333333_WP, 0.0416666666666667_WP, -0.2083333333333333_WP, -0.0416666666666667_WP, 0.2083333333333333_WP, 0.0416666666666667_WP, 0.4166666666666667_WP, -0.1666666666666667_WP, -0.1666666666666667_WP, -0.0833333333333333_WP / 
data  divgfree_M(:,30) / 0.2083333333333333_WP, 0.0416666666666667_WP, -0.2083333333333333_WP, -0.0416666666666667_WP, -0.0416666666666667_WP, -0.2083333333333333_WP, 0.0416666666666667_WP, 0.2083333333333333_WP, -0.1666666666666667_WP, 0.4166666666666667_WP, -0.0833333333333333_WP, -0.1666666666666667_WP / 
data  divgfree_M(:,31) / -0.0416666666666667_WP, -0.2083333333333333_WP, 0.0416666666666667_WP, 0.2083333333333333_WP, 0.2083333333333333_WP, 0.0416666666666667_WP, -0.2083333333333333_WP, -0.0416666666666667_WP, -0.1666666666666667_WP, -0.0833333333333333_WP, 0.4166666666666667_WP, -0.1666666666666667_WP / 
data  divgfree_M(:,32) / 0.0416666666666667_WP, 0.2083333333333333_WP, -0.0416666666666667_WP, -0.2083333333333333_WP, 0.0416666666666667_WP, 0.2083333333333333_WP, -0.0416666666666667_WP, -0.2083333333333333_WP, -0.0833333333333333_WP, -0.1666666666666667_WP, -0.1666666666666667_WP, 0.4166666666666667_WP / 
data  divgfree_M(:,33) / -0.0833333333333333_WP, -0.0416666666666667_WP, -0.2916666666666667_WP, -0.0833333333333333_WP, -0.0833333333333333_WP, -0.0416666666666667_WP, -0.2916666666666667_WP, -0.0833333333333333_WP, 0.2916666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.0416666666666667_WP / 
data  divgfree_M(:,34) / 0.0833333333333333_WP, 0.0416666666666667_WP, 0.2916666666666667_WP, 0.0833333333333333_WP, -0.0416666666666667_WP, -0.0833333333333333_WP, -0.0833333333333333_WP, -0.2916666666666667_WP, 0.0833333333333333_WP, 0.2916666666666667_WP, 0.0416666666666667_WP, 0.0833333333333333_WP / 
data  divgfree_M(:,35) / -0.0416666666666667_WP, -0.0833333333333333_WP, -0.0833333333333333_WP, -0.2916666666666667_WP, 0.0833333333333333_WP, 0.0416666666666667_WP, 0.2916666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.0416666666666667_WP, 0.2916666666666667_WP, 0.0833333333333333_WP / 
data  divgfree_M(:,36) / 0.0416666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.2916666666666667_WP, 0.0416666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.2916666666666667_WP, 0.0416666666666667_WP, 0.0833333333333333_WP, 0.0833333333333333_WP, 0.2916666666666667_WP / 


end module compgeom_lookup
