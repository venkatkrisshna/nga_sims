#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "mpi.h"

#ifdef WIN32
void mpi_init_(int *ierr)
{
  int argc;
  char **argv;
  argc = 0;
  argv = NULL;
  *ierr = MPI_Init(&argc,&argv);
}

void mpi_comm_rank_( MPI_Comm *comm, int *rank, int *ierr )
{
  *ierr = MPI_Comm_rank( *comm, rank );
}

void mpi_comm_size_( MPI_Comm *comm, int *size, int *ierr )
{
  *ierr = MPI_Comm_size( *comm, size );
}

void mpi_cart_sub_( MPI_Comm *comm, int *remain_dims, MPI_Comm *comm_new, 
		    int *ierr )
{
  *ierr = MPI_Cart_sub( *comm, remain_dims, comm_new );
}

void mpi_cart_coords_( MPI_Comm *comm, int *rank, int *maxdims, int *coords, 
		       int *ierr )
{
  *ierr = MPI_Cart_coords( *comm, *rank, *maxdims, coords );
}

void mpi_cart_create_( MPI_Comm *comm_old, int *ndims, int *dims, int *periods, 
		       int *reorder, MPI_Comm *comm_cart, int* ierr )
{
  *ierr = MPI_Cart_create( *comm_old, *ndims, dims, periods, *reorder, comm_cart );
}

void mpi_cart_rank_( MPI_Comm *comm, int *coords, int *rank, int *ierr )
{
  *ierr = MPI_Cart_rank( *comm, coords, rank );
}

void mpi_cart_shift_( MPI_Comm *comm, int *direction, int *displ, 
		      int *source, int *dest, int *ierr )
{
  *ierr = MPI_Cart_shift( *comm, *direction, *displ, source, dest );
}

void mpi_type_size_( MPI_Datatype *datatype, int *size, int *ierr )
{
  *ierr = MPI_Type_size( *datatype, size );
}

void mpi_type_commit_( MPI_Datatype *datatype, int *ierr )
{
  *ierr = MPI_Type_commit( datatype );
}

void mpi_type_create_subarray_( int *ndims, int *array_of_sizes, 
				int *array_of_subsizes, int *array_of_starts, 
				int *order, MPI_Datatype *oldtype, 
				MPI_Datatype *newtype, int *ierr)
{
  *ierr = MPI_Type_create_subarray( *ndims, array_of_sizes, array_of_subsizes, 
				    array_of_starts, *order, *oldtype, newtype);
}





void mpi_alltoall_( void *sendbuf, int *sendcount, MPI_Datatype *sendtype, 
		    void *recvbuf, int *recvcnt, MPI_Datatype *recvtype, 
		    MPI_Comm *comm, int *ierr )
{ 
  *ierr = MPI_Alltoall( sendbuf, *sendcount, *sendtype, recvbuf, *recvcnt, 
			*recvtype, *comm );
}

void mpi_bcast_( void *buffer, int *count, MPI_Datatype *datatype, int *root, 
		 MPI_Comm *comm, int *ierr )
{
  *ierr = MPI_Bcast( buffer, *count, *datatype, *root, *comm );
}

double mpi_wtime_()
{
  return MPI_Wtime();
}

void mpi_finalize_(int *ierr)
{
  *ierr = MPI_Finalize();
}

void mpi_info_set_(MPI_Info *info, char *key, char *value, int *ierr,
		   const int size1, const int size2)
{
  char Key[64];
  strncpy(Key,key,(size_t) size1);
  Key[size1] = '\0';
  
  char Value[64/*size_file*/];
  strncpy(Value,value,(size_t) size2);
  Value[size2] = '\0';
  
  *ierr = MPI_Info_set(*info, Key, Value);
}

void mpi_info_create_(MPI_Info *info, int *ierr)
{
  *ierr = MPI_Info_create(info);
}

void mpi_allgather_( void *sendbuf, int *sendcount, MPI_Datatype *sendtype,
		     void *recvbuf, int *recvcount, MPI_Datatype *recvtype, 
		     MPI_Comm *comm , int *ierr)
{
  *ierr = MPI_Allgather( sendbuf, *sendcount, *sendtype, recvbuf, 
			 *recvcount, *recvtype, *comm );
}

void mpi_allreduce_( void *sendbuf, void *recvbuf, int *count, 
		     MPI_Datatype *datatype, MPI_Op *op, MPI_Comm *comm , int *ierr)
{
  *ierr = MPI_Allreduce( sendbuf, recvbuf, *count, *datatype, *op, *comm );
}

void mpi_file_close_(MPI_File *fh, int *ierr)
{
  *ierr = MPI_File_close( fh);
}

void mpi_file_read_all_(MPI_File *fh, void *buf, int *count, 
			MPI_Datatype *datatype, MPI_Status *status, 
			int *ierr)
{
  *ierr = MPI_File_read_all( *fh, buf, *count, *datatype, status);
}

void mpi_file_set_view_(MPI_File *fh, MPI_Offset *disp, MPI_Datatype *etype,
			MPI_Datatype *filetype, char *data, MPI_Info *info, 
			int *ierr, const int size1)
{
  char datarep[64];
  strncpy(datarep,data,(size_t) size1);
  datarep[size1] = '\0';

  *ierr = MPI_File_set_view(*fh, *disp, *etype, *filetype, datarep, *info);
}

void mpi_file_open_(MPI_Comm *comm, char *file, int *amode, 
		    MPI_Info *info, MPI_File *fh, int *ierr, const int size1)
{
  char filename[64];
  strncpy(filename,file,(size_t) size1);
  filename[size1] = '\0';

  *ierr = MPI_File_open( *comm, filename, *amode, *info, fh);
}

void mpi_send_( void *buf, int *count, MPI_Datatype *datatype, int *dest, 
		int *tag, MPI_Comm *comm , int *ierr)
{
  *ierr = MPI_Send( buf, *count, *datatype, *dest, *tag, *comm );
}

void mpi_sendrecv_( void *sendbuf, int *sendcount, MPI_Datatype *sendtype, 
		    int *dest, int *sendtag, 
		    void *recvbuf, int *recvcount, MPI_Datatype *recvtype, 
		    int *source, int *recvtag, MPI_Comm *comm, 
		    MPI_Status *status , int *ierr)
{
  *ierr = MPI_Sendrecv( sendbuf, *sendcount, *sendtype, *dest, *sendtag, 
			recvbuf, *recvcount, *recvtype, *source, *recvtag, 
			*comm, status );
}

void mpi_waitall_( int *count, MPI_Request array_of_requests[], 
		   MPI_Status array_of_statuses[] , int *ierr)
{
  *ierr = MPI_Waitall( *count, array_of_requests, array_of_statuses );
}

void mpi_irecv_( void *buf, int *count, MPI_Datatype *datatype, int *source, 
		 int *tag, MPI_Comm *comm, MPI_Request *request , int *ierr)
{
  *ierr = MPI_Irecv( buf, *count, *datatype, *source, *tag, *comm, request );
}

void mpi_isend_( void *buf, int *count, MPI_Datatype *datatype, int *dest, 
		 int *tag, MPI_Comm *comm, MPI_Request *request , int *ierr)
{
  *ierr = MPI_Isend( buf, *count, *datatype, *dest, *tag, *comm, request );
}

void mpi_comm_split_( MPI_Comm *comm, int *color, int *key, 
		      MPI_Comm *comm_out , int *ierr)
{
  *ierr = MPI_Comm_split( *comm, *color, *key, comm_out );
}

void mpi_bsend_( void *buf, int *count, MPI_Datatype *datatype, int *dest, 
		 int *tag, MPI_Comm *comm , int *ierr)
{
  *ierr = MPI_Bsend( buf, *count, *datatype, *dest, *tag, *comm );
}

void mpi_abort_( MPI_Comm *comm, int *errorcode , int *ierr)
{
  *ierr = MPI_Abort( *comm, *errorcode );
}

void mpi_recv_( void *buf, int *count, MPI_Datatype *datatype, int *source, 
		int *tag, MPI_Comm *comm, MPI_Status *status , int *ierr)
{
  *ierr = MPI_Recv( buf, *count, *datatype, *source, *tag, *comm, status );
}

void mpi_iprobe_( int *source, int *tag, MPI_Comm *comm, int *flag, 
		  MPI_Status *status , int *ierr)
{
  *ierr = MPI_Iprobe( *source, *tag, *comm, flag, status );
}

void mpi_buffer_attach_( void *buffer, int *size , int *ierr)
{
  *ierr = MPI_Buffer_attach( buffer, *size );
}

void mpi_file_write_(MPI_File *fh, void *buf, int *count, 
		     MPI_Datatype *datatype, MPI_Status *status, int *ierr)
{
  *ierr = MPI_File_write( *fh, buf, *count, *datatype, status);
}

void mpi_file_write_at_(MPI_File *fh, MPI_Offset *offset, void *buf,
			int *count, MPI_Datatype *datatype, 
			MPI_Status *status, int *ierr)
{
  *ierr = MPI_File_write_at( *fh, *offset, buf, *count, *datatype, status);
}

void mpi_file_read_at_(MPI_File *fh, MPI_Offset *offset, void *buf,
		       int *count, MPI_Datatype *datatype, MPI_Status *status, int *ierr)
{
  *ierr = MPI_File_read_at( *fh, *offset, buf, *count, *datatype, status);
}

void mpi_file_delete_(char *file, MPI_Info *info, int *ierr, const int size1)
{
  char filename[64];
  strncpy(filename,file,(size_t) size1);
  filename[size1] = '\0';
  
  *ierr = MPI_File_delete( filename, *info);
}

void mpi_type_create_struct_( int *count, int blocklens[], MPI_Aint indices[], 
		       MPI_Datatype old_types[], MPI_Datatype *newtype , int *ierr)
{
  *ierr = mpi_type_create_struct( *count, blocklens, indices, old_types, newtype );
}

void mpi_address_( void *location, MPI_Aint *address, int *ierr)
{
  *ierr = MPI_Address( location, address);
}

void mpi_file_write_all_(MPI_File *fh, void *buf, int *count, 
			 MPI_Datatype *datatype, MPI_Status *status, int *ierr)
{
  *ierr = MPI_File_write_all( *fh, buf, *count, *datatype, status);
}

void mpi_type_indexed_( int *count, int blocklens[], int indices[], 
			MPI_Datatype *old_type, MPI_Datatype *newtype , int *ierr)
{
  *ierr = MPI_Type_indexed( *count, blocklens, indices, *old_type, newtype );
}

void mpi_barrier_( MPI_Comm *comm , int *ierr)
{
  *ierr = MPI_Barrier( *comm );
}

void mpi_file_get_size_(MPI_File *fh, MPI_Offset *size, int *ierr)
{
  *ierr = MPI_File_get_size( *fh, size);
}

#endif
