# INITIALIZATION PARAMETERS ########################

! Name
Simulation :		Taylor vortex

! Parameters
nx :			64
ny :			64
nz :			1

Stretching x :		0.0
Stretching y :		0.0

Orientation :		z

! Files
Init config file :	config
Init data file :	data.init

# RUNNING PARAMETERS ###############################

! Files
Configuration file :	config
Data file to read :	data.init
Data file to write :	data
Data frequency :	10.0

! Partitioning
Processors along X :	2
Processors along Y :	2
Processors along Z :	1

! Chemistry - Properties
Chemistry model :	none
Density :               1.0
Viscosity :             0.15
Diffusivity :           0
Temperature :		300
Pressure :		1e5

! Subgrid Scale model
Use SGS model :		0

! Time advancement
Timestep size :		0.01
CFL number :		0.8
Subiterations :		2
Implicit directions :	none

! End of simulation
Maximum iterations :	100
Maximum time :		100
Maximum wall time :	100

! Schemes
Velocity conv scheme :	2
Velocity visc scheme :	2
Scalar scheme :		bquick

! Pressure
Pressure solver :	bicgstab
Pressure fft :		1
Pressure precond :	tridiag
Pressure cvg :		1e-5
Pressure iterations :	1

! Output
Output type :		visit
Visit outputs:		Velocity P
Output frequency :	0.1
