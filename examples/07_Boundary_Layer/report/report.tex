\NeedsTeXFormat{LaTeX2e}

\documentclass{article}

\usepackage{graphicx}
\usepackage{natbib}
\usepackage{float}
\restylefloat{table}


%%% Example macros (some are not used in this sample file) %%%

% For units of measure
\newcommand\dynpercm{\nobreak\mbox{$\;$dyn\,cm$^{-1}$}}
\newcommand\cmpermin{\nobreak\mbox{$\;$cm\,min$^{-1}$}}

% Various bold symbols
\providecommand\bnabla{\boldsymbol{\nabla}}
\providecommand\bcdot{\boldsymbol{\cdot}}
\newcommand\biS{\boldsymbol{S}}
\newcommand\etb{\boldsymbol{\eta}}

% For multiletter symbols
\newcommand\Real{\mbox{Re}} % cf plain TeX's \Re and Reynolds number
\newcommand\Imag{\mbox{Im}} % cf plain TeX's \Im
\newcommand\Rey{\mbox{\textit{Re}}}  % Reynolds number
\newcommand\Pran{\mbox{\textit{Pr}}} % Prandtl number, cf TeX's \Pr product
\newcommand\Pen{\mbox{\textit{Pe}}}  % Peclet number
\newcommand\Ai{\mbox{Ai}}            % Airy function
\newcommand\Bi{\mbox{Bi}}            % Airy function

% For sans serif characters:
% The following macros are setup in JFM.cls for sans-serif fonts in text
% and math.  If you use these macros in your article, the required fonts
% will be substitued when you article is typeset by the typesetter.
%
% \textsfi, \mathsfi   : sans-serif slanted
% \textsfb, \mathsfb   : sans-serif bold
% \textsfbi, \mathsfbi : sans-serif bold slanted (doesnt exist in CM fonts)
%
% For san-serif roman use \textsf and \mathsf as normal.
%
\newcommand\ssC{\mathsf{C}}    % for sans serif C
\newcommand\sfsP{\mathsfi{P}}  % for sans serif sloping P
\newcommand\slsQ{\mathsfbi{Q}} % for sans serif bold-sloping Q

% Hat position
\newcommand\hatp{\skew3\hat{p}}      % p with hat
\newcommand\hatR{\skew3\hat{R}}      % R with hat
\newcommand\hatRR{\skew3\hat{\hatR}} % R with 2 hats
\newcommand\doubletildesigma{\skew2\tilde{\skew2\tilde{\Sigma}}}
%       italic Sigma with double tilde

% array strut to make delimiters come out right size both ends
\newsavebox{\astrutbox}
\sbox{\astrutbox}{\rule[-5pt]{0pt}{20pt}}
\newcommand{\astrut}{\usebox{\astrutbox}}

\newcommand\GaPQ{\ensuremath{G_a(P,Q)}}
\newcommand\GsPQ{\ensuremath{G_s(P,Q)}}
\newcommand\p{\ensuremath{\partial}}
\newcommand\tti{\ensuremath{\rightarrow\infty}}
\newcommand\kgd{\ensuremath{k\gamma d}}
\newcommand\shalf{\ensuremath{{\scriptstyle\frac{1}{2}}}}
\newcommand\sh{\ensuremath{^{\shalf}}}
\newcommand\smh{\ensuremath{^{-\shalf}}}
\newcommand\squart{\ensuremath{{\textstyle\frac{1}{4}}}}
\newcommand\thalf{\ensuremath{{\textstyle\frac{1}{2}}}}
\newcommand\Gat{\ensuremath{\widetilde{G_a}}}
\newcommand\ttz{\ensuremath{\rightarrow 0}}
\newcommand\ndq{\ensuremath{\frac{\mbox{$\partial$}}{\mbox{$\partial$} n_q}}}
\newcommand\sumjm{\ensuremath{\sum_{j=1}^{M}}}
\newcommand\pvi{\ensuremath{\int_0^{\infty}%
  \mskip \ifCUPmtlplainloaded -30mu\else -33mu\fi -\quad}}

\newcommand\etal{\mbox{\textit{et al.}}}
\newcommand\etc{etc.\ }
\newcommand\eg{e.g.\ }


\newtheorem{lemma}{Lemma}
\newtheorem{corollary}{Corollary}

\title{Spatially developing laminar boundary layer}
\author{Sunil K. Arolla}

\begin{document}

\maketitle

\section{Problem description}\label{sec:intro}

The objective is to simulate spatially evolving laminar boundary layer (see figure \ref{figure1}) using recycling and rescaling method. The momentum thickness Reynolds number at the inflow is, $Re_{\theta}=\theta U_{\infty}/\nu = 100$ with $\theta=1.0$ specified as the target inflow momentum thickness. This corresponds to $Re_{x}=U_{\infty}x/\nu \approx 2.2 \times 10^{4}$ with the kinematic visocity ($\nu$) being $1.0 \times 10^{-2}$ $m^{2}/sec$ and $U_{\infty}=1.0$ $m/sec$. The x location at the inflow is implicit from Blasius boundary layer theory:

\begin{equation}
  \theta = 0.664 \sqrt{\frac{\nu x}{U_{\infty}}}
\end{equation}

The Blasius profile is already coded within NGA math libraries (see figure \ref{figure2}). So, it was used for generating the initial flow field. Note that appropriate scaling for the y coordinate to be consistent is $\eta = y/\sqrt{\nu x/U_{\infty}}$ which corresponds to the following ODE in terms of Blasius function:

\begin{equation}
  ff''+ 2f''' = 0
\end{equation}

\section{Sample results}

Figure \ref{figure3} shows that the simulation converges. The skin friction and momentum thickness reach to an equilibrium value. The variation of momentum thickness in x is shown in figure \ref{figure4}. It varies as $x^{1/2}$ in agreement with the theory. The time averaged velocity profile is compared with the blasius profile in figure \ref{figure5}. The slight disagreement observed could be attributed to the interpolation errors and to the top wall boundary condition.

\begin{figure}
\centering{
\includegraphics[width=80mm]{figure1}
}
\caption{Flow configuration}
\label{figure1}
\end{figure}

\begin{figure}
\centering{
\includegraphics[width=80mm]{figure2}
}
\caption{Checking Blasius profile coded in NGA with Matlab solution}
\label{figure2}
\end{figure}

\begin{figure}
\centering{
\includegraphics[width=80mm]{figure3}
}
\caption{Convergence history}
\label{figure3}
\end{figure}

\begin{figure}
\centering{
\includegraphics[width=80mm]{figure4}
}
\caption{Variation of momentum thickness along the streamwise direction}
\label{figure4}
\end{figure}

\begin{figure}
\centering{
\includegraphics[width=80mm]{figure5}
}
\caption{Time averaged velocity profile}
\label{figure5}
\end{figure}


\bibliographystyle{jfm}
\bibliography{report}

\end{document}
